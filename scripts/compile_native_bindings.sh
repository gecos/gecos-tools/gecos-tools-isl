#!/bin/bash

# GMP lib
pushd bundles/fr.irisa.cairn.jnimap.isl/native/build/gmp
make
popd

# ISL lib
pushd bundles/fr.irisa.cairn.jnimap.isl/native/build/isl
rm -rf isl
make
popd

# ISL bindings
pushd bundles/fr.irisa.cairn.jnimap.isl/native/build
make
popd

# NTL lib
pushd bundles/fr.irisa.cairn.jnimap.barvinok/native/build/ntl
make
popd

# PolyLib lib
pushd bundles/fr.irisa.cairn.jnimap.polylib/native/build/polylib
rm -rf polylib
make
popd

# Barvinok Lib
pushd bundles/fr.irisa.cairn.jnimap.barvinok/native/build/barvinok
rm -rf barvinok
make
popd

# Polylib bindings
pushd bundles/fr.irisa.cairn.jnimap.polylib/native/build
make
popd

# Barvinok bindings
pushd bundles/fr.irisa.cairn.jnimap.barvinok/native/build
make
popd
