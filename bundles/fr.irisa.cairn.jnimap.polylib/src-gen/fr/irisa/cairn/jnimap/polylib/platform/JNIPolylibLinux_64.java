package fr.irisa.cairn.jnimap.polylib.platform;

import java.io.File;

public class JNIPolylibLinux_64 extends JNIPolylibAbstractPlatform {

	@Override
	protected String getDllPath() {
		return new File(".")+File.separator+"./.jnimap.temp.linux_64";
	}

	public void loadPlatformLibraries() {
		// Get input stream from jar resource
		String lib = "linux_64_libjnipolylib.so";
		try {
			//Copy other dynamic libraries from jar to temporary location
			//Copy PolyLib
			copyLibToTemp("Polylib_linux_64" + File.separator, "libpolylibgmp.so.8");
			//Copy GMP
			copyLibToTemp("Polylib_linux_64" + File.separator, "libgmp.so.10");
			//Copy the binding object file from jar to temporary location
			File JNIDLL = copyLibToTemp("", lib);
			// Finally, load the dll
			System.load(JNIDLL.getAbsolutePath());
		} catch (Exception e) {
			throw new RuntimeException("Problem during native library loading Polylib:",e);
		}
	}
}
