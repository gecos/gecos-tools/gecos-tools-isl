package fr.irisa.cairn.jnimap.polylib;

import fr.irisa.cairn.jnimap.polylib.platform.*;
import fr.irisa.cairn.jnimap.isl.ISLNative;

public class PolylibNative {

	private static final String arch = System.getProperty("sun.arch.data.model");

	private static JNIPolylibAbstractPlatform loader ;

	private static boolean loaded = false;
	public static void loadLibrary() {
		if (loaded) return;
		ISLNative.loadLibrary();
		String osName = System.getProperty("os.name");
		String arch   = System.getProperty("sun.arch.data.model");
		if (osName.equals("Linux")) {
			if (arch.equals("32")) {
				throw new RuntimeException("Linux_32 is not supported.");
			} else {
				loader = new JNIPolylibLinux_64();
			}
		} else if (osName.equals("Windows XP")) {
			loader = new JNIPolylibCygwin_32();
		} else if (osName.equals("Mac OS X")) {
			if (arch.equals("32")) {
				throw new RuntimeException("Macosx_32 is not supported.");
			} else {
				loader = new JNIPolylibMacosx_64();
			} 
		} else  {
			throw new RuntimeException("Unsupported Operating system : " + osName+"_"+arch);
		}
		loader.loadPlatformLibraries();
		loaded = true;
	}
	
	static {
		loadLibrary();
	}

	public static native long derefPointer(long ptr);
	
	
	/*************************************************************
	 *	                     USER MODULES                        *
	 *************************************************************/
	 
	 public static class UserModules {
	 	// UserModule : matrix_getset
	 	public static native long matrix_get(long m, int i, int j);
	 	public static native void matrix_set(long m, int i, int j, long value);
	}
	
	/*************************************************************
	 *	                        METHODS                          *
	 *************************************************************/
	 public static native long Matrix_Alloc(int nbrow, int nbcols);
	 public static native long matrix_get(long m, int i, int j);
	 public static native void matrix_set(long m, int i, int j, long value);
	 public static native void Matrix_Free(long m);
	 public static native long Polyhedron2Constraints(long Pol);
	 public static native long Empty_Polyhedron(int Dimension);
	 public static native long Polyhedron_Alloc(int Dimension, int NbConstraints, int NbRays);
	 public static native long Constraints2Polyhedron(long Constraints, int NbMaxRays);
	 public static native long Rays2Polyhedron(long Ray, int NbMaxConstrs);
	 public static native long Universe_Polyhedron(int Dimension);
	 public static native long DomainDifference(long Pol1, long Pol2, int NbMaxRays);
	 public static native long DomainIntersection(long Pol1, long Pol2, int NbMaxRays);
	 public static native long DomainSimplify(long Pol1, long Pol2, int NbMaxRays);
	 public static native long DomainConstraintSimplify(long P, int MaxRays);
	 public static native long AddPolyToDomain(long Pol, long PolDomain);
	 public static native long align_context(long Pol, int align_dimension, int NbMaxRays);
	 public static native long Disjoint_Domain(long Pol, int flag, int NbMaxRays);
	 public static native long Stras_DomainSimplify(long Pol1, long Pol2, int NbMaxRays);
	 public static native long Polyhedron2Rays(long Pol);
	 public static native int PolyhedronIncludes(long Pol1, long Pol2);
	 public static native long Polyhedron_Copy(long Pol);
	 public static native void Polyhedron_Free(long Pol);
	 public static native long Polyhedron_Image(long Pol, long Func, int NbMaxConstrs);
	 public static native long Polyhedron_Preimage(long Pol, long Func, int NbMaxRays);
	 public static native long Polyhedron_Scan(long D, long C, int MAXRAYS);
	 public static native void PolyPrint(long Pol);
	 public static native void Domain_Free(long Pol);
	 public static native int Gauss(long Mat, int NbEq, int Dimension);
	 public static native long DomainUnion(long Pol1, long Pol2, int NbMaxRays);
	 public static native long Domain_Copy(long Pol);
	 public static native long DomainAddConstraints(long Pol, long Mat, int NbMaxRays);
	 public static native long DomainAddRays(long Pol, long Ray, int NbMaxConstrs);
	 public static native long DomainConvex(long Pol, int NbMaxConstrs);
	 
	/*************************************************************
	 *	                    CLASS MAPPING                        *
	 *************************************************************/
	 // getter for PolyLibMatrix.NbRows
	 public static native int matrix_get_NbRows(long ptr);
	 // tester for PolyLibMatrix.NbRows
	 public static native int matrix_test_NbRows(long ptr);
	 // setter for PolyLibMatrix.NbRows
	 public static native void matrix_set_NbRows(long ptr, int value);
	 // getter for PolyLibMatrix.NbColumns
	 public static native int matrix_get_NbColumns(long ptr);
	 // tester for PolyLibMatrix.NbColumns
	 public static native int matrix_test_NbColumns(long ptr);
	 // setter for PolyLibMatrix.NbColumns
	 public static native void matrix_set_NbColumns(long ptr, int value);
	 // getter for PolyLibPolyhedron.Dimension
	 public static native int polyhedron_get_Dimension(long ptr);
	 // tester for PolyLibPolyhedron.Dimension
	 public static native int polyhedron_test_Dimension(long ptr);
	 // setter for PolyLibPolyhedron.Dimension
	 public static native void polyhedron_set_Dimension(long ptr, int value);
	 // getter for PolyLibPolyhedron.NbConstraints
	 public static native int polyhedron_get_NbConstraints(long ptr);
	 // tester for PolyLibPolyhedron.NbConstraints
	 public static native int polyhedron_test_NbConstraints(long ptr);
	 // setter for PolyLibPolyhedron.NbConstraints
	 public static native void polyhedron_set_NbConstraints(long ptr, int value);
	 // getter for PolyLibPolyhedron.NbRays
	 public static native int polyhedron_get_NbRays(long ptr);
	 // tester for PolyLibPolyhedron.NbRays
	 public static native int polyhedron_test_NbRays(long ptr);
	 // setter for PolyLibPolyhedron.NbRays
	 public static native void polyhedron_set_NbRays(long ptr, int value);
	 // getter for PolyLibPolyhedron.NbEq
	 public static native int polyhedron_get_NbEq(long ptr);
	 // tester for PolyLibPolyhedron.NbEq
	 public static native int polyhedron_test_NbEq(long ptr);
	 // setter for PolyLibPolyhedron.NbEq
	 public static native void polyhedron_set_NbEq(long ptr, int value);
	 // getter for PolyLibPolyhedron.NbBid
	 public static native int polyhedron_get_NbBid(long ptr);
	 // tester for PolyLibPolyhedron.NbBid
	 public static native int polyhedron_test_NbBid(long ptr);
	 // setter for PolyLibPolyhedron.NbBid
	 public static native void polyhedron_set_NbBid(long ptr, int value);
	


}
