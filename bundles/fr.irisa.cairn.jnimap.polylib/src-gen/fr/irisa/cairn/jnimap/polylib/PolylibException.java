package fr.irisa.cairn.jnimap.polylib;

public class PolylibException extends Exception {
	private static final long serialVersionUID = 1L;
	public PolylibException(String msg) {
		super(msg);
	} 
}
