package fr.irisa.cairn.jnimap.polylib.extra;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.jnimap.polylib.PolyLibMatrix;
import fr.irisa.cairn.jnimap.polylib.PolyLibPolyhedron;

public class PolylibVertexRay {

	public static class Pair {
		public long num;
		public long denum;
		public Pair(long n, long d) {
			num = n; denum = d;
		}
		@Override
		public String toString() {
			return num+((denum != 1)?"/"+denum:"");
		}
	}
	
	public static class DualPolyhedron {
		public List<List<Pair>> vertices;
		public List<List<Pair>> rays;
		public List<List<Pair>> lines;
		public DualPolyhedron() {
			vertices = new ArrayList<List<Pair>>();
			rays = new ArrayList<List<Pair>>();
			lines = new ArrayList<List<Pair>>();
		}
		@Override
		public String toString() {
			StringBuffer sb = new StringBuffer();
			boolean first;
			
			sb.append("vertices : {");
			first = true;
			for (List<Pair> l : vertices) {
				if (first) first = false;
				else sb.append(", ");
				sb.append("[");
				boolean first2 = true;
				for (Pair p : l) {
					if (first2) first2 = false;
					else sb.append(", ");
					sb.append(p);
				}
				sb.append("]");
			}
			sb.append("}\n");
			
			sb.append("rays : {");
			first = true;
			for (List<Pair> l : rays) {
				if (first) first = false;
				else sb.append(", ");
				sb.append("[");
				boolean first2 = true;
				for (Pair p : l) {
					if (first2) first2 = false;
					else sb.append(", ");
					sb.append(p);
				}
				sb.append("]");
			}
			sb.append("}\n");
			sb.append("lines : {");
			first = true;
			for (List<Pair> l : lines) {
				if (first) first = false;
				else sb.append(", ");
				sb.append("[");
				boolean first2 = true;
				for (Pair p : l) {
					if (first2) first2 = false;
					else sb.append(", ");
					sb.append(p);
				}
				sb.append("]");
			}
			sb.append("}");
			
			return sb.toString();
		}
	}
	
	public static DualPolyhedron compute(PolyLibPolyhedron p) {
		PolyLibMatrix rv = p.builRaysVertices();
		DualPolyhedron res = new DualPolyhedron();

		int rows = rv.getNbRows();
		int cols = rv.getNbColumns();
		int size = cols-2;

		for (int i = 0; i < rows; i++) {
			long S = rv.getAt(i,0);
			long K = rv.getAt(i,cols-1);
			
			/*
			 * PolyLib manual, page 17 (bottom) :
			 * S = 0 >> line
			 * S = 1 & K = 0 >> ray
			 * S = 1 & K != 0 >> vertex (divide coefficients by K)
			 */
			
			List<Pair> l = new ArrayList<PolylibVertexRay.Pair>(size);
			for (int j = 1; j < cols-1; j++) {
				l.add(new Pair(rv.getAt(i, j),(S == 0 || K == 0)?1:K));
			}
			
			if (S == 0)
				res.lines.add(l);
			else if (K == 0)
				res.rays.add(l);
			else
				res.vertices.add(l);
		}
		
		return res;
	}
}
