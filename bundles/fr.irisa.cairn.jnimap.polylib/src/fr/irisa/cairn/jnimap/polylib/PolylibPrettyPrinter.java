package fr.irisa.cairn.jnimap.polylib;

import fr.irisa.cairn.jnimap.polylib.PolyLibMatrix;
import fr.irisa.cairn.jnimap.polylib.PolyLibPolyhedron;
import fr.irisa.cairn.jnimap.polylib.PolyLibVector;

public class PolylibPrettyPrinter {

	public static String asString(PolyLibPolyhedron jniPolyhedron) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String asString(PolyLibMatrix m) {
		int rows = m.getNbRows();
		int cols = m.getNbColumns();
		String s = "";
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				if (j > 0) s += ",\t";
				s += (m.getAt(i, j));
			}
			s += "\n";
		}
		return s;
	}

//	public static String asString(JNIPolyLibValue jniValue) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	public static String asString(PolyLibVector jniVector) {
		// TODO Auto-generated method stub
		return null;
	}




}
