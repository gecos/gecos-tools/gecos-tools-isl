#include "PolylibUser_matrix_getset.h"

/*PROTECTED REGION ID(PolylibUser_matrix_getset_local) ENABLED START*/
/* Protected region for methods used locally in this file */
/*PROTECTED REGION END*/	

long matrix_get(struct matrix* m, int i, int j) {
/*PROTECTED REGION ID(PolylibUser_matrix_getset_matrix_get) ENABLED START*/
	if((unsigned int)i<(m->NbRows) && (unsigned int)j<(m->NbColumns))
		return VALUE_TO_INT(m->p[i][j]);
	else
		return -0x123456;
/*PROTECTED REGION END*/	
}
void matrix_set(struct matrix* m, int i, int j, long value) {
/*PROTECTED REGION ID(PolylibUser_matrix_getset_matrix_set) ENABLED START*/
	if((unsigned int)i<(m->NbRows) && (unsigned int)j<(m->NbColumns))
		mpz_set_si(m->p[i][j],value);
	else
		fprintf(stderr,"Error : [%d,%d] is out of bound for M[0:%d][0:%d] ",i,j,m->NbRows-1,m->NbColumns-1);
/*PROTECTED REGION END*/	
}
