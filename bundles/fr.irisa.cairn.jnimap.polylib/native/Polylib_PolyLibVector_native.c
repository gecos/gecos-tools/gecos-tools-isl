#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <polylib/polylibgmp.h>
#include <polylib/arithmetique.h>
#include <polylib/arithmetic_errors.h>
#include <polylib/types.h>
#include <polylib/errormsg.h>
#include <polylib/vector.h>
#include <polylib/matrix.h>
#include <polylib/polyhedron.h>
#include <polylib/polyparam.h>
#include <polylib/param.h>
#include <polylib/alpha.h>
#include <polylib/ehrhart.h>
#include <polylib/ext_ehrhart.h>
#include <polylib/eval_ehrhart.h>
#include <polylib/SolveDio.h>
#include <polylib/Lattice.h>
#include <polylib/Matop.h>
#include <polylib/NormalForms.h>
#include <polylib/Zpolyhedron.h>
#include <polylib/matrix_addon.h>
#include <polylib/matrix_permutations.h>
#include <polylib/compress_parms.h>
#include <gmp.h>

#include "PolylibUser_matrix_getset.h"

#include "fr_irisa_cairn_jnimap_polylib_jni_PolylibNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);




