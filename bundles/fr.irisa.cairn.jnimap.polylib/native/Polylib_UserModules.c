#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <polylib/polylibgmp.h>
#include <polylib/arithmetique.h>
#include <polylib/arithmetic_errors.h>
#include <polylib/types.h>
#include <polylib/errormsg.h>
#include <polylib/vector.h>
#include <polylib/matrix.h>
#include <polylib/polyhedron.h>
#include <polylib/polyparam.h>
#include <polylib/param.h>
#include <polylib/alpha.h>
#include <polylib/ehrhart.h>
#include <polylib/ext_ehrhart.h>
#include <polylib/eval_ehrhart.h>
#include <polylib/SolveDio.h>
#include <polylib/Lattice.h>
#include <polylib/Matop.h>
#include <polylib/NormalForms.h>
#include <polylib/Zpolyhedron.h>
#include <polylib/matrix_addon.h>
#include <polylib/matrix_permutations.h>
#include <polylib/compress_parms.h>
#include <gmp.h>

#include "fr_irisa_cairn_jnimap_polylib_PolylibNative.h"
#include "fr_irisa_cairn_jnimap_polylib_PolylibNative_UserModules.h"



/**********************************
 ** matrix_getset
 **********************************/
#include "PolylibUser_matrix_getset.h"

//matrix_getset . matrix_get
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_00024UserModules_matrix_1get
(JNIEnv *env, jclass clazz, jlong m, jint i, jint j)
 {
	struct matrix* m_c = (struct matrix*)GECOS_PTRSIZE m;
	int i_c = (int)i;
	int j_c = (int)j;
	
	long res = (long) matrix_get(m_c, i_c, j_c);

	return (jlong)  res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
//matrix_getset . matrix_set
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_00024UserModules_matrix_1set
(JNIEnv *env, jclass clazz, jlong m, jint i, jint j, jlong value)
 {
	struct matrix* m_c = (struct matrix*)GECOS_PTRSIZE m;
	int i_c = (int)i;
	int j_c = (int)j;
	long value_c = (long)value;
	
	 matrix_set(m_c, i_c, j_c, value_c);
error:
	return;
}
