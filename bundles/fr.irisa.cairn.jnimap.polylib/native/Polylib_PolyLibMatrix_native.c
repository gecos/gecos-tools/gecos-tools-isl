#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <polylib/polylibgmp.h>
#include <polylib/arithmetique.h>
#include <polylib/arithmetic_errors.h>
#include <polylib/types.h>
#include <polylib/errormsg.h>
#include <polylib/vector.h>
#include <polylib/matrix.h>
#include <polylib/polyhedron.h>
#include <polylib/polyparam.h>
#include <polylib/param.h>
#include <polylib/alpha.h>
#include <polylib/ehrhart.h>
#include <polylib/ext_ehrhart.h>
#include <polylib/eval_ehrhart.h>
#include <polylib/SolveDio.h>
#include <polylib/Lattice.h>
#include <polylib/Matop.h>
#include <polylib/NormalForms.h>
#include <polylib/Zpolyhedron.h>
#include <polylib/matrix_addon.h>
#include <polylib/matrix_permutations.h>
#include <polylib/compress_parms.h>
#include <gmp.h>

#include "PolylibUser_matrix_getset.h"

#include "fr_irisa_cairn_jnimap_polylib_PolylibNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_matrix_1get_1NbRows
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(matrix_NbRows_getter) DISABLED START */
	struct matrix* stPtr = (struct matrix *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getNbRows");
	return (jint) GECOS_PTRSIZE stPtr->NbRows;
	/* PROTECTED REGION END */
}

JNIEXPORT void JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_matrix_1set_1NbRows
	(JNIEnv *env, jclass class, jlong ptr, jint value) {
	/* PROTECTED REGION ID(matrix_NbRows_setter) DISABLED START */
	struct matrix* stPtr = (struct matrix *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in setNbRows");
	stPtr->NbRows= (int) GECOS_PTRSIZE value;
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_matrix_1test_1NbRows
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(matrix_NbRows_tester) DISABLED START */
	struct matrix* stPtr = (struct matrix *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getNbRows");
	return (GECOS_PTRSIZE stPtr->NbRows) != (GECOS_PTRSIZE NULL);
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_matrix_1get_1NbColumns
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(matrix_NbColumns_getter) DISABLED START */
	struct matrix* stPtr = (struct matrix *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getNbColumns");
	return (jint) GECOS_PTRSIZE stPtr->NbColumns;
	/* PROTECTED REGION END */
}

JNIEXPORT void JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_matrix_1set_1NbColumns
	(JNIEnv *env, jclass class, jlong ptr, jint value) {
	/* PROTECTED REGION ID(matrix_NbColumns_setter) DISABLED START */
	struct matrix* stPtr = (struct matrix *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in setNbColumns");
	stPtr->NbColumns= (int) GECOS_PTRSIZE value;
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_matrix_1test_1NbColumns
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(matrix_NbColumns_tester) DISABLED START */
	struct matrix* stPtr = (struct matrix *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getNbColumns");
	return (GECOS_PTRSIZE stPtr->NbColumns) != (GECOS_PTRSIZE NULL);
	/* PROTECTED REGION END */
}


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Matrix_1Alloc
(JNIEnv *env, jclass class, jint nbrow, jint nbcols)
 {
#ifdef TRACE_ALL
	printf("Entering Matrix_Alloc\n");fflush(stdout);
#endif
	int nbrow_c = (int) nbrow;
	int nbcols_c = (int) nbcols;

	struct matrix* res = (struct matrix*) Matrix_Alloc(nbrow_c, nbcols_c);


#ifdef TRACE_ALL
	printf("Leaving Matrix_Alloc\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_matrix_1get
(JNIEnv *env, jclass class, jlong m, jint i, jint j)
 {
#ifdef TRACE_ALL
	printf("Entering matrix_get\n");fflush(stdout);
#endif
	struct matrix* m_c = (struct matrix*) GECOS_PTRSIZE m; 
	if(((void*)m_c)==NULL) {
		throwException(env, "Null pointer in matrix_get for parameter m");
		goto error;
	}
	int i_c = (int) i;
	int j_c = (int) j;

	long res = (long) matrix_get(m_c, i_c, j_c);


#ifdef TRACE_ALL
	printf("Leaving matrix_get\n");fflush(stdout);
#endif
	

	return (jlong)  res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_matrix_1set
(JNIEnv *env, jclass class, jlong m, jint i, jint j, jlong value)
 {
#ifdef TRACE_ALL
	printf("Entering matrix_set\n");fflush(stdout);
#endif
	struct matrix* m_c = (struct matrix*) GECOS_PTRSIZE m; 
	if(((void*)m_c)==NULL) {
		throwException(env, "Null pointer in matrix_set for parameter m");
		goto error;
	}
	int i_c = (int) i;
	int j_c = (int) j;
	long value_c = (long) value;

	 matrix_set(m_c, i_c, j_c, value_c);


#ifdef TRACE_ALL
	printf("Leaving matrix_set\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Matrix_1Free
(JNIEnv *env, jclass class, jlong m)
 {
#ifdef TRACE_ALL
	printf("Entering Matrix_Free\n");fflush(stdout);
#endif
	struct matrix* m_c = (struct matrix*) GECOS_PTRSIZE m; 
	if(((void*)m_c)==NULL) {
		throwException(env, "Null pointer in Matrix_Free for parameter m");
		goto error;
	}

	 Matrix_Free(m_c);


#ifdef TRACE_ALL
	printf("Leaving Matrix_Free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Polyhedron2Constraints
(JNIEnv *env, jclass class, jlong Pol)
 {
#ifdef TRACE_ALL
	printf("Entering Polyhedron2Constraints\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in Polyhedron2Constraints for parameter Pol");
		goto error;
	}

	struct matrix* res = (struct matrix*) Polyhedron2Constraints(Pol_c);


#ifdef TRACE_ALL
	printf("Leaving Polyhedron2Constraints\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}


