#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <polylib/polylibgmp.h>
#include <polylib/arithmetique.h>
#include <polylib/arithmetic_errors.h>
#include <polylib/types.h>
#include <polylib/errormsg.h>
#include <polylib/vector.h>
#include <polylib/matrix.h>
#include <polylib/polyhedron.h>
#include <polylib/polyparam.h>
#include <polylib/param.h>
#include <polylib/alpha.h>
#include <polylib/ehrhart.h>
#include <polylib/ext_ehrhart.h>
#include <polylib/eval_ehrhart.h>
#include <polylib/SolveDio.h>
#include <polylib/Lattice.h>
#include <polylib/Matop.h>
#include <polylib/NormalForms.h>
#include <polylib/Zpolyhedron.h>
#include <polylib/matrix_addon.h>
#include <polylib/matrix_permutations.h>
#include <polylib/compress_parms.h>
#include <gmp.h>

#include "PolylibUser_matrix_getset.h"

#include "fr_irisa_cairn_jnimap_polylib_PolylibNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1get_1Dimension
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(polyhedron_Dimension_getter) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getDimension");
	return (jint) GECOS_PTRSIZE stPtr->Dimension;
	/* PROTECTED REGION END */
}

JNIEXPORT void JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1set_1Dimension
	(JNIEnv *env, jclass class, jlong ptr, jint value) {
	/* PROTECTED REGION ID(polyhedron_Dimension_setter) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in setDimension");
	stPtr->Dimension= (int) GECOS_PTRSIZE value;
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1test_1Dimension
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(polyhedron_Dimension_tester) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getDimension");
	return (GECOS_PTRSIZE stPtr->Dimension) != (GECOS_PTRSIZE NULL);
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1get_1NbConstraints
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(polyhedron_NbConstraints_getter) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getNbConstraints");
	return (jint) GECOS_PTRSIZE stPtr->NbConstraints;
	/* PROTECTED REGION END */
}

JNIEXPORT void JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1set_1NbConstraints
	(JNIEnv *env, jclass class, jlong ptr, jint value) {
	/* PROTECTED REGION ID(polyhedron_NbConstraints_setter) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in setNbConstraints");
	stPtr->NbConstraints= (int) GECOS_PTRSIZE value;
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1test_1NbConstraints
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(polyhedron_NbConstraints_tester) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getNbConstraints");
	return (GECOS_PTRSIZE stPtr->NbConstraints) != (GECOS_PTRSIZE NULL);
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1get_1NbRays
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(polyhedron_NbRays_getter) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getNbRays");
	return (jint) GECOS_PTRSIZE stPtr->NbRays;
	/* PROTECTED REGION END */
}

JNIEXPORT void JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1set_1NbRays
	(JNIEnv *env, jclass class, jlong ptr, jint value) {
	/* PROTECTED REGION ID(polyhedron_NbRays_setter) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in setNbRays");
	stPtr->NbRays= (int) GECOS_PTRSIZE value;
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1test_1NbRays
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(polyhedron_NbRays_tester) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getNbRays");
	return (GECOS_PTRSIZE stPtr->NbRays) != (GECOS_PTRSIZE NULL);
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1get_1NbEq
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(polyhedron_NbEq_getter) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getNbEq");
	return (jint) GECOS_PTRSIZE stPtr->NbEq;
	/* PROTECTED REGION END */
}

JNIEXPORT void JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1set_1NbEq
	(JNIEnv *env, jclass class, jlong ptr, jint value) {
	/* PROTECTED REGION ID(polyhedron_NbEq_setter) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in setNbEq");
	stPtr->NbEq= (int) GECOS_PTRSIZE value;
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1test_1NbEq
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(polyhedron_NbEq_tester) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getNbEq");
	return (GECOS_PTRSIZE stPtr->NbEq) != (GECOS_PTRSIZE NULL);
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1get_1NbBid
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(polyhedron_NbBid_getter) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getNbBid");
	return (jint) GECOS_PTRSIZE stPtr->NbBid;
	/* PROTECTED REGION END */
}

JNIEXPORT void JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1set_1NbBid
	(JNIEnv *env, jclass class, jlong ptr, jint value) {
	/* PROTECTED REGION ID(polyhedron_NbBid_setter) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in setNbBid");
	stPtr->NbBid= (int) GECOS_PTRSIZE value;
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_polyhedron_1test_1NbBid
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(polyhedron_NbBid_tester) DISABLED START */
	struct polyhedron* stPtr = (struct polyhedron *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getNbBid");
	return (GECOS_PTRSIZE stPtr->NbBid) != (GECOS_PTRSIZE NULL);
	/* PROTECTED REGION END */
}


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Empty_1Polyhedron
(JNIEnv *env, jclass class, jint Dimension)
 {
#ifdef TRACE_ALL
	printf("Entering Empty_Polyhedron\n");fflush(stdout);
#endif
	int Dimension_c = (int) Dimension;

	struct polyhedron* res = (struct polyhedron*) Empty_Polyhedron(Dimension_c);


#ifdef TRACE_ALL
	printf("Leaving Empty_Polyhedron\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Polyhedron_1Alloc
(JNIEnv *env, jclass class, jint Dimension, jint NbConstraints, jint NbRays)
 {
#ifdef TRACE_ALL
	printf("Entering Polyhedron_Alloc\n");fflush(stdout);
#endif
	int Dimension_c = (int) Dimension;
	int NbConstraints_c = (int) NbConstraints;
	int NbRays_c = (int) NbRays;

	struct polyhedron* res = (struct polyhedron*) Polyhedron_Alloc(Dimension_c, NbConstraints_c, NbRays_c);


#ifdef TRACE_ALL
	printf("Leaving Polyhedron_Alloc\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Constraints2Polyhedron
(JNIEnv *env, jclass class, jlong Constraints, jint NbMaxRays)
 {
#ifdef TRACE_ALL
	printf("Entering Constraints2Polyhedron\n");fflush(stdout);
#endif
	struct matrix* Constraints_c = (struct matrix*) GECOS_PTRSIZE Constraints; 
	if(((void*)Constraints_c)==NULL) {
		throwException(env, "Null pointer in Constraints2Polyhedron for parameter Constraints");
		goto error;
	}
	int NbMaxRays_c = (int) NbMaxRays;

	struct polyhedron* res = (struct polyhedron*) Constraints2Polyhedron(Constraints_c, NbMaxRays_c);


#ifdef TRACE_ALL
	printf("Leaving Constraints2Polyhedron\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Rays2Polyhedron
(JNIEnv *env, jclass class, jlong Ray, jint NbMaxConstrs)
 {
#ifdef TRACE_ALL
	printf("Entering Rays2Polyhedron\n");fflush(stdout);
#endif
	struct matrix* Ray_c = (struct matrix*) GECOS_PTRSIZE Ray; 
	if(((void*)Ray_c)==NULL) {
		throwException(env, "Null pointer in Rays2Polyhedron for parameter Ray");
		goto error;
	}
	int NbMaxConstrs_c = (int) NbMaxConstrs;

	struct polyhedron* res = (struct polyhedron*) Rays2Polyhedron(Ray_c, NbMaxConstrs_c);


#ifdef TRACE_ALL
	printf("Leaving Rays2Polyhedron\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Universe_1Polyhedron
(JNIEnv *env, jclass class, jint Dimension)
 {
#ifdef TRACE_ALL
	printf("Entering Universe_Polyhedron\n");fflush(stdout);
#endif
	int Dimension_c = (int) Dimension;

	struct polyhedron* res = (struct polyhedron*) Universe_Polyhedron(Dimension_c);


#ifdef TRACE_ALL
	printf("Leaving Universe_Polyhedron\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_DomainDifference
(JNIEnv *env, jclass class, jlong Pol1, jlong Pol2, jint NbMaxRays)
 {
#ifdef TRACE_ALL
	printf("Entering DomainDifference\n");fflush(stdout);
#endif
	struct polyhedron* Pol1_c = (struct polyhedron*) GECOS_PTRSIZE Pol1; 
	if(((void*)Pol1_c)==NULL) {
		throwException(env, "Null pointer in DomainDifference for parameter Pol1");
		goto error;
	}
	struct polyhedron* Pol2_c = (struct polyhedron*) GECOS_PTRSIZE Pol2; 
	if(((void*)Pol2_c)==NULL) {
		throwException(env, "Null pointer in DomainDifference for parameter Pol2");
		goto error;
	}
	int NbMaxRays_c = (int) NbMaxRays;

	struct polyhedron* res = (struct polyhedron*) DomainDifference(Pol1_c, Pol2_c, NbMaxRays_c);


#ifdef TRACE_ALL
	printf("Leaving DomainDifference\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_DomainIntersection
(JNIEnv *env, jclass class, jlong Pol1, jlong Pol2, jint NbMaxRays)
 {
#ifdef TRACE_ALL
	printf("Entering DomainIntersection\n");fflush(stdout);
#endif
	struct polyhedron* Pol1_c = (struct polyhedron*) GECOS_PTRSIZE Pol1; 
	if(((void*)Pol1_c)==NULL) {
		throwException(env, "Null pointer in DomainIntersection for parameter Pol1");
		goto error;
	}
	struct polyhedron* Pol2_c = (struct polyhedron*) GECOS_PTRSIZE Pol2; 
	if(((void*)Pol2_c)==NULL) {
		throwException(env, "Null pointer in DomainIntersection for parameter Pol2");
		goto error;
	}
	int NbMaxRays_c = (int) NbMaxRays;

	struct polyhedron* res = (struct polyhedron*) DomainIntersection(Pol1_c, Pol2_c, NbMaxRays_c);


#ifdef TRACE_ALL
	printf("Leaving DomainIntersection\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_DomainSimplify
(JNIEnv *env, jclass class, jlong Pol1, jlong Pol2, jint NbMaxRays)
 {
#ifdef TRACE_ALL
	printf("Entering DomainSimplify\n");fflush(stdout);
#endif
	struct polyhedron* Pol1_c = (struct polyhedron*) GECOS_PTRSIZE Pol1; 
	if(((void*)Pol1_c)==NULL) {
		throwException(env, "Null pointer in DomainSimplify for parameter Pol1");
		goto error;
	}
	struct polyhedron* Pol2_c = (struct polyhedron*) GECOS_PTRSIZE Pol2; 
	if(((void*)Pol2_c)==NULL) {
		throwException(env, "Null pointer in DomainSimplify for parameter Pol2");
		goto error;
	}
	int NbMaxRays_c = (int) NbMaxRays;

	struct polyhedron* res = (struct polyhedron*) DomainSimplify(Pol1_c, Pol2_c, NbMaxRays_c);


#ifdef TRACE_ALL
	printf("Leaving DomainSimplify\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_DomainConstraintSimplify
(JNIEnv *env, jclass class, jlong P, jint MaxRays)
 {
#ifdef TRACE_ALL
	printf("Entering DomainConstraintSimplify\n");fflush(stdout);
#endif
	struct polyhedron* P_c = (struct polyhedron*) GECOS_PTRSIZE P; 
	if(((void*)P_c)==NULL) {
		throwException(env, "Null pointer in DomainConstraintSimplify for parameter P");
		goto error;
	}
	int MaxRays_c = (int) MaxRays;

	struct polyhedron* res = (struct polyhedron*) DomainConstraintSimplify(P_c, MaxRays_c);


#ifdef TRACE_ALL
	printf("Leaving DomainConstraintSimplify\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_AddPolyToDomain
(JNIEnv *env, jclass class, jlong Pol, jlong PolDomain)
 {
#ifdef TRACE_ALL
	printf("Entering AddPolyToDomain\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in AddPolyToDomain for parameter Pol");
		goto error;
	}
	struct polyhedron* PolDomain_c = (struct polyhedron*) GECOS_PTRSIZE PolDomain; 
	if(((void*)PolDomain_c)==NULL) {
		throwException(env, "Null pointer in AddPolyToDomain for parameter PolDomain");
		goto error;
	}

	struct polyhedron* res = (struct polyhedron*) AddPolyToDomain(Pol_c, PolDomain_c);


#ifdef TRACE_ALL
	printf("Leaving AddPolyToDomain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_align_1context
(JNIEnv *env, jclass class, jlong Pol, jint align_dimension, jint NbMaxRays)
 {
#ifdef TRACE_ALL
	printf("Entering align_context\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in align_context for parameter Pol");
		goto error;
	}
	int align_dimension_c = (int) align_dimension;
	int NbMaxRays_c = (int) NbMaxRays;

	struct polyhedron* res = (struct polyhedron*) align_context(Pol_c, align_dimension_c, NbMaxRays_c);


#ifdef TRACE_ALL
	printf("Leaving align_context\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Disjoint_1Domain
(JNIEnv *env, jclass class, jlong Pol, jint flag, jint NbMaxRays)
 {
#ifdef TRACE_ALL
	printf("Entering Disjoint_Domain\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in Disjoint_Domain for parameter Pol");
		goto error;
	}
	int flag_c = (int) flag;
	int NbMaxRays_c = (int) NbMaxRays;

	struct polyhedron* res = (struct polyhedron*) Disjoint_Domain(Pol_c, flag_c, NbMaxRays_c);


#ifdef TRACE_ALL
	printf("Leaving Disjoint_Domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Stras_1DomainSimplify
(JNIEnv *env, jclass class, jlong Pol1, jlong Pol2, jint NbMaxRays)
 {
#ifdef TRACE_ALL
	printf("Entering Stras_DomainSimplify\n");fflush(stdout);
#endif
	struct polyhedron* Pol1_c = (struct polyhedron*) GECOS_PTRSIZE Pol1; 
	if(((void*)Pol1_c)==NULL) {
		throwException(env, "Null pointer in Stras_DomainSimplify for parameter Pol1");
		goto error;
	}
	struct polyhedron* Pol2_c = (struct polyhedron*) GECOS_PTRSIZE Pol2; 
	if(((void*)Pol2_c)==NULL) {
		throwException(env, "Null pointer in Stras_DomainSimplify for parameter Pol2");
		goto error;
	}
	int NbMaxRays_c = (int) NbMaxRays;

	struct polyhedron* res = (struct polyhedron*) Stras_DomainSimplify(Pol1_c, Pol2_c, NbMaxRays_c);


#ifdef TRACE_ALL
	printf("Leaving Stras_DomainSimplify\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Polyhedron2Rays
(JNIEnv *env, jclass class, jlong Pol)
 {
#ifdef TRACE_ALL
	printf("Entering Polyhedron2Rays\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in Polyhedron2Rays for parameter Pol");
		goto error;
	}

	struct matrix* res = (struct matrix*) Polyhedron2Rays(Pol_c);


#ifdef TRACE_ALL
	printf("Leaving Polyhedron2Rays\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_PolyhedronIncludes
(JNIEnv *env, jclass class, jlong Pol1, jlong Pol2)
 {
#ifdef TRACE_ALL
	printf("Entering PolyhedronIncludes\n");fflush(stdout);
#endif
	struct polyhedron* Pol1_c = (struct polyhedron*) GECOS_PTRSIZE Pol1; 
	if(((void*)Pol1_c)==NULL) {
		throwException(env, "Null pointer in PolyhedronIncludes for parameter Pol1");
		goto error;
	}
	struct polyhedron* Pol2_c = (struct polyhedron*) GECOS_PTRSIZE Pol2; 
	if(((void*)Pol2_c)==NULL) {
		throwException(env, "Null pointer in PolyhedronIncludes for parameter Pol2");
		goto error;
	}

	int res = (int) PolyhedronIncludes(Pol1_c, Pol2_c);


#ifdef TRACE_ALL
	printf("Leaving PolyhedronIncludes\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Polyhedron_1Copy
(JNIEnv *env, jclass class, jlong Pol)
 {
#ifdef TRACE_ALL
	printf("Entering Polyhedron_Copy\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in Polyhedron_Copy for parameter Pol");
		goto error;
	}

	struct polyhedron* res = (struct polyhedron*) Polyhedron_Copy(Pol_c);


#ifdef TRACE_ALL
	printf("Leaving Polyhedron_Copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Polyhedron_1Free
(JNIEnv *env, jclass class, jlong Pol)
 {
#ifdef TRACE_ALL
	printf("Entering Polyhedron_Free\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in Polyhedron_Free for parameter Pol");
		goto error;
	}

	 Polyhedron_Free(Pol_c);


#ifdef TRACE_ALL
	printf("Leaving Polyhedron_Free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Polyhedron_1Image
(JNIEnv *env, jclass class, jlong Pol, jlong Func, jint NbMaxConstrs)
 {
#ifdef TRACE_ALL
	printf("Entering Polyhedron_Image\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in Polyhedron_Image for parameter Pol");
		goto error;
	}
	struct matrix* Func_c = (struct matrix*) GECOS_PTRSIZE Func; 
	if(((void*)Func_c)==NULL) {
		throwException(env, "Null pointer in Polyhedron_Image for parameter Func");
		goto error;
	}
	int NbMaxConstrs_c = (int) NbMaxConstrs;

	struct polyhedron* res = (struct polyhedron*) Polyhedron_Image(Pol_c, Func_c, NbMaxConstrs_c);


#ifdef TRACE_ALL
	printf("Leaving Polyhedron_Image\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Polyhedron_1Preimage
(JNIEnv *env, jclass class, jlong Pol, jlong Func, jint NbMaxRays)
 {
#ifdef TRACE_ALL
	printf("Entering Polyhedron_Preimage\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in Polyhedron_Preimage for parameter Pol");
		goto error;
	}
	struct matrix* Func_c = (struct matrix*) GECOS_PTRSIZE Func; 
	if(((void*)Func_c)==NULL) {
		throwException(env, "Null pointer in Polyhedron_Preimage for parameter Func");
		goto error;
	}
	int NbMaxRays_c = (int) NbMaxRays;

	struct polyhedron* res = (struct polyhedron*) Polyhedron_Preimage(Pol_c, Func_c, NbMaxRays_c);


#ifdef TRACE_ALL
	printf("Leaving Polyhedron_Preimage\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Polyhedron_1Scan
(JNIEnv *env, jclass class, jlong D, jlong C, jint MAXRAYS)
 {
#ifdef TRACE_ALL
	printf("Entering Polyhedron_Scan\n");fflush(stdout);
#endif
	struct polyhedron* D_c = (struct polyhedron*) GECOS_PTRSIZE D; 
	if(((void*)D_c)==NULL) {
		throwException(env, "Null pointer in Polyhedron_Scan for parameter D");
		goto error;
	}
	struct polyhedron* C_c = (struct polyhedron*) GECOS_PTRSIZE C; 
	if(((void*)C_c)==NULL) {
		throwException(env, "Null pointer in Polyhedron_Scan for parameter C");
		goto error;
	}
	int MAXRAYS_c = (int) MAXRAYS;

	struct polyhedron* res = (struct polyhedron*) Polyhedron_Scan(D_c, C_c, MAXRAYS_c);


#ifdef TRACE_ALL
	printf("Leaving Polyhedron_Scan\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_PolyPrint
(JNIEnv *env, jclass class, jlong Pol)
 {
#ifdef TRACE_ALL
	printf("Entering PolyPrint\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in PolyPrint for parameter Pol");
		goto error;
	}

	 PolyPrint(Pol_c);


#ifdef TRACE_ALL
	printf("Leaving PolyPrint\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Domain_1Free
(JNIEnv *env, jclass class, jlong Pol)
 {
#ifdef TRACE_ALL
	printf("Entering Domain_Free\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in Domain_Free for parameter Pol");
		goto error;
	}

	 Domain_Free(Pol_c);


#ifdef TRACE_ALL
	printf("Leaving Domain_Free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Gauss
(JNIEnv *env, jclass class, jlong Mat, jint NbEq, jint Dimension)
 {
#ifdef TRACE_ALL
	printf("Entering Gauss\n");fflush(stdout);
#endif
	struct matrix* Mat_c = (struct matrix*) GECOS_PTRSIZE Mat; 
	if(((void*)Mat_c)==NULL) {
		throwException(env, "Null pointer in Gauss for parameter Mat");
		goto error;
	}
	int NbEq_c = (int) NbEq;
	int Dimension_c = (int) Dimension;

	int res = (int) Gauss(Mat_c, NbEq_c, Dimension_c);


#ifdef TRACE_ALL
	printf("Leaving Gauss\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_DomainUnion
(JNIEnv *env, jclass class, jlong Pol1, jlong Pol2, jint NbMaxRays)
 {
#ifdef TRACE_ALL
	printf("Entering DomainUnion\n");fflush(stdout);
#endif
	struct polyhedron* Pol1_c = (struct polyhedron*) GECOS_PTRSIZE Pol1; 
	if(((void*)Pol1_c)==NULL) {
		throwException(env, "Null pointer in DomainUnion for parameter Pol1");
		goto error;
	}
	struct polyhedron* Pol2_c = (struct polyhedron*) GECOS_PTRSIZE Pol2; 
	if(((void*)Pol2_c)==NULL) {
		throwException(env, "Null pointer in DomainUnion for parameter Pol2");
		goto error;
	}
	int NbMaxRays_c = (int) NbMaxRays;

	struct polyhedron* res = (struct polyhedron*) DomainUnion(Pol1_c, Pol2_c, NbMaxRays_c);


#ifdef TRACE_ALL
	printf("Leaving DomainUnion\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_Domain_1Copy
(JNIEnv *env, jclass class, jlong Pol)
 {
#ifdef TRACE_ALL
	printf("Entering Domain_Copy\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in Domain_Copy for parameter Pol");
		goto error;
	}

	struct polyhedron* res = (struct polyhedron*) Domain_Copy(Pol_c);


#ifdef TRACE_ALL
	printf("Leaving Domain_Copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_DomainAddConstraints
(JNIEnv *env, jclass class, jlong Pol, jlong Mat, jint NbMaxRays)
 {
#ifdef TRACE_ALL
	printf("Entering DomainAddConstraints\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in DomainAddConstraints for parameter Pol");
		goto error;
	}
	struct matrix* Mat_c = (struct matrix*) GECOS_PTRSIZE Mat; 
	if(((void*)Mat_c)==NULL) {
		throwException(env, "Null pointer in DomainAddConstraints for parameter Mat");
		goto error;
	}
	int NbMaxRays_c = (int) NbMaxRays;

	struct polyhedron* res = (struct polyhedron*) DomainAddConstraints(Pol_c, Mat_c, NbMaxRays_c);


#ifdef TRACE_ALL
	printf("Leaving DomainAddConstraints\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_DomainAddRays
(JNIEnv *env, jclass class, jlong Pol, jlong Ray, jint NbMaxConstrs)
 {
#ifdef TRACE_ALL
	printf("Entering DomainAddRays\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in DomainAddRays for parameter Pol");
		goto error;
	}
	struct matrix* Ray_c = (struct matrix*) GECOS_PTRSIZE Ray; 
	if(((void*)Ray_c)==NULL) {
		throwException(env, "Null pointer in DomainAddRays for parameter Ray");
		goto error;
	}
	int NbMaxConstrs_c = (int) NbMaxConstrs;

	struct polyhedron* res = (struct polyhedron*) DomainAddRays(Pol_c, Ray_c, NbMaxConstrs_c);


#ifdef TRACE_ALL
	printf("Leaving DomainAddRays\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_polylib_PolylibNative_DomainConvex
(JNIEnv *env, jclass class, jlong Pol, jint NbMaxConstrs)
 {
#ifdef TRACE_ALL
	printf("Entering DomainConvex\n");fflush(stdout);
#endif
	struct polyhedron* Pol_c = (struct polyhedron*) GECOS_PTRSIZE Pol; 
	if(((void*)Pol_c)==NULL) {
		throwException(env, "Null pointer in DomainConvex for parameter Pol");
		goto error;
	}
	int NbMaxConstrs_c = (int) NbMaxConstrs;

	struct polyhedron* res = (struct polyhedron*) DomainConvex(Pol_c, NbMaxConstrs_c);


#ifdef TRACE_ALL
	printf("Leaving DomainConvex\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}


