#PROTECTED REGION ID(libraries_Macosx_64) ENABLED START#
      PolyLib_INCDIR= $(HOME)/complibs/usr/local/include/
      PolyLib_LIBDIR= $(HOME)/complibs/usr/local/lib/
      PolyLib_LIBVERSION= 8

      GMP_INCDIR= $(HOME)/complibs/usr/local/include/
      GMP_LIBVERSION= 10

      ISL_LOCATION=$(HOME)/projects/GeCoS/Tools/gecos-tools-isl/bundles/fr.irisa.cairn.jnimap.isl/
#PROTECTED REGION END#

GMP_LIBDIR=$(HOME)/complibs/usr/local/lib
