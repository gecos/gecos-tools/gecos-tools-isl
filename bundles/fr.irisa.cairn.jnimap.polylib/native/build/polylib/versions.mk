METHOD=GIT
VERSION=5.22.5

# isl git repo name
GITREP=polylib

### if METHOD = GIT, clone from GITADDR @head or @ CHECKOUT if specified
GITADDR=git://repo.or.cz/polylib.git
CHECKOUT=8d610e28e0d8b11dd1c887ea7cc355430b3fad87

### else, download archive (FILE) from URL and unzip  
DLDIR=$(GITREP)$(VERSION)
FILE=$(DLDIR).tgz
URL=http://www.irisa.fr/polylib/$(FILE)

ifeq ($(METHOD),GIT)
	FILE = 
	DIR=$(GITREP)
else
	DIR=$(DLDIR)
endif


