###############
##  PolyLib  ##
###############
ifeq ($(LIB),polylib)
CFG_OPTIONS= \
--host=$(HOST) \
--prefix=$(PREFIX_BASE) \
--with-pic \
--disable-static \
--enable-shared \
--with-libgmp=$(PREFIX_BASE)
endif
