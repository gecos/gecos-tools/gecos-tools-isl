#PROTECTED REGION ID(libraries_Linux_64) ENABLED START#
      PolyLib_INCDIR= $(HOME)/complibs/usr/local/include/
      PolyLib_LIBDIR= $(HOME)/complibs/usr/local/lib/

      GMP_INCDIR= $(HOME)/complibs/usr/local/include/

      ISL_LOCATION=$(HOME)/projects/GeCoS/Tools/gecos-tools-isl/bundles//fr.irisa.cairn.jnimap.isl/
#PROTECTED REGION END#

GMP_LIBDIR=${ISL_LOCATION}/lib/ISL_linux_64/
