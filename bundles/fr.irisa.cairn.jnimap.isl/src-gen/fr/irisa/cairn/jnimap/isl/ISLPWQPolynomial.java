package fr.irisa.cairn.jnimap.isl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.irisa.cairn.jnimap.runtime.JNIObject;


/*** PROTECTED REGION ID(ISLPWQPolynomial_userImport) ENABLED START ***/
/*** PROTECTED REGION END ***/

/**
 *  Automatically generated by jnimap 
 * @generated
 */
public class ISLPWQPolynomial extends JNIObject  {
	
	/* @generated */
	protected ISLPWQPolynomial(long ptr) {
		/*** PROTECTED REGION ID(ISLPWQPolynomial_Constructor) DISABLED START ***/
		super(ptr);
		/*** PROTECTED REGION END ***/
	}
	
	/* @generated */
	protected static ISLPWQPolynomial build(long ptr) {
		return new ISLPWQPolynomial(ptr);
	}
	
	/*** PROTECTED REGION ID(ISLPWQPolynomial_userCode) ENABLED START ***/
	public ISLSet getDomain() {
		return copy().domain();
	}
	
	public ISLPWQPolynomialFold bound(ISLFold type) {
		JNIPtrBoolean tight = new JNIPtrBoolean();
		return bound(type, tight);
	}

	public List<ISLQPolynomialPiece> getPieces() {
		int n = this.getNbPieces();
		List<ISLQPolynomialPiece> res = new ArrayList<ISLQPolynomialPiece>(
				n);
		for (int i = 0; i < n; i++) {
			res.add(this.getPieceAt(i));
		}
		return res;
	}

	public List<ISLQPolynomialPiece> getLiftedPieces() {
		int n = this.getNbLiftedPieces();
		List<ISLQPolynomialPiece> res = new ArrayList<ISLQPolynomialPiece>(
				n);
		for (int i = 0; i < n; i++) {
			res.add(this.getLiftedPieceAt(i));
		}
		return res;
	}

	
	/**
	 * Generates a String to be paste in the libreoffice Calc Spreadsheet
	 * @return
	 */
	public String toTabString() {
		String ifKey = "SI";
		String andKey = "ET";
		String orKey = "OU";
		String floorKey = "ARRONDI.INF";
		StringBuffer sb = new StringBuffer();
		for (ISLQPolynomialPiece p : this.getPieces()) {
			ISLSet set = p.getSet();
			String string = set.toString();
			string = string.substring(string.indexOf(":") + 2)
					.replace(" }", "");
			boolean first = true;
			
			String res = "";
			for (String tok : string.split(" and ")) {
				if (first) {
					first = false;
					res = tok;
				} else {
					res = andKey + "(" + res + ";" + tok + ")";
				}
			}
			string = res;
			for (String tok : string.split(" or ")) {
				if (first) {
					first = false;
					res = tok;
				} else {
					res = orKey + "(" + res + ";" + tok + ")";
				}
			}
			string = res;
			ISLQPolynomial qp = p.getQp();
			String string2 = qp.toString();
			string2 = string2.substring(string2.indexOf("{") + 2).replace(" }",
					"");
			while (string2.contains("[")) {
				string2 = string2.replaceFirst("(\\[)", floorKey + "(");
				string2 = string2.replaceFirst("(\\])", ")");
			}
			sb.append(ifKey + "(" + string + ";" + string2 + ";");
		}
		sb.append("0");
		for (int i = 0; i < this.getNbPieces(); i++)
			sb.append(")");

		return sb.toString();
	}
	/*** PROTECTED REGION END ***/

	
	/*************************************** 
	 *	         Static Methods            * 
	 ***************************************/
	/**
	 * isl_pw_qpolynomial_read_from_str 
	 * 
	 * @generated
	**/
	 public static ISLPWQPolynomial buildFromString(ISLContext ctx, String str) { 
		/*** PROTECTED REGION ID(static_isl_pw_qpolynomial_read_from_str) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_read_from_str(getNativePtr(ctx), str));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_zero 
	 * 
	@take dim
	 * @generated
	**/
	 public static ISLPWQPolynomial buildZero(ISLSpace dim) { 
		/*** PROTECTED REGION ID(static_isl_pw_qpolynomial_zero) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(dim);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_zero(getNativePtr(dim)));
			} finally {
				taken(dim);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_alloc 
	 * 
	@take set qp
	 * @generated
	**/
	 public static ISLPWQPolynomial build(ISLSet set, ISLQPolynomial qp) { 
		/*** PROTECTED REGION ID(static_isl_pw_qpolynomial_alloc) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(set, qp);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_alloc(getNativePtr(set), getNativePtr(qp)));
			} finally {
				taken(set);
				taken(qp);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_to_string 
	 * 
	 * @generated
	**/
	 public static String _toString(ISLPWQPolynomial pwqp, int format) { 
		/*** PROTECTED REGION ID(static_isl_pw_qpolynomial_to_string) DISABLED START ***/
		String res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_pw_qpolynomial_to_string(getNativePtr(pwqp), format);
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	
	/*************************************** 
	 *	         Member Methods            * 
	 ***************************************/
	/**
	 * isl_pw_qpolynomial_get_ctx 
	 * 
	 * @generated
	**/
	public ISLContext getContext() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_get_ctx) DISABLED START ***/
		ISLContext res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLContext.build(ISLNative.isl_pw_qpolynomial_get_ctx(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_get_space 
	 * 
	 * @generated
	**/
	public ISLSpace getSpace() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_get_space) DISABLED START ***/
		ISLSpace res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLSpace.build(ISLNative.isl_pw_qpolynomial_get_space(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_get_domain_space 
	 * 
	 * @generated
	**/
	public ISLSpace getDomainSpace() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_get_domain_space) DISABLED START ***/
		ISLSpace res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLSpace.build(ISLNative.isl_pw_qpolynomial_get_domain_space(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_copy 
	 * 
	 * @generated
	**/
	public ISLPWQPolynomial copy() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_copy) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_copy(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_free 
	 * 
	 * @take this
	 * @generated
	**/
	public void free() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_free) DISABLED START ***/
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				ISLNative.isl_pw_qpolynomial_free(getNativePtr(this));
			} finally {
				taken(this);
			}
		}
		
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_plain_is_equal 
	 * 
	 * @generated
	**/
	public boolean isPlainEqual(ISLPWQPolynomial pwqp2) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_plain_is_equal) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_pw_qpolynomial_plain_is_equal(getNativePtr(this), getNativePtr(pwqp2)) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_is_zero 
	 * 
	 * @generated
	**/
	public boolean isZero() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_is_zero) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_pw_qpolynomial_is_zero(getNativePtr(this)) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_reset_domain_space 
	 * 
	 * @take this dim
	 * @generated
	**/
	public ISLPWQPolynomial resetDomainSpace(ISLSpace dim) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_reset_domain_space) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this, dim);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_reset_domain_space(getNativePtr(this), getNativePtr(dim)));
			} finally {
				taken(this);
				taken(dim);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_dim 
	 * 
	 * @generated
	**/
	public int dim(ISLDimType type) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_dim) DISABLED START ***/
		int res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_pw_qpolynomial_dim(getNativePtr(this), type.getValue());
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_involves_dims 
	 * 
	 * @generated
	**/
	public boolean involvesDims(ISLDimType type, int first, int n) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_involves_dims) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_pw_qpolynomial_involves_dims(getNativePtr(this), type.getValue(), first, n) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_has_equal_space 
	 * 
	 * @generated
	**/
	public boolean hasEqualSpace(ISLPWQPolynomial pwqp2) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_has_equal_space) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_pw_qpolynomial_has_equal_space(getNativePtr(this), getNativePtr(pwqp2)) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_set_dim_name 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLPWQPolynomial setDimName(ISLDimType type, int pos, String s) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_set_dim_name) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_set_dim_name(getNativePtr(this), type.getValue(), pos, s));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_domain 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLSet domain() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_domain) DISABLED START ***/
		ISLSet res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLSet.build(ISLNative.isl_pw_qpolynomial_domain(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_intersect_domain 
	 * 
	 * @take this set
	 * @generated
	**/
	public ISLPWQPolynomial intersectDomain(ISLSet set) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_intersect_domain) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this, set);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_intersect_domain(getNativePtr(this), getNativePtr(set)));
			} finally {
				taken(this);
				taken(set);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_intersect_params 
	 * 
	 * @take this set
	 * @generated
	**/
	public ISLPWQPolynomial intersectParams(ISLSet set) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_intersect_params) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this, set);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_intersect_params(getNativePtr(this), getNativePtr(set)));
			} finally {
				taken(this);
				taken(set);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_project_domain_on_params 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLPWQPolynomial projectDomainOnParams() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_project_domain_on_params) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_project_domain_on_params(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_drop_dims 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLPWQPolynomial dropDims(ISLDimType type, int first, int n) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_drop_dims) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_drop_dims(getNativePtr(this), type.getValue(), first, n));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_split_dims 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLPWQPolynomial splitDims(ISLDimType type, int first, int n) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_split_dims) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_split_dims(getNativePtr(this), type.getValue(), first, n));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_intersect_domain_wrapped_domain 
	 * 
	 * @take this set
	 * @generated
	**/
	public ISLPWQPolynomial intersectDomainWrappedDomain(ISLSet set) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_intersect_domain_wrapped_domain) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this, set);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_intersect_domain_wrapped_domain(getNativePtr(this), getNativePtr(set)));
			} finally {
				taken(this);
				taken(set);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_intersect_domain_wrapped_range 
	 * 
	 * @take this set
	 * @generated
	**/
	public ISLPWQPolynomial intersectDomainWrappedRange(ISLSet set) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_intersect_domain_wrapped_range) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this, set);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_intersect_domain_wrapped_range(getNativePtr(this), getNativePtr(set)));
			} finally {
				taken(this);
				taken(set);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_add 
	 * 
	 * @take this pwqp2
	 * @generated
	**/
	public ISLPWQPolynomial add(ISLPWQPolynomial pwqp2) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_add) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this, pwqp2);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_add(getNativePtr(this), getNativePtr(pwqp2)));
			} finally {
				taken(this);
				taken(pwqp2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_sub 
	 * 
	 * @take this pwqp2
	 * @generated
	**/
	public ISLPWQPolynomial sub(ISLPWQPolynomial pwqp2) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_sub) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this, pwqp2);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_sub(getNativePtr(this), getNativePtr(pwqp2)));
			} finally {
				taken(this);
				taken(pwqp2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_add_disjoint 
	 * 
	 * @take this pwqp2
	 * @generated
	**/
	public ISLPWQPolynomial addDisjoint(ISLPWQPolynomial pwqp2) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_add_disjoint) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this, pwqp2);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_add_disjoint(getNativePtr(this), getNativePtr(pwqp2)));
			} finally {
				taken(this);
				taken(pwqp2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_neg 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLPWQPolynomial neg() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_neg) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_neg(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_mul 
	 * 
	 * @take this pwqp2
	 * @generated
	**/
	public ISLPWQPolynomial mul(ISLPWQPolynomial pwqp2) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_mul) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this, pwqp2);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_mul(getNativePtr(this), getNativePtr(pwqp2)));
			} finally {
				taken(this);
				taken(pwqp2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_scale_val 
	 * 
	 * @take this v
	 * @generated
	**/
	public ISLPWQPolynomial scale(ISLVal v) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_scale_val) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this, v);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_scale_val(getNativePtr(this), getNativePtr(v)));
			} finally {
				taken(this);
				taken(v);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_pow 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLPWQPolynomial pow(int exponent) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_pow) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_pow(getNativePtr(this), exponent));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_insert_dims 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLPWQPolynomial insertDims(ISLDimType type, int first, int n) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_insert_dims) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_insert_dims(getNativePtr(this), type.getValue(), first, n));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_add_dims 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLPWQPolynomial addDims(ISLDimType type, int n) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_add_dims) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_add_dims(getNativePtr(this), type.getValue(), n));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_move_dims 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLPWQPolynomial moveDims(ISLDimType dst_type, int dst_pos, ISLDimType src_type, int src_pos, int n) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_move_dims) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_move_dims(getNativePtr(this), dst_type.getValue(), dst_pos, src_type.getValue(), src_pos, n));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_eval 
	 * 
	 * @take this pnt
	 * @generated
	**/
	public ISLVal eval(ISLPoint pnt) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_eval) DISABLED START ***/
		ISLVal res;
		
		synchronized(LOCK) {
			checkParameters(this, pnt);
			try {
				res = ISLVal.build(ISLNative.isl_pw_qpolynomial_eval(getNativePtr(this), getNativePtr(pnt)));
			} finally {
				taken(this);
				taken(pnt);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_max 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLVal max() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_max) DISABLED START ***/
		ISLVal res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLVal.build(ISLNative.isl_pw_qpolynomial_max(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_min 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLVal min() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_min) DISABLED START ***/
		ISLVal res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLVal.build(ISLNative.isl_pw_qpolynomial_min(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_bound 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLPWQPolynomialFold bound(ISLFold type, JNIPtrBoolean tight) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_bound) DISABLED START ***/
		ISLPWQPolynomialFold res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLPWQPolynomialFold.build(ISLNative.isl_pw_qpolynomial_bound(getNativePtr(this), type.getValue(), tight));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_coalesce 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLPWQPolynomial coalesce() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_coalesce) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_coalesce(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_gist 
	 * 
	 * @take this context
	 * @generated
	**/
	public ISLPWQPolynomial gist(ISLSet context) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_gist) DISABLED START ***/
		ISLPWQPolynomial res;
		
		synchronized(LOCK) {
			checkParameters(this, context);
			try {
				res = ISLPWQPolynomial.build(ISLNative.isl_pw_qpolynomial_gist(getNativePtr(this), getNativePtr(context)));
			} finally {
				taken(this);
				taken(context);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_numberof_piece 
	 * 
	 * @generated
	**/
	public int getNbPieces() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_numberof_piece) DISABLED START ***/
		int res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_pw_qpolynomial_numberof_piece(getNativePtr(this));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_get_piece_at 
	 * 
	 * @generated
	**/
	public ISLQPolynomialPiece getPieceAt(int pos) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_get_piece_at) DISABLED START ***/
		ISLQPolynomialPiece res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLQPolynomialPiece.build(ISLNative.isl_pw_qpolynomial_get_piece_at(getNativePtr(this), pos));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_numberof_lifted_piece 
	 * 
	 * @generated
	**/
	public int getNbLiftedPieces() {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_numberof_lifted_piece) DISABLED START ***/
		int res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_pw_qpolynomial_numberof_lifted_piece(getNativePtr(this));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_pw_qpolynomial_get_lifted_piece_at 
	 * 
	 * @generated
	**/
	public ISLQPolynomialPiece getLiftedPieceAt(int pos) {
		/*** PROTECTED REGION ID(isl_pw_qpolynomial_get_lifted_piece_at) DISABLED START ***/
		ISLQPolynomialPiece res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLQPolynomialPiece.build(ISLNative.isl_pw_qpolynomial_get_lifted_piece_at(getNativePtr(this), pos));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	
	public String toString() {
		/*** PROTECTED REGION ID(ISLPWQPolynomial_toString) DISABLED START ***/
			return ISLPrettyPrinter.asString(this);
		/*** PROTECTED REGION END ***/
	}
}
