package fr.irisa.cairn.jnimap.isl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.irisa.cairn.jnimap.runtime.JNIObject;


/*** PROTECTED REGION ID(ISLASTUnscannedNode_userImport) ENABLED START ***/
	/*
		Put you custom import here ....
	*/
/*** PROTECTED REGION END ***/

/**
 *  Automatically generated by jnimap 
 * @generated
 */
public class ISLASTUnscannedNode extends ISLASTNode  {
	
	/* @generated */
	protected ISLASTUnscannedNode(long ptr) {
		/*** PROTECTED REGION ID(ISLASTUnscannedNode_Constructor) DISABLED START ***/
		super(ptr);
		/*** PROTECTED REGION END ***/
	}
	
	/* @generated */
	protected static ISLASTUnscannedNode build(long ptr) {
		return new ISLASTUnscannedNode(ptr);
	}
	
	/*** PROTECTED REGION ID(ISLASTUnscannedNode_userCode) ENABLED START ***/
	//Same methods as ASTNodeUser; putting this in the jnimap file creates name crash due to having the same function twice
	public ISLASTExpression getExpression() {
		ISLASTExpression res;
		synchronized (LOCK) {
			res = ISLASTExpression.build(ISLNative.isl_ast_node_user_get_expr(getNativePtr(this)));
		}
		return res;
	}
	/*** PROTECTED REGION END ***/

	
	/*************************************** 
	 *	         Static Methods            * 
	 ***************************************/
	
	/*************************************** 
	 *	         Member Methods            * 
	 ***************************************/
	/**
	 * isl_ast_node_user_get_schedule 
	 * 
	 * @generated
	**/
	public ISLUnionMap getSchedule() {
		/*** PROTECTED REGION ID(isl_ast_node_user_get_schedule) DISABLED START ***/
		ISLUnionMap res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLUnionMap.build(ISLNative.isl_ast_node_user_get_schedule(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	
	public void accept(IISLASTNodeVisitor visitor) {
		visitor.visitISLASTUnscannedNode(this);
	}
	
	public String toString() {
		/*** PROTECTED REGION ID(ISLASTUnscannedNode_toString) DISABLED START ***/
			return ISLPrettyPrinter.asString(this);
		/*** PROTECTED REGION END ***/
	}
}
