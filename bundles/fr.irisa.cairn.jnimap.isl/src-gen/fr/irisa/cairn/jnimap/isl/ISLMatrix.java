package fr.irisa.cairn.jnimap.isl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.irisa.cairn.jnimap.runtime.JNIObject;


/*** PROTECTED REGION ID(ISLMatrix_userImport) ENABLED START ***/
	/*
		Put you custom import here ....
	*/
/*** PROTECTED REGION END ***/

/**
 *  Automatically generated by jnimap 
 * @generated
 */
public class ISLMatrix extends JNIObject  {
	
	/* @generated */
	protected ISLMatrix(long ptr) {
		/*** PROTECTED REGION ID(ISLMatrix_Constructor) DISABLED START ***/
		super(ptr);
		/*** PROTECTED REGION END ***/
	}
	
	/* @generated */
	protected static ISLMatrix build(long ptr) {
		return new ISLMatrix(ptr);
	}
	
	/*** PROTECTED REGION ID(ISLMatrix_userCode) ENABLED START ***/
	public long getElement(int row, int col) {
		return getElementVal(row, col).asLong();
	}
	public ISLMatrix setElement(int row, int col, long val) {
		return setElement(row, col, ISLVal.buildFromLong(getContext(), val));
	}
	
	public static ISLMatrix buildFromLongMatrix(long[][] mat) {
		if (mat == null || mat.length == 0 || mat[0].length == 0)
			throw new IllegalArgumentException();
		
		final int nbRows = mat.length;
		final int nbCols = mat[0].length;
		ISLMatrix islMat = ISLMatrix.build(ISLContext.getInstance(), nbRows, nbCols);
		
		for (int r = 0; r < nbRows; r++)
			for (int c = 0; c < nbCols; c++)
				islMat = islMat.setElement(r, c, mat[r][c]);
		
		return islMat;
	}
	
	public long[][] toLongMatrix() {
		final int nbRows = getNbRows();
		final int nbCols = getNbCols();
		long[][] mat = new long[nbRows][nbCols];
		
		for (int r = 0; r < nbRows; r++)
			for (int c = 0; c < nbCols; c++)
				mat[r][c] = getElement(r, c);
		
		return mat;
	}
	/*** PROTECTED REGION END ***/

	
	/*************************************** 
	 *	         Static Methods            * 
	 ***************************************/
	/**
	 * isl_mat_alloc 
	 * 
	 * @generated
	**/
	 public static ISLMatrix build(ISLContext ctx, int n_row, int n_col) { 
		/*** PROTECTED REGION ID(static_isl_mat_alloc) DISABLED START ***/
		ISLMatrix res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLMatrix.build(ISLNative.isl_mat_alloc(getNativePtr(ctx), n_row, n_col));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	
	/*************************************** 
	 *	         Member Methods            * 
	 ***************************************/
	/**
	 * isl_mat_get_ctx 
	 * 
	 * @generated
	**/
	public ISLContext getContext() {
		/*** PROTECTED REGION ID(isl_mat_get_ctx) DISABLED START ***/
		ISLContext res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLContext.build(ISLNative.isl_mat_get_ctx(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_copy 
	 * 
	 * @generated
	**/
	public ISLMatrix copy() {
		/*** PROTECTED REGION ID(isl_mat_copy) DISABLED START ***/
		ISLMatrix res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLMatrix.build(ISLNative.isl_mat_copy(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_free 
	 * 
	 * @take this
	 * @generated
	**/
	public void free() {
		/*** PROTECTED REGION ID(isl_mat_free) DISABLED START ***/
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				ISLNative.isl_mat_free(getNativePtr(this));
			} finally {
				taken(this);
			}
		}
		
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_rows 
	 * 
	 * @generated
	**/
	public int getNbRows() {
		/*** PROTECTED REGION ID(isl_mat_rows) DISABLED START ***/
		int res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_mat_rows(getNativePtr(this));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_cols 
	 * 
	 * @generated
	**/
	public int getNbCols() {
		/*** PROTECTED REGION ID(isl_mat_cols) DISABLED START ***/
		int res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_mat_cols(getNativePtr(this));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_get_element_val 
	 * 
	 * @generated
	**/
	public ISLVal getElementVal(int row, int col) {
		/*** PROTECTED REGION ID(isl_mat_get_element_val) DISABLED START ***/
		ISLVal res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLVal.build(ISLNative.isl_mat_get_element_val(getNativePtr(this), row, col));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_set_element_si 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMatrix setElement(int row, int col, int v) {
		/*** PROTECTED REGION ID(isl_mat_set_element_si) DISABLED START ***/
		ISLMatrix res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMatrix.build(ISLNative.isl_mat_set_element_si(getNativePtr(this), row, col, v));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_set_element_val 
	 * 
	 * @take this v
	 * @generated
	**/
	public ISLMatrix setElement(int row, int col, ISLVal v) {
		/*** PROTECTED REGION ID(isl_mat_set_element_val) DISABLED START ***/
		ISLMatrix res;
		
		synchronized(LOCK) {
			checkParameters(this, v);
			try {
				res = ISLMatrix.build(ISLNative.isl_mat_set_element_val(getNativePtr(this), row, col, getNativePtr(v)));
			} finally {
				taken(this);
				taken(v);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_rank 
	 * 
	 * @generated
	**/
	public int rank() {
		/*** PROTECTED REGION ID(isl_mat_rank) DISABLED START ***/
		int res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_mat_rank(getNativePtr(this));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_right_inverse 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMatrix rightInverse() {
		/*** PROTECTED REGION ID(isl_mat_right_inverse) DISABLED START ***/
		ISLMatrix res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMatrix.build(ISLNative.isl_mat_right_inverse(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_right_kernel 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMatrix rightKernel() {
		/*** PROTECTED REGION ID(isl_mat_right_kernel) DISABLED START ***/
		ISLMatrix res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMatrix.build(ISLNative.isl_mat_right_kernel(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_row_basis 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMatrix rowBasis() {
		/*** PROTECTED REGION ID(isl_mat_row_basis) DISABLED START ***/
		ISLMatrix res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMatrix.build(ISLNative.isl_mat_row_basis(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_row_basis_extension 
	 * 
	 * @take this mat2
	 * @generated
	**/
	public ISLMatrix rowBasisExtension(ISLMatrix mat2) {
		/*** PROTECTED REGION ID(isl_mat_row_basis_extension) DISABLED START ***/
		ISLMatrix res;
		
		synchronized(LOCK) {
			checkParameters(this, mat2);
			try {
				res = ISLMatrix.build(ISLNative.isl_mat_row_basis_extension(getNativePtr(this), getNativePtr(mat2)));
			} finally {
				taken(this);
				taken(mat2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_has_linearly_independent_rows 
	 * 
	 * @generated
	**/
	public boolean hasLinearlyIndependentRows(ISLMatrix mat2) {
		/*** PROTECTED REGION ID(isl_mat_has_linearly_independent_rows) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_mat_has_linearly_independent_rows(getNativePtr(this), getNativePtr(mat2)) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_concat 
	 * 
	 * @take this bot
	 * @generated
	**/
	public ISLMatrix concat(ISLMatrix bot) {
		/*** PROTECTED REGION ID(isl_mat_concat) DISABLED START ***/
		ISLMatrix res;
		
		synchronized(LOCK) {
			checkParameters(this, bot);
			try {
				res = ISLMatrix.build(ISLNative.isl_mat_concat(getNativePtr(this), getNativePtr(bot)));
			} finally {
				taken(this);
				taken(bot);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_drop_rows 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMatrix dropRows(int row, int n) {
		/*** PROTECTED REGION ID(isl_mat_drop_rows) DISABLED START ***/
		ISLMatrix res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMatrix.build(ISLNative.isl_mat_drop_rows(getNativePtr(this), row, n));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_mat_drop_cols 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMatrix dropCols(int col, int n) {
		/*** PROTECTED REGION ID(isl_mat_drop_cols) DISABLED START ***/
		ISLMatrix res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMatrix.build(ISLNative.isl_mat_drop_cols(getNativePtr(this), col, n));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * custom_isl_left_hermite 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLHermiteResult leftHermite() {
		/*** PROTECTED REGION ID(custom_isl_left_hermite) DISABLED START ***/
		ISLHermiteResult res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLHermiteResult.build(ISLNative.custom_isl_left_hermite(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * custom_isl_left_hermite_neg 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLHermiteResult leftHermiteNeg(int neg) {
		/*** PROTECTED REGION ID(custom_isl_left_hermite_neg) DISABLED START ***/
		ISLHermiteResult res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLHermiteResult.build(ISLNative.custom_isl_left_hermite_neg(getNativePtr(this), neg));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	
	public String toString() {
		/*** PROTECTED REGION ID(ISLMatrix_toString) DISABLED START ***/
			return ISLPrettyPrinter.asString(this);
		/*** PROTECTED REGION END ***/
	}
}
