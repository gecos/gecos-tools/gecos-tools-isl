package fr.irisa.cairn.jnimap.isl;

public class ISLException extends Exception {
	private static final long serialVersionUID = 1L;
	public ISLException(String msg) {
		super(msg);
	} 
}
