package fr.irisa.cairn.jnimap.isl;

/**
 *  Automatically generated by jnimap 
 * @generated
 */
public interface IISLScheduleNodeVisitor {
	void visitISLScheduleNode(ISLScheduleNode obj);
	void visitISLScheduleBandNode(ISLScheduleBandNode obj);
	void visitISLScheduleContextNode(ISLScheduleContextNode obj);
	void visitISLScheduleDomainNode(ISLScheduleDomainNode obj);
	void visitISLScheduleExpansionNode(ISLScheduleExpansionNode obj);
	void visitISLScheduleExtensionNode(ISLScheduleExtensionNode obj);
	void visitISLScheduleFilterNode(ISLScheduleFilterNode obj);
	void visitISLScheduleGuardNode(ISLScheduleGuardNode obj);
	void visitISLScheduleLeafNode(ISLScheduleLeafNode obj);
	void visitISLScheduleMarkNode(ISLScheduleMarkNode obj);
	void visitISLScheduleSequenceNode(ISLScheduleSequenceNode obj);
	void visitISLScheduleSetNode(ISLScheduleSetNode obj);
}
