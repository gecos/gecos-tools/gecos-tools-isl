package fr.irisa.cairn.jnimap.isl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.irisa.cairn.jnimap.runtime.JNIObject;


/*** PROTECTED REGION ID(IISLSingleSpaceReadOnlyMethods_userImport) ENABLED START ***/
	/*
		Put you custom import here ....
	*/
/*** PROTECTED REGION END ***/

/**
 *  Automatically generated by jnimap 
 * @generated
 */
public interface IISLSingleSpaceReadOnlyMethods  extends IISLStandardMethods {
	
	
	/*** PROTECTED REGION ID(IISLSingleSpaceReadOnlyMethods_userCode) ENABLED START ***/
	public String getDimName(ISLDimType dimType, int pos);
	
	public default List<String> getDimNames(ISLDimType dim) {
		List<String> res = new ArrayList<String>();
		for (int i=0;i<this.dim(dim);i++) {
			String name = this.getDimName(dim, i);
			if (name == null) return null;
			res.add(name);
		}
		return res;
	}

	public default List<String> getParamNames() {
		return getDimNames(ISLDimType.isl_dim_param);
	}

	public default String getParamName(int i) {
		return getDimName(ISLDimType.isl_dim_param, i);
	}
	/*** PROTECTED REGION END ***/

	
	/*************************************** 
	 *	         Static Methods            * 
	 ***************************************/
	
	
}
