package fr.irisa.cairn.jnimap.isl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.irisa.cairn.jnimap.runtime.JNIObject;


/*** PROTECTED REGION ID(ISLMultiPWAff_userImport) ENABLED START ***/
	/*
		Put you custom import here ....
	*/
/*** PROTECTED REGION END ***/

/**
 *  Automatically generated by jnimap 
 * @generated
 */
public class ISLMultiPWAff extends JNIObject implements IISLStandardMapMethods {
	
	/* @generated */
	protected ISLMultiPWAff(long ptr) {
		/*** PROTECTED REGION ID(ISLMultiPWAff_Constructor) DISABLED START ***/
		super(ptr);
		/*** PROTECTED REGION END ***/
	}
	
	/* @generated */
	protected static ISLMultiPWAff build(long ptr) {
		return new ISLMultiPWAff(ptr);
	}
	
	/*** PROTECTED REGION ID(ISLMultiPWAff_userCode) ENABLED START ***/
	public String toString(ISL_FORMAT format) {
		return ISLPrettyPrinter.asString(this, format);
	}
	/*** PROTECTED REGION END ***/

	
	/*************************************** 
	 *	         Static Methods            * 
	 ***************************************/
	/**
	 * isl_multi_pw_aff_read_from_str 
	 * 
	 * @generated
	**/
	 public static ISLMultiPWAff buildFromString(ISLContext ctx, String str) { 
		/*** PROTECTED REGION ID(static_isl_multi_pw_aff_read_from_str) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_read_from_str(getNativePtr(ctx), str));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_zero 
	 * 
	@take space
	 * @generated
	**/
	 public static ISLMultiPWAff buildZero(ISLSpace space) { 
		/*** PROTECTED REGION ID(static_isl_multi_pw_aff_zero) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(space);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_zero(getNativePtr(space)));
			} finally {
				taken(space);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_identity 
	 * 
	@take space
	 * @generated
	**/
	 public static ISLMultiPWAff buildIdentity(ISLSpace space) { 
		/*** PROTECTED REGION ID(static_isl_multi_pw_aff_identity) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(space);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_identity(getNativePtr(space)));
			} finally {
				taken(space);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_identity_on_domain_space 
	 * 
	@take space
	 * @generated
	**/
	 public static ISLMultiPWAff buildIdentityOnDomainSpace(ISLSpace space) { 
		/*** PROTECTED REGION ID(static_isl_multi_pw_aff_identity_on_domain_space) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(space);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_identity_on_domain_space(getNativePtr(space)));
			} finally {
				taken(space);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_identity_multi_pw_aff 
	 * 
	@take mpa
	 * @generated
	**/
	 public static ISLMultiPWAff buildIdentity(ISLMultiPWAff mpa) { 
		/*** PROTECTED REGION ID(static_isl_multi_pw_aff_identity_multi_pw_aff) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(mpa);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_identity_multi_pw_aff(getNativePtr(mpa)));
			} finally {
				taken(mpa);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_from_pw_aff_list 
	 * 
	@take space list
	 * @generated
	**/
	 public static ISLMultiPWAff buildFromPWAffList(ISLSpace space, ISLPWAffList list) { 
		/*** PROTECTED REGION ID(static_isl_multi_pw_aff_from_pw_aff_list) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(space, list);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_from_pw_aff_list(getNativePtr(space), getNativePtr(list)));
			} finally {
				taken(space);
				taken(list);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_to_string 
	 * 
	 * @generated
	**/
	 public static String _toString(ISLMultiPWAff mpa, int format) { 
		/*** PROTECTED REGION ID(static_isl_multi_pw_aff_to_string) DISABLED START ***/
		String res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_to_string(getNativePtr(mpa), format);
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	
	/*************************************** 
	 *	         Member Methods            * 
	 ***************************************/
	/**
	 * isl_pw_multi_aff_from_multi_pw_aff 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLPWMultiAff toPWMultiAff() {
		/*** PROTECTED REGION ID(isl_pw_multi_aff_from_multi_pw_aff) DISABLED START ***/
		ISLPWMultiAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLPWMultiAff.build(ISLNative.isl_pw_multi_aff_from_multi_pw_aff(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_set_from_multi_pw_aff 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLSet toSet() {
		/*** PROTECTED REGION ID(isl_set_from_multi_pw_aff) DISABLED START ***/
		ISLSet res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLSet.build(ISLNative.isl_set_from_multi_pw_aff(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_map_from_multi_pw_aff 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMap toMap() {
		/*** PROTECTED REGION ID(isl_map_from_multi_pw_aff) DISABLED START ***/
		ISLMap res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMap.build(ISLNative.isl_map_from_multi_pw_aff(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_union_pw_aff_from_multi_pw_aff 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiUnionPWAff toMultiUnionPWAff() {
		/*** PROTECTED REGION ID(isl_multi_union_pw_aff_from_multi_pw_aff) DISABLED START ***/
		ISLMultiUnionPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiUnionPWAff.build(ISLNative.isl_multi_union_pw_aff_from_multi_pw_aff(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_from_range 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff fromRange() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_from_range) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_from_range(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_get_ctx 
	 * 
	 * @generated
	**/
	public ISLContext getContext() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_get_ctx) DISABLED START ***/
		ISLContext res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLContext.build(ISLNative.isl_multi_pw_aff_get_ctx(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_get_space 
	 * 
	 * @generated
	**/
	public ISLSpace getSpace() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_get_space) DISABLED START ***/
		ISLSpace res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLSpace.build(ISLNative.isl_multi_pw_aff_get_space(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_get_domain_space 
	 * 
	 * @generated
	**/
	public ISLSpace getDomainSpace() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_get_domain_space) DISABLED START ***/
		ISLSpace res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLSpace.build(ISLNative.isl_multi_pw_aff_get_domain_space(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_copy 
	 * 
	 * @generated
	**/
	public ISLMultiPWAff copy() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_copy) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_copy(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_free 
	 * 
	 * @take this
	 * @generated
	**/
	public void free() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_free) DISABLED START ***/
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				ISLNative.isl_multi_pw_aff_free(getNativePtr(this));
			} finally {
				taken(this);
			}
		}
		
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_size 
	 * 
	 * @generated
	**/
	public int size() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_size) DISABLED START ***/
		int res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_size(getNativePtr(this));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_get_at 
	 * 
	 * @generated
	**/
	public ISLPWAff getAt(int pos) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_get_at) DISABLED START ***/
		ISLPWAff res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLPWAff.build(ISLNative.isl_multi_pw_aff_get_at(getNativePtr(this), pos));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_set_at 
	 * 
	 * @take this pa
	 * @generated
	**/
	public ISLMultiPWAff setAt(int pos, ISLPWAff pa) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_set_at) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, pa);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_set_at(getNativePtr(this), pos, getNativePtr(pa)));
			} finally {
				taken(this);
				taken(pa);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_dim 
	 * 
	 * @generated
	**/
	public int dim(ISLDimType type) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_dim) DISABLED START ***/
		int res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_dim(getNativePtr(this), type.getValue());
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_set_dim_id 
	 * 
	 * @take this id
	 * @generated
	**/
	public ISLMultiPWAff setDimID(ISLDimType type, int pos, ISLIdentifier id) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_set_dim_id) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, id);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_set_dim_id(getNativePtr(this), type.getValue(), pos, getNativePtr(id)));
			} finally {
				taken(this);
				taken(id);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_get_dim_id 
	 * 
	 * @generated
	**/
	public ISLIdentifier getDimID(ISLDimType type, int pos) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_get_dim_id) DISABLED START ***/
		ISLIdentifier res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLIdentifier.build(ISLNative.isl_multi_pw_aff_get_dim_id(getNativePtr(this), type.getValue(), pos));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_set_dim_name 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff setDimName(ISLDimType type, int pos, String s) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_set_dim_name) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_set_dim_name(getNativePtr(this), type.getValue(), pos, s));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_find_dim_by_id 
	 * 
	 * @generated
	**/
	public int findDimByID(ISLDimType type, ISLIdentifier id) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_find_dim_by_id) DISABLED START ***/
		int res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_find_dim_by_id(getNativePtr(this), type.getValue(), getNativePtr(id));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_find_dim_by_name 
	 * 
	 * @generated
	**/
	public int findDimByName(ISLDimType type, String name) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_find_dim_by_name) DISABLED START ***/
		int res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_find_dim_by_name(getNativePtr(this), type.getValue(), name);
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_reset_tuple_id 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff resetTupleID(ISLDimType type) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_reset_tuple_id) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_reset_tuple_id(getNativePtr(this), type.getValue()));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_has_tuple_id 
	 * 
	 * @generated
	**/
	public boolean hasTupleID(ISLDimType type) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_has_tuple_id) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_has_tuple_id(getNativePtr(this), type.getValue()) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_get_tuple_id 
	 * 
	 * @generated
	**/
	public ISLIdentifier getTupleID(ISLDimType type) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_get_tuple_id) DISABLED START ***/
		ISLIdentifier res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLIdentifier.build(ISLNative.isl_multi_pw_aff_get_tuple_id(getNativePtr(this), type.getValue()));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_set_tuple_name 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff setTupleName(ISLDimType type, String s) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_set_tuple_name) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_set_tuple_name(getNativePtr(this), type.getValue(), s));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_involves_dims 
	 * 
	 * @generated
	**/
	public boolean involvesDims(ISLDimType type, int first, int n) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_involves_dims) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_involves_dims(getNativePtr(this), type.getValue(), first, n) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_involves_param_id 
	 * 
	 * @generated
	**/
	public boolean involvesParamID(ISLIdentifier id) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_involves_param_id) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_involves_param_id(getNativePtr(this), getNativePtr(id)) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_involves_param_id_list 
	 * 
	 * @generated
	**/
	public boolean invovlvesParamIDList(ISLIdentifierList list) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_involves_param_id_list) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_involves_param_id_list(getNativePtr(this), getNativePtr(list)) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_range_is_wrapping 
	 * 
	 * @generated
	**/
	public boolean rangeIsWrapping() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_range_is_wrapping) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_range_is_wrapping(getNativePtr(this)) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_is_cst 
	 * 
	 * @generated
	**/
	public boolean isConstant() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_is_cst) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_is_cst(getNativePtr(this)) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_involves_nan 
	 * 
	 * @generated
	**/
	public boolean involvesNaN() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_involves_nan) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_involves_nan(getNativePtr(this)) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_plain_is_equal 
	 * 
	 * @generated
	**/
	public boolean isPlainEqual(ISLMultiPWAff mpa2) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_plain_is_equal) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_plain_is_equal(getNativePtr(this), getNativePtr(mpa2)) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_is_equal 
	 * 
	 * @generated
	**/
	public boolean isEqual(ISLMultiPWAff mpa2) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_is_equal) DISABLED START ***/
		boolean res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_multi_pw_aff_is_equal(getNativePtr(this), getNativePtr(mpa2)) != 0;
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_project_domain_on_params 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff projectDomainOnParams() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_project_domain_on_params) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_project_domain_on_params(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_domain 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLSet domain() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_domain) DISABLED START ***/
		ISLSet res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLSet.build(ISLNative.isl_multi_pw_aff_domain(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_coalesce 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff coalesce() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_coalesce) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_coalesce(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_flatten_range 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff flattenRange() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_flatten_range) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_flatten_range(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_neg 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff neg() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_neg) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_neg(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_insert_dims 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff insertDims(ISLDimType type, int first, int n) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_insert_dims) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_insert_dims(getNativePtr(this), type.getValue(), first, n));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_add_dims 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff addDims(ISLDimType type, int n) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_add_dims) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_add_dims(getNativePtr(this), type.getValue(), n));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_move_dims 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff moveDims(ISLDimType dst_type, int dst_pos, ISLDimType src_type, int src_pos, int n) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_move_dims) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_move_dims(getNativePtr(this), dst_type.getValue(), dst_pos, src_type.getValue(), src_pos, n));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_intersect_params 
	 * 
	 * @take this set
	 * @generated
	**/
	public ISLMultiPWAff intersectParams(ISLSet set) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_intersect_params) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, set);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_intersect_params(getNativePtr(this), getNativePtr(set)));
			} finally {
				taken(this);
				taken(set);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_intersect_domain 
	 * 
	 * @take this domain
	 * @generated
	**/
	public ISLMultiPWAff intersectDomain(ISLSet domain) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_intersect_domain) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, domain);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_intersect_domain(getNativePtr(this), getNativePtr(domain)));
			} finally {
				taken(this);
				taken(domain);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_pullback_multi_aff 
	 * 
	 * @take this ma
	 * @generated
	**/
	public ISLMultiPWAff pullback(ISLMultiAff ma) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_pullback_multi_aff) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, ma);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_pullback_multi_aff(getNativePtr(this), getNativePtr(ma)));
			} finally {
				taken(this);
				taken(ma);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_pullback_pw_multi_aff 
	 * 
	 * @take this pma
	 * @generated
	**/
	public ISLMultiPWAff pullback(ISLPWMultiAff pma) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_pullback_pw_multi_aff) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, pma);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_pullback_pw_multi_aff(getNativePtr(this), getNativePtr(pma)));
			} finally {
				taken(this);
				taken(pma);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_pullback_multi_pw_aff 
	 * 
	 * @take this mpa2
	 * @generated
	**/
	public ISLMultiPWAff pullback(ISLMultiPWAff mpa2) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_pullback_multi_pw_aff) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, mpa2);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_pullback_multi_pw_aff(getNativePtr(this), getNativePtr(mpa2)));
			} finally {
				taken(this);
				taken(mpa2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_range_product 
	 * 
	 * @take this mpa2
	 * @generated
	**/
	public ISLMultiPWAff rangeProduct(ISLMultiPWAff mpa2) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_range_product) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, mpa2);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_range_product(getNativePtr(this), getNativePtr(mpa2)));
			} finally {
				taken(this);
				taken(mpa2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_product 
	 * 
	 * @take this mpa2
	 * @generated
	**/
	public ISLMultiPWAff product(ISLMultiPWAff mpa2) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_product) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, mpa2);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_product(getNativePtr(this), getNativePtr(mpa2)));
			} finally {
				taken(this);
				taken(mpa2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_flat_range_product 
	 * 
	 * @take this mpa2
	 * @generated
	**/
	public ISLMultiPWAff flatRangeProduct(ISLMultiPWAff mpa2) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_flat_range_product) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, mpa2);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_flat_range_product(getNativePtr(this), getNativePtr(mpa2)));
			} finally {
				taken(this);
				taken(mpa2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_factor_range 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff factorRange() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_factor_range) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_factor_range(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_range_factor_domain 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff rangeFactorDomain() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_range_factor_domain) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_range_factor_domain(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_range_factor_range 
	 * 
	 * @take this
	 * @generated
	**/
	public ISLMultiPWAff rangeFactorRange() {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_range_factor_range) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_range_factor_range(getNativePtr(this)));
			} finally {
				taken(this);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_range_splice 
	 * 
	 * @take this mpa2
	 * @generated
	**/
	public ISLMultiPWAff rangeSplice(int pos, ISLMultiPWAff mpa2) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_range_splice) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, mpa2);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_range_splice(getNativePtr(this), pos, getNativePtr(mpa2)));
			} finally {
				taken(this);
				taken(mpa2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_splice 
	 * 
	 * @take this mpa2
	 * @generated
	**/
	public ISLMultiPWAff splice(int in_pos, int out_pos, ISLMultiPWAff mpa2) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_splice) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, mpa2);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_splice(getNativePtr(this), in_pos, out_pos, getNativePtr(mpa2)));
			} finally {
				taken(this);
				taken(mpa2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_gist_params 
	 * 
	 * @take this set
	 * @generated
	**/
	public ISLMultiPWAff gistParams(ISLSet set) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_gist_params) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, set);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_gist_params(getNativePtr(this), getNativePtr(set)));
			} finally {
				taken(this);
				taken(set);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_gist 
	 * 
	 * @take this set
	 * @generated
	**/
	public ISLMultiPWAff gist(ISLSet set) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_gist) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, set);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_gist(getNativePtr(this), getNativePtr(set)));
			} finally {
				taken(this);
				taken(set);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_add 
	 * 
	 * @take this mpa2
	 * @generated
	**/
	public ISLMultiPWAff add(ISLMultiPWAff mpa2) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_add) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, mpa2);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_add(getNativePtr(this), getNativePtr(mpa2)));
			} finally {
				taken(this);
				taken(mpa2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_sub 
	 * 
	 * @take this mpa2
	 * @generated
	**/
	public ISLMultiPWAff sub(ISLMultiPWAff mpa2) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_sub) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, mpa2);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_sub(getNativePtr(this), getNativePtr(mpa2)));
			} finally {
				taken(this);
				taken(mpa2);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_scale_val 
	 * 
	 * @take this v
	 * @generated
	**/
	public ISLMultiPWAff scale(ISLVal v) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_scale_val) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, v);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_scale_val(getNativePtr(this), getNativePtr(v)));
			} finally {
				taken(this);
				taken(v);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_scale_down_val 
	 * 
	 * @take this v
	 * @generated
	**/
	public ISLMultiPWAff scaleDown(ISLVal v) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_scale_down_val) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, v);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_scale_down_val(getNativePtr(this), getNativePtr(v)));
			} finally {
				taken(this);
				taken(v);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_mod_multi_val 
	 * 
	 * @take this mv
	 * @generated
	**/
	public ISLMultiPWAff mod(ISLMultiVal mv) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_mod_multi_val) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, mv);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_mod_multi_val(getNativePtr(this), getNativePtr(mv)));
			} finally {
				taken(this);
				taken(mv);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_scale_multi_val 
	 * 
	 * @take this mv
	 * @generated
	**/
	public ISLMultiPWAff scale(ISLMultiVal mv) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_scale_multi_val) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, mv);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_scale_multi_val(getNativePtr(this), getNativePtr(mv)));
			} finally {
				taken(this);
				taken(mv);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_multi_pw_aff_scale_down_multi_val 
	 * 
	 * @take this mv
	 * @generated
	**/
	public ISLMultiPWAff scaleDown(ISLMultiVal mv) {
		/*** PROTECTED REGION ID(isl_multi_pw_aff_scale_down_multi_val) DISABLED START ***/
		ISLMultiPWAff res;
		
		synchronized(LOCK) {
			checkParameters(this, mv);
			try {
				res = ISLMultiPWAff.build(ISLNative.isl_multi_pw_aff_scale_down_multi_val(getNativePtr(this), getNativePtr(mv)));
			} finally {
				taken(this);
				taken(mv);
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	
	public String toString() {
		/*** PROTECTED REGION ID(ISLMultiPWAff_toString) DISABLED START ***/
			return ISLPrettyPrinter.asString(this);
		/*** PROTECTED REGION END ***/
	}
}
