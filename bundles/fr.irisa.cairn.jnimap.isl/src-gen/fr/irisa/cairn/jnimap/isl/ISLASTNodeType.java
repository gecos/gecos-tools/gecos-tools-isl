package fr.irisa.cairn.jnimap.isl;

import java.util.HashMap;
import java.util.Map;

/*** PROTECTED REGION ID(ISLASTNodeType_userImport) ENABLED START ***/
	/*
		Put you custom import here ....
	*/
/*** PROTECTED REGION END ***/

/**
 *  Automatically generated by jnimap 
 * @generated
 */

public class ISLASTNodeType  {

	int value;
	String name;

	/*** PROTECTED REGION ID(ISLASTNodeType_values) DISABLED START ***/
	public static final int ISL_AST_NODE_FOR = 1;
	public static final int ISL_AST_NODE_IF = 2;
	public static final int ISL_AST_NODE_BLOCK = 3;
	public static final int ISL_AST_NODE_MARK = 4;
	public static final int ISL_AST_NODE_USER = 5;
	/*** PROTECTED REGION END ***/
	
	public static ISLASTNodeType isl_ast_node_for = new ISLASTNodeType("isl_ast_node_for", ISL_AST_NODE_FOR);
	public static ISLASTNodeType isl_ast_node_if = new ISLASTNodeType("isl_ast_node_if", ISL_AST_NODE_IF);
	public static ISLASTNodeType isl_ast_node_block = new ISLASTNodeType("isl_ast_node_block", ISL_AST_NODE_BLOCK);
	public static ISLASTNodeType isl_ast_node_mark = new ISLASTNodeType("isl_ast_node_mark", ISL_AST_NODE_MARK);
	public static ISLASTNodeType isl_ast_node_user = new ISLASTNodeType("isl_ast_node_user", ISL_AST_NODE_USER);
	
	public static ISLASTNodeType build(long value) {
		return getFromInt((int) value) ;
	}
	
	
	public static  ISLASTNodeType getFromInt(int value) {
		/*** PROTECTED REGION ID(ISLASTNodeType_getValues) DISABLED START ***/
		switch(value) {
			case ISL_AST_NODE_FOR:
			return isl_ast_node_for;
			case ISL_AST_NODE_IF:
			return isl_ast_node_if;
			case ISL_AST_NODE_BLOCK:
			return isl_ast_node_block;
			case ISL_AST_NODE_MARK:
			return isl_ast_node_mark;
			case ISL_AST_NODE_USER:
			return isl_ast_node_user;
				default :
					return null;
			}
		/*** PROTECTED REGION END ***/
	}

	private ISLASTNodeType(String name, int value) {
		this.value=value;
		this.name=name;
	}
	
	public int getValue() {
		return this.value;
	}

	public String getName() {
		return this.name;
	}
	
}
