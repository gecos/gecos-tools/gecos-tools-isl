package fr.irisa.cairn.jnimap.isl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.irisa.cairn.jnimap.runtime.JNIObject;


/*** PROTECTED REGION ID(ISLUnionFlow_userImport) ENABLED START ***/
	/*
		Put you custom import here ....
	*/
/*** PROTECTED REGION END ***/

/**
 *  Automatically generated by jnimap 
 * @generated
 */
public class ISLUnionFlow extends JNIObject implements IISLDataflowResults {
	
	/* @generated */
	protected ISLUnionFlow(long ptr) {
		/*** PROTECTED REGION ID(ISLUnionFlow_Constructor) DISABLED START ***/
		super(ptr);
		/*** PROTECTED REGION END ***/
	}
	
	/* @generated */
	protected static ISLUnionFlow build(long ptr) {
		return new ISLUnionFlow(ptr);
	}
	
	/*** PROTECTED REGION ID(ISLUnionFlow_userCode) ENABLED START ***/
	/*** PROTECTED REGION END ***/

	
	/*************************************** 
	 *	         Static Methods            * 
	 ***************************************/
	/**
	 * isl_union_flow_to_string 
	 * 
	 * @generated
	**/
	 public static String _toString(ISLUnionFlow flow, int format) { 
		/*** PROTECTED REGION ID(static_isl_union_flow_to_string) DISABLED START ***/
		String res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLNative.isl_union_flow_to_string(getNativePtr(flow), format);
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	
	/*************************************** 
	 *	         Member Methods            * 
	 ***************************************/
	/**
	 * isl_union_flow_get_ctx 
	 * 
	 * @generated
	**/
	public ISLContext getContext() {
		/*** PROTECTED REGION ID(isl_union_flow_get_ctx) DISABLED START ***/
		ISLContext res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLContext.build(ISLNative.isl_union_flow_get_ctx(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_union_flow_copy 
	 * 
	 * @generated
	**/
	public ISLUnionFlow copy() {
		/*** PROTECTED REGION ID(isl_union_flow_copy) DISABLED START ***/
		ISLUnionFlow res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLUnionFlow.build(ISLNative.isl_union_flow_copy(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_union_flow_free 
	 * 
	 * @take this
	 * @generated
	**/
	public void free() {
		/*** PROTECTED REGION ID(isl_union_flow_free) DISABLED START ***/
		
		synchronized(LOCK) {
			checkParameters(this);
			try {
				ISLNative.isl_union_flow_free(getNativePtr(this));
			} finally {
				taken(this);
			}
		}
		
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_union_flow_get_must_dependence 
	 * 
	 * @generated
	**/
	public ISLUnionMap getMustDependence() {
		/*** PROTECTED REGION ID(isl_union_flow_get_must_dependence) DISABLED START ***/
		ISLUnionMap res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLUnionMap.build(ISLNative.isl_union_flow_get_must_dependence(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_union_flow_get_may_dependence 
	 * 
	 * @generated
	**/
	public ISLUnionMap getMayDependence() {
		/*** PROTECTED REGION ID(isl_union_flow_get_may_dependence) DISABLED START ***/
		ISLUnionMap res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLUnionMap.build(ISLNative.isl_union_flow_get_may_dependence(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_union_flow_get_full_must_dependence 
	 * 
	 * @generated
	**/
	public ISLUnionMap getFullMustDependence() {
		/*** PROTECTED REGION ID(isl_union_flow_get_full_must_dependence) DISABLED START ***/
		ISLUnionMap res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLUnionMap.build(ISLNative.isl_union_flow_get_full_must_dependence(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_union_flow_get_full_may_dependence 
	 * 
	 * @generated
	**/
	public ISLUnionMap getFullMayDependence() {
		/*** PROTECTED REGION ID(isl_union_flow_get_full_may_dependence) DISABLED START ***/
		ISLUnionMap res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLUnionMap.build(ISLNative.isl_union_flow_get_full_may_dependence(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_union_flow_get_must_no_source 
	 * 
	 * @generated
	**/
	public ISLUnionMap getMustNoSource() {
		/*** PROTECTED REGION ID(isl_union_flow_get_must_no_source) DISABLED START ***/
		ISLUnionMap res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLUnionMap.build(ISLNative.isl_union_flow_get_must_no_source(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	/**
	 * isl_union_flow_get_may_no_source 
	 * 
	 * @generated
	**/
	public ISLUnionMap getMayNoSource() {
		/*** PROTECTED REGION ID(isl_union_flow_get_may_no_source) DISABLED START ***/
		ISLUnionMap res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLUnionMap.build(ISLNative.isl_union_flow_get_may_no_source(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	
	public String toString() {
		/*** PROTECTED REGION ID(ISLUnionFlow_toString) DISABLED START ***/
			return ISLPrettyPrinter.asString(this);
		/*** PROTECTED REGION END ***/
	}
}
