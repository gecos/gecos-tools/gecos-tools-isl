package fr.irisa.cairn.jnimap.isl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.irisa.cairn.jnimap.runtime.JNIObject;


/*** PROTECTED REGION ID(ISLASTUserNode_userImport) ENABLED START ***/
	/*
		Put you custom import here ....
	*/
/*** PROTECTED REGION END ***/

/**
 *  Automatically generated by jnimap 
 * @generated
 */
public class ISLASTUserNode extends ISLASTNode  {
	
	/* @generated */
	protected ISLASTUserNode(long ptr) {
		/*** PROTECTED REGION ID(ISLASTUserNode_Constructor) DISABLED START ***/
		super(ptr);
		/*** PROTECTED REGION END ***/
	}
	
	/* @generated */
	protected static ISLASTUserNode build(long ptr) {
		return new ISLASTUserNode(ptr);
	}
	
	/*** PROTECTED REGION ID(ISLASTUserNode_userCode) ENABLED START ***/
	/*** PROTECTED REGION END ***/

	
	/*************************************** 
	 *	         Static Methods            * 
	 ***************************************/
	
	/*************************************** 
	 *	         Member Methods            * 
	 ***************************************/
	/**
	 * isl_ast_node_user_get_expr 
	 * 
	 * @generated
	**/
	public ISLASTExpression getExpression() {
		/*** PROTECTED REGION ID(isl_ast_node_user_get_expr) DISABLED START ***/
		ISLASTExpression res;
		
		synchronized(LOCK) {
			checkParameters();
			try {
				res = ISLASTExpression.build(ISLNative.isl_ast_node_user_get_expr(getNativePtr(this)));
			} finally {
			}
		}
		
		return res;
		/*** PROTECTED REGION END ***/
	}
	
	
	public void accept(IISLASTNodeVisitor visitor) {
		visitor.visitISLASTUserNode(this);
	}
	
	public String toString() {
		/*** PROTECTED REGION ID(ISLASTUserNode_toString) DISABLED START ***/
			return ISLPrettyPrinter.asString(this);
		/*** PROTECTED REGION END ***/
	}
}
