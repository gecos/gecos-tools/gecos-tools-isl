package fr.irisa.cairn.jnimap.isl;

public class ISLReversedUnionFlow implements IISLDataflowResults {

	private final ISLUnionFlow flow;
	
	public ISLReversedUnionFlow(ISLUnionFlow flow) {
		this.flow = flow;
	}
	
//	public static String _toString(ISLUnionFlow flow, int format) { 
//		return ISLUnionFlow._toString(flow, format);
//	}
	 
	public ISLContext getContext() {
		return flow.getContext();
	}
	
	@Override
	public ISLUnionMap getMustDependence() {
		return flow.getMustDependence().reverse();
	}

	@Override
	public ISLUnionMap getMayDependence() {
		return flow.getMayDependence().reverse();
	}

	@Override
	public ISLUnionMap getFullMustDependence() {
		return flow.getFullMustDependence().reverse();
	}

	@Override
	public ISLUnionMap getFullMayDependence() {
		return flow.getFullMayDependence().reverse();
	}

	@Override
	public ISLUnionMap getMustNoSource() {
		return flow.getMustNoSource().reverse();
	}

	@Override
	public ISLUnionMap getMayNoSource() {
		return flow.getMayNoSource().reverse();
	}
	
	public String toString() {
		return ISLPrettyPrinter.asString(this.flow);
	}

}
