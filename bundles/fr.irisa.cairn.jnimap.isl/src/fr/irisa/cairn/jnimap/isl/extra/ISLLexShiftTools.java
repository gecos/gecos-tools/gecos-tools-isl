
package fr.irisa.cairn.jnimap.isl.extra;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLMultiAff;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLSpace;

public class ISLLexShiftTools {
		
	public static ISLMap lexNextPower(ISLSet dom, int depth, int power) {
		return lexShiftPower(dom,depth,power,false,true);	
	}
	
	public static ISLMap lexPredPower(ISLSet dom, int depth, int power) {
		return lexShiftPower(dom,depth,power,true,false);	
	}
	
	public static ISLMap lexShiftPower(ISLSet dom, int depth, int power, boolean pred, boolean verbose) {
		if (verbose) System.out.println("[LexShiftPower] ***** Start *****");
		if (verbose) System.out.println("[LexShiftPower] Computing "+(pred?"Pred":"Next")+" on "+depth+" innermost dimensions of");
		if (verbose) System.out.println("[LexShiftPower] "+dom);
		
		//compute next
		ISLMap current = pred?lexPred(dom,depth):lexNext(dom,depth);
		
		if (verbose) System.out.println("[LexShiftPower] "+(pred?"Pred":"Next")+" = "+current);
		
		return current.power(power);
	}
	
	public static ISLMap lexNext(ISLSet dom, int nbInnerLoop) {

		int nb = dom.getNbIndices();
		if (nbInnerLoop > nb) nbInnerLoop = nb;
		if (nbInnerLoop < 1) {
			//happen when dom has 0 dimensions.
			return ISLMap.buildEmpty(ISLSpace.idMapDimFromSetDim(dom.getSpace()));
		}
		ISLMap map = ISLMap.buildFromDomainAndRange(dom.copy(), dom.copy());
//		JNIISLMap lt = JNIISLMap.lexLT(dom.getSpace().copy());
		
		ISLMap nxt = null;
//		int nb = lt.getNumberOfBasicMap();
		int i;
		for (i = nb-1; i >= nb-nbInnerLoop; i--) {
//			JNIISLMap succs = JNIISLMap.fromBasicMap(lt.getBasicMapAt(i));
			ISLMap succs = dom.getSpace().lexLTAt(i);
			ISLMap lexMin = map.intersect(succs).makeDisjoint().lexMin();
			if (nxt == null) {
				nxt = lexMin;
			} else {
				nxt = nxt.union(lexMin);
			}
			/*
			 * Complement + intersect is more efficient in terms guard count.
			 * Mainly due to isl behavior.
			 */
//			JNIISLSet remain = JNIISLSet.substract(dom.copy(),nxt.copy().getDomain());
			ISLSet remain = nxt.getDomain().complement().intersect(dom.copy());
			map = ISLMap.buildFromDomainAndRange(remain, dom.copy());
		}

		return nxt.coalesce();
	}
	
	/**
	 * Computes the immediate successor in a given domain, where the validity
	 * domains have been gisted.
	 * 
	 * Main purpose : guard-less code generation.
	 * 
	 * @param dom
	 *            domain to scan
	 * @param nbInnerLoop
	 *            number of innermost loops to stick to (limit complexity)
	 * @return a list containing the expression of the immediate successor in
	 *         dom, XXX ordered by dimensions, from innermost to outermost
	 *         dimensions. Entries are also ordered within the Maps (because of
	 *         the gist operation).
	 */
	public static List<Map<ISLSet,ISLMultiAff>> gistedOrderedLexNext(ISLSet dom, int nbInnerLoop) {
		int nDim = (int) dom.getNbIndices();
		if (nbInnerLoop > nDim) nbInnerLoop = nDim;
		if (nbInnerLoop < 1) {
			//happen when dom has 0 dimensions.
			return new ArrayList<Map<ISLSet,ISLMultiAff>>(0);
		}
		
		List<Map<ISLSet,ISLMultiAff>> res = new ArrayList<Map<ISLSet,ISLMultiAff>>(nbInnerLoop);
		
		int i;
		for (i = nDim-1; i >= nDim-nbInnerLoop; i--) {
			ISLMap lexLTi = dom.getSpace().lexLTAt(i);
			ISLSet from = dom.copy().eliminateDims(i+1,nDim-i-1).simplify();
			ISLMap start = ISLMap.buildFromDomainAndRange(from, dom.copy());
			ISLMap succi = start.intersect(lexLTi);
			ISLMap tmpnexti = succi.lexMin().simplify();
			Map<ISLSet, ISLMultiAff> closedFormRelation = tmpnexti.getClosedFormRelation();
			
			// use LinkedHashMap to keep the order on the key set.
			Map<ISLSet, ISLMultiAff> tmp = new LinkedHashMap<ISLSet, ISLMultiAff>();
			for (Entry<ISLSet, ISLMultiAff> e : closedFormRelation.entrySet()) {
				ISLSet valid = e.getKey();
				ISLMultiAff func = e.getValue();
				ISLSet gistedValid = valid.gist(dom.copy()).simplify();
				if (!gistedValid.isEmpty()) {
					tmp.put(gistedValid, func);
				}
				
			}
			res.add(tmp);
		}
		return res;
	}

	/**
	 * @deprecated doesnt seem to be doing what it should do...
	 */
	@Deprecated
	static List<Map<ISLSet,ISLMultiAff>> gistedOrderedLexNext(ISLSet dom, int nbInnerLoop, int power) {
		int nDim = (int) dom.getNbIndices();
		if (nbInnerLoop > nDim) nbInnerLoop = nDim;
		if (nbInnerLoop < 1) {
			//happen when dom has 0 dimensions.
			return new ArrayList<Map<ISLSet,ISLMultiAff>>(0);
		}
		
		List<Map<ISLSet,ISLMultiAff>> res = new ArrayList<Map<ISLSet,ISLMultiAff>>(nbInnerLoop);
		
		ISLSet history = null;
		//Innermost dimension
		{
			//find the case when next^power is within the innermost loop
			int dim = nDim-1;
			ISLMap lexLTi = dom.getSpace().copy().lexLTAt(dim);
			ISLSet from = dom.copy().eliminateDims(dim+1,nDim-dim-1).simplify();
			ISLMap start = ISLMap.buildFromDomainAndRange(from, dom.copy());
			ISLMap succi = start.copy().intersect(lexLTi.copy());
			System.err.println("succi: "+succi);
			ISLMap tmpnexti = succi.copy().lexMin().simplify();
			System.err.println("tmpnexti: "+tmpnexti);
			
			history = tmpnexti.copy().getDomain();
			
			res.add(getClosedForm(tmpnexti, dom));
		}
		
		// shifting in innermost loop only
		// innermost + 1 = shift in the innermost for (power-1) and then find next
		// innermost + 2 = shift in the innermost for (power-2), find next and then shift by 1
		//   ...
		// innermost + power-1
		
		// innermost loop only
		// outer(1) + shift
		// outer(2) + shift
		// outer(...) + shift
		//  -- assume the shift wont cross another boundary 
		
		{
			int dim = nDim-1;
			ISLMap lexLTi = dom.getSpace().copy().lexLTAt(dim-1);
			ISLSet from = dom.copy().eliminateDims(dim+1,nDim-dim-1).simplify();
			from = from.subtract(history);
			ISLMap start = ISLMap.buildFromDomainAndRange(from, dom.copy());
			ISLMap succi = start.copy().intersect(lexLTi.copy());
			System.err.println("succi: "+succi);
			ISLMap tmpnexti = succi.copy().lexMin().simplify();
			System.err.println("tmpnexti: "+tmpnexti);
			res.add(getClosedForm(tmpnexti, dom));
		}
		
		{
			int dim = nDim-1;
			ISLMap lexLTi = dom.getSpace().copy().lexLTAt(dim-1);
			ISLSet from = dom.copy().eliminateDims(dim+1,nDim-dim-1).simplify();
			from = from.subtract(history);
			ISLMap start = ISLMap.buildFromDomainAndRange(from, dom.copy());
			ISLMap succi = start.copy().intersect(lexLTi.copy());
			System.err.println("succi: "+succi);
			ISLMap tmpnexti = succi.copy().lexMin().simplify();
			System.err.println("tmpnexti: "+tmpnexti);
			res.add(getClosedForm(tmpnexti, dom));
		}
		return res;
	}
	
	private static Map<ISLSet, ISLMultiAff> getClosedForm(ISLMap map, ISLSet dom) {
		Map<ISLSet, ISLMultiAff> closedFormRelation = map.getClosedFormRelation();
		
		// use LinkedHashMap to keep the order on the key set.
		Map<ISLSet, ISLMultiAff> tmp = new LinkedHashMap<ISLSet, ISLMultiAff>();
		for (Entry<ISLSet, ISLMultiAff> e : closedFormRelation.entrySet()) {
			ISLSet valid = e.getKey();
			ISLMultiAff func = e.getValue();
			ISLSet gistedValid = valid.copy().gist(dom.copy()).simplify();
			if (!gistedValid.isEmpty())
				tmp.put(gistedValid, func);
		}
		
		return tmp;
	}
	
	public static ISLMap lexPred(ISLSet dom, int nbInnerLoop) {

		int nDim = dom.getNbIndices();
		if (nbInnerLoop > nDim) nbInnerLoop = nDim;
		if (nbInnerLoop < 1) {
			//happen when dom has 0 dimensions.
			return ISLMap.buildEmpty(ISLSpace.idMapDimFromSetDim(dom.getSpace()));
		}
		
		ISLMap map = ISLMap.buildFromDomainAndRange(dom.copy(), dom.copy());
		ISLMap gt = dom.getSpace().copy().toLexGTMap();
		
		ISLMap prd = null;
		int nb = gt.getNbBasicMaps();
		int i;
		for (i = nb-1; i >= nb-nbInnerLoop; i--) {
			ISLMap preds = gt.getBasicMapAt(i).toMap();
			ISLMap lexMax = map.intersect(preds).makeDisjoint().lexMax();
			if (prd == null) {
				prd = lexMax;
			} else {
				prd = prd.union(lexMax);
			}
			ISLSet remain = dom.copy().subtract(prd.copy().getDomain());
			map = ISLMap.buildFromDomainAndRange(remain,dom.copy());
		}
		return prd.coalesce();
	}
}