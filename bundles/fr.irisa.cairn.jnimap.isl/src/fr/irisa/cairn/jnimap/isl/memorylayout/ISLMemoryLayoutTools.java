package fr.irisa.cairn.jnimap.isl.memorylayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.javatuples.Pair;

import fr.irisa.cairn.jnimap.isl.ISLBasicMap;
import fr.irisa.cairn.jnimap.isl.ISLConstraint;
import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLMultiAff;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLSpace;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;

/**
 * 
 * @author amorvan
 * 
 */
public class ISLMemoryLayoutTools {

	public static boolean debug = true;
	private static String DEFAULT_PREFIX = "[MEM_ALLOC] ";
	private String PREFIX;
	public ISLMemoryLayoutTools() {PREFIX = DEFAULT_PREFIX;}
	public ISLMemoryLayoutTools(String p) {PREFIX = p+DEFAULT_PREFIX;}
	private void debug(Object o) {
		if (debug) {
			String string = PREFIX+o.toString().replace("\n","\n"+PREFIX);
			System.out.println(string);
		}
	}
	
	/**
	 * Cf. memory_allocation.pdf
	 * 
	 * @param arrayName
	 * @param domains
	 * @param writes
	 * @param reads
	 * @param prdg
	 * @param schedule
	 * @return
	 */
	public List<Map<ISLSet, ISLMultiAff>> contractSuccessiveModuloBasic(
			String arrayName, ISLSet context,ISLUnionSet domains, ISLUnionMap writes,
			ISLUnionMap reads, ISLUnionMap prdg, ISLUnionMap schedule
			) {
		int nbDims = schedule.getRange().getSetAt(0).getNbIndices();
		List<Boolean> parallelDims = new ArrayList<Boolean>(nbDims);
		//by default : consider all dimensions are sequential
		for (int i = 0; i < nbDims; i++)
			parallelDims.add(false);
		return contractSuccessiveModuloBasic(arrayName, context, domains, writes, reads, prdg, schedule, parallelDims);
	}
	
	/**
	 * Cf. memory_allocation.pdf
	 * 
	 * @param arrayName
	 * @param domains
	 * @param writes
	 * @param reads
	 * @param prdg
	 * @param schedule
	 * @param parallelDimensions
	 * @return
	 */
	public List<Map<ISLSet, ISLMultiAff>> contractSuccessiveModuloBasic(
			String arrayName,
			ISLSet context,
			ISLUnionSet domains, 
			ISLUnionMap writes,
			ISLUnionMap reads,
			ISLUnionMap prdg,
			ISLUnionMap schedule,
			List<Boolean> parallelDimensions
			) {

		debug("/*****************************\n ** START Contracting array "+arrayName+"\n *****************************/\n");

		debug("input:");
		debug("  context =  "+context);
		debug("  domain =   "+domains);
		debug("  prdg =     "+prdg);
		debug("  schedule = "+schedule);
		debug("  // dims =  "+parallelDimensions);
		debug("  writes =   "+writes);
		debug("  reads =    "+reads);
		
		Pair<ISLMap, ISLSet> memoryBasedScheduledDependencies = selectAndScheduleForContraction(arrayName, domains, writes, reads, prdg, schedule);
		ISLMap deps = memoryBasedScheduledDependencies.getValue0();
		ISLSet scheduledWriteDomain = memoryBasedScheduledDependencies.getValue1();
		
		if (deps == null)
			throw new RuntimeException();
		ISLMap cs = buildCS(deps,scheduledWriteDomain,parallelDimensions);
		
		/**
		 * Move CS from the scheduled iteration space to the accessed array
		 * index space. Necessary for in-place contraction.
		 * 
		 * XXX
		 */
		List<ISLMap> writesMaps = writes.getMaps();
		ISLUnionMap arayWrites = null;
		for (ISLMap m : writesMaps) {
			if (m.getTupleName(ISLDimType.isl_dim_out).equals(arrayName))
				if (arayWrites == null) arayWrites = m.toUnionMap();
				else arayWrites = arayWrites.union(m.toUnionMap());
		}
		ISLUnionMap applyRange = schedule.copy().reverse().applyRange(arayWrites.copy());
		if (applyRange.getNbMaps() != 1)
			throw new RuntimeException();
		ISLMap scheduledWriteToArrayAccess = applyRange.getMapAt(0);
		cs = cs.applyDomain(scheduledWriteToArrayAccess.copy()).applyRange(scheduledWriteToArrayAccess.copy());
		debug("cs (in the array space) = "+cs);
		ISLSet ds = buildDS(context, cs);
		List<Map<ISLSet, ISLMultiAff>> successiveModuloBasic = successiveModuloBasic(ds);
		debug("\n/*****************************\n ** END Contracting array "+arrayName+"\n *****************************/");

		return successiveModuloBasic;
	}
	
	/**
	 * 
	 * @param ds
	 * @return
	 */
	List<Map<ISLSet, ISLMultiAff>> successiveModuloBasic(ISLSet ds) {

		long nbDims = ds.copy().getNbIndices();
		List<Map<ISLSet, ISLMultiAff>> res = new ArrayList<Map<ISLSet, ISLMultiAff>>((int)nbDims);
		ISLSpace space = ds.getSpace();
		
		ISLMap map = ISLMap.lexEQ(space.copy());
		map = map.projectOut(ISLDimType.isl_dim_in, 0, map.getNbInputs());
		map = map.intersectRange(ds.copy());
		
		space = map.getSpace();
		
		for (int i = 0; i < nbDims; i++) {
			debug("expansion degree #"+i+" : ");
			ISLMap workingCopyDS = map.copy();
			
			ISLBasicMap previousDimsToZero = ISLBasicMap.buildUniverse(space.copy());
			for (int j = 0; j < i; j++) {
				ISLConstraint c = ISLConstraint.buildEquality(space.copy());
				c = c.setConstant(0);
				c = c.setCoefficient(ISLDimType.isl_dim_out, j, 1);
				previousDimsToZero = previousDimsToZero.addConstraint(c);
			}
			workingCopyDS = workingCopyDS.intersect(previousDimsToZero.toMap());
			debug(" - projecting (on "+workingCopyDS+")");
			workingCopyDS = workingCopyDS.copy().projectOnto(ISLDimType.isl_dim_out, i);
			
			debug(" - simple hull (on "+workingCopyDS+")");
			ISLMap tmpCopy = workingCopyDS.copy();
			workingCopyDS = workingCopyDS.intersectRange(tmpCopy.getRange().simpleHull().toSet());//.coalesce();
			
			// XXX if the schedule has less dimensions than the array, there
//			if (workingCopyDS.getNbBasicMaps() == 1) {
//				JNIISLBasicMap basicMapAt = workingCopyDS.copy().getBasicMapAt(0);
//				if (basicMapAt.projectOut(JNIISLDimType.isl_dim_param, 0, basicMapAt.getNbDims(JNIISLDimType.isl_dim_param)).isUniverse()) {
//					res.add(new LinkedHashMap<JNIISLSet, JNIISLMultiAff>());
//					continue;
//				}
//			}
			
			debug(" - computing lexmax (on "+workingCopyDS+")");
			workingCopyDS = workingCopyDS.lexMax().coalesce();
			debug("\t - lexmax = "+workingCopyDS);
	
			if (workingCopyDS.involvesDims(ISLDimType.isl_dim_param, 0, workingCopyDS.getNbParams())) {
				// Check is parameter space is bounded or not
				ISLMap noParamWorkingCopyDS = workingCopyDS.copy().projectOut(ISLDimType.isl_dim_param, 0, workingCopyDS.getNbParams()).removeDivs();
				debug("\t - after projecting parameters = "+noParamWorkingCopyDS);
				if (noParamWorkingCopyDS.copy().range().hasUpperBound(ISLDimType.isl_dim_set, 0)) {
					debug("\t - you are lucky ! there exist a constant array foldoing factor "+noParamWorkingCopyDS.copy().lexMax());
					noParamWorkingCopyDS = noParamWorkingCopyDS.copy().lexMax();
					noParamWorkingCopyDS = noParamWorkingCopyDS.alignParams(workingCopyDS.getSpace());
					debug("\t - aligned "+noParamWorkingCopyDS);
					workingCopyDS =  noParamWorkingCopyDS;
				}
				//if (noParamWorkingCopyDS.is)
			}

			debug(" - shifting +1  (on "+workingCopyDS+")");
			ISLMap shift = ISLBasicMap.buildShift(workingCopyDS.copy().getRange().getSpace(), 0, 1).toMap();
			workingCopyDS = workingCopyDS.applyRange(shift);
			
			Map<ISLSet, ISLMultiAff> closedFormRelation = workingCopyDS.copy().getClosedFormRelation();
			/**
			 * various heuristics to reduce the number of pieces.
			 */
			debug(" - simplify "+closedFormRelation);
			closedFormRelation = new AllocationFunctionSimplification(PREFIX).simplify(closedFormRelation);

			debug("expansion #"+i+" : "+closedFormRelation);
			res.add(closedFormRelation);
		}
		
		return res;
	}
	
	/**
	 * For memory allocation : select the dependencies involved with statementID
	 * and move it to the scheduled space
	 */
	ISLMap selectAndScheduleForAllocation(
			String statementID, // the ID of the statement that has to be reallocated
			ISLUnionSet domains, 
			ISLUnionMap writes,
			ISLUnionMap reads,
			ISLUnionMap valueBasedPrdg,
			ISLUnionMap schedule
			) {
		throw new UnsupportedOperationException("Not implemented yet");
	}

	/**
	 * For array contraction : select the dependencies involved with arrayName
	 * and move them to the scheduled space
	 */
	Pair<ISLMap,ISLSet> selectAndScheduleForContraction(
			String arrayName, // the name of the array symbol that has to be reallocated
			ISLUnionSet domains, 
			ISLUnionMap writes,
			ISLUnionMap reads,
			ISLUnionMap valueBasedPrdg,
			ISLUnionMap schedule
			) {
		debug("Contracting array "+arrayName);
		//select statements writing into the selected array
		ISLSpace space1 = writes.getDomain().getSetAt(0).getSpace();
		ISLSet base = ISLSet.buildEmpty(space1.copy());
		ISLUnionSet stmtsAccessingTmp = base.toUnionSet();
		List<ISLMap> maps = writes.copy().getMaps();
		debug("statements writing "+arrayName+" : ");
		for (ISLMap map : maps)
			if (map.getTupleName(ISLDimType.isl_dim_out).equals(arrayName)) {
				stmtsAccessingTmp = stmtsAccessingTmp.union(map.getDomain().toUnionSet());
				debug("    "+map);
			}
		
		ISLUnionSet scheduledWriteDomain = domains.copy().intersect(stmtsAccessingTmp.copy()).apply(schedule.copy());
		if (scheduledWriteDomain.getNbSets() > 1)
			throw new RuntimeException();
		
		ISLUnionMap selectedPrdg = valueBasedPrdg.copy().intersectRange(stmtsAccessingTmp);
		//select statements reading from the selected array
		base = ISLSet.buildEmpty(space1);
		stmtsAccessingTmp = base.toUnionSet();
		maps = reads.copy().getMaps();
		debug("statements reading "+arrayName+" : ");
		for (ISLMap map : maps)
			if (map.getTupleName(ISLDimType.isl_dim_out).equals(arrayName)) {
				stmtsAccessingTmp = stmtsAccessingTmp.union(map.getDomain().toUnionSet());
				debug("    "+map);
			}
		selectedPrdg = selectedPrdg.intersectDomain(stmtsAccessingTmp);
		
		//from here, we work in the scheduled space
		ISLUnionMap schedSelectedPrdg = selectedPrdg.applyDomain(schedule.copy()).applyRange(schedule.copy()).coalesce();
		debug("scheduled selected PRDG : "+schedSelectedPrdg);
		
		ISLMap res;
		if (schedSelectedPrdg.getNbMaps() == 0) 
			res = ISLMap.buildEmpty(ISLSpace.idMapDimFromSetDim(schedule.getRange().getSetAt(0).getSpace()));
		else if (schedSelectedPrdg.getNbMaps() == 1)
			res = schedSelectedPrdg.getMapAt(0);
		else
			throw new RuntimeException();
		
		return new Pair<ISLMap, ISLSet>(res, scheduledWriteDomain.getSetAt(0));
	}
	
	
	/**
	 * Build the conflict set from the selected dependencies.
	 * Instead of returning a set in Z^2n, returns a relation in
	 * Z^n <-> Z^n.
	 * @param parallelDimensions 
	 */
	ISLMap buildCS(ISLMap schedSelectedPrdgMap, ISLSet scheduledWriteDomain, List<Boolean> parallelDimensions) {
		ISLMap lastUse = schedSelectedPrdgMap.copy().reverse().lexMax().coalesce();
		debug("last use : "+lastUse);
		
		ISLSpace space = scheduledWriteDomain.copy().getSpace();
		ISLMap lexCmp = space.toLexGEMap(); // original definition
		debug("lexGE : "+lexCmp);
		
		ISLMap predWrites = lexCmp.copy().intersectRange(schedSelectedPrdgMap.copy().getRange());
		debug("predWrites : "+predWrites);
		
		ISLMap part1 = lastUse.applyRange(predWrites).coalesce();
		debug("part1 : "+part1);
		
		ISLMap part2 = part1.copy().reverse();
		debug("part2 : "+part2);
		
		ISLMap cs = part1.intersect(part2).coalesce();
		
		/** 
		 * The following commented section is covered by 
		 * the // process parallel dimensions. 
		 **/
		// cs is symetric, therefore any writing iteration conflicts with itself
//		JNIISLMap idConflict = JNIISLMap.lexEQFirst(space.copy(),nbDims);
//		debug("id conflict = "+idConflict);
//		idConflict = idConflict.intersectDomain(scheduledWriteDomain.copy());
//		debug("id conflict = "+idConflict);
//		idConflict = idConflict.intersectRange(scheduledWriteDomain.copy());
//		debug("id conflict = "+idConflict);
//		cs = cs.union(idConflict);
		
		// process parallel dimensions
		debug("process parallel dimensions");
		ISLBasicMap bm = ISLBasicMap.buildUniverse(cs.getSpace().copy());
		for (int i = 0; i < parallelDimensions.size(); i++) {
			if (parallelDimensions.get(i)) {
				// the dimension is parallel, hence one operation conflicts with
				// all the others on that same dimension
				debug("  - dim "+i+" is parallel");
			} else {
				// the dimension is not parallel, hence one operation conflicts
				// with itself only on that same dimension
				debug("  - dim "+i+" is not parallel");
				ISLConstraint c = ISLConstraint.buildEquality(cs.getSpace());
				c = c.setConstant(0);
				c = c.setCoefficient(ISLDimType.isl_dim_in, i, 1);
				c = c.setCoefficient(ISLDimType.isl_dim_out, i, -1);
				bm = bm.addConstraint(c);
			}
		}
		ISLMap parallelConflicts = bm.toMap();
		debug("  - tomap : "+parallelConflicts);
		
		parallelConflicts = parallelConflicts.intersectDomain(scheduledWriteDomain.copy());
		parallelConflicts = parallelConflicts.intersectRange(scheduledWriteDomain.copy());
		
		cs = cs.union(parallelConflicts).coalesce();

		debug("cs = "+cs);
		return cs;
	}

	/**
	 * Construct the difference set from the conflict set.
	 * 
	 * @param cs
	 */
	ISLSet buildDS(ISLSet context, ISLMap cs) {
		int nbDims = cs.dim(ISLDimType.isl_dim_in);
		ISLMap ds_tmp = cs.moveDims(ISLDimType.isl_dim_in, 0, ISLDimType.isl_dim_out, 0, nbDims);
		ds_tmp = ds_tmp.insertDims(ISLDimType.isl_dim_out, 0, nbDims);
		ISLBasicMap subtract = ISLBasicMap.buildUniverse(ds_tmp.getSpace());
		//insert constraint out = (in1-in2)
		for (int i = 0; i < nbDims; i++) {
			//out_i = in_i - in_(nbDim+i)
			ISLConstraint constraint = ISLConstraint.buildEquality(ds_tmp.getSpace());
			constraint = constraint.setConstant(0);
			constraint = constraint.setCoefficient(ISLDimType.isl_dim_in, i, 1);
			constraint = constraint.setCoefficient(ISLDimType.isl_dim_in, i+nbDims, -1);
			constraint = constraint.setCoefficient(ISLDimType.isl_dim_out, i, -1);
			subtract   = subtract.addConstraint(constraint);
		}
		
		ds_tmp = ds_tmp.intersect(subtract.toMap());
		debug("ds map = "+ds_tmp);
		ISLSet ds = ds_tmp.getRange().intersectParams(context).coalesce();
		
//		// cs is symetric, therefore ds is 0-symetric (and should contain \vec{0})
//		JNIISLSpace space = ds.getSpace();
//		JNIISLBasicSet bs = JNIISLBasicSet.universe(space.copy());
//		// build \vec{0}
//		for (int i = 0; i < nbDims; i++) {
//			JNIISLConstraint c = JNIISLConstraint.equality(space.copy());
//			c = c.setConstant(0);
//			c = c.setCoef(JNIISLDimType.isl_dim_set, i, 1);
//			bs = bs.addConstraint(c);
//		}
//		// check if DS contains \vec{0}
//		if (ds.copy().intersect(bs.toSet()).empty())
//			throw new RuntimeException();

		debug("ds = "+ds);
		return ds;
	}
	
	
	public static String printAlloc(List<Map<ISLSet, ISLMultiAff>> alloc) {
		String str = "";
		for (Map<ISLSet, ISLMultiAff> map : alloc) {
			String tmp="[";
			if (map.size() > 1)
				tmp+="max(";
			boolean first = true;
			for (ISLMultiAff maff : map.values()) {
				if (first) first = false;
				else tmp+=",";
				String string = maff.toString();

				string = string.substring(0,string.lastIndexOf("}")-3);
				string = string.substring(string.lastIndexOf(">")+4);
				
				tmp += string;
			}
			if (map.size() > 1)
				tmp += ")";
			tmp+="]";
			if (!tmp.equals("[1]"))
				str += tmp;
			else 
				str += "[]";
		}
//		if (str.contains("[]"))
//			throw new RuntimeException();
		return str;
	}
}
