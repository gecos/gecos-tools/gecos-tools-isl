package fr.irisa.cairn.jnimap.isl;

public enum ISL_FORMAT {
	ISL, POLYLIB, POLYLIB_CONSTRAINTS, OMEGA, C, LATEX
}
