 package fr.irisa.cairn.jnimap.isl.extra;

import fr.irisa.cairn.jnimap.isl.ISLBasicMap;
import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLConstraint;
import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLLocalSpace;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLSpace;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;

public class JNIISLScheduleTiling {
	
	static final int waveFront = 1;
	static final int outerBlock = 2;
	public static boolean debug = false;
	//XXX under construction...
	@Deprecated
	public static ISLUnionMap tile(ISLUnionMap plutoSchedule, int tileSize){
		if ( debug ) {
			System.out.println("*****************************************************");
			System.out.println("(ORIGSC) "+plutoSchedule);
		}
		//check schedule
		int nbStmt = plutoSchedule.getNbMaps();
		if (nbStmt < 1) throw new RuntimeException();
		
		//init tiled schedule space
		ISLSpace scheduleSpace = plutoSchedule.getMapAt(0).getSpace();
		int nbScheduleDims = (int) scheduleSpace.dim(ISLDimType.isl_dim_out);
		int nbParams = (int) scheduleSpace.dim(ISLDimType.isl_dim_param);
		ISLSpace tiledScheduleSpace = ISLSpace.alloc(nbParams, nbScheduleDims, nbScheduleDims+1);
		String tupleName = scheduleSpace.getTupleName(ISLDimType.isl_dim_out);
		if (tupleName != null) {
			tiledScheduleSpace = tiledScheduleSpace.setTupleName(ISLDimType.isl_dim_in, tupleName);
			tiledScheduleSpace = tiledScheduleSpace.setTupleName(ISLDimType.isl_dim_out, tupleName);
		}
		for (int i = 0; i < nbParams; i++) {
			tiledScheduleSpace = tiledScheduleSpace.setDimName(ISLDimType.isl_dim_param, i, scheduleSpace.getDimName(ISLDimType.isl_dim_param, i));
		}
		if ( debug )
			System.out.println("(SPACES) "+scheduleSpace+" >> "+tiledScheduleSpace);
		
		//init tiling function
//		JNIISLBasicMap btf = JNIISLBasicMap.empty(tiledScheduleSpace);
		ISLMap lexEQFirst = ISLMap.lexEQFirst(tiledScheduleSpace.copy(), nbScheduleDims-1);
		if ( debug )
			System.out.println("(LEXEQ)  "+lexEQFirst);
		
		if (lexEQFirst.getNbBasicMaps() != 1) throw new RuntimeException();
		ISLBasicMap eq = lexEQFirst.getBasicMapAt(0);
		
		eq = eq.addConstraint(genConstraint1(tiledScheduleSpace, nbScheduleDims-1, nbScheduleDims-1, nbScheduleDims, tileSize));
		eq = eq.addConstraint(genConstraint2(tiledScheduleSpace, nbScheduleDims-1, nbScheduleDims-1, nbScheduleDims, tileSize));
		eq = eq.addConstraint(genConstraint3(tiledScheduleSpace, nbScheduleDims-1, nbScheduleDims-1, nbScheduleDims, tileSize));
		
		
		ISLMap tilingFunction = eq.toMap();
		if ( debug )
			System.out.println("Tiling Function : "+tilingFunction);
		
		//apply tiling function to schedule
		ISLUnionMap res = ISLMap.buildEmpty(plutoSchedule.getSpace().copy()).toUnionMap();
		for (ISLMap map : plutoSchedule.getMaps()) {
			if (map.getNbOutputs() != nbScheduleDims) throw new RuntimeException();
			if (map.getNbParams() != nbParams) throw new RuntimeException();
			
			ISLMap tileScheduleFunction = map.applyRange(tilingFunction.copy());
			ISLUnionMap tmpUMap = tileScheduleFunction.toUnionMap();
			res = res.union(tmpUMap);
			
			
		}
		return res;
	}
	
	// generates [i] -> [i',ii] : i = 8*i' + ii
	static ISLConstraint genConstraint1(ISLSpace tiledSpace, int inPos, int outPos1, int outPos2, int tileSize) {
		ISLConstraint res = ISLConstraint.buildEquality(tiledSpace.copy().toLocalSpace());
		res = res.setCoefficient(ISLDimType.isl_dim_in, inPos, -1);
		res = res.setCoefficient(ISLDimType.isl_dim_out, outPos1, tileSize);
		res = res.setCoefficient(ISLDimType.isl_dim_out, outPos2, 1);
		
		return res;
	}

	// generates [i] -> [i',ii] : 0 <= ii
	static ISLConstraint genConstraint2(ISLSpace tiledSpace, int inPos, int outPos1, int outPos2, int tileSize) {
		ISLConstraint res = ISLConstraint.buildInequality(tiledSpace.copy().toLocalSpace());

		res = res.setCoefficient(ISLDimType.isl_dim_out, outPos2, 1);
		
		return res;
	}

	// generates [i] -> [i',ii] : ii < 8
	static ISLConstraint genConstraint3(ISLSpace tiledSpace, int inPos, int outPos1, int outPos2, int tileSize) {
		ISLConstraint res = ISLConstraint.buildInequality(tiledSpace.copy().toLocalSpace());

		res = res.setCoefficient(ISLDimType.isl_dim_out, outPos2, -1);
		res = res.setConstant(tileSize-1);
		
		return res;
	}
	
	
	private static ISLSpace initScheduleSpace(ISLSpace scheduleSpace,
			int nbParams, int nbInDims, int nbOutDims) {
		ISLSpace newScheduleSpace = ISLSpace.alloc(nbParams, nbInDims, nbOutDims);
		String tupleName = scheduleSpace.getTupleName(ISLDimType.isl_dim_out);
		if (tupleName != null) {
			newScheduleSpace = newScheduleSpace.setTupleName(ISLDimType.isl_dim_in, tupleName);
			newScheduleSpace = newScheduleSpace.setTupleName(ISLDimType.isl_dim_out, tupleName);
		}
		for (int i = 0; i < nbParams; i++) {
			newScheduleSpace = newScheduleSpace.setDimName(ISLDimType.isl_dim_param, i, scheduleSpace.getDimName(ISLDimType.isl_dim_param, i));
		}
		if ( debug )
			System.out.println("(SPACES) "+scheduleSpace+" >> " + newScheduleSpace);
		return newScheduleSpace;
	}

	private static ISLConstraint generateEqualityConstraint(
			ISLSpace wrapSpace, int inDim, int outDim) {
		ISLConstraint res = ISLConstraint.buildEquality(wrapSpace.copy().toLocalSpace());
		res = res.setCoefficient(ISLDimType.isl_dim_set, inDim, -1);
		res = res.setCoefficient(ISLDimType.isl_dim_set, outDim, 1);
		return res;
	}

	private static ISLConstraint generateEqualityConstraintOnMap(
			ISLSpace waveScheduleSpace, int i, int outDim) {
		ISLConstraint res = ISLConstraint.buildEquality(waveScheduleSpace.copy().toLocalSpace());
		res = res.setCoefficient(ISLDimType.isl_dim_in, i, -1);
		res = res.setCoefficient(ISLDimType.isl_dim_out, outDim, 1);
		return res;
	}

	/* Generates schedule to stripmine the outermost tiled loop
	 * outerLevelBand - is starting level at which the tile band begins 
	 * innerLevelBand - is the inner level till which tiling is performed
	 * numProcessors - is the size of the tile
	 * scheduleSpace - is the space of the schedule 
	 * */
	private static ISLBasicMap generateOuterBlockSchedule(int outerLevelBand, int numProcessors, ISLSpace scheduleSpace ) {
		
		/* Initialize the new schedule space */
		ISLSpace waveScheduleSpace = initScheduleSpace(scheduleSpace, scheduleSpace.dim(ISLDimType.isl_dim_param), 
				scheduleSpace.dim(ISLDimType.isl_dim_out), scheduleSpace.dim(ISLDimType.isl_dim_out)+1);
		ISLBasicMap wavefrontMap = ISLBasicMap.buildUniverse(waveScheduleSpace.copy());
		
		/* Add constraints for stripmining.  (i, j) -> (i, j', j'')*/
		/* j = j' * numProcessor + j'' */
		ISLConstraint pointOuter = ISLConstraint.buildEquality(waveScheduleSpace.copy().toLocalSpace());
		pointOuter = pointOuter.setCoefficient(ISLDimType.isl_dim_out, outerLevelBand, numProcessors);
		pointOuter = pointOuter.setCoefficient(ISLDimType.isl_dim_out, outerLevelBand+1, 1);
		pointOuter = pointOuter.setCoefficient(ISLDimType.isl_dim_in, outerLevelBand, -1);
		wavefrontMap = wavefrontMap.addConstraint(pointOuter);
		/* j'' > 0 */
		ISLConstraint plb = ISLConstraint.buildInequality(waveScheduleSpace.copy().toLocalSpace());
		plb = plb.setCoefficient(ISLDimType.isl_dim_out, outerLevelBand+1, 1);
		wavefrontMap = wavefrontMap.addConstraint(plb);
		/* j'' < numProcessors */
		ISLConstraint pub = ISLConstraint.buildInequality(waveScheduleSpace.copy().toLocalSpace());
		pub = pub.setCoefficient(ISLDimType.isl_dim_out, outerLevelBand+1, -1);
		pub = pub.setConstant(numProcessors-1);
		wavefrontMap = wavefrontMap.addConstraint(pub);
		/* add equality constraint for other dimensions */
		for ( int i = 0; i < waveScheduleSpace.dim(ISLDimType.isl_dim_in) ; i++ ) {
			if ( i == outerLevelBand )
				continue;
			int outDim = (i < outerLevelBand) ? i: i+1;
			ISLConstraint res = generateEqualityConstraintOnMap(waveScheduleSpace, i, outDim);
			wavefrontMap = wavefrontMap.addConstraint(res);
		}
		if ( debug )
			System.out.println("OuterBlockcyclic Transform " + wavefrontMap);
		
		waveScheduleSpace.free();
		return wavefrontMap;
	}

	/* Generates a schedule to scan tiles in a wavefront order 
	 * outerLevelBand, innerLevelBand are the outer and inner levels of the tile band
	 * ScheduleSpace is the space on which tiling has to be applied
	 */
	private static ISLBasicMap generateWavefrontSchedule(int outerLevelBand,
			int innerLevelBand, ISLSpace scheduleSpace) {
		
		int nbScheduleDims = (int) scheduleSpace.dim(ISLDimType.isl_dim_out);
		int nbParams = (int) scheduleSpace.dim(ISLDimType.isl_dim_param);
		int nbTileDims = (innerLevelBand - outerLevelBand + 1);
		
		/*Intialize the new schedule space */
		ISLSpace waveScheduleSpace = initScheduleSpace(scheduleSpace, nbParams, nbScheduleDims+nbTileDims, nbScheduleDims+nbTileDims);
		
		ISLBasicMap wavefrontMap = ISLBasicMap.buildUniverse(waveScheduleSpace.copy());
		
		/* Add the constraint to skew the outer loop for ex: [i, j, k] -> [i+j+k, j, k] */
		ISLConstraint skew = ISLConstraint.buildEquality(waveScheduleSpace.copy().toLocalSpace());
		skew = skew.setCoefficient(ISLDimType.isl_dim_out, outerLevelBand, -1);
		for ( int i = outerLevelBand; i <= innerLevelBand; i++ ) {
			skew = skew.setCoefficient(ISLDimType.isl_dim_in, i, 1);
		}
		wavefrontMap = wavefrontMap.addConstraint(skew);
		for ( int i = 0; i < nbScheduleDims + nbTileDims; i++ ) {
			if ( i == outerLevelBand )
				continue;
			ISLConstraint res = generateEqualityConstraintOnMap(waveScheduleSpace, i, i);
			wavefrontMap = wavefrontMap.addConstraint(res);
		}
		if ( debug )
			System.out.println("Wavefront Transform " + wavefrontMap);
		
		waveScheduleSpace.free();
		return wavefrontMap;
	}

	/* getTileSchedule returns a Map that defines the schedule for tiling in the given schedule space 
	 * plutoSchedule - is the original schedule union map
	 * outerLevelBand - is starting level at which the tile band begins 
	 * innerLevelBand - is the inner level till which tiling is performed
	 * tileSize - array of tile sizes for each level from outerLevel to innerLevel. 
	 * tileSize[0] contains tiling for outerLevel
	 * scanOrder - specifies how the tiles should be scanned 
	 * 0 - generates simple tiled loop
	 * 1 - generates tiled loop such that tiles are visited in wavefront order
	 * 2 - block cyclic allocation on the outermost loop. Basically srtipmines the outerloop
	 * by the specified constant
	 * numProcessors - Only used when scanOrder is 2. specifies the constant to sripmine
	 * the outermost loop
	 * This function generates the schedule in the space: 
	 * 		{ [i...OuterLevel..innerLevel..j] -> 
	 *  		[i'...OuterTileLevel...innerTileLevel, OuterPointLevel...innerPointLevel...j'] }
	 *  Constraints:
	 *   	i' = i, j' = j, 
	 *  	OuterPointLevel = OuterLevel, OuterLevel * tileSize <= OuterPointLebel < (OuterLevel + 1) * tileSize
	 *  For example: 
	 *   (i, j, k) -> (i', j', k', j'', k'') : i = i' & j'' = j && k'' = k && 8*k' <= k'' < 8*(k'+1)  
	 */
	public static ISLMap getTileSchedule(ISLSpace scheduleSpace, int outerLevelBand, int innerLevelBand, int[] tileSize, int scanOrder, int numProcessors){
	
		//init tiled schedule space
		int nbScheduleDims = scheduleSpace.dim(ISLDimType.isl_dim_out);
		int nbParams = scheduleSpace.dim(ISLDimType.isl_dim_param);
		int nbTileDims = (innerLevelBand - outerLevelBand + 1);
		
		ISLSpace tiledScheduleSpace = initScheduleSpace(scheduleSpace,
				nbParams, nbScheduleDims, nbTileDims+nbScheduleDims);
		ISLSpace wrapSpace = tiledScheduleSpace.copy().wrap();
		
		ISLBasicSet tileSet = ISLBasicSet.buildUniverse(wrapSpace.copy());
		
		/* Add Identity schedule for all dimensions not in the band 
		 * [i,j] -> [i', j'] : i = i' 
		 * */
		for ( int i = 0; i < outerLevelBand; i++ ) {
			ISLConstraint res = generateEqualityConstraint(wrapSpace, i, nbScheduleDims + i);
			tileSet = tileSet.addConstraint(res);
		}
		for ( int i = innerLevelBand + 1; i < nbScheduleDims; i++ ) {
			ISLConstraint res = generateEqualityConstraint(wrapSpace, i, nbScheduleDims + nbTileDims + i);
			tileSet = tileSet.addConstraint(res);
		}
		
		/* Add tiled schedule for tile band */
		for ( int i = outerLevelBand; i <= innerLevelBand; i++) {
			/* [i, j] -> [i, j', j''] */
			/*j' >= 0 */
			ISLConstraint tlb = ISLConstraint.buildInequality(wrapSpace.copy().toLocalSpace());
			tlb = tlb.setCoefficient(ISLDimType.isl_dim_set, i + nbScheduleDims, 1);
			tileSet = tileSet.addConstraint(tlb); 
			/* constraints for point loop */
			/* j'' >= 8 * j' */
			ISLConstraint plb = ISLConstraint.buildInequality(wrapSpace.copy().toLocalSpace());
			plb = plb.setCoefficient(ISLDimType.isl_dim_set, i + nbScheduleDims + nbTileDims, 1);
			plb = plb.setCoefficient(ISLDimType.isl_dim_set, i + nbScheduleDims, -1 * tileSize[i-outerLevelBand]);
			tileSet = tileSet.addConstraint(plb);
			/* j'' <= 8 * j' + 7 */
			ISLConstraint pub = ISLConstraint.buildInequality(wrapSpace.copy().toLocalSpace());
			pub = pub.setCoefficient(ISLDimType.isl_dim_set, i + nbScheduleDims + nbTileDims, -1);
			pub = pub.setCoefficient(ISLDimType.isl_dim_set, i + nbScheduleDims, tileSize[i-outerLevelBand]);
			pub = pub.setConstant(tileSize[i-outerLevelBand]-1);
			tileSet = tileSet.addConstraint(pub);
			/* j = j'' */
			ISLConstraint peq = generateEqualityConstraint(wrapSpace, i, nbScheduleDims + nbTileDims + i);
			tileSet = tileSet.addConstraint(peq); 
		}
		
		ISLBasicMap bmap = tileSet.unwrap();
		if ( debug )
			System.out.println("Tiling Function : "+bmap);
		
		/* Apply a schedule to visit the tiles in chosen order. In a wavefront schedule  the tiles
		 * are visited in a systolic wavefront order. In the outerBlock schedule The outermost loop
		 * is stripmined. This will help generating code for multi processor systems. 
		 */
		if ( scanOrder == waveFront ) {
			ISLBasicMap wavefrontMap = generateWavefrontSchedule(outerLevelBand, innerLevelBand, scheduleSpace);
			ISLBasicMap outerBlock = generateOuterBlockSchedule(outerLevelBand+1, numProcessors, tiledScheduleSpace);
			wavefrontMap = wavefrontMap.copy().applyRange(outerBlock.copy());
			bmap = bmap.applyRange(wavefrontMap);
		} else if ( scanOrder == outerBlock ) {
			ISLBasicMap wavefrontMap = generateOuterBlockSchedule(outerLevelBand, numProcessors, tiledScheduleSpace);
			bmap = bmap.applyRange(wavefrontMap);
		}
		ISLMap tilingFunction = bmap.toMap();
		if ( debug )
			System.out.println("Tiling Function : "+tilingFunction);
		
		return tilingFunction;
	}

	/* tileBand tiles a band of loops. 
	 * plutoSchedule - is the original schedule union map
	 * outerLevelBand - is starting level at which the tile band begins 
	 * innerLevelBand - is the inner level till which tiling is performed
	 * tileSize - array of tile sizes for each level from outerLevel to innerLevel. 
	 * tileSize[0] contains tiling for outerLevel
	 * scanOrder - specifies how the tiles should be scanned 
	 * 0 - generates simple tiled loop
	 * 1 - generates tiled loop such that tiles are visited in wavefront order
	 * 2 - block cyclic allocation on the outermost loop. Basically srtipmines the outerloop
	 * by the specified constant
	 * numProcessors - Only used when scanOrder is 2. specifies the constant to sripmine
	 * the outermost loop
	 */
	public static ISLUnionMap tileBand(ISLUnionMap plutoSchedule, int outerLevelBand, int innerLevelBand, int[] tileSize, int scanOrder, int numProcessors){
		ISLSpace scheduleSpace = plutoSchedule.getMapAt(0).getSpace();
		int nbScheduleDims = (int) scheduleSpace.dim(ISLDimType.isl_dim_out);
		int nbParams = (int) scheduleSpace.dim(ISLDimType.isl_dim_param);
		
		ISLMap tilingFunction = getTileSchedule(scheduleSpace, outerLevelBand, innerLevelBand, tileSize, scanOrder, numProcessors);

		//apply tiling function to schedule
		ISLUnionMap res = ISLMap.buildEmpty(plutoSchedule.getSpace()).toUnionMap();
		for (ISLMap map : plutoSchedule.getMaps()) {
			if (map.getNbOutputs() != nbScheduleDims) throw new RuntimeException();
			if (map.getNbParams() != nbParams) throw new RuntimeException();
			String s = map.getOutputTupleName();
			if ( s == null ) {
				s = map.getInputTupleName();
			}
			ISLMap tileScheduleFunction = map.applyRange(tilingFunction.copy());
			if ( s != null ) 
				tileScheduleFunction = tileScheduleFunction.setOutputTupleName(s);
			ISLUnionMap tmpUMap = tileScheduleFunction.toUnionMap();
			res = res.union(tmpUMap);
		}
		tilingFunction.free();
		return res;
	}
	
	public static ISLMap stripmine(ISLMap map, int tileSize){

		//init tiled schedule space
		ISLSpace scheduleSpace = map.getSpace();
		int nbScheduleDims = (int) scheduleSpace.dim(ISLDimType.isl_dim_out);
		int nbParams = (int) scheduleSpace.dim(ISLDimType.isl_dim_param);
		ISLSpace tiledScheduleSpace = ISLSpace.alloc(nbParams, nbScheduleDims, nbScheduleDims+1);
		String tupleName = scheduleSpace.getTupleName(ISLDimType.isl_dim_out);
		if (tupleName != null) {
			tiledScheduleSpace = tiledScheduleSpace.setTupleName(ISLDimType.isl_dim_in, tupleName);
			tiledScheduleSpace = tiledScheduleSpace.setTupleName(ISLDimType.isl_dim_out, tupleName);
		}
		for (int i = 0; i < nbParams; i++) {
			tiledScheduleSpace = tiledScheduleSpace.setDimName(ISLDimType.isl_dim_param, i, scheduleSpace.getDimName(ISLDimType.isl_dim_param, i));
		}
				
		ISLMap lexEQFirst = ISLMap.lexEQFirst(tiledScheduleSpace, nbScheduleDims-1);
				
		if (lexEQFirst.getNbBasicMaps() != 1) throw new RuntimeException();
		ISLBasicMap eq = lexEQFirst.getBasicMapAt(0);
		
		eq = eq.addConstraint(genConstraint1(tiledScheduleSpace, nbScheduleDims-1, nbScheduleDims-1, nbScheduleDims, tileSize));
		eq = eq.addConstraint(genConstraint2(tiledScheduleSpace, nbScheduleDims-1, nbScheduleDims-1, nbScheduleDims, tileSize));
		eq = eq.addConstraint(genConstraint3(tiledScheduleSpace, nbScheduleDims-1, nbScheduleDims-1, nbScheduleDims, tileSize));
		
		ISLMap tilingFunction = eq.toMap();
				
		//apply tiling function to schedule
		ISLMap res = map.applyRange(tilingFunction.copy()); 
		return res;
	}
	
	
}



