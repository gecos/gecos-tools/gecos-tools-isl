 package fr.irisa.cairn.jnimap.isl.extra;

import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLConstraint;
import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;

//XXX under construction. We still need to consider read and write accesses 
public class JNIISLFlowComputation {
	
	public static ISLUnionSet computeDomainOfProcess(ISLUnionMap tile, int processTileLevel, int pid) {
		return computeDomainOfProcess(tile.copy().getRange(), processTileLevel, pid);
	}
	
	public static ISLUnionSet computeDomainOfProcess(ISLUnionSet domain, int processTileLevel, int pid) {
		ISLUnionSet domainSet = ISLUnionSet.buildEmpty(domain.copy().getSpace());
		for (ISLSet s : domain.getSets() ) {
			ISLSet ns = ISLSet.buildEmpty(s.getSpace());
			for ( ISLBasicSet bs : s.getBasicSets() ) {
				ISLConstraint cons = ISLConstraint.buildEquality(bs.getSpace());
				cons = cons.setCoefficient(ISLDimType.isl_dim_set, processTileLevel, -1);
				cons = cons.setConstant(pid);
				bs = bs.addConstraint(cons.copy());
				ns = ns.union(bs.toSet());
			}
			domainSet = domainSet.addSet(ns);
		}
		domainSet = domainSet.coalesce();
		return domainSet;
	}

	public static ISLUnionMap computeFlowOutSet(ISLUnionMap tile, ISLUnionMap prdg, int processorTileLevel, 
			int sourcePid, int destPid,	int numProcesses) {
			
		/*compute the set of writes at the Source Actor */
		ISLUnionSet domainSet =  computeDomainOfProcess(tile.copy(), processorTileLevel, sourcePid);
		ISLUnionMap WriteSet = prdg.copy().intersectRange(domainSet.copy());
		
		/* Get the domain of read operation in the Destination Actor */
		ISLUnionSet domainRead = computeDomainOfProcess(tile.copy(), processorTileLevel, destPid);
		ISLUnionMap readSet = WriteSet.copy().intersectDomain(domainRead.copy());
		
		ISLUnionMap flowOut = readSet.copy();
		
		return flowOut;
	}
	
	
	public static ISLUnionMap computeFlowInSet(ISLUnionMap tile, ISLUnionMap prdg, int processorTileLevel, int sourcePid, 
			int destPid, int numProcesses) {
		
		/* Get the domain of read operation in the Destination Actor */
		ISLUnionSet domainRead = computeDomainOfProcess(tile.copy(), processorTileLevel, destPid);
		ISLUnionMap readSet = prdg.copy().intersectDomain(domainRead.copy());
		
		/*compute the set of writes at the Source Actor */
		ISLUnionSet domainSet =  computeDomainOfProcess(tile.copy(), processorTileLevel, sourcePid);
		ISLUnionMap WriteSet = prdg.copy().intersectRange(domainSet.copy());
		
		ISLUnionMap flowInSet = WriteSet.copy().intersect(readSet.copy());
		
		return flowInSet;
	} 
	
}