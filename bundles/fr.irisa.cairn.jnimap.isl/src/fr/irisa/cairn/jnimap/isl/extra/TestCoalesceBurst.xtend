package fr.irisa.cairn.jnimap.isl.jni.extra

import fr.irisa.cairn.jnimap.isl.jni.ISLFactory

class TestCoalesceBurst {
	
	
		def static void main(String[] args) {
			
		val mapping  = ISLFactory.islMap('''[]->{ [i,j] -> [x,i,j] : x= 256i+j}''')
		val symDomain =ISLFactory.islSet('''[]->{ [i,j] : 0<=j<256 and 0<=i<16}''')
		val domain =ISLFactory.islSet('''[]->{ [i,j] : 32<=j<48and 4<=i<8}''')
	
		val regionMapping = mapping.copy.intersectDomain(domain.copy)

		val nextReadAddress= regionMapping.copy.getRange.lexNextMap(0).toPWMultiAff
		
		val complement =  domain.copy.complement
		val unreadRegionMapping = mapping.copy.intersectDomain(complement.copy)
		val nextUnreadAddress= unreadRegionMapping.copy.getRange.lexNextMap(0).toPWMultiAff

		
		for (start: nextReadAddress.pieces) {
			for (end: nextUnreadAddress.pieces) {
				val dom = start.set.copy.intersect(end.set.copy)
				if (!(dom.isEmpty)) {
					println("burst from "+start.maff+" to "+end.maff)
				}
			}
		}	
		
	}
	
}