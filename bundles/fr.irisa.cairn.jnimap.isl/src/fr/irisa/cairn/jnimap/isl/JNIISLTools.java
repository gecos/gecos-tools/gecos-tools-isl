package fr.irisa.cairn.jnimap.isl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import fr.irisa.cairn.jnimap.isl.ISLAff;
import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLSpace;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;

public class JNIISLTools {

	/**
	 * Wraps a block of code executing ISL operations with calls to recordStart and
	 * End. The record methods are used to capture ISL outputs to stderr. Calling
	 * ISL operations through this wrapper allows the exceptions to be thrown with
	 * meaningful error messages from ISL.
	 * 
	 * @param r
	 * @param f
	 * @throws ISLErrorException
	 */
	public static void recordError(Runnable r, Function<String, ISLErrorException> f) throws ISLErrorException {
		try {
			ISLContext.recordStderrStart();
			r.run();
		} catch (UnsatisfiedLinkError ule) {
			ule.printStackTrace();
			System.err.println("Use of JNIISLContext in finally clause fails due to LinkError");
			throw ule;
		} catch (RuntimeException re) {
			String err = ISLContext.recordStderrEnd();
			//If ISL did not output any error, then it could be some error in the Java side.
			if (err == null || err.length() == 0) throw re;
			throw f.apply(err);
		} finally {
			ISLContext.recordStderrEnd();
		}
	}

	public static void recordError(Runnable r) throws ISLErrorException {
		recordError(r, (err) -> new ISLErrorException(err.trim()));
	}

	/**
	 * Variant of recordError for operations that return a value in the end. (Supplier instead of Runnable)
	 * 
	 * @param r
	 * @param f
	 * @return
	 * @throws ISLErrorException
	 */
	public static <T> T recordError(Supplier<T> r, Function<String, ISLErrorException> f) throws ISLErrorException {
		try {
			ISLContext.recordStderrStart();
			T res = r.get();
			return res;
		} catch (UnsatisfiedLinkError ule) {
			ule.printStackTrace();
			System.err.println("Use of JNIISLContext in finally clause fails due to LinkError");
			throw ule;
		} catch (RuntimeException re) {
			String err = ISLContext.recordStderrEnd();
			//If ISL did not output any error, then it could be some error in the Java side.
			if (err == null || err.length() == 0) throw re;
			throw f.apply(err);
		} finally {
			ISLContext.recordStderrEnd();
		}
	}

	public static <T> T recordError(Supplier<T> r) throws ISLErrorException {
		return recordError(r, (err) -> new ISLErrorException(err.trim()));
	}

	public static void test(ISLSet x) {
		JNIISLTools.<ISLSet>recordError(() -> (x));
	}

	// public static JNIISLMap buildRenameMapString(List<String> newIndicesNames,
	// String tupleName, List<String> parametersNames,
	// List<String> indicesName) {
	// int size = indicesName.size();
	// JNIISLMap islMap;
	// if (size != newIndicesNames.size()) {
	// throw new UnsupportedOperationException("Not yet implemented");
	// } else {
	// String map_next_string = parametersNames + " -> { " + tupleName
	// + indicesName + " -> " + tupleName + newIndicesNames
	// + " : ";
	// for (int j = 0; j < size; j++) {
	// if (j > 0) {
	// map_next_string += " & ";
	// }
	// map_next_string += newIndicesNames.get(j) + " = "
	// + indicesName.get(j);
	// }
	// islMap = ISLFactory.islMap(map_next_string + " }");
	// }
	// return islMap;
	// }
	//
	// public static JNIISLMap renameDimensions(JNIISLMap map, JNIISLDimType type,
	// List<String> names) {
	// List<String> parametersNames = map.getParametersNames();
	// switch (type.getValue()) {
	// case JNIISLDimType.ISL_DIM_IN:
	// String tupleName = map.getTupleName(JNIISLDimType.isl_dim_in);
	//
	// List<String> indicesName = map.getDomainNames();
	// JNIISLMap buildRenameMapString = buildRenameMapString(names,
	// tupleName, parametersNames, indicesName);
	//
	// return JNIISLMap.applyDomain(map.copy(), buildRenameMapString);
	//
	// case JNIISLDimType.ISL_DIM_OUT:
	// tupleName = map.getTupleName(JNIISLDimType.isl_dim_out);
	//
	// indicesName = map.getRangeNames();
	// buildRenameMapString = buildRenameMapString(names, tupleName,
	// parametersNames, indicesName);
	// return JNIISLMap.applyRange(map.copy(), buildRenameMapString);
	// default:
	// throw new UnsupportedOperationException("Not yet implemented");
	//
	// }
	// }
	//
	// public static JNIISLSet renameDimensions(JNIISLSet set, List<String> names) {
	// List<String> parametersNames = set.getParametersNames();
	// String tupleName = set.getTupleName();
	//
	// List<String> indicesName = set.getIndicesNames();
	// JNIISLMap buildRenameMapString = buildRenameMapString(names, tupleName,
	// parametersNames, indicesName);
	// return set.copy().apply(buildRenameMapString);
	// }
	//
	// public static JNIISLMap identity(JNIISLMap map) {
	//
	// List<String> domainNames = map.getDomainNames();
	// int size = domainNames.size();
	// JNIISLMap islMap;
	// List<String> rangeNames = map.getRangeNames();
	// if (size != rangeNames.size()) {
	// throw new UnsupportedOperationException("Not yet implemented");
	// } else {
	// List<String> parametersNames = map.getParametersNames();
	// String inTupleName = map.getTupleName(JNIISLDimType.isl_dim_in);
	// String outTupleName = map.getTupleName(JNIISLDimType.isl_dim_out);
	// String map_next_string = parametersNames + " -> { " + inTupleName
	// + domainNames + " -> " + outTupleName + rangeNames + " : ";
	// for (int j = 0; j < size; j++) {
	// if (j > 0) {
	// map_next_string += " & ";
	// }
	// map_next_string += rangeNames.get(j) + " = "
	// + domainNames.get(j);
	// }
	// islMap = ISLFactory.islMap(map_next_string + " }");
	// }
	// return islMap;
	// }

	// public static JNIISLMap shift(JNIISLSet set, int dimId, int offset) {

	// JNIISLMap empty = JNIISLMap.fromDomainAndRange(set.copy(), set.copy());
	// dim.getNameList(JNIISLDimType.isl_dim_in);
	// for(String index : dim.getNameList(JNIISLDimType.isl_dim_in)) {
	//
	// }
	// return empty;
	// throw new UnsupportedOperationException();
	// }
	//
	// /**
	// * Simply add dimensions, without changing current ones.
	// *
	// * @param set
	// * @param dims
	// * @return
	// */
	// public static JNIISLSet extendDimsWith(JNIISLSet set, List<String> dims) {
	// if (dims.isEmpty())
	// return set.copy();
	// set = set.copy().makeDisjoint();
	// JNIISLSpace dimsOrig = set.getSpace();
	// JNIISLSet res = null;
	// int nbBset = set.getNumberOfBasicSet();
	// for (int i = 0; i < nbBset; i++) {
	// JNIISLBasicSet bs = set.getBasicSetAt(i);
	// String base = "[";
	// // params
	// List<String> parametersNames = bs.getParametersNames();
	// for (int k = 0; k < parametersNames.size(); k++) {
	// if (k > 0)
	// base += ",";
	// base += parametersNames.get(k);
	// }
	// String tupleName = set.getTupleName();
	// if (tupleName == null)
	// tupleName = "";
	// base += "] -> { " + tupleName + "[";
	// // domain's current dimensions
	// List<String> current = bs.getIndicesNames();
	// boolean first = true;
	//
	// for (int j = 0; j < dimsOrig.getSize(JNIISLDimType.isl_dim_set); j++) {
	// if (first)
	// first = false;
	// else
	// base += ",";
	// base += (current.get(j) == null) ? "i" + j : current.get(j);
	// }
	//
	// for (String s : dims) {
	// if (first)
	// first = false;
	// else
	// base += ",";
	// base += s;
	// }
	// base += "] : ";
	// String pa = bs.toString(ISL_FORMAT.ISL);
	// pa = pa.substring(pa.indexOf(":") + 1).replace("}", "").trim();
	// String bstring = base + pa + "}";
	//
	// JNIISLSet s = ISLFactory.islSet(bstring);
	// if (res == null)
	// res = s;
	// else
	// res = JNIISLSet.union(res, s);
	// }
	// return res;
	// }
	//
	// /**
	// * Simply add dimensions, without changing current ones.
	// *
	// * @param set
	// * @param dims
	// * @return
	// */
	// public static JNIISLSet renameIndices(JNIISLSet set, List<String> dims) {
	// set = set.copy().makeDisjoint();
	// JNIISLSpace dimsOrig = set.getSpace();
	//
	// List<String> origNames = dimsOrig
	// .getNameList(JNIISLDimType.isl_dim_set);
	//
	// if (origNames.size() != dims.size()) {
	// throw new RuntimeException();
	// }
	//
	// String base = "";
	// // params
	// base += set.getParametersNames().toString();
	//
	// String tupleName = set.getTupleName();
	// if (tupleName == null)
	// tupleName = "";
	// base += " -> { " + tupleName + "[";
	//
	// // domain's current dimensions
	// List<String> current = set.getIndicesNames();
	// boolean first = true;
	// List<String> dimIn = new ArrayList<String>();
	// for (int j = 0; j < dimsOrig.getSize(JNIISLDimType.isl_dim_set); j++) {
	// if (first)
	// first = false;
	// else
	// base += ",";
	// String varName = (current.get(j) == null) ? "i" + j : current
	// .get(j);
	// dimIn.add(varName);
	// base += varName;
	// }
	// base += "] -> " + tupleName + "[";
	// // new dimensions
	// first = true;
	// for (String s : dims) {
	// if (first)
	// first = false;
	// else
	// base += ",";
	// base += s;
	// }
	// base += "] : ";
	// first = true;
	// for (int i = 0; i < dimIn.size(); i++) {
	// if (first)
	// first = false;
	// else
	// base += " and ";
	// base += dimIn.get(i) + " = " + dims.get(i);
	// }
	// String bstring = base + "}";
	//
	// System.out.println(bstring);
	// JNIISLMap s = ISLFactory.islMap(bstring);
	//
	// JNIISLSet res = set.copy().apply(s);
	//
	// res = res.coalesce();
	// return res;
	// }

	/**
	 * This method infer the parameter context domain of a set, that is the
	 * parameter values that ensure that the set will contain at least one point.
	 * 
	 * @param set
	 * @return
	 */
	public static ISLSet inferParameterContextDomain(ISLSet set) {
		ISLSet contextSet = set.copy();

		int nDim = set.getNbIndices();
		if (nDim > 0)
			contextSet = contextSet.projectOut(ISLDimType.isl_dim_set, 0, nDim);
		else {
			ISLMap identity = ISLMap.buildIdentity(contextSet.copy().getSpace()).reverse();
			contextSet = contextSet.apply(identity);
		}

		// JNIISLSet lexMin = set.copy().lexMin();
		// long nbDims = set.getDimensions().getSize(JNIISLDimType.isl_dim_set);
		// JNIISLSet contextSet =
		// lexMin.copy().projectOut(JNIISLDimType.isl_dim_set, 0,
		// nbDims).coalesce();
		// contextSet = contextSet.setTupleName(set.getTupleName());
		// contextSet = JNIISLTools.extendDimsWith(contextSet,
		// set.getIndicesNames());
		// contextSet = contextSet.projectOut(JNIISLDimType.isl_dim_set, 0,
		// contextSet.getNbDims());
		return contextSet;
	}

	public static ISLSet inferParameterContextDomain(ISLUnionSet uset) {
		ISLSet res = null;

		for (ISLSet set : uset.getSets()) {
			ISLSet paramCtx = inferParameterContextDomain(set);
			if (res == null)
				res = paramCtx;
			else
				res = res.union(paramCtx);
		}
		res = res.coalesce();
		return res;
	}

	/**
	 * This methods expands the dimension of input domain to match those of target
	 * domain, and then does return the intersection of the two "aligned domains".
	 * 
	 * @param input
	 * @param target
	 * @return
	 */
	public static ISLSet expandTo(ISLSet input, ISLSet target) {
		int nExtraDims = target.getNbIndices() - input.getNbIndices();
		ISLSet expandedSet = input.copy().insertDims(ISLDimType.isl_dim_set, input.getNbIndices(), nExtraDims);
		ISLMap map = ISLMap.buildFromDomainAndRange(expandedSet.copy(), target.copy());
		map = ISLMap.buildIdentity(map.getSpace().copy());
		expandedSet = expandedSet.apply(map.copy());
		expandedSet = expandedSet.intersect(target.copy());
		return expandedSet;
	}

	public static ISLBasicSet expandTo(ISLBasicSet input, ISLBasicSet target) {
		ISLSet inputSet = input.toSet();
		ISLSet targetSet = target.toSet();
		ISLSet expandedSet = expandTo(inputSet, targetSet);
		if (expandedSet.getNbBasicSets() != 1)
			throw new RuntimeException();
		ISLBasicSet expanded = expandedSet.getBasicSetAt(0);
		return expanded;
	}

	public static ISLSet renameTo(ISLSet input, ISLSet target) {
		long nExtraDims = target.copy().getNbIndices() - input.copy().getNbIndices();
		if (nExtraDims != 0) {
			throw new UnsupportedOperationException("Not yet implemented");
		}
		ISLSet renamed = input.copy();
		ISLMap map = ISLMap.buildFromDomainAndRange(renamed.copy(), target.copy());
		map = ISLMap.buildIdentity(map.getSpace().copy());
		renamed = renamed.apply(map.copy());
		return renamed;
	}

	public static ISLMap renameRangeTo(ISLMap input, ISLSpace targetDim) {
		ISLSet target = ISLSet.buildEmpty(targetDim);
		ISLSet range = input.copy().getRange();
		long nExtraDims = target.copy().getNbIndices() - range.copy().getNbIndices();
		if (nExtraDims != 0) {
			throw new UnsupportedOperationException("Not yet implemented");
		}
		ISLMap map = ISLMap.buildFromDomainAndRange(range.copy(), target.copy());
		map = ISLMap.buildIdentity(map.getSpace().copy());
		map = input.copy().applyRange(map.copy());
		return map;
	}

	public static boolean allDisjointBasic(Collection<ISLBasicSet> sets) {
		Iterator<ISLBasicSet> iterator = sets.iterator();
		Collection<ISLSet> tmp = new ArrayList<ISLSet>(sets.size());
		while (iterator.hasNext()) {
			tmp.add(iterator.next().copy().toSet());
		}
		return allDisjoint(tmp);
	}

	public static boolean allDisjoint(Collection<ISLSet> sets) {
		Iterator<ISLSet> iterator = sets.iterator();
		if (!iterator.hasNext())
			return true;
		ISLSet init = iterator.next();
		ISLSet set = init.copy();
		boolean result = true;
		while (iterator.hasNext()) {
			ISLSet bset = iterator.next();
			if (!bset.getSpace().isEqual(set.getSpace())) {
				result = false;
				break;
			}
			ISLSet set2 = bset;
			ISLSet tmp = set.copy().intersect(set2.copy());
			if (!tmp.isEmpty()) {
				result = false;
				break;
			}
			set = set.union(set2);
		}
		return result;
	}

	/**
	 * this method returns this list of dimension's indices of the range for which
	 * the dimension is always a scalar, whatever the set (of the range).
	 * 
	 * @param umap
	 * @return
	 */
	public static List<Integer> scalarDims(ISLUnionMap umap) {
		throw new UnsupportedOperationException("To reimplement.");
		// if (umap.isEmpty()==0) {
		// JNIISLUnionSet scheduleRange = umap.copy().getRange().coalesce();
		// //all the statements should be scheduled in the same space
		// if (scheduleRange.getNbSet() != 1) throw new RuntimeException();
		// JNIISLSet scheduledDomain = scheduleRange.getSetAt(0);
		// int nbDims = (int) scheduledDomain.getNbDims();
		// //the number of potential scalar dimensions is not greater
		// //than the number of dimensions of the scheduled domain.
		// List<Integer> res = new ArrayList<Integer>(nbDims);
		// for (int i = 0; i < nbDims; i++) {
		// res.add(i);
		// }
		//
		// for (JNIISLMap map : umap.getMaps()) {
		// //the number of schedule per statement should be
		// //exactly one.
		// if (map.getNumberOfBasicMap() != 1) throw new RuntimeException();
		//
		// Map<JNIISLBasicSet, JNIISLAffList> m =
		// map.getBasicMapAt(0).lexmaxV2();
		// //schedule for one given statement should be bijective.
		// if (m.size() != 1) throw new RuntimeException();
		//
		// JNIISLAffList l = m.entrySet().iterator().next().getValue();
		// //schedule function should have the same number of dimensions
		// //as it's scheduled destination domain.
		// if (l.getSize() != nbDims) throw new RuntimeException();
		//
		// for (int i = 0; i < l.getSize(); i++) {
		// //if the current dimension is already known as non-
		// //scalar, skip it.
		// if (res.indexOf(i) == -1) continue;
		//
		// JNIISLAff aff = l.getAffAt(i);
		// if (!isConstant(aff)) res.remove(res.indexOf(i));
		// }
		// }
		//
		// return res;
		// } else {
		// return new ArrayList<Integer>(0);
		// }
	}

	/**
	 * This method returns true if the expression is a constant.
	 * 
	 * @param aff
	 * @return
	 */
	public static boolean isConstant(ISLAff aff) {
		ISLDimType dimType;

		dimType = ISLDimType.isl_dim_out;
		for (int i = 0; i < aff.dim(dimType); i++) {

			if (aff.getCoefficientVal(dimType, i).asLong() != 0)
				return false;
		}

		dimType = ISLDimType.isl_dim_in;
		for (int i = 0; i < aff.dim(dimType); i++) {
			if (aff.getCoefficientVal(dimType, i).asLong() != 0)
				return false;
		}

		dimType = ISLDimType.isl_dim_param;
		for (int i = 0; i < aff.dim(dimType); i++) {
			if (aff.getCoefficientVal(dimType, i).asLong() != 0)
				return false;
		}

		// we assume that if a div is a constant, then it has been
		// simplified.
		// XXX : should we assume that?
		dimType = ISLDimType.isl_dim_div;
		for (int i = 0; i < aff.dim(dimType); i++) {
			if (aff.getCoefficientVal(dimType, i).asLong() != 0)
				return false;
		}

		return true;
	}

	/**
	 * Build an ISL set with the variables of another set added at the end of its
	 * indices space.
	 * 
	 * @param set
	 *            domain to normalize
	 * @param other
	 *            another domain
	 * @return
	 */
	public static ISLSet normalize(ISLSet set, ISLSet other) {
		List<String> set1Indices = set.getIndexNames();
		List<String> set2indices = other.getIndexNames();

		Collection<String> normalizedIndices = new LinkedHashSet<String>();
		normalizedIndices.addAll(set1Indices);
		normalizedIndices.addAll(set2indices);

		return normalize(set, normalizedIndices);
	}

	/**
	 * Build the normalized intersection of two polyhedral domains. First dimensions
	 * are those coming from set1.
	 * 
	 * @param set1
	 * @param set2
	 * @return
	 */
	public static ISLSet normalizedIntersection(ISLSet set1, ISLSet set2) {
		Collection<String> normalizedIndices = new LinkedHashSet<String>();
		normalizedIndices.addAll(set1.getIndexNames());
		normalizedIndices.addAll(set2.getIndexNames());

		Collection<String> normalizedParameters = new LinkedHashSet<String>();
		normalizedParameters.addAll(set1.getParamNames());
		normalizedParameters.addAll(set2.getParamNames());

		ISLSet nset1 = normalize(set1, normalizedIndices, normalizedParameters);
		ISLSet nset2 = normalize(set2, normalizedIndices, normalizedParameters);
		// System.out.println("\nJNIISLTools.normalizedIntersection(): start");
		ISLSet result = nset1.intersect(nset2);
		// System.out.println("JNIISLTools.normalizedIntersection(): end");
		return result;
	}

	public static ISLSet normalize(ISLSet set, Collection<String> dimensions, Collection<String> parameters) {
		return normalize(set, dimensions, parameters.toArray(new String[parameters.size()]));
	}

	/**
	 * Normalize an ISL set to given ordered set of dimensions.
	 * 
	 * @param set
	 * @param dimensions
	 * @return
	 */
	public static ISLSet normalize(ISLSet set, Collection<String> dimensions, String... parameters) {
		String newVariablesDeclaration = formatVariables(dimensions, parameters);
		String currentVariablesDeclaration = formatVariables(set.getIndexNames(),
				set.getParamNames().toArray(new String[(int) set.getNbParams()]));

		ISLSet newSet = null;
		for (ISLBasicSet currentBset : set.getBasicSets()) {
			String bsetString = currentBset.toString(ISL_FORMAT.ISL).replace(currentVariablesDeclaration,
					newVariablesDeclaration);
			ISLBasicSet bset = ISLFactory.islBasicSet(bsetString);
			if (newSet == null) {
				newSet = bset.toSet();
			} else {
				newSet = newSet.union(bset.toSet());
			}
		}
		return newSet;
	}

	private static String formatVariables(Collection<String> indices, String... parameters) {
		StringBuffer sb = new StringBuffer("[");
		if (parameters.length > 0) {
			for (int i = 0; i < parameters.length - 1; i++) {
				sb.append(parameters[i]).append(", ");
			}
			if (parameters.length > 0) {
				sb.append(parameters[parameters.length - 1]);
			}
			sb.append("]->{ [");
		}
		boolean first = true;
		for (String s : indices) {
			if (first)
				first = false;
			else
				sb.append(", ");
			sb.append(s);
		}
		sb.append("]");
		return sb.toString();
	}
}
