package fr.irisa.cairn.jnimap.isl;


/**
 * Interface to Dataflow Analysis in ISL (new version using isl_union_access_info).
 * The ISL interface assumes dataflow view of dependences. The JNI binding adds a 
 * wrapper {@link IISLDataflowResults} to provide dependence view, which is used by default.
 * 
 * The direction of allow is controlled by a flag in the singleton instance,
 * use {@link #setArrowDirectionToDataflow} to configure.
 * 
 * ISLUnionAccessInfo contains:
 *   - sink access (read accesses)
 *   - may source access (writes considered for may dependences)
 *   - kill access (accesses that suppresses all preceding writes to the same location)
 *   - must source access (+ automatically considered as kill)
 *  
 *  and gives
 *  
 *  ISLUnionFlow with:
 *   - NoDependence (iterations where its reads have no corresponding write)
 *   - Dependence (preceding writes to same location without any kill access in between)
 *   - FullDependence (wrapped relation that contains both dependences and accesses)
 *  for both May and Must variants.
 *  
 *  All computeDependence methods give an instance of {@link IISLDataflowResults}.
 *  All computeDependenceGrah methods give the PRDG (or dataflow graph) encoded as
 *  ISLUnionMap, where the must and may dependence are merged.
 * 
 * @author tyuki
 *
 */
public class ISLDataflowAnalysis {
	
	private boolean computeProducerToConsumer = false;
	
	private static final ISLDataflowAnalysis INSTANCE = new ISLDataflowAnalysis();
	
	private ISLDataflowAnalysis() {}
	
	public static void setArrowDirectionToDataflow(boolean producerToConsumer) {
		INSTANCE.computeProducerToConsumer = producerToConsumer;
	}
	
	/**
	 * Returns PRDG (or dataflow graph) as ISLUnionMap.
	 * Both may and must dependences are merged into a single map. If the distinction is important,
	 * use {@link #computeDependences} instead. 
	 * 
	 * @param sink
	 * @param mustSrc
	 * @param maySrc
	 * @param kill
	 * @param schedule
	 * @return
	 */
	public static ISLUnionMap computeDependenceGraph(ISLUnionMap sink, ISLUnionMap mustSrc, ISLUnionMap maySrc, ISLUnionMap kill, ISLUnionMap schedule) {
		IISLDataflowResults res = computeDependences(sink, mustSrc, maySrc, kill, schedule);
		return res.getMustDependence().union(res.getMayDependence());
	}
	
	/**
	 * Returns PRDG (or dataflow graph) as ISLUnionMap. 
	 * Uses {@link #computeValueBasedDependence}, and computes ISLUnionMap from its result. 
	 * 
	 * @param domain
	 * @param writes
	 * @param reads
	 * @param schedule
	 * @return
	 */
	public static ISLUnionMap computeValueBasedDependenceGraph(ISLUnionSet domain, ISLUnionMap writes, ISLUnionMap reads, ISLUnionMap schedule) {
		IISLDataflowResults res = computeValueBasedDependences(domain, writes, reads, schedule);
		return res.getMustDependence();
	}

	
	/**
	 * Returns PRDG (or dataflow graph) as ISLUnionMap. 
	 * Uses {@link #computeMemoryBasedDependenceGraph}, and computes ISLUnionMap from its result. 
	 * 
	 * @param domain
	 * @param writes
	 * @param reads
	 * @param schedule
	 * @return
	 */
	public static ISLUnionMap computeMemoryBasedDependenceGraph(ISLUnionSet domain, ISLUnionMap writes, ISLUnionMap reads, ISLUnionMap schedule) {
		IISLDataflowResults res = computeMemoryBasedDependences(domain, writes, reads, schedule);
		return res.getMustDependence().union(res.getMayDependence());
	}
	
	/**
	 * Provides the most direct interface to isl_union_access_info_compute_flow.
	 * 
	 * It is recommended to only use this method if a specific combination of must/may/kill accesses
	 * are needed for some special analysis. Otherwise {@link #computeValueBasedDependences} or 
	 * {@link #computeMemoryBasedDependences} should suffice.
	 * 
	 * @param sink
	 * @param mustSrc
	 * @param maySrc
	 * @param kill
	 * @param schedule
	 * @return
	 */
	public static IISLDataflowResults computeDependences(ISLUnionMap sink, ISLUnionMap mustSrc, ISLUnionMap maySrc, ISLUnionMap kill, ISLUnionMap schedule) {
		ISLUnionAccessInfo    uai = ISLUnionAccessInfo.buildFromSink(sink);
		if (mustSrc  !=null)  uai = uai.setMustSource(mustSrc);
		if (maySrc   !=null)  uai = uai.setMaySource(maySrc);
		if (kill  !=null)  uai = uai.setKill(kill);
		if (schedule != null) uai = uai.setScheduleMap(schedule);
		
		ISLUnionFlow flow = uai.computeFlow();
		
		if (INSTANCE.computeProducerToConsumer) {
			return flow;
		} else {
			return new ISLReversedUnionFlow(flow);
		}
	}

	/**
	 * Simplified interface to compute value-based dependences (flow/true dependences).
	 * All writes are assumed to be must accesses.
	 * 
	 * @param domain
	 * @param writes
	 * @param reads
	 * @param schedule
	 * @return
	 */
	public static IISLDataflowResults computeValueBasedDependences(ISLUnionSet domain, ISLUnionMap writes, ISLUnionMap reads, ISLUnionMap schedule) {
		ISLUnionAccessInfo uai = ISLUnionAccessInfo.buildFromSink(reads.intersectDomain(domain.copy()));
		uai = uai.setScheduleMap(schedule);
		uai = uai.setMustSource(writes.copy().intersectDomain(domain.copy()));
		
		ISLUnionFlow flow = uai.computeFlow();
		
		if (INSTANCE.computeProducerToConsumer) {
			return flow;
		} else {
			return new ISLReversedUnionFlow(flow);
		}
	}

	/**
	 * Simplified interface to compute memory-based dependences (flow/true + anti dependences).
	 * 
	 * @param domain
	 * @param writes
	 * @param reads
	 * @param schedule
	 * @return
	 */
	public static IISLDataflowResults computeMemoryBasedDependences(ISLUnionSet domain, ISLUnionMap writes, ISLUnionMap reads, ISLUnionMap schedule) {
		ISLUnionAccessInfo uai = ISLUnionAccessInfo.buildFromSink(reads.intersectDomain(domain.copy()));
		uai = uai.setScheduleMap(schedule);
		uai = uai.setMaySource(writes.copy().intersectDomain(domain.copy()));
		
		ISLUnionFlow flow = uai.computeFlow();
		
		if (INSTANCE.computeProducerToConsumer) {
			return flow;
		} else {
			return new ISLReversedUnionFlow(flow);
		}
	}
}
