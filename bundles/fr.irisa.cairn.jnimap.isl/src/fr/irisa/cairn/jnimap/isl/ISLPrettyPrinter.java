package fr.irisa.cairn.jnimap.isl;

public class ISLPrettyPrinter {

//	public static enum ISL_FORMAT {};

	
	//private static JNIISLContext context;

	public static ISLContext getContext() {
		return ISLContext.getInstance();
	}
	
	public static String asString(ISLVal v) {
		return ISLVal._toString(v, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLMultiVal mv) {
		return ISLMultiVal._toString(mv, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLValList vlist) {
		return ISLValList._toString(vlist, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLIdentifier id) {
		return ISLIdentifier._toString(id, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLIdentifierList idlist) {
		return ISLIdentifierList._toString(idlist, ISL_FORMAT.ISL.ordinal());
	}
	
	public static String asString(ISLSpace dim) {
		String tin = dim.getTupleName(ISLDimType.isl_dim_in);
		String tout = dim.getTupleName(ISLDimType.isl_dim_out);
		tin = tin==null?"":tin;
		tout = tout==null?"":tout;
		return ""+
			dim.getDimNames(ISLDimType.isl_dim_param)+" -> {"+
			tin+
			dim.getDimNames(ISLDimType.isl_dim_in)+" -> "+
			tout+
			dim.getDimNames(ISLDimType.isl_dim_out)+" :"
			;
	}

	public static String asString(ISLSpace space, ISL_FORMAT format) {
		return asString(space);
	}

	public static String asString(ISLLocalSpace space, ISL_FORMAT format) {
		return asString(space);
	}
	
	public static String asString(ISLSet set) {
		return ISLSet._toString(set, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLSet set, ISL_FORMAT format) {
		return ISLSet._toString(set, format.ordinal());
	}
	
	public static String asString(ISLBasicSet set) {
		return ISLBasicSet._toString(set, ISL_FORMAT.ISL.ordinal());
	}
	
	public static String asString(ISLBasicSet set, ISL_FORMAT format) {
		return ISLBasicSet._toString(set, format.ordinal());
	}

	public static String asString(ISLUnionSet jniislUnionSet) {
		return ISLUnionSet._toString(jniislUnionSet, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLUnionSet jniislUnionSet, ISL_FORMAT format) {
		return ISLUnionSet._toString(jniislUnionSet, format.ordinal());
	}

	public static String asString(ISLBasicSetList bsetlist) {
		return ISLBasicSetList._toString(bsetlist, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLSetList setlist) {
		return ISLSetList._toString(setlist, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLBasicMap set, ISL_FORMAT format) {
		return ISLBasicMap._toString(set, format.ordinal());
	}

	public static String asString(ISLBasicMap set) {
		return ISLBasicMap._toString(set, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLMap set) {
		return ISLMap._toString(set, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLMap set, ISL_FORMAT format) {
		return ISLMap._toString(set, format.ordinal());
	}

	public static String asString(ISLUnionMap set,ISL_FORMAT format) {
		return ISLUnionMap._toString(set, format.ordinal());
	}
	
	public static String asString(ISLUnionMap jniislUnionMap) {
		return ISLUnionMap._toString(jniislUnionMap, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLAff aff) {
		return ISLAff._toString(aff, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLAff aff, ISL_FORMAT format) {
		return ISLAff._toString(aff, format.ordinal());
	}

	public static String asString(ISLPWAff pa) {
		return ISLPWAff._toString(pa, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLPWAff pa, ISL_FORMAT format) {
		return ISLPWAff._toString(pa, format.ordinal());
	}
	
	public static String asString(ISLMultiAff maff) {
		return ISLMultiAff._toString(maff, ISL_FORMAT.ISL.ordinal());
	}
	
	public static String asString(ISLMultiAff maff, ISL_FORMAT format) {
		return ISLMultiAff._toString(maff, format.ordinal());
	}

	public static String asString(ISLPWMultiAff pma) {
		return ISLPWMultiAff._toString(pma, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLPWMultiAff pma, ISL_FORMAT format) {
		return ISLPWMultiAff._toString(pma, format.ordinal());
	}
	
	public static String asString(ISLMultiPWAff mpa) {
		return ISLMultiPWAff._toString(mpa, ISL_FORMAT.ISL.ordinal());
	}
	
	public static String asString(ISLMultiPWAff mpa, ISL_FORMAT format) {
		return ISLMultiPWAff._toString(mpa, format.ordinal());
	}

	public static String asString(ISLUnionPWAff upa) {
		return ISLUnionPWAff._toString(upa, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLUnionPWAff upa, ISL_FORMAT format) {
		return ISLUnionPWAff._toString(upa, format.ordinal());
	}

	public static String asString(ISLUnionPWMultiAff upma) {
		return ISLUnionPWMultiAff._toString(upma, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLUnionPWMultiAff upma, ISL_FORMAT format) {
		return ISLUnionPWMultiAff._toString(upma, format.ordinal());
	}

	public static String asString(ISLMultiUnionPWAff mupwa) {
		return ISLMultiUnionPWAff._toString(mupwa, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLMultiUnionPWAff mupwa, ISL_FORMAT format) {
		return ISLMultiUnionPWAff._toString(mupwa, format.ordinal());
	}

	public static String asString(ISLTerm set) {
		return "";//set.getN()+"/"+set.getD()+"^?";
	}

	public static String asString(ISLConstraint cstr) {
		return ISLConstraint._toString(cstr, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLPoint p) {
		return p.getCoordinates().toString();
	}
	
	public static String asString(ISLUnionAccessInfo access) {
		return ISLUnionAccessInfo._toString(access, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLUnionFlow flow) {
		return ISLUnionFlow._toString(flow, ISL_FORMAT.ISL.ordinal());
	}
	
	public static String asString(ISLMatrix m) {

		StringBuffer res = new StringBuffer();
		res.append(m.getNbRows()+"\n"+m.getNbCols()+"\n");
		for(int i=0;i<m.getNbRows();i++) {
			for(int j=0;j<m.getNbCols();j++) {
				long val = m.getElement(i,j);
				res.append(val+" ");
				
			}
			res.append("\n");
		}
		return res.toString();
		
	}

	public static String asString(ISLQPolynomial qp) {
		return ISLQPolynomial._toString(qp, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLQPolynomialFold set) {
		return ISLQPolynomialFold._toString(set, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLPWQPolynomial set) {
		return ISLPWQPolynomial._toString(set, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLPWQPolynomialFold p) {
		return ISLPWQPolynomialFold._toString(p, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLSchedule s) {
		return ISLSchedule._toString(s, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLScheduleNode n) {
		return n.toISLString();
	}

	public static String asString(ISLASTNode b) {
		return ISLASTNode._toString(b, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(ISLASTExpression b) {
		return ISLASTExpression._toString(b, ISL_FORMAT.ISL.ordinal());
	}

	public static String asString(Object obj) {
		
		return "default:"+obj.getClass().getSimpleName();
	}

}
