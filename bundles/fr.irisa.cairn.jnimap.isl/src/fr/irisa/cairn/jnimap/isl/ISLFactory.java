package fr.irisa.cairn.jnimap.isl;

import java.util.Collection;
import java.util.function.BiFunction;

public class ISLFactory {

	public static final int ISL_FORMAT_ISL=0;
	public static final int ISL_FORMAT_POLYLIB=1;
	public static final int ISL_FORMAT_POLYLIB_CONSTRAINTS=2;
	public static final int ISL_FORMAT_OMEGA=3;
	public static final int ISL_FORMAT_C	=		4;
	public static final int ISL_FORMAT_LATEX=		5;

	public static ISLContext getContext() {
		return ISLContext.getInstance();
	}
	
	public static ISLIdentifier islID(String name) {
		if (name == null) throw new RuntimeException(String.format("Cannot build ISLIdentifier from null string."));
		
		return JNIISLTools.recordError(
				()->ISLIdentifier.alloc(getContext(), name),
				(err)->new ISLErrorException(err, String.format("Cannot build ISLIdentifier\nisl error: %s", err)));
	}

	public static ISLVal islVal(long val) {
		return JNIISLTools.recordError(
				()->ISLVal.buildFromLong(getContext(), val),
				(err)->new ISLErrorException(err, String.format("Cannot build ISLVal\nisl error: %s", err)));
	}

	public static ISLVal islVal(long n, long d) {
		return JNIISLTools.recordError(
				()->ISLVal.buildRationalValue(getContext(), n, d),
				(err)->new ISLErrorException(err, String.format("Cannot build ISLVal\nisl error: %s", err)));
	}

	public static ISLMultiVal islMultiVal(ISLSpace space, long ... vals) {
		return JNIISLTools.recordError(
				()->{
					ISLValList list = ISLValList.build(getContext(), vals.length);
					for (long val : vals) {
						list = list.add(ISLVal.buildFromLong(getContext(), val));
					}
					return ISLMultiVal.buildFromValList(space, list);
				},
				(err)->new ISLErrorException(err, String.format("Cannot build ISLVal\nisl error: %s", err)));
	}
	
	private static <T> T islBuildFromString(Class<T> c, BiFunction<ISLContext, String, T> f, String str) {
		if (c == null || f == null) throw new IllegalArgumentException();
		if (str == null) throw new RuntimeException(String.format("Cannot build %s from null string.", c.getName()));
		
		return JNIISLTools.recordError(
				()->f.apply(getContext(), str), 
				(err)->new ISLErrorException(err, 
						String.format("Cannot build %s from %s\nisl error: %s", c.getName(), str, err)));
	}

	public static ISLBasicSet islBasicSet(String bsetStr) {
		return islBuildFromString(ISLBasicSet.class, ISLBasicSet::buildFromString, bsetStr);
	}
	
	public static ISLSet islSet(String setStr) {
		return islBuildFromString(ISLSet.class, ISLSet::buildFromString, setStr);
	}
	
	public static ISLBasicMap islBasicMap(String bmapStr) {
		return islBuildFromString(ISLBasicMap.class, ISLBasicMap::buildFromString, bmapStr);
	}

	public static ISLMap islMap(String mapStr) {
		return islBuildFromString(ISLMap.class, ISLMap::buildFromString, mapStr);
	}

	public static ISLUnionSet islUnionSet(String usetStr) {
		return islBuildFromString(ISLUnionSet.class, ISLUnionSet::buildFromString, usetStr);
	}

	public static ISLUnionMap islUnionMap(String umapStr) {
		return islBuildFromString(ISLUnionMap.class, ISLUnionMap::buildFromString, umapStr);
	}

	public static ISLAff islAff(String affStr) {
		return islBuildFromString(ISLAff.class, ISLAff::buildFromString, affStr);
	}
	
	public static ISLMultiAff islMultiAff(String maffStr) {
		return islBuildFromString(ISLMultiAff.class, ISLMultiAff::buildFromString, maffStr);
	}
	
	public static ISLPWAff islPWAff(String pwaffStr) {
		return islBuildFromString(ISLPWAff.class, ISLPWAff::buildFromString, pwaffStr);
	}
	
	public static ISLPWMultiAff islPWMultiAff(String pwmaStr) {
		return islBuildFromString(ISLPWMultiAff.class, ISLPWMultiAff::buildFromString, pwmaStr);
	}
	
	public static ISLMultiPWAff islMultiPWAff(String mpwaStr) {
		return islBuildFromString(ISLMultiPWAff.class, ISLMultiPWAff::buildFromString, mpwaStr);
	}
	
	public static ISLUnionPWAff islUnionPWAff(String upwaStr) {
		return islBuildFromString(ISLUnionPWAff.class, ISLUnionPWAff::buildFromString, upwaStr);
	}
	
	public static ISLUnionPWMultiAff ISLUnionPWMultiAff(String upwmaStr) {
		return islBuildFromString(ISLUnionPWMultiAff.class, ISLUnionPWMultiAff::buildFromString, upwmaStr);
	}
	
	public static ISLMultiUnionPWAff islMultiUnionPWAff(String mupwaStr) {
		return islBuildFromString(ISLMultiUnionPWAff.class, ISLMultiUnionPWAff::buildFromString, mupwaStr);
	}

	public static ISLPWQPolynomial islPWQPolynomial(String pwqpStr) {
		return islBuildFromString(ISLPWQPolynomial.class, ISLPWQPolynomial::buildFromString, pwqpStr);
	}

	public static ISLUnionPWQPolynomial islUnionPWQPolynomial(String upwqpStr) {
		return islBuildFromString(ISLUnionPWQPolynomial.class, ISLUnionPWQPolynomial::buildFromString, upwqpStr);
	}
	
	public static ISLVector islVector(Collection<Integer> values) {
		return ISLVector.buildFrom(getContext(), values);
	}

	public static String asString(ISLSet set) {
		return ISLSet._toString(set, ISL_FORMAT_ISL);
	}
	
	public static String asString(ISLMap set) {
		return ISLMap._toString(set, ISL_FORMAT_ISL);
	}

	public static String asString(ISLBasicSet set) {
		return ISLBasicSet._toString(set, ISL_FORMAT_ISL);
	}
	
	public static String asString(ISLBasicMap map) {
		return ISLBasicMap._toString(map, ISL_FORMAT_ISL);
	}

}
