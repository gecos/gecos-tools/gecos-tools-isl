#include "ISLUser_mat.h"

/* PROTECTED REGION ID(ISLUser_mat_local) ENABLED START */
	/* Protected region for methods used locally in this file */
void isl_hermite_result_free(long result) {
	struct isl_hermite_result* actual;

	actual = (struct isl_hermite_result*) result;
	isl_mat_free(actual->H);
	isl_mat_free(actual->U);
	isl_mat_free(actual->Q);
	free(actual);
}

/* PROTECTED REGION END */

struct isl_hermite_result* custom_isl_left_hermite(isl_mat* M) {
	/* PROTECTED REGION ID(ISLUser_mat_custom_isl_left_hermite) ENABLED START */
	return custom_isl_left_hermite_neg(M, 0);
	/* PROTECTED REGION END */
}

struct isl_hermite_result* custom_isl_left_hermite_neg(isl_mat* M, int neg) {
	/* PROTECTED REGION ID(ISLUser_mat_custom_isl_left_hermite_neg) ENABLED START */
	isl_mat *H, *U, *Q;
  isl_ctx *ctx;
	struct isl_hermite_result* result;

  ctx = isl_mat_get_ctx(M);
	// Compute the HNF.
	H = isl_mat_left_hermite(M, neg, &U, &Q);

	// Allocate space for the result and store the values into it.
	result = isl_alloc_type(ctx, struct isl_hermite_result);
	result->H = H;
	result->U = U;
	result->Q = Q;

	return result;
	/* PROTECTED REGION END */
}
