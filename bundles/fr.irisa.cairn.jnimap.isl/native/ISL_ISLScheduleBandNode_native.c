#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <isl/aff.h>
#include <isl/aff_type.h>
#include <isl/arg.h>
#include <isl/ast_build.h>
#include <isl/ast.h>
#include <isl/ast_type.h>
#include <isl/constraint.h>
#include <isl/ctx.h>
#include <isl/flow.h>
#include <isl/hash.h>
#include <isl/id.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/ilp.h>
#include <isl/list.h>
#include <isl/local_space.h>
#include <isl/lp.h>
#include <isl/map.h>
#include <isl/map_to_basic_set.h>
#include <isl/map_type.h>
#include <isl/mat.h>
#include <isl/multi.h>
#include <isl/obj.h>
#include <isl/options.h>
#include <isl/point.h>
#include <isl/polynomial.h>
#include <isl/polynomial_type.h>
#include <isl/printer.h>
#include <isl/printer_type.h>
#include <isl/schedule.h>
#include <isl/schedule_node.h>
#include <isl/schedule_type.h>
#include <isl/set.h>
#include <isl/set_type.h>
#include <isl/space.h>
#include <isl/stdint.h>
#include <isl/stream.h>
#include <isl/union_map.h>
#include <isl/union_map_type.h>
#include <isl/union_set.h>
#include <isl/union_set_type.h>
#include <isl/val_gmp.h>
#include <isl/val.h>
#include <isl/vec.h>
#include <isl/version.h>
#include <isl/vertices.h>
#include <isl/fixed_box.h>
#include <isl/stride_info.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/map_to_basic_set.h>

#include "ISLUser_vertices.h"
#include "ISLUser_ast.h"
#include "ISLUser_schedule_tree.h"
#include "ISLUser_codegen.h"
#include "ISLUser_collections.h"
#include "ISLUser_scheduling.h"
#include "ISLUser_tostring.h"
#include "ISLUser_lexnext.h"
#include "ISLUser_lexpred.h"
#include "ISLUser_misc.h"
#include "ISLUser_polynomial.h"
#include "ISLUser_stdio.h"
#include "ISLUser_mat.h"

#include "fr_irisa_cairn_jnimap_isl_ISLNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1get_1space
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_get_space\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_get_space for parameter node");
		goto error;
	}

	isl_space* res = (isl_space*) isl_schedule_node_band_get_space(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_get_space\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1get_1partial_1schedule
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_get_partial_schedule\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_get_partial_schedule for parameter node");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_schedule_node_band_get_partial_schedule(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_get_partial_schedule\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1get_1partial_1schedule_1union_1map
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_get_partial_schedule_union_map\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_get_partial_schedule_union_map for parameter node");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_schedule_node_band_get_partial_schedule_union_map(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_get_partial_schedule_union_map\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1n_1member
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_n_member\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_n_member for parameter node");
		goto error;
	}

	int res = (int) isl_schedule_node_band_n_member(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_n_member\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1member_1get_1coincident
(JNIEnv *env, jclass class, jlong node, jint pos)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_member_get_coincident\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_member_get_coincident for parameter node");
		goto error;
	}
	int pos_c = (int) pos;

	int res = (int) isl_schedule_node_band_member_get_coincident(node_c, pos_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_member_get_coincident\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1member_1set_1coincident
(JNIEnv *env, jclass class, jlong node, jint pos, jint coincident)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_member_set_coincident\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_member_set_coincident for parameter node");
		goto error;
	}
	int pos_c = (int) pos;
	int coincident_c = (int) coincident;

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_band_member_set_coincident(node_c, pos_c, coincident_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_member_set_coincident\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1get_1permutable
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_get_permutable\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_get_permutable for parameter node");
		goto error;
	}

	int res = (int) isl_schedule_node_band_get_permutable(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_get_permutable\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1set_1permutable
(JNIEnv *env, jclass class, jlong node, jint permutable)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_set_permutable\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_set_permutable for parameter node");
		goto error;
	}
	int permutable_c = (int) permutable;

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_band_set_permutable(node_c, permutable_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_set_permutable\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1member_1get_1ast_1loop_1type
(JNIEnv *env, jclass class, jlong node, jint pos)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_member_get_ast_loop_type\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_member_get_ast_loop_type for parameter node");
		goto error;
	}
	int pos_c = (int) pos;

	enum isl_ast_loop_type res = (enum isl_ast_loop_type) isl_schedule_node_band_member_get_ast_loop_type(node_c, pos_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_member_get_ast_loop_type\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return -1000;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1member_1set_1ast_1loop_1type
(JNIEnv *env, jclass class, jlong node, jint pos, jint type)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_member_set_ast_loop_type\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_member_set_ast_loop_type for parameter node");
		goto error;
	}
	int pos_c = (int) pos;
	enum isl_ast_loop_type type_c = (enum isl_ast_loop_type) type;

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_band_member_set_ast_loop_type(node_c, pos_c, type_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_member_set_ast_loop_type\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1member_1get_1isolate_1ast_1loop_1type
(JNIEnv *env, jclass class, jlong node, jint pos)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_member_get_isolate_ast_loop_type\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_member_get_isolate_ast_loop_type for parameter node");
		goto error;
	}
	int pos_c = (int) pos;

	enum isl_ast_loop_type res = (enum isl_ast_loop_type) isl_schedule_node_band_member_get_isolate_ast_loop_type(node_c, pos_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_member_get_isolate_ast_loop_type\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return -1000;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1member_1set_1isolate_1ast_1loop_1type
(JNIEnv *env, jclass class, jlong node, jint pos, jint type)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_member_set_isolate_ast_loop_type\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_member_set_isolate_ast_loop_type for parameter node");
		goto error;
	}
	int pos_c = (int) pos;
	enum isl_ast_loop_type type_c = (enum isl_ast_loop_type) type;

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_band_member_set_isolate_ast_loop_type(node_c, pos_c, type_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_member_set_isolate_ast_loop_type\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1get_1ast_1build_1options
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_get_ast_build_options\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_get_ast_build_options for parameter node");
		goto error;
	}

	isl_union_set* res = (isl_union_set*) isl_schedule_node_band_get_ast_build_options(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_get_ast_build_options\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1set_1ast_1build_1options
(JNIEnv *env, jclass class, jlong node, jlong options)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_set_ast_build_options\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_set_ast_build_options for parameter node");
		goto error;
	}
	isl_union_set* options_c = (isl_union_set*) GECOS_PTRSIZE options; 
	if(((void*)options_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_set_ast_build_options for parameter options");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_band_set_ast_build_options(node_c, options_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_set_ast_build_options\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1get_1ast_1isolate_1option
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_get_ast_isolate_option\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_get_ast_isolate_option for parameter node");
		goto error;
	}

	isl_set* res = (isl_set*) isl_schedule_node_band_get_ast_isolate_option(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_get_ast_isolate_option\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1scale
(JNIEnv *env, jclass class, jlong node, jlong mv)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_scale\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_scale for parameter node");
		goto error;
	}
	isl_multi_val* mv_c = (isl_multi_val*) GECOS_PTRSIZE mv; 
	if(((void*)mv_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_scale for parameter mv");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_band_scale(node_c, mv_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_scale\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1scale_1down
(JNIEnv *env, jclass class, jlong node, jlong mv)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_scale_down\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_scale_down for parameter node");
		goto error;
	}
	isl_multi_val* mv_c = (isl_multi_val*) GECOS_PTRSIZE mv; 
	if(((void*)mv_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_scale_down for parameter mv");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_band_scale_down(node_c, mv_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_scale_down\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1mod
(JNIEnv *env, jclass class, jlong node, jlong mv)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_mod\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_mod for parameter node");
		goto error;
	}
	isl_multi_val* mv_c = (isl_multi_val*) GECOS_PTRSIZE mv; 
	if(((void*)mv_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_mod for parameter mv");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_band_mod(node_c, mv_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_mod\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1shift
(JNIEnv *env, jclass class, jlong node, jlong shift)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_shift\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_shift for parameter node");
		goto error;
	}
	isl_multi_union_pw_aff* shift_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE shift; 
	if(((void*)shift_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_shift for parameter shift");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_band_shift(node_c, shift_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_shift\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1tile
(JNIEnv *env, jclass class, jlong node, jlong sizes)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_tile\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_tile for parameter node");
		goto error;
	}
	isl_multi_val* sizes_c = (isl_multi_val*) GECOS_PTRSIZE sizes; 
	if(((void*)sizes_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_tile for parameter sizes");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_band_tile(node_c, sizes_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_tile\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1split
(JNIEnv *env, jclass class, jlong node, jint pos)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_split\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_split for parameter node");
		goto error;
	}
	int pos_c = (int) pos;

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_band_split(node_c, pos_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_split\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1band_1sink
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_band_sink\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_band_sink for parameter node");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_band_sink(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_band_sink\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1is_1band
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_is_band\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_is_band for parameter node");
		goto error;
	}

	int res = (int) isl_schedule_node_is_band(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_is_band\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}


