#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <isl/aff.h>
#include <isl/aff_type.h>
#include <isl/arg.h>
#include <isl/ast_build.h>
#include <isl/ast.h>
#include <isl/ast_type.h>
#include <isl/constraint.h>
#include <isl/ctx.h>
#include <isl/flow.h>
#include <isl/hash.h>
#include <isl/id.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/ilp.h>
#include <isl/list.h>
#include <isl/local_space.h>
#include <isl/lp.h>
#include <isl/map.h>
#include <isl/map_to_basic_set.h>
#include <isl/map_type.h>
#include <isl/mat.h>
#include <isl/multi.h>
#include <isl/obj.h>
#include <isl/options.h>
#include <isl/point.h>
#include <isl/polynomial.h>
#include <isl/polynomial_type.h>
#include <isl/printer.h>
#include <isl/printer_type.h>
#include <isl/schedule.h>
#include <isl/schedule_node.h>
#include <isl/schedule_type.h>
#include <isl/set.h>
#include <isl/set_type.h>
#include <isl/space.h>
#include <isl/stdint.h>
#include <isl/stream.h>
#include <isl/union_map.h>
#include <isl/union_map_type.h>
#include <isl/union_set.h>
#include <isl/union_set_type.h>
#include <isl/val_gmp.h>
#include <isl/val.h>
#include <isl/vec.h>
#include <isl/version.h>
#include <isl/vertices.h>
#include <isl/fixed_box.h>
#include <isl/stride_info.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/map_to_basic_set.h>

#include "ISLUser_vertices.h"
#include "ISLUser_ast.h"
#include "ISLUser_schedule_tree.h"
#include "ISLUser_codegen.h"
#include "ISLUser_collections.h"
#include "ISLUser_scheduling.h"
#include "ISLUser_tostring.h"
#include "ISLUser_lexnext.h"
#include "ISLUser_lexpred.h"
#include "ISLUser_misc.h"
#include "ISLUser_polynomial.h"
#include "ISLUser_stdio.h"
#include "ISLUser_mat.h"

#include "fr_irisa_cairn_jnimap_isl_ISLNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1read_1from_1str
(JNIEnv *env, jclass class, jlong ctx, jstring str)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_read_from_str\n");fflush(stdout);
#endif
	isl_ctx* ctx_c = (isl_ctx*) GECOS_PTRSIZE ctx; 
	if(((void*)ctx_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_read_from_str for parameter ctx");
		goto error;
	}
	char* str_c;
	str_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, str, NULL);
	if (str_c==NULL) {
		throwException(env, "GetStringUTFChars Failed  in isl_multi_union_pw_aff_read_from_str for parameter str");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_read_from_str(ctx_c, str_c);

	(*env)->ReleaseStringUTFChars(env, str, str_c);

#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_read_from_str\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1zero
(JNIEnv *env, jclass class, jlong space)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_zero\n");fflush(stdout);
#endif
	isl_space* space_c = (isl_space*) GECOS_PTRSIZE space; 
	if(((void*)space_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_zero for parameter space");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_zero(space_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_zero\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1multi_1val_1on_1domain
(JNIEnv *env, jclass class, jlong domain, jlong mv)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_multi_val_on_domain\n");fflush(stdout);
#endif
	isl_union_set* domain_c = (isl_union_set*) GECOS_PTRSIZE domain; 
	if(((void*)domain_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_multi_val_on_domain for parameter domain");
		goto error;
	}
	isl_multi_val* mv_c = (isl_multi_val*) GECOS_PTRSIZE mv; 
	if(((void*)mv_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_multi_val_on_domain for parameter mv");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_multi_val_on_domain(domain_c, mv_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_multi_val_on_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1multi_1aff_1on_1domain
(JNIEnv *env, jclass class, jlong domain, jlong ma)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_multi_aff_on_domain\n");fflush(stdout);
#endif
	isl_union_set* domain_c = (isl_union_set*) GECOS_PTRSIZE domain; 
	if(((void*)domain_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_multi_aff_on_domain for parameter domain");
		goto error;
	}
	isl_multi_aff* ma_c = (isl_multi_aff*) GECOS_PTRSIZE ma; 
	if(((void*)ma_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_multi_aff_on_domain for parameter ma");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_multi_aff_on_domain(domain_c, ma_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_multi_aff_on_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1pw_1multi_1aff_1on_1domain
(JNIEnv *env, jclass class, jlong domain, jlong pma)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_pw_multi_aff_on_domain\n");fflush(stdout);
#endif
	isl_union_set* domain_c = (isl_union_set*) GECOS_PTRSIZE domain; 
	if(((void*)domain_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_pw_multi_aff_on_domain for parameter domain");
		goto error;
	}
	isl_pw_multi_aff* pma_c = (isl_pw_multi_aff*) GECOS_PTRSIZE pma; 
	if(((void*)pma_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_pw_multi_aff_on_domain for parameter pma");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_pw_multi_aff_on_domain(domain_c, pma_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_pw_multi_aff_on_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1from_1union_1pw_1aff_1list
(JNIEnv *env, jclass class, jlong space, jlong list)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_from_union_pw_aff_list\n");fflush(stdout);
#endif
	isl_space* space_c = (isl_space*) GECOS_PTRSIZE space; 
	if(((void*)space_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_from_union_pw_aff_list for parameter space");
		goto error;
	}
	isl_union_pw_aff_list* list_c = (isl_union_pw_aff_list*) GECOS_PTRSIZE list; 
	if(((void*)list_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_from_union_pw_aff_list for parameter list");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_from_union_pw_aff_list(space_c, list_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_from_union_pw_aff_list\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1from_1range
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_from_range\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_from_range for parameter mupa");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_from_range(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_from_range\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1get_1ctx
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_get_ctx\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_get_ctx for parameter mupa");
		goto error;
	}

	isl_ctx* res = (isl_ctx*) isl_multi_union_pw_aff_get_ctx(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_get_ctx\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1get_1space
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_get_space\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_get_space for parameter mupa");
		goto error;
	}

	isl_space* res = (isl_space*) isl_multi_union_pw_aff_get_space(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_get_space\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1copy
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_copy\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_copy for parameter mupa");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_copy(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1free
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_free\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_free for parameter mupa");
		goto error;
	}

	 isl_multi_union_pw_aff_free(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1dim
(JNIEnv *env, jclass class, jlong mupa, jint type)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_dim\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_dim for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;

	unsigned int res = (unsigned int) isl_multi_union_pw_aff_dim(mupa_c, type_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_dim\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1get_1union_1pw_1aff
(JNIEnv *env, jclass class, jlong mupa, jint pos)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_get_union_pw_aff\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_get_union_pw_aff for parameter mupa");
		goto error;
	}
	int pos_c = (int) pos;

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_multi_union_pw_aff_get_union_pw_aff(mupa_c, pos_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_get_union_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1set_1union_1pw_1aff
(JNIEnv *env, jclass class, jlong mupa, jint pos, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_set_union_pw_aff\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_set_union_pw_aff for parameter mupa");
		goto error;
	}
	int pos_c = (int) pos;
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_set_union_pw_aff for parameter upa");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_set_union_pw_aff(mupa_c, pos_c, upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_set_union_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1extract_1multi_1pw_1aff
(JNIEnv *env, jclass class, jlong mupa, jlong space)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_extract_multi_pw_aff\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_extract_multi_pw_aff for parameter mupa");
		goto error;
	}
	isl_space* space_c = (isl_space*) GECOS_PTRSIZE space; 
	if(((void*)space_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_extract_multi_pw_aff for parameter space");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_union_pw_aff_extract_multi_pw_aff(mupa_c, space_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_extract_multi_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1set_1dim_1id
(JNIEnv *env, jclass class, jlong mupa, jint type, jint pos, jlong id)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_set_dim_id\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_set_dim_id for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	unsigned int pos_c = (unsigned int) pos;
	isl_id* id_c = (isl_id*) GECOS_PTRSIZE id; 
	if(((void*)id_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_set_dim_id for parameter id");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_set_dim_id(mupa_c, type_c, pos_c, id_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_set_dim_id\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1get_1dim_1id
(JNIEnv *env, jclass class, jlong mupa, jint type, jint pos)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_get_dim_id\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_get_dim_id for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	unsigned int pos_c = (unsigned int) pos;

	isl_id* res = (isl_id*) isl_multi_union_pw_aff_get_dim_id(mupa_c, type_c, pos_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_get_dim_id\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1set_1dim_1name
(JNIEnv *env, jclass class, jlong mupa, jint type, jint pos, jstring s)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_set_dim_name\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_set_dim_name for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	unsigned int pos_c = (unsigned int) pos;
	char* s_c;
	s_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, s, NULL);
	if (s_c==NULL) {
		throwException(env, "GetStringUTFChars Failed  in isl_multi_union_pw_aff_set_dim_name for parameter s");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_set_dim_name(mupa_c, type_c, pos_c, s_c);

	(*env)->ReleaseStringUTFChars(env, s, s_c);

#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_set_dim_name\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1find_1dim_1by_1id
(JNIEnv *env, jclass class, jlong mupa, jint type, jlong id)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_find_dim_by_id\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_find_dim_by_id for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	isl_id* id_c = (isl_id*) GECOS_PTRSIZE id; 
	if(((void*)id_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_find_dim_by_id for parameter id");
		goto error;
	}

	int res = (int) isl_multi_union_pw_aff_find_dim_by_id(mupa_c, type_c, id_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_find_dim_by_id\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1find_1dim_1by_1name
(JNIEnv *env, jclass class, jlong mupa, jint type, jstring name)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_find_dim_by_name\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_find_dim_by_name for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	char* name_c;
	name_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, name, NULL);
	if (name_c==NULL) {
		throwException(env, "GetStringUTFChars Failed  in isl_multi_union_pw_aff_find_dim_by_name for parameter name");
		goto error;
	}

	int res = (int) isl_multi_union_pw_aff_find_dim_by_name(mupa_c, type_c, name_c);

	(*env)->ReleaseStringUTFChars(env, name, name_c);

#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_find_dim_by_name\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1set_1tuple_1id
(JNIEnv *env, jclass class, jlong mupa, jint type, jlong id)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_set_tuple_id\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_set_tuple_id for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	isl_id* id_c = (isl_id*) GECOS_PTRSIZE id; 
	if(((void*)id_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_set_tuple_id for parameter id");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_set_tuple_id(mupa_c, type_c, id_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_set_tuple_id\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1reset_1tuple_1id
(JNIEnv *env, jclass class, jlong mupa, jint type)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_reset_tuple_id\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_reset_tuple_id for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_reset_tuple_id(mupa_c, type_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_reset_tuple_id\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1has_1tuple_1id
(JNIEnv *env, jclass class, jlong mupa, jint type)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_has_tuple_id\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_has_tuple_id for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;

	int res = (int) isl_multi_union_pw_aff_has_tuple_id(mupa_c, type_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_has_tuple_id\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1get_1tuple_1id
(JNIEnv *env, jclass class, jlong mupa, jint type)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_get_tuple_id\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_get_tuple_id for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;

	isl_id* res = (isl_id*) isl_multi_union_pw_aff_get_tuple_id(mupa_c, type_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_get_tuple_id\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1set_1tuple_1name
(JNIEnv *env, jclass class, jlong mupa, jint type, jstring s)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_set_tuple_name\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_set_tuple_name for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	char* s_c;
	s_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, s, NULL);
	if (s_c==NULL) {
		throwException(env, "GetStringUTFChars Failed  in isl_multi_union_pw_aff_set_tuple_name for parameter s");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_set_tuple_name(mupa_c, type_c, s_c);

	(*env)->ReleaseStringUTFChars(env, s, s_c);

#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_set_tuple_name\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1get_1tuple_1name
(JNIEnv *env, jclass class, jlong mupa, jint type)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_get_tuple_name\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_get_tuple_name for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;

	char * res = (char *) isl_multi_union_pw_aff_get_tuple_name(mupa_c, type_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_get_tuple_name\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1reset_1user
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_reset_user\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_reset_user for parameter mupa");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_reset_user(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_reset_user\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1range_1is_1wrapping
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_range_is_wrapping\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_range_is_wrapping for parameter mupa");
		goto error;
	}

	int res = (int) isl_multi_union_pw_aff_range_is_wrapping(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_range_is_wrapping\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1involves_1nan
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_involves_nan\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_involves_nan for parameter mupa");
		goto error;
	}

	int res = (int) isl_multi_union_pw_aff_involves_nan(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_involves_nan\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1plain_1is_1equal
(JNIEnv *env, jclass class, jlong mupa1, jlong mupa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_plain_is_equal\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa1_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa1; 
	if(((void*)mupa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_plain_is_equal for parameter mupa1");
		goto error;
	}
	isl_multi_union_pw_aff* mupa2_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa2; 
	if(((void*)mupa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_plain_is_equal for parameter mupa2");
		goto error;
	}

	int res = (int) isl_multi_union_pw_aff_plain_is_equal(mupa1_c, mupa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_plain_is_equal\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1domain
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_domain\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_domain for parameter mupa");
		goto error;
	}

	isl_union_set* res = (isl_union_set*) isl_multi_union_pw_aff_domain(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1zero_1union_1set
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_zero_union_set\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_zero_union_set for parameter mupa");
		goto error;
	}

	isl_union_set* res = (isl_union_set*) isl_multi_union_pw_aff_zero_union_set(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_zero_union_set\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1coalesce
(JNIEnv *env, jclass class, jlong aff)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_coalesce\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* aff_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE aff; 
	if(((void*)aff_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_coalesce for parameter aff");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_coalesce(aff_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_coalesce\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1min_1multi_1val
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_min_multi_val\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_min_multi_val for parameter mupa");
		goto error;
	}

	isl_multi_val* res = (isl_multi_val*) isl_multi_union_pw_aff_min_multi_val(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_min_multi_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1max_1multi_1val
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_max_multi_val\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_max_multi_val for parameter mupa");
		goto error;
	}

	isl_multi_val* res = (isl_multi_val*) isl_multi_union_pw_aff_max_multi_val(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_max_multi_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1flatten_1range
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_flatten_range\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_flatten_range for parameter mupa");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_flatten_range(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_flatten_range\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1align_1params
(JNIEnv *env, jclass class, jlong mupa, jlong model)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_align_params\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_align_params for parameter mupa");
		goto error;
	}
	isl_space* model_c = (isl_space*) GECOS_PTRSIZE model; 
	if(((void*)model_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_align_params for parameter model");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_align_params(mupa_c, model_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_align_params\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1drop_1dims
(JNIEnv *env, jclass class, jlong mupa, jint type, jint first, jint n)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_drop_dims\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_drop_dims for parameter mupa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	unsigned int first_c = (unsigned int) first;
	unsigned int n_c = (unsigned int) n;

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_drop_dims(mupa_c, type_c, first_c, n_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_drop_dims\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1neg
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_neg\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_neg for parameter mupa");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_neg(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_neg\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1floor
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_floor\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_floor for parameter mupa");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_floor(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_floor\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1intersect_1domain
(JNIEnv *env, jclass class, jlong mupa, jlong uset)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_intersect_domain\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_intersect_domain for parameter mupa");
		goto error;
	}
	isl_union_set* uset_c = (isl_union_set*) GECOS_PTRSIZE uset; 
	if(((void*)uset_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_intersect_domain for parameter uset");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_intersect_domain(mupa_c, uset_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_intersect_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1intersect_1params
(JNIEnv *env, jclass class, jlong mupa, jlong params)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_intersect_params\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_intersect_params for parameter mupa");
		goto error;
	}
	isl_set* params_c = (isl_set*) GECOS_PTRSIZE params; 
	if(((void*)params_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_intersect_params for parameter params");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_intersect_params(mupa_c, params_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_intersect_params\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1intersect_1range
(JNIEnv *env, jclass class, jlong mupa, jlong set)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_intersect_range\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_intersect_range for parameter mupa");
		goto error;
	}
	isl_set* set_c = (isl_set*) GECOS_PTRSIZE set; 
	if(((void*)set_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_intersect_range for parameter set");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_intersect_range(mupa_c, set_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_intersect_range\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1apply_1aff
(JNIEnv *env, jclass class, jlong mupa, jlong aff)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_apply_aff\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_apply_aff for parameter mupa");
		goto error;
	}
	isl_aff* aff_c = (isl_aff*) GECOS_PTRSIZE aff; 
	if(((void*)aff_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_apply_aff for parameter aff");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_multi_union_pw_aff_apply_aff(mupa_c, aff_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_apply_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1apply_1pw_1aff
(JNIEnv *env, jclass class, jlong mupa, jlong pa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_apply_pw_aff\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_apply_pw_aff for parameter mupa");
		goto error;
	}
	isl_pw_aff* pa_c = (isl_pw_aff*) GECOS_PTRSIZE pa; 
	if(((void*)pa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_apply_pw_aff for parameter pa");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_multi_union_pw_aff_apply_pw_aff(mupa_c, pa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_apply_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1apply_1multi_1aff
(JNIEnv *env, jclass class, jlong mupa, jlong ma)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_apply_multi_aff\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_apply_multi_aff for parameter mupa");
		goto error;
	}
	isl_multi_aff* ma_c = (isl_multi_aff*) GECOS_PTRSIZE ma; 
	if(((void*)ma_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_apply_multi_aff for parameter ma");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_apply_multi_aff(mupa_c, ma_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_apply_multi_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1apply_1pw_1multi_1aff
(JNIEnv *env, jclass class, jlong mupa, jlong pma)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_apply_pw_multi_aff\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_apply_pw_multi_aff for parameter mupa");
		goto error;
	}
	isl_pw_multi_aff* pma_c = (isl_pw_multi_aff*) GECOS_PTRSIZE pma; 
	if(((void*)pma_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_apply_pw_multi_aff for parameter pma");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_apply_pw_multi_aff(mupa_c, pma_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_apply_pw_multi_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1pullback_1union_1pw_1multi_1aff
(JNIEnv *env, jclass class, jlong mupa, jlong upma)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_pullback_union_pw_multi_aff\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_pullback_union_pw_multi_aff for parameter mupa");
		goto error;
	}
	isl_union_pw_multi_aff* upma_c = (isl_union_pw_multi_aff*) GECOS_PTRSIZE upma; 
	if(((void*)upma_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_pullback_union_pw_multi_aff for parameter upma");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_pullback_union_pw_multi_aff(mupa_c, upma_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_pullback_union_pw_multi_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1range_1product
(JNIEnv *env, jclass class, jlong mupa1, jlong mupa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_range_product\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa1_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa1; 
	if(((void*)mupa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_range_product for parameter mupa1");
		goto error;
	}
	isl_multi_union_pw_aff* mupa2_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa2; 
	if(((void*)mupa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_range_product for parameter mupa2");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_range_product(mupa1_c, mupa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_range_product\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1flat_1range_1product
(JNIEnv *env, jclass class, jlong mupa1, jlong mupa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_flat_range_product\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa1_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa1; 
	if(((void*)mupa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_flat_range_product for parameter mupa1");
		goto error;
	}
	isl_multi_union_pw_aff* mupa2_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa2; 
	if(((void*)mupa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_flat_range_product for parameter mupa2");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_flat_range_product(mupa1_c, mupa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_flat_range_product\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1factor_1range
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_factor_range\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_factor_range for parameter mupa");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_factor_range(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_factor_range\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1range_1factor_1domain
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_range_factor_domain\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_range_factor_domain for parameter mupa");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_range_factor_domain(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_range_factor_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1range_1factor_1range
(JNIEnv *env, jclass class, jlong mupa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_range_factor_range\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_range_factor_range for parameter mupa");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_range_factor_range(mupa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_range_factor_range\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1range_1splice
(JNIEnv *env, jclass class, jlong mupa1, jint pos, jlong mupa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_range_splice\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa1_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa1; 
	if(((void*)mupa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_range_splice for parameter mupa1");
		goto error;
	}
	unsigned int pos_c = (unsigned int) pos;
	isl_multi_union_pw_aff* mupa2_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa2; 
	if(((void*)mupa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_range_splice for parameter mupa2");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_range_splice(mupa1_c, pos_c, mupa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_range_splice\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1gist_1params
(JNIEnv *env, jclass class, jlong aff, jlong context)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_gist_params\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* aff_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE aff; 
	if(((void*)aff_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_gist_params for parameter aff");
		goto error;
	}
	isl_set* context_c = (isl_set*) GECOS_PTRSIZE context; 
	if(((void*)context_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_gist_params for parameter context");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_gist_params(aff_c, context_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_gist_params\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1gist
(JNIEnv *env, jclass class, jlong aff, jlong context)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_gist\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* aff_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE aff; 
	if(((void*)aff_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_gist for parameter aff");
		goto error;
	}
	isl_union_set* context_c = (isl_union_set*) GECOS_PTRSIZE context; 
	if(((void*)context_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_gist for parameter context");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_gist(aff_c, context_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_gist\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1add
(JNIEnv *env, jclass class, jlong mupa1, jlong mupa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_add\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa1_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa1; 
	if(((void*)mupa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_add for parameter mupa1");
		goto error;
	}
	isl_multi_union_pw_aff* mupa2_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa2; 
	if(((void*)mupa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_add for parameter mupa2");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_add(mupa1_c, mupa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_add\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1sub
(JNIEnv *env, jclass class, jlong mupa1, jlong mupa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_sub\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa1_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa1; 
	if(((void*)mupa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_sub for parameter mupa1");
		goto error;
	}
	isl_multi_union_pw_aff* mupa2_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa2; 
	if(((void*)mupa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_sub for parameter mupa2");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_sub(mupa1_c, mupa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_sub\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1union_1add
(JNIEnv *env, jclass class, jlong mupa1, jlong mupa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_union_add\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa1_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa1; 
	if(((void*)mupa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_union_add for parameter mupa1");
		goto error;
	}
	isl_multi_union_pw_aff* mupa2_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa2; 
	if(((void*)mupa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_union_add for parameter mupa2");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_union_add(mupa1_c, mupa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_union_add\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1scale_1val
(JNIEnv *env, jclass class, jlong mupa, jlong v)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_scale_val\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_scale_val for parameter mupa");
		goto error;
	}
	isl_val* v_c = (isl_val*) GECOS_PTRSIZE v; 
	if(((void*)v_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_scale_val for parameter v");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_scale_val(mupa_c, v_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_scale_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1scale_1down_1val
(JNIEnv *env, jclass class, jlong mupa, jlong v)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_scale_down_val\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_scale_down_val for parameter mupa");
		goto error;
	}
	isl_val* v_c = (isl_val*) GECOS_PTRSIZE v; 
	if(((void*)v_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_scale_down_val for parameter v");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_scale_down_val(mupa_c, v_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_scale_down_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1mod_1multi_1val
(JNIEnv *env, jclass class, jlong upma, jlong mv)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_mod_multi_val\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* upma_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE upma; 
	if(((void*)upma_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_mod_multi_val for parameter upma");
		goto error;
	}
	isl_multi_val* mv_c = (isl_multi_val*) GECOS_PTRSIZE mv; 
	if(((void*)mv_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_mod_multi_val for parameter mv");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_mod_multi_val(upma_c, mv_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_mod_multi_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1scale_1multi_1val
(JNIEnv *env, jclass class, jlong mupa, jlong mv)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_scale_multi_val\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_scale_multi_val for parameter mupa");
		goto error;
	}
	isl_multi_val* mv_c = (isl_multi_val*) GECOS_PTRSIZE mv; 
	if(((void*)mv_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_scale_multi_val for parameter mv");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_scale_multi_val(mupa_c, mv_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_scale_multi_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1scale_1down_1multi_1val
(JNIEnv *env, jclass class, jlong mupa, jlong mv)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_scale_down_multi_val\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_scale_down_multi_val for parameter mupa");
		goto error;
	}
	isl_multi_val* mv_c = (isl_multi_val*) GECOS_PTRSIZE mv; 
	if(((void*)mv_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_scale_down_multi_val for parameter mv");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_scale_down_multi_val(mupa_c, mv_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_scale_down_multi_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1to_1string
(JNIEnv *env, jclass class, jlong mupa, jint format)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_to_string\n");fflush(stdout);
#endif
	isl_multi_union_pw_aff* mupa_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE mupa; 
	if(((void*)mupa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_to_string for parameter mupa");
		goto error;
	}
	int format_c = (int) format;

	char * res = (char *) isl_multi_union_pw_aff_to_string(mupa_c, format_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_to_string\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}


