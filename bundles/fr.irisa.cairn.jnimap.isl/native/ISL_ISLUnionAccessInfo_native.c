#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <isl/aff.h>
#include <isl/aff_type.h>
#include <isl/arg.h>
#include <isl/ast_build.h>
#include <isl/ast.h>
#include <isl/ast_type.h>
#include <isl/constraint.h>
#include <isl/ctx.h>
#include <isl/flow.h>
#include <isl/hash.h>
#include <isl/id.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/ilp.h>
#include <isl/list.h>
#include <isl/local_space.h>
#include <isl/lp.h>
#include <isl/map.h>
#include <isl/map_to_basic_set.h>
#include <isl/map_type.h>
#include <isl/mat.h>
#include <isl/multi.h>
#include <isl/obj.h>
#include <isl/options.h>
#include <isl/point.h>
#include <isl/polynomial.h>
#include <isl/polynomial_type.h>
#include <isl/printer.h>
#include <isl/printer_type.h>
#include <isl/schedule.h>
#include <isl/schedule_node.h>
#include <isl/schedule_type.h>
#include <isl/set.h>
#include <isl/set_type.h>
#include <isl/space.h>
#include <isl/stdint.h>
#include <isl/stream.h>
#include <isl/union_map.h>
#include <isl/union_map_type.h>
#include <isl/union_set.h>
#include <isl/union_set_type.h>
#include <isl/val_gmp.h>
#include <isl/val.h>
#include <isl/vec.h>
#include <isl/version.h>
#include <isl/vertices.h>
#include <isl/fixed_box.h>
#include <isl/stride_info.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/map_to_basic_set.h>

#include "ISLUser_vertices.h"
#include "ISLUser_ast.h"
#include "ISLUser_schedule_tree.h"
#include "ISLUser_codegen.h"
#include "ISLUser_collections.h"
#include "ISLUser_scheduling.h"
#include "ISLUser_tostring.h"
#include "ISLUser_lexnext.h"
#include "ISLUser_lexpred.h"
#include "ISLUser_misc.h"
#include "ISLUser_polynomial.h"
#include "ISLUser_stdio.h"
#include "ISLUser_mat.h"

#include "fr_irisa_cairn_jnimap_isl_ISLNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1access_1info_1get_1ctx
(JNIEnv *env, jclass class, jlong access)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_access_info_get_ctx\n");fflush(stdout);
#endif
	isl_union_access_info* access_c = (isl_union_access_info*) GECOS_PTRSIZE access; 
	if(((void*)access_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_get_ctx for parameter access");
		goto error;
	}

	isl_ctx* res = (isl_ctx*) isl_union_access_info_get_ctx(access_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_access_info_get_ctx\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1access_1info_1copy
(JNIEnv *env, jclass class, jlong access)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_access_info_copy\n");fflush(stdout);
#endif
	isl_union_access_info* access_c = (isl_union_access_info*) GECOS_PTRSIZE access; 
	if(((void*)access_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_copy for parameter access");
		goto error;
	}

	isl_union_access_info* res = (isl_union_access_info*) isl_union_access_info_copy(access_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_access_info_copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1access_1info_1free
(JNIEnv *env, jclass class, jlong access)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_access_info_free\n");fflush(stdout);
#endif
	isl_union_access_info* access_c = (isl_union_access_info*) GECOS_PTRSIZE access; 
	if(((void*)access_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_free for parameter access");
		goto error;
	}

	 isl_union_access_info_free(access_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_access_info_free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1access_1info_1compute_1flow
(JNIEnv *env, jclass class, jlong access)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_access_info_compute_flow\n");fflush(stdout);
#endif
	isl_union_access_info* access_c = (isl_union_access_info*) GECOS_PTRSIZE access; 
	if(((void*)access_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_compute_flow for parameter access");
		goto error;
	}

	isl_union_flow* res = (isl_union_flow*) isl_union_access_info_compute_flow(access_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_access_info_compute_flow\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1access_1info_1from_1sink
(JNIEnv *env, jclass class, jlong sink)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_access_info_from_sink\n");fflush(stdout);
#endif
	isl_union_map* sink_c = (isl_union_map*) GECOS_PTRSIZE sink; 
	if(((void*)sink_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_from_sink for parameter sink");
		goto error;
	}

	isl_union_access_info* res = (isl_union_access_info*) isl_union_access_info_from_sink(sink_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_access_info_from_sink\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1access_1info_1set_1kill
(JNIEnv *env, jclass class, jlong access, jlong kill)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_access_info_set_kill\n");fflush(stdout);
#endif
	isl_union_access_info* access_c = (isl_union_access_info*) GECOS_PTRSIZE access; 
	if(((void*)access_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_set_kill for parameter access");
		goto error;
	}
	isl_union_map* kill_c = (isl_union_map*) GECOS_PTRSIZE kill; 
	if(((void*)kill_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_set_kill for parameter kill");
		goto error;
	}

	isl_union_access_info* res = (isl_union_access_info*) isl_union_access_info_set_kill(access_c, kill_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_access_info_set_kill\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1access_1info_1set_1may_1source
(JNIEnv *env, jclass class, jlong access, jlong may_source)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_access_info_set_may_source\n");fflush(stdout);
#endif
	isl_union_access_info* access_c = (isl_union_access_info*) GECOS_PTRSIZE access; 
	if(((void*)access_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_set_may_source for parameter access");
		goto error;
	}
	isl_union_map* may_source_c = (isl_union_map*) GECOS_PTRSIZE may_source; 
	if(((void*)may_source_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_set_may_source for parameter may_source");
		goto error;
	}

	isl_union_access_info* res = (isl_union_access_info*) isl_union_access_info_set_may_source(access_c, may_source_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_access_info_set_may_source\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1access_1info_1set_1must_1source
(JNIEnv *env, jclass class, jlong access, jlong must_source)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_access_info_set_must_source\n");fflush(stdout);
#endif
	isl_union_access_info* access_c = (isl_union_access_info*) GECOS_PTRSIZE access; 
	if(((void*)access_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_set_must_source for parameter access");
		goto error;
	}
	isl_union_map* must_source_c = (isl_union_map*) GECOS_PTRSIZE must_source; 
	if(((void*)must_source_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_set_must_source for parameter must_source");
		goto error;
	}

	isl_union_access_info* res = (isl_union_access_info*) isl_union_access_info_set_must_source(access_c, must_source_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_access_info_set_must_source\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1access_1info_1set_1schedule
(JNIEnv *env, jclass class, jlong access, jlong schedule)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_access_info_set_schedule\n");fflush(stdout);
#endif
	isl_union_access_info* access_c = (isl_union_access_info*) GECOS_PTRSIZE access; 
	if(((void*)access_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_set_schedule for parameter access");
		goto error;
	}
	isl_schedule* schedule_c = (isl_schedule*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_set_schedule for parameter schedule");
		goto error;
	}

	isl_union_access_info* res = (isl_union_access_info*) isl_union_access_info_set_schedule(access_c, schedule_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_access_info_set_schedule\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1access_1info_1set_1schedule_1map
(JNIEnv *env, jclass class, jlong access, jlong schedule_map)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_access_info_set_schedule_map\n");fflush(stdout);
#endif
	isl_union_access_info* access_c = (isl_union_access_info*) GECOS_PTRSIZE access; 
	if(((void*)access_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_set_schedule_map for parameter access");
		goto error;
	}
	isl_union_map* schedule_map_c = (isl_union_map*) GECOS_PTRSIZE schedule_map; 
	if(((void*)schedule_map_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_set_schedule_map for parameter schedule_map");
		goto error;
	}

	isl_union_access_info* res = (isl_union_access_info*) isl_union_access_info_set_schedule_map(access_c, schedule_map_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_access_info_set_schedule_map\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1access_1info_1to_1string
(JNIEnv *env, jclass class, jlong access, jint format)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_access_info_to_string\n");fflush(stdout);
#endif
	isl_union_access_info* access_c = (isl_union_access_info*) GECOS_PTRSIZE access; 
	if(((void*)access_c)==NULL) {
		throwException(env, "Null pointer in isl_union_access_info_to_string for parameter access");
		goto error;
	}
	int format_c = (int) format;

	char * res = (char *) isl_union_access_info_to_string(access_c, format_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_access_info_to_string\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}


