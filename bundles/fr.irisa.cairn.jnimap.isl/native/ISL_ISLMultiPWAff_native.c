#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <isl/aff.h>
#include <isl/aff_type.h>
#include <isl/arg.h>
#include <isl/ast_build.h>
#include <isl/ast.h>
#include <isl/ast_type.h>
#include <isl/constraint.h>
#include <isl/ctx.h>
#include <isl/flow.h>
#include <isl/hash.h>
#include <isl/id.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/ilp.h>
#include <isl/list.h>
#include <isl/local_space.h>
#include <isl/lp.h>
#include <isl/map.h>
#include <isl/map_to_basic_set.h>
#include <isl/map_type.h>
#include <isl/mat.h>
#include <isl/multi.h>
#include <isl/obj.h>
#include <isl/options.h>
#include <isl/point.h>
#include <isl/polynomial.h>
#include <isl/polynomial_type.h>
#include <isl/printer.h>
#include <isl/printer_type.h>
#include <isl/schedule.h>
#include <isl/schedule_node.h>
#include <isl/schedule_type.h>
#include <isl/set.h>
#include <isl/set_type.h>
#include <isl/space.h>
#include <isl/stdint.h>
#include <isl/stream.h>
#include <isl/union_map.h>
#include <isl/union_map_type.h>
#include <isl/union_set.h>
#include <isl/union_set_type.h>
#include <isl/val_gmp.h>
#include <isl/val.h>
#include <isl/vec.h>
#include <isl/version.h>
#include <isl/vertices.h>
#include <isl/fixed_box.h>
#include <isl/stride_info.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/map_to_basic_set.h>

#include "ISLUser_vertices.h"
#include "ISLUser_ast.h"
#include "ISLUser_schedule_tree.h"
#include "ISLUser_codegen.h"
#include "ISLUser_collections.h"
#include "ISLUser_scheduling.h"
#include "ISLUser_tostring.h"
#include "ISLUser_lexnext.h"
#include "ISLUser_lexpred.h"
#include "ISLUser_misc.h"
#include "ISLUser_polynomial.h"
#include "ISLUser_stdio.h"
#include "ISLUser_mat.h"

#include "fr_irisa_cairn_jnimap_isl_ISLNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1pw_1multi_1aff_1from_1multi_1pw_1aff
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_pw_multi_aff_from_multi_pw_aff\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_pw_multi_aff_from_multi_pw_aff for parameter mpa");
		goto error;
	}

	isl_pw_multi_aff* res = (isl_pw_multi_aff*) isl_pw_multi_aff_from_multi_pw_aff(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_pw_multi_aff_from_multi_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1set_1from_1multi_1pw_1aff
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_set_from_multi_pw_aff\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_set_from_multi_pw_aff for parameter mpa");
		goto error;
	}

	isl_set* res = (isl_set*) isl_set_from_multi_pw_aff(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_set_from_multi_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1map_1from_1multi_1pw_1aff
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_map_from_multi_pw_aff\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_map_from_multi_pw_aff for parameter mpa");
		goto error;
	}

	isl_map* res = (isl_map*) isl_map_from_multi_pw_aff(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_map_from_multi_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1from_1multi_1pw_1aff
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_from_multi_pw_aff\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_from_multi_pw_aff for parameter mpa");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_from_multi_pw_aff(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_from_multi_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1from_1range
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_from_range\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_from_range for parameter mpa");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_from_range(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_from_range\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1read_1from_1str
(JNIEnv *env, jclass class, jlong ctx, jstring str)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_read_from_str\n");fflush(stdout);
#endif
	isl_ctx* ctx_c = (isl_ctx*) GECOS_PTRSIZE ctx; 
	if(((void*)ctx_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_read_from_str for parameter ctx");
		goto error;
	}
	char* str_c;
	str_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, str, NULL);
	if (str_c==NULL) {
		throwException(env, "GetStringUTFChars Failed  in isl_multi_pw_aff_read_from_str for parameter str");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_read_from_str(ctx_c, str_c);

	(*env)->ReleaseStringUTFChars(env, str, str_c);

#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_read_from_str\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1zero
(JNIEnv *env, jclass class, jlong space)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_zero\n");fflush(stdout);
#endif
	isl_space* space_c = (isl_space*) GECOS_PTRSIZE space; 
	if(((void*)space_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_zero for parameter space");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_zero(space_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_zero\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1identity
(JNIEnv *env, jclass class, jlong space)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_identity\n");fflush(stdout);
#endif
	isl_space* space_c = (isl_space*) GECOS_PTRSIZE space; 
	if(((void*)space_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_identity for parameter space");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_identity(space_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_identity\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1identity_1on_1domain_1space
(JNIEnv *env, jclass class, jlong space)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_identity_on_domain_space\n");fflush(stdout);
#endif
	isl_space* space_c = (isl_space*) GECOS_PTRSIZE space; 
	if(((void*)space_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_identity_on_domain_space for parameter space");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_identity_on_domain_space(space_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_identity_on_domain_space\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1identity_1multi_1pw_1aff
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_identity_multi_pw_aff\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_identity_multi_pw_aff for parameter mpa");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_identity_multi_pw_aff(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_identity_multi_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1from_1pw_1aff_1list
(JNIEnv *env, jclass class, jlong space, jlong list)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_from_pw_aff_list\n");fflush(stdout);
#endif
	isl_space* space_c = (isl_space*) GECOS_PTRSIZE space; 
	if(((void*)space_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_from_pw_aff_list for parameter space");
		goto error;
	}
	isl_pw_aff_list* list_c = (isl_pw_aff_list*) GECOS_PTRSIZE list; 
	if(((void*)list_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_from_pw_aff_list for parameter list");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_from_pw_aff_list(space_c, list_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_from_pw_aff_list\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1get_1ctx
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_get_ctx\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_get_ctx for parameter mpa");
		goto error;
	}

	isl_ctx* res = (isl_ctx*) isl_multi_pw_aff_get_ctx(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_get_ctx\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1get_1space
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_get_space\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_get_space for parameter mpa");
		goto error;
	}

	isl_space* res = (isl_space*) isl_multi_pw_aff_get_space(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_get_space\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1get_1domain_1space
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_get_domain_space\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_get_domain_space for parameter mpa");
		goto error;
	}

	isl_space* res = (isl_space*) isl_multi_pw_aff_get_domain_space(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_get_domain_space\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1copy
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_copy\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_copy for parameter mpa");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_copy(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1free
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_free\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_free for parameter mpa");
		goto error;
	}

	 isl_multi_pw_aff_free(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1size
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_size\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_size for parameter mpa");
		goto error;
	}

	int res = (int) isl_multi_pw_aff_size(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_size\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1get_1at
(JNIEnv *env, jclass class, jlong mpa, jint pos)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_get_at\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_get_at for parameter mpa");
		goto error;
	}
	int pos_c = (int) pos;

	isl_pw_aff* res = (isl_pw_aff*) isl_multi_pw_aff_get_at(mpa_c, pos_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_get_at\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1set_1at
(JNIEnv *env, jclass class, jlong mpa, jint pos, jlong pa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_set_at\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_set_at for parameter mpa");
		goto error;
	}
	int pos_c = (int) pos;
	isl_pw_aff* pa_c = (isl_pw_aff*) GECOS_PTRSIZE pa; 
	if(((void*)pa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_set_at for parameter pa");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_set_at(mpa_c, pos_c, pa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_set_at\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1dim
(JNIEnv *env, jclass class, jlong mpa, jint type)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_dim\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_dim for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;

	unsigned int res = (unsigned int) isl_multi_pw_aff_dim(mpa_c, type_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_dim\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1set_1dim_1id
(JNIEnv *env, jclass class, jlong mpa, jint type, jint pos, jlong id)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_set_dim_id\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_set_dim_id for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	unsigned int pos_c = (unsigned int) pos;
	isl_id* id_c = (isl_id*) GECOS_PTRSIZE id; 
	if(((void*)id_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_set_dim_id for parameter id");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_set_dim_id(mpa_c, type_c, pos_c, id_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_set_dim_id\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1get_1dim_1id
(JNIEnv *env, jclass class, jlong mpa, jint type, jint pos)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_get_dim_id\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_get_dim_id for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	unsigned int pos_c = (unsigned int) pos;

	isl_id* res = (isl_id*) isl_multi_pw_aff_get_dim_id(mpa_c, type_c, pos_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_get_dim_id\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1set_1dim_1name
(JNIEnv *env, jclass class, jlong mpa, jint type, jint pos, jstring s)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_set_dim_name\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_set_dim_name for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	unsigned int pos_c = (unsigned int) pos;
	char* s_c;
	s_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, s, NULL);
	if (s_c==NULL) {
		throwException(env, "GetStringUTFChars Failed  in isl_multi_pw_aff_set_dim_name for parameter s");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_set_dim_name(mpa_c, type_c, pos_c, s_c);

	(*env)->ReleaseStringUTFChars(env, s, s_c);

#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_set_dim_name\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1find_1dim_1by_1id
(JNIEnv *env, jclass class, jlong mpa, jint type, jlong id)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_find_dim_by_id\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_find_dim_by_id for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	isl_id* id_c = (isl_id*) GECOS_PTRSIZE id; 
	if(((void*)id_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_find_dim_by_id for parameter id");
		goto error;
	}

	int res = (int) isl_multi_pw_aff_find_dim_by_id(mpa_c, type_c, id_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_find_dim_by_id\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1find_1dim_1by_1name
(JNIEnv *env, jclass class, jlong mpa, jint type, jstring name)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_find_dim_by_name\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_find_dim_by_name for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	char* name_c;
	name_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, name, NULL);
	if (name_c==NULL) {
		throwException(env, "GetStringUTFChars Failed  in isl_multi_pw_aff_find_dim_by_name for parameter name");
		goto error;
	}

	int res = (int) isl_multi_pw_aff_find_dim_by_name(mpa_c, type_c, name_c);

	(*env)->ReleaseStringUTFChars(env, name, name_c);

#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_find_dim_by_name\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1reset_1tuple_1id
(JNIEnv *env, jclass class, jlong mpa, jint type)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_reset_tuple_id\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_reset_tuple_id for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_reset_tuple_id(mpa_c, type_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_reset_tuple_id\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1has_1tuple_1id
(JNIEnv *env, jclass class, jlong mpa, jint type)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_has_tuple_id\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_has_tuple_id for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;

	int res = (int) isl_multi_pw_aff_has_tuple_id(mpa_c, type_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_has_tuple_id\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1get_1tuple_1id
(JNIEnv *env, jclass class, jlong mpa, jint type)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_get_tuple_id\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_get_tuple_id for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;

	isl_id* res = (isl_id*) isl_multi_pw_aff_get_tuple_id(mpa_c, type_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_get_tuple_id\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1set_1tuple_1name
(JNIEnv *env, jclass class, jlong mpa, jint type, jstring s)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_set_tuple_name\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_set_tuple_name for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	char* s_c;
	s_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, s, NULL);
	if (s_c==NULL) {
		throwException(env, "GetStringUTFChars Failed  in isl_multi_pw_aff_set_tuple_name for parameter s");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_set_tuple_name(mpa_c, type_c, s_c);

	(*env)->ReleaseStringUTFChars(env, s, s_c);

#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_set_tuple_name\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1involves_1dims
(JNIEnv *env, jclass class, jlong mpa, jint type, jint first, jint n)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_involves_dims\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_involves_dims for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	unsigned int first_c = (unsigned int) first;
	unsigned int n_c = (unsigned int) n;

	int res = (int) isl_multi_pw_aff_involves_dims(mpa_c, type_c, first_c, n_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_involves_dims\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1involves_1param_1id
(JNIEnv *env, jclass class, jlong mpa, jlong id)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_involves_param_id\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_involves_param_id for parameter mpa");
		goto error;
	}
	isl_id* id_c = (isl_id*) GECOS_PTRSIZE id; 
	if(((void*)id_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_involves_param_id for parameter id");
		goto error;
	}

	int res = (int) isl_multi_pw_aff_involves_param_id(mpa_c, id_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_involves_param_id\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1involves_1param_1id_1list
(JNIEnv *env, jclass class, jlong mpa, jlong list)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_involves_param_id_list\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_involves_param_id_list for parameter mpa");
		goto error;
	}
	isl_id_list* list_c = (isl_id_list*) GECOS_PTRSIZE list; 
	if(((void*)list_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_involves_param_id_list for parameter list");
		goto error;
	}

	int res = (int) isl_multi_pw_aff_involves_param_id_list(mpa_c, list_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_involves_param_id_list\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1range_1is_1wrapping
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_range_is_wrapping\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_range_is_wrapping for parameter mpa");
		goto error;
	}

	int res = (int) isl_multi_pw_aff_range_is_wrapping(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_range_is_wrapping\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1is_1cst
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_is_cst\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_is_cst for parameter mpa");
		goto error;
	}

	int res = (int) isl_multi_pw_aff_is_cst(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_is_cst\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1involves_1nan
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_involves_nan\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_involves_nan for parameter mpa");
		goto error;
	}

	int res = (int) isl_multi_pw_aff_involves_nan(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_involves_nan\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1plain_1is_1equal
(JNIEnv *env, jclass class, jlong mpa1, jlong mpa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_plain_is_equal\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa1_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa1; 
	if(((void*)mpa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_plain_is_equal for parameter mpa1");
		goto error;
	}
	isl_multi_pw_aff* mpa2_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa2; 
	if(((void*)mpa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_plain_is_equal for parameter mpa2");
		goto error;
	}

	int res = (int) isl_multi_pw_aff_plain_is_equal(mpa1_c, mpa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_plain_is_equal\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1is_1equal
(JNIEnv *env, jclass class, jlong mpa1, jlong mpa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_is_equal\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa1_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa1; 
	if(((void*)mpa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_is_equal for parameter mpa1");
		goto error;
	}
	isl_multi_pw_aff* mpa2_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa2; 
	if(((void*)mpa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_is_equal for parameter mpa2");
		goto error;
	}

	int res = (int) isl_multi_pw_aff_is_equal(mpa1_c, mpa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_is_equal\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1project_1domain_1on_1params
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_project_domain_on_params\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_project_domain_on_params for parameter mpa");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_project_domain_on_params(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_project_domain_on_params\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1domain
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_domain\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_domain for parameter mpa");
		goto error;
	}

	isl_set* res = (isl_set*) isl_multi_pw_aff_domain(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1coalesce
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_coalesce\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_coalesce for parameter mpa");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_coalesce(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_coalesce\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1flatten_1range
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_flatten_range\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_flatten_range for parameter mpa");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_flatten_range(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_flatten_range\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1neg
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_neg\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_neg for parameter mpa");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_neg(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_neg\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1insert_1dims
(JNIEnv *env, jclass class, jlong mpa, jint type, jint first, jint n)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_insert_dims\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_insert_dims for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	unsigned int first_c = (unsigned int) first;
	unsigned int n_c = (unsigned int) n;

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_insert_dims(mpa_c, type_c, first_c, n_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_insert_dims\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1add_1dims
(JNIEnv *env, jclass class, jlong mpa, jint type, jint n)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_add_dims\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_add_dims for parameter mpa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	unsigned int n_c = (unsigned int) n;

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_add_dims(mpa_c, type_c, n_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_add_dims\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1move_1dims
(JNIEnv *env, jclass class, jlong pma, jint dst_type, jint dst_pos, jint src_type, jint src_pos, jint n)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_move_dims\n");fflush(stdout);
#endif
	isl_multi_pw_aff* pma_c = (isl_multi_pw_aff*) GECOS_PTRSIZE pma; 
	if(((void*)pma_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_move_dims for parameter pma");
		goto error;
	}
	enum isl_dim_type dst_type_c = (enum isl_dim_type) dst_type;
	unsigned int dst_pos_c = (unsigned int) dst_pos;
	enum isl_dim_type src_type_c = (enum isl_dim_type) src_type;
	unsigned int src_pos_c = (unsigned int) src_pos;
	unsigned int n_c = (unsigned int) n;

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_move_dims(pma_c, dst_type_c, dst_pos_c, src_type_c, src_pos_c, n_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_move_dims\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1intersect_1params
(JNIEnv *env, jclass class, jlong mpa, jlong set)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_intersect_params\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_intersect_params for parameter mpa");
		goto error;
	}
	isl_set* set_c = (isl_set*) GECOS_PTRSIZE set; 
	if(((void*)set_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_intersect_params for parameter set");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_intersect_params(mpa_c, set_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_intersect_params\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1intersect_1domain
(JNIEnv *env, jclass class, jlong mpa, jlong domain)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_intersect_domain\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_intersect_domain for parameter mpa");
		goto error;
	}
	isl_set* domain_c = (isl_set*) GECOS_PTRSIZE domain; 
	if(((void*)domain_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_intersect_domain for parameter domain");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_intersect_domain(mpa_c, domain_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_intersect_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1pullback_1multi_1aff
(JNIEnv *env, jclass class, jlong mpa, jlong ma)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_pullback_multi_aff\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_pullback_multi_aff for parameter mpa");
		goto error;
	}
	isl_multi_aff* ma_c = (isl_multi_aff*) GECOS_PTRSIZE ma; 
	if(((void*)ma_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_pullback_multi_aff for parameter ma");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_pullback_multi_aff(mpa_c, ma_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_pullback_multi_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1pullback_1pw_1multi_1aff
(JNIEnv *env, jclass class, jlong mpa, jlong pma)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_pullback_pw_multi_aff\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_pullback_pw_multi_aff for parameter mpa");
		goto error;
	}
	isl_pw_multi_aff* pma_c = (isl_pw_multi_aff*) GECOS_PTRSIZE pma; 
	if(((void*)pma_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_pullback_pw_multi_aff for parameter pma");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_pullback_pw_multi_aff(mpa_c, pma_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_pullback_pw_multi_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1pullback_1multi_1pw_1aff
(JNIEnv *env, jclass class, jlong mpa1, jlong mpa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_pullback_multi_pw_aff\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa1_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa1; 
	if(((void*)mpa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_pullback_multi_pw_aff for parameter mpa1");
		goto error;
	}
	isl_multi_pw_aff* mpa2_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa2; 
	if(((void*)mpa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_pullback_multi_pw_aff for parameter mpa2");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_pullback_multi_pw_aff(mpa1_c, mpa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_pullback_multi_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1range_1product
(JNIEnv *env, jclass class, jlong mpa1, jlong mpa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_range_product\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa1_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa1; 
	if(((void*)mpa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_range_product for parameter mpa1");
		goto error;
	}
	isl_multi_pw_aff* mpa2_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa2; 
	if(((void*)mpa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_range_product for parameter mpa2");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_range_product(mpa1_c, mpa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_range_product\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1product
(JNIEnv *env, jclass class, jlong mpa1, jlong mpa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_product\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa1_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa1; 
	if(((void*)mpa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_product for parameter mpa1");
		goto error;
	}
	isl_multi_pw_aff* mpa2_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa2; 
	if(((void*)mpa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_product for parameter mpa2");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_product(mpa1_c, mpa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_product\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1flat_1range_1product
(JNIEnv *env, jclass class, jlong mpa1, jlong mpa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_flat_range_product\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa1_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa1; 
	if(((void*)mpa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_flat_range_product for parameter mpa1");
		goto error;
	}
	isl_multi_pw_aff* mpa2_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa2; 
	if(((void*)mpa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_flat_range_product for parameter mpa2");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_flat_range_product(mpa1_c, mpa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_flat_range_product\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1factor_1range
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_factor_range\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_factor_range for parameter mpa");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_factor_range(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_factor_range\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1range_1factor_1domain
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_range_factor_domain\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_range_factor_domain for parameter mpa");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_range_factor_domain(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_range_factor_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1range_1factor_1range
(JNIEnv *env, jclass class, jlong mpa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_range_factor_range\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_range_factor_range for parameter mpa");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_range_factor_range(mpa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_range_factor_range\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1range_1splice
(JNIEnv *env, jclass class, jlong mpa1, jint pos, jlong mpa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_range_splice\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa1_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa1; 
	if(((void*)mpa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_range_splice for parameter mpa1");
		goto error;
	}
	unsigned int pos_c = (unsigned int) pos;
	isl_multi_pw_aff* mpa2_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa2; 
	if(((void*)mpa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_range_splice for parameter mpa2");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_range_splice(mpa1_c, pos_c, mpa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_range_splice\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1splice
(JNIEnv *env, jclass class, jlong mpa1, jint in_pos, jint out_pos, jlong mpa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_splice\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa1_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa1; 
	if(((void*)mpa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_splice for parameter mpa1");
		goto error;
	}
	unsigned int in_pos_c = (unsigned int) in_pos;
	unsigned int out_pos_c = (unsigned int) out_pos;
	isl_multi_pw_aff* mpa2_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa2; 
	if(((void*)mpa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_splice for parameter mpa2");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_splice(mpa1_c, in_pos_c, out_pos_c, mpa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_splice\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1gist_1params
(JNIEnv *env, jclass class, jlong mpa, jlong set)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_gist_params\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_gist_params for parameter mpa");
		goto error;
	}
	isl_set* set_c = (isl_set*) GECOS_PTRSIZE set; 
	if(((void*)set_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_gist_params for parameter set");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_gist_params(mpa_c, set_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_gist_params\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1gist
(JNIEnv *env, jclass class, jlong mpa, jlong set)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_gist\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_gist for parameter mpa");
		goto error;
	}
	isl_set* set_c = (isl_set*) GECOS_PTRSIZE set; 
	if(((void*)set_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_gist for parameter set");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_gist(mpa_c, set_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_gist\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1add
(JNIEnv *env, jclass class, jlong mpa1, jlong mpa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_add\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa1_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa1; 
	if(((void*)mpa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_add for parameter mpa1");
		goto error;
	}
	isl_multi_pw_aff* mpa2_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa2; 
	if(((void*)mpa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_add for parameter mpa2");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_add(mpa1_c, mpa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_add\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1sub
(JNIEnv *env, jclass class, jlong mpa1, jlong mpa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_sub\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa1_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa1; 
	if(((void*)mpa1_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_sub for parameter mpa1");
		goto error;
	}
	isl_multi_pw_aff* mpa2_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa2; 
	if(((void*)mpa2_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_sub for parameter mpa2");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_sub(mpa1_c, mpa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_sub\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1scale_1val
(JNIEnv *env, jclass class, jlong mpa, jlong v)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_scale_val\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_scale_val for parameter mpa");
		goto error;
	}
	isl_val* v_c = (isl_val*) GECOS_PTRSIZE v; 
	if(((void*)v_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_scale_val for parameter v");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_scale_val(mpa_c, v_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_scale_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1scale_1down_1val
(JNIEnv *env, jclass class, jlong mpa, jlong v)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_scale_down_val\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_scale_down_val for parameter mpa");
		goto error;
	}
	isl_val* v_c = (isl_val*) GECOS_PTRSIZE v; 
	if(((void*)v_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_scale_down_val for parameter v");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_scale_down_val(mpa_c, v_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_scale_down_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1mod_1multi_1val
(JNIEnv *env, jclass class, jlong mpa, jlong mv)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_mod_multi_val\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_mod_multi_val for parameter mpa");
		goto error;
	}
	isl_multi_val* mv_c = (isl_multi_val*) GECOS_PTRSIZE mv; 
	if(((void*)mv_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_mod_multi_val for parameter mv");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_mod_multi_val(mpa_c, mv_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_mod_multi_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1scale_1multi_1val
(JNIEnv *env, jclass class, jlong mpa, jlong mv)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_scale_multi_val\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_scale_multi_val for parameter mpa");
		goto error;
	}
	isl_multi_val* mv_c = (isl_multi_val*) GECOS_PTRSIZE mv; 
	if(((void*)mv_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_scale_multi_val for parameter mv");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_scale_multi_val(mpa_c, mv_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_scale_multi_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1scale_1down_1multi_1val
(JNIEnv *env, jclass class, jlong mpa, jlong mv)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_scale_down_multi_val\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_scale_down_multi_val for parameter mpa");
		goto error;
	}
	isl_multi_val* mv_c = (isl_multi_val*) GECOS_PTRSIZE mv; 
	if(((void*)mv_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_scale_down_multi_val for parameter mv");
		goto error;
	}

	isl_multi_pw_aff* res = (isl_multi_pw_aff*) isl_multi_pw_aff_scale_down_multi_val(mpa_c, mv_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_scale_down_multi_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1pw_1aff_1to_1string
(JNIEnv *env, jclass class, jlong mpa, jint format)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_pw_aff_to_string\n");fflush(stdout);
#endif
	isl_multi_pw_aff* mpa_c = (isl_multi_pw_aff*) GECOS_PTRSIZE mpa; 
	if(((void*)mpa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_pw_aff_to_string for parameter mpa");
		goto error;
	}
	int format_c = (int) format;

	char * res = (char *) isl_multi_pw_aff_to_string(mpa_c, format_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_pw_aff_to_string\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}


