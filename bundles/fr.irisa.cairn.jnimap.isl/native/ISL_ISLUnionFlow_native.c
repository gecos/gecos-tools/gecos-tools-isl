#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <isl/aff.h>
#include <isl/aff_type.h>
#include <isl/arg.h>
#include <isl/ast_build.h>
#include <isl/ast.h>
#include <isl/ast_type.h>
#include <isl/constraint.h>
#include <isl/ctx.h>
#include <isl/flow.h>
#include <isl/hash.h>
#include <isl/id.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/ilp.h>
#include <isl/list.h>
#include <isl/local_space.h>
#include <isl/lp.h>
#include <isl/map.h>
#include <isl/map_to_basic_set.h>
#include <isl/map_type.h>
#include <isl/mat.h>
#include <isl/multi.h>
#include <isl/obj.h>
#include <isl/options.h>
#include <isl/point.h>
#include <isl/polynomial.h>
#include <isl/polynomial_type.h>
#include <isl/printer.h>
#include <isl/printer_type.h>
#include <isl/schedule.h>
#include <isl/schedule_node.h>
#include <isl/schedule_type.h>
#include <isl/set.h>
#include <isl/set_type.h>
#include <isl/space.h>
#include <isl/stdint.h>
#include <isl/stream.h>
#include <isl/union_map.h>
#include <isl/union_map_type.h>
#include <isl/union_set.h>
#include <isl/union_set_type.h>
#include <isl/val_gmp.h>
#include <isl/val.h>
#include <isl/vec.h>
#include <isl/version.h>
#include <isl/vertices.h>
#include <isl/fixed_box.h>
#include <isl/stride_info.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/map_to_basic_set.h>

#include "ISLUser_vertices.h"
#include "ISLUser_ast.h"
#include "ISLUser_schedule_tree.h"
#include "ISLUser_codegen.h"
#include "ISLUser_collections.h"
#include "ISLUser_scheduling.h"
#include "ISLUser_tostring.h"
#include "ISLUser_lexnext.h"
#include "ISLUser_lexpred.h"
#include "ISLUser_misc.h"
#include "ISLUser_polynomial.h"
#include "ISLUser_stdio.h"
#include "ISLUser_mat.h"

#include "fr_irisa_cairn_jnimap_isl_ISLNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1flow_1get_1ctx
(JNIEnv *env, jclass class, jlong flow)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_flow_get_ctx\n");fflush(stdout);
#endif
	isl_union_flow* flow_c = (isl_union_flow*) GECOS_PTRSIZE flow; 
	if(((void*)flow_c)==NULL) {
		throwException(env, "Null pointer in isl_union_flow_get_ctx for parameter flow");
		goto error;
	}

	isl_ctx* res = (isl_ctx*) isl_union_flow_get_ctx(flow_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_flow_get_ctx\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1flow_1copy
(JNIEnv *env, jclass class, jlong flow)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_flow_copy\n");fflush(stdout);
#endif
	isl_union_flow* flow_c = (isl_union_flow*) GECOS_PTRSIZE flow; 
	if(((void*)flow_c)==NULL) {
		throwException(env, "Null pointer in isl_union_flow_copy for parameter flow");
		goto error;
	}

	isl_union_flow* res = (isl_union_flow*) isl_union_flow_copy(flow_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_flow_copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1flow_1free
(JNIEnv *env, jclass class, jlong flow)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_flow_free\n");fflush(stdout);
#endif
	isl_union_flow* flow_c = (isl_union_flow*) GECOS_PTRSIZE flow; 
	if(((void*)flow_c)==NULL) {
		throwException(env, "Null pointer in isl_union_flow_free for parameter flow");
		goto error;
	}

	 isl_union_flow_free(flow_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_flow_free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1flow_1get_1must_1dependence
(JNIEnv *env, jclass class, jlong flow)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_flow_get_must_dependence\n");fflush(stdout);
#endif
	isl_union_flow* flow_c = (isl_union_flow*) GECOS_PTRSIZE flow; 
	if(((void*)flow_c)==NULL) {
		throwException(env, "Null pointer in isl_union_flow_get_must_dependence for parameter flow");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_union_flow_get_must_dependence(flow_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_flow_get_must_dependence\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1flow_1get_1may_1dependence
(JNIEnv *env, jclass class, jlong flow)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_flow_get_may_dependence\n");fflush(stdout);
#endif
	isl_union_flow* flow_c = (isl_union_flow*) GECOS_PTRSIZE flow; 
	if(((void*)flow_c)==NULL) {
		throwException(env, "Null pointer in isl_union_flow_get_may_dependence for parameter flow");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_union_flow_get_may_dependence(flow_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_flow_get_may_dependence\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1flow_1get_1full_1must_1dependence
(JNIEnv *env, jclass class, jlong flow)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_flow_get_full_must_dependence\n");fflush(stdout);
#endif
	isl_union_flow* flow_c = (isl_union_flow*) GECOS_PTRSIZE flow; 
	if(((void*)flow_c)==NULL) {
		throwException(env, "Null pointer in isl_union_flow_get_full_must_dependence for parameter flow");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_union_flow_get_full_must_dependence(flow_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_flow_get_full_must_dependence\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1flow_1get_1full_1may_1dependence
(JNIEnv *env, jclass class, jlong flow)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_flow_get_full_may_dependence\n");fflush(stdout);
#endif
	isl_union_flow* flow_c = (isl_union_flow*) GECOS_PTRSIZE flow; 
	if(((void*)flow_c)==NULL) {
		throwException(env, "Null pointer in isl_union_flow_get_full_may_dependence for parameter flow");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_union_flow_get_full_may_dependence(flow_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_flow_get_full_may_dependence\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1flow_1get_1must_1no_1source
(JNIEnv *env, jclass class, jlong flow)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_flow_get_must_no_source\n");fflush(stdout);
#endif
	isl_union_flow* flow_c = (isl_union_flow*) GECOS_PTRSIZE flow; 
	if(((void*)flow_c)==NULL) {
		throwException(env, "Null pointer in isl_union_flow_get_must_no_source for parameter flow");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_union_flow_get_must_no_source(flow_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_flow_get_must_no_source\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1flow_1get_1may_1no_1source
(JNIEnv *env, jclass class, jlong flow)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_flow_get_may_no_source\n");fflush(stdout);
#endif
	isl_union_flow* flow_c = (isl_union_flow*) GECOS_PTRSIZE flow; 
	if(((void*)flow_c)==NULL) {
		throwException(env, "Null pointer in isl_union_flow_get_may_no_source for parameter flow");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_union_flow_get_may_no_source(flow_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_flow_get_may_no_source\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1flow_1to_1string
(JNIEnv *env, jclass class, jlong flow, jint format)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_flow_to_string\n");fflush(stdout);
#endif
	isl_union_flow* flow_c = (isl_union_flow*) GECOS_PTRSIZE flow; 
	if(((void*)flow_c)==NULL) {
		throwException(env, "Null pointer in isl_union_flow_to_string for parameter flow");
		goto error;
	}
	int format_c = (int) format;

	char * res = (char *) isl_union_flow_to_string(flow_c, format_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_flow_to_string\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}


