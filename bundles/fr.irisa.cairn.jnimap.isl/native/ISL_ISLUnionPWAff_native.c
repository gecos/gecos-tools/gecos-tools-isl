#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <isl/aff.h>
#include <isl/aff_type.h>
#include <isl/arg.h>
#include <isl/ast_build.h>
#include <isl/ast.h>
#include <isl/ast_type.h>
#include <isl/constraint.h>
#include <isl/ctx.h>
#include <isl/flow.h>
#include <isl/hash.h>
#include <isl/id.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/ilp.h>
#include <isl/list.h>
#include <isl/local_space.h>
#include <isl/lp.h>
#include <isl/map.h>
#include <isl/map_to_basic_set.h>
#include <isl/map_type.h>
#include <isl/mat.h>
#include <isl/multi.h>
#include <isl/obj.h>
#include <isl/options.h>
#include <isl/point.h>
#include <isl/polynomial.h>
#include <isl/polynomial_type.h>
#include <isl/printer.h>
#include <isl/printer_type.h>
#include <isl/schedule.h>
#include <isl/schedule_node.h>
#include <isl/schedule_type.h>
#include <isl/set.h>
#include <isl/set_type.h>
#include <isl/space.h>
#include <isl/stdint.h>
#include <isl/stream.h>
#include <isl/union_map.h>
#include <isl/union_map_type.h>
#include <isl/union_set.h>
#include <isl/union_set_type.h>
#include <isl/val_gmp.h>
#include <isl/val.h>
#include <isl/vec.h>
#include <isl/version.h>
#include <isl/vertices.h>
#include <isl/fixed_box.h>
#include <isl/stride_info.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/map_to_basic_set.h>

#include "ISLUser_vertices.h"
#include "ISLUser_ast.h"
#include "ISLUser_schedule_tree.h"
#include "ISLUser_codegen.h"
#include "ISLUser_collections.h"
#include "ISLUser_scheduling.h"
#include "ISLUser_tostring.h"
#include "ISLUser_lexnext.h"
#include "ISLUser_lexpred.h"
#include "ISLUser_misc.h"
#include "ISLUser_polynomial.h"
#include "ISLUser_stdio.h"
#include "ISLUser_mat.h"

#include "fr_irisa_cairn_jnimap_isl_ISLNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1map_1from_1union_1pw_1aff
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_map_from_union_pw_aff\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_map_from_union_pw_aff for parameter upa");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_union_map_from_union_pw_aff(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_map_from_union_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1multi_1aff_1from_1union_1pw_1aff
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_multi_aff_from_union_pw_aff\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_multi_aff_from_union_pw_aff for parameter upa");
		goto error;
	}

	isl_union_pw_multi_aff* res = (isl_union_pw_multi_aff*) isl_union_pw_multi_aff_from_union_pw_aff(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_multi_aff_from_union_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1multi_1union_1pw_1aff_1from_1union_1pw_1aff
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_multi_union_pw_aff_from_union_pw_aff\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_multi_union_pw_aff_from_union_pw_aff for parameter upa");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_multi_union_pw_aff_from_union_pw_aff(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_multi_union_pw_aff_from_union_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1get_1ctx
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_get_ctx\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_get_ctx for parameter upa");
		goto error;
	}

	isl_ctx* res = (isl_ctx*) isl_union_pw_aff_get_ctx(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_get_ctx\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1get_1space
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_get_space\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_get_space for parameter upa");
		goto error;
	}

	isl_space* res = (isl_space*) isl_union_pw_aff_get_space(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_get_space\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1copy
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_copy\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_copy for parameter upa");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_copy(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1free
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_free\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_free for parameter upa");
		goto error;
	}

	 isl_union_pw_aff_free(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1read_1from_1str
(JNIEnv *env, jclass class, jlong ctx, jstring str)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_read_from_str\n");fflush(stdout);
#endif
	isl_ctx* ctx_c = (isl_ctx*) GECOS_PTRSIZE ctx; 
	if(((void*)ctx_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_read_from_str for parameter ctx");
		goto error;
	}
	char* str_c;
	str_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, str, NULL);
	if (str_c==NULL) {
		throwException(env, "GetStringUTFChars Failed  in isl_union_pw_aff_read_from_str for parameter str");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_read_from_str(ctx_c, str_c);

	(*env)->ReleaseStringUTFChars(env, str, str_c);

#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_read_from_str\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1empty
(JNIEnv *env, jclass class, jlong space)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_empty\n");fflush(stdout);
#endif
	isl_space* space_c = (isl_space*) GECOS_PTRSIZE space; 
	if(((void*)space_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_empty for parameter space");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_empty(space_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_empty\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1val_1on_1domain
(JNIEnv *env, jclass class, jlong domain, jlong v)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_val_on_domain\n");fflush(stdout);
#endif
	isl_union_set* domain_c = (isl_union_set*) GECOS_PTRSIZE domain; 
	if(((void*)domain_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_val_on_domain for parameter domain");
		goto error;
	}
	isl_val* v_c = (isl_val*) GECOS_PTRSIZE v; 
	if(((void*)v_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_val_on_domain for parameter v");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_val_on_domain(domain_c, v_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_val_on_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1param_1on_1domain_1id
(JNIEnv *env, jclass class, jlong domain, jlong id)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_param_on_domain_id\n");fflush(stdout);
#endif
	isl_union_set* domain_c = (isl_union_set*) GECOS_PTRSIZE domain; 
	if(((void*)domain_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_param_on_domain_id for parameter domain");
		goto error;
	}
	isl_id* id_c = (isl_id*) GECOS_PTRSIZE id; 
	if(((void*)id_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_param_on_domain_id for parameter id");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_param_on_domain_id(domain_c, id_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_param_on_domain_id\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1aff_1on_1domain
(JNIEnv *env, jclass class, jlong domain, jlong aff)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_aff_on_domain\n");fflush(stdout);
#endif
	isl_union_set* domain_c = (isl_union_set*) GECOS_PTRSIZE domain; 
	if(((void*)domain_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_aff_on_domain for parameter domain");
		goto error;
	}
	isl_aff* aff_c = (isl_aff*) GECOS_PTRSIZE aff; 
	if(((void*)aff_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_aff_on_domain for parameter aff");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_aff_on_domain(domain_c, aff_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_aff_on_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1pw_1aff_1on_1domain
(JNIEnv *env, jclass class, jlong domain, jlong pa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_pw_aff_on_domain\n");fflush(stdout);
#endif
	isl_union_set* domain_c = (isl_union_set*) GECOS_PTRSIZE domain; 
	if(((void*)domain_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_pw_aff_on_domain for parameter domain");
		goto error;
	}
	isl_pw_aff* pa_c = (isl_pw_aff*) GECOS_PTRSIZE pa; 
	if(((void*)pa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_pw_aff_on_domain for parameter pa");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_pw_aff_on_domain(domain_c, pa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_pw_aff_on_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1dim
(JNIEnv *env, jclass class, jlong upa, jint type)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_dim\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_dim for parameter upa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;

	unsigned int res = (unsigned int) isl_union_pw_aff_dim(upa_c, type_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_dim\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1n_1pw_1aff
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_n_pw_aff\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_n_pw_aff for parameter upa");
		goto error;
	}

	int res = (int) isl_union_pw_aff_n_pw_aff(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_n_pw_aff\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1set_1dim_1name
(JNIEnv *env, jclass class, jlong upa, jint type, jint pos, jstring s)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_set_dim_name\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_set_dim_name for parameter upa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	unsigned int pos_c = (unsigned int) pos;
	char* s_c;
	s_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, s, NULL);
	if (s_c==NULL) {
		throwException(env, "GetStringUTFChars Failed  in isl_union_pw_aff_set_dim_name for parameter s");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_set_dim_name(upa_c, type_c, pos_c, s_c);

	(*env)->ReleaseStringUTFChars(env, s, s_c);

#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_set_dim_name\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1find_1dim_1by_1name
(JNIEnv *env, jclass class, jlong upa, jint type, jstring name)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_find_dim_by_name\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_find_dim_by_name for parameter upa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	char* name_c;
	name_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, name, NULL);
	if (name_c==NULL) {
		throwException(env, "GetStringUTFChars Failed  in isl_union_pw_aff_find_dim_by_name for parameter name");
		goto error;
	}

	int res = (int) isl_union_pw_aff_find_dim_by_name(upa_c, type_c, name_c);

	(*env)->ReleaseStringUTFChars(env, name, name_c);

#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_find_dim_by_name\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1reset_1user
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_reset_user\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_reset_user for parameter upa");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_reset_user(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_reset_user\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1extract_1pw_1aff
(JNIEnv *env, jclass class, jlong upa, jlong space)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_extract_pw_aff\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_extract_pw_aff for parameter upa");
		goto error;
	}
	isl_space* space_c = (isl_space*) GECOS_PTRSIZE space; 
	if(((void*)space_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_extract_pw_aff for parameter space");
		goto error;
	}

	isl_pw_aff* res = (isl_pw_aff*) isl_union_pw_aff_extract_pw_aff(upa_c, space_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_extract_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1get_1pw_1aff_1list
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_get_pw_aff_list\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_get_pw_aff_list for parameter upa");
		goto error;
	}

	isl_pw_aff_list* res = (isl_pw_aff_list*) isl_union_pw_aff_get_pw_aff_list(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_get_pw_aff_list\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1neg
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_neg\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_neg for parameter upa");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_neg(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_neg\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1floor
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_floor\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_floor for parameter upa");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_floor(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_floor\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1add_1pw_1aff
(JNIEnv *env, jclass class, jlong upa, jlong pa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_add_pw_aff\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_add_pw_aff for parameter upa");
		goto error;
	}
	isl_pw_aff* pa_c = (isl_pw_aff*) GECOS_PTRSIZE pa; 
	if(((void*)pa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_add_pw_aff for parameter pa");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_add_pw_aff(upa_c, pa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_add_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1add
(JNIEnv *env, jclass class, jlong upa1, jlong upa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_add\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa1_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa1; 
	if(((void*)upa1_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_add for parameter upa1");
		goto error;
	}
	isl_union_pw_aff* upa2_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa2; 
	if(((void*)upa2_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_add for parameter upa2");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_add(upa1_c, upa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_add\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1sub
(JNIEnv *env, jclass class, jlong upa1, jlong upa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_sub\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa1_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa1; 
	if(((void*)upa1_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_sub for parameter upa1");
		goto error;
	}
	isl_union_pw_aff* upa2_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa2; 
	if(((void*)upa2_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_sub for parameter upa2");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_sub(upa1_c, upa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_sub\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1union_1add
(JNIEnv *env, jclass class, jlong upa1, jlong upa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_union_add\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa1_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa1; 
	if(((void*)upa1_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_union_add for parameter upa1");
		goto error;
	}
	isl_union_pw_aff* upa2_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa2; 
	if(((void*)upa2_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_union_add for parameter upa2");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_union_add(upa1_c, upa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_union_add\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1mod_1val
(JNIEnv *env, jclass class, jlong upa, jlong f)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_mod_val\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_mod_val for parameter upa");
		goto error;
	}
	isl_val* f_c = (isl_val*) GECOS_PTRSIZE f; 
	if(((void*)f_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_mod_val for parameter f");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_mod_val(upa_c, f_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_mod_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1scale_1val
(JNIEnv *env, jclass class, jlong upa, jlong f)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_scale_val\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_scale_val for parameter upa");
		goto error;
	}
	isl_val* f_c = (isl_val*) GECOS_PTRSIZE f; 
	if(((void*)f_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_scale_val for parameter f");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_scale_val(upa_c, f_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_scale_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1scale_1down_1val
(JNIEnv *env, jclass class, jlong upa, jlong v)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_scale_down_val\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_scale_down_val for parameter upa");
		goto error;
	}
	isl_val* v_c = (isl_val*) GECOS_PTRSIZE v; 
	if(((void*)v_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_scale_down_val for parameter v");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_scale_down_val(upa_c, v_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_scale_down_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1involves_1nan
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_involves_nan\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_involves_nan for parameter upa");
		goto error;
	}

	int res = (int) isl_union_pw_aff_involves_nan(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_involves_nan\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1plain_1is_1equal
(JNIEnv *env, jclass class, jlong upa1, jlong upa2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_plain_is_equal\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa1_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa1; 
	if(((void*)upa1_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_plain_is_equal for parameter upa1");
		goto error;
	}
	isl_union_pw_aff* upa2_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa2; 
	if(((void*)upa2_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_plain_is_equal for parameter upa2");
		goto error;
	}

	int res = (int) isl_union_pw_aff_plain_is_equal(upa1_c, upa2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_plain_is_equal\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1domain
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_domain\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_domain for parameter upa");
		goto error;
	}

	isl_union_set* res = (isl_union_set*) isl_union_pw_aff_domain(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1zero_1union_1set
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_zero_union_set\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_zero_union_set for parameter upa");
		goto error;
	}

	isl_union_set* res = (isl_union_set*) isl_union_pw_aff_zero_union_set(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_zero_union_set\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1coalesce
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_coalesce\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_coalesce for parameter upa");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_coalesce(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_coalesce\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1align_1params
(JNIEnv *env, jclass class, jlong upa, jlong model)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_align_params\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_align_params for parameter upa");
		goto error;
	}
	isl_space* model_c = (isl_space*) GECOS_PTRSIZE model; 
	if(((void*)model_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_align_params for parameter model");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_align_params(upa_c, model_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_align_params\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1drop_1dims
(JNIEnv *env, jclass class, jlong upa, jint type, jint first, jint n)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_drop_dims\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_drop_dims for parameter upa");
		goto error;
	}
	enum isl_dim_type type_c = (enum isl_dim_type) type;
	unsigned int first_c = (unsigned int) first;
	unsigned int n_c = (unsigned int) n;

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_drop_dims(upa_c, type_c, first_c, n_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_drop_dims\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1min_1val
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_min_val\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_min_val for parameter upa");
		goto error;
	}

	isl_val* res = (isl_val*) isl_union_pw_aff_min_val(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_min_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1max_1val
(JNIEnv *env, jclass class, jlong upa)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_max_val\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_max_val for parameter upa");
		goto error;
	}

	isl_val* res = (isl_val*) isl_union_pw_aff_max_val(upa_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_max_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1intersect_1domain
(JNIEnv *env, jclass class, jlong upa, jlong uset)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_intersect_domain\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_intersect_domain for parameter upa");
		goto error;
	}
	isl_union_set* uset_c = (isl_union_set*) GECOS_PTRSIZE uset; 
	if(((void*)uset_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_intersect_domain for parameter uset");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_intersect_domain(upa_c, uset_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_intersect_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1intersect_1params
(JNIEnv *env, jclass class, jlong upa, jlong set)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_intersect_params\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_intersect_params for parameter upa");
		goto error;
	}
	isl_set* set_c = (isl_set*) GECOS_PTRSIZE set; 
	if(((void*)set_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_intersect_params for parameter set");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_intersect_params(upa_c, set_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_intersect_params\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1intersect_1domain_1wrapped_1domain
(JNIEnv *env, jclass class, jlong upa, jlong uset)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_intersect_domain_wrapped_domain\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_intersect_domain_wrapped_domain for parameter upa");
		goto error;
	}
	isl_union_set* uset_c = (isl_union_set*) GECOS_PTRSIZE uset; 
	if(((void*)uset_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_intersect_domain_wrapped_domain for parameter uset");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_intersect_domain_wrapped_domain(upa_c, uset_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_intersect_domain_wrapped_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1intersect_1domain_1wrapped_1range
(JNIEnv *env, jclass class, jlong upa, jlong uset)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_intersect_domain_wrapped_range\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_intersect_domain_wrapped_range for parameter upa");
		goto error;
	}
	isl_union_set* uset_c = (isl_union_set*) GECOS_PTRSIZE uset; 
	if(((void*)uset_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_intersect_domain_wrapped_range for parameter uset");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_intersect_domain_wrapped_range(upa_c, uset_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_intersect_domain_wrapped_range\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1subtract_1domain
(JNIEnv *env, jclass class, jlong upa, jlong uset)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_subtract_domain\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_subtract_domain for parameter upa");
		goto error;
	}
	isl_union_set* uset_c = (isl_union_set*) GECOS_PTRSIZE uset; 
	if(((void*)uset_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_subtract_domain for parameter uset");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_subtract_domain(upa_c, uset_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_subtract_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1pullback_1union_1pw_1multi_1aff
(JNIEnv *env, jclass class, jlong upa, jlong upma)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_pullback_union_pw_multi_aff\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_pullback_union_pw_multi_aff for parameter upa");
		goto error;
	}
	isl_union_pw_multi_aff* upma_c = (isl_union_pw_multi_aff*) GECOS_PTRSIZE upma; 
	if(((void*)upma_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_pullback_union_pw_multi_aff for parameter upma");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_pullback_union_pw_multi_aff(upa_c, upma_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_pullback_union_pw_multi_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1gist
(JNIEnv *env, jclass class, jlong upa, jlong context)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_gist\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_gist for parameter upa");
		goto error;
	}
	isl_union_set* context_c = (isl_union_set*) GECOS_PTRSIZE context; 
	if(((void*)context_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_gist for parameter context");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_gist(upa_c, context_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_gist\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1gist_1params
(JNIEnv *env, jclass class, jlong upa, jlong context)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_gist_params\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_gist_params for parameter upa");
		goto error;
	}
	isl_set* context_c = (isl_set*) GECOS_PTRSIZE context; 
	if(((void*)context_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_gist_params for parameter context");
		goto error;
	}

	isl_union_pw_aff* res = (isl_union_pw_aff*) isl_union_pw_aff_gist_params(upa_c, context_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_gist_params\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1union_1pw_1aff_1to_1string
(JNIEnv *env, jclass class, jlong upa, jint format)
 {
#ifdef TRACE_ALL
	printf("Entering isl_union_pw_aff_to_string\n");fflush(stdout);
#endif
	isl_union_pw_aff* upa_c = (isl_union_pw_aff*) GECOS_PTRSIZE upa; 
	if(((void*)upa_c)==NULL) {
		throwException(env, "Null pointer in isl_union_pw_aff_to_string for parameter upa");
		goto error;
	}
	int format_c = (int) format;

	char * res = (char *) isl_union_pw_aff_to_string(upa_c, format_c);


#ifdef TRACE_ALL
	printf("Leaving isl_union_pw_aff_to_string\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}


