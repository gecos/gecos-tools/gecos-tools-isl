#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <isl/aff.h>
#include <isl/aff_type.h>
#include <isl/arg.h>
#include <isl/ast_build.h>
#include <isl/ast.h>
#include <isl/ast_type.h>
#include <isl/constraint.h>
#include <isl/ctx.h>
#include <isl/flow.h>
#include <isl/hash.h>
#include <isl/id.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/ilp.h>
#include <isl/list.h>
#include <isl/local_space.h>
#include <isl/lp.h>
#include <isl/map.h>
#include <isl/map_to_basic_set.h>
#include <isl/map_type.h>
#include <isl/mat.h>
#include <isl/multi.h>
#include <isl/obj.h>
#include <isl/options.h>
#include <isl/point.h>
#include <isl/polynomial.h>
#include <isl/polynomial_type.h>
#include <isl/printer.h>
#include <isl/printer_type.h>
#include <isl/schedule.h>
#include <isl/schedule_node.h>
#include <isl/schedule_type.h>
#include <isl/set.h>
#include <isl/set_type.h>
#include <isl/space.h>
#include <isl/stdint.h>
#include <isl/stream.h>
#include <isl/union_map.h>
#include <isl/union_map_type.h>
#include <isl/union_set.h>
#include <isl/union_set_type.h>
#include <isl/val_gmp.h>
#include <isl/val.h>
#include <isl/vec.h>
#include <isl/version.h>
#include <isl/vertices.h>
#include <isl/fixed_box.h>
#include <isl/stride_info.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/map_to_basic_set.h>

#include "ISLUser_vertices.h"
#include "ISLUser_ast.h"
#include "ISLUser_schedule_tree.h"
#include "ISLUser_codegen.h"
#include "ISLUser_collections.h"
#include "ISLUser_scheduling.h"
#include "ISLUser_tostring.h"
#include "ISLUser_lexnext.h"
#include "ISLUser_lexpred.h"
#include "ISLUser_misc.h"
#include "ISLUser_polynomial.h"
#include "ISLUser_stdio.h"
#include "ISLUser_mat.h"

#include "fr_irisa_cairn_jnimap_isl_ISLNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1get_1ctx
(JNIEnv *env, jclass class, jlong expr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_get_ctx\n");fflush(stdout);
#endif
	isl_ast_expr* expr_c = (isl_ast_expr*) GECOS_PTRSIZE expr; 
	if(((void*)expr_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_get_ctx for parameter expr");
		goto error;
	}

	isl_ctx* res = (isl_ctx*) isl_ast_expr_get_ctx(expr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_get_ctx\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1copy
(JNIEnv *env, jclass class, jlong expr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_copy\n");fflush(stdout);
#endif
	isl_ast_expr* expr_c = (isl_ast_expr*) GECOS_PTRSIZE expr; 
	if(((void*)expr_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_copy for parameter expr");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_copy(expr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1free
(JNIEnv *env, jclass class, jlong expr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_free\n");fflush(stdout);
#endif
	isl_ast_expr* expr_c = (isl_ast_expr*) GECOS_PTRSIZE expr; 
	if(((void*)expr_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_free for parameter expr");
		goto error;
	}

	 isl_ast_expr_free(expr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1get_1type
(JNIEnv *env, jclass class, jlong expr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_get_type\n");fflush(stdout);
#endif
	isl_ast_expr* expr_c = (isl_ast_expr*) GECOS_PTRSIZE expr; 
	if(((void*)expr_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_get_type for parameter expr");
		goto error;
	}

	enum isl_ast_expr_type res = (enum isl_ast_expr_type) isl_ast_expr_get_type(expr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_get_type\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return -1000;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1is_1equal
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_is_equal\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_is_equal for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_is_equal for parameter expr2");
		goto error;
	}

	int res = (int) isl_ast_expr_is_equal(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_is_equal\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1set_1op_1arg
(JNIEnv *env, jclass class, jlong expr, jint pos, jlong arg)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_set_op_arg\n");fflush(stdout);
#endif
	isl_ast_expr* expr_c = (isl_ast_expr*) GECOS_PTRSIZE expr; 
	if(((void*)expr_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_set_op_arg for parameter expr");
		goto error;
	}
	int pos_c = (int) pos;
	isl_ast_expr* arg_c = (isl_ast_expr*) GECOS_PTRSIZE arg; 
	if(((void*)arg_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_set_op_arg for parameter arg");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_set_op_arg(expr_c, pos_c, arg_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_set_op_arg\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1substitute_1ids
(JNIEnv *env, jclass class, jlong expr, jlong id2expr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_substitute_ids\n");fflush(stdout);
#endif
	isl_ast_expr* expr_c = (isl_ast_expr*) GECOS_PTRSIZE expr; 
	if(((void*)expr_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_substitute_ids for parameter expr");
		goto error;
	}
	isl_id_to_ast_expr* id2expr_c = (isl_id_to_ast_expr*) GECOS_PTRSIZE id2expr; 
	if(((void*)id2expr_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_substitute_ids for parameter id2expr");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_substitute_ids(expr_c, id2expr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_substitute_ids\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1from_1val
(JNIEnv *env, jclass class, jlong v)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_from_val\n");fflush(stdout);
#endif
	isl_val* v_c = (isl_val*) GECOS_PTRSIZE v; 
	if(((void*)v_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_from_val for parameter v");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_from_val(v_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_from_val\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1from_1id
(JNIEnv *env, jclass class, jlong id)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_from_id\n");fflush(stdout);
#endif
	isl_id* id_c = (isl_id*) GECOS_PTRSIZE id; 
	if(((void*)id_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_from_id for parameter id");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_from_id(id_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_from_id\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1neg
(JNIEnv *env, jclass class, jlong expr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_neg\n");fflush(stdout);
#endif
	isl_ast_expr* expr_c = (isl_ast_expr*) GECOS_PTRSIZE expr; 
	if(((void*)expr_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_neg for parameter expr");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_neg(expr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_neg\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1address_1of
(JNIEnv *env, jclass class, jlong expr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_address_of\n");fflush(stdout);
#endif
	isl_ast_expr* expr_c = (isl_ast_expr*) GECOS_PTRSIZE expr; 
	if(((void*)expr_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_address_of for parameter expr");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_address_of(expr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_address_of\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1add
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_add\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_add for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_add for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_add(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_add\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1sub
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_sub\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_sub for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_sub for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_sub(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_sub\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1mul
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_mul\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_mul for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_mul for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_mul(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_mul\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1div
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_div\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_div for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_div for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_div(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_div\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1pdiv_1q
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_pdiv_q\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_pdiv_q for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_pdiv_q for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_pdiv_q(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_pdiv_q\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1pdiv_1r
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_pdiv_r\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_pdiv_r for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_pdiv_r for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_pdiv_r(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_pdiv_r\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1and
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_and\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_and for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_and for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_and(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_and\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1and_1then
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_and_then\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_and_then for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_and_then for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_and_then(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_and_then\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1or
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_or\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_or for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_or for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_or(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_or\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1or_1else
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_or_else\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_or_else for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_or_else for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_or_else(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_or_else\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1eq
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_eq\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_eq for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_eq for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_eq(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_eq\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1le
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_le\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_le for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_le for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_le(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_le\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1lt
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_lt\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_lt for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_lt for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_lt(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_lt\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1ge
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_ge\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_ge for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_ge for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_ge(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_ge\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1gt
(JNIEnv *env, jclass class, jlong expr1, jlong expr2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_gt\n");fflush(stdout);
#endif
	isl_ast_expr* expr1_c = (isl_ast_expr*) GECOS_PTRSIZE expr1; 
	if(((void*)expr1_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_gt for parameter expr1");
		goto error;
	}
	isl_ast_expr* expr2_c = (isl_ast_expr*) GECOS_PTRSIZE expr2; 
	if(((void*)expr2_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_gt for parameter expr2");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_gt(expr1_c, expr2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_gt\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1access
(JNIEnv *env, jclass class, jlong array, jlong indices)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_access\n");fflush(stdout);
#endif
	isl_ast_expr* array_c = (isl_ast_expr*) GECOS_PTRSIZE array; 
	if(((void*)array_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_access for parameter array");
		goto error;
	}
	isl_ast_expr_list* indices_c = (isl_ast_expr_list*) GECOS_PTRSIZE indices; 
	if(((void*)indices_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_access for parameter indices");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_access(array_c, indices_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_access\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1call
(JNIEnv *env, jclass class, jlong function, jlong arguments)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_call\n");fflush(stdout);
#endif
	isl_ast_expr* function_c = (isl_ast_expr*) GECOS_PTRSIZE function; 
	if(((void*)function_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_call for parameter function");
		goto error;
	}
	isl_ast_expr_list* arguments_c = (isl_ast_expr_list*) GECOS_PTRSIZE arguments; 
	if(((void*)arguments_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_call for parameter arguments");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_ast_expr_call(function_c, arguments_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_call\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1to_1C_1str
(JNIEnv *env, jclass class, jlong expr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_to_C_str\n");fflush(stdout);
#endif
	isl_ast_expr* expr_c = (isl_ast_expr*) GECOS_PTRSIZE expr; 
	if(((void*)expr_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_to_C_str for parameter expr");
		goto error;
	}

	char * res = (char *) isl_ast_expr_to_C_str(expr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_to_C_str\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1ast_1expr_1to_1string
(JNIEnv *env, jclass class, jlong expr, jint format)
 {
#ifdef TRACE_ALL
	printf("Entering isl_ast_expr_to_string\n");fflush(stdout);
#endif
	isl_ast_expr* expr_c = (isl_ast_expr*) GECOS_PTRSIZE expr; 
	if(((void*)expr_c)==NULL) {
		throwException(env, "Null pointer in isl_ast_expr_to_string for parameter expr");
		goto error;
	}
	int format_c = (int) format;

	char * res = (char *) isl_ast_expr_to_string(expr_c, format_c);


#ifdef TRACE_ALL
	printf("Leaving isl_ast_expr_to_string\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}


