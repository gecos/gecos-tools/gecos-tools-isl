#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <isl/aff.h>
#include <isl/aff_type.h>
#include <isl/arg.h>
#include <isl/ast_build.h>
#include <isl/ast.h>
#include <isl/ast_type.h>
#include <isl/constraint.h>
#include <isl/ctx.h>
#include <isl/flow.h>
#include <isl/hash.h>
#include <isl/id.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/ilp.h>
#include <isl/list.h>
#include <isl/local_space.h>
#include <isl/lp.h>
#include <isl/map.h>
#include <isl/map_to_basic_set.h>
#include <isl/map_type.h>
#include <isl/mat.h>
#include <isl/multi.h>
#include <isl/obj.h>
#include <isl/options.h>
#include <isl/point.h>
#include <isl/polynomial.h>
#include <isl/polynomial_type.h>
#include <isl/printer.h>
#include <isl/printer_type.h>
#include <isl/schedule.h>
#include <isl/schedule_node.h>
#include <isl/schedule_type.h>
#include <isl/set.h>
#include <isl/set_type.h>
#include <isl/space.h>
#include <isl/stdint.h>
#include <isl/stream.h>
#include <isl/union_map.h>
#include <isl/union_map_type.h>
#include <isl/union_set.h>
#include <isl/union_set_type.h>
#include <isl/val_gmp.h>
#include <isl/val.h>
#include <isl/vec.h>
#include <isl/version.h>
#include <isl/vertices.h>
#include <isl/fixed_box.h>
#include <isl/stride_info.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/map_to_basic_set.h>

#include "ISLUser_vertices.h"
#include "ISLUser_ast.h"
#include "ISLUser_schedule_tree.h"
#include "ISLUser_codegen.h"
#include "ISLUser_collections.h"
#include "ISLUser_scheduling.h"
#include "ISLUser_tostring.h"
#include "ISLUser_lexnext.h"
#include "ISLUser_lexpred.h"
#include "ISLUser_misc.h"
#include "ISLUser_polynomial.h"
#include "ISLUser_stdio.h"
#include "ISLUser_mat.h"

#include "fr_irisa_cairn_jnimap_isl_ISLNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1id_1to_1ast_1expr_1alloc
(JNIEnv *env, jclass class, jlong ctx, jint min_size)
 {
#ifdef TRACE_ALL
	printf("Entering isl_id_to_ast_expr_alloc\n");fflush(stdout);
#endif
	isl_ctx* ctx_c = (isl_ctx*) GECOS_PTRSIZE ctx; 
	if(((void*)ctx_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_alloc for parameter ctx");
		goto error;
	}
	int min_size_c = (int) min_size;

	isl_id_to_ast_expr* res = (isl_id_to_ast_expr*) isl_id_to_ast_expr_alloc(ctx_c, min_size_c);


#ifdef TRACE_ALL
	printf("Leaving isl_id_to_ast_expr_alloc\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1id_1to_1ast_1expr_1get_1ctx
(JNIEnv *env, jclass class, jlong id2expr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_id_to_ast_expr_get_ctx\n");fflush(stdout);
#endif
	isl_id_to_ast_expr* id2expr_c = (isl_id_to_ast_expr*) GECOS_PTRSIZE id2expr; 
	if(((void*)id2expr_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_get_ctx for parameter id2expr");
		goto error;
	}

	isl_ctx* res = (isl_ctx*) isl_id_to_ast_expr_get_ctx(id2expr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_id_to_ast_expr_get_ctx\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1id_1to_1ast_1expr_1copy
(JNIEnv *env, jclass class, jlong id2expr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_id_to_ast_expr_copy\n");fflush(stdout);
#endif
	isl_id_to_ast_expr* id2expr_c = (isl_id_to_ast_expr*) GECOS_PTRSIZE id2expr; 
	if(((void*)id2expr_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_copy for parameter id2expr");
		goto error;
	}

	isl_id_to_ast_expr* res = (isl_id_to_ast_expr*) isl_id_to_ast_expr_copy(id2expr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_id_to_ast_expr_copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1id_1to_1ast_1expr_1free
(JNIEnv *env, jclass class, jlong id2expr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_id_to_ast_expr_free\n");fflush(stdout);
#endif
	isl_id_to_ast_expr* id2expr_c = (isl_id_to_ast_expr*) GECOS_PTRSIZE id2expr; 
	if(((void*)id2expr_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_free for parameter id2expr");
		goto error;
	}

	 isl_id_to_ast_expr_free(id2expr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_id_to_ast_expr_free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1id_1to_1ast_1expr_1has
(JNIEnv *env, jclass class, jlong id2expr, jlong key)
 {
#ifdef TRACE_ALL
	printf("Entering isl_id_to_ast_expr_has\n");fflush(stdout);
#endif
	isl_id_to_ast_expr* id2expr_c = (isl_id_to_ast_expr*) GECOS_PTRSIZE id2expr; 
	if(((void*)id2expr_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_has for parameter id2expr");
		goto error;
	}
	isl_id* key_c = (isl_id*) GECOS_PTRSIZE key; 
	if(((void*)key_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_has for parameter key");
		goto error;
	}

	int res = (int) isl_id_to_ast_expr_has(id2expr_c, key_c);


#ifdef TRACE_ALL
	printf("Leaving isl_id_to_ast_expr_has\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1id_1to_1ast_1expr_1get
(JNIEnv *env, jclass class, jlong id2expr, jlong key)
 {
#ifdef TRACE_ALL
	printf("Entering isl_id_to_ast_expr_get\n");fflush(stdout);
#endif
	isl_id_to_ast_expr* id2expr_c = (isl_id_to_ast_expr*) GECOS_PTRSIZE id2expr; 
	if(((void*)id2expr_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_get for parameter id2expr");
		goto error;
	}
	isl_id* key_c = (isl_id*) GECOS_PTRSIZE key; 
	if(((void*)key_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_get for parameter key");
		goto error;
	}

	isl_ast_expr* res = (isl_ast_expr*) isl_id_to_ast_expr_get(id2expr_c, key_c);


#ifdef TRACE_ALL
	printf("Leaving isl_id_to_ast_expr_get\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1id_1to_1ast_1expr_1set
(JNIEnv *env, jclass class, jlong id2expr, jlong key, jlong val)
 {
#ifdef TRACE_ALL
	printf("Entering isl_id_to_ast_expr_set\n");fflush(stdout);
#endif
	isl_id_to_ast_expr* id2expr_c = (isl_id_to_ast_expr*) GECOS_PTRSIZE id2expr; 
	if(((void*)id2expr_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_set for parameter id2expr");
		goto error;
	}
	isl_id* key_c = (isl_id*) GECOS_PTRSIZE key; 
	if(((void*)key_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_set for parameter key");
		goto error;
	}
	isl_ast_expr* val_c = (isl_ast_expr*) GECOS_PTRSIZE val; 
	if(((void*)val_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_set for parameter val");
		goto error;
	}

	isl_id_to_ast_expr* res = (isl_id_to_ast_expr*) isl_id_to_ast_expr_set(id2expr_c, key_c, val_c);


#ifdef TRACE_ALL
	printf("Leaving isl_id_to_ast_expr_set\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1id_1to_1ast_1expr_1drop
(JNIEnv *env, jclass class, jlong id2expr, jlong key)
 {
#ifdef TRACE_ALL
	printf("Entering isl_id_to_ast_expr_drop\n");fflush(stdout);
#endif
	isl_id_to_ast_expr* id2expr_c = (isl_id_to_ast_expr*) GECOS_PTRSIZE id2expr; 
	if(((void*)id2expr_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_drop for parameter id2expr");
		goto error;
	}
	isl_id* key_c = (isl_id*) GECOS_PTRSIZE key; 
	if(((void*)key_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_drop for parameter key");
		goto error;
	}

	isl_id_to_ast_expr* res = (isl_id_to_ast_expr*) isl_id_to_ast_expr_drop(id2expr_c, key_c);


#ifdef TRACE_ALL
	printf("Leaving isl_id_to_ast_expr_drop\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1id_1to_1ast_1expr_1to_1string
(JNIEnv *env, jclass class, jlong id2expr, jint format)
 {
#ifdef TRACE_ALL
	printf("Entering isl_id_to_ast_expr_to_string\n");fflush(stdout);
#endif
	isl_id_to_ast_expr* id2expr_c = (isl_id_to_ast_expr*) GECOS_PTRSIZE id2expr; 
	if(((void*)id2expr_c)==NULL) {
		throwException(env, "Null pointer in isl_id_to_ast_expr_to_string for parameter id2expr");
		goto error;
	}
	int format_c = (int) format;

	char * res = (char *) isl_id_to_ast_expr_to_string(id2expr_c, format_c);


#ifdef TRACE_ALL
	printf("Leaving isl_id_to_ast_expr_to_string\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}


