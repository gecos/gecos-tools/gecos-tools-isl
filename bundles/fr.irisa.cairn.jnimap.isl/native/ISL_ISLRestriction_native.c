#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <isl/aff.h>
#include <isl/aff_type.h>
#include <isl/arg.h>
#include <isl/ast_build.h>
#include <isl/ast.h>
#include <isl/ast_type.h>
#include <isl/constraint.h>
#include <isl/ctx.h>
#include <isl/flow.h>
#include <isl/hash.h>
#include <isl/id.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/ilp.h>
#include <isl/list.h>
#include <isl/local_space.h>
#include <isl/lp.h>
#include <isl/map.h>
#include <isl/map_to_basic_set.h>
#include <isl/map_type.h>
#include <isl/mat.h>
#include <isl/multi.h>
#include <isl/obj.h>
#include <isl/options.h>
#include <isl/point.h>
#include <isl/polynomial.h>
#include <isl/polynomial_type.h>
#include <isl/printer.h>
#include <isl/printer_type.h>
#include <isl/schedule.h>
#include <isl/schedule_node.h>
#include <isl/schedule_type.h>
#include <isl/set.h>
#include <isl/set_type.h>
#include <isl/space.h>
#include <isl/stdint.h>
#include <isl/stream.h>
#include <isl/union_map.h>
#include <isl/union_map_type.h>
#include <isl/union_set.h>
#include <isl/union_set_type.h>
#include <isl/val_gmp.h>
#include <isl/val.h>
#include <isl/vec.h>
#include <isl/version.h>
#include <isl/vertices.h>
#include <isl/fixed_box.h>
#include <isl/stride_info.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/map_to_basic_set.h>

#include "ISLUser_vertices.h"
#include "ISLUser_ast.h"
#include "ISLUser_schedule_tree.h"
#include "ISLUser_codegen.h"
#include "ISLUser_collections.h"
#include "ISLUser_scheduling.h"
#include "ISLUser_tostring.h"
#include "ISLUser_lexnext.h"
#include "ISLUser_lexpred.h"
#include "ISLUser_misc.h"
#include "ISLUser_polynomial.h"
#include "ISLUser_stdio.h"
#include "ISLUser_mat.h"

#include "fr_irisa_cairn_jnimap_isl_ISLNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1restriction_1input
(JNIEnv *env, jclass class, jlong source_restr, jlong sink_restr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_restriction_input\n");fflush(stdout);
#endif
	isl_set* source_restr_c = (isl_set*) GECOS_PTRSIZE source_restr; 
	if(((void*)source_restr_c)==NULL) {
		throwException(env, "Null pointer in isl_restriction_input for parameter source_restr");
		goto error;
	}
	isl_set* sink_restr_c = (isl_set*) GECOS_PTRSIZE sink_restr; 
	if(((void*)sink_restr_c)==NULL) {
		throwException(env, "Null pointer in isl_restriction_input for parameter sink_restr");
		goto error;
	}

	isl_restriction* res = (isl_restriction*) isl_restriction_input(source_restr_c, sink_restr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_restriction_input\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1restriction_1output
(JNIEnv *env, jclass class, jlong source_restr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_restriction_output\n");fflush(stdout);
#endif
	isl_set* source_restr_c = (isl_set*) GECOS_PTRSIZE source_restr; 
	if(((void*)source_restr_c)==NULL) {
		throwException(env, "Null pointer in isl_restriction_output for parameter source_restr");
		goto error;
	}

	isl_restriction* res = (isl_restriction*) isl_restriction_output(source_restr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_restriction_output\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1restriction_1none
(JNIEnv *env, jclass class, jlong source_map)
 {
#ifdef TRACE_ALL
	printf("Entering isl_restriction_none\n");fflush(stdout);
#endif
	isl_map* source_map_c = (isl_map*) GECOS_PTRSIZE source_map; 
	if(((void*)source_map_c)==NULL) {
		throwException(env, "Null pointer in isl_restriction_none for parameter source_map");
		goto error;
	}

	isl_restriction* res = (isl_restriction*) isl_restriction_none(source_map_c);


#ifdef TRACE_ALL
	printf("Leaving isl_restriction_none\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1restriction_1empty
(JNIEnv *env, jclass class, jlong source_map)
 {
#ifdef TRACE_ALL
	printf("Entering isl_restriction_empty\n");fflush(stdout);
#endif
	isl_map* source_map_c = (isl_map*) GECOS_PTRSIZE source_map; 
	if(((void*)source_map_c)==NULL) {
		throwException(env, "Null pointer in isl_restriction_empty for parameter source_map");
		goto error;
	}

	isl_restriction* res = (isl_restriction*) isl_restriction_empty(source_map_c);


#ifdef TRACE_ALL
	printf("Leaving isl_restriction_empty\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1restriction_1free
(JNIEnv *env, jclass class, jlong restr)
 {
#ifdef TRACE_ALL
	printf("Entering isl_restriction_free\n");fflush(stdout);
#endif
	isl_restriction* restr_c = (isl_restriction*) GECOS_PTRSIZE restr; 
	if(((void*)restr_c)==NULL) {
		throwException(env, "Null pointer in isl_restriction_free for parameter restr");
		goto error;
	}

	 isl_restriction_free(restr_c);


#ifdef TRACE_ALL
	printf("Leaving isl_restriction_free\n");fflush(stdout);
#endif
	
error:
	return;
}


