#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <isl/aff.h>
#include <isl/aff_type.h>
#include <isl/arg.h>
#include <isl/ast_build.h>
#include <isl/ast.h>
#include <isl/ast_type.h>
#include <isl/constraint.h>
#include <isl/ctx.h>
#include <isl/flow.h>
#include <isl/hash.h>
#include <isl/id.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/ilp.h>
#include <isl/list.h>
#include <isl/local_space.h>
#include <isl/lp.h>
#include <isl/map.h>
#include <isl/map_to_basic_set.h>
#include <isl/map_type.h>
#include <isl/mat.h>
#include <isl/multi.h>
#include <isl/obj.h>
#include <isl/options.h>
#include <isl/point.h>
#include <isl/polynomial.h>
#include <isl/polynomial_type.h>
#include <isl/printer.h>
#include <isl/printer_type.h>
#include <isl/schedule.h>
#include <isl/schedule_node.h>
#include <isl/schedule_type.h>
#include <isl/set.h>
#include <isl/set_type.h>
#include <isl/space.h>
#include <isl/stdint.h>
#include <isl/stream.h>
#include <isl/union_map.h>
#include <isl/union_map_type.h>
#include <isl/union_set.h>
#include <isl/union_set_type.h>
#include <isl/val_gmp.h>
#include <isl/val.h>
#include <isl/vec.h>
#include <isl/version.h>
#include <isl/vertices.h>
#include <isl/fixed_box.h>
#include <isl/stride_info.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/map_to_basic_set.h>

#include "ISLUser_vertices.h"
#include "ISLUser_ast.h"
#include "ISLUser_schedule_tree.h"
#include "ISLUser_codegen.h"
#include "ISLUser_collections.h"
#include "ISLUser_scheduling.h"
#include "ISLUser_tostring.h"
#include "ISLUser_lexnext.h"
#include "ISLUser_lexpred.h"
#include "ISLUser_misc.h"
#include "ISLUser_polynomial.h"
#include "ISLUser_stdio.h"
#include "ISLUser_mat.h"

#include "fr_irisa_cairn_jnimap_isl_ISLNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1get_1ctx
(JNIEnv *env, jclass class, jlong sched)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_get_ctx\n");fflush(stdout);
#endif
	isl_schedule* sched_c = (isl_schedule*) GECOS_PTRSIZE sched; 
	if(((void*)sched_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_get_ctx for parameter sched");
		goto error;
	}

	isl_ctx* res = (isl_ctx*) isl_schedule_get_ctx(sched_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_get_ctx\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1empty
(JNIEnv *env, jclass class, jlong space)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_empty\n");fflush(stdout);
#endif
	isl_space* space_c = (isl_space*) GECOS_PTRSIZE space; 
	if(((void*)space_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_empty for parameter space");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_empty(space_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_empty\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1from_1domain
(JNIEnv *env, jclass class, jlong domain)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_from_domain\n");fflush(stdout);
#endif
	isl_union_set* domain_c = (isl_union_set*) GECOS_PTRSIZE domain; 
	if(((void*)domain_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_from_domain for parameter domain");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_from_domain(domain_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_from_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1read_1from_1str
(JNIEnv *env, jclass class, jlong ctx, jstring str)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_read_from_str\n");fflush(stdout);
#endif
	isl_ctx* ctx_c = (isl_ctx*) GECOS_PTRSIZE ctx; 
	if(((void*)ctx_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_read_from_str for parameter ctx");
		goto error;
	}
	char* str_c;
	str_c = (char*) (const jbyte*)(*env)->GetStringUTFChars(env, str, NULL);
	if (str_c==NULL) {
		throwException(env, "GetStringUTFChars Failed  in isl_schedule_read_from_str for parameter str");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_read_from_str(ctx_c, str_c);

	(*env)->ReleaseStringUTFChars(env, str, str_c);

#ifdef TRACE_ALL
	printf("Leaving isl_schedule_read_from_str\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1copy
(JNIEnv *env, jclass class, jlong sched)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_copy\n");fflush(stdout);
#endif
	isl_schedule* sched_c = (isl_schedule*) GECOS_PTRSIZE sched; 
	if(((void*)sched_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_copy for parameter sched");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_copy(sched_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1free
(JNIEnv *env, jclass class, jlong sched)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_free\n");fflush(stdout);
#endif
	isl_schedule* sched_c = (isl_schedule*) GECOS_PTRSIZE sched; 
	if(((void*)sched_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_free for parameter sched");
		goto error;
	}

	 isl_schedule_free(sched_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1plain_1is_1equal
(JNIEnv *env, jclass class, jlong schedule1, jlong schedule2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_plain_is_equal\n");fflush(stdout);
#endif
	isl_schedule* schedule1_c = (isl_schedule*) GECOS_PTRSIZE schedule1; 
	if(((void*)schedule1_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_plain_is_equal for parameter schedule1");
		goto error;
	}
	isl_schedule* schedule2_c = (isl_schedule*) GECOS_PTRSIZE schedule2; 
	if(((void*)schedule2_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_plain_is_equal for parameter schedule2");
		goto error;
	}

	int res = (int) isl_schedule_plain_is_equal(schedule1_c, schedule2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_plain_is_equal\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1get_1domain
(JNIEnv *env, jclass class, jlong schedule)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_get_domain\n");fflush(stdout);
#endif
	isl_schedule* schedule_c = (isl_schedule*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_get_domain for parameter schedule");
		goto error;
	}

	isl_union_set* res = (isl_union_set*) isl_schedule_get_domain(schedule_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_get_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1get_1map
(JNIEnv *env, jclass class, jlong sched)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_get_map\n");fflush(stdout);
#endif
	isl_schedule* sched_c = (isl_schedule*) GECOS_PTRSIZE sched; 
	if(((void*)sched_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_get_map for parameter sched");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_schedule_get_map(sched_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_get_map\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1insert_1partial_1schedule
(JNIEnv *env, jclass class, jlong schedule, jlong partial)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_insert_partial_schedule\n");fflush(stdout);
#endif
	isl_schedule* schedule_c = (isl_schedule*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_insert_partial_schedule for parameter schedule");
		goto error;
	}
	isl_multi_union_pw_aff* partial_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE partial; 
	if(((void*)partial_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_insert_partial_schedule for parameter partial");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_insert_partial_schedule(schedule_c, partial_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_insert_partial_schedule\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1insert_1context
(JNIEnv *env, jclass class, jlong schedule, jlong context)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_insert_context\n");fflush(stdout);
#endif
	isl_schedule* schedule_c = (isl_schedule*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_insert_context for parameter schedule");
		goto error;
	}
	isl_set* context_c = (isl_set*) GECOS_PTRSIZE context; 
	if(((void*)context_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_insert_context for parameter context");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_insert_context(schedule_c, context_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_insert_context\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1insert_1guard
(JNIEnv *env, jclass class, jlong schedule, jlong guard)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_insert_guard\n");fflush(stdout);
#endif
	isl_schedule* schedule_c = (isl_schedule*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_insert_guard for parameter schedule");
		goto error;
	}
	isl_set* guard_c = (isl_set*) GECOS_PTRSIZE guard; 
	if(((void*)guard_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_insert_guard for parameter guard");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_insert_guard(schedule_c, guard_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_insert_guard\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1sequence
(JNIEnv *env, jclass class, jlong schedule1, jlong schedule2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_sequence\n");fflush(stdout);
#endif
	isl_schedule* schedule1_c = (isl_schedule*) GECOS_PTRSIZE schedule1; 
	if(((void*)schedule1_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_sequence for parameter schedule1");
		goto error;
	}
	isl_schedule* schedule2_c = (isl_schedule*) GECOS_PTRSIZE schedule2; 
	if(((void*)schedule2_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_sequence for parameter schedule2");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_sequence(schedule1_c, schedule2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_sequence\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1set
(JNIEnv *env, jclass class, jlong schedule1, jlong schedule2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_set\n");fflush(stdout);
#endif
	isl_schedule* schedule1_c = (isl_schedule*) GECOS_PTRSIZE schedule1; 
	if(((void*)schedule1_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_set for parameter schedule1");
		goto error;
	}
	isl_schedule* schedule2_c = (isl_schedule*) GECOS_PTRSIZE schedule2; 
	if(((void*)schedule2_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_set for parameter schedule2");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_set(schedule1_c, schedule2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_set\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1intersect_1domain
(JNIEnv *env, jclass class, jlong schedule, jlong domain)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_intersect_domain\n");fflush(stdout);
#endif
	isl_schedule* schedule_c = (isl_schedule*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_intersect_domain for parameter schedule");
		goto error;
	}
	isl_union_set* domain_c = (isl_union_set*) GECOS_PTRSIZE domain; 
	if(((void*)domain_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_intersect_domain for parameter domain");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_intersect_domain(schedule_c, domain_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_intersect_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1gist_1domain_1params
(JNIEnv *env, jclass class, jlong schedule, jlong context)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_gist_domain_params\n");fflush(stdout);
#endif
	isl_schedule* schedule_c = (isl_schedule*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_gist_domain_params for parameter schedule");
		goto error;
	}
	isl_set* context_c = (isl_set*) GECOS_PTRSIZE context; 
	if(((void*)context_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_gist_domain_params for parameter context");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_gist_domain_params(schedule_c, context_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_gist_domain_params\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1reset_1user
(JNIEnv *env, jclass class, jlong schedule)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_reset_user\n");fflush(stdout);
#endif
	isl_schedule* schedule_c = (isl_schedule*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_reset_user for parameter schedule");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_reset_user(schedule_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_reset_user\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1align_1params
(JNIEnv *env, jclass class, jlong schedule, jlong space)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_align_params\n");fflush(stdout);
#endif
	isl_schedule* schedule_c = (isl_schedule*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_align_params for parameter schedule");
		goto error;
	}
	isl_space* space_c = (isl_space*) GECOS_PTRSIZE space; 
	if(((void*)space_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_align_params for parameter space");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_align_params(schedule_c, space_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_align_params\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1pullback_1union_1pw_1multi_1aff
(JNIEnv *env, jclass class, jlong schedule, jlong upma)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_pullback_union_pw_multi_aff\n");fflush(stdout);
#endif
	isl_schedule* schedule_c = (isl_schedule*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_pullback_union_pw_multi_aff for parameter schedule");
		goto error;
	}
	isl_union_pw_multi_aff* upma_c = (isl_union_pw_multi_aff*) GECOS_PTRSIZE upma; 
	if(((void*)upma_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_pullback_union_pw_multi_aff for parameter upma");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_pullback_union_pw_multi_aff(schedule_c, upma_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_pullback_union_pw_multi_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1expand
(JNIEnv *env, jclass class, jlong schedule, jlong contraction, jlong expansion)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_expand\n");fflush(stdout);
#endif
	isl_schedule* schedule_c = (isl_schedule*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_expand for parameter schedule");
		goto error;
	}
	isl_union_pw_multi_aff* contraction_c = (isl_union_pw_multi_aff*) GECOS_PTRSIZE contraction; 
	if(((void*)contraction_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_expand for parameter contraction");
		goto error;
	}
	isl_schedule* expansion_c = (isl_schedule*) GECOS_PTRSIZE expansion; 
	if(((void*)expansion_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_expand for parameter expansion");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_expand(schedule_c, contraction_c, expansion_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_expand\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1get_1root
(JNIEnv *env, jclass class, jlong schedule)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_get_root\n");fflush(stdout);
#endif
	isl_schedule* schedule_c = (isl_schedule*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_get_root for parameter schedule");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_get_root(schedule_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_get_root\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1compute_1schedule
(JNIEnv *env, jclass class, jlong ctx, jlong domain, jlong validity, jlong proximity, jint max_coefficient, jint max_constant_term, jint fuse, jint maximize_band_depth, jint outer_coincidence, jint split_scaled, jint algorithm, jint separate_components)
 {
#ifdef TRACE_ALL
	printf("Entering isl_compute_schedule\n");fflush(stdout);
#endif
	isl_ctx* ctx_c = (isl_ctx*) GECOS_PTRSIZE ctx; 
	if(((void*)ctx_c)==NULL) {
		throwException(env, "Null pointer in isl_compute_schedule for parameter ctx");
		goto error;
	}
	isl_union_set* domain_c = (isl_union_set*) GECOS_PTRSIZE domain; 
	if(((void*)domain_c)==NULL) {
		throwException(env, "Null pointer in isl_compute_schedule for parameter domain");
		goto error;
	}
	isl_union_map* validity_c = (isl_union_map*) GECOS_PTRSIZE validity; 
	if(((void*)validity_c)==NULL) {
		throwException(env, "Null pointer in isl_compute_schedule for parameter validity");
		goto error;
	}
	isl_union_map* proximity_c = (isl_union_map*) GECOS_PTRSIZE proximity; 
	if(((void*)proximity_c)==NULL) {
		throwException(env, "Null pointer in isl_compute_schedule for parameter proximity");
		goto error;
	}
	int max_coefficient_c = (int) max_coefficient;
	int max_constant_term_c = (int) max_constant_term;
	int fuse_c = (int) fuse;
	int maximize_band_depth_c = (int) maximize_band_depth;
	int outer_coincidence_c = (int) outer_coincidence;
	int split_scaled_c = (int) split_scaled;
	int algorithm_c = (int) algorithm;
	int separate_components_c = (int) separate_components;

	isl_schedule* res = (isl_schedule*) isl_compute_schedule(ctx_c, domain_c, validity_c, proximity_c, max_coefficient_c, max_constant_term_c, fuse_c, maximize_band_depth_c, outer_coincidence_c, split_scaled_c, algorithm_c, separate_components_c);


#ifdef TRACE_ALL
	printf("Leaving isl_compute_schedule\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1to_1string
(JNIEnv *env, jclass class, jlong s, jint format)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_to_string\n");fflush(stdout);
#endif
	isl_schedule* s_c = (isl_schedule*) GECOS_PTRSIZE s; 
	if(((void*)s_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_to_string for parameter s");
		goto error;
	}
	int format_c = (int) format;

	char * res = (char *) isl_schedule_to_string(s_c, format_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_to_string\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}


