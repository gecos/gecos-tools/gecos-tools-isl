#include "ISLUser_schedule_tree.h"

/* PROTECTED REGION ID(ISLUser_schedule_tree_local) ENABLED START */
	/* Protected region for methods used locally in this file */
#define SCH_NODE_IS(TYPE) return node!=NULL?isl_schedule_node_get_type(node)==(TYPE):0;
/* PROTECTED REGION END */

int isl_schedule_node_is_band(isl_schedule_node* node) {
	/* PROTECTED REGION ID(ISLUser_schedule_tree_isl_schedule_node_is_band) ENABLED START */
	SCH_NODE_IS(isl_schedule_node_band);
	/* PROTECTED REGION END */
}
int isl_schedule_node_is_context(isl_schedule_node* node) {
	/* PROTECTED REGION ID(ISLUser_schedule_tree_isl_schedule_node_is_context) ENABLED START */
	SCH_NODE_IS(isl_schedule_node_context);
	/* PROTECTED REGION END */
}
int isl_schedule_node_is_domain(isl_schedule_node* node) {
	/* PROTECTED REGION ID(ISLUser_schedule_tree_isl_schedule_node_is_domain) ENABLED START */
	SCH_NODE_IS(isl_schedule_node_domain);
	/* PROTECTED REGION END */
}
int isl_schedule_node_is_expansion(isl_schedule_node* node) {
	/* PROTECTED REGION ID(ISLUser_schedule_tree_isl_schedule_node_is_expansion) ENABLED START */
	SCH_NODE_IS(isl_schedule_node_expansion);
	/* PROTECTED REGION END */
}
int isl_schedule_node_is_extension(isl_schedule_node* node) {
	/* PROTECTED REGION ID(ISLUser_schedule_tree_isl_schedule_node_is_extension) ENABLED START */
	SCH_NODE_IS(isl_schedule_node_extension);
	/* PROTECTED REGION END */
}
int isl_schedule_node_is_filter(isl_schedule_node* node) {
	/* PROTECTED REGION ID(ISLUser_schedule_tree_isl_schedule_node_is_filter) ENABLED START */
	SCH_NODE_IS(isl_schedule_node_filter);
	/* PROTECTED REGION END */
}
int isl_schedule_node_is_leaf(isl_schedule_node* node) {
	/* PROTECTED REGION ID(ISLUser_schedule_tree_isl_schedule_node_is_leaf) ENABLED START */
	SCH_NODE_IS(isl_schedule_node_leaf);
	/* PROTECTED REGION END */
}
int isl_schedule_node_is_guard(isl_schedule_node* node) {
	/* PROTECTED REGION ID(ISLUser_schedule_tree_isl_schedule_node_is_guard) ENABLED START */
	SCH_NODE_IS(isl_schedule_node_guard);
	/* PROTECTED REGION END */
}
int isl_schedule_node_is_mark(isl_schedule_node* node) {
	/* PROTECTED REGION ID(ISLUser_schedule_tree_isl_schedule_node_is_mark) ENABLED START */
	SCH_NODE_IS(isl_schedule_node_mark);
	/* PROTECTED REGION END */
}
int isl_schedule_node_is_sequence(isl_schedule_node* node) {
	/* PROTECTED REGION ID(ISLUser_schedule_tree_isl_schedule_node_is_sequence) ENABLED START */
	SCH_NODE_IS(isl_schedule_node_sequence);
	/* PROTECTED REGION END */
}
int isl_schedule_node_is_set(isl_schedule_node* node) {
	/* PROTECTED REGION ID(ISLUser_schedule_tree_isl_schedule_node_is_set) ENABLED START */
	SCH_NODE_IS(isl_schedule_node_set);
	/* PROTECTED REGION END */
}
