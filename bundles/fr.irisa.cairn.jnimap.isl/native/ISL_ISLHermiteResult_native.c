#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <isl/aff.h>
#include <isl/aff_type.h>
#include <isl/arg.h>
#include <isl/ast_build.h>
#include <isl/ast.h>
#include <isl/ast_type.h>
#include <isl/constraint.h>
#include <isl/ctx.h>
#include <isl/flow.h>
#include <isl/hash.h>
#include <isl/id.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/ilp.h>
#include <isl/list.h>
#include <isl/local_space.h>
#include <isl/lp.h>
#include <isl/map.h>
#include <isl/map_to_basic_set.h>
#include <isl/map_type.h>
#include <isl/mat.h>
#include <isl/multi.h>
#include <isl/obj.h>
#include <isl/options.h>
#include <isl/point.h>
#include <isl/polynomial.h>
#include <isl/polynomial_type.h>
#include <isl/printer.h>
#include <isl/printer_type.h>
#include <isl/schedule.h>
#include <isl/schedule_node.h>
#include <isl/schedule_type.h>
#include <isl/set.h>
#include <isl/set_type.h>
#include <isl/space.h>
#include <isl/stdint.h>
#include <isl/stream.h>
#include <isl/union_map.h>
#include <isl/union_map_type.h>
#include <isl/union_set.h>
#include <isl/union_set_type.h>
#include <isl/val_gmp.h>
#include <isl/val.h>
#include <isl/vec.h>
#include <isl/version.h>
#include <isl/vertices.h>
#include <isl/fixed_box.h>
#include <isl/stride_info.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/map_to_basic_set.h>

#include "ISLUser_vertices.h"
#include "ISLUser_ast.h"
#include "ISLUser_schedule_tree.h"
#include "ISLUser_codegen.h"
#include "ISLUser_collections.h"
#include "ISLUser_scheduling.h"
#include "ISLUser_tostring.h"
#include "ISLUser_lexnext.h"
#include "ISLUser_lexpred.h"
#include "ISLUser_misc.h"
#include "ISLUser_polynomial.h"
#include "ISLUser_stdio.h"
#include "ISLUser_mat.h"

#include "fr_irisa_cairn_jnimap_isl_ISLNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);

JNIEXPORT jlong JNICALL
Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1hermite_1result_1get_1H
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(isl_hermite_result_H_getter) DISABLED START */
	struct isl_hermite_result* stPtr = (struct isl_hermite_result *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getH");
	return (jlong) GECOS_PTRSIZE isl_mat_copy(stPtr->H);
	/* PROTECTED REGION END */
}

JNIEXPORT void JNICALL
Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_hermite_result_1set_1H
	(JNIEnv *env, jclass class, jlong ptr, jlong value) {
	/* PROTECTED REGION ID(isl_hermite_result_H_setter) DISABLED START */
	struct isl_hermite_result* stPtr = (struct isl_hermite_result *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in setH");
	stPtr->H= (isl_mat*) GECOS_PTRSIZE value;
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1hermite_1result_1test_1H
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(isl_hermite_result_H_tester) DISABLED START */
	struct isl_hermite_result* stPtr = (struct isl_hermite_result *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getH");
	return (GECOS_PTRSIZE isl_mat_copy(stPtr->H)) != (GECOS_PTRSIZE NULL);
	/* PROTECTED REGION END */
}

JNIEXPORT jlong JNICALL
Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1hermite_1result_1get_1U
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(isl_hermite_result_U_getter) DISABLED START */
	struct isl_hermite_result* stPtr = (struct isl_hermite_result *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getU");
	return (jlong) GECOS_PTRSIZE isl_mat_copy(stPtr->U);
	/* PROTECTED REGION END */
}

JNIEXPORT void JNICALL
Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_hermite_result_1set_1U
	(JNIEnv *env, jclass class, jlong ptr, jlong value) {
	/* PROTECTED REGION ID(isl_hermite_result_U_setter) DISABLED START */
	struct isl_hermite_result* stPtr = (struct isl_hermite_result *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in setU");
	stPtr->U= (isl_mat*) GECOS_PTRSIZE value;
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1hermite_1result_1test_1U
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(isl_hermite_result_U_tester) DISABLED START */
	struct isl_hermite_result* stPtr = (struct isl_hermite_result *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getU");
	return (GECOS_PTRSIZE isl_mat_copy(stPtr->U)) != (GECOS_PTRSIZE NULL);
	/* PROTECTED REGION END */
}

JNIEXPORT jlong JNICALL
Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1hermite_1result_1get_1Q
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(isl_hermite_result_Q_getter) DISABLED START */
	struct isl_hermite_result* stPtr = (struct isl_hermite_result *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getQ");
	return (jlong) GECOS_PTRSIZE isl_mat_copy(stPtr->Q);
	/* PROTECTED REGION END */
}

JNIEXPORT void JNICALL
Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_hermite_result_1set_1Q
	(JNIEnv *env, jclass class, jlong ptr, jlong value) {
	/* PROTECTED REGION ID(isl_hermite_result_Q_setter) DISABLED START */
	struct isl_hermite_result* stPtr = (struct isl_hermite_result *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in setQ");
	stPtr->Q= (isl_mat*) GECOS_PTRSIZE value;
	/* PROTECTED REGION END */
}

JNIEXPORT jint JNICALL
Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1hermite_1result_1test_1Q
	(JNIEnv *env, jclass class, jlong ptr) {
	/* PROTECTED REGION ID(isl_hermite_result_Q_tester) DISABLED START */
	struct isl_hermite_result* stPtr = (struct isl_hermite_result *) GECOS_PTRSIZE ptr;
	if(stPtr==NULL)
		throwException(env, "Null Pointer in getQ");
	return (GECOS_PTRSIZE isl_mat_copy(stPtr->Q)) != (GECOS_PTRSIZE NULL);
	/* PROTECTED REGION END */
}


JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1hermite_1result_1free
(JNIEnv *env, jclass class, jlong result)
 {
#ifdef TRACE_ALL
	printf("Entering isl_hermite_result_free\n");fflush(stdout);
#endif
	struct isl_hermite_result* result_c = (struct isl_hermite_result*) GECOS_PTRSIZE result; 
	if(((void*)result_c)==NULL) {
		throwException(env, "Null pointer in isl_hermite_result_free for parameter result");
		goto error;
	}

	 isl_hermite_result_free(result_c);


#ifdef TRACE_ALL
	printf("Leaving isl_hermite_result_free\n");fflush(stdout);
#endif
	
error:
	return;
}


