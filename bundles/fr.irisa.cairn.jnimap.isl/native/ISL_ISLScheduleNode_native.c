#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <isl/aff.h>
#include <isl/aff_type.h>
#include <isl/arg.h>
#include <isl/ast_build.h>
#include <isl/ast.h>
#include <isl/ast_type.h>
#include <isl/constraint.h>
#include <isl/ctx.h>
#include <isl/flow.h>
#include <isl/hash.h>
#include <isl/id.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/ilp.h>
#include <isl/list.h>
#include <isl/local_space.h>
#include <isl/lp.h>
#include <isl/map.h>
#include <isl/map_to_basic_set.h>
#include <isl/map_type.h>
#include <isl/mat.h>
#include <isl/multi.h>
#include <isl/obj.h>
#include <isl/options.h>
#include <isl/point.h>
#include <isl/polynomial.h>
#include <isl/polynomial_type.h>
#include <isl/printer.h>
#include <isl/printer_type.h>
#include <isl/schedule.h>
#include <isl/schedule_node.h>
#include <isl/schedule_type.h>
#include <isl/set.h>
#include <isl/set_type.h>
#include <isl/space.h>
#include <isl/stdint.h>
#include <isl/stream.h>
#include <isl/union_map.h>
#include <isl/union_map_type.h>
#include <isl/union_set.h>
#include <isl/union_set_type.h>
#include <isl/val_gmp.h>
#include <isl/val.h>
#include <isl/vec.h>
#include <isl/version.h>
#include <isl/vertices.h>
#include <isl/fixed_box.h>
#include <isl/stride_info.h>
#include <isl/id_to_ast_expr.h>
#include <isl/id_to_id.h>
#include <isl/id_to_pw_aff.h>
#include <isl/map_to_basic_set.h>

#include "ISLUser_vertices.h"
#include "ISLUser_ast.h"
#include "ISLUser_schedule_tree.h"
#include "ISLUser_codegen.h"
#include "ISLUser_collections.h"
#include "ISLUser_scheduling.h"
#include "ISLUser_tostring.h"
#include "ISLUser_lexnext.h"
#include "ISLUser_lexpred.h"
#include "ISLUser_misc.h"
#include "ISLUser_polynomial.h"
#include "ISLUser_stdio.h"
#include "ISLUser_mat.h"

#include "fr_irisa_cairn_jnimap_isl_ISLNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1ctx
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_ctx\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_ctx for parameter node");
		goto error;
	}

	isl_ctx* res = (isl_ctx*) isl_schedule_node_get_ctx(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_ctx\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1copy
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_copy\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_copy for parameter node");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_copy(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1free
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_free\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_free for parameter node");
		goto error;
	}

	 isl_schedule_node_free(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1is_1equal
(JNIEnv *env, jclass class, jlong node1, jlong node2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_is_equal\n");fflush(stdout);
#endif
	isl_schedule_node* node1_c = (isl_schedule_node*) GECOS_PTRSIZE node1; 
	if(((void*)node1_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_is_equal for parameter node1");
		goto error;
	}
	isl_schedule_node* node2_c = (isl_schedule_node*) GECOS_PTRSIZE node2; 
	if(((void*)node2_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_is_equal for parameter node2");
		goto error;
	}

	int res = (int) isl_schedule_node_is_equal(node1_c, node2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_is_equal\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1type
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_type\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_type for parameter node");
		goto error;
	}

	enum isl_schedule_node_type res = (enum isl_schedule_node_type) isl_schedule_node_get_type(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_type\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return -1000;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1parent_1type
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_parent_type\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_parent_type for parameter node");
		goto error;
	}

	enum isl_schedule_node_type res = (enum isl_schedule_node_type) isl_schedule_node_get_parent_type(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_parent_type\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return -1000;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1schedule
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_schedule\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_schedule for parameter node");
		goto error;
	}

	isl_schedule* res = (isl_schedule*) isl_schedule_node_get_schedule(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_schedule\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1has_1parent
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_has_parent\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_has_parent for parameter node");
		goto error;
	}

	int res = (int) isl_schedule_node_has_parent(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_has_parent\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1parent
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_parent\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_parent for parameter node");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_parent(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_parent\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1root
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_root\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_root for parameter node");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_root(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_root\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1ancestor
(JNIEnv *env, jclass class, jlong node, jint generation)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_ancestor\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_ancestor for parameter node");
		goto error;
	}
	int generation_c = (int) generation;

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_ancestor(node_c, generation_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_ancestor\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1n_1children
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_n_children\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_n_children for parameter node");
		goto error;
	}

	int res = (int) isl_schedule_node_n_children(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_n_children\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1child
(JNIEnv *env, jclass class, jlong node, jint pos)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_child\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_child for parameter node");
		goto error;
	}
	int pos_c = (int) pos;

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_child(node_c, pos_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_child\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1has_1children
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_has_children\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_has_children for parameter node");
		goto error;
	}

	int res = (int) isl_schedule_node_has_children(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_has_children\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1first_1child
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_first_child\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_first_child for parameter node");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_first_child(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_first_child\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1has_1previous_1sibling
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_has_previous_sibling\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_has_previous_sibling for parameter node");
		goto error;
	}

	int res = (int) isl_schedule_node_has_previous_sibling(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_has_previous_sibling\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1previous_1sibling
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_previous_sibling\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_previous_sibling for parameter node");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_previous_sibling(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_previous_sibling\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1has_1next_1sibling
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_has_next_sibling\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_has_next_sibling for parameter node");
		goto error;
	}

	int res = (int) isl_schedule_node_has_next_sibling(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_has_next_sibling\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1next_1sibling
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_next_sibling\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_next_sibling for parameter node");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_next_sibling(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_next_sibling\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1tree_1depth
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_tree_depth\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_tree_depth for parameter node");
		goto error;
	}

	int res = (int) isl_schedule_node_get_tree_depth(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_tree_depth\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1child_1position
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_child_position\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_child_position for parameter node");
		goto error;
	}

	int res = (int) isl_schedule_node_get_child_position(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_child_position\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1ancestor_1child_1position
(JNIEnv *env, jclass class, jlong node, jlong ancestor)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_ancestor_child_position\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_ancestor_child_position for parameter node");
		goto error;
	}
	isl_schedule_node* ancestor_c = (isl_schedule_node*) GECOS_PTRSIZE ancestor; 
	if(((void*)ancestor_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_ancestor_child_position for parameter ancestor");
		goto error;
	}

	int res = (int) isl_schedule_node_get_ancestor_child_position(node_c, ancestor_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_ancestor_child_position\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1child
(JNIEnv *env, jclass class, jlong node, jint pos)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_child\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_child for parameter node");
		goto error;
	}
	int pos_c = (int) pos;

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_get_child(node_c, pos_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_child\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1shared_1ancestor
(JNIEnv *env, jclass class, jlong node1, jlong node2)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_shared_ancestor\n");fflush(stdout);
#endif
	isl_schedule_node* node1_c = (isl_schedule_node*) GECOS_PTRSIZE node1; 
	if(((void*)node1_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_shared_ancestor for parameter node1");
		goto error;
	}
	isl_schedule_node* node2_c = (isl_schedule_node*) GECOS_PTRSIZE node2; 
	if(((void*)node2_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_shared_ancestor for parameter node2");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_get_shared_ancestor(node1_c, node2_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_shared_ancestor\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1cut
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_cut\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_cut for parameter node");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_cut(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_cut\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1delete
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_delete\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_delete for parameter node");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_delete(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_delete\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1is_1subtree_1anchored
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_is_subtree_anchored\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_is_subtree_anchored for parameter node");
		goto error;
	}

	int res = (int) isl_schedule_node_is_subtree_anchored(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_is_subtree_anchored\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1reset_1user
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_reset_user\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_reset_user for parameter node");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_reset_user(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_reset_user\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1align_1params
(JNIEnv *env, jclass class, jlong node, jlong space)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_align_params\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_align_params for parameter node");
		goto error;
	}
	isl_space* space_c = (isl_space*) GECOS_PTRSIZE space; 
	if(((void*)space_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_align_params for parameter space");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_align_params(node_c, space_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_align_params\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1prefix_1schedule_1multi_1union_1pw_1aff
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_prefix_schedule_multi_union_pw_aff\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_prefix_schedule_multi_union_pw_aff for parameter node");
		goto error;
	}

	isl_multi_union_pw_aff* res = (isl_multi_union_pw_aff*) isl_schedule_node_get_prefix_schedule_multi_union_pw_aff(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_prefix_schedule_multi_union_pw_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1prefix_1schedule_1union_1pw_1multi_1aff
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_prefix_schedule_union_pw_multi_aff\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_prefix_schedule_union_pw_multi_aff for parameter node");
		goto error;
	}

	isl_union_pw_multi_aff* res = (isl_union_pw_multi_aff*) isl_schedule_node_get_prefix_schedule_union_pw_multi_aff(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_prefix_schedule_union_pw_multi_aff\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1prefix_1schedule_1union_1map
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_prefix_schedule_union_map\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_prefix_schedule_union_map for parameter node");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_schedule_node_get_prefix_schedule_union_map(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_prefix_schedule_union_map\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1prefix_1schedule_1relation
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_prefix_schedule_relation\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_prefix_schedule_relation for parameter node");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_schedule_node_get_prefix_schedule_relation(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_prefix_schedule_relation\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1subtree_1schedule_1union_1map
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_subtree_schedule_union_map\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_subtree_schedule_union_map for parameter node");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_schedule_node_get_subtree_schedule_union_map(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_subtree_schedule_union_map\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1subtree_1expansion
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_subtree_expansion\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_subtree_expansion for parameter node");
		goto error;
	}

	isl_union_map* res = (isl_union_map*) isl_schedule_node_get_subtree_expansion(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_subtree_expansion\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1subtree_1contraction
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_subtree_contraction\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_subtree_contraction for parameter node");
		goto error;
	}

	isl_union_pw_multi_aff* res = (isl_union_pw_multi_aff*) isl_schedule_node_get_subtree_contraction(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_subtree_contraction\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jint JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1schedule_1depth
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_schedule_depth\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_schedule_depth for parameter node");
		goto error;
	}

	int res = (int) isl_schedule_node_get_schedule_depth(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_schedule_depth\n");fflush(stdout);
#endif
	

	return (jint)  res;
error:
	return  (jint) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1domain
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_domain\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_domain for parameter node");
		goto error;
	}

	isl_union_set* res = (isl_union_set*) isl_schedule_node_get_domain(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1get_1universe_1domain
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_get_universe_domain\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_get_universe_domain for parameter node");
		goto error;
	}

	isl_union_set* res = (isl_union_set*) isl_schedule_node_get_universe_domain(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_get_universe_domain\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1insert_1partial_1schedule
(JNIEnv *env, jclass class, jlong node, jlong schedule)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_insert_partial_schedule\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_partial_schedule for parameter node");
		goto error;
	}
	isl_multi_union_pw_aff* schedule_c = (isl_multi_union_pw_aff*) GECOS_PTRSIZE schedule; 
	if(((void*)schedule_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_partial_schedule for parameter schedule");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_insert_partial_schedule(node_c, schedule_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_insert_partial_schedule\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1insert_1context
(JNIEnv *env, jclass class, jlong node, jlong context)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_insert_context\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_context for parameter node");
		goto error;
	}
	isl_set* context_c = (isl_set*) GECOS_PTRSIZE context; 
	if(((void*)context_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_context for parameter context");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_insert_context(node_c, context_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_insert_context\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1insert_1filter
(JNIEnv *env, jclass class, jlong node, jlong filter)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_insert_filter\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_filter for parameter node");
		goto error;
	}
	isl_union_set* filter_c = (isl_union_set*) GECOS_PTRSIZE filter; 
	if(((void*)filter_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_filter for parameter filter");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_insert_filter(node_c, filter_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_insert_filter\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1insert_1guard
(JNIEnv *env, jclass class, jlong node, jlong guard)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_insert_guard\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_guard for parameter node");
		goto error;
	}
	isl_set* guard_c = (isl_set*) GECOS_PTRSIZE guard; 
	if(((void*)guard_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_guard for parameter guard");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_insert_guard(node_c, guard_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_insert_guard\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1insert_1mark
(JNIEnv *env, jclass class, jlong node, jlong mark)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_insert_mark\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_mark for parameter node");
		goto error;
	}
	isl_id* mark_c = (isl_id*) GECOS_PTRSIZE mark; 
	if(((void*)mark_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_mark for parameter mark");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_insert_mark(node_c, mark_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_insert_mark\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1insert_1sequence
(JNIEnv *env, jclass class, jlong node, jlong filters)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_insert_sequence\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_sequence for parameter node");
		goto error;
	}
	isl_union_set_list* filters_c = (isl_union_set_list*) GECOS_PTRSIZE filters; 
	if(((void*)filters_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_sequence for parameter filters");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_insert_sequence(node_c, filters_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_insert_sequence\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1insert_1set
(JNIEnv *env, jclass class, jlong node, jlong filters)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_insert_set\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_set for parameter node");
		goto error;
	}
	isl_union_set_list* filters_c = (isl_union_set_list*) GECOS_PTRSIZE filters; 
	if(((void*)filters_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_insert_set for parameter filters");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_insert_set(node_c, filters_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_insert_set\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1group
(JNIEnv *env, jclass class, jlong node, jlong group_id)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_group\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_group for parameter node");
		goto error;
	}
	isl_id* group_id_c = (isl_id*) GECOS_PTRSIZE group_id; 
	if(((void*)group_id_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_group for parameter group_id");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_group(node_c, group_id_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_group\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1order_1before
(JNIEnv *env, jclass class, jlong node, jlong filter)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_order_before\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_order_before for parameter node");
		goto error;
	}
	isl_union_set* filter_c = (isl_union_set*) GECOS_PTRSIZE filter; 
	if(((void*)filter_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_order_before for parameter filter");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_order_before(node_c, filter_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_order_before\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1order_1after
(JNIEnv *env, jclass class, jlong node, jlong filter)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_order_after\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_order_after for parameter node");
		goto error;
	}
	isl_union_set* filter_c = (isl_union_set*) GECOS_PTRSIZE filter; 
	if(((void*)filter_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_order_after for parameter filter");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_order_after(node_c, filter_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_order_after\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1graft_1before
(JNIEnv *env, jclass class, jlong node, jlong graft)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_graft_before\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_graft_before for parameter node");
		goto error;
	}
	isl_schedule_node* graft_c = (isl_schedule_node*) GECOS_PTRSIZE graft; 
	if(((void*)graft_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_graft_before for parameter graft");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_graft_before(node_c, graft_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_graft_before\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1graft_1after
(JNIEnv *env, jclass class, jlong node, jlong graft)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_graft_after\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_graft_after for parameter node");
		goto error;
	}
	isl_schedule_node* graft_c = (isl_schedule_node*) GECOS_PTRSIZE graft; 
	if(((void*)graft_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_graft_after for parameter graft");
		goto error;
	}

	isl_schedule_node* res = (isl_schedule_node*) isl_schedule_node_graft_after(node_c, graft_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_graft_after\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1to_1str
(JNIEnv *env, jclass class, jlong node)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_to_str\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_to_str for parameter node");
		goto error;
	}

	char * res = (char *) isl_schedule_node_to_str(node_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_to_str\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_isl_ISLNative_isl_1schedule_1node_1to_1string
(JNIEnv *env, jclass class, jlong node, jint format)
 {
#ifdef TRACE_ALL
	printf("Entering isl_schedule_node_to_string\n");fflush(stdout);
#endif
	isl_schedule_node* node_c = (isl_schedule_node*) GECOS_PTRSIZE node; 
	if(((void*)node_c)==NULL) {
		throwException(env, "Null pointer in isl_schedule_node_to_string for parameter node");
		goto error;
	}
	int format_c = (int) format;

	char * res = (char *) isl_schedule_node_to_string(node_c, format_c);


#ifdef TRACE_ALL
	printf("Leaving isl_schedule_node_to_string\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}


