package fr.irisa.cairn.jnimap.barvinok;

public class BarvinokException extends Exception {
	private static final long serialVersionUID = 1L;
	public BarvinokException(String msg) {
		super(msg);
	} 
}
