package fr.irisa.cairn.jnimap.barvinok;

import fr.irisa.cairn.jnimap.barvinok.platform.*;
import fr.irisa.cairn.jnimap.polylib.PolylibNative;
import fr.irisa.cairn.jnimap.isl.ISLNative;

public class BarvinokNative {

	private static final String arch = System.getProperty("sun.arch.data.model");

	private static JNIBarvinokAbstractPlatform loader ;

	private static boolean loaded = false;
	public static void loadLibrary() {
		if (loaded) return;
		PolylibNative.loadLibrary();
		ISLNative.loadLibrary();
		String osName = System.getProperty("os.name");
		String arch   = System.getProperty("sun.arch.data.model");
		if (osName.equals("Linux")) {
			if (arch.equals("32")) {
				throw new RuntimeException("Linux_32 is not supported.");
			} else {
				loader = new JNIBarvinokLinux_64();
			}
		} else if (osName.equals("Windows XP")) {
			loader = new JNIBarvinokCygwin_32();
		} else if (osName.equals("Mac OS X")) {
			if (arch.equals("32")) {
				throw new RuntimeException("Macosx_32 is not supported.");
			} else {
				loader = new JNIBarvinokMacosx_64();
			} 
		} else  {
			throw new RuntimeException("Unsupported Operating system : " + osName+"_"+arch);
		}
		loader.loadPlatformLibraries();
		loaded = true;
	}
	
	static {
		loadLibrary();
	}

	public static native long derefPointer(long ptr);
	
	
	/*************************************************************
	 *	                     USER MODULES                        *
	 *************************************************************/
	 
	 public static class UserModules {
	}
	
	/*************************************************************
	 *	                        METHODS                          *
	 *************************************************************/
	 public static native long isl_map_card(long map);
	 public static native long isl_set_card(long set);
	 
	/*************************************************************
	 *	                    CLASS MAPPING                        *
	 *************************************************************/
	


}
