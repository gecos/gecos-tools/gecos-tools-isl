package fr.irisa.cairn.jnimap.barvinok.platform;

import java.io.File;

public class JNIBarvinokLinux_64 extends JNIBarvinokAbstractPlatform {

	@Override
	protected String getDllPath() {
		return new File(".")+File.separator+"./.jnimap.temp.linux_64";
	}

	public void loadPlatformLibraries() {
		// Get input stream from jar resource
		String lib = "linux_64_libjnibarvinok.so";
		try {
			//Copy other dynamic libraries from jar to temporary location
			//Copy Barvinok
			copyLibToTemp("Barvinok_linux_64" + File.separator, "libbarvinok.so.23");
			//Copy NTL
			copyLibToTemp("Barvinok_linux_64" + File.separator, "libntl.so.39");
			//Copy the binding object file from jar to temporary location
			File JNIDLL = copyLibToTemp("", lib);
			// Finally, load the dll
			System.load(JNIDLL.getAbsolutePath());
		} catch (Exception e) {
			throw new RuntimeException("Problem during native library loading Barvinok:",e);
		}
	}
}
