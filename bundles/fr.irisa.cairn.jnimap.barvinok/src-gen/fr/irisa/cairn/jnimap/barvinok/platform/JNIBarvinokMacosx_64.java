package fr.irisa.cairn.jnimap.barvinok.platform;

import java.io.File;

public class JNIBarvinokMacosx_64 extends JNIBarvinokAbstractPlatform {

	@Override
	protected String getDllPath() {
		return new File(".")+File.separator+"./.jnimap.temp.macosx_64";
	}

	public void loadPlatformLibraries() {
		// Get input stream from jar resource
		String lib;
		String libDirectory;
		if (System.getProperty("os.arch").equals("aarch64")) {
			// M1 macs
			lib = "macosx_arm64_libjnibarvinok.so";
			libDirectory = "Barvinok_macosx_arm64";
		} else {
			// Intel macs
			lib = "macosx_64_libjnibarvinok.so";
			libDirectory = "Barvinok_macosx_64";
		}
		try {
			//Copy other dynamic libraries from jar to temporary location
			//Copy Barvinok
			copyLibToTemp(libDirectory + File.separator, "libbarvinok.23.dylib");
			//Copy NTL
			copyLibToTemp(libDirectory + File.separator, "libntl.39.dylib");
			//Copy the binding object file from jar to temporary location
			File JNIDLL = copyLibToTemp("", lib);
			// Finally, load the dll
			System.load(JNIDLL.getAbsolutePath());
		} catch (Exception e) {
			throw new RuntimeException("Problem during native library loading Barvinok:",e);
		}
	}
}
