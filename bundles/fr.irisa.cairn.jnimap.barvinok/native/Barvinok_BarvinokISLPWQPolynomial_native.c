#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <barvinok/barvinok.h>
#include <isl/set.h>
#include <isl/map.h>

#include "BarvinokUser_tostring.h"

#include "fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative_isl_1pw_1qpolynomial_1sum
(JNIEnv *env, jclass class, jlong pwqp)
 {
#ifdef TRACE_ALL
	printf("Entering isl_pw_qpolynomial_sum\n");fflush(stdout);
#endif
	isl_pw_qpolynomial* pwqp_c = (isl_pw_qpolynomial*) GECOS_PTRSIZE pwqp; 
	if(((void*)pwqp_c)==NULL) {
		throwException(env, "Null pointer in isl_pw_qpolynomial_sum for parameter pwqp");
		goto error;
	}

	isl_pw_qpolynomial* res = (isl_pw_qpolynomial*) isl_pw_qpolynomial_sum(pwqp_c);


#ifdef TRACE_ALL
	printf("Leaving isl_pw_qpolynomial_sum\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative_isl_1pw_1qpolynomial_1copy
(JNIEnv *env, jclass class, jlong pwqp)
 {
#ifdef TRACE_ALL
	printf("Entering isl_pw_qpolynomial_copy\n");fflush(stdout);
#endif
	isl_pw_qpolynomial* pwqp_c = (isl_pw_qpolynomial*) GECOS_PTRSIZE pwqp; 
	if(((void*)pwqp_c)==NULL) {
		throwException(env, "Null pointer in isl_pw_qpolynomial_copy for parameter pwqp");
		goto error;
	}

	isl_pw_qpolynomial* res = (isl_pw_qpolynomial*) isl_pw_qpolynomial_copy(pwqp_c);


#ifdef TRACE_ALL
	printf("Leaving isl_pw_qpolynomial_copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative_isl_1pw_1qpolynomial_1free
(JNIEnv *env, jclass class, jlong pwqp)
 {
#ifdef TRACE_ALL
	printf("Entering isl_pw_qpolynomial_free\n");fflush(stdout);
#endif
	isl_pw_qpolynomial* pwqp_c = (isl_pw_qpolynomial*) GECOS_PTRSIZE pwqp; 
	if(((void*)pwqp_c)==NULL) {
		throwException(env, "Null pointer in isl_pw_qpolynomial_free for parameter pwqp");
		goto error;
	}

	 isl_pw_qpolynomial_free(pwqp_c);


#ifdef TRACE_ALL
	printf("Leaving isl_pw_qpolynomial_free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jstring JNICALL Java_fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative_isl_1pw_1qpolynomial_1to_1string
(JNIEnv *env, jclass class, jlong set, jint format)
 {
#ifdef TRACE_ALL
	printf("Entering isl_pw_qpolynomial_to_string\n");fflush(stdout);
#endif
	isl_pw_qpolynomial* set_c = (isl_pw_qpolynomial*) GECOS_PTRSIZE set; 
	if(((void*)set_c)==NULL) {
		throwException(env, "Null pointer in isl_pw_qpolynomial_to_string for parameter set");
		goto error;
	}
	int format_c = (int) format;

	char * res = (char *) isl_pw_qpolynomial_to_string(set_c, format_c);


#ifdef TRACE_ALL
	printf("Leaving isl_pw_qpolynomial_to_string\n");fflush(stdout);
#endif
	

	return (*env)->NewStringUTF(env, res);

error:
	return (jstring) GECOS_PTRSIZE NULL;
}


