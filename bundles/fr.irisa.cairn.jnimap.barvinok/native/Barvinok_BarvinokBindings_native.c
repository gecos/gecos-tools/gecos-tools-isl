#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <barvinok/barvinok.h>
#include <isl/set.h>
#include <isl/map.h>


#include "fr_irisa_cairn_jnimap_barvinok_BarvinokNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_barvinok_BarvinokNative_isl_1map_1card
(JNIEnv *env, jclass class, jlong map)
 {
#ifdef TRACE_ALL
	printf("Entering isl_map_card\n");fflush(stdout);
#endif
	isl_map* map_c = (isl_map*) GECOS_PTRSIZE map; 
	if(((void*)map_c)==NULL) {
		throwException(env, "Null pointer in isl_map_card for parameter map");
		goto error;
	}

	isl_pw_qpolynomial* res = (isl_pw_qpolynomial*) isl_map_card(map_c);


#ifdef TRACE_ALL
	printf("Leaving isl_map_card\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_barvinok_BarvinokNative_isl_1set_1card
(JNIEnv *env, jclass class, jlong set)
 {
#ifdef TRACE_ALL
	printf("Entering isl_set_card\n");fflush(stdout);
#endif
	isl_set* set_c = (isl_set*) GECOS_PTRSIZE set; 
	if(((void*)set_c)==NULL) {
		throwException(env, "Null pointer in isl_set_card for parameter set");
		goto error;
	}

	isl_pw_qpolynomial* res = (isl_pw_qpolynomial*) isl_set_card(set_c);


#ifdef TRACE_ALL
	printf("Leaving isl_set_card\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}


