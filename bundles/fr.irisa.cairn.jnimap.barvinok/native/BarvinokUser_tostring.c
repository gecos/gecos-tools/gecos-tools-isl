#include "BarvinokUser_tostring.h"

/* PROTECTED REGION ID(BarvinokUser_tostring_local) ENABLED START */
	/* Protected region for methods used locally in this file */
/* PROTECTED REGION END */

char * isl_pw_qpolynomial_to_string(isl_pw_qpolynomial* set, int format) {
	/* PROTECTED REGION ID(BarvinokUser_tostring_isl_pw_qpolynomial_to_string) ENABLED START */
#ifdef TRACE_ALL
        printf("Entering isl_pw_qpolynomial_to_string\n");fflush(stdout);
#endif
        struct isl_printer *pr = isl_printer_to_str(isl_pw_qpolynomial_get_ctx(set));
        pr = isl_printer_set_output_format(pr, format);
        isl_printer_print_pw_qpolynomial(pr,set);
        char *buffer = isl_printer_get_str(pr);

        isl_printer_flush(pr);
        isl_printer_free(pr);
#ifdef TRACE_ALL
        printf("Leaving isl_pw_qpolynomial_to_string\n");fflush(stdout);
#endif
        return buffer;
	/* PROTECTED REGION END */
}
