#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <barvinok/barvinok.h>
#include <isl/set.h>
#include <isl/map.h>

#include "BarvinokUser_tostring.h"

#include "fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative_isl_1map_1card
(JNIEnv *env, jclass class, jlong map)
 {
#ifdef TRACE_ALL
	printf("Entering isl_map_card\n");fflush(stdout);
#endif
	isl_map* map_c = (isl_map*) GECOS_PTRSIZE map; 
	if(((void*)map_c)==NULL) {
		throwException(env, "Null pointer in isl_map_card for parameter map");
		goto error;
	}

	isl_pw_qpolynomial* res = (isl_pw_qpolynomial*) isl_map_card(map_c);


#ifdef TRACE_ALL
	printf("Leaving isl_map_card\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative_isl_1map_1copy
(JNIEnv *env, jclass class, jlong map)
 {
#ifdef TRACE_ALL
	printf("Entering isl_map_copy\n");fflush(stdout);
#endif
	isl_map* map_c = (isl_map*) GECOS_PTRSIZE map; 
	if(((void*)map_c)==NULL) {
		throwException(env, "Null pointer in isl_map_copy for parameter map");
		goto error;
	}

	isl_map* res = (isl_map*) isl_map_copy(map_c);


#ifdef TRACE_ALL
	printf("Leaving isl_map_copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative_isl_1map_1free
(JNIEnv *env, jclass class, jlong map)
 {
#ifdef TRACE_ALL
	printf("Entering isl_map_free\n");fflush(stdout);
#endif
	isl_map* map_c = (isl_map*) GECOS_PTRSIZE map; 
	if(((void*)map_c)==NULL) {
		throwException(env, "Null pointer in isl_map_free for parameter map");
		goto error;
	}

	 isl_map_free(map_c);


#ifdef TRACE_ALL
	printf("Leaving isl_map_free\n");fflush(stdout);
#endif
	
error:
	return;
}


