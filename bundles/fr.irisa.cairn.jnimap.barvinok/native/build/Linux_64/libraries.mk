#PROTECTED REGION ID(libraries_Linux_64) ENABLED START#
      Barvinok_INCDIR= $(HOME)/complibs/usr/local/include/
      Barvinok_LIBDIR= $(HOME)/complibs/usr/local/lib/
      
      NTL_INCDIR= $(HOME)/complibs/usr/local/include/
      NTL_LIBDIR= $(HOME)/complibs/usr/local/lib/
      
      ISL_LOCATION=$(HOME)/projects/GeCoS/Tools/gecos-tools-isl/bundles//fr.irisa.cairn.jnimap.isl/
      Polylib_LOCATION=$(HOME)/projects/GeCoS/Tools/gecos-tools-isl/bundles/fr.irisa.cairn.jnimap.polylib/
#PROTECTED REGION END#

PolyLib_LIBDIR=${Polylib_LOCATION}/lib/Polylib_linux_64/
ISL_LIBDIR=${ISL_LOCATION}/lib/ISL_linux_64/
GMP_LIBDIR=${ISL_LOCATION}/lib/ISL_linux_64/
