METHOD=GIT
VERSION=0.41.2

# barvinok git repo name
GITREP=barvinok

### if METHOD = GIT, clone from GITADDR @head or @ CHECKOUT if specified
GITADDR=git://repo.or.cz/barvinok.git
CHECKOUT=barvinok-$(VERSION)

### else, download archive (FILE) from URL and unzip  
DLDIR=$(GITREP)-$(VERSION)
FILE=$(DLDIR).tar.bz2
URL=http://barvinok.gforge.inria.fr/$(FILE)

ifeq ($(METHOD),GIT)
	FILE = 
	DIR=$(GITREP)
else
	DIR=$(DLDIR)
endif


