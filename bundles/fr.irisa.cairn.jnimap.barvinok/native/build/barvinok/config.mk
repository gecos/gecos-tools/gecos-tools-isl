####################
##    BARVINOK    ##
####################
ifeq ($(LIB),barvinok)
CFG_OPTIONS= \
--host=$(HOST) \
--prefix=$(PREFIX_BASE) \
--with-pic \
--disable-static \
--enable-shared-barvinok \
--enable-portable-binary \
--with-clang=no \
--with-glpk=no \
--without-zsolve \
--with-isl-prefix=$(PREFIX_BASE)\
--with-polylib-prefix=$(PREFIX_BASE)\
--with-gmp-prefix=$(PREFIX_BASE)\
--with-ntl-prefix=$(PREFIX_BASE)
#--with-gmp=system
endif
