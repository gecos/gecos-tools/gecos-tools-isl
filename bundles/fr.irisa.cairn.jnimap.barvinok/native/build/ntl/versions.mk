METHOD=#GIT
VERSION=11.3.2

# barvinok git repo name
GITREP=ntl

#no git for ntl
### if METHOD = GIT, clone from GITADDR @head or @ CHECKOUT if specified
#GITADDR=git://repo.or.cz/barvinok.git
#CHECKOUT=d2f282cbf206f1c92aaea0ea7ccff1709220414c

### else, download archive (FILE) from URL and unzip  
DLDIR=$(GITREP)-$(VERSION)
FILE=$(DLDIR).tar.gz
URL=https://www.shoup.net/ntl/$(FILE)

ifeq ($(METHOD),GIT)
	FILE = 
	DIR=$(GITREP)
else
	DIR=$(DLDIR)/src
endif


