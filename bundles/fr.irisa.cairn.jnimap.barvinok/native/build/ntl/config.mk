##############
##    NTL   ##
##############
ifeq ($(LIB),ntl)
CFG_OPTIONS= \
CXXFLAGS="-g -O2 -fno-builtin -m64"\
PREFIX=$(PREFIX_BASE)\
SHARED=on\
NTL_GMP_LIP=on\
GMP_PREFIX=$(PREFIX_BASE)\
NTL_THREADS=off \
NTL_THREAD_BOOST=off
endif
