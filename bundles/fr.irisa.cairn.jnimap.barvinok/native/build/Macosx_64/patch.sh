#!/bin/bash

library=$1
version=$2

if [[ "$library" -eq "Polylib" ]]; then
  llibrary=polylibgmp
else
  llibrary=`echo $library | tr '[:upper:]' '[:lower:]'`
fi

sed -i '' 's~lib'${llibrary}'.'${version}'~lib'${llibrary}'.${'${library}'_LIBVERSION}~g' Makefile
