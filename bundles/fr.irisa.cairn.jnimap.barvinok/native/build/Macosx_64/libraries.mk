#PROTECTED REGION ID(libraries_Macosx_64) ENABLED START#

      Barvinok_INCDIR=$(HOME)/complibs/usr/local/include
      Barvinok_LIBDIR=$(HOME)/complibs/usr/local/lib
      Barvinok_LIBVERSION=23

      NTL_INCDIR=$(HOME)/complibs/usr/local/include
      NTL_LIBDIR=$(HOME)/complibs/usr/local/lib
      NTL_LIBVERSION=39

      PolyLib_INCDIR=$(HOME)/complibs/usr/local/include
      PolyLib_LIBDIR=$(HOME)/complibs/usr/local/lib
      PolyLib_LIBVERSION=8

      ISL_INCDIR=$(HOME)/complibs/usr/local/include
      ISL_LIBDIR=$(HOME)/complibs/usr/local/lib
      ISL_LIBVERSION=22

      GMP_INCDIR=$(HOME)/complibs/usr/local/include
      GMP_LIBDIR=$(HOME)/complibs/usr/local/lib
      GMP_LIBVERSION=10

#PROTECTED REGION END#

