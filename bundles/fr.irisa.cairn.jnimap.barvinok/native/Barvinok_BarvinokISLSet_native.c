#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <barvinok/barvinok.h>
#include <isl/set.h>
#include <isl/map.h>

#include "BarvinokUser_tostring.h"

#include "fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative.h"

extern void throwException(JNIEnv * env, char* msg);
extern jobject createInteger(JNIEnv * env, int value);
extern jint getIntegerValue(JNIEnv * env, jobject obj);


JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative_isl_1set_1card
(JNIEnv *env, jclass class, jlong set)
 {
#ifdef TRACE_ALL
	printf("Entering isl_set_card\n");fflush(stdout);
#endif
	isl_set* set_c = (isl_set*) GECOS_PTRSIZE set; 
	if(((void*)set_c)==NULL) {
		throwException(env, "Null pointer in isl_set_card for parameter set");
		goto error;
	}

	isl_pw_qpolynomial* res = (isl_pw_qpolynomial*) isl_set_card(set_c);


#ifdef TRACE_ALL
	printf("Leaving isl_set_card\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative_isl_1set_1copy
(JNIEnv *env, jclass class, jlong set)
 {
#ifdef TRACE_ALL
	printf("Entering isl_set_copy\n");fflush(stdout);
#endif
	isl_set* set_c = (isl_set*) GECOS_PTRSIZE set; 
	if(((void*)set_c)==NULL) {
		throwException(env, "Null pointer in isl_set_copy for parameter set");
		goto error;
	}

	isl_set* res = (isl_set*) isl_set_copy(set_c);


#ifdef TRACE_ALL
	printf("Leaving isl_set_copy\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}
JNIEXPORT void JNICALL Java_fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative_isl_1set_1free
(JNIEnv *env, jclass class, jlong set)
 {
#ifdef TRACE_ALL
	printf("Entering isl_set_free\n");fflush(stdout);
#endif
	isl_set* set_c = (isl_set*) GECOS_PTRSIZE set; 
	if(((void*)set_c)==NULL) {
		throwException(env, "Null pointer in isl_set_free for parameter set");
		goto error;
	}

	 isl_set_free(set_c);


#ifdef TRACE_ALL
	printf("Leaving isl_set_free\n");fflush(stdout);
#endif
	
error:
	return;
}
JNIEXPORT jlong JNICALL Java_fr_irisa_cairn_jnimap_barvinok_jni_BarvinokNative_isl_1set_1apply_1pw_1qpolynomial
(JNIEnv *env, jclass class, jlong set, jlong pwqp)
 {
#ifdef TRACE_ALL
	printf("Entering isl_set_apply_pw_qpolynomial\n");fflush(stdout);
#endif
	isl_set* set_c = (isl_set*) GECOS_PTRSIZE set; 
	if(((void*)set_c)==NULL) {
		throwException(env, "Null pointer in isl_set_apply_pw_qpolynomial for parameter set");
		goto error;
	}
	isl_pw_qpolynomial* pwqp_c = (isl_pw_qpolynomial*) GECOS_PTRSIZE pwqp; 
	if(((void*)pwqp_c)==NULL) {
		throwException(env, "Null pointer in isl_set_apply_pw_qpolynomial for parameter pwqp");
		goto error;
	}

	isl_pw_qpolynomial* res = (isl_pw_qpolynomial*) isl_set_apply_pw_qpolynomial(set_c, pwqp_c);


#ifdef TRACE_ALL
	printf("Leaving isl_set_apply_pw_qpolynomial\n");fflush(stdout);
#endif
	

	return (jlong) GECOS_PTRSIZE res;
error:
	return  (jlong) GECOS_PTRSIZE NULL;
}


