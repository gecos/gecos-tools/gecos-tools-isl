#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <barvinok/barvinok.h>
#include <isl/set.h>
#include <isl/map.h>

/* PROTECTED REGION ID(BarvinokUser_tostring_header) ENABLED START */
	 /* Custom includes for this module */
/* PROTECTED REGION END */

char * isl_pw_qpolynomial_to_string(isl_pw_qpolynomial* set, int format);
