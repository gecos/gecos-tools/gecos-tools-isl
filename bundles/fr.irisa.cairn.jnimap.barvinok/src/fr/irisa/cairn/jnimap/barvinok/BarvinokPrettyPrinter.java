package fr.irisa.cairn.jnimap.barvinok;

import fr.irisa.cairn.jnimap.isl.ISLPrettyPrinter;
import fr.irisa.cairn.jnimap.runtime.JNIObject;

/**
 * 
 * @author amorvan
 *
 */
public class BarvinokPrettyPrinter {

	public static String asString(JNIObject jniObj) {
		return ISLPrettyPrinter.asString(jniObj);
	}
}
