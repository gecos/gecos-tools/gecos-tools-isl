package fr.irisa.cairn.jnimap.barvinok;

import java.util.List;

import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLFold;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLPWQPolynomial;
import fr.irisa.cairn.jnimap.isl.ISLPWQPolynomialFold;
import fr.irisa.cairn.jnimap.isl.ISLQPolynomial;
import fr.irisa.cairn.jnimap.isl.ISLQPolynomialFold;
import fr.irisa.cairn.jnimap.isl.ISLQPolynomialFoldPiece;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLVal;

/**
 * 
 * Allows to call Barvinok functions on JNIISL objects.
 * 
 * @author amorvan
 *
 */
public class BarvinokFunctions {
	
	public static boolean debug = false;

	public static ISLPWQPolynomial card(ISLMap map) {
		return BarvinokBindings.card(map);
		//return islPWQPFromBarvinok(barvinokMapFromISL(map.copy().makeDisjoint().coalesce()).card());
	}
	
	public static ISLPWQPolynomial card(ISLSet set) {
		return BarvinokBindings.card(set);
	}
	
//	public static JNIISLPWQPolynomial apply(JNIISLSet set, JNIISLPWQPolynomial poly) {
//		return islPWQPFromBarvinok(barvinokSetFromISL(set).apply(barvinokPWQPFromIsl(poly)));
//	}
//
//	public static JNIISLPWQPolynomial sum(JNIISLPWQPolynomial poly) {
//		return islPWQPFromBarvinok(barvinokPWQPFromIsl(poly).sum());
//	}

	public static long absoluteCard(ISLMap map, boolean maximize) {
		//the cardinal is always positive
		long res = maximize?0:Long.MAX_VALUE;
		int nbParam = map.getNbParams();
		// move parameters into "in" dimensions so that the cardinal becomes
		// non-parametrized, but with same expression(s).
		ISLMap finalMap = map.copy().moveDims(ISLDimType.isl_dim_in, 0, ISLDimType.isl_dim_param, 0, nbParam);
		// non parametrized cardinal
		ISLPWQPolynomial card = card(finalMap.copy());
		if (debug) {
			System.out.println("map   = "+map);
			System.out.println("card  = "+card);
			// parametrized cardinal to compare expressions.
			ISLPWQPolynomial cardOrig = card(map.copy());
			System.out.println("Pcard = "+cardOrig);
		}
		
		if (!maximize) {
			/*
			 * The piecewise polynomial is only defined when the cardinal is greater
			 * than 0. Therefore, for all the points in the difference between the
			 * definition domain of the relation and the definition domain of the
			 * polynomial, the cardinal equals 0. Since the cardinal of a relation
			 * is always positive, if this difference is not empty, then the minimum
			 * is 0.
			 */
			ISLSet origDomain = finalMap.copy().domain();
			ISLSet cardDomain = card.copy().domain();
			ISLSet subtract = origDomain.subtract(cardDomain);
			if (!subtract.isEmpty()) {
				res = 0;
			}
		}
		
		if (maximize || res != 0) {
			ISLPWQPolynomialFold bound = maximize?
					card.copy().bound(ISLFold.isl_fold_max).coalesce():
					card.copy().bound(ISLFold.isl_fold_min).coalesce();
			if (debug) System.out.println("bound = "+bound);
			List<ISLQPolynomialFoldPiece> liftedPieces = bound.getLiftedPieces();
			if (liftedPieces.size() == 0) 
				res = 0;
			else {
				main: for (ISLQPolynomialFoldPiece p : liftedPieces) {
					ISLQPolynomialFold fold = p.getFold();
					for (ISLQPolynomial qp : fold.getQPolynomials()) {
						if (qp.isConstant()) {
							ISLVal constantVal = qp.getConstantVal();
							// if the value is not integer, make a pessimist
							// flooring/ceiling of the value 
							constantVal = maximize?constantVal.floor():constantVal.ceil();
							long n = constantVal.getNumerator();
							if (n < 0) {
								res = -1;
								break main;
							} else {
								res = maximize?Math.max(res,n):Math.min(res,n);
							}
						} else {
							//unable to bound the cardinal by a constant
							res = -1;
							break main;
						}
					}
				}
			}
		}
		if (debug) System.out.println((maximize?"max":"min")+" card  = "+res+"\n********************");
		return res;
	}
	
	
	/**
	 * Given a map, the card() method returns the number of points in the range
	 * associated to each point in the domain, as a function of the parameters
	 * and the input indices. This method compute the minimum value it can take
	 * in the parameter domain. Returns -1 if it can not compute the minimum.
	 * 
	 * @param map
	 * @return
	 */
	public static long absoluteMinimumCard(ISLMap map) {
		return absoluteCard(map, false);
//		long min = Long.MAX_VALUE;
//		int nbParam = map.getNbParams();
//		// move parameters into "in" dimensions so that the cardinal becomes
//		// non-parametrized, but with same expression(s).
//		JNIISLMap finalMap = map.copy().moveDims(JNIISLDimType.isl_dim_in, 0, JNIISLDimType.isl_dim_param, 0, nbParam);
//		// non parametrized cardinal
//		JNIISLPWQPolynomial card = card(finalMap);
//		if (debug) {
//			System.out.println("map   = "+map);
//			System.out.println("card  = "+card);
//			// parametrized cardinal to compare expressions.
//			JNIISLPWQPolynomial cardOrig = card(map.copy());
//			System.out.println("Pcard = "+cardOrig);
//		}
//		JNIISLPWQPolynomialFold bound = card.copy().bound(JNIISLFold.isl_fold_min).coalesce();
//		
//		/*
//		 * The piecewise polynomial is only defined when the cardinal is greater
//		 * than 0. Therefore, for all the points in the difference between the
//		 * definition domain of the relation and the definition domain of the
//		 * polynomial, the cardinal equals 0. Since the cardinal of a relation
//		 * is always positive, if this difference is not empty, then the minimum
//		 * is 0.
//		 */
//		JNIISLSet origDomain = finalMap.copy().domain();
//		JNIISLSet cardDomain = card.copy().domain();
//		JNIISLSet subtract = origDomain.subtract(cardDomain);
//		if (!subtract.isEmpty()) {
//			min = 0;
//		} else {
//			if (debug) System.out.println("bound = "+bound);
//			List<JNIISLQPolynomialFoldPiece> liftedPieces = bound.getLiftedPieces();
//			if (liftedPieces.size() == 0) 
//				min = 0;
//			else {
//				main: for (JNIISLQPolynomialFoldPiece p : liftedPieces) {
//					JNIISLQPolynomialFold fold = p.getFold();
//					for (JNIISLQPolynomial qp : fold.getQPolynomials()) {
//						if (qp.isConstant()) {
//							if (qp.getConstantVal().getDenominator() != 1) {
//								throw new RuntimeException("There cannot be a non integer number of points in a discrete set of points.");
//							}
//							long n = qp.getConstantVal().getNumerator();
//							if (n < 0) {
//								min = -1;
//								break main;
//							} else {
//								min = Math.min(min,n);
//							}
//						} else {
//							//unable to bound the cardinal by a constant
//							min = -1;
//							break main;
//						}
//					}
//				}
//			}
//		}
//		if (debug) System.out.println("min   = "+min+"\n********************");
//		return min;
	}

	public static long absoluteMaximumCard(ISLMap map) {
		return absoluteCard(map, true);
//		long max = Long.MIN_VALUE;
//		int nbParam = map.getNbParams();
//		// move parameters into "in" dimensions so that the cardinal becomes
//		// non-parametrized, but with same expression(s).
//		JNIISLMap finalMap = map.copy().moveDims(JNIISLDimType.isl_dim_in, 0, JNIISLDimType.isl_dim_param, 0, nbParam);
//		// non parametrized cardinal
//		JNIISLPWQPolynomial card = card(finalMap);
//		if (debug) {
//			System.out.println("map   = "+map);
//			System.out.println("card  = "+card);
//			// parametrized cardinal to compare expressions.
//			JNIISLPWQPolynomial cardOrig = card(map.copy());
//			System.out.println("Pcard = "+cardOrig);
//		}
//		JNIISLPWQPolynomialFold bound = card.copy().bound(JNIISLFold.isl_fold_max).coalesce();
//	
//		if (debug) System.out.println("bound = "+bound);
//		List<JNIISLQPolynomialFoldPiece> liftedPieces = bound.getLiftedPieces();
//		if (liftedPieces.size() == 0) 
//			max = 0;
//		else {
//			main: for (JNIISLQPolynomialFoldPiece p : liftedPieces) {
//				JNIISLQPolynomialFold fold = p.getFold();
//				for (JNIISLQPolynomial qp : fold.getQPolynomials()) {
//					if (qp.isConstant()) {
//						if (qp.getConstantVal().getDenominator() != 1) {
//							throw new RuntimeException("There cannot be a non integer number of points in a discrete set of points.");
//						}
//						long n = qp.getConstantVal().getNumerator();
//						if (n < 0) {
//							max = -1;
//							break main;
//						} else {
//							max = Math.max(max,n);
//						}
//					} else {
//						//unable to bound the cardinal by a constant
//						max = -1;
//						break main;
//					}
//				}
//			}
//		}
//		
//		if (debug) System.out.println("max   = "+max+"\n********************");
//		return max;
	}

	public static long absoluteMaximumCard(ISLSet set) {
		ISLMap m = ISLMap.buildFromRange(set.copy());
		return absoluteMaximumCard(m);
	}

	public static long absoluteMinimumCard(ISLSet set) {
		ISLMap m = ISLMap.buildFromRange(set.copy());
		return absoluteMinimumCard(m);
	}
	
	
//	public static int absoluteMaximumCard2(JNIISLSet set) {
////		System.err.println("Warning : BarvinokFunctions.absoluteMinimumCard(JNIISLMap) has not been extensively tested.");
//		int max = Integer.MIN_VALUE;
//		// non parametrized cardinal
//		JNIISLMap m = JNIISLMap.fromRange(set.copy());
//		int nparam = (int)m.copy().getNbParam();
//		m = m.moveDims(JNIISLDimType.isl_dim_in, 0, JNIISLDimType.isl_dim_param, 0, nparam);
//		
//		JNIISLPWQPolynomial card = BarvinokFunctions.card(m.copy());
//		JNIISLPWQPolynomialFold bound = card.copy().bound(JNIISLFold.isl_fold_max).coalesce();
//		
//		List<JNIISLQPolynomialFoldPiece> liftedPieces = bound.getLiftedPieces();
//		if (liftedPieces.size() == 0) 
//			max = 0;
//		else {
//			main: for (JNIISLQPolynomialFoldPiece p : liftedPieces) {
//				JNIISLQPolynomialFold fold = p.getFold();
//				for (JNIISLQPolynomial qp : fold.getQPolynomials()) {
//					throw new UnsupportedOperationException("This part does not work with new jniisl binding");
////					JNIISLInt n = new JNIISLInt();
////					JNIISLInt d = new JNIISLInt();
////					boolean isCst = qp._isConstant(n,d) != 0;
////					if (isCst) {
////						if (n._toInt() < 0) {
////							max = -1;
////							break main;
////						} else {
////							max = Math.max(max,n._toInt());
////						}
////					} else {
////						//unable to bound the cardinal by a constant
////						max = -1;
////						break main;
////					}
//				}
//			}
//		}
//		
//		return max;
//	}	
}

