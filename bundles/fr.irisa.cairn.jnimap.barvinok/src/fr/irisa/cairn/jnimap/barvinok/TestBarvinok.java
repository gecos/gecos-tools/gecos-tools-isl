package fr.irisa.cairn.jnimap.barvinok;

import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLPWQPolynomial;
import fr.irisa.cairn.jnimap.isl.ISLSet;

public class TestBarvinok {

	
	public static void main(String[] args) {
		
		ISLSet set = ISLFactory.islSet("[N]->{ [i,j] : 0<=i<=j<N}");
		ISLPWQPolynomial setCard = BarvinokFunctions.card(set);
		
		System.out.println(setCard);
		

		
		ISLMap map = ISLFactory.islMap("[N]->{ [i,j]->[x,y] : 0<=i<=j<N and x=i+j and y=i-j}");
		ISLPWQPolynomial mapCard = BarvinokFunctions.card(map);
		
		System.out.println(mapCard);
		
	}
}
