JNI Bindings for ISL and related libraries.

The libraries and it dependencies are included in the project as binaries.
These binaries are extracted from the jar and linked at run-time (with linking
forced by rpath) to give semi-portability. Hence, the libraries for each target
platform must be compiled before-hand. We currently maintain three targets:
  * Linux 64 bits
  * Macos x86_64 bits
  * Macos arm64 bits

Each jni project includes a directory named ``native`` that containts the C code
for the bindings. Running the Makefile at ``native/build`` should re-compile
the binaries if all libraries are already in place.

The libraries are assumed to be located at ``~/complibs/usr/local/lib`` although
the location can be customized by modifying ``libraries.mk``. If you do not have
the necessary libraries, the scripts in the corresponding directory under 
``native/build``, e.g., ``native/build/isl``, will fetch the library source from
their websites and compile with the appropriate options.


When compiling all libraries and bindings from scratch the order is important 
due to dependences across libraries. The order should be:
1. Compile GMP library (isl/native/build/gmp)
2. Compile ISL library (isl/native/build/isl)
3. Compile ISL bindings (isl/native/build)
4. Compile NTL library (barvinok/native/build/ntl)
5. Compile PolyLib library (polylib/native/build/polylib)
6. Compile Barvinok library (barvinok/native/build/barvinok)
7. Compile PolyLib bindings (polylib/native/build)
8. Compile Barvinok bindings (barvinok/native/build)

Stop after step 3 if all you need is ISL. 
Use `scripts/compile_native_bindings.sh` to build everything.

Note that Polylib bindings must be before Barvinok bindings. It is currently
setup to use Polylib compiled by Barvinok installation, and hence Barvinok
library is compiled first. However, since the dependence is from Barvinok to
Polylib, the bindings for Polylib must be compiled first to properly reuse
dynamic libraries.
