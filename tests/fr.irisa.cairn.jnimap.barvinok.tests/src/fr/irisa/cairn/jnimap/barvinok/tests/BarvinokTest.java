package fr.irisa.cairn.jnimap.barvinok.tests;

import org.junit.Test;

import fr.irisa.cairn.jnimap.barvinok.BarvinokFunctions;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLPWQPolynomial;
import fr.irisa.cairn.jnimap.isl.ISLSet;

public class BarvinokTest {
	
	@Test
	public void setTest1() {
		testSetCard("[N]->{ [i,j] : 0<=i<=j<N}");
	}

	@Test
	public void mapTest1() {
		testMapCard("[N]->{ [i,j]->[x,y] : 0<=i<=j<N and x=i+j and y=i-j}");
	}
	
	private void testSetCard(String setStr) {
		ISLSet set = ISLFactory.islSet(setStr);
		ISLPWQPolynomial setCard = BarvinokFunctions.card(set);
		
		System.out.println(setCard);
	}

	private void testMapCard(String mapStr) {
		ISLMap map = ISLFactory.islMap(mapStr);
		ISLPWQPolynomial mapCard = BarvinokFunctions.card(map);
		
		System.out.println(mapCard);
	}
}
