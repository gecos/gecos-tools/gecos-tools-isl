package fr.irisa.cairn.jnimap.isl.tests.sandbox;

import com.google.common.base.Objects;
import fr.irisa.cairn.jnimap.isl.ISLBasicMap;
import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLConstraint;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLMultiAff;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.codegen.FSMCodeGeneration;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class FSMVHDLPrettyPrinter {
  private HashSet<String> stateList;
  
  private HashSet<String> commandList;
  
  private FSMCodeGeneration.JNIFSM fsm;
  
  private String name;
  
  public FSMVHDLPrettyPrinter(final String name, final FSMCodeGeneration.JNIFSM fsm) {
    this.fsm = fsm;
    this.name = name;
    final Function1<ISLMap, String> _function = (ISLMap e) -> {
      return e.getInputTupleName();
    };
    List<String> _map = ListExtensions.<ISLMap, String>map(fsm.getCommands().getMaps(), _function);
    HashSet<String> _hashSet = new HashSet<String>(_map);
    this.stateList = _hashSet;
    final Function1<ISLMap, String> _function_1 = (ISLMap e) -> {
      return e.getOutputTupleName();
    };
    List<String> _map_1 = ListExtensions.<ISLMap, String>map(fsm.getCommands().getMaps(), _function_1);
    HashSet<String> _hashSet_1 = new HashSet<String>(_map_1);
    this.commandList = _hashSet_1;
  }
  
  public void generate() {
    try {
      final PrintStream ps = new PrintStream((this.name + ".vhd"));
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("library ieee;");
      _builder.newLine();
      _builder.append("library work;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("use ieee.std_logic_1164.all;");
      _builder.newLine();
      _builder.append("use ieee.std_logic_arith.all;");
      _builder.newLine();
      _builder.append("use ieee.std_logic_unsigned.all;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("entity ");
      _builder.append(this.name);
      _builder.append(" is port (");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("clk : int std_logic;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("rst : in std_logic;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("ce : in std_logic;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("------------------");
      _builder.newLine();
      {
        boolean _hasElements = false;
        for(final String c : this.commandList) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(";", "\t");
          }
          _builder.append("\t");
          _builder.append(c, "\t");
          _builder.append(" : out std_logic");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.append(");");
      _builder.newLine();
      _builder.append("end entity;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("architecture RTL of ");
      _builder.append(this.name);
      _builder.append(" is");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("type T_");
      String _firstUpper = StringExtensions.toFirstUpper(this.name);
      _builder.append(_firstUpper, "\t");
      _builder.append(" is (");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("idle,");
      _builder.newLine();
      {
        boolean _hasElements_1 = false;
        for(final String state : this.stateList) {
          if (!_hasElements_1) {
            _hasElements_1 = true;
          } else {
            _builder.appendImmediate(",", "\t\t");
          }
          _builder.append("\t\t");
          _builder.append(state, "\t\t");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t");
      _builder.append(");");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("signal ns,cs : T_");
      String _firstUpper_1 = StringExtensions.toFirstUpper(this.name);
      _builder.append(_firstUpper_1, "\t");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("constant ONE : std_logic:=\'1\';");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("constant ZERO : std_logic:=\'0\';");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("begin");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("SYNC: process(clk,rst) ");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("begin");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("if (rst=\'1\') then");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("cs <= INIT;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("elsif rising_edge(clk) then");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("case cs is");
      _builder.newLine();
      {
        List<ISLSet> _sets = this.fsm.getStates().getSets();
        for(final ISLSet state_1 : _sets) {
          _builder.append("\t\t\t");
          _builder.append("when ");
          String _tupleName = state_1.getTupleName();
          _builder.append(_tupleName, "\t\t\t");
          _builder.append(" =>");
          _builder.newLineIfNotEmpty();
          {
            final Function1<ISLMap, Boolean> _function = (ISLMap t) -> {
              String _inputTupleName = t.getInputTupleName();
              String _tupleName_1 = state_1.getTupleName();
              return Boolean.valueOf(Objects.equal(_inputTupleName, _tupleName_1));
            };
            Iterable<ISLMap> _filter = IterableExtensions.<ISLMap>filter(this.fsm.getTransitions().getMaps(), _function);
            for(final ISLMap m : _filter) {
              _builder.append("\t\t\t");
              _builder.append("\t");
              String _generateTransition = this.generateTransition(m, state_1);
              _builder.append(_generateTransition, "\t\t\t\t");
              _builder.newLineIfNotEmpty();
            }
          }
          {
            final Function1<ISLMap, Boolean> _function_1 = (ISLMap t) -> {
              String _inputTupleName = t.getInputTupleName();
              String _tupleName_1 = state_1.getTupleName();
              return Boolean.valueOf(Objects.equal(_inputTupleName, _tupleName_1));
            };
            Iterable<ISLMap> _filter_1 = IterableExtensions.<ISLMap>filter(this.fsm.getCommands().getMaps(), _function_1);
            for(final ISLMap m_1 : _filter_1) {
              _builder.append("\t\t\t");
              _builder.append("\t");
              String _outputTupleName = m_1.getOutputTupleName();
              _builder.append(_outputTupleName, "\t\t\t\t");
              _builder.append(" <= \'1\'");
              _builder.newLineIfNotEmpty();
            }
          }
        }
      }
      _builder.append("\t\t\t");
      _builder.append("when others =>");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("null;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("end case;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("end if; ");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("end process;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("end RTL;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.newLine();
      ps.append(_builder);
      ps.close();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public String generateTransition(final ISLMap t, final ISLSet m) {
    int _nbBasicMaps = t.getNbBasicMaps();
    boolean _equals = (_nbBasicMaps == 1);
    if (_equals) {
      final ISLBasicMap bmp = t.getBasicMaps().get(0);
      final ISLBasicSet dom = bmp.copy().getDomain();
      final ISLBasicSet gisted_dom = dom.gist(m.copy().convexHull());
      int _size = gisted_dom.getConstraints().size();
      boolean _greaterThan = (_size > 0);
      if (_greaterThan) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("if (");
        {
          List<ISLConstraint> _constraints = gisted_dom.getConstraints();
          boolean _hasElements = false;
          for(final ISLConstraint c : _constraints) {
            if (!_hasElements) {
              _hasElements = true;
            } else {
              _builder.appendImmediate(" and ", "");
            }
            _builder.append("(");
            String _string = c.toString();
            _builder.append(_string);
            _builder.append(")");
          }
        }
        _builder.append(") then");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("-- here we should update indices");
        _builder.newLine();
        _builder.append(" \t");
        _builder.append("cs <= ");
        String _outputTupleName = t.getOutputTupleName();
        _builder.append(_outputTupleName, " \t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
        _builder.append("end if;");
        _builder.newLine();
        _builder.newLine();
        Map<ISLSet, ISLMultiAff> _closedFormRelation = bmp.getClosedFormRelation();
        _builder.append(_closedFormRelation);
        _builder.newLineIfNotEmpty();
        return _builder.toString();
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("-- Single transition");
        _builder_1.newLine();
        _builder_1.append("cs <= ");
        String _outputTupleName_1 = t.getOutputTupleName();
        _builder_1.append(_outputTupleName_1);
        _builder_1.append(";");
        _builder_1.newLineIfNotEmpty();
        return _builder_1.toString();
      }
    }
    int _nbBasicMaps_1 = t.getNbBasicMaps();
    boolean _equals_1 = (_nbBasicMaps_1 == 0);
    if (_equals_1) {
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("-- Warning no basicmap");
      _builder_2.newLine();
      _builder_2.append("cs <= ");
      String _outputTupleName_2 = t.getOutputTupleName();
      _builder_2.append(_outputTupleName_2);
      _builder_2.append(";");
      _builder_2.newLineIfNotEmpty();
      return _builder_2.toString();
    } else {
      throw new UnsupportedOperationException("Unsuported");
    }
  }
}
