package fr.irisa.cairn.jnimap.isl.tests.codegen;

import com.google.common.base.Objects;
import fr.irisa.cairn.jnimap.isl.ISLASTExpression;
import fr.irisa.cairn.jnimap.isl.ISLASTIdentifier;
import fr.irisa.cairn.jnimap.isl.ISLASTLiteral;
import fr.irisa.cairn.jnimap.isl.ISLASTOpType;
import fr.irisa.cairn.jnimap.isl.ISLASTOperation;
import fr.irisa.cairn.jnimap.isl.ISLIdentifier;
import java.util.Arrays;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IntegerRange;

@SuppressWarnings("all")
public class JNIISLExpressionChecker {
  public JNIISLExpressionChecker() {
  }
  
  protected void _buildExpr(final ISLASTLiteral obj) {
  }
  
  protected void _buildExpr(final ISLASTIdentifier obj) {
    ISLIdentifier _iD = obj.getID();
    boolean _equals = Objects.equal(_iD, null);
    if (_equals) {
      throw new RuntimeException(("Cannot build void object from " + obj));
    }
  }
  
  protected void _buildExpr(final ISLASTOperation obj) {
    final ISLASTExpression arg0 = obj.getArgument(0);
    final ISLASTExpression arg1 = obj.getArgument(1);
    this.buildExpr(arg0);
    this.buildExpr(arg1);
    final ISLASTOpType opType = obj.getOpType();
    if ((opType == null)) {
      InputOutput.<ISLASTOperation>println(obj);
    }
    Object _switchResult = null;
    int _value = opType.getValue();
    switch (_value) {
      case ISLASTOpType.ISL_AST_OP_MINUS:
        _switchResult = null;
        break;
      case ISLASTOpType.ISL_AST_OP_ADD:
        _switchResult = null;
        break;
      case ISLASTOpType.ISL_AST_OP_SUB:
        _switchResult = null;
        break;
      case ISLASTOpType.ISL_AST_OP_MUL:
        _switchResult = null;
        break;
      case ISLASTOpType.ISL_AST_OP_MIN:
        _switchResult = null;
        break;
      case ISLASTOpType.ISL_AST_OP_MAX:
        _switchResult = null;
        break;
      case ISLASTOpType.ISL_AST_OP_DIV:
        _switchResult = null;
        break;
      case ISLASTOpType.ISL_AST_OP_FDIV_Q:
        _switchResult = null;
        break;
      case ISLASTOpType.ISL_AST_OP_PDIV_Q:
        _switchResult = null;
        break;
      case ISLASTOpType.ISL_AST_OP_PDIV_R:
        _switchResult = null;
        break;
      default:
        String _name = obj.getOpType().getName();
        String _plus = (_name + " not supported");
        throw new RuntimeException(_plus);
    }
    final Object res = _switchResult;
  }
  
  protected void _buildConstraint(final ISLASTExpression obj) {
    throw new RuntimeException(("Cannot build void object from " + obj));
  }
  
  protected void _buildConstraint(final ISLASTOperation obj) {
    this.buildExpr(obj.getArgument(0));
    this.buildExpr(obj.getArgument(1));
    ISLASTOpType _opType = obj.getOpType();
    boolean _notEquals = (!Objects.equal(_opType, null));
    if (_notEquals) {
      int _value = obj.getOpType().getValue();
      switch (_value) {
      }
    } else {
      throw new RuntimeException(("Cannot build void object from " + obj));
    }
  }
  
  public void buildUnionOfConstraintSystem(final ISLASTOperation obj) {
    int _value = obj.getOpType().getValue();
    switch (_value) {
      case ISLASTOpType.ISL_AST_OP_OR:
        int _nbArgs = obj.getNbArgs();
        int _minus = (_nbArgs - 1);
        IntegerRange _upTo = new IntegerRange(0, _minus);
        for (final Integer i : _upTo) {
          this.buildConstraint(obj.getArgument((i).intValue()));
        }
        break;
      case ISLASTOpType.ISL_AST_OP_AND:
        this.buildConstraintSystem(obj);
        break;
      default:
        this.buildConstraint(obj);
        break;
    }
  }
  
  public void buildConstraintSystem(final ISLASTOperation obj) {
    final ISLASTOpType opType = obj.getOpType();
    int _value = opType.getValue();
    switch (_value) {
      case ISLASTOpType.ISL_AST_OP_AND:
        int _nbArgs = obj.getNbArgs();
        int _minus = (_nbArgs - 1);
        IntegerRange _upTo = new IntegerRange(0, _minus);
        for (final Integer i : _upTo) {
          this.buildConstraint(obj.getArgument((i).intValue()));
        }
        break;
      case ISLASTOpType.ISL_AST_OP_MIN:
        int _nbArgs_1 = obj.getNbArgs();
        int _minus_1 = (_nbArgs_1 - 1);
        IntegerRange _upTo_1 = new IntegerRange(0, _minus_1);
        for (final Integer i_1 : _upTo_1) {
          this.buildConstraint(obj.getArgument((i_1).intValue()));
        }
        break;
      case ISLASTOpType.ISL_AST_OP_EQ:
        this.buildConstraint(obj);
        break;
      case ISLASTOpType.ISL_AST_OP_LE:
        this.buildConstraint(obj);
        break;
      case ISLASTOpType.ISL_AST_OP_LT:
        this.buildConstraint(obj);
        break;
      case ISLASTOpType.ISL_AST_OP_GE:
        this.buildConstraint(obj);
        break;
      case ISLASTOpType.ISL_AST_OP_GT:
        this.buildConstraint(obj);
        break;
      default:
        throw new RuntimeException(("Cannot build void object from " + obj));
    }
  }
  
  public void buildStatement(final ISLASTOperation obj) {
    final ISLASTOpType optype = obj.getOpType();
    int _value = optype.getValue();
    switch (_value) {
      case ISLASTOpType.ISL_AST_OP_CALL:
        int _nbArgs = obj.getNbArgs();
        IntegerRange _upTo = new IntegerRange(1, _nbArgs);
        for (final Integer i : _upTo) {
          throw new RuntimeException(("Cannot build voidMacro object from " + obj));
        }
        break;
      default:
        throw new RuntimeException(("Cannot build voidMacro object from " + obj));
    }
  }
  
  public void buildExpr(final ISLASTExpression obj) {
    if (obj instanceof ISLASTIdentifier) {
      _buildExpr((ISLASTIdentifier)obj);
      return;
    } else if (obj instanceof ISLASTLiteral) {
      _buildExpr((ISLASTLiteral)obj);
      return;
    } else if (obj instanceof ISLASTOperation) {
      _buildExpr((ISLASTOperation)obj);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(obj).toString());
    }
  }
  
  public void buildConstraint(final ISLASTExpression obj) {
    if (obj instanceof ISLASTOperation) {
      _buildConstraint((ISLASTOperation)obj);
      return;
    } else if (obj != null) {
      _buildConstraint(obj);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(obj).toString());
    }
  }
}
