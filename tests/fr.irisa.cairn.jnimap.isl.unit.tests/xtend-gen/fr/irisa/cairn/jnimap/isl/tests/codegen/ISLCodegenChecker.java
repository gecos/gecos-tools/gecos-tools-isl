package fr.irisa.cairn.jnimap.isl.tests.codegen;

import fr.irisa.cairn.jnimap.isl.ISLASTBuild;
import fr.irisa.cairn.jnimap.isl.ISLASTNode;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.tests.codegen.JNIISLASTChecker;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.junit.Test;

@SuppressWarnings("all")
public class ISLCodegenChecker {
  public void codegen(final ISLSet context, final ISLUnionMap umap) {
    ISLASTBuild build = ISLASTBuild.buildFromContext(context);
    ISLASTNode root = ISLASTNode.buildFromSchedule(build, umap);
    InputOutput.<ISLASTNode>println(root);
    JNIISLASTChecker.adapt(root);
  }
  
  @Test
  public void test1() {
    ISLSet context = ISLFactory.islSet("[M,N]-> { : M>=1 & N >= 1 }");
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("[N,M] -> { ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("A[i,j] -> [i,2j] : 0 <= i <= N and 0 <= j <= M ;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("B[i,j] -> [i,j] : 0 <= i <= N and 0 <= j <= M ");
    _builder.newLine();
    _builder.append("}");
    ISLUnionMap umap = ISLFactory.islUnionMap(_builder.toString());
    this.codegen(context, umap);
  }
}
