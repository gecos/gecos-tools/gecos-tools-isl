/**
 * Generated using {@link fr.irisa.cairn.gecos.model.scop.scheduling.tests.tools.GenISLFromC}
 */
package fr.irisa.cairn.jnimap.isl.jni.tests;

import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;

public abstract class AbstractTestFromISL extends junit.framework.TestCase { 
	
	public abstract void computeTest(String path,
			ISLUnionSet domains, ISLUnionMap idSchedules, ISLUnionMap writes, ISLUnionMap reads,
			ISLUnionMap valueBasedPrdg, ISLUnionMap valueBasedPlutoSchedule, ISLUnionMap valueBasedFeautrierSchedule,
			ISLUnionMap memoryBasedPrdg, ISLUnionMap memoryBasedPlutoSchedule, ISLUnionMap memoryBasedFeautrierSchedule);
	
	protected void translateAndComputeTest(String path,String domainsStr, String idSchedulesStr, String writesStr, String readsStr,
			String valueBasedPrdgStr, String valueBasedPlutoScheduleStr, String valueBasedFeautrierScheduleStr,
			String memoryBasedPrdgStr, String memoryBasedPlutoScheduleStr, String memoryBasedFeautrierScheduleStr) {
		ISLUnionSet domains = ISLFactory.islUnionSet(domainsStr);
		ISLUnionMap idSchedules = ISLFactory.islUnionMap(idSchedulesStr);
		ISLUnionMap writes = ISLFactory.islUnionMap(writesStr);
		ISLUnionMap reads = ISLFactory.islUnionMap(readsStr);
		ISLUnionMap valueBasedPrdg = ISLFactory.islUnionMap(valueBasedPrdgStr);
		ISLUnionMap valueBasedPlutoSchedule = ISLFactory.islUnionMap(valueBasedPlutoScheduleStr);
		ISLUnionMap valueBasedFeautrierSchedule = ISLFactory.islUnionMap(valueBasedFeautrierScheduleStr);
		ISLUnionMap memoryBasedPrdg = ISLFactory.islUnionMap(memoryBasedPrdgStr);
		ISLUnionMap memoryBasedPlutoSchedule = ISLFactory.islUnionMap(memoryBasedPlutoScheduleStr);
		ISLUnionMap memoryBasedFeautrierSchedule = ISLFactory.islUnionMap(memoryBasedFeautrierScheduleStr);
		computeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #0
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : QR_givensreverse_QR_givensreverse/scop_0
	 *   block : 
	 *     for : 0 <= k <= N-2 (stride = 1)
	 *       for : 0 <= i <= N-k-2 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [angle=call vectorize(M[N-i-1][k],M[N-i-2][k])]
	 *           ([angle]) = f([M[N-i-1][k], M[N-i-2][k]])
	 *           (Domain = [N] -> {S0[k,i] : (i >= 0) and (-k-i+N-2 >= 0) and (k >= 0) and (-k+N-2 >= 0)})
	 *           for : k <= j <= N-1 (stride = 1)
	 *             S1 (depth = 3) [call rotate(M[N-i-2][j],M[N-i-1][j],((*SF64)&(M[N-i-2][j])),((*SF64)&(M[N-i-1][j])),angle)]
	 *             ([M[N-i-2][j], M[N-i-1][j]]) = f([M[N-i-2][j], M[N-i-1][j], angle])
	 *             (Domain = [N] -> {S1[k,i,j] : (-k+j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-k-i+N-2 >= 0) and (k >= 0) and (-k+N-2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_qr_trigo_simple_GScopRoot_QR_givensreverse_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_qr_trigo_simple_GScopRoot_QR_givensreverse_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S1[k, i, j] : j >= k and j <= -1 + N and i >= 0 and i <= -2 + N - k and k >= 0 and k <= -2 + N; S0[k, i] : i >= 0 and i <= -2 + N - k and k >= 0 and k <= -2 + N }";
		String idSchedules = "[N] -> { S0[k, i] -> [0, k, 0, i, 0, 0, 0]; S1[k, i, j] -> [0, k, 0, i, 1, j, 0] }";
		String writes = "[N] -> { S1[k, i, j] -> M[-1 + N - i, j]; S1[k, i, j] -> M[-2 + N - i, j]; S0[k, i] -> angle[] }";
		String reads = "[N] -> { S1[k, i, j] -> M[-1 + N - i, j]; S1[k, i, j] -> M[-2 + N - i, j]; S1[k, i, j] -> angle[]; S0[k, i] -> M[-1 + N - i, k]; S0[k, i] -> M[-2 + N - i, k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S1[k, i, j] -> S0[k, i] : j >= k and j <= -1 + N and i >= 0 and i <= -2 + N - k and k >= 0 and k <= -2 + N; S1[k, i, j] -> S1[-1 + k, 1 + i, j] : j >= k and j <= -1 + N and i >= 0 and i <= -2 + N - k and k >= 1; S1[k, i, j] -> S1[k, -1 + i, j] : j >= k and j <= -1 + N and i >= 1 and i <= -2 + N - k and k >= 0; S1[k, 0, j] -> S1[-1 + k, 0, j] : j >= k and j <= -1 + N and k <= -2 + N and k >= 1; S0[k, i] -> S1[-1 + k, 1 + i, k] : i >= 0 and i <= -2 + N - k and k >= 1; S0[k, i] -> S1[k, -1 + i, k] : i >= 1 and i <= -2 + N - k and k >= 0; S0[k, 0] -> S1[-1 + k, 0, k] : k <= -2 + N and k >= 1 }";
		String valueBasedPlutoSchedule = "[N] -> { S0[k, i] -> [k, k + i, k, 0]; S1[k, i, j] -> [k, k + i, j, 1] }";
		String valueBasedFeautrierSchedule = "[N] -> { S0[k, i] -> [2k + i, 0, k, 0]; S1[k, i, j] -> [2k + i, 1, k, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S0[k, i] -> S0[k, -1 + i] : i >= 1 and i <= -2 + N - k and k >= 0; S0[k, 0] -> S0[-1 + k, -1 + N - k] : k <= -2 + N and k >= 1; S1[k, i, j] -> S0[k, i] : k >= 0 and i >= 0 and i <= -2 + N - k and j >= k and j <= -1 + N; S1[k, i, j] -> S1[-1 + k, 1 + i, j] : k >= 1 and i >= 0 and i <= -2 + N - k and j >= k and j <= -1 + N; S1[k, i, j] -> S1[k, -1 + i, j] : k >= 0 and i >= 1 and i <= -2 + N - k and j >= k and j <= -1 + N; S1[k, 0, j] -> S1[-1 + k, 0, j] : k >= 1 and k <= -2 + N and j >= k and j <= -1 + N; S0[k, i] -> S1[k, -1 + i, j] : k >= 0 and i >= 1 and i <= -2 + N - k and j >= k and j <= -1 + N; S0[k, i] -> S1[-1 + k, 1 + i, k] : k >= 1 and i >= 0 and i <= -2 + N - k; S0[k, 0] -> S1[-1 + k, -1 + N - k, j] : k >= 1 and k <= -2 + N and j >= -1 + k and j <= -1 + N; S0[k, 0] -> S1[-1 + k, 0, k] : k >= 1 and k <= -2 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[k, i] -> [k, i, 0, 0]; S1[k, i, j] -> [k, i, j, 1] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S0[k, i] -> [k, i, 0, 0]; S1[k, i, j] -> [k, i, 1, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #1
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : evaluate_evaluate/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [y[i]=b[i]]
	 *         ([y[i]]) = f([b[i]])
	 *         (Domain = [N] -> {S0[i] : (i >= 0) and (-i+N-1 >= 0)})
	 *         for : 0 <= j <= i-1 (stride = 1)
	 *           S1 (depth = 2) [y[i]=sub(y[i],mul(L[i][j],y[j]))]
	 *           ([y[i]]) = f([y[i], L[i][j], y[j]])
	 *           (Domain = [N] -> {S1[i,j] : (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 *         S2 (depth = 1) [y[i]=div(y[i],L[i][i])]
	 *         ([y[i]]) = f([y[i], L[i][i]])
	 *         (Domain = [N] -> {S2[i] : (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_forwardSubstitution_GScopRoot_evaluate_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_forwardSubstitution_GScopRoot_evaluate_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[i] : i >= 0 and i <= -1 + N; S1[i, j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N; S2[i] : i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S2[i] -> [0, i, 2, 0, 0]; S1[i, j] -> [0, i, 1, j, 0]; S0[i] -> [0, i, 0, 0, 0] }";
		String writes = "[N] -> { S1[i, j] -> y[i]; S2[i] -> y[i]; S0[i] -> y[i] }";
		String reads = "[N] -> { S0[i] -> b[i]; S1[i, j] -> L[i, j]; S1[i, j] -> y[i]; S1[i, j] -> y[j]; S2[i] -> L[i, i]; S2[i] -> y[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S1[i, j] -> S2[j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N; S1[i, j] -> S1[i, -1 + j] : i <= -1 + N and j <= -1 + i and j >= 1 and i >= 0; S2[0] -> S0[0] : N >= 1; S2[i] -> S1[i, -1 + i] : i >= 1 and i <= -1 + N; S1[i, 0] -> S0[i] : i <= -1 + N and i >= 1 }";
		String valueBasedPlutoSchedule = "[N] -> { S0[i] -> [0, i, 0]; S2[i] -> [i, 2i, 1]; S1[i, j] -> [i, i + j, 2] }";
		String valueBasedFeautrierSchedule = "[N] -> { S1[i, j] -> [1 + i + j, j]; S2[i] -> [1 + 2i, 0]; S0[i] -> [0, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S1[i, j] -> S2[j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N; S1[i, j] -> S1[i, -1 + j] : i <= -1 + N and j >= 1 and j <= -1 + i; S2[0] -> S0[0] : N >= 1; S2[i] -> S1[i, -1 + i] : i >= 1 and i <= -1 + N; S1[i, 0] -> S0[i] : i >= 1 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[i] -> [0, i, 0]; S2[i] -> [i, 2i, 1]; S1[i, j] -> [i, i + j, 2] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S1[i, j] -> [1 + i + j, j]; S2[i] -> [1 + 2i, 0]; S0[i] -> [0, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #2
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : private_gaussian_filter_3_private_gaussian_filter_3/scop_0
	 *   block : 
	 *     for : 2 <= i <= M-1 (stride = 1)
	 *       S0 (depth = 1) [out[0][i-1]=sub(sub(mul(IntExpr(3),tmp[0][i-1]),tmp[0][i-2]),tmp[0][i])]
	 *       ([out[0][i-1]]) = f([tmp[0][i-1], tmp[0][i-2], tmp[0][i]])
	 *       (Domain = [M,N] -> {S0[i] : (i-2 >= 0) and (-i+M-1 >= 0)})
	 *     for : 1 <= i <= N-2 (stride = 1)
	 *       block : 
	 *         for : 0 <= j <= 1 (stride = 1)
	 *           S1 (depth = 2) [tmp[i][j]=sub(sub(mul(IntExpr(3),in[i][j]),in[i-1][j]),in[i+1][j])]
	 *           ([tmp[i][j]]) = f([in[i][j], in[i-1][j], in[i+1][j]])
	 *           (Domain = [M,N] -> {S1[i,j] : (j >= 0) and (-j+1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *         for : 2 <= j <= M-1 (stride = 4)
	 *           block : 
	 *             S2 (depth = 2) [tmp[i][j]=sub(sub(mul(IntExpr(3),in[i][j]),in[i-1][j]),in[i+1][j])]
	 *             ([tmp[i][j]]) = f([in[i][j], in[i-1][j], in[i+1][j]])
	 *             (Domain = [M,N] -> {S2[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *             S3 (depth = 2) [out[i][j-1]=sub(sub(mul(IntExpr(3),tmp[i][j-1]),tmp[i][j-2]),tmp[i][j])]
	 *             ([out[i][j-1]]) = f([tmp[i][j-1], tmp[i][j-2], tmp[i][j]])
	 *             (Domain = [M,N] -> {S3[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *             S4 (depth = 2) [tmp[i][j+1]=sub(sub(mul(IntExpr(3),in[i][j+1]),in[i-1][j+1]),in[i+1][j+1])]
	 *             ([tmp[i][j+1]]) = f([in[i][j+1], in[i-1][j+1], in[i+1][j+1]])
	 *             (Domain = [M,N] -> {S4[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *             S5 (depth = 2) [out[i][j]=sub(sub(mul(IntExpr(3),tmp[i][j]),tmp[i][j-1]),tmp[i][j+1])]
	 *             ([out[i][j]]) = f([tmp[i][j], tmp[i][j-1], tmp[i][j+1]])
	 *             (Domain = [M,N] -> {S5[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *             S6 (depth = 2) [tmp[i][j+2]=sub(sub(mul(IntExpr(3),in[i][j+2]),in[i-1][j+2]),in[i+1][j+2])]
	 *             ([tmp[i][j+2]]) = f([in[i][j+2], in[i-1][j+2], in[i+1][j+2]])
	 *             (Domain = [M,N] -> {S6[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *             S7 (depth = 2) [out[i][j+1]=sub(sub(mul(IntExpr(3),tmp[i][j+1]),tmp[i][j]),tmp[i][j+2])]
	 *             ([out[i][j+1]]) = f([tmp[i][j+1], tmp[i][j], tmp[i][j+2]])
	 *             (Domain = [M,N] -> {S7[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *             S8 (depth = 2) [tmp[i][j+3]=sub(sub(mul(IntExpr(3),in[i][j+3]),in[i-1][j+3]),in[i+1][j+3])]
	 *             ([tmp[i][j+3]]) = f([in[i][j+3], in[i-1][j+3], in[i+1][j+3]])
	 *             (Domain = [M,N] -> {S8[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *             S9 (depth = 2) [out[i][j+2]=sub(sub(mul(IntExpr(3),tmp[i][2]),tmp[i][j+1]),tmp[i][j+3])]
	 *             ([out[i][j+2]]) = f([tmp[i][2], tmp[i][j+1], tmp[i][j+3]])
	 *             (Domain = [M,N] -> {S9[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *     for : 2 <= i <= M-1 (stride = 1)
	 *       S10 (depth = 1) [out[N-1][i-1]=sub(sub(mul(IntExpr(3),tmp[N-1][i-1]),tmp[N-1][i-2]),tmp[N-1][i])]
	 *       ([out[N-1][i-1]]) = f([tmp[N-1][i-1], tmp[N-1][i-2], tmp[N-1][i]])
	 *       (Domain = [M,N] -> {S10[i] : (i-2 >= 0) and (-i+M-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_register_tilling_2D_gaussian_filter_GScopRoot_private_gaussian_filter_3_scop_0_ () {
		String path = "basics_register_tilling_2D_gaussian_filter_GScopRoot_private_gaussian_filter_3_scop_0_";
		// SCoP Extraction Data
		String domains = "[M, N] -> { S5[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S3[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S7[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S6[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S1[i, j] : j >= 0 and j <= 1 and i >= 1 and i <= -2 + N; S2[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S0[i] : i >= 2 and i <= -1 + M; S4[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S9[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S8[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S10[i] : i >= 2 and i <= -1 + M }";
		String idSchedules = "[M, N] -> { S1[i, j] -> [1, i, 0, j, 0]; S7[i, j] -> [1, i, 1, 4j, 5]; S0[i] -> [0, i, 0, 0, 0]; S2[i, j] -> [1, i, 1, 4j, 0]; S4[i, j] -> [1, i, 1, 4j, 2]; S9[i, j] -> [1, i, 1, 4j, 7]; S3[i, j] -> [1, i, 1, 4j, 1]; S5[i, j] -> [1, i, 1, 4j, 3]; S8[i, j] -> [1, i, 1, 4j, 6]; S10[i] -> [2, i, 0, 0, 0]; S6[i, j] -> [1, i, 1, 4j, 4] }";
		String writes = "[M, N] -> { S8[i, j] -> tmp[i, 3 + j]; S3[i, j] -> out[i, -1 + j]; S7[i, j] -> out[i, 1 + j]; S4[i, j] -> tmp[i, 1 + j]; S0[i] -> out[0, -1 + i]; S9[i, j] -> out[i, 2 + j]; S10[i] -> out[-1 + N, -1 + i]; S6[i, j] -> tmp[i, 2 + j]; S1[i, j] -> tmp[i, j]; S5[i, j] -> out[i, j]; S2[i, j] -> tmp[i, j] }";
		String reads = "[M, N] -> { S4[i, j] -> in[1 + i, 1 + j]; S4[i, j] -> in[i, 1 + j]; S4[i, j] -> in[-1 + i, 1 + j]; S0[i] -> tmp[0, i]; S0[i] -> tmp[0, -1 + i]; S0[i] -> tmp[0, -2 + i]; S2[i, j] -> in[1 + i, j]; S2[i, j] -> in[i, j]; S2[i, j] -> in[-1 + i, j]; S8[i, j] -> in[1 + i, 3 + j]; S8[i, j] -> in[i, 3 + j]; S8[i, j] -> in[-1 + i, 3 + j]; S9[i, j] -> tmp[i, 3 + j]; S9[i, j] -> tmp[i, 2]; S9[i, j] -> tmp[i, 1 + j]; S3[i, j] -> tmp[i, j]; S3[i, j] -> tmp[i, -1 + j]; S3[i, j] -> tmp[i, -2 + j]; S5[i, j] -> tmp[i, 1 + j]; S5[i, j] -> tmp[i, j]; S5[i, j] -> tmp[i, -1 + j]; S1[i, j] -> in[1 + i, j]; S1[i, j] -> in[i, j]; S1[i, j] -> in[-1 + i, j]; S6[i, j] -> in[1 + i, 2 + j]; S6[i, j] -> in[i, 2 + j]; S6[i, j] -> in[-1 + i, 2 + j]; S10[i] -> tmp[-1 + N, i]; S10[i] -> tmp[-1 + N, -1 + i]; S10[i] -> tmp[-1 + N, -2 + i]; S7[i, j] -> tmp[i, 2 + j]; S7[i, j] -> tmp[i, 1 + j]; S7[i, j] -> tmp[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[M, N] -> { S5[i, 2] -> S1[i, 1] : i <= -2 + N and M >= 3 and i >= 1; S9[i, j] -> S8[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S7[i, j] -> S6[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S3[i, j] -> S1[i, j'] : j' >= -2 + j and i >= 1 and i <= -2 + N and j' <= 1 and j >= 2 and j <= -1 + M; S7[i, j] -> S2[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S3[i, j] -> S2[i, j'] : i >= 1 and i <= -2 + N and j' >= -2 + j and j <= -1 + M and j' <= j and j' >= 2; S9[i, j] -> S2[i, 2] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S5[i, j] -> S4[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S7[i, j] -> S4[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S5[i, j] -> S2[i, j'] : j' >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N and j' >= -1 + j and j' <= j; S9[i, j] -> S4[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N }";
		String valueBasedPlutoSchedule = "[M, N] -> { S2[i, j] -> [i, j, 2]; S4[i, j] -> [i, j, 1]; S8[i, j] -> [i, j, 0]; S10[i] -> [i, 0, 0]; S0[i] -> [i, 0, 0]; S5[i, j] -> [i, j, 8]; S3[i, j] -> [i, j, 7]; S9[i, j] -> [i, j, 3]; S1[i, j] -> [i, j, 4]; S7[i, j] -> [i, j, 6]; S6[i, j] -> [i, j, 5] }";
		String valueBasedFeautrierSchedule = "[M, N] -> { S5[i, j] -> [i, j]; S10[i] -> [i, 0]; S3[i, j] -> [i, j]; S7[i, j] -> [i, j]; S1[i, j] -> [i, j]; S2[i, j] -> [i, j]; S4[i, j] -> [i, j]; S8[i, j] -> [i, j]; S0[i] -> [i, 0]; S6[i, j] -> [i, j]; S9[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[M, N] -> { S2[i, j] -> S7[i, -1 + j] : i >= 1 and i <= -2 + N and j >= 3 and j <= -1 + M; S7[i, j] -> S2[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S2[i, j] -> S9[i, -1 + j] : i >= 1 and i <= -2 + N and j >= 3 and j <= -1 + M; S2[i, j] -> S4[i, -1 + j] : j >= 3 and j <= -1 + M and i >= 1 and i <= -2 + N; S9[i, j] -> S8[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S3[i, j] -> S5[i, -1 + j] : j >= 3 and j <= -1 + M and i >= 1 and i <= -2 + N; S5[i, j] -> S7[i, -1 + j] : j >= 3 and j <= -1 + M and i >= 1 and i <= -2 + N; S5[i, 2] -> S1[i, 1] : i <= -2 + N and M >= 3 and i >= 1; S7[i, j] -> S9[i, -1 + j] : j >= 3 and j <= -1 + M and i >= 1 and i <= -2 + N; S5[i, j] -> S2[i, j'] : j' >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N and j' >= -1 + j and j' <= j; S10[i] -> S0[i] : N = 1 and i >= 2 and i <= -1 + M; S9[i, j] -> S2[i, 2] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S3[i, j] -> S1[i, j'] : j' >= -2 + j and i >= 1 and i <= -2 + N and j' <= 1 and j >= 2 and j <= -1 + M; S2[i, j] -> S5[i, -1 + j] : i >= 1 and i <= -2 + N and j >= 3 and j <= -1 + M; S3[i, j] -> S2[i, j'] : i >= 1 and i <= -2 + N and j' >= -2 + j and j <= -1 + M and j' <= j and j' >= 2; S6[i, j] -> S8[i, -1 + j] : j >= 3 and j <= -1 + M and i >= 1 and i <= -2 + N; S4[i, j] -> S6[i, -1 + j] : j >= 3 and j <= -1 + M and i >= 1 and i <= -2 + N; S6[i, j] -> S9[i, -1 + j] : i >= 1 and i <= -2 + N and j >= 3 and j <= -1 + M; S9[i, j] -> S4[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S7[i, j] -> S6[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S5[i, j] -> S4[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S4[i, j] -> S7[i, -1 + j] : i >= 1 and i <= -2 + N and j >= 3 and j <= -1 + M; S7[i, j] -> S4[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N }";
		String memoryBasedPlutoSchedule = "[M, N] -> { S2[i, j] -> [i, j, 2]; S4[i, j] -> [i, j, 1]; S8[i, j] -> [i, j, 0]; S10[i] -> [i, 1, 0]; S0[i] -> [i, 0, 0]; S5[i, j] -> [i, j, 8]; S3[i, j] -> [i, j, 7]; S9[i, j] -> [i, j, 3]; S1[i, j] -> [i, j, 4]; S7[i, j] -> [i, j, 6]; S6[i, j] -> [i, j, 5] }";
		String memoryBasedFeautrierSchedule = "[M, N] -> { S2[i, j] -> [j, 0, i]; S4[i, j] -> [j, 0, i]; S8[i, j] -> [i, j, 0]; S10[i] -> [i, 0, 0]; S0[i] -> [i, 0, 0]; S5[i, j] -> [j, 1, i]; S3[i, j] -> [i, j, 0]; S9[i, j] -> [j, 1, i]; S1[i, j] -> [i, j, 0]; S7[i, j] -> [j, 1, i]; S6[i, j] -> [j, 0, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #3
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : toto_toto/scop_0
	 *   for : 0 <= i <= M-1 (stride = 1)
	 *     S0 (depth = 1) [x[i]=div(x[i],IntExpr(i))]
	 *     ([x[i]]) = f([x[i]])
	 *     (Domain = [M] -> {S0[i] : (i >= 0) and (-i+M-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_sideScops_GScopRoot_toto_scop_0_ () {
		String path = "polymodel_src_corner_cases_sideScops_GScopRoot_toto_scop_0_";
		// SCoP Extraction Data
		String domains = "[M] -> { S0[i] : i >= 0 and i <= -1 + M }";
		String idSchedules = "[M] -> { S0[i] -> [0, i, 0] }";
		String writes = "[M] -> { S0[i] -> x[i] }";
		String reads = "[M] -> { S0[i] -> x[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[M] -> {  }";
		String valueBasedPlutoSchedule = "[M] -> { S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "[M] -> { S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[M] -> {  }";
		String memoryBasedPlutoSchedule = "[M] -> { S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "[M] -> { S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #4
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : toto_scop_new
	 *   block : 
	 *     S0 (depth = 0) [M=IntExpr(0)]
	 *     ([M]) = f([])
	 *     (Domain = [N] -> {S0[] : })
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       S1 (depth = 1) [M=add(M,x[i])]
	 *       ([M]) = f([M, x[i]])
	 *       (Domain = [N] -> {S1[i] : (i >= 0) and (-i+N-1 >= 0)})
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       S2 (depth = 1) [x[i]=add(x[i],IntExpr(i))]
	 *       ([x[i]]) = f([x[i]])
	 *       (Domain = [N] -> {S2[i] : (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_sideScops_GScopRoot_scop_new_ () {
		String path = "polymodel_src_corner_cases_sideScops_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[N] -> { S1[i] : i >= 0 and i <= -1 + N; S0[]; S2[i] : i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S0[] -> [0, 0, 0]; S1[i] -> [1, i, 0]; S2[i] -> [2, i, 0] }";
		String writes = "[N] -> { S0[] -> M[]; S1[i] -> M[]; S2[i] -> x[i] }";
		String reads = "[N] -> { S1[i] -> x[i]; S1[i] -> M[]; S2[i] -> x[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S1[i] -> S1[-1 + i] : i >= 1 and i <= -1 + N; S1[0] -> S0[] : N >= 1 }";
		String valueBasedPlutoSchedule = "[N] -> { S1[i] -> [i, 1]; S0[] -> [0, 0]; S2[i] -> [i, 0] }";
		String valueBasedFeautrierSchedule = "[N] -> { S1[i] -> [1 + i]; S0[] -> [0]; S2[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S1[0] -> S0[] : N >= 1; S1[i] -> S1[-1 + i] : i >= 1 and i <= -1 + N; S2[i] -> S1[i] : i >= 0 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S1[i] -> [i, 1]; S0[] -> [0, 0]; S2[i] -> [i, 2] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S1[i] -> [1 + i]; S0[] -> [0]; S2[i] -> [2 + i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #5
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_gramschmidt_kernel_gramschmidt/scop_0
	 *   for : 0 <= k <= nj-1 (stride = 1)
	 *     block : 
	 *       S0 (depth = 1) [nrm=IntExpr(0)]
	 *       ([nrm]) = f([])
	 *       (Domain = [nj,ni] -> {S0[k] : (k >= 0) and (-k+nj-1 >= 0)})
	 *       for : 0 <= i <= ni-1 (stride = 1)
	 *         S1 (depth = 2) [nrm=add(nrm,mul(A[i][k],A[i][k]))]
	 *         ([nrm]) = f([nrm, A[i][k], A[i][k]])
	 *         (Domain = [nj,ni] -> {S1[k,i] : (i >= 0) and (-i+ni-1 >= 0) and (k >= 0) and (-k+nj-1 >= 0)})
	 *       S2 (depth = 1) [R[k][k]=call sqrt(nrm)]
	 *       ([R[k][k]]) = f([nrm])
	 *       (Domain = [nj,ni] -> {S2[k] : (k >= 0) and (-k+nj-1 >= 0)})
	 *       for : 0 <= i <= ni-1 (stride = 1)
	 *         S3 (depth = 2) [Q[i][k]=div(A[i][k],R[k][k])]
	 *         ([Q[i][k]]) = f([A[i][k], R[k][k]])
	 *         (Domain = [nj,ni] -> {S3[k,i] : (i >= 0) and (-i+ni-1 >= 0) and (k >= 0) and (-k+nj-1 >= 0)})
	 *       for : k+1 <= j <= nj-1 (stride = 1)
	 *         block : 
	 *           S4 (depth = 2) [R[k][j]=IntExpr(0)]
	 *           ([R[k][j]]) = f([])
	 *           (Domain = [nj,ni] -> {S4[k,j] : (-k+j-1 >= 0) and (-j+nj-1 >= 0) and (k >= 0) and (-k+nj-1 >= 0)})
	 *           for : 0 <= i <= ni-1 (stride = 1)
	 *             S5 (depth = 3) [R[k][j]=add(R[k][j],mul(Q[i][k],A[i][j]))]
	 *             ([R[k][j]]) = f([R[k][j], Q[i][k], A[i][j]])
	 *             (Domain = [nj,ni] -> {S5[k,j,i] : (i >= 0) and (-i+ni-1 >= 0) and (-k+j-1 >= 0) and (-j+nj-1 >= 0) and (k >= 0) and (-k+nj-1 >= 0)})
	 *           for : 0 <= i <= ni-1 (stride = 1)
	 *             S6 (depth = 3) [A[i][j]=sub(A[i][j],mul(Q[i][k],R[k][j]))]
	 *             ([A[i][j]]) = f([A[i][j], Q[i][k], R[k][j]])
	 *             (Domain = [nj,ni] -> {S6[k,j,i] : (i >= 0) and (-i+ni-1 >= 0) and (-k+j-1 >= 0) and (-j+nj-1 >= 0) and (k >= 0) and (-k+nj-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_solvers_gramschmidt_gramschmidt_GScopRoot_kernel_gramschmidt_scop_0_ () {
		String path = "polybench_gecos_linear_algebra_solvers_gramschmidt_gramschmidt_GScopRoot_kernel_gramschmidt_scop_0_";
		// SCoP Extraction Data
		String domains = "[nj, ni] -> { S0[k] : k >= 0 and k <= -1 + nj; S2[k] : k >= 0 and k <= -1 + nj; S3[k, i] : i >= 0 and i <= -1 + ni and k >= 0 and k <= -1 + nj; S5[k, j, i] : i >= 0 and i <= -1 + ni and j >= 1 + k and j <= -1 + nj and k >= 0 and k <= -1 + nj; S6[k, j, i] : i >= 0 and i <= -1 + ni and j >= 1 + k and j <= -1 + nj and k >= 0 and k <= -1 + nj; S1[k, i] : i >= 0 and i <= -1 + ni and k >= 0 and k <= -1 + nj; S4[k, j] : j >= 1 + k and j <= -1 + nj and k >= 0 and k <= -1 + nj }";
		String idSchedules = "[nj, ni] -> { S3[k, i] -> [0, k, 3, i, 0, 0, 0]; S1[k, i] -> [0, k, 1, i, 0, 0, 0]; S6[k, j, i] -> [0, k, 4, j, 2, i, 0]; S0[k] -> [0, k, 0, 0, 0, 0, 0]; S5[k, j, i] -> [0, k, 4, j, 1, i, 0]; S2[k] -> [0, k, 2, 0, 0, 0, 0]; S4[k, j] -> [0, k, 4, j, 0, 0, 0] }";
		String writes = "[nj, ni] -> { S1[k, i] -> nrm[]; S4[k, j] -> R[k, j]; S5[k, j, i] -> R[k, j]; S6[k, j, i] -> A[i, j]; S2[k] -> R[k, k]; S0[k] -> nrm[]; S3[k, i] -> Q[i, k] }";
		String reads = "[nj, ni] -> { S5[k, j, i] -> Q[i, k]; S1[k, i] -> nrm[]; S6[k, j, i] -> R[k, j]; S2[k] -> nrm[]; S3[k, i] -> R[k, k]; S6[k, j, i] -> Q[i, k]; S5[k, j, i] -> R[k, j]; S6[k, j, i] -> A[i, j]; S3[k, i] -> A[i, k]; S5[k, j, i] -> A[i, j]; S1[k, i] -> A[i, k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[nj, ni] -> { S1[k, i] -> S1[k, -1 + i] : i >= 1 and i <= -1 + ni and k >= 0 and k <= -1 + nj; S2[k] -> S0[k] : k >= 0 and k <= -1 + nj and ni <= 0; S5[k, j, i] -> S6[-1 + k, j, i] : i >= 0 and i <= -1 + ni and j >= 1 + k and j <= -1 + nj and k >= 1 and k <= -1 + nj; S3[k, i] -> S2[k] : i >= 0 and i <= -1 + ni and k >= 0 and k <= -1 + nj; S3[k, i] -> S6[-1 + k, k, i] : i >= 0 and i <= -1 + ni and k >= 1 and k <= -1 + nj; S5[k, j, i] -> S3[k, i] : i >= 0 and i <= -1 + ni and j >= 1 + k and j <= -1 + nj and k >= 0 and k <= -1 + nj; S2[k] -> S1[k, -1 + ni] : k >= 0 and k <= -1 + nj and ni >= 1; S6[k, j, i] -> S5[k, j, -1 + ni] : i >= 0 and i <= -1 + ni and j >= 1 + k and j <= -1 + nj and k >= 0 and k <= -1 + nj; S1[k, 0] -> S0[k] : k <= -1 + nj and ni >= 1 and k >= 0; S6[k, j, i] -> S6[-1 + k, j, i] : i >= 0 and i <= -1 + ni and j >= 1 + k and j <= -1 + nj and k >= 1 and k <= -1 + nj; S5[k, j, 0] -> S4[k, j] : k >= 0 and ni >= 1 and j >= 1 + k and j <= -1 + nj and k <= -1 + nj; S6[k, j, i] -> S3[k, i] : i >= 0 and i <= -1 + ni and j >= 1 + k and j <= -1 + nj and k >= 0 and k <= -1 + nj; S1[k, i] -> S6[-1 + k, k, i] : i >= 0 and i <= -1 + ni and k >= 1 and k <= -1 + nj; S5[k, j, i] -> S5[k, j, -1 + i] : i >= 1 and i <= -1 + ni and j >= 1 + k and j <= -1 + nj and k >= 0 and k <= -1 + nj }";
		String valueBasedPlutoSchedule = "[nj, ni] -> { S6[k, j, i] -> [k, j, ni + i, 0, 0]; S0[k] -> [0, 0, 0, k, 0]; S3[k, i] -> [k, k, 1, i, 2]; S4[k, j] -> [0, k, j, 0, 0]; S2[k] -> [k, k, 1, 0, 1]; S5[k, j, i] -> [k, j, i, 1, 0]; S1[k, i] -> [k, k, 1, -ni + i, 0] }";
		String valueBasedFeautrierSchedule = "[nj, ni] -> { S2[k] -> [2 + 5k, 0, 0]; S0[k] -> [0, k, 0]; S3[k, i] -> [3 + 5k, i, 0]; S4[k, j] -> [0, k, j]; S1[k, i] -> [1 + 5k, i, 0]; S6[k, j, i] -> [2 + 2k + 3j, k + j, i]; S5[k, j, i] -> [1 + 2k + 3j, i, k + j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[nj, ni] -> { S1[k, i] -> S1[k, -1 + i] : k >= 0 and k <= -1 + nj and i >= 1 and i <= -1 + ni; S2[k] -> S0[k] : k >= 0 and k <= -1 + nj and ni <= 0; S5[k, j, i] -> S6[-1 + k, j, i] : i >= 0 and i <= -1 + ni and j >= 1 + k and j <= -1 + nj and k >= 1 and k <= -1 + nj; S3[k, i] -> S2[k] : i >= 0 and i <= -1 + ni and k >= 0 and k <= -1 + nj; S3[k, i] -> S6[-1 + k, k, i] : i >= 0 and i <= -1 + ni and k >= 1 and k <= -1 + nj; S5[k, j, i] -> S3[k, i] : i >= 0 and i <= -1 + ni and j >= 1 + k and j <= -1 + nj and k >= 0 and k <= -1 + nj; S0[k] -> S1[-1 + k, -1 + ni] : k >= 1 and k <= -1 + nj and ni >= 1; S2[k] -> S1[k, -1 + ni] : k >= 0 and k <= -1 + nj and ni >= 1; S6[k, j, i] -> S5[k, j, i] : k >= 0 and j >= 1 + k and j <= -1 + nj and i >= 0 and i <= -1 + ni; S6[k, j, i] -> S5[k, j, -1 + ni] : k >= 0 and j >= 1 + k and j <= -1 + nj and i >= 0 and i <= -1 + ni; S1[k, 0] -> S0[k] : ni >= 1 and k >= 0 and k <= -1 + nj; S6[k, j, i] -> S6[-1 + k, j, i] : k >= 1 and j >= 1 + k and j <= -1 + nj and i >= 0 and i <= -1 + ni; S5[k, j, 0] -> S4[k, j] : ni >= 1 and k >= 0 and j >= 1 + k and j <= -1 + nj; S6[k, j, i] -> S3[k, i] : i >= 0 and i <= -1 + ni and j >= 1 + k and j <= -1 + nj and k >= 0 and k <= -1 + nj; S1[k, i] -> S6[-1 + k, k, i] : i >= 0 and i <= -1 + ni and k >= 1 and k <= -1 + nj; S0[k] -> S2[-1 + k] : k >= 1 and k <= -1 + nj; S0[k] -> S0[-1 + k] : k >= 1 and k <= -1 + nj and ni <= 0; S5[k, j, i] -> S5[k, j, -1 + i] : k >= 0 and j >= 1 + k and j <= -1 + nj and i >= 1 and i <= -1 + ni }";
		String memoryBasedPlutoSchedule = "[nj, ni] -> { S6[k, j, i] -> [k, j, ni + i, 0, 0]; S0[k] -> [k, k, 0, 0, 0]; S3[k, i] -> [k, k, 1, i, 2]; S4[k, j] -> [0, k, j, 0, 0]; S2[k] -> [k, k, 1, 0, 1]; S5[k, j, i] -> [k, j, i, 1, 0]; S1[k, i] -> [k, k, 1, -ni + i, 0] }";
		String memoryBasedFeautrierSchedule = "[nj, ni] -> { S2[k] -> [2 + 5k, 0, 0]; S0[k] -> [5k, 0, 0]; S3[k, i] -> [3 + 5k, i, 0]; S4[k, j] -> [0, k, j]; S1[k, i] -> [1 + 5k, i, 0]; S6[k, j, i] -> [2 + 2k + 3j, k + j, i]; S5[k, j, i] -> [1 + 2k + 3j, i, k + j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #6
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_multirefs_test_multirefs/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       S0 (depth = 1) [A[i]=mul(A[i-1],A[i-1])]
	 *       ([A[i]]) = f([A[i-1], A[i-1]])
	 *       (Domain = [N] -> {S0[i] : (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_test_multiref_GScopRoot_test_multirefs_scop_0_ () {
		String path = "polymodel_src_corner_cases_test_multiref_GScopRoot_test_multirefs_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[i] : i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S0[i] -> [0, i, 0] }";
		String writes = "[N] -> { S0[i] -> A[i] }";
		String reads = "[N] -> { S0[i] -> A[-1 + i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S0[i] -> S0[-1 + i] : i >= 1 and i <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "[N] -> { S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S0[i] -> S0[-1 + i] : i >= 1 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #7
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : scop_array_scop_array/scop_0
	 *   for : 0 <= i <= 9 (stride = 1)
	 *     block : 
	 *       for : 0 <= j <= -i+9 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [a[i][0]=add(a[i-j][j],IntExpr(1))]
	 *           ([a[i][0]]) = f([a[i-j][j]])
	 *           (Domain = [] -> {S0[i,j] : (j >= 0) and (-i-j+9 >= 0) and (i >= 0) and (-i+9 >= 0)})
	 *           if : i > j
	 *             block : 
	 *               S1 (depth = 2) [a[i][0]=mul(b[i-j][j],a[j][i-j])]
	 *               ([a[i][0]]) = f([b[i-j][j], a[j][i-j]])
	 *               (Domain = [] -> {S1[i,j] : (i-j-1 >= 0) and (j >= 0) and (-i-j+9 >= 0) and (i >= 0) and (-i+9 >= 0)})
	 *               S2 (depth = 2) [b[i][0]=mul(b[i-j][j],b[j][i-j])]
	 *               ([b[i][0]]) = f([b[i-j][j], b[j][i-j]])
	 *               (Domain = [] -> {S2[i,j] : (i-j-1 >= 0) and (j >= 0) and (-i-j+9 >= 0) and (i >= 0) and (-i+9 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_simple_scop_array_GScopRoot_scop_array_scop_0_ () {
		String path = "basics_simple_scop_array_GScopRoot_scop_array_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S0[i, j] : j >= 0 and j <= 9 - i and i >= 0 and i <= 9; S2[i, j] : j <= -1 + i and j >= 0 and j <= 9 - i and i >= 0 and i <= 9; S1[i, j] : j <= -1 + i and j >= 0 and j <= 9 - i and i >= 0 and i <= 9 }";
		String idSchedules = "{ S1[i, j] -> [0, i, 0, j, 1]; S0[i, j] -> [0, i, 0, j, 0]; S2[i, j] -> [0, i, 0, j, 2] }";
		String writes = "{ S1[i, j] -> a[i, 0]; S2[i, j] -> b[i, 0]; S0[i, j] -> a[i, 0] }";
		String reads = "{ S1[i, j] -> a[j, i - j]; S1[i, j] -> b[i - j, j]; S2[i, j] -> b[j, i - j]; S2[i, j] -> b[i - j, j]; S0[i, j] -> a[i - j, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{  }";
		String valueBasedPlutoSchedule = "{ S0[i, j] -> [i, j]; S1[i, j] -> [i, j]; S2[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "{ S0[i, j] -> [i, j]; S1[i, j] -> [i, j]; S2[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S0[i, j] -> S0[i, -1 + j] : j >= 1 + i and j <= 9 - i and i >= 0; S2[i, 0] -> S1[i, 0] : i <= 9 and i >= 1; S1[i, j] -> S0[i, j] : j <= -1 + i and j >= 0 and j <= 9 - i and i >= 0 and i <= 9; S2[i, j] -> S2[i, -1 + j] : j <= 9 - i and j >= 1 and j <= -1 + i and i <= 9 and i >= 0; S0[i, j] -> S1[i, -1 + j] : j >= 1 and j <= 9 - i and j <= i }";
		String memoryBasedPlutoSchedule = "{ S0[i, j] -> [i, j, 0]; S1[i, j] -> [i, j, 1]; S2[i, j] -> [i, j, 2] }";
		String memoryBasedFeautrierSchedule = "{ S0[i, j] -> [2j, i]; S1[i, j] -> [1 + 2j, i]; S2[i, j] -> [2 + j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #8
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : prodMat_prodMat/scop_0
	 *   block : 
	 *     for : 0 <= i <= 2 (stride = 1)
	 *       for : 0 <= j <= 2 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [c[i][j]=IntExpr(0)]
	 *           ([c[i][j]]) = f([])
	 *           (Domain = [] -> {S0[i,j] : (j >= 0) and (-j+2 >= 0) and (i >= 0) and (-i+2 >= 0)})
	 *           for : 0 <= k <= 2 (stride = 1)
	 *             S1 (depth = 3) [c[i][j]=add(c[i][j],mul(M1[i][k],M2[k][j]))]
	 *             ([c[i][j]]) = f([c[i][j], M1[i][k], M2[k][j]])
	 *             (Domain = [] -> {S1[i,j,k] : (k >= 0) and (-k+2 >= 0) and (j >= 0) and (-j+2 >= 0) and (i >= 0) and (-i+2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_prodMat_GScopRoot_prodMat_scop_0_ () {
		String path = "basics_oldFiles_prodMat_GScopRoot_prodMat_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S0[i, j] : j >= 0 and j <= 2 and i >= 0 and i <= 2; S1[i, j, k] : k >= 0 and k <= 2 and j >= 0 and j <= 2 and i >= 0 and i <= 2 }";
		String idSchedules = "{ S0[i, j] -> [0, i, 0, j, 0, 0, 0]; S1[i, j, k] -> [0, i, 0, j, 1, k, 0] }";
		String writes = "{ S0[i, j] -> c[i, j]; S1[i, j, k] -> c[i, j] }";
		String reads = "{ S1[i, j, k] -> c[i, j]; S1[i, j, k] -> M1[i, k]; S1[i, j, k] -> M2[k, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S1[i, j, k] -> S1[i, j, -1 + k] : k >= 1 and k <= 2 and j >= 0 and j <= 2 and i >= 0 and i <= 2; S1[i, j, 0] -> S0[i, j] : i >= 0 and i <= 2 and j >= 0 and j <= 2 }";
		String valueBasedPlutoSchedule = "{ S0[i, j] -> [0, i, j, 1]; S1[i, j, k] -> [i, 2 + j, 2 + k, 0] }";
		String valueBasedFeautrierSchedule = "{ S1[i, j, k] -> [1 + k, j, i]; S0[i, j] -> [0, i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S1[i, j, k] -> S1[i, j, -1 + k] : i <= 2 and i >= 0 and j <= 2 and j >= 0 and k <= 2 and k >= 1; S1[i, j, 0] -> S0[i, j] : i <= 2 and i >= 0 and j <= 2 and j >= 0 }";
		String memoryBasedPlutoSchedule = "{ S0[i, j] -> [0, i, j, 1]; S1[i, j, k] -> [i, 2 + j, 2 + k, 0] }";
		String memoryBasedFeautrierSchedule = "{ S1[i, j, k] -> [1 + k, j, i]; S0[i, j] -> [0, i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #9
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : prodmat_prodmat/scop_0
	 *   block : 
	 *     for : 0 <= i <= M-1 (stride = 1)
	 *       for : 0 <= j <= N-1 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [C[i][j]=IntExpr(0)]
	 *           ([C[i][j]]) = f([])
	 *           (Domain = [M,N,P] -> {S0[i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+M-1 >= 0)})
	 *           for : 0 <= k <= P-1 (stride = 1)
	 *             S1 (depth = 3) [C[i][j]=add(C[i][j],mul(A[i][k],B[k][j]))]
	 *             ([C[i][j]]) = f([C[i][j], A[i][k], B[k][j]])
	 *             (Domain = [M,N,P] -> {S1[i,j,k] : (k >= 0) and (-k+P-1 >= 0) and (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+M-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_prodmat_GScopRoot_prodmat_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_prodmat_GScopRoot_prodmat_scop_0_";
		// SCoP Extraction Data
		String domains = "[M, N, P] -> { S1[i, j, k] : k >= 0 and k <= -1 + P and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + M; S0[i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + M }";
		String idSchedules = "[M, N, P] -> { S0[i, j] -> [0, i, 0, j, 0, 0, 0]; S1[i, j, k] -> [0, i, 0, j, 1, k, 0] }";
		String writes = "[M, N, P] -> { S1[i, j, k] -> C[i, j]; S0[i, j] -> C[i, j] }";
		String reads = "[M, N, P] -> { S1[i, j, k] -> A[i, k]; S1[i, j, k] -> C[i, j]; S1[i, j, k] -> B[k, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[M, N, P] -> { S1[i, j, k] -> S1[i, j, -1 + k] : k >= 1 and k <= -1 + P and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + M; S1[i, j, 0] -> S0[i, j] : i <= -1 + M and P >= 1 and j >= 0 and j <= -1 + N and i >= 0 }";
		String valueBasedPlutoSchedule = "[M, N, P] -> { S1[i, j, k] -> [i, i + j, i + j + k, 1]; S0[i, j] -> [0, i, j, 0] }";
		String valueBasedFeautrierSchedule = "[M, N, P] -> { S0[i, j] -> [0, i, j]; S1[i, j, k] -> [1 + k, j, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[M, N, P] -> { S1[i, j, k] -> S1[i, j, -1 + k] : i >= 0 and i <= -1 + M and j >= 0 and j <= -1 + N and k >= 1 and k <= -1 + P; S1[i, j, 0] -> S0[i, j] : P >= 1 and i >= 0 and i <= -1 + M and j >= 0 and j <= -1 + N }";
		String memoryBasedPlutoSchedule = "[M, N, P] -> { S1[i, j, k] -> [i, i + j, i + j + k, 1]; S0[i, j] -> [0, i, j, 0] }";
		String memoryBasedFeautrierSchedule = "[M, N, P] -> { S0[i, j] -> [0, i, j]; S1[i, j, k] -> [1 + k, j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #10
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_scop8_test_scop8/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         if : i-2*M > 3
	 *           block : 
	 *             S0 (depth = 1) [X[i]=IntExpr(2)]
	 *             ([X[i]]) = f([])
	 *             (Domain = [N,M] -> {S0[i] : (i-2*M-4 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *             for : 1 <= j <= M-i-1 (stride = 1)
	 *               S1 (depth = 2) [X[i]=call min(X[i],X[i-1])]
	 *               ([X[i]]) = f([X[i], X[i-1]])
	 *               (Domain = [N,M] -> {S1[i,j] : 1 = 0})
	 *         S2 (depth = 1) [X[-2*N+i+M]=IntExpr(1)]
	 *         ([X[-2*N+i+M]]) = f([])
	 *         (Domain = [N,M] -> {S2[i] : (i-1 >= 0) and (-i+N-1 >= 0)})
	 *         for : i <= j <= M-i-1 (stride = 1)
	 *           S3 (depth = 2) [X[i]=IntExpr(4)]
	 *           ([X[i]]) = f([])
	 *           (Domain = [N,M] -> {S3[i,j] : (-i+j >= 0) and (-i-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop8_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop8_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S0[i] : i >= 4 + 2M and i >= 1 and i <= -1 + N; S2[i] : i >= 1 and i <= -1 + N; S3[i, j] : j >= i and j <= -1 + M - i and i >= 1 and i <= -1 + N }";
		String idSchedules = "[N, M] -> { S1[i, j] -> [0, i, 1, j, 0]; S2[i] -> [0, i, 2, 0, 0]; S0[i] -> [0, i, 0, 0, 0]; S3[i, j] -> [0, i, 3, j, 0] }";
		String writes = "[N, M] -> { S3[i, j] -> X[i]; S0[i] -> X[i]; S1[i, j] -> X[i]; S2[i] -> X[-2N + M + i] }";
		String reads = "[N, M] -> { S1[i, j] -> X[i]; S1[i, j] -> X[-1 + i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> {  }";
		String valueBasedPlutoSchedule = "[N, M] -> { S3[i, j] -> [i, j]; S2[i] -> [i, 0]; S0[i] -> [i, 0] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S3[i, j] -> [i, j]; S2[i] -> [i, 0]; S0[i] -> [i, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> { S3[i, j] -> S3[i, -1 + j] : j >= 1 + i and j <= -1 + M - i and i >= 1 and i <= -1 + N; S3[i, i] -> S2[2N - M + i] : i <= -1 + N and M >= 2N and i >= 1 - 2N + M; S2[i] -> S3[-2N + M + i, -1 + 2N - i] : i >= 1 + 2N - M and i <= -1 + N and M <= -1 + 2N and i >= 1 }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S3[i, j] -> [i, j, 0]; S0[i] -> [i, 0, 0]; S2[i] -> [i, 0, 1] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S3[i, j] -> [i, 1, j]; S0[i] -> [i, 0, 0]; S2[i] -> [i, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #11
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_scop2_test_scop2/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [X[i]=IntExpr(1)]
	 *         ([X[i]]) = f([])
	 *         (Domain = [N,M] -> {S0[i] : (i-1 >= 0) and (-i+N-1 >= 0)})
	 *         for : 1 <= j <= M-1 (stride = 1)
	 *           if : i > j
	 *             S1 (depth = 2) [X[i]=IntExpr(2)]
	 *             ([X[i]]) = f([])
	 *             (Domain = [N,M] -> {S1[i,j] : (i-j-1 >= 0) and (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *           else
	 *             S2 (depth = 2) [X[i]=IntExpr(3)]
	 *             ([X[i]]) = f([])
	 *             (Domain = [N,M] -> {S2[i,j] : (-i+j >= 0) and (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop2_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop2_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S2[i, j] : j >= i and j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S0[i] : i >= 1 and i <= -1 + N; S1[i, j] : j <= -1 + i and j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N }";
		String idSchedules = "[N, M] -> { S1[i, j] -> [0, i, 1, j, 0]; S2[i, j] -> [0, i, 1, j, 1]; S0[i] -> [0, i, 0, 0, 0] }";
		String writes = "[N, M] -> { S2[i, j] -> X[i]; S0[i] -> X[i]; S1[i, j] -> X[i] }";
		String reads = "[N, M] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> {  }";
		String valueBasedPlutoSchedule = "[N, M] -> { S2[i, j] -> [i, j]; S0[i] -> [i, 0]; S1[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S2[i, j] -> [i, j]; S0[i] -> [i, 0]; S1[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> { S1[i, 1] -> S0[i] : i >= 2 and i <= -1 + N and M >= 2; S2[1, 1] -> S0[1] : N >= 2 and M >= 2; S2[i, i] -> S1[i, -1 + i] : i <= -1 + N and i >= 2 and i <= -1 + M; S1[i, j] -> S1[i, -1 + j] : j <= -1 + i and i <= -1 + N and j <= -1 + M and j >= 2 and i >= 1; S2[i, j] -> S2[i, -1 + j] : i <= -1 + N and j >= 1 + i and j <= -1 + M and i >= 1 and j >= 1 }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S2[i, j] -> [i, 2j, 2]; S0[i] -> [0, i, 1]; S1[i, j] -> [i, i + j, 0] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S2[i, j] -> [j, i]; S0[i] -> [0, i]; S1[i, j] -> [j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #12
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_scop6_test_scop6/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [X[-2*N+i+M]=IntExpr(1)]
	 *         ([X[-2*N+i+M]]) = f([])
	 *         (Domain = [N,M] -> {S0[i] : (i-1 >= 0) and (-i+N-1 >= 0)})
	 *         if : i-M > 2
	 *           block : 
	 *             S1 (depth = 1) [X[i]=IntExpr(2)]
	 *             ([X[i]]) = f([])
	 *             (Domain = [N,M] -> {S1[i] : (i-M-3 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *             for : 1 <= j <= M-i-1 (stride = 1)
	 *               S2 (depth = 2) [X[i]=IntExpr(3)]
	 *               ([X[i]]) = f([])
	 *               (Domain = [N,M] -> {S2[i,j] : 1 = 0})
	 *         for : i <= j <= M-i-1 (stride = 1)
	 *           S3 (depth = 2) [X[i]=IntExpr(4)]
	 *           ([X[i]]) = f([])
	 *           (Domain = [N,M] -> {S3[i,j] : (-i+j >= 0) and (-i-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop6_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop6_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S0[i] : i >= 1 and i <= -1 + N; S1[i] : i >= 3 + M and i >= 1 and i <= -1 + N; S3[i, j] : j >= i and j <= -1 + M - i and i >= 1 and i <= -1 + N }";
		String idSchedules = "[N, M] -> { S2[i, j] -> [0, i, 2, j, 0]; S0[i] -> [0, i, 0, 0, 0]; S3[i, j] -> [0, i, 3, j, 0]; S1[i] -> [0, i, 1, 0, 0] }";
		String writes = "[N, M] -> { S2[i, j] -> X[i]; S3[i, j] -> X[i]; S0[i] -> X[-2N + M + i]; S1[i] -> X[i] }";
		String reads = "[N, M] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> {  }";
		String valueBasedPlutoSchedule = "[N, M] -> { S3[i, j] -> [i, j]; S1[i] -> [i, 0]; S0[i] -> [i, 0] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S3[i, j] -> [i, j]; S1[i] -> [i, 0]; S0[i] -> [i, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> { S3[i, i] -> S0[2N - M + i] : i <= -1 + N and M >= 2N and i >= 1 - 2N + M; S3[i, j] -> S3[i, -1 + j] : j >= 1 + i and j <= -1 + M - i and i >= 1 and i <= -1 + N; S0[i] -> S3[-2N + M + i, -1 + 2N - i] : i >= 1 + 2N - M and i <= -1 + N and M <= -1 + 2N and i >= 1 }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S1[i] -> [i, 0, 0]; S3[i, j] -> [i, j, 0]; S0[i] -> [i, 0, 1] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S1[i] -> [i, 0, 0]; S3[i, j] -> [i, 1, j]; S0[i] -> [i, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #13
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_not_a_scop1_test_not_a_scop1/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       for : 1 <= j <= M-1 (stride = 1)
	 *         S0 (depth = 2) [X[i]=call min(X[i],add(X[i-2],IntExpr(4)))]
	 *         ([X[i]]) = f([X[i], X[i-2]])
	 *         (Domain = [N,M] -> {S0[i,j] : (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_not_a_scop1_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_not_a_scop1_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S0[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N }";
		String idSchedules = "[N, M] -> { S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[N, M] -> { S0[i, j] -> X[i] }";
		String reads = "[N, M] -> { S0[i, j] -> X[i]; S0[i, j] -> X[-2 + i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> { S0[i, j] -> S0[-2 + i, -1 + M] : j >= 1 and j <= -1 + M and i >= 3 and i <= -1 + N; S0[i, j] -> S0[i, -1 + j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -1 + N }";
		String valueBasedPlutoSchedule = "[N, M] -> { S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> { S0[i, j] -> S0[-2 + i, -1 + M] : i >= 3 and i <= -1 + N and j >= 1 and j <= -1 + M; S0[i, j] -> S0[i, -1 + j] : i >= 1 and i <= -1 + N and j >= 2 and j <= -1 + M }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S0[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S0[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #14
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_scop7_test_scop7/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [X[-2*N+i+M]=IntExpr(1)]
	 *         ([X[-2*N+i+M]]) = f([])
	 *         (Domain = [N,M] -> {S0[i] : (i-1 >= 0) and (-i+N-1 >= 0)})
	 *         if : i-M > 5
	 *           block : 
	 *             S1 (depth = 1) [X[i]=IntExpr(2)]
	 *             ([X[i]]) = f([])
	 *             (Domain = [N,M] -> {S1[i] : (i-M-6 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *             for : 1 <= j <= M-i-1 (stride = 1)
	 *               S2 (depth = 2) [X[i]=IntExpr(3)]
	 *               ([X[i]]) = f([])
	 *               (Domain = [N,M] -> {S2[i,j] : 1 = 0})
	 *         for : i <= j <= M-i-1 (stride = 1)
	 *           S3 (depth = 2) [X[i]=IntExpr(4)]
	 *           ([X[i]]) = f([])
	 *           (Domain = [N,M] -> {S3[i,j] : (-i+j >= 0) and (-i-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop7_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop7_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S0[i] : i >= 1 and i <= -1 + N; S1[i] : i >= 6 + M and i >= 1 and i <= -1 + N; S3[i, j] : j >= i and j <= -1 + M - i and i >= 1 and i <= -1 + N }";
		String idSchedules = "[N, M] -> { S2[i, j] -> [0, i, 2, j, 0]; S0[i] -> [0, i, 0, 0, 0]; S3[i, j] -> [0, i, 3, j, 0]; S1[i] -> [0, i, 1, 0, 0] }";
		String writes = "[N, M] -> { S2[i, j] -> X[i]; S3[i, j] -> X[i]; S0[i] -> X[-2N + M + i]; S1[i] -> X[i] }";
		String reads = "[N, M] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> {  }";
		String valueBasedPlutoSchedule = "[N, M] -> { S3[i, j] -> [i, j]; S1[i] -> [i, 0]; S0[i] -> [i, 0] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S3[i, j] -> [i, j]; S1[i] -> [i, 0]; S0[i] -> [i, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> { S3[i, i] -> S0[2N - M + i] : i <= -1 + N and M >= 2N and i >= 1 - 2N + M; S3[i, j] -> S3[i, -1 + j] : j >= 1 + i and j <= -1 + M - i and i >= 1 and i <= -1 + N; S0[i] -> S3[-2N + M + i, -1 + 2N - i] : i >= 1 + 2N - M and i <= -1 + N and M <= -1 + 2N and i >= 1 }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S1[i] -> [i, 0, 0]; S3[i, j] -> [i, j, 0]; S0[i] -> [i, 0, 1] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S1[i] -> [i, 0, 0]; S3[i, j] -> [i, 1, j]; S0[i] -> [i, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #15
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_notafullscop9_scop_new
	 *   block : 
	 *     if : 2*i+M > 3
	 *       block : 
	 *         S0 (depth = 0) [X[i]=IntExpr(2)]
	 *         ([X[i]]) = f([])
	 *         (Domain = [i,M,N] -> {S0[] : 2*i+M-4 >= 0})
	 *         for : 1 <= j <= M-i-1 (stride = 1)
	 *           S1 (depth = 1) [X[i]=call min(X[i],X[i-1])]
	 *           ([X[i]]) = f([X[i], X[i-1]])
	 *           (Domain = [i,M,N] -> {S1[j] : (j-1 >= 0) and (-j-i+M-1 >= 0) and (2*i+M-4 >= 0)})
	 *     S2 (depth = 0) [X[-2*N+i+M]=IntExpr(1)]
	 *     ([X[-2*N+i+M]]) = f([])
	 *     (Domain = [i,M,N] -> {S2[] : })
	 *     for : i <= j <= M-i-1 (stride = 1)
	 *       S3 (depth = 1) [X[i]=IntExpr(4)]
	 *       ([X[i]]) = f([])
	 *       (Domain = [i,M,N] -> {S3[j] : (j-i >= 0) and (-j-i+M-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_scop_tests_GScopRoot_scop_new_ () {
		String path = "polymodel_src_polybenchs_perso_scop_tests_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[i, M, N] -> { S0[] : M >= 4 - 2i; S1[j] : j >= 1 and j <= -1 - i + M and M >= 4 - 2i; S3[j] : j >= i and j <= -1 - i + M; S2[] }";
		String idSchedules = "[i, M, N] -> { S0[] -> [0, 0, 0]; S1[j] -> [1, j, 0]; S2[] -> [2, 0, 0]; S3[j] -> [3, j, 0] }";
		String writes = "[i, M, N] -> { S2[] -> X[i + M - 2N]; S0[] -> X[i]; S1[j] -> X[i]; S3[j] -> X[i] }";
		String reads = "[i, M, N] -> { S1[j] -> X[i]; S1[j] -> X[-1 + i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[i, M, N] -> { S1[1] -> S0[] : M >= 4 - 2i and M >= 2 + i; S1[j] -> S1[-1 + j] : j >= 2 and j <= -1 - i + M and M >= 4 - 2i }";
		String valueBasedPlutoSchedule = "[i, M, N] -> { S2[] -> [0, 0]; S0[] -> [0, 1]; S3[j] -> [j, 0]; S1[j] -> [j, 0] }";
		String valueBasedFeautrierSchedule = "[i, M, N] -> { S0[] -> [0]; S1[j] -> [j]; S3[j] -> [j]; S2[] -> [0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[i, M, N] -> { S1[1] -> S0[] : M >= 2 + i and M >= 4 - 2i; S3[i] -> S1[-1 - i + M] : (M >= 1 + 2i and M >= 4 - 2i and 2N <= -1 + M) or (M >= 1 + 2i and M >= 4 - 2i and 2N >= 1 + M); S3[i] -> S2[] : 2N = M and M >= 1 + 2i; S2[] -> S0[] : 2N = M and M <= 1 + i and M >= 4 - 2i; S2[] -> S1[j] : 2N = 1 + M and M >= 4 - 2i and j <= -1 - i + M and j >= 1; S2[] -> S1[-1 - i + M] : 2N = M and M >= 2 + i and M >= 4 - 2i; S1[j] -> S1[-1 + j] : M >= 4 - 2i and j <= -1 - i + M and j >= 2; S3[j] -> S3[-1 + j] : j >= 1 + i and j <= -1 - i + M }";
		String memoryBasedPlutoSchedule = "[i, M, N] -> { S0[] -> [0, 0, 0]; S1[j] -> [1, -2M + j, 2]; S2[] -> [1, 0, 0]; S3[j] -> [1, -i + j, 1] }";
		String memoryBasedFeautrierSchedule = "[i, M, N] -> { S0[] -> [0]; S1[j] -> [j]; S3[j] -> [j]; S2[] -> [0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #16
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_scop5_test_scop5/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [X[-2*N+i+M]=IntExpr(1)]
	 *         ([X[-2*N+i+M]]) = f([])
	 *         (Domain = [N,M] -> {S0[i] : (i-1 >= 0) and (-i+N-1 >= 0)})
	 *         if : i-M > 2
	 *           block : 
	 *             S1 (depth = 1) [X[i]=IntExpr(2)]
	 *             ([X[i]]) = f([])
	 *             (Domain = [N,M] -> {S1[i] : (i-M-3 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *             for : 1 <= j <= M-i-1 (stride = 1)
	 *               S2 (depth = 2) [X[i]=IntExpr(3)]
	 *               ([X[i]]) = f([])
	 *               (Domain = [N,M] -> {S2[i,j] : 1 = 0})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop5_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop5_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S0[i] : i >= 1 and i <= -1 + N; S1[i] : i >= 3 + M and i >= 1 and i <= -1 + N }";
		String idSchedules = "[N, M] -> { S2[i, j] -> [0, i, 2, j, 0]; S0[i] -> [0, i, 0, 0, 0]; S1[i] -> [0, i, 1, 0, 0] }";
		String writes = "[N, M] -> { S2[i, j] -> X[i]; S0[i] -> X[-2N + M + i]; S1[i] -> X[i] }";
		String reads = "[N, M] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> {  }";
		String valueBasedPlutoSchedule = "[N, M] -> { S0[i] -> [i]; S1[i] -> [i] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S0[i] -> [i]; S1[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> {  }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S0[i] -> [i]; S1[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S0[i] -> [i]; S1[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #17
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_scop4_test_scop4/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [X[i]=IntExpr(1)]
	 *         ([X[i]]) = f([])
	 *         (Domain = [N,M] -> {S0[i] : (i-1 >= 0) and (-i+N-1 >= 0)})
	 *         if : i > N
	 *           block : 
	 *             S1 (depth = 1) [X[i]=IntExpr(2)]
	 *             ([X[i]]) = f([])
	 *             (Domain = [N,M] -> {S1[i] : 1 = 0})
	 *             for : 1 <= j <= M-1 (stride = 1)
	 *               S2 (depth = 2) [X[i]=IntExpr(3)]
	 *               ([X[i]]) = f([])
	 *               (Domain = [N,M] -> {S2[i,j] : 1 = 0})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop4_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop4_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S0[i] : i >= 1 and i <= -1 + N }";
		String idSchedules = "[N, M] -> { S2[i, j] -> [0, i, 2, j, 0]; S0[i] -> [0, i, 0, 0, 0]; S1[i] -> [0, i, 1, 0, 0] }";
		String writes = "[N, M] -> { S2[i, j] -> X[i]; S0[i] -> X[i]; S1[i] -> X[i] }";
		String reads = "[N, M] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> {  }";
		String valueBasedPlutoSchedule = "[N, M] -> { S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> {  }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #18
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_scop1_test_scop1/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       for : 1 <= j <= M-1 (stride = 1)
	 *         if : 2*i+M > 3
	 *           block : 
	 *             for : 1 <= k <= M-i-1 (stride = 1)
	 *               S0 (depth = 3) [X[i+1]=call min(X[i],X[i-2])]
	 *               ([X[i+1]]) = f([X[i], X[i-2]])
	 *               (Domain = [N,M] -> {S0[i,j,k] : (k-1 >= 0) and (-i-k+M-1 >= 0) and (2*i+M-4 >= 0) and (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *             if : i > j
	 *               S1 (depth = 2) [X[i+1]=IntExpr(1)]
	 *               ([X[i+1]]) = f([])
	 *               (Domain = [N,M] -> {S1[i,j] : (i-j-1 >= 0) and (2*i+M-4 >= 0) and (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop1_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop1_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S0[i, j, k] : k >= 1 and k <= -1 + M - i and 2i >= 4 - M and j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S1[i, j] : j <= -1 + i and 2i >= 4 - M and j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N }";
		String idSchedules = "[N, M] -> { S0[i, j, k] -> [0, i, 0, j, 0, k, 0]; S1[i, j] -> [0, i, 0, j, 1, 0, 0] }";
		String writes = "[N, M] -> { S0[i, j, k] -> X[1 + i]; S1[i, j] -> X[1 + i] }";
		String reads = "[N, M] -> { S0[i, j, k] -> X[i]; S0[i, j, k] -> X[-2 + i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> { S0[i, j, k] -> S0[-3 + i, -1 + M, 2 + M - i] : k >= 1 and k <= -1 + M - i and i <= -1 + N and j >= 1 and j <= -1 + M and i >= 4; S0[i, j, k] -> S0[-1 + i, -1 + M, M - i] : k >= 1 and k <= -1 + M - i and i <= -1 + N and j >= 1 and j <= -1 + M and i >= 2 }";
		String valueBasedPlutoSchedule = "[N, M] -> { S0[i, j, k] -> [i, -j, -k]; S1[i, j] -> [i, j, 0] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S0[i, j, k] -> [-k, j, i]; S1[i, j] -> [i, j, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> { S0[i, j, k] -> S0[-3 + i, -1 + M, 2 + M - i] : i >= 4 and i <= -1 + N and j >= 1 and j <= -1 + M and k >= 1 and k <= -1 + M - i; S0[i, j, k] -> S0[-1 + i, -1 + M, M - i] : i >= 2 and i <= -1 + N and j >= 1 and j <= -1 + M and k >= 1 and k <= -1 + M - i; S0[i, j, k] -> S0[i, j, -1 + k] : i >= 1 and i <= -1 + N and j >= 1 and j <= -1 + M and k >= 2 and k <= -1 + M - i; S0[i, j, 1] -> S0[i, -1 + j, -1 + M - i] : i >= 1 and i <= -1 + N and j >= 1 + i and j <= -1 + M; S0[i, j, 1] -> S1[i, -1 + j] : j >= 2 and i <= -2 + M and i <= -1 + N and j <= i and j <= -1 + M and 2i >= 4 - M and i >= 1; S1[i, j] -> S1[i, -1 + j] : j <= -1 + i and i <= -1 + N and j >= 2 and j <= -1 + M and i >= -1 + M and i >= 1 and 2i >= 4 - M; S1[i, j] -> S0[i, j, -1 + M - i] : j <= -1 + i and i <= -2 + M and j >= 1 and i <= -1 + N and i >= 1 and 2i >= 4 - M and j <= -1 + M }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S0[i, j, k] -> [i, j, k, 1]; S1[i, j] -> [i, j, M, 0] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S0[i, j, k] -> [i, j, 0, k]; S1[i, j] -> [i, j, 1, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #19
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_scop3_test_scop3/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [X[i]=IntExpr(1)]
	 *         ([X[i]]) = f([])
	 *         (Domain = [N,M] -> {S0[i] : (i-1 >= 0) and (-i+N-1 >= 0)})
	 *         for : 1 <= j <= M-1 (stride = 1)
	 *           block : 
	 *             if : i > j
	 *               S1 (depth = 2) [X[i]=IntExpr(2)]
	 *               ([X[i]]) = f([])
	 *               (Domain = [N,M] -> {S1[i,j] : (i-j-1 >= 0) and (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *             S2 (depth = 2) [X[i]=IntExpr(3)]
	 *             ([X[i]]) = f([])
	 *             (Domain = [N,M] -> {S2[i,j] : (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *             if : i <= j+5
	 *               S3 (depth = 2) [X[i]=IntExpr(4)]
	 *               ([X[i]]) = f([])
	 *               (Domain = [N,M] -> {S3[i,j] : (-i+j+5 >= 0) and (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop3_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_scop_tests_GScopRoot_test_scop3_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S2[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S0[i] : i >= 1 and i <= -1 + N; S1[i, j] : j <= -1 + i and j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S3[i, j] : j >= -5 + i and j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N }";
		String idSchedules = "[N, M] -> { S1[i, j] -> [0, i, 1, j, 0]; S2[i, j] -> [0, i, 1, j, 1]; S0[i] -> [0, i, 0, 0, 0]; S3[i, j] -> [0, i, 1, j, 2] }";
		String writes = "[N, M] -> { S2[i, j] -> X[i]; S3[i, j] -> X[i]; S0[i] -> X[i]; S1[i, j] -> X[i] }";
		String reads = "[N, M] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> {  }";
		String valueBasedPlutoSchedule = "[N, M] -> { S3[i, j] -> [i, j]; S2[i, j] -> [i, j]; S0[i] -> [i, 0]; S1[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S3[i, j] -> [i, j]; S2[i, j] -> [i, j]; S0[i] -> [i, 0]; S1[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> { S1[i, 1] -> S0[i] : i >= 2 and i <= -1 + N and M >= 2; S2[i, j] -> S3[i, -1 + j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -1 + N and j >= i; S1[i, j] -> S3[i, -1 + j] : j <= -1 + i and j >= 2 and j <= -1 + M and j >= -4 + i and i <= -1 + N and i >= 1; S1[i, j] -> S2[i, -1 + j] : j <= -5 + i and j >= 2 and j <= -1 + M and i >= 7 and i <= -1 + N; S3[i, j] -> S2[i, j] : j >= -5 + i and j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S2[1, 1] -> S0[1] : N >= 2 and M >= 2; S2[i, j] -> S1[i, j] : j >= 1 and j <= -1 + M and j <= -1 + i and i <= -1 + N and i >= 1 }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S2[i, j] -> [i, i + j, 1]; S3[i, j] -> [i, i + j, 2]; S0[i] -> [0, i, 3]; S1[i, j] -> [i, i + j, 0] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S2[i, j] -> [j, 1, i]; S3[i, j] -> [j, 2, i]; S0[i] -> [0, 0, i]; S1[i, j] -> [j, 0, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #20
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_scop10_scop_new
	 *   block : 
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         if : i-M > 3
	 *           block : 
	 *             S0 (depth = 1) [X[i]=IntExpr(2)]
	 *             ([X[i]]) = f([])
	 *             (Domain = [N,M] -> {S0[i] : (i-M-4 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *             for : 1 <= j <= M-i-1 (stride = 1)
	 *               S1 (depth = 2) [X[i]=call min(X[i],X[i-1])]
	 *               ([X[i]]) = f([X[i], X[i-1]])
	 *               (Domain = [N,M] -> {S1[i,j] : 1 = 0})
	 *         S2 (depth = 1) [X[-2*N+i+M]=IntExpr(1)]
	 *         ([X[-2*N+i+M]]) = f([])
	 *         (Domain = [N,M] -> {S2[i] : (i-1 >= 0) and (-i+N-1 >= 0)})
	 *         for : i <= j <= M-i-1 (stride = 1)
	 *           S3 (depth = 2) [X[i]=IntExpr(4)]
	 *           ([X[i]]) = f([])
	 *           (Domain = [N,M] -> {S3[i,j] : (-i+j >= 0) and (-i-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         if : i-M > 3
	 *           block : 
	 *             S4 (depth = 1) [X[i]=IntExpr(2)]
	 *             ([X[i]]) = f([])
	 *             (Domain = [N,M] -> {S4[i] : (i-M-4 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *             for : 1 <= j <= M-i-1 (stride = 1)
	 *               S5 (depth = 2) [X[i]=call min(X[i],X[i-1])]
	 *               ([X[i]]) = f([X[i], X[i-1]])
	 *               (Domain = [N,M] -> {S5[i,j] : 1 = 0})
	 *         S6 (depth = 1) [X[-2*N+i+M]=IntExpr(1)]
	 *         ([X[-2*N+i+M]]) = f([])
	 *         (Domain = [N,M] -> {S6[i] : (i-1 >= 0) and (-i+N-1 >= 0)})
	 *         for : i <= j <= M-i-1 (stride = 1)
	 *           S7 (depth = 2) [X[i]=IntExpr(4)]
	 *           ([X[i]]) = f([])
	 *           (Domain = [N,M] -> {S7[i,j] : (-i+j >= 0) and (-i-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_scop_tests_GScopRoot_scop_new__0 () {
		String path = "polymodel_src_polybenchs_perso_scop_tests_GScopRoot_scop_new__0";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S6[i] : i >= 1 and i <= -1 + N; S4[i] : i >= 4 + M and i >= 1 and i <= -1 + N; S0[i] : i >= 4 + M and i >= 1 and i <= -1 + N; S7[i, j] : j >= i and j <= -1 + M - i and i >= 1 and i <= -1 + N; S2[i] : i >= 1 and i <= -1 + N; S3[i, j] : j >= i and j <= -1 + M - i and i >= 1 and i <= -1 + N }";
		String idSchedules = "[N, M] -> { S1[i, j] -> [0, i, 1, j, 0]; S2[i] -> [0, i, 2, 0, 0]; S5[i, j] -> [1, i, 1, j, 0]; S6[i] -> [1, i, 2, 0, 0]; S0[i] -> [0, i, 0, 0, 0]; S7[i, j] -> [1, i, 3, j, 0]; S3[i, j] -> [0, i, 3, j, 0]; S4[i] -> [1, i, 0, 0, 0] }";
		String writes = "[N, M] -> { S3[i, j] -> X[i]; S7[i, j] -> X[i]; S6[i] -> X[-2N + M + i]; S0[i] -> X[i]; S1[i, j] -> X[i]; S5[i, j] -> X[i]; S2[i] -> X[-2N + M + i]; S4[i] -> X[i] }";
		String reads = "[N, M] -> { S1[i, j] -> X[i]; S1[i, j] -> X[-1 + i]; S5[i, j] -> X[i]; S5[i, j] -> X[-1 + i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> {  }";
		String valueBasedPlutoSchedule = "[N, M] -> { S3[i, j] -> [i, j]; S4[i] -> [i, 0]; S2[i] -> [i, 0]; S6[i] -> [i, 0]; S7[i, j] -> [i, j]; S0[i] -> [i, 0] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S3[i, j] -> [i, j]; S4[i] -> [i, 0]; S2[i] -> [i, 0]; S6[i] -> [i, 0]; S7[i, j] -> [i, j]; S0[i] -> [i, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> { S7[i, i] -> S6[2N - M + i] : i <= -1 + N and M >= 2N and i >= 1 - 2N + M; S6[i] -> S2[i] : (i >= 1 and i <= -1 + N and i <= 2N - M) or (i >= 3N - M and i >= 1 and i <= -1 + N); S7[i, i] -> S2[2N - M + i] : M <= -1 + 2N and i <= -1 - N + M and i >= 1 and i <= -1 + N and 2i <= -1 + M; S6[i] -> S7[-2N + M + i, -1 + 2N - i] : i >= 1 + 2N - M and i <= -1 + N and M <= -1 + 2N and i >= 1; S3[i, j] -> S3[i, -1 + j] : j >= 1 + i and j <= -1 + M - i and i >= 1 and i <= -1 + N; S2[i] -> S3[-2N + M + i, -1 + 2N - i] : i >= 1 + 2N - M and i <= -1 + N and M <= -1 + 2N and i >= 1; S7[i, i] -> S3[i, -1 + M - i] : (i <= -2N + M and i >= 1 and i <= -1 + N) or (i >= 1 and 2i <= -1 + M and i >= -N + M); S7[i, j] -> S7[i, -1 + j] : j >= 1 + i and j <= -1 + M - i and i >= 1 and i <= -1 + N; S6[i] -> S3[-2N + M + i, -1 + 2N - i] : i >= 1 and i <= -1 + N and M >= 2N and i <= -1 + 3N - M; S4[i] -> S0[i] : i >= 4 + M and i >= 1 and i <= -1 + N; S3[i, i] -> S2[2N - M + i] : i <= -1 + N and M >= 2N and i >= 1 - 2N + M }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S3[i, j] -> [i, j, 0]; S6[i] -> [N + i, 0, 1]; S7[i, j] -> [N + i, j, 0]; S0[i] -> [i, 0, 0]; S4[i] -> [i, 1, 0]; S2[i] -> [i, 0, 1] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S3[i, j] -> [i, 1, j]; S6[i] -> [N + i, 0, 0]; S7[i, j] -> [N + i, 1, j]; S0[i] -> [i, 0, 0]; S4[i] -> [i, 0, 0]; S2[i] -> [i, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #21
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : main_main/scop_0
	 *   for : 0 <= i <= 9 (stride = 1)
	 *     for : 0 <= j <= -i+9 (stride = 1)
	 *       block : 
	 *         S0 (depth = 2) [a=sub(a,IntExpr(2))]
	 *         ([a]) = f([a])
	 *         (Domain = [] -> {S0[i,j] : (j >= 0) and (-i-j+9 >= 0) and (i >= 0) and (-i+9 >= 0)})
	 *         if : i > j
	 *           S1 (depth = 2) [a=add(a,IntExpr(1))]
	 *           ([a]) = f([a])
	 *           (Domain = [] -> {S1[i,j] : (i-j-1 >= 0) and (j >= 0) and (-i-j+9 >= 0) and (i >= 0) and (-i+9 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_simple_scop_noarray_GScopRoot_main_scop_0_ () {
		String path = "basics_simple_scop_noarray_GScopRoot_main_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S0[i, j] : j >= 0 and j <= 9 - i and i >= 0 and i <= 9; S1[i, j] : j <= -1 + i and j >= 0 and j <= 9 - i and i >= 0 and i <= 9 }";
		String idSchedules = "{ S1[i, j] -> [0, i, 0, j, 1]; S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "{ S1[i, j] -> a[]; S0[i, j] -> a[] }";
		String reads = "{ S1[i, j] -> a[]; S0[i, j] -> a[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S0[i, j] -> S0[i, -1 + j] : j >= 1 + i and j <= 9 - i and i >= 0; S0[i, 0] -> S0[-1 + i, 10 - i] : i >= 1 and i <= 5; S1[i, j] -> S0[i, j] : j <= -1 + i and j >= 0 and j <= 9 - i and i >= 0 and i <= 9; S0[i, j] -> S1[i, -1 + j] : j >= 1 and j <= 9 - i and j <= i; S0[i, 0] -> S1[-1 + i, 10 - i] : i >= 6 and i <= 9 }";
		String valueBasedPlutoSchedule = "{ S0[i, j] -> [i, 9i + j, 0]; S1[i, j] -> [i, 9i + j, 1] }";
		String valueBasedFeautrierSchedule = "{ S0[i, j] -> [19i + 2j, 9i + j]; S1[i, j] -> [1 + 19i + 2j, 9i + j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S0[i, j] -> S0[i, -1 + j] : i >= 0 and j <= 9 - i and j >= 1 + i; S0[i, 0] -> S0[-1 + i, 10 - i] : i <= 5 and i >= 1; S1[i, j] -> S0[i, j] : j <= 9 - i and j >= 0 and j <= -1 + i; S0[i, j] -> S1[i, -1 + j] : j <= 9 - i and j >= 1 and j <= i; S0[i, 0] -> S1[-1 + i, 10 - i] : i <= 9 and i >= 6 }";
		String memoryBasedPlutoSchedule = "{ S0[i, j] -> [i, 9i + j, 0]; S1[i, j] -> [i, 9i + j, 1] }";
		String memoryBasedFeautrierSchedule = "{ S0[i, j] -> [19i + 2j, 9i + j]; S1[i, j] -> [1 + 19i + 2j, 9i + j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #22
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : stencil_stencil/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       for : 1 <= j <= M-1 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [resid=div(add(add(add(add(uold[i][j],uold[i-1][j]),uold[i+1][j]),uold[i][j-1]),uold[i][j+1]),IntExpr(b))]
	 *           ([resid]) = f([uold[i][j], uold[i-1][j], uold[i+1][j], uold[i][j-1], uold[i][j+1]])
	 *           (Domain = [N,M,b,omega] -> {S0[i,j] : (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *           S1 (depth = 2) [u[i][j]=sub(uold[i][j],mul(IntExpr(omega),resid))]
	 *           ([u[i][j]]) = f([uold[i][j], resid])
	 *           (Domain = [N,M,b,omega] -> {S1[i,j] : (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *           S2 (depth = 2) [error=add(error,mul(resid,resid))]
	 *           ([error]) = f([error, resid, resid])
	 *           (Domain = [N,M,b,omega] -> {S2[i,j] : (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_openMP_Stencil_GScopRoot_stencil_scop_0_ () {
		String path = "polymodel_others_openMP_Stencil_GScopRoot_stencil_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M, b, omega] -> { S1[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S0[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S2[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N }";
		String idSchedules = "[N, M, b, omega] -> { S0[i, j] -> [0, i, 0, j, 0]; S1[i, j] -> [0, i, 0, j, 1]; S2[i, j] -> [0, i, 0, j, 2] }";
		String writes = "[N, M, b, omega] -> { S2[i, j] -> error[]; S1[i, j] -> u[i, j]; S0[i, j] -> resid[] }";
		String reads = "[N, M, b, omega] -> { S0[i, j] -> uold[i, 1 + j]; S0[i, j] -> uold[1 + i, j]; S0[i, j] -> uold[i, j]; S0[i, j] -> uold[-1 + i, j]; S0[i, j] -> uold[i, -1 + j]; S2[i, j] -> error[]; S2[i, j] -> resid[]; S1[i, j] -> resid[]; S1[i, j] -> uold[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M, b, omega] -> { S1[i, j] -> S0[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S2[i, j] -> S0[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S2[i, j] -> S2[i, -1 + j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -1 + N; S2[i, 1] -> S2[-1 + i, -1 + M] : i <= -1 + N and M >= 2 and i >= 2 }";
		String valueBasedPlutoSchedule = "[N, M, b, omega] -> { S2[i, j] -> [i, j, 1]; S0[i, j] -> [i, j, 0]; S1[i, j] -> [i, j, 2] }";
		String valueBasedFeautrierSchedule = "[N, M, b, omega] -> { S1[i, j] -> [i, j]; S2[i, j] -> [i, j]; S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M, b, omega] -> { S2[i, j] -> S2[i, -1 + j] : i >= 1 and i <= -1 + N and j >= 2 and j <= -1 + M; S2[i, 1] -> S2[-1 + i, -1 + M] : M >= 2 and i >= 2 and i <= -1 + N; S2[i, j] -> S0[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S0[i, j] -> S2[i, -1 + j] : i >= 1 and i <= -1 + N and j >= 2 and j <= -1 + M; S0[i, 1] -> S2[-1 + i, -1 + M] : M >= 2 and i >= 2 and i <= -1 + N; S0[i, j] -> S0[i, -1 + j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -1 + N; S0[i, 1] -> S0[-1 + i, -1 + M] : i <= -1 + N and M >= 2 and i >= 2; S1[i, j] -> S0[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S0[i, j] -> S1[i, -1 + j] : i >= 1 and i <= -1 + N and j >= 2 and j <= -1 + M; S0[i, 1] -> S1[-1 + i, -1 + M] : M >= 2 and i >= 2 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N, M, b, omega] -> { S2[i, j] -> [i, j, 1]; S0[i, j] -> [i, j, 0]; S1[i, j] -> [i, j, 2] }";
		String memoryBasedFeautrierSchedule = "[N, M, b, omega] -> { S2[i, j] -> [i, j, 1]; S0[i, j] -> [i, j, 0]; S1[i, j] -> [i, j, 1] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #23
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : gauss_gauss/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-2 (stride = 1)
	 *       for : 0 <= j <= M-1 (stride = 1)
	 *         S0 (depth = 2) [tmp[i][j]=sub(sub(mul(IntExpr(3),in[i][j]),in[i-1][j]),in[i+1][j])]
	 *         ([tmp[i][j]]) = f([in[i][j], in[i-1][j], in[i+1][j]])
	 *         (Domain = [N,M] -> {S0[i,j] : (j >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *     for : 1 <= i <= N-2 (stride = 1)
	 *       for : 1 <= j <= M-2 (stride = 1)
	 *         S1 (depth = 2) [out[i][j]=sub(sub(mul(IntExpr(3),tmp[i][j]),tmp[i][j-1]),tmp[i][j+1])]
	 *         ([out[i][j]]) = f([tmp[i][j], tmp[i][j-1], tmp[i][j+1]])
	 *         (Domain = [N,M] -> {S1[i,j] : (j-1 >= 0) and (-j+M-2 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_gauss_GScopRoot_gauss_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_gauss_GScopRoot_gauss_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S1[i, j] : j >= 1 and j <= -2 + M and i >= 1 and i <= -2 + N; S0[i, j] : j >= 0 and j <= -1 + M and i >= 1 and i <= -2 + N }";
		String idSchedules = "[N, M] -> { S1[i, j] -> [1, i, 0, j, 0]; S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[N, M] -> { S1[i, j] -> out[i, j]; S0[i, j] -> tmp[i, j] }";
		String reads = "[N, M] -> { S0[i, j] -> in[1 + i, j]; S0[i, j] -> in[i, j]; S0[i, j] -> in[-1 + i, j]; S1[i, j] -> tmp[i, 1 + j]; S1[i, j] -> tmp[i, j]; S1[i, j] -> tmp[i, -1 + j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> { S1[i, j] -> S0[i, j'] : j' >= -1 + j and i >= 1 and i <= -2 + N and j >= 1 and j <= -2 + M and j' <= 1 + j }";
		String valueBasedPlutoSchedule = "[N, M] -> { S1[i, j] -> [i, 1 + j, 1]; S0[i, j] -> [i, j, 0] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S0[i, j] -> [i, j]; S1[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> { S1[i, j] -> S0[i, j'] : j' >= -1 + j and i >= 1 and i <= -2 + N and j >= 1 and j <= -2 + M and j' <= 1 + j }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S1[i, j] -> [i, 1 + j, 1]; S0[i, j] -> [i, j, 0] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S0[i, j] -> [i, j]; S1[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #24
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_lu_kernel_lu/scop_0
	 *   for : 0 <= k <= n-1 (stride = 1)
	 *     block : 
	 *       for : k+1 <= j <= n-1 (stride = 1)
	 *         S0 (depth = 2) [A[k][j]=div(A[k][j],A[k][k])]
	 *         ([A[k][j]]) = f([A[k][j], A[k][k]])
	 *         (Domain = [n] -> {S0[k,j] : (-k+j-1 >= 0) and (-j+n-1 >= 0) and (k >= 0) and (-k+n-1 >= 0)})
	 *       for : k+1 <= i <= n-1 (stride = 1)
	 *         for : k+1 <= j <= n-1 (stride = 1)
	 *           S1 (depth = 3) [A[i][j]=sub(A[i][j],mul(A[i][k],A[k][j]))]
	 *           ([A[i][j]]) = f([A[i][j], A[i][k], A[k][j]])
	 *           (Domain = [n] -> {S1[k,i,j] : (-k+j-1 >= 0) and (-j+n-1 >= 0) and (-k+i-1 >= 0) and (-i+n-1 >= 0) and (k >= 0) and (-k+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_solvers_lu_lu_GScopRoot_kernel_lu_scop_0_ () {
		String path = "polybench_gecos_linear_algebra_solvers_lu_lu_GScopRoot_kernel_lu_scop_0_";
		// SCoP Extraction Data
		String domains = "[n] -> { S1[k, i, j] : j >= 1 + k and j <= -1 + n and i >= 1 + k and i <= -1 + n and k >= 0 and k <= -1 + n; S0[k, j] : j >= 1 + k and j <= -1 + n and k >= 0 and k <= -1 + n }";
		String idSchedules = "[n] -> { S0[k, j] -> [0, k, 0, j, 0, 0, 0]; S1[k, i, j] -> [0, k, 1, i, 0, j, 0] }";
		String writes = "[n] -> { S0[k, j] -> A[k, j]; S1[k, i, j] -> A[i, j] }";
		String reads = "[n] -> { S0[k, j] -> A[k, k]; S0[k, j] -> A[k, j]; S1[k, i, j] -> A[i, k]; S1[k, i, j] -> A[k, j]; S1[k, i, j] -> A[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S0[k, j] -> S1[-1 + k, k, k] : j >= 1 + k and j <= -1 + n and k >= 1; S0[k, j] -> S1[-1 + k, k, j] : j >= 1 + k and j <= -1 + n and k >= 1; S1[k, i, j] -> S0[k, j] : j >= 1 + k and j <= -1 + n and i >= 1 + k and i <= -1 + n and k >= 0 and k <= -1 + n; S1[k, i, j] -> S1[-1 + k, i, k] : j >= 1 + k and j <= -1 + n and i >= 1 + k and i <= -1 + n and k >= 1; S1[k, i, j] -> S1[-1 + k, i, j] : j >= 1 + k and j <= -1 + n and i >= 1 + k and i <= -1 + n and k >= 1 }";
		String valueBasedPlutoSchedule = "[n] -> { S0[k, j] -> [k, k, j, 0]; S1[k, i, j] -> [k, i, j, 1] }";
		String valueBasedFeautrierSchedule = "[n] -> { S0[k, j] -> [2k, j, 0]; S1[k, i, j] -> [k + i, i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S0[k, j] -> S1[-1 + k, k, k] : k >= 1 and j >= 1 + k and j <= -1 + n; S0[k, j] -> S1[-1 + k, k, j] : k >= 1 and j >= 1 + k and j <= -1 + n; S1[k, i, j] -> S0[k, j] : j >= 1 + k and j <= -1 + n and i >= 1 + k and i <= -1 + n and k >= 0 and k <= -1 + n; S1[k, i, j] -> S1[-1 + k, i, k] : k >= 1 and i >= 1 + k and i <= -1 + n and j >= 1 + k and j <= -1 + n; S1[k, i, j] -> S1[-1 + k, i, j] : k >= 1 and i >= 1 + k and i <= -1 + n and j >= 1 + k and j <= -1 + n }";
		String memoryBasedPlutoSchedule = "[n] -> { S0[k, j] -> [k, k, j, 0]; S1[k, i, j] -> [k, i, j, 1] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S0[k, j] -> [2k, j, 0]; S1[k, i, j] -> [k + i, i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #25
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_floyd_warshall_kernel_floyd_warshall/scop_0
	 *   for : 0 <= k <= n-1 (stride = 1)
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       for : 0 <= j <= n-1 (stride = 1)
	 *         S0 (depth = 3) [path[i][j]=mux(lt(path[i][j],add(path[i][k],path[k][j])),path[i][j],add(path[i][k],path[k][j]))]
	 *         ([path[i][j]]) = f([path[i][j], path[i][k], path[k][j], path[i][j], path[i][k], path[k][j]])
	 *         (Domain = [n] -> {S0[k,i,j] : (j >= 0) and (-j+n-1 >= 0) and (i >= 0) and (-i+n-1 >= 0) and (k >= 0) and (-k+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_medley_floyd_warshall_floyd_warshall_GScopRoot_kernel_floyd_warshall_scop_0_ () {
		String path = "polybench_gecos_medley_floyd_warshall_floyd_warshall_GScopRoot_kernel_floyd_warshall_scop_0_";
		// SCoP Extraction Data
		String domains = "[n] -> { S0[k, i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n and k >= 0 and k <= -1 + n }";
		String idSchedules = "[n] -> { S0[k, i, j] -> [0, k, 0, i, 0, j, 0] }";
		String writes = "[n] -> { S0[k, i, j] -> path[i, j] }";
		String reads = "[n] -> { S0[k, i, j] -> path[i, k]; S0[k, i, j] -> path[k, j]; S0[k, i, j] -> path[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S0[k, i, j] -> S0[k, i, k] : j >= 1 + k and j <= -1 + n and i >= 0 and i <= -1 + n and k >= 0; S0[k, i, j] -> S0[-1 + k, i, k] : i <= -1 + n and i >= 0 and k <= -1 + n and j <= -1 + k and j >= 0; S0[k, i, j] -> S0[k, k, j] : j >= 0 and i >= 1 + k and i <= -1 + n and j <= -1 + n and k >= 0; S0[k, i, j] -> S0[-1 + k, k, j] : j <= -1 + n and i >= 0 and k >= 1 and i <= k and j >= 0 and k <= -1 + n; S0[k, i, j] -> S0[-1 + k, i, j] : (k >= 1 and i >= 1 + k and i <= -1 + n and j >= 0 and j <= -1 + n) or (k <= -1 + n and i >= 0 and i <= -1 + k and j >= 0 and j <= -1 + n) }";
		String valueBasedPlutoSchedule = "[n] -> { S0[k, i, j] -> [k, i, j] }";
		String valueBasedFeautrierSchedule = "[n] -> { S0[k, i, j] -> [k, i + j, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S0[k, i, j] -> S0[k, i, k] : k >= 0 and i >= 0 and i <= -1 + n and j >= 1 + k and j <= -1 + n; S0[k, i, j] -> S0[-1 + k, i, k] : i <= -1 + n and k <= -1 + n and i >= 0 and j >= 0 and k >= 1 and j <= k; S0[k, i, k] -> S0[k, i, j'] : k <= -1 + n and i >= 0 and i <= -1 + n and j' >= 0 and j' <= -1 + k; S0[k, i, -1 + k] -> S0[-1 + k, i, j'] : i >= 0 and j' >= k and j' <= -1 + n and k >= 1 and i <= -1 + n; S0[k, i, j] -> S0[k, k, j] : j >= 0 and i >= 1 + k and i <= -1 + n and k >= 0 and j <= -1 + n; S0[k, i, j] -> S0[-1 + k, k, j] : j <= -1 + n and i >= 0 and i <= -1 + k and k <= -1 + n and j >= 0; S0[k, k, j] -> S0[k, i', j] : i' >= 0 and i' <= -1 + k and j <= -1 + n and k <= -1 + n and j >= 0; S0[k, i, j] -> S0[-1 + k, i, j] : i <= -1 + n and i >= 0 and k >= 1 and j >= 0 and j <= -1 + n and k <= -1 + n; S0[k, -1 + k, j] -> S0[-1 + k, i', j] : i' >= k and i' <= -1 + n and j >= 0 and k >= 1 and j <= -1 + n }";
		String memoryBasedPlutoSchedule = "[n] -> { S0[k, i, j] -> [k, i, j] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S0[k, i, j] -> [k, i + j, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #26
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : fpt_example_fpt_example/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= N-i-1 (stride = 1)
	 *         S0 (depth = 2) [Y[j]=mul(Y[j],Y[j])]
	 *         ([Y[j]]) = f([Y[j], Y[j]])
	 *         (Domain = [N] -> {S0[i,j] : (j >= 0) and (-i-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_fpt_example_GScopRoot_fpt_example_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_fpt_example_GScopRoot_fpt_example_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[i, j] : j >= 0 and j <= -1 + N - i and i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[N] -> { S0[i, j] -> Y[j] }";
		String reads = "[N] -> { S0[i, j] -> Y[j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S0[i, j] -> S0[-1 + i, j] : j >= 0 and j <= -1 + N - i and i >= 1 and i <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S0[i, j] -> S0[-1 + i, j] : i >= 1 and j >= 0 and j <= -1 + N - i }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #27
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_gesummv_kernel_gesummv/scop_0
	 *   for : 0 <= i <= n-1 (stride = 1)
	 *     block : 
	 *       S0 (depth = 1) [tmp[i]=IntExpr(0)]
	 *       ([tmp[i]]) = f([])
	 *       (Domain = [n,alpha,beta] -> {S0[i] : (i >= 0) and (-i+n-1 >= 0)})
	 *       S1 (depth = 1) [y[i]=IntExpr(0)]
	 *       ([y[i]]) = f([])
	 *       (Domain = [n,alpha,beta] -> {S1[i] : (i >= 0) and (-i+n-1 >= 0)})
	 *       for : 0 <= j <= n-1 (stride = 1)
	 *         block : 
	 *           S2 (depth = 2) [tmp[i]=add(mul(A[i][j],x[j]),tmp[i])]
	 *           ([tmp[i]]) = f([A[i][j], x[j], tmp[i]])
	 *           (Domain = [n,alpha,beta] -> {S2[i,j] : (j >= 0) and (-j+n-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *           S3 (depth = 2) [y[i]=add(mul(B[i][j],x[j]),y[i])]
	 *           ([y[i]]) = f([B[i][j], x[j], y[i]])
	 *           (Domain = [n,alpha,beta] -> {S3[i,j] : (j >= 0) and (-j+n-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *       S4 (depth = 1) [y[i]=add(mul(IntExpr(alpha),tmp[i]),mul(IntExpr(beta),y[i]))]
	 *       ([y[i]]) = f([tmp[i], y[i]])
	 *       (Domain = [n,alpha,beta] -> {S4[i] : (i >= 0) and (-i+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_gesummv_gesummv_GScopRoot_kernel_gesummv_scop_0_ () {
		String path = "polybench_gecos_linear_algebra_kernels_gesummv_gesummv_GScopRoot_kernel_gesummv_scop_0_";
		// SCoP Extraction Data
		String domains = "[n, alpha, beta] -> { S0[i] : i >= 0 and i <= -1 + n; S4[i] : i >= 0 and i <= -1 + n; S2[i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n; S3[i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n; S1[i] : i >= 0 and i <= -1 + n }";
		String idSchedules = "[n, alpha, beta] -> { S3[i, j] -> [0, i, 2, j, 1]; S1[i] -> [0, i, 1, 0, 0]; S2[i, j] -> [0, i, 2, j, 0]; S4[i] -> [0, i, 3, 0, 0]; S0[i] -> [0, i, 0, 0, 0] }";
		String writes = "[n, alpha, beta] -> { S2[i, j] -> tmp[i]; S1[i] -> y[i]; S3[i, j] -> y[i]; S4[i] -> y[i]; S0[i] -> tmp[i] }";
		String reads = "[n, alpha, beta] -> { S2[i, j] -> tmp[i]; S2[i, j] -> A[i, j]; S3[i, j] -> y[i]; S4[i] -> y[i]; S3[i, j] -> x[j]; S3[i, j] -> B[i, j]; S2[i, j] -> x[j]; S4[i] -> tmp[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n, alpha, beta] -> { S4[i] -> S3[i, -1 + n] : i >= 0 and i <= -1 + n; S2[i, 0] -> S0[i] : i <= -1 + n and n >= 1 and i >= 0; S4[i] -> S2[i, -1 + n] : i >= 0 and i <= -1 + n; S3[i, 0] -> S1[i] : i <= -1 + n and n >= 1 and i >= 0; S2[i, j] -> S2[i, -1 + j] : j >= 1 and j <= -1 + n and i >= 0 and i <= -1 + n; S3[i, j] -> S3[i, -1 + j] : j >= 1 and j <= -1 + n and i >= 0 and i <= -1 + n }";
		String valueBasedPlutoSchedule = "[n, alpha, beta] -> { S2[i, j] -> [i, i + j, 3]; S0[i] -> [0, i, 2]; S3[i, j] -> [i, i + j, 1]; S1[i] -> [0, i, 0]; S4[i] -> [i, n + i, 4] }";
		String valueBasedFeautrierSchedule = "[n, alpha, beta] -> { S4[i] -> [1 + n, i]; S1[i] -> [0, i]; S2[i, j] -> [1 + j, i]; S3[i, j] -> [1 + j, i]; S0[i] -> [0, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n, alpha, beta] -> { S4[i] -> S3[i, -1 + n] : i >= 0 and i <= -1 + n; S2[i, 0] -> S0[i] : i >= 0 and i <= -1 + n; S4[i] -> S2[i, -1 + n] : i >= 0 and i <= -1 + n; S3[i, 0] -> S1[i] : i >= 0 and i <= -1 + n; S2[i, j] -> S2[i, -1 + j] : i >= 0 and i <= -1 + n and j >= 1 and j <= -1 + n; S3[i, j] -> S3[i, -1 + j] : i >= 0 and i <= -1 + n and j >= 1 and j <= -1 + n }";
		String memoryBasedPlutoSchedule = "[n, alpha, beta] -> { S2[i, j] -> [i, i + j, 3]; S0[i] -> [0, i, 2]; S3[i, j] -> [i, i + j, 1]; S1[i] -> [0, i, 0]; S4[i] -> [i, n + i, 4] }";
		String memoryBasedFeautrierSchedule = "[n, alpha, beta] -> { S4[i] -> [1 + n, i]; S1[i] -> [0, i]; S2[i, j] -> [1 + j, i]; S3[i, j] -> [1 + j, i]; S0[i] -> [0, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #28
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : SetupLargerBlocks_SetupLargerBlocks/scop_0
	 *   block : 
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S0 (depth = 1) [BlockSAD[list][refindex][6][pos]=add(BlockSAD[list][refindex][7][pos],BlockSAD[list][refindex][11][pos])]
	 *       ([BlockSAD[list][refindex][6][pos]]) = f([BlockSAD[list][refindex][7][pos], BlockSAD[list][refindex][11][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S0[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S1 (depth = 1) [BlockSAD[list][refindex][7][pos]=add(BlockSAD[list][refindex][8][pos],BlockSAD[list][refindex][12][pos])]
	 *       ([BlockSAD[list][refindex][7][pos]]) = f([BlockSAD[list][refindex][8][pos], BlockSAD[list][refindex][12][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S1[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S2 (depth = 1) [BlockSAD[list][refindex][8][pos]=add(BlockSAD[list][refindex][9][pos],BlockSAD[list][refindex][13][pos])]
	 *       ([BlockSAD[list][refindex][8][pos]]) = f([BlockSAD[list][refindex][9][pos], BlockSAD[list][refindex][13][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S2[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S3 (depth = 1) [BlockSAD[list][refindex][9][pos]=add(BlockSAD[list][refindex][10][pos],BlockSAD[list][refindex][14][pos])]
	 *       ([BlockSAD[list][refindex][9][pos]]) = f([BlockSAD[list][refindex][10][pos], BlockSAD[list][refindex][14][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S3[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S4 (depth = 1) [BlockSAD[list][refindex][14][pos]=add(BlockSAD[list][refindex][15][pos],BlockSAD[list][refindex][19][pos])]
	 *       ([BlockSAD[list][refindex][14][pos]]) = f([BlockSAD[list][refindex][15][pos], BlockSAD[list][refindex][19][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S4[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S5 (depth = 1) [BlockSAD[list][refindex][15][pos]=add(BlockSAD[list][refindex][16][pos],BlockSAD[list][refindex][20][pos])]
	 *       ([BlockSAD[list][refindex][15][pos]]) = f([BlockSAD[list][refindex][16][pos], BlockSAD[list][refindex][20][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S5[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S6 (depth = 1) [BlockSAD[list][refindex][16][pos]=add(BlockSAD[list][refindex][17][pos],BlockSAD[list][refindex][21][pos])]
	 *       ([BlockSAD[list][refindex][16][pos]]) = f([BlockSAD[list][refindex][17][pos], BlockSAD[list][refindex][21][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S6[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S7 (depth = 1) [BlockSAD[list][refindex][17][pos]=add(BlockSAD[list][refindex][18][pos],BlockSAD[list][refindex][22][pos])]
	 *       ([BlockSAD[list][refindex][17][pos]]) = f([BlockSAD[list][refindex][18][pos], BlockSAD[list][refindex][22][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S7[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S8 (depth = 1) [BlockSAD[list][refindex][5][pos]=add(BlockSAD[list][refindex][7][pos],BlockSAD[list][refindex][8][pos])]
	 *       ([BlockSAD[list][refindex][5][pos]]) = f([BlockSAD[list][refindex][7][pos], BlockSAD[list][refindex][8][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S8[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S9 (depth = 1) [BlockSAD[list][refindex][7][pos]=add(BlockSAD[list][refindex][9][pos],BlockSAD[list][refindex][10][pos])]
	 *       ([BlockSAD[list][refindex][7][pos]]) = f([BlockSAD[list][refindex][9][pos], BlockSAD[list][refindex][10][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S9[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S10 (depth = 1) [BlockSAD[list][refindex][9][pos]=add(BlockSAD[list][refindex][11][pos],BlockSAD[list][refindex][10][pos])]
	 *       ([BlockSAD[list][refindex][9][pos]]) = f([BlockSAD[list][refindex][11][pos], BlockSAD[list][refindex][10][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S10[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S11 (depth = 1) [BlockSAD[list][refindex][11][pos]=add(BlockSAD[list][refindex][13][pos],BlockSAD[list][refindex][12][pos])]
	 *       ([BlockSAD[list][refindex][11][pos]]) = f([BlockSAD[list][refindex][13][pos], BlockSAD[list][refindex][12][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S11[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S12 (depth = 1) [BlockSAD[list][refindex][13][pos]=add(BlockSAD[list][refindex][15][pos],BlockSAD[list][refindex][14][pos])]
	 *       ([BlockSAD[list][refindex][13][pos]]) = f([BlockSAD[list][refindex][15][pos], BlockSAD[list][refindex][14][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S12[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S13 (depth = 1) [BlockSAD[list][refindex][15][pos]=add(BlockSAD[list][refindex][17][pos],BlockSAD[list][refindex][16][pos])]
	 *       ([BlockSAD[list][refindex][15][pos]]) = f([BlockSAD[list][refindex][17][pos], BlockSAD[list][refindex][16][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S13[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S14 (depth = 1) [BlockSAD[list][refindex][17][pos]=add(BlockSAD[list][refindex][19][pos],BlockSAD[list][refindex][18][pos])]
	 *       ([BlockSAD[list][refindex][17][pos]]) = f([BlockSAD[list][refindex][19][pos], BlockSAD[list][refindex][18][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S14[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S15 (depth = 1) [BlockSAD[list][refindex][4][pos]=add(BlockSAD[list][refindex][6][pos],BlockSAD[list][refindex][7][pos])]
	 *       ([BlockSAD[list][refindex][4][pos]]) = f([BlockSAD[list][refindex][6][pos], BlockSAD[list][refindex][7][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S15[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S16 (depth = 1) [BlockSAD[list][refindex][6][pos]=add(BlockSAD[list][refindex][8][pos],BlockSAD[list][refindex][9][pos])]
	 *       ([BlockSAD[list][refindex][6][pos]]) = f([BlockSAD[list][refindex][8][pos], BlockSAD[list][refindex][9][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S16[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S17 (depth = 1) [BlockSAD[list][refindex][12][pos]=add(BlockSAD[list][refindex][14][pos],BlockSAD[list][refindex][15][pos])]
	 *       ([BlockSAD[list][refindex][12][pos]]) = f([BlockSAD[list][refindex][14][pos], BlockSAD[list][refindex][15][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S17[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S18 (depth = 1) [BlockSAD[list][refindex][14][pos]=add(BlockSAD[list][refindex][16][pos],BlockSAD[list][refindex][17][pos])]
	 *       ([BlockSAD[list][refindex][14][pos]]) = f([BlockSAD[list][refindex][16][pos], BlockSAD[list][refindex][17][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S18[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S19 (depth = 1) [BlockSAD[list][refindex][3][pos]=add(BlockSAD[list][refindex][4][pos],BlockSAD[list][refindex][12][pos])]
	 *       ([BlockSAD[list][refindex][3][pos]]) = f([BlockSAD[list][refindex][4][pos], BlockSAD[list][refindex][12][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S19[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S20 (depth = 1) [BlockSAD[list][refindex][5][pos]=add(BlockSAD[list][refindex][6][pos],BlockSAD[list][refindex][14][pos])]
	 *       ([BlockSAD[list][refindex][5][pos]]) = f([BlockSAD[list][refindex][6][pos], BlockSAD[list][refindex][14][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S20[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S21 (depth = 1) [BlockSAD[list][refindex][2][pos]=add(BlockSAD[list][refindex][4][pos],BlockSAD[list][refindex][6][pos])]
	 *       ([BlockSAD[list][refindex][2][pos]]) = f([BlockSAD[list][refindex][4][pos], BlockSAD[list][refindex][6][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S21[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S22 (depth = 1) [BlockSAD[list][refindex][10][pos]=add(BlockSAD[list][refindex][12][pos],BlockSAD[list][refindex][14][pos])]
	 *       ([BlockSAD[list][refindex][10][pos]]) = f([BlockSAD[list][refindex][12][pos], BlockSAD[list][refindex][14][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S22[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 *     for : 0 <= pos <= max_pos-1 (stride = 1)
	 *       S23 (depth = 1) [BlockSAD[list][refindex][1][pos]=add(BlockSAD[list][refindex][3][pos],BlockSAD[list][refindex][5][pos])]
	 *       ([BlockSAD[list][refindex][1][pos]]) = f([BlockSAD[list][refindex][3][pos], BlockSAD[list][refindex][5][pos]])
	 *       (Domain = [max_pos,list,refindex] -> {S23[pos] : (pos >= 0) and (-pos+max_pos-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_SetupLargerBlocks_GScopRoot_SetupLargerBlocks_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_SetupLargerBlocks_GScopRoot_SetupLargerBlocks_scop_0_";
		// SCoP Extraction Data
		String domains = "[max_pos, list, refindex] -> { S7[pos] : pos >= 0 and pos <= -1 + max_pos; S15[pos] : pos >= 0 and pos <= -1 + max_pos; S21[pos] : pos >= 0 and pos <= -1 + max_pos; S17[pos] : pos >= 0 and pos <= -1 + max_pos; S22[pos] : pos >= 0 and pos <= -1 + max_pos; S19[pos] : pos >= 0 and pos <= -1 + max_pos; S18[pos] : pos >= 0 and pos <= -1 + max_pos; S11[pos] : pos >= 0 and pos <= -1 + max_pos; S8[pos] : pos >= 0 and pos <= -1 + max_pos; S9[pos] : pos >= 0 and pos <= -1 + max_pos; S3[pos] : pos >= 0 and pos <= -1 + max_pos; S20[pos] : pos >= 0 and pos <= -1 + max_pos; S2[pos] : pos >= 0 and pos <= -1 + max_pos; S0[pos] : pos >= 0 and pos <= -1 + max_pos; S5[pos] : pos >= 0 and pos <= -1 + max_pos; S6[pos] : pos >= 0 and pos <= -1 + max_pos; S10[pos] : pos >= 0 and pos <= -1 + max_pos; S12[pos] : pos >= 0 and pos <= -1 + max_pos; S1[pos] : pos >= 0 and pos <= -1 + max_pos; S13[pos] : pos >= 0 and pos <= -1 + max_pos; S14[pos] : pos >= 0 and pos <= -1 + max_pos; S16[pos] : pos >= 0 and pos <= -1 + max_pos; S23[pos] : pos >= 0 and pos <= -1 + max_pos; S4[pos] : pos >= 0 and pos <= -1 + max_pos }";
		String idSchedules = "[max_pos, list, refindex] -> { S15[pos] -> [15, pos, 0]; S16[pos] -> [16, pos, 0]; S17[pos] -> [17, pos, 0]; S20[pos] -> [20, pos, 0]; S21[pos] -> [21, pos, 0]; S11[pos] -> [11, pos, 0]; S2[pos] -> [2, pos, 0]; S9[pos] -> [9, pos, 0]; S23[pos] -> [23, pos, 0]; S0[pos] -> [0, pos, 0]; S19[pos] -> [19, pos, 0]; S5[pos] -> [5, pos, 0]; S22[pos] -> [22, pos, 0]; S12[pos] -> [12, pos, 0]; S3[pos] -> [3, pos, 0]; S18[pos] -> [18, pos, 0]; S14[pos] -> [14, pos, 0]; S7[pos] -> [7, pos, 0]; S6[pos] -> [6, pos, 0]; S13[pos] -> [13, pos, 0]; S8[pos] -> [8, pos, 0]; S1[pos] -> [1, pos, 0]; S4[pos] -> [4, pos, 0]; S10[pos] -> [10, pos, 0] }";
		String writes = "[max_pos, list, refindex] -> { S22[pos] -> BlockSAD[list, refindex, 10, pos]; S1[pos] -> BlockSAD[list, refindex, 7, pos]; S23[pos] -> BlockSAD[list, refindex, 1, pos]; S4[pos] -> BlockSAD[list, refindex, 14, pos]; S15[pos] -> BlockSAD[list, refindex, 4, pos]; S7[pos] -> BlockSAD[list, refindex, 17, pos]; S16[pos] -> BlockSAD[list, refindex, 6, pos]; S9[pos] -> BlockSAD[list, refindex, 7, pos]; S19[pos] -> BlockSAD[list, refindex, 3, pos]; S2[pos] -> BlockSAD[list, refindex, 8, pos]; S8[pos] -> BlockSAD[list, refindex, 5, pos]; S20[pos] -> BlockSAD[list, refindex, 5, pos]; S17[pos] -> BlockSAD[list, refindex, 12, pos]; S12[pos] -> BlockSAD[list, refindex, 13, pos]; S6[pos] -> BlockSAD[list, refindex, 16, pos]; S0[pos] -> BlockSAD[list, refindex, 6, pos]; S3[pos] -> BlockSAD[list, refindex, 9, pos]; S5[pos] -> BlockSAD[list, refindex, 15, pos]; S14[pos] -> BlockSAD[list, refindex, 17, pos]; S21[pos] -> BlockSAD[list, refindex, 2, pos]; S13[pos] -> BlockSAD[list, refindex, 15, pos]; S18[pos] -> BlockSAD[list, refindex, 14, pos]; S10[pos] -> BlockSAD[list, refindex, 9, pos]; S11[pos] -> BlockSAD[list, refindex, 11, pos] }";
		String reads = "[max_pos, list, refindex] -> { S22[pos] -> BlockSAD[list, refindex, 14, pos]; S22[pos] -> BlockSAD[list, refindex, 12, pos]; S1[pos] -> BlockSAD[list, refindex, 12, pos]; S1[pos] -> BlockSAD[list, refindex, 8, pos]; S23[pos] -> BlockSAD[list, refindex, 5, pos]; S23[pos] -> BlockSAD[list, refindex, 3, pos]; S4[pos] -> BlockSAD[list, refindex, 19, pos]; S4[pos] -> BlockSAD[list, refindex, 15, pos]; S15[pos] -> BlockSAD[list, refindex, 7, pos]; S15[pos] -> BlockSAD[list, refindex, 6, pos]; S7[pos] -> BlockSAD[list, refindex, 22, pos]; S7[pos] -> BlockSAD[list, refindex, 18, pos]; S16[pos] -> BlockSAD[list, refindex, 9, pos]; S16[pos] -> BlockSAD[list, refindex, 8, pos]; S9[pos] -> BlockSAD[list, refindex, 10, pos]; S9[pos] -> BlockSAD[list, refindex, 9, pos]; S19[pos] -> BlockSAD[list, refindex, 12, pos]; S19[pos] -> BlockSAD[list, refindex, 4, pos]; S2[pos] -> BlockSAD[list, refindex, 13, pos]; S2[pos] -> BlockSAD[list, refindex, 9, pos]; S8[pos] -> BlockSAD[list, refindex, 8, pos]; S8[pos] -> BlockSAD[list, refindex, 7, pos]; S20[pos] -> BlockSAD[list, refindex, 14, pos]; S20[pos] -> BlockSAD[list, refindex, 6, pos]; S17[pos] -> BlockSAD[list, refindex, 15, pos]; S17[pos] -> BlockSAD[list, refindex, 14, pos]; S12[pos] -> BlockSAD[list, refindex, 15, pos]; S12[pos] -> BlockSAD[list, refindex, 14, pos]; S6[pos] -> BlockSAD[list, refindex, 21, pos]; S6[pos] -> BlockSAD[list, refindex, 17, pos]; S0[pos] -> BlockSAD[list, refindex, 11, pos]; S0[pos] -> BlockSAD[list, refindex, 7, pos]; S3[pos] -> BlockSAD[list, refindex, 14, pos]; S3[pos] -> BlockSAD[list, refindex, 10, pos]; S5[pos] -> BlockSAD[list, refindex, 20, pos]; S5[pos] -> BlockSAD[list, refindex, 16, pos]; S14[pos] -> BlockSAD[list, refindex, 19, pos]; S14[pos] -> BlockSAD[list, refindex, 18, pos]; S21[pos] -> BlockSAD[list, refindex, 6, pos]; S21[pos] -> BlockSAD[list, refindex, 4, pos]; S13[pos] -> BlockSAD[list, refindex, 17, pos]; S13[pos] -> BlockSAD[list, refindex, 16, pos]; S18[pos] -> BlockSAD[list, refindex, 17, pos]; S18[pos] -> BlockSAD[list, refindex, 16, pos]; S10[pos] -> BlockSAD[list, refindex, 11, pos]; S10[pos] -> BlockSAD[list, refindex, 10, pos]; S11[pos] -> BlockSAD[list, refindex, 13, pos]; S11[pos] -> BlockSAD[list, refindex, 12, pos] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[max_pos, list, refindex] -> { S16[pos] -> S2[pos] : pos >= 0 and pos <= -1 + max_pos; S20[pos] -> S16[pos] : pos >= 0 and pos <= -1 + max_pos; S12[pos] -> S5[pos] : pos >= 0 and pos <= -1 + max_pos; S21[pos] -> S16[pos] : pos >= 0 and pos <= -1 + max_pos; S23[pos] -> S20[pos] : pos >= 0 and pos <= -1 + max_pos; S12[pos] -> S4[pos] : pos >= 0 and pos <= -1 + max_pos; S8[pos] -> S2[pos] : pos >= 0 and pos <= -1 + max_pos; S22[pos] -> S17[pos] : pos >= 0 and pos <= -1 + max_pos; S18[pos] -> S14[pos] : pos >= 0 and pos <= -1 + max_pos; S21[pos] -> S15[pos] : pos >= 0 and pos <= -1 + max_pos; S13[pos] -> S7[pos] : pos >= 0 and pos <= -1 + max_pos; S8[pos] -> S1[pos] : pos >= 0 and pos <= -1 + max_pos; S18[pos] -> S6[pos] : pos >= 0 and pos <= -1 + max_pos; S17[pos] -> S4[pos] : pos >= 0 and pos <= -1 + max_pos; S19[pos] -> S17[pos] : pos >= 0 and pos <= -1 + max_pos; S20[pos] -> S18[pos] : pos >= 0 and pos <= -1 + max_pos; S9[pos] -> S3[pos] : pos >= 0 and pos <= -1 + max_pos; S17[pos] -> S13[pos] : pos >= 0 and pos <= -1 + max_pos; S15[pos] -> S0[pos] : pos >= 0 and pos <= -1 + max_pos; S15[pos] -> S9[pos] : pos >= 0 and pos <= -1 + max_pos; S22[pos] -> S18[pos] : pos >= 0 and pos <= -1 + max_pos; S13[pos] -> S6[pos] : pos >= 0 and pos <= -1 + max_pos; S23[pos] -> S19[pos] : pos >= 0 and pos <= -1 + max_pos; S19[pos] -> S15[pos] : pos >= 0 and pos <= -1 + max_pos; S16[pos] -> S10[pos] : pos >= 0 and pos <= -1 + max_pos }";
		String valueBasedPlutoSchedule = "[max_pos, list, refindex] -> { S7[pos] -> [pos, 8]; S17[pos] -> [pos, 10]; S19[pos] -> [pos, 15]; S0[pos] -> [pos, 11]; S10[pos] -> [pos, 1]; S14[pos] -> [pos, 4]; S20[pos] -> [pos, 7]; S16[pos] -> [pos, 3]; S12[pos] -> [pos, 19]; S5[pos] -> [pos, 18]; S23[pos] -> [pos, 16]; S1[pos] -> [pos, 17]; S13[pos] -> [pos, 9]; S11[pos] -> [pos, 0]; S3[pos] -> [pos, 12]; S2[pos] -> [pos, 2]; S4[pos] -> [pos, 0]; S22[pos] -> [pos, 21]; S8[pos] -> [pos, 20]; S9[pos] -> [pos, 13]; S15[pos] -> [pos, 14]; S18[pos] -> [pos, 6]; S21[pos] -> [pos, 22]; S6[pos] -> [pos, 5] }";
		String valueBasedFeautrierSchedule = "[max_pos, list, refindex] -> { S1[pos] -> [pos]; S16[pos] -> [pos]; S19[pos] -> [pos]; S10[pos] -> [pos]; S3[pos] -> [pos]; S5[pos] -> [pos]; S4[pos] -> [pos]; S17[pos] -> [pos]; S14[pos] -> [pos]; S21[pos] -> [pos]; S23[pos] -> [pos]; S7[pos] -> [pos]; S2[pos] -> [pos]; S9[pos] -> [pos]; S15[pos] -> [pos]; S11[pos] -> [pos]; S0[pos] -> [pos]; S22[pos] -> [pos]; S18[pos] -> [pos]; S8[pos] -> [pos]; S12[pos] -> [pos]; S20[pos] -> [pos]; S13[pos] -> [pos]; S6[pos] -> [pos] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[max_pos, list, refindex] -> { S17[pos] -> S13[pos] : pos >= 0 and pos <= -1 + max_pos; S23[pos] -> S19[pos] : pos >= 0 and pos <= -1 + max_pos; S19[pos] -> S15[pos] : pos >= 0 and pos <= -1 + max_pos; S16[pos] -> S15[pos] : pos >= 0 and pos <= -1 + max_pos; S22[pos] -> S17[pos] : pos >= 0 and pos <= -1 + max_pos; S21[pos] -> S16[pos] : pos >= 0 and pos <= -1 + max_pos; S18[pos] -> S17[pos] : pos >= 0 and pos <= -1 + max_pos; S8[pos] -> S1[pos] : pos >= 0 and pos <= -1 + max_pos; S12[pos] -> S5[pos] : pos >= 0 and pos <= -1 + max_pos; S8[pos] -> S2[pos] : pos >= 0 and pos <= -1 + max_pos; S13[pos] -> S5[pos] : pos >= 0 and pos <= -1 + max_pos; S13[pos] -> S7[pos] : pos >= 0 and pos <= -1 + max_pos; S10[pos] -> S9[pos] : pos >= 0 and pos <= -1 + max_pos; S15[pos] -> S9[pos] : pos >= 0 and pos <= -1 + max_pos; S1[pos] -> S0[pos] : pos >= 0 and pos <= -1 + max_pos; S17[pos] -> S11[pos] : pos >= 0 and pos <= -1 + max_pos; S20[pos] -> S16[pos] : pos >= 0 and pos <= -1 + max_pos; S18[pos] -> S6[pos] : pos >= 0 and pos <= -1 + max_pos; S15[pos] -> S0[pos] : pos >= 0 and pos <= -1 + max_pos; S18[pos] -> S4[pos] : pos >= 0 and pos <= -1 + max_pos; S16[pos] -> S0[pos] : pos >= 0 and pos <= -1 + max_pos; S12[pos] -> S4[pos] : pos >= 0 and pos <= -1 + max_pos; S10[pos] -> S3[pos] : pos >= 0 and pos <= -1 + max_pos; S12[pos] -> S2[pos] : pos >= 0 and pos <= -1 + max_pos; S7[pos] -> S6[pos] : pos >= 0 and pos <= -1 + max_pos; S18[pos] -> S12[pos] : pos >= 0 and pos <= -1 + max_pos; S16[pos] -> S10[pos] : pos >= 0 and pos <= -1 + max_pos; S18[pos] -> S14[pos] : pos >= 0 and pos <= -1 + max_pos; S21[pos] -> S15[pos] : pos >= 0 and pos <= -1 + max_pos; S4[pos] -> S3[pos] : pos >= 0 and pos <= -1 + max_pos; S11[pos] -> S10[pos] : pos >= 0 and pos <= -1 + max_pos; S22[pos] -> S9[pos] : pos >= 0 and pos <= -1 + max_pos; S9[pos] -> S8[pos] : pos >= 0 and pos <= -1 + max_pos; S13[pos] -> S12[pos] : pos >= 0 and pos <= -1 + max_pos; S20[pos] -> S8[pos] : pos >= 0 and pos <= -1 + max_pos; S14[pos] -> S13[pos] : pos >= 0 and pos <= -1 + max_pos; S13[pos] -> S6[pos] : pos >= 0 and pos <= -1 + max_pos; S9[pos] -> S3[pos] : pos >= 0 and pos <= -1 + max_pos; S14[pos] -> S7[pos] : pos >= 0 and pos <= -1 + max_pos; S17[pos] -> S1[pos] : pos >= 0 and pos <= -1 + max_pos; S11[pos] -> S0[pos] : pos >= 0 and pos <= -1 + max_pos; S22[pos] -> S10[pos] : pos >= 0 and pos <= -1 + max_pos; S16[pos] -> S2[pos] : pos >= 0 and pos <= -1 + max_pos; S17[pos] -> S4[pos] : pos >= 0 and pos <= -1 + max_pos; S5[pos] -> S4[pos] : pos >= 0 and pos <= -1 + max_pos; S19[pos] -> S17[pos] : pos >= 0 and pos <= -1 + max_pos; S23[pos] -> S20[pos] : pos >= 0 and pos <= -1 + max_pos; S12[pos] -> S11[pos] : pos >= 0 and pos <= -1 + max_pos; S9[pos] -> S1[pos] : pos >= 0 and pos <= -1 + max_pos; S6[pos] -> S5[pos] : pos >= 0 and pos <= -1 + max_pos; S22[pos] -> S18[pos] : pos >= 0 and pos <= -1 + max_pos; S3[pos] -> S2[pos] : pos >= 0 and pos <= -1 + max_pos; S20[pos] -> S18[pos] : pos >= 0 and pos <= -1 + max_pos; S2[pos] -> S1[pos] : pos >= 0 and pos <= -1 + max_pos; S22[pos] -> S3[pos] : pos >= 0 and pos <= -1 + max_pos }";
		String memoryBasedPlutoSchedule = "[max_pos, list, refindex] -> { S7[pos] -> [pos, 14]; S17[pos] -> [pos, 17]; S19[pos] -> [pos, 20]; S0[pos] -> [pos, 0]; S10[pos] -> [pos, 7]; S14[pos] -> [pos, 16]; S20[pos] -> [pos, 19]; S16[pos] -> [pos, 9]; S12[pos] -> [pos, 12]; S5[pos] -> [pos, 10]; S23[pos] -> [pos, 21]; S1[pos] -> [pos, 1]; S13[pos] -> [pos, 15]; S11[pos] -> [pos, 11]; S3[pos] -> [pos, 3]; S2[pos] -> [pos, 2]; S4[pos] -> [pos, 4]; S22[pos] -> [pos, 22]; S8[pos] -> [pos, 5]; S9[pos] -> [pos, 6]; S15[pos] -> [pos, 8]; S18[pos] -> [pos, 18]; S21[pos] -> [pos, 23]; S6[pos] -> [pos, 13] }";
		String memoryBasedFeautrierSchedule = "[max_pos, list, refindex] -> { S1[pos] -> [pos]; S16[pos] -> [pos]; S19[pos] -> [pos]; S10[pos] -> [pos]; S3[pos] -> [pos]; S5[pos] -> [pos]; S4[pos] -> [pos]; S17[pos] -> [pos]; S14[pos] -> [pos]; S21[pos] -> [pos]; S23[pos] -> [pos]; S7[pos] -> [pos]; S2[pos] -> [pos]; S9[pos] -> [pos]; S15[pos] -> [pos]; S11[pos] -> [pos]; S0[pos] -> [pos]; S22[pos] -> [pos]; S18[pos] -> [pos]; S8[pos] -> [pos]; S12[pos] -> [pos]; S20[pos] -> [pos]; S13[pos] -> [pos]; S6[pos] -> [pos] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #29
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : main_scop_new
	 *   block : 
	 *     S0 (depth = 0) [i=IntExpr(0)]
	 *     ([i]) = f([])
	 *     (Domain = [] -> {S0[] : })
	 *     S1 (depth = 0) [i=add(i,IntExpr(1))]
	 *     ([i]) = f([i])
	 *     (Domain = [] -> {S1[] : })
	 *     S2 (depth = 0) [i=sub(i,IntExpr(1))]
	 *     ([i]) = f([i])
	 *     (Domain = [] -> {S2[] : })
	 *     S3 (depth = 0) [i=sub(sub(add(add(i,i),i),i),i)]
	 *     ([i]) = f([i, i, i, i, i])
	 *     (Domain = [] -> {S3[] : })
	 *     S4 (depth = 0) [i=add(i,IntExpr(1))]
	 *     ([i]) = f([i])
	 *     (Domain = [] -> {S4[] : })
	 *     S5 (depth = 0) [i=add(i,IntExpr(1))]
	 *     ([i]) = f([i])
	 *     (Domain = [] -> {S5[] : })
	 *     S6 (depth = 0) [i=sub(i,IntExpr(1))]
	 *     ([i]) = f([i])
	 *     (Domain = [] -> {S6[] : })
	 *     for : 0 <= i <= 9 (stride = 1)
	 *       S7 (depth = 1) [a=IntExpr(0)]
	 *       ([a]) = f([])
	 *       (Domain = [] -> {S7[i] : (i >= 0) and (-i+9 >= 0)})
	 *     for : 0 <= i <= 9 (stride = 1)
	 *       S8 (depth = 1) [a=IntExpr(0)]
	 *       ([a]) = f([])
	 *       (Domain = [] -> {S8[i] : (i >= 0) and (-i+9 >= 0)})
	 *     for : 1 <= i <= 10 (stride = -1)
	 *       S9 (depth = 1) [a=IntExpr(0)]
	 *       ([a]) = f([])
	 *       (Domain = [] -> {S9[i] : (i-1 >= 0) and (-i+10 >= 0)})
	 *     for : 1 <= i <= 10 (stride = -1)
	 *       S10 (depth = 1) [a=IntExpr(0)]
	 *       ([a]) = f([])
	 *       (Domain = [] -> {S10[i] : (i-1 >= 0) and (-i+10 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_simple_operator_GScopRoot_scop_new_ () {
		String path = "basics_simple_operator_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "{ S2[]; S6[]; S1[]; S7[i] : i >= 0 and i <= 9; S10[i] : i >= 1 and i <= 10; S3[]; S5[]; S8[i] : i >= 0 and i <= 9; S9[i] : i >= 1 and i <= 10; S4[]; S0[] }";
		String idSchedules = "{ S1[] -> [1, 0, 0]; S4[] -> [4, 0, 0]; S6[] -> [6, 0, 0]; S0[] -> [0, 0, 0]; S10[i] -> [10, -i, 0]; S9[i] -> [9, -i, 0]; S7[i] -> [7, i, 0]; S2[] -> [2, 0, 0]; S5[] -> [5, 0, 0]; S8[i] -> [8, i, 0]; S3[] -> [3, 0, 0] }";
		String writes = "{ S3[] -> i[]; S6[] -> i[]; S7[i] -> a[]; S9[i] -> a[]; S5[] -> i[]; S10[i] -> a[]; S2[] -> i[]; S1[] -> i[]; S4[] -> i[]; S8[i] -> a[]; S0[] -> i[] }";
		String reads = "{ S3[] -> i[]; S6[] -> i[]; S5[] -> i[]; S2[] -> i[]; S1[] -> i[]; S4[] -> i[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S2[] -> S1[]; S3[] -> S2[]; S5[] -> S4[]; S1[] -> S0[]; S6[] -> S5[]; S4[] -> S3[] }";
		String valueBasedPlutoSchedule = "{ S8[i] -> [i]; S2[] -> [2]; S0[] -> [0]; S10[i] -> [i]; S5[] -> [5]; S9[i] -> [i]; S4[] -> [4]; S6[] -> [6]; S3[] -> [3]; S7[i] -> [i]; S1[] -> [1] }";
		String valueBasedFeautrierSchedule = "{ S8[i] -> [i]; S2[] -> [2]; S0[] -> [0]; S10[i] -> [i]; S5[] -> [5]; S9[i] -> [i]; S4[] -> [4]; S6[] -> [6]; S3[] -> [3]; S7[i] -> [i]; S1[] -> [1] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S7[i] -> S7[-1 + i] : i >= 1 and i <= 9; S8[0] -> S7[9]; S8[i] -> S8[-1 + i] : i >= 1 and i <= 9; S1[] -> S0[]; S2[] -> S1[]; S10[i] -> S10[1 + i] : i >= 1 and i <= 9; S3[] -> S2[]; S6[] -> S5[]; S4[] -> S3[]; S9[i] -> S9[1 + i] : i >= 1 and i <= 9; S9[10] -> S8[9]; S5[] -> S4[]; S10[10] -> S9[1] }";
		String memoryBasedPlutoSchedule = "{ S1[] -> [1, 0]; S2[] -> [2, 0]; S3[] -> [3, 0]; S5[] -> [5, 0]; S9[i] -> [28 - i, 2]; S8[i] -> [9 + i, 1]; S7[i] -> [i, 0]; S10[i] -> [37 - i, 3]; S0[] -> [0, 0]; S4[] -> [4, 0]; S6[] -> [6, 0] }";
		String memoryBasedFeautrierSchedule = "{ S8[i] -> [10 + i]; S2[] -> [2]; S0[] -> [0]; S10[i] -> [40 - i]; S5[] -> [5]; S9[i] -> [30 - i]; S4[] -> [4]; S6[] -> [6]; S3[] -> [3]; S7[i] -> [i]; S1[] -> [1] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #30
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_atax_scop_new
	 *   block : 
	 *     for : 0 <= i <= ny-1 (stride = 1)
	 *       S0 (depth = 1) [y[i]=IntExpr(0)]
	 *       ([y[i]]) = f([])
	 *       (Domain = [ny,nx] -> {S0[i] : (i >= 0) and (-i+ny-1 >= 0)})
	 *     for : 0 <= i <= nx-1 (stride = 1)
	 *       block : 
	 *         S1 (depth = 1) [tmp[i]=IntExpr(0)]
	 *         ([tmp[i]]) = f([])
	 *         (Domain = [ny,nx] -> {S1[i] : (i >= 0) and (-i+nx-1 >= 0)})
	 *         for : 0 <= j <= ny-1 (stride = 1)
	 *           S2 (depth = 2) [tmp[i]=add(tmp[i],mul(A[i][j],x[j]))]
	 *           ([tmp[i]]) = f([tmp[i], A[i][j], x[j]])
	 *           (Domain = [ny,nx] -> {S2[i,j] : (j >= 0) and (-j+ny-1 >= 0) and (i >= 0) and (-i+nx-1 >= 0)})
	 *         for : 0 <= j <= ny-1 (stride = 1)
	 *           S3 (depth = 2) [y[j]=add(y[j],mul(A[i][j],tmp[i]))]
	 *           ([y[j]]) = f([y[j], A[i][j], tmp[i]])
	 *           (Domain = [ny,nx] -> {S3[i,j] : (j >= 0) and (-j+ny-1 >= 0) and (i >= 0) and (-i+nx-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_atax_atax_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_linear_algebra_kernels_atax_atax_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[ny, nx] -> { S3[i, j] : j >= 0 and j <= -1 + ny and i >= 0 and i <= -1 + nx; S0[i] : i >= 0 and i <= -1 + ny; S2[i, j] : j >= 0 and j <= -1 + ny and i >= 0 and i <= -1 + nx; S1[i] : i >= 0 and i <= -1 + nx }";
		String idSchedules = "[ny, nx] -> { S1[i] -> [1, i, 0, 0, 0]; S0[i] -> [0, i, 0, 0, 0]; S2[i, j] -> [1, i, 1, j, 0]; S3[i, j] -> [1, i, 2, j, 0] }";
		String writes = "[ny, nx] -> { S3[i, j] -> y[j]; S0[i] -> y[i]; S1[i] -> tmp[i]; S2[i, j] -> tmp[i] }";
		String reads = "[ny, nx] -> { S3[i, j] -> y[j]; S3[i, j] -> A[i, j]; S2[i, j] -> A[i, j]; S3[i, j] -> tmp[i]; S2[i, j] -> tmp[i]; S2[i, j] -> x[j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[ny, nx] -> { S3[0, j] -> S0[j] : j >= 0 and j <= -1 + ny and nx >= 1; S2[i, j] -> S2[i, -1 + j] : j >= 1 and j <= -1 + ny and i >= 0 and i <= -1 + nx; S2[i, 0] -> S1[i] : i <= -1 + nx and ny >= 1 and i >= 0; S3[i, j] -> S3[-1 + i, j] : j >= 0 and j <= -1 + ny and i >= 1 and i <= -1 + nx; S3[i, j] -> S2[i, -1 + ny] : j >= 0 and j <= -1 + ny and i >= 0 and i <= -1 + nx }";
		String valueBasedPlutoSchedule = "[ny, nx] -> { S2[i, j] -> [i, i + j, 1]; S3[i, j] -> [i, ny + i + j, 3]; S0[i] -> [0, i, 2]; S1[i] -> [0, i, 0] }";
		String valueBasedFeautrierSchedule = "[ny, nx] -> { S3[i, j] -> [1 + ny + i, j]; S1[i] -> [0, i]; S0[i] -> [0, i]; S2[i, j] -> [1 + j, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[ny, nx] -> { S3[0, j] -> S0[j] : nx >= 1 and j >= 0 and j <= -1 + ny; S2[i, j] -> S2[i, -1 + j] : i >= 0 and i <= -1 + nx and j >= 1 and j <= -1 + ny; S2[i, 0] -> S1[i] : ny >= 1 and i >= 0 and i <= -1 + nx; S3[i, j] -> S3[-1 + i, j] : i >= 1 and i <= -1 + nx and j >= 0 and j <= -1 + ny; S3[i, j] -> S2[i, -1 + ny] : j >= 0 and j <= -1 + ny and i >= 0 and i <= -1 + nx }";
		String memoryBasedPlutoSchedule = "[ny, nx] -> { S2[i, j] -> [i, i + j, 1]; S3[i, j] -> [i, ny + i + j, 3]; S0[i] -> [0, i, 2]; S1[i] -> [0, i, 0] }";
		String memoryBasedFeautrierSchedule = "[ny, nx] -> { S3[i, j] -> [1 + ny + i, j]; S1[i] -> [0, i]; S0[i] -> [0, i]; S2[i, j] -> [1 + j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #31
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_bicg_scop_new
	 *   block : 
	 *     for : 0 <= i <= ny-1 (stride = 1)
	 *       S0 (depth = 1) [s[i]=IntExpr(0)]
	 *       ([s[i]]) = f([])
	 *       (Domain = [ny,nx] -> {S0[i] : (i >= 0) and (-i+ny-1 >= 0)})
	 *     for : 0 <= i <= nx-1 (stride = 1)
	 *       block : 
	 *         S1 (depth = 1) [q[i]=IntExpr(0)]
	 *         ([q[i]]) = f([])
	 *         (Domain = [ny,nx] -> {S1[i] : (i >= 0) and (-i+nx-1 >= 0)})
	 *         for : 0 <= j <= ny-1 (stride = 1)
	 *           block : 
	 *             S2 (depth = 2) [s[j]=add(s[j],mul(r[i],A[i][j]))]
	 *             ([s[j]]) = f([s[j], r[i], A[i][j]])
	 *             (Domain = [ny,nx] -> {S2[i,j] : (j >= 0) and (-j+ny-1 >= 0) and (i >= 0) and (-i+nx-1 >= 0)})
	 *             S3 (depth = 2) [q[i]=add(q[i],mul(A[i][j],p[j]))]
	 *             ([q[i]]) = f([q[i], A[i][j], p[j]])
	 *             (Domain = [ny,nx] -> {S3[i,j] : (j >= 0) and (-j+ny-1 >= 0) and (i >= 0) and (-i+nx-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_bicg_bicg_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_linear_algebra_kernels_bicg_bicg_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[ny, nx] -> { S3[i, j] : j >= 0 and j <= -1 + ny and i >= 0 and i <= -1 + nx; S0[i] : i >= 0 and i <= -1 + ny; S2[i, j] : j >= 0 and j <= -1 + ny and i >= 0 and i <= -1 + nx; S1[i] : i >= 0 and i <= -1 + nx }";
		String idSchedules = "[ny, nx] -> { S1[i] -> [1, i, 0, 0, 0]; S0[i] -> [0, i, 0, 0, 0]; S2[i, j] -> [1, i, 1, j, 0]; S3[i, j] -> [1, i, 1, j, 1] }";
		String writes = "[ny, nx] -> { S2[i, j] -> s[j]; S3[i, j] -> q[i]; S1[i] -> q[i]; S0[i] -> s[i] }";
		String reads = "[ny, nx] -> { S2[i, j] -> r[i]; S2[i, j] -> s[j]; S3[i, j] -> q[i]; S3[i, j] -> A[i, j]; S3[i, j] -> p[j]; S2[i, j] -> A[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[ny, nx] -> { S2[0, j] -> S0[j] : j >= 0 and j <= -1 + ny and nx >= 1; S2[i, j] -> S2[-1 + i, j] : j >= 0 and j <= -1 + ny and i >= 1 and i <= -1 + nx; S3[i, j] -> S3[i, -1 + j] : j >= 1 and j <= -1 + ny and i >= 0 and i <= -1 + nx; S3[i, 0] -> S1[i] : i <= -1 + nx and ny >= 1 and i >= 0 }";
		String valueBasedPlutoSchedule = "[ny, nx] -> { S2[i, j] -> [i, j, 1]; S3[i, j] -> [i, i + j, 1]; S0[i] -> [0, i, 0]; S1[i] -> [0, i, 0] }";
		String valueBasedFeautrierSchedule = "[ny, nx] -> { S3[i, j] -> [1 + j, i]; S1[i] -> [0, i]; S0[i] -> [0, i]; S2[i, j] -> [1 + i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[ny, nx] -> { S2[0, j] -> S0[j] : nx >= 1 and j >= 0 and j <= -1 + ny; S2[i, j] -> S2[-1 + i, j] : i >= 1 and i <= -1 + nx and j >= 0 and j <= -1 + ny; S3[i, j] -> S3[i, -1 + j] : i >= 0 and i <= -1 + nx and j >= 1 and j <= -1 + ny; S3[i, 0] -> S1[i] : ny >= 1 and i >= 0 and i <= -1 + nx }";
		String memoryBasedPlutoSchedule = "[ny, nx] -> { S2[i, j] -> [i, j, 1]; S3[i, j] -> [i, i + j, 1]; S0[i] -> [0, i, 0]; S1[i] -> [0, i, 0] }";
		String memoryBasedFeautrierSchedule = "[ny, nx] -> { S3[i, j] -> [1 + j, i]; S1[i] -> [0, i]; S0[i] -> [0, i]; S2[i, j] -> [1 + i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #32
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : fact3_scop_new
	 *   block : 
	 *     S0 (depth = 0) [res=IntExpr(1)]
	 *     ([res]) = f([])
	 *     (Domain = [] -> {S0[] : })
	 *     for : 1 <= i <= 3 (stride = 1)
	 *       S1 (depth = 1) [res=mul(res,IntExpr(i))]
	 *       ([res]) = f([res])
	 *       (Domain = [] -> {S1[i] : (i-1 >= 0) and (-i+3 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_fact_GScopRoot_scop_new_ () {
		String path = "basics_oldFiles_fact_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "{ S1[i] : i >= 1 and i <= 3; S0[] }";
		String idSchedules = "{ S0[] -> [0, 0, 0]; S1[i] -> [1, i, 0] }";
		String writes = "{ S0[] -> res[]; S1[i] -> res[] }";
		String reads = "{ S1[i] -> res[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S1[1] -> S0[]; S1[i] -> S1[-1 + i] : i >= 2 and i <= 3 }";
		String valueBasedPlutoSchedule = "{ S1[i] -> [i, 1]; S0[] -> [0, 0] }";
		String valueBasedFeautrierSchedule = "{ S0[] -> [0]; S1[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S1[1] -> S0[]; S1[i] -> S1[-1 + i] : i <= 3 and i >= 2 }";
		String memoryBasedPlutoSchedule = "{ S1[i] -> [i, 1]; S0[] -> [0, 0] }";
		String memoryBasedFeautrierSchedule = "{ S0[] -> [0]; S1[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #33
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_gemm_kernel_gemm/scop_0
	 *   for : 0 <= i <= ni-1 (stride = 1)
	 *     for : 0 <= j <= nj-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 2) [C[i][j]=mul(C[i][j],IntExpr(beta))]
	 *         ([C[i][j]]) = f([C[i][j]])
	 *         (Domain = [ni,nj,beta,nk,alpha] -> {S0[i,j] : (j >= 0) and (-j+nj-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *         for : 0 <= k <= nk-1 (stride = 1)
	 *           S1 (depth = 3) [C[i][j]=add(C[i][j],mul(mul(IntExpr(alpha),A[i][k]),B[k][j]))]
	 *           ([C[i][j]]) = f([C[i][j], A[i][k], B[k][j]])
	 *           (Domain = [ni,nj,beta,nk,alpha] -> {S1[i,j,k] : (k >= 0) and (-k+nk-1 >= 0) and (j >= 0) and (-j+nj-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_gemm_gemm_GScopRoot_kernel_gemm_scop_0_ () {
		String path = "polybench_gecos_linear_algebra_kernels_gemm_gemm_GScopRoot_kernel_gemm_scop_0_";
		// SCoP Extraction Data
		String domains = "[ni, nj, beta, nk, alpha] -> { S0[i, j] : j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S1[i, j, k] : k >= 0 and k <= -1 + nk and j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni }";
		String idSchedules = "[ni, nj, beta, nk, alpha] -> { S1[i, j, k] -> [0, i, 0, j, 1, k, 0]; S0[i, j] -> [0, i, 0, j, 0, 0, 0] }";
		String writes = "[ni, nj, beta, nk, alpha] -> { S0[i, j] -> C[i, j]; S1[i, j, k] -> C[i, j] }";
		String reads = "[ni, nj, beta, nk, alpha] -> { S0[i, j] -> C[i, j]; S1[i, j, k] -> B[k, j]; S1[i, j, k] -> C[i, j]; S1[i, j, k] -> A[i, k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[ni, nj, beta, nk, alpha] -> { S1[i, j, k] -> S1[i, j, -1 + k] : k >= 1 and k <= -1 + nk and j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S1[i, j, 0] -> S0[i, j] : i <= -1 + ni and nk >= 1 and j >= 0 and j <= -1 + nj and i >= 0 }";
		String valueBasedPlutoSchedule = "[ni, nj, beta, nk, alpha] -> { S0[i, j] -> [0, i, j, 0]; S1[i, j, k] -> [i, i + j, i + j + k, 1] }";
		String valueBasedFeautrierSchedule = "[ni, nj, beta, nk, alpha] -> { S1[i, j, k] -> [1 + k, j, i]; S0[i, j] -> [0, i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[ni, nj, beta, nk, alpha] -> { S1[i, j, k] -> S1[i, j, -1 + k] : i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + nj and k >= 1 and k <= -1 + nk; S1[i, j, 0] -> S0[i, j] : nk >= 1 and i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + nj }";
		String memoryBasedPlutoSchedule = "[ni, nj, beta, nk, alpha] -> { S0[i, j] -> [0, i, j, 0]; S1[i, j, k] -> [i, i + j, i + j + k, 1] }";
		String memoryBasedFeautrierSchedule = "[ni, nj, beta, nk, alpha] -> { S1[i, j, k] -> [1 + k, j, i]; S0[i, j] -> [0, i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #34
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_symm_kernel_symm/scop_0
	 *   for : 0 <= i <= ni-1 (stride = 1)
	 *     for : 0 <= j <= nj-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 2) [acc=IntExpr(0)]
	 *         ([acc]) = f([])
	 *         (Domain = [ni,nj,alpha,beta] -> {S0[i,j] : (j >= 0) and (-j+nj-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *         for : 0 <= k <= j-2 (stride = 1)
	 *           block : 
	 *             S1 (depth = 3) [C[k][j]=add(C[k][j],mul(mul(IntExpr(alpha),A[k][i]),B[i][j]))]
	 *             ([C[k][j]]) = f([C[k][j], A[k][i], B[i][j]])
	 *             (Domain = [ni,nj,alpha,beta] -> {S1[i,j,k] : (k >= 0) and (j-k-2 >= 0) and (j >= 0) and (-j+nj-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *             S2 (depth = 3) [acc=add(acc,mul(B[k][j],A[k][i]))]
	 *             ([acc]) = f([acc, B[k][j], A[k][i]])
	 *             (Domain = [ni,nj,alpha,beta] -> {S2[i,j,k] : (k >= 0) and (j-k-2 >= 0) and (j >= 0) and (-j+nj-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *         S3 (depth = 2) [C[i][j]=add(add(mul(IntExpr(beta),C[i][j]),mul(mul(IntExpr(alpha),A[i][i]),B[i][j])),mul(IntExpr(alpha),acc))]
	 *         ([C[i][j]]) = f([C[i][j], A[i][i], B[i][j], acc])
	 *         (Domain = [ni,nj,alpha,beta] -> {S3[i,j] : (j >= 0) and (-j+nj-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_symm_symm_GScopRoot_kernel_symm_scop_0_ () {
		String path = "polybench_gecos_linear_algebra_kernels_symm_symm_GScopRoot_kernel_symm_scop_0_";
		// SCoP Extraction Data
		String domains = "[ni, nj, alpha, beta] -> { S0[i, j] : j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S3[i, j] : j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S1[i, j, k] : k >= 0 and k <= -2 + j and j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S2[i, j, k] : k >= 0 and k <= -2 + j and j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni }";
		String idSchedules = "[ni, nj, alpha, beta] -> { S3[i, j] -> [0, i, 0, j, 2, 0, 0]; S1[i, j, k] -> [0, i, 0, j, 1, k, 0]; S2[i, j, k] -> [0, i, 0, j, 1, k, 1]; S0[i, j] -> [0, i, 0, j, 0, 0, 0] }";
		String writes = "[ni, nj, alpha, beta] -> { S2[i, j, k] -> acc[]; S1[i, j, k] -> C[k, j]; S3[i, j] -> C[i, j]; S0[i, j] -> acc[] }";
		String reads = "[ni, nj, alpha, beta] -> { S2[i, j, k] -> acc[]; S1[i, j, k] -> A[k, i]; S1[i, j, k] -> C[k, j]; S3[i, j] -> acc[]; S2[i, j, k] -> A[k, i]; S3[i, j] -> C[i, j]; S3[i, j] -> B[i, j]; S1[i, j, k] -> B[i, j]; S3[i, j] -> A[i, i]; S2[i, j, k] -> B[k, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[ni, nj, alpha, beta] -> { S2[i, j, 0] -> S0[i, j] : i <= -1 + ni and j >= 2 and i >= 0 and j <= -1 + nj; S3[i, j] -> S2[i, j, -2 + j] : j >= 2 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S1[i, j, k] -> S1[-1 + i, j, k] : (i <= -1 + ni and j <= -1 + nj and k >= 0 and k <= -2 + j and k <= -2 + i) or (i >= 1 and i <= -1 + ni and j <= -1 + nj and k >= i and k <= -2 + j); S2[i, j, k] -> S2[i, j, -1 + k] : i <= -1 + ni and k <= -2 + j and k >= 1 and j <= -1 + nj and i >= 0 and j >= 0; S1[i, j, -1 + i] -> S3[-1 + i, j] : i >= 1 and j >= 1 + i and j >= 0 and j <= -1 + nj and i <= -1 + ni; S3[i, j] -> S0[i, j] : j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni and j <= 1; S3[i, j] -> S1[i, j, i] : j >= 2 + i and j <= -1 + nj and i >= 0 and i <= -1 + ni and j >= 0 }";
		String valueBasedPlutoSchedule = "[ni, nj, alpha, beta] -> { S0[i, j] -> [0, i, j, 2]; S2[i, j, k] -> [i, i + j, i + j + k, 0]; S1[i, j, k] -> [i, j + k, 2j + 3k, 1]; S3[i, j] -> [i, i + j, 3i + 2j, 3] }";
		String valueBasedFeautrierSchedule = "[ni, nj, alpha, beta] -> { S0[i, j] -> [0, i, j]; S1[i, j, k] -> [2i + j - 2k, i, k]; S2[i, j, k] -> [1 + k, j, i]; S3[i, j] -> [1 + j, i, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[ni, nj, alpha, beta] -> { S0[i, j] -> S2[i, -1 + j, -3 + j] : j >= 3 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S0[i, 0] -> S2[-1 + i, -1 + nj, -3 + nj] : i <= -1 + ni and i >= 1 and nj >= 3; S0[i, j] -> S0[i, -1 + j] : j >= 1 and j <= -1 + nj and i >= 0 and i <= -1 + ni and j <= 2; S0[i, 0] -> S0[-1 + i, -1 + nj] : i <= -1 + ni and nj >= 1 and nj <= 2 and i >= 1; S0[i, j] -> S3[i, -1 + j] : i >= 0 and i <= -1 + ni and j <= -1 + nj and j >= 1; S0[i, 0] -> S3[-1 + i, -1 + nj] : i >= 1 and i <= -1 + ni and nj >= 1; S2[i, j, 0] -> S0[i, j] : i >= 0 and i <= -1 + ni and j >= 2 and j <= -1 + nj; S3[i, j] -> S2[i, j, -2 + j] : j >= 2 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S1[i, j, k] -> S1[-1 + i, j, k] : (i <= -1 + ni and j <= -1 + nj and k >= 0 and k <= -2 + j and k <= -2 + i) or (i >= 1 and i <= -1 + ni and j <= -1 + nj and k >= i and k <= -2 + j); S2[i, j, k] -> S2[i, j, -1 + k] : i >= 0 and i <= -1 + ni and j <= -1 + nj and k >= 1 and k <= -2 + j; S1[i, j, -1 + i] -> S3[-1 + i, j] : i >= 1 and i <= -1 + ni and j >= 1 + i and j <= -1 + nj; S3[i, j] -> S0[i, j] : j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni and j <= 1; S3[i, j] -> S1[i, j, i] : i >= 0 and i <= -1 + ni and j >= 2 + i and j <= -1 + nj }";
		String memoryBasedPlutoSchedule = "[ni, nj, alpha, beta] -> { S0[i, j] -> [i, j, 0, 0]; S2[i, j, k] -> [i, j, k, 1]; S1[i, j, k] -> [i, j, k, 2]; S3[i, j] -> [i, j, j, 3] }";
		String memoryBasedFeautrierSchedule = "[ni, nj, alpha, beta] -> { S0[i, j] -> [1 + i, j, 0, 0]; S2[i, j, k] -> [1 + i, j, 1, k]; S1[i, j, k] -> [2i - k, j, i, 0]; S3[i, j] -> [1 + i, j, 2, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #35
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_syrk_scop_new
	 *   block : 
	 *     for : 0 <= i <= ni-1 (stride = 1)
	 *       for : 0 <= j <= ni-1 (stride = 1)
	 *         S0 (depth = 2) [C[i][j]=mul(C[i][j],IntExpr(beta))]
	 *         ([C[i][j]]) = f([C[i][j]])
	 *         (Domain = [ni,beta,nj,alpha] -> {S0[i,j] : (j >= 0) and (-j+ni-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *     for : 0 <= i <= ni-1 (stride = 1)
	 *       for : 0 <= j <= ni-1 (stride = 1)
	 *         for : 0 <= k <= nj-1 (stride = 1)
	 *           S1 (depth = 3) [C[i][j]=add(C[i][j],mul(mul(IntExpr(alpha),A[i][k]),A[j][k]))]
	 *           ([C[i][j]]) = f([C[i][j], A[i][k], A[j][k]])
	 *           (Domain = [ni,beta,nj,alpha] -> {S1[i,j,k] : (k >= 0) and (-k+nj-1 >= 0) and (j >= 0) and (-j+ni-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_syrk_syrk_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_linear_algebra_kernels_syrk_syrk_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[ni, beta, nj, alpha] -> { S1[i, j, k] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + ni and i >= 0 and i <= -1 + ni; S0[i, j] : j >= 0 and j <= -1 + ni and i >= 0 and i <= -1 + ni }";
		String idSchedules = "[ni, beta, nj, alpha] -> { S1[i, j, k] -> [1, i, 0, j, 0, k, 0]; S0[i, j] -> [0, i, 0, j, 0, 0, 0] }";
		String writes = "[ni, beta, nj, alpha] -> { S0[i, j] -> C[i, j]; S1[i, j, k] -> C[i, j] }";
		String reads = "[ni, beta, nj, alpha] -> { S1[i, j, k] -> A[i, k]; S1[i, j, k] -> A[j, k]; S0[i, j] -> C[i, j]; S1[i, j, k] -> C[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[ni, beta, nj, alpha] -> { S1[i, j, k] -> S1[i, j, -1 + k] : k >= 1 and k <= -1 + nj and j >= 0 and j <= -1 + ni and i >= 0 and i <= -1 + ni; S1[i, j, 0] -> S0[i, j] : i <= -1 + ni and nj >= 1 and j >= 0 and j <= -1 + ni and i >= 0 }";
		String valueBasedPlutoSchedule = "[ni, beta, nj, alpha] -> { S0[i, j] -> [0, i, j, 0]; S1[i, j, k] -> [i, i + j, i + j + k, 1] }";
		String valueBasedFeautrierSchedule = "[ni, beta, nj, alpha] -> { S1[i, j, k] -> [1 + k, j, i]; S0[i, j] -> [0, i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[ni, beta, nj, alpha] -> { S1[i, j, k] -> S1[i, j, -1 + k] : i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + ni and k >= 1 and k <= -1 + nj; S1[i, j, 0] -> S0[i, j] : nj >= 1 and i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + ni }";
		String memoryBasedPlutoSchedule = "[ni, beta, nj, alpha] -> { S0[i, j] -> [0, i, j, 0]; S1[i, j, k] -> [i, i + j, i + j + k, 1] }";
		String memoryBasedFeautrierSchedule = "[ni, beta, nj, alpha] -> { S1[i, j, k] -> [1 + k, j, i]; S0[i, j] -> [0, i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #36
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_for_if_test_for_if/scop_0
	 *   block : 
	 *     S0 (depth = 0) [k=IntExpr(0)]
	 *     ([k]) = f([])
	 *     (Domain = [N] -> {S0[] : })
	 *     for : 0 <= j <= N-1 (stride = 1)
	 *       S1 (depth = 1) [k=add(k,IntExpr(2))]
	 *       ([k]) = f([k])
	 *       (Domain = [N] -> {S1[j] : (j >= 0) and (-j+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_test_GScopRoot_test_for_if_scop_0_ () {
		String path = "basics_oldFiles_test_GScopRoot_test_for_if_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S1[j] : j >= 0 and j <= -1 + N; S0[] }";
		String idSchedules = "[N] -> { S0[] -> [0, 0, 0]; S1[j] -> [1, j, 0] }";
		String writes = "[N] -> { S1[j] -> k[]; S0[] -> k[] }";
		String reads = "[N] -> { S1[j] -> k[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S1[j] -> S1[-1 + j] : j >= 1 and j <= -1 + N; S1[0] -> S0[] : N >= 1 }";
		String valueBasedPlutoSchedule = "[N] -> { S1[j] -> [j, 1]; S0[] -> [0, 0] }";
		String valueBasedFeautrierSchedule = "[N] -> { S1[j] -> [1 + j]; S0[] -> [0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S1[j] -> S1[-1 + j] : j >= 1 and j <= -1 + N; S1[0] -> S0[] : N >= 1 }";
		String memoryBasedPlutoSchedule = "[N] -> { S1[j] -> [j, 1]; S0[] -> [0, 0] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S1[j] -> [1 + j]; S0[] -> [0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #37
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_trmm_kernel_trmm/scop_0
	 *   for : 1 <= i <= ni-1 (stride = 1)
	 *     for : 0 <= j <= ni-1 (stride = 1)
	 *       for : 0 <= k <= i-1 (stride = 1)
	 *         S0 (depth = 3) [B[i][j]=add(B[i][j],mul(mul(IntExpr(alpha),A[i][k]),B[j][k]))]
	 *         ([B[i][j]]) = f([B[i][j], A[i][k], B[j][k]])
	 *         (Domain = [ni,alpha] -> {S0[i,j,k] : (k >= 0) and (i-k-1 >= 0) and (j >= 0) and (-j+ni-1 >= 0) and (i-1 >= 0) and (-i+ni-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_trmm_trmm_GScopRoot_kernel_trmm_scop_0_ () {
		String path = "polybench_gecos_linear_algebra_kernels_trmm_trmm_GScopRoot_kernel_trmm_scop_0_";
		// SCoP Extraction Data
		String domains = "[ni, alpha] -> { S0[i, j, k] : k >= 0 and k <= -1 + i and j >= 0 and j <= -1 + ni and i >= 1 and i <= -1 + ni }";
		String idSchedules = "[ni, alpha] -> { S0[i, j, k] -> [0, i, 0, j, 0, k, 0] }";
		String writes = "[ni, alpha] -> { S0[i, j, k] -> B[i, j] }";
		String reads = "[ni, alpha] -> { S0[i, j, k] -> A[i, k]; S0[i, j, k] -> B[i, j]; S0[i, j, k] -> B[j, k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[ni, alpha] -> { S0[i, j, k] -> S0[j, k, -1 + j] : k >= 0 and k <= -1 + i and j <= i and i <= -1 + ni and j >= 1; S0[i, j, k] -> S0[i, j, -1 + k] : i <= -1 + ni and k <= -1 + i and j >= 0 and j <= -1 + ni and k >= 1 }";
		String valueBasedPlutoSchedule = "[ni, alpha] -> { S0[i, j, k] -> [i, j + k, i + j] }";
		String valueBasedFeautrierSchedule = "[ni, alpha] -> { S0[i, j, k] -> [j + k, i, k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[ni, alpha] -> { S0[i, j, 0] -> S0[i', i, j] : i' <= -1 + i and i <= -1 + ni and j >= 0 and i' >= 1 + j; S0[i, j, k] -> S0[j, k, -1 + j] : j >= 1 and k >= 0 and j <= i and k <= -1 + i and i <= -1 + ni; S0[i, j, k] -> S0[i, j, -1 + k] : i <= -1 + ni and j >= 0 and j <= -1 + ni and k >= 1 and k <= -1 + i }";
		String memoryBasedPlutoSchedule = "[ni, alpha] -> { S0[i, j, k] -> [i, j, j + k] }";
		String memoryBasedFeautrierSchedule = "[ni, alpha] -> { S0[i, j, k] -> [i, j + k, k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #38
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_2mm_scop_new
	 *   block : 
	 *     for : 0 <= i <= ni-1 (stride = 1)
	 *       for : 0 <= j <= nj-1 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [tmp[i][j]=IntExpr(0)]
	 *           ([tmp[i][j]]) = f([])
	 *           (Domain = [ni,nj,nk,alpha,nl,beta] -> {S0[i,j] : (j >= 0) and (-j+nj-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *           for : 0 <= k <= nk-1 (stride = 1)
	 *             S1 (depth = 3) [tmp[i][j]=add(tmp[i][j],mul(mul(IntExpr(alpha),A[i][k]),B[k][j]))]
	 *             ([tmp[i][j]]) = f([tmp[i][j], A[i][k], B[k][j]])
	 *             (Domain = [ni,nj,nk,alpha,nl,beta] -> {S1[i,j,k] : (k >= 0) and (-k+nk-1 >= 0) and (j >= 0) and (-j+nj-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *     for : 0 <= i <= ni-1 (stride = 1)
	 *       for : 0 <= j <= nl-1 (stride = 1)
	 *         block : 
	 *           S2 (depth = 2) [D[i][j]=mul(D[i][j],IntExpr(beta))]
	 *           ([D[i][j]]) = f([D[i][j]])
	 *           (Domain = [ni,nj,nk,alpha,nl,beta] -> {S2[i,j] : (j >= 0) and (-j+nl-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *           for : 0 <= k <= nj-1 (stride = 1)
	 *             S3 (depth = 3) [D[i][j]=add(D[i][j],mul(tmp[i][k],C[k][j]))]
	 *             ([D[i][j]]) = f([D[i][j], tmp[i][k], C[k][j]])
	 *             (Domain = [ni,nj,nk,alpha,nl,beta] -> {S3[i,j,k] : (k >= 0) and (-k+nj-1 >= 0) and (j >= 0) and (-j+nl-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_2mm_2mm_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_linear_algebra_kernels_2mm_2mm_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[ni, nj, nk, alpha, nl, beta] -> { S1[i, j, k] : k >= 0 and k <= -1 + nk and j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S3[i, j, k] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni; S0[i, j] : j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S2[i, j] : j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni }";
		String idSchedules = "[ni, nj, nk, alpha, nl, beta] -> { S3[i, j, k] -> [1, i, 0, j, 1, k, 0]; S1[i, j, k] -> [0, i, 0, j, 1, k, 0]; S0[i, j] -> [0, i, 0, j, 0, 0, 0]; S2[i, j] -> [1, i, 0, j, 0, 0, 0] }";
		String writes = "[ni, nj, nk, alpha, nl, beta] -> { S1[i, j, k] -> tmp[i, j]; S3[i, j, k] -> D[i, j]; S0[i, j] -> tmp[i, j]; S2[i, j] -> D[i, j] }";
		String reads = "[ni, nj, nk, alpha, nl, beta] -> { S3[i, j, k] -> tmp[i, k]; S1[i, j, k] -> B[k, j]; S1[i, j, k] -> tmp[i, j]; S3[i, j, k] -> C[k, j]; S1[i, j, k] -> A[i, k]; S3[i, j, k] -> D[i, j]; S2[i, j] -> D[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[ni, nj, nk, alpha, nl, beta] -> { S3[i, j, k] -> S3[i, j, -1 + k] : k >= 1 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni; S3[i, j, k] -> S1[i, k, -1 + nk] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni and nk >= 1; S1[i, j, 0] -> S0[i, j] : i <= -1 + ni and nk >= 1 and j >= 0 and j <= -1 + nj and i >= 0; S3[i, j, 0] -> S2[i, j] : i <= -1 + ni and nj >= 1 and j >= 0 and j <= -1 + nl and i >= 0; S3[i, j, k] -> S0[i, k] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni and nk <= 0; S1[i, j, k] -> S1[i, j, -1 + k] : k >= 1 and k <= -1 + nk and j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni }";
		String valueBasedPlutoSchedule = "[ni, nj, nk, alpha, nl, beta] -> { S3[i, j, k] -> [i, i + k, 1, 1, j, 0]; S1[i, j, k] -> [i, i + j, 1, 0, k, 1]; S2[i, j] -> [0, i, 0, j, 0, 0]; S0[i, j] -> [0, i, 1, 0, j, 0] }";
		String valueBasedFeautrierSchedule = "[ni, nj, nk, alpha, nl, beta] -> { S2[i, j] -> [i, j, 0]; S0[i, j] -> [i, j, 0]; S1[i, j, k] -> [k, j, i]; S3[i, j, k] -> [k, j, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[ni, nj, nk, alpha, nl, beta] -> { S3[i, j, k] -> S1[i, k, -1 + nk] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni and nk >= 1; S3[i, j, k] -> S3[i, j, -1 + k] : i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + nl and k >= 1 and k <= -1 + nj; S1[i, j, 0] -> S0[i, j] : nk >= 1 and i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + nj; S3[i, j, 0] -> S2[i, j] : nj >= 1 and i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + nl; S3[i, j, k] -> S0[i, k] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni and nk <= 0; S1[i, j, k] -> S1[i, j, -1 + k] : i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + nj and k >= 1 and k <= -1 + nk }";
		String memoryBasedPlutoSchedule = "[ni, nj, nk, alpha, nl, beta] -> { S3[i, j, k] -> [i, i + k, 1, 1, j, 0]; S1[i, j, k] -> [i, i + j, 1, 0, k, 1]; S2[i, j] -> [0, i, 0, j, 0, 0]; S0[i, j] -> [0, i, 1, 0, j, 0] }";
		String memoryBasedFeautrierSchedule = "[ni, nj, nk, alpha, nl, beta] -> { S2[i, j] -> [i, j, 0]; S0[i, j] -> [i, j, 0]; S1[i, j, k] -> [k, j, i]; S3[i, j, k] -> [k, j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #39
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_3mm_scop_new
	 *   block : 
	 *     for : 0 <= i <= ni-1 (stride = 1)
	 *       for : 0 <= j <= nj-1 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [E[i][j]=IntExpr(0)]
	 *           ([E[i][j]]) = f([])
	 *           (Domain = [ni,nj,nk,nl,nm] -> {S0[i,j] : (j >= 0) and (-j+nj-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *           for : 0 <= k <= nk-1 (stride = 1)
	 *             S1 (depth = 3) [E[i][j]=add(E[i][j],mul(A[i][k],B[k][j]))]
	 *             ([E[i][j]]) = f([E[i][j], A[i][k], B[k][j]])
	 *             (Domain = [ni,nj,nk,nl,nm] -> {S1[i,j,k] : (k >= 0) and (-k+nk-1 >= 0) and (j >= 0) and (-j+nj-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *     for : 0 <= i <= nj-1 (stride = 1)
	 *       for : 0 <= j <= nl-1 (stride = 1)
	 *         block : 
	 *           S2 (depth = 2) [F[i][j]=IntExpr(0)]
	 *           ([F[i][j]]) = f([])
	 *           (Domain = [ni,nj,nk,nl,nm] -> {S2[i,j] : (j >= 0) and (-j+nl-1 >= 0) and (i >= 0) and (-i+nj-1 >= 0)})
	 *           for : 0 <= k <= nm-1 (stride = 1)
	 *             S3 (depth = 3) [F[i][j]=add(F[i][j],mul(C[i][k],D[k][j]))]
	 *             ([F[i][j]]) = f([F[i][j], C[i][k], D[k][j]])
	 *             (Domain = [ni,nj,nk,nl,nm] -> {S3[i,j,k] : (k >= 0) and (-k+nm-1 >= 0) and (j >= 0) and (-j+nl-1 >= 0) and (i >= 0) and (-i+nj-1 >= 0)})
	 *     for : 0 <= i <= ni-1 (stride = 1)
	 *       for : 0 <= j <= nl-1 (stride = 1)
	 *         block : 
	 *           S4 (depth = 2) [G[i][j]=IntExpr(0)]
	 *           ([G[i][j]]) = f([])
	 *           (Domain = [ni,nj,nk,nl,nm] -> {S4[i,j] : (j >= 0) and (-j+nl-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *           for : 0 <= k <= nj-1 (stride = 1)
	 *             S5 (depth = 3) [G[i][j]=add(G[i][j],mul(E[i][k],F[k][j]))]
	 *             ([G[i][j]]) = f([G[i][j], E[i][k], F[k][j]])
	 *             (Domain = [ni,nj,nk,nl,nm] -> {S5[i,j,k] : (k >= 0) and (-k+nj-1 >= 0) and (j >= 0) and (-j+nl-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_3mm_3mm_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_linear_algebra_kernels_3mm_3mm_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[ni, nj, nk, nl, nm] -> { S4[i, j] : j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni; S2[i, j] : j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + nj; S0[i, j] : j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S5[i, j, k] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni; S1[i, j, k] : k >= 0 and k <= -1 + nk and j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S3[i, j, k] : k >= 0 and k <= -1 + nm and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + nj }";
		String idSchedules = "[ni, nj, nk, nl, nm] -> { S2[i, j] -> [1, i, 0, j, 0, 0, 0]; S3[i, j, k] -> [1, i, 0, j, 1, k, 0]; S5[i, j, k] -> [2, i, 0, j, 1, k, 0]; S0[i, j] -> [0, i, 0, j, 0, 0, 0]; S4[i, j] -> [2, i, 0, j, 0, 0, 0]; S1[i, j, k] -> [0, i, 0, j, 1, k, 0] }";
		String writes = "[ni, nj, nk, nl, nm] -> { S3[i, j, k] -> F[i, j]; S2[i, j] -> F[i, j]; S1[i, j, k] -> E[i, j]; S0[i, j] -> E[i, j]; S5[i, j, k] -> G[i, j]; S4[i, j] -> G[i, j] }";
		String reads = "[ni, nj, nk, nl, nm] -> { S3[i, j, k] -> F[i, j]; S1[i, j, k] -> A[i, k]; S1[i, j, k] -> B[k, j]; S1[i, j, k] -> E[i, j]; S5[i, j, k] -> F[k, j]; S3[i, j, k] -> C[i, k]; S5[i, j, k] -> G[i, j]; S3[i, j, k] -> D[k, j]; S5[i, j, k] -> E[i, k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[ni, nj, nk, nl, nm] -> { S5[i, j, k] -> S2[k, j] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni and nm <= 0; S3[i, j, k] -> S3[i, j, -1 + k] : k >= 1 and k <= -1 + nm and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + nj; S1[i, j, k] -> S1[i, j, -1 + k] : k >= 1 and k <= -1 + nk and j >= 0 and j <= -1 + nj and i >= 0 and i <= -1 + ni; S5[i, j, k] -> S0[i, k] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni and nk <= 0; S5[i, j, 0] -> S4[i, j] : i <= -1 + ni and nj >= 1 and j >= 0 and j <= -1 + nl and i >= 0; S5[i, j, k] -> S1[i, k, -1 + nk] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni and nk >= 1; S5[i, j, k] -> S5[i, j, -1 + k] : k >= 1 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni; S3[i, j, 0] -> S2[i, j] : i <= -1 + nj and nm >= 1 and j >= 0 and j <= -1 + nl and i >= 0; S5[i, j, k] -> S3[k, j, -1 + nm] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni and nm >= 1; S1[i, j, 0] -> S0[i, j] : i <= -1 + ni and nk >= 1 and j >= 0 and j <= -1 + nj and i >= 0 }";
		String valueBasedPlutoSchedule = "[ni, nj, nk, nl, nm] -> { S2[i, j] -> [0, i, 0, j, 0]; S0[i, j] -> [0, i, 0, j, 0]; S4[i, j] -> [0, i, 0, j, 0]; S5[i, j, k] -> [i + k, i + j + k, 1, k, 0]; S1[i, j, k] -> [i, i + j, 0, k, 1]; S3[i, j, k] -> [i, i + j, 0, k, 1] }";
		String valueBasedFeautrierSchedule = "[ni, nj, nk, nl, nm] -> { S5[i, j, k] -> [k, j, i]; S0[i, j] -> [i, j, 0]; S2[i, j] -> [i, j, 0]; S4[i, j] -> [i, j, 0]; S1[i, j, k] -> [k, j, i]; S3[i, j, k] -> [k, j, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[ni, nj, nk, nl, nm] -> { S5[i, j, k] -> S2[k, j] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni and nm <= 0; S3[i, j, k] -> S3[i, j, -1 + k] : i >= 0 and i <= -1 + nj and j >= 0 and j <= -1 + nl and k >= 1 and k <= -1 + nm; S1[i, j, k] -> S1[i, j, -1 + k] : i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + nj and k >= 1 and k <= -1 + nk; S5[i, j, k] -> S0[i, k] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni and nk <= 0; S5[i, j, 0] -> S4[i, j] : nj >= 1 and i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + nl; S5[i, j, k] -> S1[i, k, -1 + nk] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni and nk >= 1; S5[i, j, k] -> S5[i, j, -1 + k] : i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + nl and k >= 1 and k <= -1 + nj; S3[i, j, 0] -> S2[i, j] : nm >= 1 and i >= 0 and i <= -1 + nj and j >= 0 and j <= -1 + nl; S5[i, j, k] -> S3[k, j, -1 + nm] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + nl and i >= 0 and i <= -1 + ni and nm >= 1; S1[i, j, 0] -> S0[i, j] : nk >= 1 and i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + nj }";
		String memoryBasedPlutoSchedule = "[ni, nj, nk, nl, nm] -> { S2[i, j] -> [0, i, 0, j, 0]; S0[i, j] -> [0, i, 0, j, 0]; S4[i, j] -> [0, i, 0, j, 0]; S5[i, j, k] -> [i + k, i + j + k, 1, k, 0]; S1[i, j, k] -> [i, i + j, 0, k, 1]; S3[i, j, k] -> [i, i + j, 0, k, 1] }";
		String memoryBasedFeautrierSchedule = "[ni, nj, nk, nl, nm] -> { S5[i, j, k] -> [k, j, i]; S0[i, j] -> [i, j, 0]; S2[i, j] -> [i, j, 0]; S4[i, j] -> [i, j, 0]; S1[i, j, k] -> [k, j, i]; S3[i, j, k] -> [k, j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #40
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_adi_kernel_adi/scop_0
	 *   for : 0 <= t <= tsteps-1 (stride = 1)
	 *     block : 
	 *       for : 0 <= i1 <= n-1 (stride = 1)
	 *         for : 1 <= i2 <= n-1 (stride = 1)
	 *           block : 
	 *             S0 (depth = 3) [X[i1][i2]=sub(X[i1][i2],div(mul(X[i1][i2-1],A[i1][i2]),B[i1][i2-1]))]
	 *             ([X[i1][i2]]) = f([X[i1][i2], X[i1][i2-1], A[i1][i2], B[i1][i2-1]])
	 *             (Domain = [tsteps,n] -> {S0[t,i1,i2] : (i2-1 >= 0) and (-i2+n-1 >= 0) and (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *             S1 (depth = 3) [B[i1][i2]=sub(B[i1][i2],div(mul(A[i1][i2],A[i1][i2]),B[i1][i2-1]))]
	 *             ([B[i1][i2]]) = f([B[i1][i2], A[i1][i2], A[i1][i2], B[i1][i2-1]])
	 *             (Domain = [tsteps,n] -> {S1[t,i1,i2] : (i2-1 >= 0) and (-i2+n-1 >= 0) and (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *       for : 0 <= i1 <= n-1 (stride = 1)
	 *         S2 (depth = 2) [X[i1][n-1]=div(X[i1][n-1],B[i1][n-1])]
	 *         ([X[i1][n-1]]) = f([X[i1][n-1], B[i1][n-1]])
	 *         (Domain = [tsteps,n] -> {S2[t,i1] : (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *       for : 0 <= i1 <= n-1 (stride = 1)
	 *         for : 0 <= i2 <= n-3 (stride = 1)
	 *           S3 (depth = 3) [X[i1][n-i2-2]=div(sub(X[i1][n-i2-2],mul(X[i1][n-i2-3],A[i1][n-i2-3])),B[i1][n-i2-3])]
	 *           ([X[i1][n-i2-2]]) = f([X[i1][n-i2-2], X[i1][n-i2-3], A[i1][n-i2-3], B[i1][n-i2-3]])
	 *           (Domain = [tsteps,n] -> {S3[t,i1,i2] : (i2 >= 0) and (-i2+n-3 >= 0) and (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *       for : 1 <= i1 <= n-1 (stride = 1)
	 *         for : 0 <= i2 <= n-1 (stride = 1)
	 *           block : 
	 *             S4 (depth = 3) [X[i1][i2]=sub(X[i1][i2],div(mul(X[i1-1][i2],A[i1][i2]),B[i1-1][i2]))]
	 *             ([X[i1][i2]]) = f([X[i1][i2], X[i1-1][i2], A[i1][i2], B[i1-1][i2]])
	 *             (Domain = [tsteps,n] -> {S4[t,i1,i2] : (i2 >= 0) and (-i2+n-1 >= 0) and (i1-1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *             S5 (depth = 3) [B[i1][i2]=sub(B[i1][i2],div(mul(A[i1][i2],A[i1][i2]),B[i1-1][i2]))]
	 *             ([B[i1][i2]]) = f([B[i1][i2], A[i1][i2], A[i1][i2], B[i1-1][i2]])
	 *             (Domain = [tsteps,n] -> {S5[t,i1,i2] : (i2 >= 0) and (-i2+n-1 >= 0) and (i1-1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *       for : 0 <= i2 <= n-1 (stride = 1)
	 *         S6 (depth = 2) [X[n-1][i2]=div(X[n-1][i2],B[n-1][i2])]
	 *         ([X[n-1][i2]]) = f([X[n-1][i2], B[n-1][i2]])
	 *         (Domain = [tsteps,n] -> {S6[t,i2] : (i2 >= 0) and (-i2+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *       for : 0 <= i1 <= n-3 (stride = 1)
	 *         for : 0 <= i2 <= n-1 (stride = 1)
	 *           S7 (depth = 3) [X[n-i1-2][i2]=div(sub(X[n-i1-2][i2],mul(X[n-i1-3][i2],A[n-i1-3][i2])),B[n-i1-2][i2])]
	 *           ([X[n-i1-2][i2]]) = f([X[n-i1-2][i2], X[n-i1-3][i2], A[n-i1-3][i2], B[n-i1-2][i2]])
	 *           (Domain = [tsteps,n] -> {S7[t,i1,i2] : (i2 >= 0) and (-i2+n-1 >= 0) and (i1 >= 0) and (-i1+n-3 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_stencils_adi_adi_GScopRoot_kernel_adi_scop_0_ () {
		String path = "polybench_gecos_stencils_adi_adi_GScopRoot_kernel_adi_scop_0_";
		// SCoP Extraction Data
		String domains = "[tsteps, n] -> { S6[t, i2] : i2 >= 0 and i2 <= -1 + n and t >= 0 and t <= -1 + tsteps; S1[t, i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S5[t, i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 1 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S2[t, i1] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S0[t, i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S3[t, i1, i2] : i2 >= 0 and i2 <= -3 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S4[t, i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 1 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S7[t, i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 0 and i1 <= -3 + n and t >= 0 and t <= -1 + tsteps }";
		String idSchedules = "[tsteps, n] -> { S4[t, i1, i2] -> [0, t, 3, i1, 0, i2, 0]; S7[t, i1, i2] -> [0, t, 5, i1, 0, i2, 0]; S0[t, i1, i2] -> [0, t, 0, i1, 0, i2, 0]; S6[t, i2] -> [0, t, 4, i2, 0, 0, 0]; S2[t, i1] -> [0, t, 1, i1, 0, 0, 0]; S1[t, i1, i2] -> [0, t, 0, i1, 0, i2, 1]; S3[t, i1, i2] -> [0, t, 2, i1, 0, i2, 0]; S5[t, i1, i2] -> [0, t, 3, i1, 0, i2, 1] }";
		String writes = "[tsteps, n] -> { S0[t, i1, i2] -> X[i1, i2]; S7[t, i1, i2] -> X[-2 + n - i1, i2]; S3[t, i1, i2] -> X[i1, -2 + n - i2]; S1[t, i1, i2] -> B[i1, i2]; S5[t, i1, i2] -> B[i1, i2]; S2[t, i1] -> X[i1, -1 + n]; S4[t, i1, i2] -> X[i1, i2]; S6[t, i2] -> X[-1 + n, i2] }";
		String reads = "[tsteps, n] -> { S0[t, i1, i2] -> X[i1, i2]; S0[t, i1, i2] -> X[i1, -1 + i2]; S0[t, i1, i2] -> A[i1, i2]; S3[t, i1, i2] -> X[i1, -2 + n - i2]; S3[t, i1, i2] -> X[i1, -3 + n - i2]; S4[t, i1, i2] -> B[-1 + i1, i2]; S7[t, i1, i2] -> X[-2 + n - i1, i2]; S7[t, i1, i2] -> X[-3 + n - i1, i2]; S4[t, i1, i2] -> A[i1, i2]; S1[t, i1, i2] -> B[i1, i2]; S1[t, i1, i2] -> B[i1, -1 + i2]; S5[t, i1, i2] -> B[i1, i2]; S5[t, i1, i2] -> B[-1 + i1, i2]; S2[t, i1] -> X[i1, -1 + n]; S5[t, i1, i2] -> A[i1, i2]; S6[t, i2] -> B[-1 + n, i2]; S7[t, i1, i2] -> B[-2 + n - i1, i2]; S4[t, i1, i2] -> X[i1, i2]; S4[t, i1, i2] -> X[-1 + i1, i2]; S3[t, i1, i2] -> A[i1, -3 + n - i2]; S3[t, i1, i2] -> B[i1, -3 + n - i2]; S7[t, i1, i2] -> A[-3 + n - i1, i2]; S2[t, i1] -> B[i1, -1 + n]; S0[t, i1, i2] -> B[i1, -1 + i2]; S6[t, i2] -> X[-1 + n, i2]; S1[t, i1, i2] -> A[i1, i2] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[tsteps, n] -> { S1[t, i1, i2] -> S1[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S1[t, 0, i2] -> S1[-1 + t, 0, i2] : i2 >= 1 and i2 <= -1 + n and t <= -1 + tsteps and t >= 1; S2[t, i1] -> S0[t, i1, -1 + n] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S0[t, i1, 1] -> S5[-1 + t, i1, 0] : t <= -1 + tsteps and n >= 2 and i1 >= 1 and i1 <= -1 + n and t >= 1; S4[t, i1, i2] -> S4[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S0[t, 0, -1 + n] -> S2[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S4[t, i1, 0] -> S7[-1 + t, -2 + n - i1, 0] : t <= -1 + tsteps and t >= 1 and i1 >= 1 and i1 <= -2 + n and n >= 1; S7[t, i1, i2] -> S4[t, i1', i2] : i2 >= 0 and i2 <= -1 + n and t >= 0 and i1' >= 1 and i1' >= -3 + n - i1 and t <= -1 + tsteps and i1 >= 0 and i1' <= -2 + n - i1; S4[t, i1, i2] -> S3[t, i1, -2 + n - i2] : i2 >= 1 and i2 <= -2 + n and i1 >= 1 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S4[t, 1, i2] -> S3[t, 0, -2 + n - i2] : i2 >= 1 and t >= 0 and t <= -1 + tsteps and i2 <= -2 + n; S0[t, i1, i2] -> S1[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S3[t, -1 + n, -3 + n] -> S6[-1 + t, 0] : n >= 3 and t <= -1 + tsteps and t >= 1; S2[t, 0] -> S6[-1 + t, 0] : n = 1 and t >= 1 and t <= -1 + tsteps; S5[t, i1, i2] -> S1[t, i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 1 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S5[t, 1, i2] -> S1[t, 0, i2] : i2 >= 1 and i2 <= -1 + n and t <= -1 + tsteps and t >= 0; S3[t, i1, -3 + n] -> S7[-1 + t, -2 + n - i1, 0] : n >= 3 and t <= -1 + tsteps and i1 >= 1 and i1 <= -2 + n and t >= 1; S3[t, i1, i2] -> S1[t, i1, -3 + n - i2] : i2 >= 0 and i2 <= -4 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S0[t, -1 + n, i2] -> S6[-1 + t, i2] : i2 >= 1 and i2 <= -1 + n and t >= 1 and t <= -1 + tsteps; S0[t, -1 + n, 1] -> S6[-1 + t, 0] : t <= -1 + tsteps and n >= 2 and t >= 1; S5[t, i1, i2] -> S5[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S5[t, i1, 0] -> S5[-1 + t, i1, 0] : t <= -1 + tsteps and t >= 1 and i1 >= 1 and i1 <= -1 + n; S6[t, i2] -> S5[t, -1 + n, i2] : i2 >= 0 and i2 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S2[t, i1] -> S1[t, i1, -1 + n] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S4[t, 1, i2] -> S1[t, 0, i2] : i2 >= 1 and i2 <= -1 + n and t <= -1 + tsteps and n >= 2 and t >= 0; S6[t, i2] -> S4[t, -1 + n, i2] : i2 >= 0 and i2 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S6[t, 0] -> S2[t, 0] : n = 1 and t >= 0 and t <= -1 + tsteps; S7[t, -3 + n, i2] -> S3[t, 0, -2 + n - i2] : i2 >= 1 and i2 <= -2 + n and n >= 3 and t <= -1 + tsteps and t >= 0; S0[t, i1, i2] -> S7[-1 + t, -2 + n - i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 1 and i1 <= -2 + n and t >= 1 and t <= -1 + tsteps; S0[t, i1, 1] -> S7[-1 + t, -2 + n - i1, 0] : t <= -1 + tsteps and i1 >= 1 and t >= 1 and i1 <= -2 + n; S7[t, i1, i2] -> S5[t, -2 + n - i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 0 and i1 <= -3 + n and t >= 0 and t <= -1 + tsteps; S4[t, i1, -1 + n] -> S2[t, i1] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S4[t, 1, -1 + n] -> S2[t, 0] : n >= 2 and t >= 0 and t <= -1 + tsteps; S0[t, 0, i2] -> S3[-1 + t, 0, -2 + n - i2] : i2 >= 1 and t >= 1 and t <= -1 + tsteps and i2 <= -2 + n and n >= 1; S4[t, i1, i2] -> S5[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S7[t, -3 + n, -1 + n] -> S2[t, 0] : n >= 3 and t >= 0 and t <= -1 + tsteps; S3[t, i1, i2] -> S0[t, i1, i2'] : i2' <= -2 + n - i2 and i2' >= 1 and i1 <= -1 + n and t >= 0 and i2' >= -3 + n - i2 and t <= -1 + tsteps and i2 >= 0 and i1 >= 0; S0[t, i1, i2] -> S0[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S4[t, -1 + n, 0] -> S6[-1 + t, 0] : t <= -1 + tsteps and t >= 1 and n >= 2; S1[t, i1, i2] -> S5[-1 + t, i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 1 and i1 <= -1 + n and t >= 1 and t <= -1 + tsteps; S1[t, i1, 1] -> S5[-1 + t, i1, 0] : t <= -1 + tsteps and t >= 1 and i1 >= 1 and i1 <= -1 + n; S3[t, i1, -3 + n] -> S5[-1 + t, i1, 0] : n >= 3 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and t >= 1 }";
		String valueBasedPlutoSchedule = "[tsteps, n] -> { S5[t, i1, i2] -> [t, i1, i2, 6]; S6[t, i2] -> [t, 1 + n, 2 + i2, 7]; S0[t, i1, i2] -> [t, 2 + i1, 2 + i2, 0]; S4[t, i1, i2] -> [t, 2 + i1, 2 + i2, 3]; S1[t, i1, i2] -> [t, i1, i2, 5]; S7[t, i1, i2] -> [t, n - i1, 2 + i2, 4]; S2[t, i1] -> [t, 2 + i1, 1 + n, 2]; S3[t, i1, i2] -> [t, 2 + i1, n - i2, 1] }";
		String valueBasedFeautrierSchedule = "[tsteps, n] -> { S3[t, i1, i2] -> [n + 4t + i1 - i2, t, i2]; S7[t, i1, i2] -> [2 + n + 4t - i1 + i2, t, i2]; S2[t, i1] -> [1 + n + 4t + i1, t, 0]; S0[t, i1, i2] -> [1 + 4t + i1 + i2, t, i2]; S4[t, i1, i2] -> [3 + 4t + i1 + i2, t, i2]; S6[t, i2] -> [3 + n + 4t + i2, t, 0]; S1[t, i1, i2] -> [2t + i1 + i2, t, i2]; S5[t, i1, i2] -> [1 + 2t + i1 + i2, t, i2] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[tsteps, n] -> { S3[t, i1, i2] -> S0[t, i1, i2'] : t <= -1 + tsteps and i2' >= 1 and i2' >= -3 + n - i2 and i2 >= 0 and i2' <= -1 + n and i2 <= -3 + n and i2' <= -1 + n - i2 and t >= 0 and i1 >= 0 and i1 <= -1 + n; S2[t, i1] -> S1[t, i1, -1 + n] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S7[t, i1, i2] -> S7[t, -1 + i1, i2] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -3 + n and i2 >= 0 and i2 <= -1 + n; S4[t, i1, i2] -> S3[t, i1, -2 + n - i2] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and i2 <= -2 + n and i2 >= 1; S4[t, 1, i2] -> S3[t, 0, -2 + n - i2] : t >= 0 and t <= -1 + tsteps and i2 <= -2 + n and i2 >= 1; S4[t, i1, 0] -> S3[t, i1, -3 + n] : n >= 3 and t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S1[t, i1, i2] -> S4[-1 + t, 1 + i1, i2] : t >= 1 and t <= -1 + tsteps and i1 >= 0 and i1 <= -2 + n and i2 >= 1 and i2 <= -1 + n; S0[t, i1, 1] -> S5[-1 + t, i1, 0] : t <= -1 + tsteps and n >= 2 and i1 >= 1 and i1 <= -1 + n and t >= 1; S3[t, i1, -3 + n] -> S7[-1 + t, -2 + n - i1, 0] : n >= 3 and t <= -1 + tsteps and i1 >= 1 and i1 <= -2 + n and t >= 1; S7[t, i1, i2] -> S5[t, -2 + n - i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 0 and i1 <= -3 + n and t >= 0 and t <= -1 + tsteps; S2[t, i1] -> S0[t, i1, -1 + n] : n >= 2 and t >= 0 and t <= -1 + tsteps and i1 >= 0 and i1 <= -1 + n; S1[t, i1, i2] -> S7[-1 + t, -2 + n - i1, i2] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1 and i2 >= 1 and i2 <= -1 + n; S5[t, -1 + n, 0] -> S6[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S0[t, i1, i2] -> S7[-1 + t, -2 + n - i1, i2] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1 and i2 >= 1 and i2 <= -1 + n; S0[t, 0, i2] -> S7[-1 + t, -3 + n, i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -2 + n; S0[t, i1, 1] -> S7[-1 + t, -2 + n - i1, 0] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1; S0[t, 0, -1 + n] -> S7[-1 + t, -3 + n, -1 + n] : n >= 3 and t >= 1 and t <= -1 + tsteps; S4[t, -1 + n, 0] -> S6[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S4[t, 1, i2] -> S1[t, 0, i2] : i2 >= 1 and i2 <= -1 + n and t <= -1 + tsteps and n >= 2 and t >= 0; S1[t, 0, i2] -> S0[-1 + t, 0, 1 + i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -2 + n and n >= 1; S2[t, 0] -> S6[-1 + t, 0] : n = 1 and t >= 1 and t <= -1 + tsteps; S0[t, i1, i2] -> S1[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S0[t, 0, i2] -> S3[-1 + t, 0, -2 + n - i2] : t >= 1 and t <= -1 + tsteps and i2 <= -2 + n and i2 >= 1; S6[t, 0] -> S2[t, 0] : n = 1 and t >= 0 and t <= -1 + tsteps; S4[t, i1, -1 + n] -> S2[t, i1] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S4[t, 1, -1 + n] -> S2[t, 0] : n >= 2 and t >= 0 and t <= -1 + tsteps; S0[t, 0, -1 + n] -> S2[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S5[t, i1, 0] -> S7[-1 + t, -2 + n - i1, 0] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1 and n >= 1; S5[t, i1, i2] -> S1[t, i1, i2'] : i2' >= i2 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and i2' >= 1 and i2' <= -1 + n and i2' <= 1 + i2 and t >= 0; S5[t, 1, i2] -> S1[t, 0, i2] : t >= 0 and t <= -1 + tsteps and i2 >= 1 and i2 <= -1 + n; S7[t, -3 + n, i2] -> S3[t, 0, -2 + n - i2] : i2 >= 1 and i2 <= -2 + n and n >= 3 and t <= -1 + tsteps and t >= 0; S1[t, 0, -1 + n] -> S2[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S4[t, i1, 0] -> S0[t, i1, 1] : n >= 2 and t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S7[t, i1, i2] -> S4[t, i1', i2] : t <= -1 + tsteps and i1' >= 1 and i1' >= -3 + n - i1 and i1 >= 0 and i2 >= 0 and i1' <= -1 + n and i1' <= -1 + n - i1 and i2 <= -1 + n and t >= 0 and i1 <= -3 + n; S4[t, i1, i2] -> S4[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S1[t, -1 + n, i2] -> S6[-1 + t, i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -1 + n and n >= 1; S6[t, i2] -> S5[t, -1 + n, i2] : i2 >= 0 and i2 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S5[t, i1, i2] -> S3[t, i1, -3 + n - i2] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and i2 <= -3 + n and i2 >= 0; S5[t, i1, i2] -> S5[t, -1 + i1, i2] : t >= 0 and t <= -1 + tsteps and i1 >= 2 and i1 <= -1 + n and i2 >= 0 and i2 <= -1 + n; S5[t, i1, 0] -> S5[-1 + t, i1', 0] : t >= 1 and t <= -1 + tsteps and i1' <= -1 + n and i1 >= 1 and i1' >= i1 and i1' <= 1 + i1; S7[t, -3 + n, -1 + n] -> S2[t, 0] : n >= 3 and t >= 0 and t <= -1 + tsteps; S5[t, i1, -1 + n] -> S2[t, i1] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and n >= 1; S5[t, i1, i2] -> S0[t, i1, 1 + i2] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and i2 >= 0 and i2 <= -2 + n; S6[t, i2] -> S4[t, -1 + n, i2] : n >= 2 and t >= 0 and t <= -1 + tsteps and i2 >= 0 and i2 <= -1 + n; S3[t, -1 + n, -3 + n] -> S6[-1 + t, 0] : n >= 3 and t <= -1 + tsteps and t >= 1; S0[t, 0, i2] -> S4[-1 + t, 1, i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -1 + n; S1[t, 0, i2] -> S1[-1 + t, 0, i2'] : t >= 1 and t <= -1 + tsteps and i2' <= -1 + n and i2 >= 1 and i2' >= i2 and i2' <= 1 + i2; S1[t, i1, i2] -> S1[t, i1, -1 + i2] : t >= 0 and t <= -1 + tsteps and i1 >= 0 and i1 <= -1 + n and i2 >= 2 and i2 <= -1 + n; S0[t, i1, i2] -> S0[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S3[t, i1, i2] -> S1[t, i1, -3 + n - i2] : i2 >= 0 and i2 <= -4 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S1[t, 0, i2] -> S3[-1 + t, 0, -3 + n - i2] : t >= 1 and t <= -1 + tsteps and i2 <= -3 + n and i2 >= 1 and n >= 1; S1[t, i1, i2] -> S5[-1 + t, i1', i2] : t >= 1 and t <= -1 + tsteps and i1' >= 1 and i1' <= -1 + n and i2 >= 1 and i2 <= -1 + n and i1' >= i1 and i1' <= 1 + i1; S1[t, i1, 1] -> S5[-1 + t, i1, 0] : t >= 1 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S3[t, i1, i2] -> S3[t, i1, -1 + i2] : t >= 0 and t <= -1 + tsteps and i1 >= 0 and i1 <= -1 + n and i2 >= 1 and i2 <= -3 + n; S0[t, -1 + n, i2] -> S6[-1 + t, i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -1 + n; S0[t, -1 + n, 1] -> S6[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S4[t, i1, i2] -> S5[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S5[t, i1, 0] -> S4[-1 + t, 1 + i1, 0] : t >= 1 and t <= -1 + tsteps and i1 >= 1 and i1 <= -2 + n and n >= 1; S3[t, i1, -3 + n] -> S5[-1 + t, i1, 0] : n >= 3 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and t >= 1; S4[t, i1, 0] -> S7[-1 + t, -2 + n - i1, 0] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1 }";
		String memoryBasedPlutoSchedule = "[tsteps, n] -> { S5[t, i1, i2] -> [t, i1, i2, 5]; S6[t, i2] -> [t, n, i2, 7]; S0[t, i1, i2] -> [t, i1, -2n + i2, 3]; S4[t, i1, i2] -> [t, i1, i2, 1]; S1[t, i1, i2] -> [t, i1, -2n + i2, 6]; S7[t, i1, i2] -> [t, n + i1, i2, 0]; S2[t, i1] -> [t, i1, 0, 4]; S3[t, i1, i2] -> [t, i1, -n + i2, 2] }";
		String memoryBasedFeautrierSchedule = "[tsteps, n] -> { S5[t, i1, i2] -> [t, 3, i1, i2]; S6[t, i2] -> [t, 5, i2, 0]; S0[t, i1, i2] -> [t, 1, i2, i1]; S4[t, i1, i2] -> [t, 4, i1, i2]; S1[t, i1, i2] -> [t, 0, i2, i1]; S7[t, i1, i2] -> [t, 5, i1, i2]; S2[t, i1] -> [t, 2, i1, 0]; S3[t, i1, i2] -> [t, 2, i2, i1] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #41
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : FFT_FFT/scop_0
	 *   if : dir = 1
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [x[i]=div(x[i],IntExpr(n))]
	 *         ([x[i]]) = f([x[i]])
	 *         (Domain = [dir,n] -> {S0[i] : (dir-1 = 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *         S1 (depth = 1) [y[i]=div(y[i],IntExpr(n))]
	 *         ([y[i]]) = f([y[i]])
	 *         (Domain = [dir,n] -> {S1[i] : (dir-1 = 0) and (i >= 0) and (-i+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_fft_GScopRoot_FFT_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_fft_GScopRoot_FFT_scop_0_";
		// SCoP Extraction Data
		String domains = "[dir, n] -> { S1[i] : dir = 1 and i >= 0 and i <= -1 + n; S0[i] : dir = 1 and i >= 0 and i <= -1 + n }";
		String idSchedules = "[dir, n] -> { S0[i] -> [0, i, 0]; S1[i] -> [0, i, 1] }";
		String writes = "[dir, n] -> { S1[i] -> y[i]; S0[i] -> x[i] }";
		String reads = "[dir, n] -> { S1[i] -> y[i]; S0[i] -> x[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[dir, n] -> {  }";
		String valueBasedPlutoSchedule = "[dir, n] -> { S1[i] -> [i]; S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "[dir, n] -> { S1[i] -> [i]; S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[dir, n] -> {  }";
		String memoryBasedPlutoSchedule = "[dir, n] -> { S1[i] -> [i]; S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "[dir, n] -> { S1[i] -> [i]; S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #42
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : FFT_scop_new
	 *   block : 
	 *     S0 (depth = 0) [n=IntExpr(1)]
	 *     ([n]) = f([])
	 *     (Domain = [m] -> {S0[] : })
	 *     for : 0 <= i <= m-1 (stride = 1)
	 *       S1 (depth = 1) [n=mul(n,IntExpr(2))]
	 *       ([n]) = f([n])
	 *       (Domain = [m] -> {S1[i] : (i >= 0) and (-i+m-1 >= 0)})
	 *     S2 (depth = 0) [j=IntExpr(0)]
	 *     ([j]) = f([])
	 *     (Domain = [m] -> {S2[] : })
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_fft_GScopRoot_scop_new_ () {
		String path = "polymodel_src_polybenchs_perso_fft_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[m] -> { S1[i] : i >= 0 and i <= -1 + m; S0[]; S2[] }";
		String idSchedules = "[m] -> { S1[i] -> [1, i, 0]; S2[] -> [2, 0, 0]; S0[] -> [0, 0, 0] }";
		String writes = "[m] -> { S2[] -> j[]; S1[i] -> n[]; S0[] -> n[] }";
		String reads = "[m] -> { S1[i] -> n[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[m] -> { S1[i] -> S1[-1 + i] : i >= 1 and i <= -1 + m; S1[0] -> S0[] : m >= 1 }";
		String valueBasedPlutoSchedule = "[m] -> { S0[] -> [0, 0]; S2[] -> [0, 0]; S1[i] -> [i, 1] }";
		String valueBasedFeautrierSchedule = "[m] -> { S1[i] -> [1 + i]; S0[] -> [0]; S2[] -> [0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[m] -> { S1[i] -> S1[-1 + i] : i >= 1 and i <= -1 + m; S1[0] -> S0[] : m >= 1 }";
		String memoryBasedPlutoSchedule = "[m] -> { S0[] -> [0, 0]; S2[] -> [0, 0]; S1[i] -> [i, 1] }";
		String memoryBasedFeautrierSchedule = "[m] -> { S1[i] -> [1 + i]; S0[] -> [0]; S2[] -> [0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #43
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : fir_basic_scop_new
	 *   block : 
	 *     S0 (depth = 0) [y=IntExpr(0)]
	 *     ([y]) = f([])
	 *     (Domain = [] -> {S0[] : })
	 *     for : 0 <= k <= 2 (stride = 1)
	 *       block : 
	 *         S1 (depth = 1) [z[0]=input[k]]
	 *         ([z[0]]) = f([input[k]])
	 *         (Domain = [] -> {S1[k] : (k >= 0) and (-k+2 >= 0)})
	 *         for : 0 <= i <= 2 (stride = 1)
	 *           S2 (depth = 2) [y=add(y,mul(h[i],z[i]))]
	 *           ([y]) = f([y, h[i], z[i]])
	 *           (Domain = [] -> {S2[k,i] : (i >= 0) and (-i+2 >= 0) and (k >= 0) and (-k+2 >= 0)})
	 *         for : 0 <= j <= 2 (stride = -1)
	 *           S3 (depth = 2) [z[j+1]=z[j]]
	 *           ([z[j+1]]) = f([z[j]])
	 *           (Domain = [] -> {S3[k,j] : (j >= 0) and (-j+2 >= 0) and (k >= 0) and (-k+2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_fir_GScopRoot_scop_new_ () {
		String path = "basics_oldFiles_fir_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "{ S2[k, i] : i >= 0 and i <= 2 and k >= 0 and k <= 2; S1[k] : k >= 0 and k <= 2; S3[k, j] : j >= 0 and j <= 2 and k >= 0 and k <= 2; S0[] }";
		String idSchedules = "{ S0[] -> [0, 0, 0, 0, 0]; S3[k, j] -> [1, k, 2, -j, 0]; S1[k] -> [1, k, 0, 0, 0]; S2[k, i] -> [1, k, 1, i, 0] }";
		String writes = "{ S2[k, i] -> y[]; S1[k] -> z[0]; S0[] -> y[]; S3[k, j] -> z[1 + j] }";
		String reads = "{ S2[k, i] -> y[]; S2[k, i] -> h[i]; S1[k] -> input[k]; S3[k, j] -> z[j]; S2[k, i] -> z[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S3[k, j] -> S3[-1 + k, -1 + j] : j >= 1 and j <= 2 and k >= 1 and k <= 2; S2[k, 0] -> S1[k] : k >= 0 and k <= 2; S2[k, i] -> S3[-1 + k, -1 + i] : i >= 1 and i <= 2 and k >= 1 and k <= 2; S2[k, i] -> S2[k, -1 + i] : i >= 1 and i <= 2 and k >= 0 and k <= 2; S2[k, 0] -> S2[-1 + k, 2] : k >= 1 and k <= 2; S3[k, 0] -> S1[k] : k >= 0 and k <= 2; S2[0, 0] -> S0[] }";
		String valueBasedPlutoSchedule = "{ S3[k, j] -> [k, 2 + j, 1]; S0[] -> [0, 0, 0]; S2[k, i] -> [k, 2k + i, 3]; S1[k] -> [0, k, 2] }";
		String valueBasedFeautrierSchedule = "{ S3[k, j] -> [1 + j, k]; S1[k] -> [0, k]; S2[k, i] -> [1 + 3k + i, k]; S0[] -> [0, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S1[k] -> S3[-1 + k, 0] : k <= 2 and k >= 1; S3[k, j] -> S3[-1 + k, j'] : k <= 2 and j' >= 0 and j' >= -1 + j and j <= 2 and k >= 1 and j' <= j; S3[k, j] -> S3[k, 1 + j] : k <= 2 and k >= 0 and j <= 1 and j >= 0; S2[k, 0] -> S1[k] : k >= 0 and k <= 2; S1[k] -> S2[-1 + k, 0] : k <= 2 and k >= 1; S3[k, j] -> S2[k, 1 + j] : k <= 2 and k >= 0 and j <= 1 and j >= 0; S2[k, i] -> S3[-1 + k, -1 + i] : i >= 1 and i <= 2 and k >= 1 and k <= 2; S2[k, i] -> S2[k, -1 + i] : k <= 2 and k >= 0 and i <= 2 and i >= 1; S2[k, 0] -> S2[-1 + k, 2] : k <= 2 and k >= 1; S3[k, 0] -> S1[k] : k >= 0 and k <= 2; S1[k] -> S1[-1 + k] : k >= 1 and k <= 2; S2[0, 0] -> S0[] }";
		String memoryBasedPlutoSchedule = "{ S3[k, j] -> [k, 3 + 3k - j, 3]; S0[] -> [0, 0, 0]; S2[k, i] -> [k, 3k + i, 2]; S1[k] -> [k, 3k, 1] }";
		String memoryBasedFeautrierSchedule = "{ S3[k, j] -> [5 + 6k - j, k]; S1[k] -> [6k, 0]; S2[k, i] -> [1 + 6k + i, k]; S0[] -> [0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #44
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_mvt_scop_new
	 *   block : 
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       for : 0 <= j <= n-1 (stride = 1)
	 *         S0 (depth = 2) [x1[i]=add(x1[i],mul(A[i][j],y_1[j]))]
	 *         ([x1[i]]) = f([x1[i], A[i][j], y_1[j]])
	 *         (Domain = [n] -> {S0[i,j] : (j >= 0) and (-j+n-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       for : 0 <= j <= n-1 (stride = 1)
	 *         S1 (depth = 2) [x2[i]=add(x2[i],mul(A[j][i],y_2[j]))]
	 *         ([x2[i]]) = f([x2[i], A[j][i], y_2[j]])
	 *         (Domain = [n] -> {S1[i,j] : (j >= 0) and (-j+n-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_mvt_mvt_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_linear_algebra_kernels_mvt_mvt_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[n] -> { S1[i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n; S0[i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n }";
		String idSchedules = "[n] -> { S0[i, j] -> [0, i, 0, j, 0]; S1[i, j] -> [1, i, 0, j, 0] }";
		String writes = "[n] -> { S1[i, j] -> x2[i]; S0[i, j] -> x1[i] }";
		String reads = "[n] -> { S0[i, j] -> A[i, j]; S1[i, j] -> y_2[j]; S1[i, j] -> x2[i]; S1[i, j] -> A[j, i]; S0[i, j] -> y_1[j]; S0[i, j] -> x1[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S0[i, j] -> S0[i, -1 + j] : j >= 1 and j <= -1 + n and i >= 0 and i <= -1 + n; S1[i, j] -> S1[i, -1 + j] : j >= 1 and j <= -1 + n and i >= 0 and i <= -1 + n }";
		String valueBasedPlutoSchedule = "[n] -> { S1[i, j] -> [i, j]; S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[n] -> { S1[i, j] -> [j, i]; S0[i, j] -> [j, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S0[i, j] -> S0[i, -1 + j] : i >= 0 and i <= -1 + n and j >= 1 and j <= -1 + n; S1[i, j] -> S1[i, -1 + j] : i >= 0 and i <= -1 + n and j >= 1 and j <= -1 + n }";
		String memoryBasedPlutoSchedule = "[n] -> { S1[i, j] -> [i, j]; S0[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S1[i, j] -> [j, i]; S0[i, j] -> [j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #45
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_test/scop_0
	 *   block : 
	 *     for : 0 <= i <= 9 (stride = 1)
	 *       for : 0 <= j <= 9 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [N[i][j]=call addSubMul(M[i][j],M[i][j+1],((*SI32)&(M[i+1][j])),((*SI32)&(M[i+1][j+1])))]
	 *           ([N[i][j], M[i+1][j], M[i+1][j+1]]) = f([M[i][j], M[i][j+1]])
	 *           (Domain = [] -> {S0[i,j] : (j >= 0) and (-j+9 >= 0) and (i >= 0) and (-i+9 >= 0)})
	 *           S1 (depth = 2) [M[i][j]=call addSubMul(N[i][j],N[i][j+1],((*SI32)&(N[i+1][j])),((*SI32)&(N[i+1][j+1])))]
	 *           ([M[i][j], N[i+1][j], N[i+1][j+1]]) = f([N[i][j], N[i][j+1]])
	 *           (Domain = [] -> {S1[i,j] : (j >= 0) and (-j+9 >= 0) and (i >= 0) and (-i+9 >= 0)})
	 *           S2 (depth = 2) [res[i]=add(add(res[i],M[i][j]),N[i][j])]
	 *           ([res[i]]) = f([res[i], M[i][j], N[i][j]])
	 *           (Domain = [] -> {S2[i,j] : (j >= 0) and (-j+9 >= 0) and (i >= 0) and (-i+9 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_pureFuncTest1_GScopRoot_test_scop_0_ () {
		String path = "polymodel_src_corner_cases_pureFuncTest1_GScopRoot_test_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S0[i, j] : j >= 0 and j <= 9 and i >= 0 and i <= 9; S2[i, j] : j >= 0 and j <= 9 and i >= 0 and i <= 9; S1[i, j] : j >= 0 and j <= 9 and i >= 0 and i <= 9 }";
		String idSchedules = "{ S1[i, j] -> [0, i, 0, j, 1]; S0[i, j] -> [0, i, 0, j, 0]; S2[i, j] -> [0, i, 0, j, 2] }";
		String writes = "{ S1[i, j] -> M[i, j]; S0[i, j] -> N[i, j]; S0[i, j] -> M[1 + i, 1 + j]; S0[i, j] -> M[1 + i, j]; S1[i, j] -> N[1 + i, 1 + j]; S1[i, j] -> N[1 + i, j]; S2[i, j] -> res[i] }";
		String reads = "{ S2[i, j] -> N[i, j]; S0[i, j] -> M[i, 1 + j]; S0[i, j] -> M[i, j]; S1[i, j] -> N[i, 1 + j]; S1[i, j] -> N[i, j]; S2[i, j] -> M[i, j]; S2[i, j] -> res[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S1[i, j] -> S1[-1 + i, 1 + j] : j >= 0 and j <= 8 and i >= 1 and i <= 9; S1[i, 9] -> S1[-1 + i, 9] : i >= 1 and i <= 9; S0[i, j] -> S0[-1 + i, j'] : i >= 1 and j' <= 1 + j and i <= 9 and j' >= j and j' <= 9 and j >= 0; S2[i, j] -> S0[i, j] : j >= 0 and j <= 9 and i >= 0 and i <= 9; S1[i, j] -> S0[i, j] : j >= 0 and j <= 9 and i >= 0 and i <= 9; S2[i, j] -> S2[i, -1 + j] : j >= 1 and j <= 9 and i >= 0 and i <= 9; S2[i, j] -> S1[i, j] : j >= 0 and j <= 9 and i >= 0 and i <= 9 }";
		String valueBasedPlutoSchedule = "{ S0[i, j] -> [i, i + j, 0]; S1[i, j] -> [i, i + j, 1]; S2[i, j] -> [i, 9 + j, 2] }";
		String valueBasedFeautrierSchedule = "{ S0[i, j] -> [i, j]; S1[i, j] -> [1 + i, j]; S2[i, j] -> [11 + j, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S0[i, j] -> S1[-1 + i, j] : i <= 9 and i >= 1 and j <= 9 and j >= 0; S0[i, j] -> S1[i, -1 + j] : i <= 9 and i >= 0 and j <= 9 and j >= 1; S1[i, j] -> S1[-1 + i, 1 + j] : i <= 9 and i >= 1 and j <= 8 and j >= 0; S1[i, j] -> S1[i, -1 + j] : i <= 9 and i >= 0 and j <= 9 and j >= 1; S1[i, 9] -> S1[-1 + i, 9] : i <= 9 and i >= 1; S0[i, j] -> S0[-1 + i, j'] : i <= 9 and j' <= 1 + j and j' <= 9 and i >= 1 and j' >= j and j >= 0; S0[i, j] -> S0[i, -1 + j] : i <= 9 and i >= 0 and j <= 9 and j >= 1; S2[i, j] -> S0[i, j] : j >= 0 and j <= 9 and i >= 0 and i <= 9; S1[i, j] -> S0[i', j'] : i' >= 0 and j <= 9 and j' <= j and j' >= i - i' and i' <= i and j' >= -1 + i + j - i' and i <= 9; S1[i, 0] -> S0[-1 + i, 0] : i <= 9 and i >= 1; S2[i, j] -> S2[i, -1 + j] : i <= 9 and i >= 0 and j <= 9 and j >= 1; S2[i, j] -> S1[i, j] : j >= 0 and j <= 9 and i >= 0 and i <= 9 }";
		String memoryBasedPlutoSchedule = "{ S0[i, j] -> [i, i + j, 0]; S1[i, j] -> [i, i + j, 1]; S2[i, j] -> [i, 9 + j, 2] }";
		String memoryBasedFeautrierSchedule = "{ S0[i, j] -> [3i + 2j, i + j]; S1[i, j] -> [1 + 3i + 2j, i + j]; S2[i, j] -> [38 + j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #46
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : comp_data_comp_data/scop_0
	 *   for : 0 <= y <= height-1 (stride = 1)
	 *     block : 
	 *       S0 (depth = 1) [tmp[x][y]=IntExpr(255)]
	 *       ([tmp[x][y]]) = f([])
	 *       (Domain = [height,x] -> {S0[y] : (y >= 0) and (-y+height-1 >= 0)})
	 *       for : 0 <= v <= 15 (stride = 1)
	 *         block : 
	 *           S1 (depth = 2) [val=mux(lt(sub(out1[x][y],out2[x-v][y]),IntExpr(0)),sub(neg(out1[x][y]),out2[x-v][y]),sub(out1[x][y],out2[x-v][y]))]
	 *           ([val]) = f([out1[x][y], out2[x-v][y], out1[x][y], out2[x-v][y], out1[x][y], out2[x-v][y]])
	 *           (Domain = [height,x] -> {S1[y,v] : (v >= 0) and (-v+15 >= 0) and (y >= 0) and (-y+height-1 >= 0)})
	 *           S2 (depth = 2) [data[x][y][v]=mul(0.0,mux(lt(val,15.0),val,15.0))]
	 *           ([data[x][y][v]]) = f([val, val])
	 *           (Domain = [height,x] -> {S2[y,v] : (v >= 0) and (-v+15 >= 0) and (y >= 0) and (-y+height-1 >= 0)})
	 *           S3 (depth = 2) [tmp[x][y]=mux(lt(tmp[x][y],data[x][y][v]),tmp[x][y],data[x][y][v])]
	 *           ([tmp[x][y]]) = f([tmp[x][y], data[x][y][v], tmp[x][y], data[x][y][v]])
	 *           (Domain = [height,x] -> {S3[y,v] : (v >= 0) and (-v+15 >= 0) and (y >= 0) and (-y+height-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_forunroller_test3fail_GScopRoot_comp_data_scop_0_ () {
		String path = "polymodel_others_forunroller_test3fail_GScopRoot_comp_data_scop_0_";
		// SCoP Extraction Data
		String domains = "[height, x] -> { S3[y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height; S1[y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height; S2[y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height; S0[y] : y >= 0 and y <= -1 + height }";
		String idSchedules = "[height, x] -> { S3[y, v] -> [0, y, 1, v, 2]; S2[y, v] -> [0, y, 1, v, 1]; S1[y, v] -> [0, y, 1, v, 0]; S0[y] -> [0, y, 0, 0, 0] }";
		String writes = "[height, x] -> { S2[y, v] -> data[x, y, v]; S1[y, v] -> val[]; S3[y, v] -> tmp[x, y]; S0[y] -> tmp[x, y] }";
		String reads = "[height, x] -> { S3[y, v] -> data[x, y, v]; S1[y, v] -> out2[x - v, y]; S3[y, v] -> tmp[x, y]; S1[y, v] -> out1[x, y]; S2[y, v] -> val[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[height, x] -> { S2[y, v] -> S1[y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height; S3[y, v] -> S2[y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height; S3[y, 0] -> S0[y] : y >= 0 and y <= -1 + height; S3[y, v] -> S3[y, -1 + v] : v >= 1 and v <= 15 and y >= 0 and y <= -1 + height }";
		String valueBasedPlutoSchedule = "[height, x] -> { S1[y, v] -> [y, v, 1]; S2[y, v] -> [y, v, 2]; S0[y] -> [0, y, 0]; S3[y, v] -> [y, y + v, 3] }";
		String valueBasedFeautrierSchedule = "[height, x] -> { S2[y, v] -> [y, v]; S0[y] -> [y, 0]; S1[y, v] -> [y, v]; S3[y, v] -> [v, y] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[height, x] -> { S1[y, v] -> S1[y, -1 + v] : v >= 1 and v <= 15 and y >= 0 and y <= -1 + height; S1[y, 0] -> S1[-1 + y, 15] : y >= 1 and y <= -1 + height; S3[y, 0] -> S0[y] : y >= 0 and y <= -1 + height; S2[y, v] -> S1[y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height; S3[y, v] -> S3[y, -1 + v] : y >= 0 and y <= -1 + height and v <= 15 and v >= 1; S1[y, v] -> S2[y, -1 + v] : y >= 0 and y <= -1 + height and v <= 15 and v >= 1; S1[y, 0] -> S2[-1 + y, 15] : y >= 1 and y <= -1 + height; S3[y, v] -> S2[y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height }";
		String memoryBasedPlutoSchedule = "[height, x] -> { S1[y, v] -> [y, 15y + v, 1]; S2[y, v] -> [y, 15y + v, 2]; S0[y] -> [0, y, 0]; S3[y, v] -> [y, 15y + v, 3] }";
		String memoryBasedFeautrierSchedule = "[height, x] -> { S2[y, v] -> [1 + 32y + 2v, y]; S0[y] -> [0, y]; S1[y, v] -> [32y + 2v, y]; S3[y, v] -> [17 + 32y + v, y] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #47
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : multivars_multivars/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [s=IntExpr(0)]
	 *         ([s]) = f([])
	 *         (Domain = [N] -> {S0[i] : (i >= 0) and (-i+N-1 >= 0)})
	 *         for : 0 <= j <= N-1 (stride = 1)
	 *           S1 (depth = 2) [s=add(A[i],IntExpr(i))]
	 *           ([s]) = f([A[i]])
	 *           (Domain = [N] -> {S1[i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 *         S2 (depth = 1) [sum=s]
	 *         ([sum]) = f([s])
	 *         (Domain = [N] -> {S2[i] : (i >= 0) and (-i+N-1 >= 0)})
	 *         S3 (depth = 1) [A[i]=s]
	 *         ([A[i]]) = f([s])
	 *         (Domain = [N] -> {S3[i] : (i >= 0) and (-i+N-1 >= 0)})
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S4 (depth = 1) [s=sum]
	 *         ([s]) = f([sum])
	 *         (Domain = [N] -> {S4[i] : (i >= 0) and (-i+N-1 >= 0)})
	 *         for : 0 <= j <= N-1 (stride = 1)
	 *           S5 (depth = 2) [s=add(A[i],IntExpr(i))]
	 *           ([s]) = f([A[i]])
	 *           (Domain = [N] -> {S5[i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 *         S6 (depth = 1) [B[i]=s]
	 *         ([B[i]]) = f([s])
	 *         (Domain = [N] -> {S6[i] : (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_multiVars_GScopRoot_multivars_scop_0_ () {
		String path = "polymodel_src_corner_cases_multiVars_GScopRoot_multivars_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S3[i] : i >= 0 and i <= -1 + N; S4[i] : i >= 0 and i <= -1 + N; S0[i] : i >= 0 and i <= -1 + N; S1[i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N; S5[i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N; S2[i] : i >= 0 and i <= -1 + N; S6[i] : i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S2[i] -> [0, i, 2, 0, 0]; S3[i] -> [0, i, 3, 0, 0]; S1[i, j] -> [0, i, 1, j, 0]; S4[i] -> [1, i, 0, 0, 0]; S6[i] -> [1, i, 2, 0, 0]; S0[i] -> [0, i, 0, 0, 0]; S5[i, j] -> [1, i, 1, j, 0] }";
		String writes = "[N] -> { S4[i] -> s[]; S3[i] -> A[i]; S0[i] -> s[]; S6[i] -> B[i]; S5[i, j] -> s[]; S1[i, j] -> s[]; S2[i] -> sum[] }";
		String reads = "[N] -> { S1[i, j] -> A[i]; S6[i] -> s[]; S5[i, j] -> A[i]; S2[i] -> s[]; S3[i] -> s[]; S4[i] -> sum[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S6[i] -> S5[i, -1 + N] : i >= 0 and i <= -1 + N; S3[i] -> S1[i, -1 + N] : i >= 0 and i <= -1 + N; S2[i] -> S1[i, -1 + N] : i >= 0 and i <= -1 + N; S5[i, j] -> S3[i] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N; S4[i] -> S2[-1 + N] : i >= 0 and i <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S3[i] -> [0, i, 3]; S0[i] -> [i, 0, 0]; S6[i] -> [i, N + i, 0]; S5[i, j] -> [i, i + j, 4]; S4[i] -> [0, N + i, 5]; S2[i] -> [0, i, 2]; S1[i, j] -> [i - j, i, 1] }";
		String valueBasedFeautrierSchedule = "[N] -> { S1[i, j] -> [i, j]; S4[i] -> [i, 0]; S6[i] -> [i, 0]; S3[i] -> [i, 0]; S5[i, j] -> [i, j]; S2[i] -> [i, 0]; S0[i] -> [i, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S6[i] -> S5[i, -1 + N] : i >= 0 and i <= -1 + N; S5[i, j] -> S3[i] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N; S4[i] -> S6[-1 + i] : i >= 1 and i <= -1 + N; S4[0] -> S1[-1 + N, -1 + N] : N >= 1; S4[i] -> S5[-1 + i, -1 + N] : i >= 1 and i <= -1 + N; S2[i] -> S1[i, -1 + N] : i >= 0 and i <= -1 + N; S0[i] -> S3[-1 + i] : i >= 1 and i <= -1 + N; S4[i] -> S2[-1 + N] : i >= 0 and i <= -1 + N; S0[i] -> S2[-1 + i] : i >= 1 and i <= -1 + N; S5[i, 0] -> S4[i] : i <= -1 + N and N >= 1 and i >= 0; S2[i] -> S2[-1 + i] : i >= 1 and i <= -1 + N; S1[i, 0] -> S0[i] : i <= -1 + N and N >= 1 and i >= 0; S4[0] -> S3[-1 + N] : N >= 1; S3[i] -> S1[i, j] : i >= 0 and i <= -1 + N and j >= 0 and j <= -1 + N; S5[i, j] -> S5[i, -1 + j] : j >= 1 and j <= -1 + N and i >= 0 and i <= -1 + N; S1[i, j] -> S1[i, -1 + j] : j >= 1 and j <= -1 + N and i >= 0 and i <= -1 + N; S0[i] -> S1[-1 + i, -1 + N] : i >= 1 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S3[i] -> [i, N, 3]; S0[i] -> [i, 0, 1]; S6[i] -> [N + i, N, 0]; S5[i, j] -> [N + i, j, 2]; S4[i] -> [N + i, 0, 1]; S2[i] -> [i, N, 0]; S1[i, j] -> [i, j, 2] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S3[i] -> [i, 2, 0]; S0[i] -> [i, 0, 0]; S6[i] -> [N + i, 2, 0]; S5[i, j] -> [N + i, 1, j]; S4[i] -> [N + i, 0, 0]; S2[i] -> [i, 2, 0]; S1[i, j] -> [i, 1, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #48
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : integrate_predictors_integrate_predictors/scop_0
	 *   block : 
	 *     for : 1 <= l <= loop (stride = 1)
	 *       for : 0 <= i <= n-1 (stride = 1)
	 *         S0 (depth = 2) [px[i][0]=add(add(add(add(add(add(add(add(mul(IntExpr(dm28),px[i][12]),mul(IntExpr(dm27),px[i][11])),mul(IntExpr(dm26),px[i][10])),mul(IntExpr(dm25),px[i][9])),mul(IntExpr(dm24),px[i][8])),mul(IntExpr(dm23),px[i][7])),mul(IntExpr(dm22),px[i][6])),mul(IntExpr(c0),add(px[i][4],px[i][5]))),px[i][2])]
	 *         ([px[i][0]]) = f([px[i][12], px[i][11], px[i][10], px[i][9], px[i][8], px[i][7], px[i][6], px[i][4], px[i][5], px[i][2]])
	 *         (Domain = [loop,n,dm28,dm27,dm26,dm25,dm24,dm23,dm22,c0] -> {S0[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_09_integrate_predictors_GScopRoot_integrate_predictors_scop_0_ () {
		String path = "polymodel_src_livermore_loops_09_integrate_predictors_GScopRoot_integrate_predictors_scop_0_";
		// SCoP Extraction Data
		String domains = "[loop, n, dm28, dm27, dm26, dm25, dm24, dm23, dm22, c0] -> { S0[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop }";
		String idSchedules = "[loop, n, dm28, dm27, dm26, dm25, dm24, dm23, dm22, c0] -> { S0[l, i] -> [0, l, 0, i, 0] }";
		String writes = "[loop, n, dm28, dm27, dm26, dm25, dm24, dm23, dm22, c0] -> { S0[l, i] -> px[i, 0] }";
		String reads = "[loop, n, dm28, dm27, dm26, dm25, dm24, dm23, dm22, c0] -> { S0[l, i] -> px[i, 12]; S0[l, i] -> px[i, 11]; S0[l, i] -> px[i, 10]; S0[l, i] -> px[i, 9]; S0[l, i] -> px[i, 8]; S0[l, i] -> px[i, 7]; S0[l, i] -> px[i, 6]; S0[l, i] -> px[i, 5]; S0[l, i] -> px[i, 4]; S0[l, i] -> px[i, 2] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[loop, n, dm28, dm27, dm26, dm25, dm24, dm23, dm22, c0] -> {  }";
		String valueBasedPlutoSchedule = "[loop, n, dm28, dm27, dm26, dm25, dm24, dm23, dm22, c0] -> { S0[l, i] -> [l, i] }";
		String valueBasedFeautrierSchedule = "[loop, n, dm28, dm27, dm26, dm25, dm24, dm23, dm22, c0] -> { S0[l, i] -> [l, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[loop, n, dm28, dm27, dm26, dm25, dm24, dm23, dm22, c0] -> { S0[l, i] -> S0[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop }";
		String memoryBasedPlutoSchedule = "[loop, n, dm28, dm27, dm26, dm25, dm24, dm23, dm22, c0] -> { S0[l, i] -> [l, i] }";
		String memoryBasedFeautrierSchedule = "[loop, n, dm28, dm27, dm26, dm25, dm24, dm23, dm22, c0] -> { S0[l, i] -> [l, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #49
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : QR_givensreverse_QR_givensreverse/scop_0
	 *   block : 
	 *     for : 0 <= k <= N-2 (stride = 1)
	 *       for : 0 <= i <= N-k-2 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [angle=call vectorize(M[N-i-1][k],M[N-i-2][k])]
	 *           ([angle]) = f([M[N-i-1][k], M[N-i-2][k]])
	 *           (Domain = [N] -> {S0[k,i] : (i >= 0) and (-k-i+N-2 >= 0) and (k >= 0) and (-k+N-2 >= 0)})
	 *           for : k <= j <= N-1 (stride = 1)
	 *             S1 (depth = 3) [call rotate(M[N-i-2][j],M[N-i-1][j],((*SF64)&(M[N-i-2][j])),((*SF64)&(M[N-i-1][j])),angle)]
	 *             ([M[N-i-2][j], M[N-i-1][j]]) = f([M[N-i-2][j], M[N-i-1][j], angle])
	 *             (Domain = [N] -> {S1[k,i,j] : (-k+j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-k-i+N-2 >= 0) and (k >= 0) and (-k+N-2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_qr_trigo_GScopRoot_QR_givensreverse_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_qr_trigo_GScopRoot_QR_givensreverse_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S1[k, i, j] : j >= k and j <= -1 + N and i >= 0 and i <= -2 + N - k and k >= 0 and k <= -2 + N; S0[k, i] : i >= 0 and i <= -2 + N - k and k >= 0 and k <= -2 + N }";
		String idSchedules = "[N] -> { S0[k, i] -> [0, k, 0, i, 0, 0, 0]; S1[k, i, j] -> [0, k, 0, i, 1, j, 0] }";
		String writes = "[N] -> { S1[k, i, j] -> M[-1 + N - i, j]; S1[k, i, j] -> M[-2 + N - i, j]; S0[k, i] -> angle[] }";
		String reads = "[N] -> { S1[k, i, j] -> M[-1 + N - i, j]; S1[k, i, j] -> M[-2 + N - i, j]; S1[k, i, j] -> angle[]; S0[k, i] -> M[-1 + N - i, k]; S0[k, i] -> M[-2 + N - i, k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S1[k, i, j] -> S0[k, i] : j >= k and j <= -1 + N and i >= 0 and i <= -2 + N - k and k >= 0 and k <= -2 + N; S1[k, i, j] -> S1[-1 + k, 1 + i, j] : j >= k and j <= -1 + N and i >= 0 and i <= -2 + N - k and k >= 1; S1[k, i, j] -> S1[k, -1 + i, j] : j >= k and j <= -1 + N and i >= 1 and i <= -2 + N - k and k >= 0; S1[k, 0, j] -> S1[-1 + k, 0, j] : j >= k and j <= -1 + N and k <= -2 + N and k >= 1; S0[k, i] -> S1[-1 + k, 1 + i, k] : i >= 0 and i <= -2 + N - k and k >= 1; S0[k, i] -> S1[k, -1 + i, k] : i >= 1 and i <= -2 + N - k and k >= 0; S0[k, 0] -> S1[-1 + k, 0, k] : k <= -2 + N and k >= 1 }";
		String valueBasedPlutoSchedule = "[N] -> { S0[k, i] -> [k, k + i, k, 0]; S1[k, i, j] -> [k, k + i, j, 1] }";
		String valueBasedFeautrierSchedule = "[N] -> { S0[k, i] -> [2k + i, 0, k, 0]; S1[k, i, j] -> [2k + i, 1, k, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S0[k, i] -> S0[k, -1 + i] : i >= 1 and i <= -2 + N - k and k >= 0; S0[k, 0] -> S0[-1 + k, -1 + N - k] : k <= -2 + N and k >= 1; S1[k, i, j] -> S0[k, i] : k >= 0 and i >= 0 and i <= -2 + N - k and j >= k and j <= -1 + N; S1[k, i, j] -> S1[-1 + k, 1 + i, j] : k >= 1 and i >= 0 and i <= -2 + N - k and j >= k and j <= -1 + N; S1[k, i, j] -> S1[k, -1 + i, j] : k >= 0 and i >= 1 and i <= -2 + N - k and j >= k and j <= -1 + N; S1[k, 0, j] -> S1[-1 + k, 0, j] : k >= 1 and k <= -2 + N and j >= k and j <= -1 + N; S0[k, i] -> S1[k, -1 + i, j] : k >= 0 and i >= 1 and i <= -2 + N - k and j >= k and j <= -1 + N; S0[k, i] -> S1[-1 + k, 1 + i, k] : k >= 1 and i >= 0 and i <= -2 + N - k; S0[k, 0] -> S1[-1 + k, -1 + N - k, j] : k >= 1 and k <= -2 + N and j >= -1 + k and j <= -1 + N; S0[k, 0] -> S1[-1 + k, 0, k] : k >= 1 and k <= -2 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[k, i] -> [k, i, 0, 0]; S1[k, i, j] -> [k, i, j, 1] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S0[k, i] -> [k, i, 0, 0]; S1[k, i, j] -> [k, i, 1, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #50
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : ADI_integration_ADI_integration/scop_0
	 *   block : 
	 *     for : 1 <= l <= loop (stride = 1)
	 *       for : 1 <= kx <= 2 (stride = 1)
	 *         for : 1 <= ky <= n-1 (stride = 1)
	 *           block : 
	 *             S0 (depth = 3) [du1[ky]=sub(u1[0][ky+1][kx],u1[0][ky-1][kx])]
	 *             ([du1[ky]]) = f([u1[0][ky+1][kx], u1[0][ky-1][kx]])
	 *             (Domain = [loop,n,a11,a12,a13,sig,a21,a22,a23,a31,a32,a33] -> {S0[l,kx,ky] : (ky-1 >= 0) and (-ky+n-1 >= 0) and (kx-1 >= 0) and (-kx+2 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *             S1 (depth = 3) [du2[ky]=sub(u2[0][ky+1][kx],u2[0][ky-1][kx])]
	 *             ([du2[ky]]) = f([u2[0][ky+1][kx], u2[0][ky-1][kx]])
	 *             (Domain = [loop,n,a11,a12,a13,sig,a21,a22,a23,a31,a32,a33] -> {S1[l,kx,ky] : (ky-1 >= 0) and (-ky+n-1 >= 0) and (kx-1 >= 0) and (-kx+2 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *             S2 (depth = 3) [du3[ky]=sub(u3[0][ky+1][kx],u3[0][ky-1][kx])]
	 *             ([du3[ky]]) = f([u3[0][ky+1][kx], u3[0][ky-1][kx]])
	 *             (Domain = [loop,n,a11,a12,a13,sig,a21,a22,a23,a31,a32,a33] -> {S2[l,kx,ky] : (ky-1 >= 0) and (-ky+n-1 >= 0) and (kx-1 >= 0) and (-kx+2 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *             S3 (depth = 3) [u1[1][ky][kx]=add(add(add(add(u1[0][ky][kx],mul(IntExpr(a11),du1[ky])),mul(IntExpr(a12),du2[ky])),mul(IntExpr(a13),du3[ky])),mul(IntExpr(sig),add(sub(u1[0][ky][kx+1],mul(2.0,u1[0][ky][kx])),u1[0][ky][kx-1])))]
	 *             ([u1[1][ky][kx]]) = f([u1[0][ky][kx], du1[ky], du2[ky], du3[ky], u1[0][ky][kx+1], u1[0][ky][kx], u1[0][ky][kx-1]])
	 *             (Domain = [loop,n,a11,a12,a13,sig,a21,a22,a23,a31,a32,a33] -> {S3[l,kx,ky] : (ky-1 >= 0) and (-ky+n-1 >= 0) and (kx-1 >= 0) and (-kx+2 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *             S4 (depth = 3) [u2[1][ky][kx]=add(add(add(add(u2[0][ky][kx],mul(IntExpr(a21),du1[ky])),mul(IntExpr(a22),du2[ky])),mul(IntExpr(a23),du3[ky])),mul(IntExpr(sig),add(sub(u2[0][ky][kx+1],mul(2.0,u2[0][ky][kx])),u2[0][ky][kx-1])))]
	 *             ([u2[1][ky][kx]]) = f([u2[0][ky][kx], du1[ky], du2[ky], du3[ky], u2[0][ky][kx+1], u2[0][ky][kx], u2[0][ky][kx-1]])
	 *             (Domain = [loop,n,a11,a12,a13,sig,a21,a22,a23,a31,a32,a33] -> {S4[l,kx,ky] : (ky-1 >= 0) and (-ky+n-1 >= 0) and (kx-1 >= 0) and (-kx+2 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *             S5 (depth = 3) [u3[1][ky][kx]=add(add(add(add(u3[0][ky][kx],mul(IntExpr(a31),du1[ky])),mul(IntExpr(a32),du2[ky])),mul(IntExpr(a33),du3[ky])),mul(IntExpr(sig),add(sub(u3[0][ky][kx+1],mul(2.0,u3[0][ky][kx])),u3[0][ky][kx-1])))]
	 *             ([u3[1][ky][kx]]) = f([u3[0][ky][kx], du1[ky], du2[ky], du3[ky], u3[0][ky][kx+1], u3[0][ky][kx], u3[0][ky][kx-1]])
	 *             (Domain = [loop,n,a11,a12,a13,sig,a21,a22,a23,a31,a32,a33] -> {S5[l,kx,ky] : (ky-1 >= 0) and (-ky+n-1 >= 0) and (kx-1 >= 0) and (-kx+2 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_08_ADI_integration_GScopRoot_ADI_integration_scop_0_ () {
		String path = "polymodel_src_livermore_loops_08_ADI_integration_GScopRoot_ADI_integration_scop_0_";
		// SCoP Extraction Data
		String domains = "[loop, n, a11, a12, a13, sig, a21, a22, a23, a31, a32, a33] -> { S2[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S1[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S5[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S3[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S0[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S4[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop }";
		String idSchedules = "[loop, n, a11, a12, a13, sig, a21, a22, a23, a31, a32, a33] -> { S4[l, kx, ky] -> [0, l, 0, kx, 0, ky, 4]; S1[l, kx, ky] -> [0, l, 0, kx, 0, ky, 1]; S2[l, kx, ky] -> [0, l, 0, kx, 0, ky, 2]; S0[l, kx, ky] -> [0, l, 0, kx, 0, ky, 0]; S5[l, kx, ky] -> [0, l, 0, kx, 0, ky, 5]; S3[l, kx, ky] -> [0, l, 0, kx, 0, ky, 3] }";
		String writes = "[loop, n, a11, a12, a13, sig, a21, a22, a23, a31, a32, a33] -> { S0[l, kx, ky] -> du1[ky]; S1[l, kx, ky] -> du2[ky]; S2[l, kx, ky] -> du3[ky]; S3[l, kx, ky] -> u1[1, ky, kx]; S5[l, kx, ky] -> u3[1, ky, kx]; S4[l, kx, ky] -> u2[1, ky, kx] }";
		String reads = "[loop, n, a11, a12, a13, sig, a21, a22, a23, a31, a32, a33] -> { S4[l, kx, ky] -> du1[ky]; S3[l, kx, ky] -> du2[ky]; S5[l, kx, ky] -> du1[ky]; S5[l, kx, ky] -> du3[ky]; S4[l, kx, ky] -> du2[ky]; S3[l, kx, ky] -> u1[0, ky, 1 + kx]; S3[l, kx, ky] -> u1[0, ky, kx]; S3[l, kx, ky] -> u1[0, ky, -1 + kx]; S5[l, kx, ky] -> u3[0, ky, 1 + kx]; S5[l, kx, ky] -> u3[0, ky, kx]; S5[l, kx, ky] -> u3[0, ky, -1 + kx]; S3[l, kx, ky] -> du1[ky]; S5[l, kx, ky] -> du2[ky]; S2[l, kx, ky] -> u3[0, 1 + ky, kx]; S2[l, kx, ky] -> u3[0, -1 + ky, kx]; S1[l, kx, ky] -> u2[0, 1 + ky, kx]; S1[l, kx, ky] -> u2[0, -1 + ky, kx]; S4[l, kx, ky] -> du3[ky]; S4[l, kx, ky] -> u2[0, ky, 1 + kx]; S4[l, kx, ky] -> u2[0, ky, kx]; S4[l, kx, ky] -> u2[0, ky, -1 + kx]; S3[l, kx, ky] -> du3[ky]; S0[l, kx, ky] -> u1[0, 1 + ky, kx]; S0[l, kx, ky] -> u1[0, -1 + ky, kx] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[loop, n, a11, a12, a13, sig, a21, a22, a23, a31, a32, a33] -> { S3[l, kx, ky] -> S2[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S4[l, kx, ky] -> S0[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S5[l, kx, ky] -> S0[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S5[l, kx, ky] -> S2[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S4[l, kx, ky] -> S2[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S5[l, kx, ky] -> S1[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S4[l, kx, ky] -> S1[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S3[l, kx, ky] -> S1[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S3[l, kx, ky] -> S0[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop }";
		String valueBasedPlutoSchedule = "[loop, n, a11, a12, a13, sig, a21, a22, a23, a31, a32, a33] -> { S2[l, kx, ky] -> [l, kx, ky, 2]; S4[l, kx, ky] -> [l, kx, ky, 3]; S5[l, kx, ky] -> [l, kx, ky, 5]; S1[l, kx, ky] -> [l, kx, ky, 1]; S3[l, kx, ky] -> [l, kx, ky, 4]; S0[l, kx, ky] -> [l, kx, ky, 0] }";
		String valueBasedFeautrierSchedule = "[loop, n, a11, a12, a13, sig, a21, a22, a23, a31, a32, a33] -> { S1[l, kx, ky] -> [l, kx, ky]; S3[l, kx, ky] -> [l, kx, ky]; S5[l, kx, ky] -> [l, kx, ky]; S2[l, kx, ky] -> [l, kx, ky]; S4[l, kx, ky] -> [l, kx, ky]; S0[l, kx, ky] -> [l, kx, ky] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[loop, n, a11, a12, a13, sig, a21, a22, a23, a31, a32, a33] -> { S1[l, 1, ky] -> S1[-1 + l, 2, ky] : ky >= 1 and ky <= -1 + n and l >= 2 and l <= loop; S1[l, 2, ky] -> S1[l, 1, ky] : ky >= 1 and ky <= -1 + n and l >= 1 and l <= loop; S5[l, kx, ky] -> S1[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S4[l, kx, ky] -> S4[-1 + l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 2 and l <= loop; S4[l, kx, ky] -> S2[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S5[l, kx, ky] -> S0[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S3[l, kx, ky] -> S2[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S4[l, kx, ky] -> S0[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S3[l, kx, ky] -> S3[-1 + l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 2 and l <= loop; S2[l, 1, ky] -> S4[-1 + l, 2, ky] : l >= 2 and l <= loop and ky >= 1 and ky <= -1 + n; S2[l, 2, ky] -> S4[l, 1, ky] : l >= 1 and l <= loop and ky >= 1 and ky <= -1 + n; S2[l, 1, ky] -> S3[-1 + l, 2, ky] : l >= 2 and l <= loop and ky >= 1 and ky <= -1 + n; S2[l, 2, ky] -> S3[l, 1, ky] : l >= 1 and l <= loop and ky >= 1 and ky <= -1 + n; S3[l, kx, ky] -> S0[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S2[l, 1, ky] -> S2[-1 + l, 2, ky] : ky >= 1 and ky <= -1 + n and l >= 2 and l <= loop; S2[l, 2, ky] -> S2[l, 1, ky] : ky >= 1 and ky <= -1 + n and l >= 1 and l <= loop; S1[l, 1, ky] -> S4[-1 + l, 2, ky] : l >= 2 and l <= loop and ky >= 1 and ky <= -1 + n; S1[l, 2, ky] -> S4[l, 1, ky] : l >= 1 and l <= loop and ky >= 1 and ky <= -1 + n; S0[l, 1, ky] -> S0[-1 + l, 2, ky] : ky >= 1 and ky <= -1 + n and l >= 2 and l <= loop; S0[l, 2, ky] -> S0[l, 1, ky] : ky >= 1 and ky <= -1 + n and l >= 1 and l <= loop; S1[l, 1, ky] -> S5[-1 + l, 2, ky] : l >= 2 and l <= loop and ky >= 1 and ky <= -1 + n; S1[l, 2, ky] -> S5[l, 1, ky] : l >= 1 and l <= loop and ky >= 1 and ky <= -1 + n; S4[l, kx, ky] -> S1[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S5[l, kx, ky] -> S2[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S0[l, 1, ky] -> S4[-1 + l, 2, ky] : l >= 2 and l <= loop and ky >= 1 and ky <= -1 + n; S0[l, 2, ky] -> S4[l, 1, ky] : l >= 1 and l <= loop and ky >= 1 and ky <= -1 + n; S1[l, 1, ky] -> S3[-1 + l, 2, ky] : l >= 2 and l <= loop and ky >= 1 and ky <= -1 + n; S1[l, 2, ky] -> S3[l, 1, ky] : l >= 1 and l <= loop and ky >= 1 and ky <= -1 + n; S3[l, kx, ky] -> S1[l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 1 and l <= loop; S2[l, 1, ky] -> S5[-1 + l, 2, ky] : l >= 2 and l <= loop and ky >= 1 and ky <= -1 + n; S2[l, 2, ky] -> S5[l, 1, ky] : l >= 1 and l <= loop and ky >= 1 and ky <= -1 + n; S5[l, kx, ky] -> S5[-1 + l, kx, ky] : ky >= 1 and ky <= -1 + n and kx >= 1 and kx <= 2 and l >= 2 and l <= loop; S0[l, 1, ky] -> S3[-1 + l, 2, ky] : l >= 2 and l <= loop and ky >= 1 and ky <= -1 + n; S0[l, 2, ky] -> S3[l, 1, ky] : l >= 1 and l <= loop and ky >= 1 and ky <= -1 + n; S0[l, 1, ky] -> S5[-1 + l, 2, ky] : l >= 2 and l <= loop and ky >= 1 and ky <= -1 + n; S0[l, 2, ky] -> S5[l, 1, ky] : l >= 1 and l <= loop and ky >= 1 and ky <= -1 + n }";
		String memoryBasedPlutoSchedule = "[loop, n, a11, a12, a13, sig, a21, a22, a23, a31, a32, a33] -> { S2[l, kx, ky] -> [l, l + kx, ky, 2]; S4[l, kx, ky] -> [l, l + kx, ky, 3]; S5[l, kx, ky] -> [l, l + kx, ky, 5]; S1[l, kx, ky] -> [l, l + kx, ky, 1]; S3[l, kx, ky] -> [l, l + kx, ky, 4]; S0[l, kx, ky] -> [l, l + kx, ky, 0] }";
		String memoryBasedFeautrierSchedule = "[loop, n, a11, a12, a13, sig, a21, a22, a23, a31, a32, a33] -> { S2[l, kx, ky] -> [2l + kx, 0, l, ky]; S4[l, kx, ky] -> [2l + kx, 1, l, ky]; S5[l, kx, ky] -> [2l + kx, 1, l, ky]; S1[l, kx, ky] -> [2l + kx, 0, l, ky]; S3[l, kx, ky] -> [2l + kx, 1, l, ky]; S0[l, kx, ky] -> [2l + kx, 0, l, ky] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #51
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : FIR_scop_new
	 *   block : 
	 *     S0 (depth = 0) [temp=IntExpr(0)]
	 *     ([temp]) = f([])
	 *     (Domain = [sample] -> {S0[] : })
	 *     S1 (depth = 0) [X[0]=IntExpr(sample)]
	 *     ([X[0]]) = f([])
	 *     (Domain = [sample] -> {S1[] : })
	 *     S2 (depth = 0) [acc=mul(X[3],h[3])]
	 *     ([acc]) = f([X[3], h[3]])
	 *     (Domain = [sample] -> {S2[] : })
	 *     for : 0 <= i <= 2 (stride = -1)
	 *       block : 
	 *         S3 (depth = 1) [acc=add(acc,mul(X[i],h[i]))]
	 *         ([acc]) = f([acc, X[i], h[i]])
	 *         (Domain = [sample] -> {S3[i] : (i >= 0) and (-i+2 >= 0)})
	 *         S4 (depth = 1) [X[i+1]=X[i]]
	 *         ([X[i+1]]) = f([X[i]])
	 *         (Domain = [sample] -> {S4[i] : (i >= 0) and (-i+2 >= 0)})
	 *     S5 (depth = 0) [y=acc]
	 *     ([y]) = f([acc])
	 *     (Domain = [sample] -> {S5[] : })
	 *     for : -7 <= j <= 0 (stride = -1)
	 *       S6 (depth = 1) [temp=add(temp,X[j])]
	 *       ([temp]) = f([temp, X[j]])
	 *       (Domain = [sample] -> {S6[j] : (j+7 >= 0) and (-j >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_forunroller_failingUnrolls2_GScopRoot_scop_new_ () {
		String path = "polymodel_others_forunroller_failingUnrolls2_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[sample] -> { S2[]; S4[i] : i >= 0 and i <= 2; S6[j] : j >= -7 and j <= 0; S0[]; S5[]; S3[i] : i >= 0 and i <= 2; S1[] }";
		String idSchedules = "[sample] -> { S5[] -> [4, 0, 0]; S6[j] -> [5, -j, 0]; S0[] -> [0, 0, 0]; S1[] -> [1, 0, 0]; S4[i] -> [3, -i, 1]; S3[i] -> [3, -i, 0]; S2[] -> [2, 0, 0] }";
		String writes = "[sample] -> { S5[] -> y[]; S2[] -> acc[]; S0[] -> temp[]; S1[] -> X[0]; S4[i] -> X[1 + i]; S3[i] -> acc[]; S6[j] -> temp[] }";
		String reads = "[sample] -> { S6[j] -> temp[]; S3[i] -> h[i]; S6[j] -> X[j]; S2[] -> h[3]; S4[i] -> X[i]; S3[i] -> X[i]; S3[i] -> acc[]; S5[] -> acc[]; S2[] -> X[3] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[sample] -> { S6[0] -> S1[]; S3[i] -> S3[1 + i] : i >= 0 and i <= 1; S4[0] -> S1[]; S6[0] -> S0[]; S5[] -> S3[0]; S3[0] -> S1[]; S3[2] -> S2[]; S6[j] -> S6[1 + j] : j >= -7 and j <= -1 }";
		String valueBasedPlutoSchedule = "[sample] -> { S4[i] -> [i, 6]; S6[j] -> [-j, 5]; S1[] -> [0, 0]; S3[i] -> [2 - i, 2]; S5[] -> [2, 3]; S2[] -> [0, 1]; S0[] -> [0, 4] }";
		String valueBasedFeautrierSchedule = "[sample] -> { S3[i] -> [-i]; S1[] -> [0]; S4[i] -> [i]; S5[] -> [0]; S0[] -> [0]; S6[j] -> [-j]; S2[] -> [0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[sample] -> { S4[2] -> S2[]; S6[0] -> S1[]; S3[i] -> S3[1 + i] : i <= 1 and i >= 0; S4[0] -> S1[]; S4[i] -> S4[1 + i] : i >= 0 and i <= 1; S6[0] -> S0[]; S5[] -> S3[0]; S3[0] -> S1[]; S4[i] -> S3[1 + i] : i >= 0 and i <= 1; S3[2] -> S2[]; S6[j] -> S6[1 + j] : j <= -1 and j >= -7 }";
		String memoryBasedPlutoSchedule = "[sample] -> { S4[i] -> [2 - i, 6]; S6[j] -> [-j, 5]; S1[] -> [0, 0]; S3[i] -> [2 - i, 2]; S5[] -> [2, 3]; S2[] -> [0, 1]; S0[] -> [0, 4] }";
		String memoryBasedFeautrierSchedule = "[sample] -> { S3[i] -> [3 - i]; S1[] -> [0]; S4[i] -> [3 - i]; S5[] -> [4]; S0[] -> [0]; S6[j] -> [1 - j]; S2[] -> [0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #52
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : gauss_C99_I32_gauss_C99_I32/scop_0
	 *   block : 
	 *     for : i0+1 <= i <= i1-1 (stride = 1)
	 *       for : j0+1 <= j <= j1-1 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [Y[i][j]=add(add(X[i-1][j-1],mul(IntExpr(2),X[i-1][j])),X[i-1][j+1])]
	 *           ([Y[i][j]]) = f([X[i-1][j-1], X[i-1][j], X[i-1][j+1]])
	 *           (Domain = [i0,i1,j0,j1] -> {S0[i,j] : (j-j0-1 >= 0) and (-j+j1-1 >= 0) and (i-i0-1 >= 0) and (-i+i1-1 >= 0)})
	 *           S1 (depth = 2) [Y[i][j]=add(Y[i][j],add(add(mul(IntExpr(2),X[i][j-1]),mul(IntExpr(4),X[i][j])),mul(IntExpr(2),X[i][j+1])))]
	 *           ([Y[i][j]]) = f([Y[i][j], X[i][j-1], X[i][j], X[i][j+1]])
	 *           (Domain = [i0,i1,j0,j1] -> {S1[i,j] : (j-j0-1 >= 0) and (-j+j1-1 >= 0) and (i-i0-1 >= 0) and (-i+i1-1 >= 0)})
	 *           S2 (depth = 2) [Y[i][j]=add(Y[i][j],add(add(X[i+1][j-1],mul(IntExpr(2),X[i+1][j])),X[i+1][j+1]))]
	 *           ([Y[i][j]]) = f([Y[i][j], X[i+1][j-1], X[i+1][j], X[i+1][j+1]])
	 *           (Domain = [i0,i1,j0,j1] -> {S2[i,j] : (j-j0-1 >= 0) and (-j+j1-1 >= 0) and (i-i0-1 >= 0) and (-i+i1-1 >= 0)})
	 *           S3 (depth = 2) [Y[i][j]=div(Y[i][j],IntExpr(16))]
	 *           ([Y[i][j]]) = f([Y[i][j]])
	 *           (Domain = [i0,i1,j0,j1] -> {S3[i,j] : (j-j0-1 >= 0) and (-j+j1-1 >= 0) and (i-i0-1 >= 0) and (-i+i1-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_gauss_C99_I32_GScopRoot_gauss_C99_I32_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_gauss_C99_I32_GScopRoot_gauss_C99_I32_scop_0_";
		// SCoP Extraction Data
		String domains = "[i0, i1, j0, j1] -> { S2[i, j] : j >= 1 + j0 and j <= -1 + j1 and i >= 1 + i0 and i <= -1 + i1; S1[i, j] : j >= 1 + j0 and j <= -1 + j1 and i >= 1 + i0 and i <= -1 + i1; S0[i, j] : j >= 1 + j0 and j <= -1 + j1 and i >= 1 + i0 and i <= -1 + i1; S3[i, j] : j >= 1 + j0 and j <= -1 + j1 and i >= 1 + i0 and i <= -1 + i1 }";
		String idSchedules = "[i0, i1, j0, j1] -> { S2[i, j] -> [0, i, 0, j, 2]; S1[i, j] -> [0, i, 0, j, 1]; S3[i, j] -> [0, i, 0, j, 3]; S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[i0, i1, j0, j1] -> { S3[i, j] -> Y[i, j]; S1[i, j] -> Y[i, j]; S0[i, j] -> Y[i, j]; S2[i, j] -> Y[i, j] }";
		String reads = "[i0, i1, j0, j1] -> { S3[i, j] -> Y[i, j]; S0[i, j] -> X[-1 + i, 1 + j]; S0[i, j] -> X[-1 + i, j]; S0[i, j] -> X[-1 + i, -1 + j]; S1[i, j] -> Y[i, j]; S2[i, j] -> X[1 + i, 1 + j]; S2[i, j] -> X[1 + i, j]; S2[i, j] -> X[1 + i, -1 + j]; S1[i, j] -> X[i, 1 + j]; S1[i, j] -> X[i, j]; S1[i, j] -> X[i, -1 + j]; S2[i, j] -> Y[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[i0, i1, j0, j1] -> { S3[i, j] -> S2[i, j] : j >= 1 + j0 and j <= -1 + j1 and i >= 1 + i0 and i <= -1 + i1; S1[i, j] -> S0[i, j] : j >= 1 + j0 and j <= -1 + j1 and i >= 1 + i0 and i <= -1 + i1; S2[i, j] -> S1[i, j] : j >= 1 + j0 and j <= -1 + j1 and i >= 1 + i0 and i <= -1 + i1 }";
		String valueBasedPlutoSchedule = "[i0, i1, j0, j1] -> { S2[i, j] -> [i, j, 2]; S3[i, j] -> [i, j, 3]; S1[i, j] -> [i, j, 1]; S0[i, j] -> [i, j, 0] }";
		String valueBasedFeautrierSchedule = "[i0, i1, j0, j1] -> { S0[i, j] -> [i, j]; S1[i, j] -> [i, j]; S2[i, j] -> [i, j]; S3[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[i0, i1, j0, j1] -> { S3[i, j] -> S2[i, j] : i >= 1 + i0 and i <= -1 + i1 and j >= 1 + j0 and j <= -1 + j1; S1[i, j] -> S0[i, j] : i >= 1 + i0 and i <= -1 + i1 and j >= 1 + j0 and j <= -1 + j1; S2[i, j] -> S1[i, j] : i >= 1 + i0 and i <= -1 + i1 and j >= 1 + j0 and j <= -1 + j1 }";
		String memoryBasedPlutoSchedule = "[i0, i1, j0, j1] -> { S2[i, j] -> [i, j, 2]; S3[i, j] -> [i, j, 3]; S1[i, j] -> [i, j, 1]; S0[i, j] -> [i, j, 0] }";
		String memoryBasedFeautrierSchedule = "[i0, i1, j0, j1] -> { S0[i, j] -> [i, j]; S1[i, j] -> [i, j]; S2[i, j] -> [i, j]; S3[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #53
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : forward_substitution_original_forward_substitution_original/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [x[i]=b[i]]
	 *         ([x[i]]) = f([b[i]])
	 *         (Domain = [N] -> {S0[i] : (i >= 0) and (-i+N-1 >= 0)})
	 *         for : 0 <= j <= i-1 (stride = 1)
	 *           S1 (depth = 2) [x[i]=sub(x[i],mul(L[i][j],x[j]))]
	 *           ([x[i]]) = f([x[i], L[i][j], x[j]])
	 *           (Domain = [N] -> {S1[i,j] : (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 *         S2 (depth = 1) [x[i]=div(x[i],L[i][i])]
	 *         ([x[i]]) = f([x[i], L[i][i]])
	 *         (Domain = [N] -> {S2[i] : (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_openMP_ForwardSubstitution_GScopRoot_forward_substitution_original_scop_0_ () {
		String path = "polymodel_others_openMP_ForwardSubstitution_GScopRoot_forward_substitution_original_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[i] : i >= 0 and i <= -1 + N; S1[i, j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N; S2[i] : i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S2[i] -> [0, i, 2, 0, 0]; S1[i, j] -> [0, i, 1, j, 0]; S0[i] -> [0, i, 0, 0, 0] }";
		String writes = "[N] -> { S1[i, j] -> x[i]; S0[i] -> x[i]; S2[i] -> x[i] }";
		String reads = "[N] -> { S0[i] -> b[i]; S1[i, j] -> L[i, j]; S1[i, j] -> x[i]; S1[i, j] -> x[j]; S2[i] -> L[i, i]; S2[i] -> x[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S1[i, j] -> S2[j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N; S1[i, j] -> S1[i, -1 + j] : i <= -1 + N and j <= -1 + i and j >= 1 and i >= 0; S2[0] -> S0[0] : N >= 1; S2[i] -> S1[i, -1 + i] : i >= 1 and i <= -1 + N; S1[i, 0] -> S0[i] : i <= -1 + N and i >= 1 }";
		String valueBasedPlutoSchedule = "[N] -> { S0[i] -> [0, i, 0]; S2[i] -> [i, 2i, 1]; S1[i, j] -> [i, i + j, 2] }";
		String valueBasedFeautrierSchedule = "[N] -> { S1[i, j] -> [1 + i + j, j]; S2[i] -> [1 + 2i, 0]; S0[i] -> [0, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S1[i, j] -> S2[j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N; S1[i, j] -> S1[i, -1 + j] : i <= -1 + N and j >= 1 and j <= -1 + i; S2[0] -> S0[0] : N >= 1; S2[i] -> S1[i, -1 + i] : i >= 1 and i <= -1 + N; S1[i, 0] -> S0[i] : i >= 1 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[i] -> [0, i, 0]; S2[i] -> [i, 2i, 1]; S1[i, j] -> [i, i + j, 2] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S1[i, j] -> [1 + i + j, j]; S2[i] -> [1 + 2i, 0]; S0[i] -> [0, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #54
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : intputDomainTest_intputDomainTest/scop_0
	 *   block : 
	 *     if : (M > 0) and (N > 0) and (P > 0)
	 *       for : 0 <= i <= M-1 (stride = 1)
	 *         for : 0 <= j <= i-1 (stride = 1)
	 *           for : 0 <= k <= P-1 (stride = 1)
	 *             block : 
	 *               S0 (depth = 3) [toto=add(A[i][k],B[k][j])]
	 *               ([toto]) = f([A[i][k], B[k][j]])
	 *               (Domain = [M,N,P] -> {S0[i,j,k] : (k >= 0) and (-k+P-1 >= 0) and (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+M-1 >= 0) and (M-1 >= 0) and (N-1 >= 0) and (P-1 >= 0)})
	 *               S1 (depth = 3) [res[i][j]=toto]
	 *               ([res[i][j]]) = f([toto])
	 *               (Domain = [M,N,P] -> {S1[i,j,k] : (k >= 0) and (-k+P-1 >= 0) and (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+M-1 >= 0) and (M-1 >= 0) and (N-1 >= 0) and (P-1 >= 0)})
	 *     if : (M > 10) and (P <= 7)
	 *       for : 0 <= i <= M-1 (stride = 1)
	 *         for : i <= j <= N-1 (stride = 1)
	 *           for : 0 <= k <= P-1 (stride = 1)
	 *             S2 (depth = 3) [res[i][j]=add(sub(A[i][k],B[k][j]),toto)]
	 *             ([res[i][j]]) = f([A[i][k], B[k][j], toto])
	 *             (Domain = [M,N,P] -> {S2[i,j,k] : (k >= 0) and (-k+P-1 >= 0) and (-i+j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+M-1 >= 0) and (M-11 >= 0) and (-P+7 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_InputDomainTest_GScopRoot_intputDomainTest_scop_0_ () {
		String path = "polymodel_src_corner_cases_InputDomainTest_GScopRoot_intputDomainTest_scop_0_";
		// SCoP Extraction Data
		String domains = "[M, N, P] -> { S1[i, j, k] : k >= 0 and k <= -1 + P and j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + M and M >= 1 and N >= 1 and P >= 1; S2[i, j, k] : k >= 0 and k <= -1 + P and j >= i and j <= -1 + N and i >= 0 and i <= -1 + M and M >= 11 and P <= 7; S0[i, j, k] : k >= 0 and k <= -1 + P and j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + M and M >= 1 and N >= 1 and P >= 1 }";
		String idSchedules = "[M, N, P] -> { S1[i, j, k] -> [0, i, 0, j, 0, k, 1]; S0[i, j, k] -> [0, i, 0, j, 0, k, 0]; S2[i, j, k] -> [1, i, 0, j, 0, k, 0] }";
		String writes = "[M, N, P] -> { S2[i, j, k] -> res[i, j]; S0[i, j, k] -> toto[]; S1[i, j, k] -> res[i, j] }";
		String reads = "[M, N, P] -> { S2[i, j, k] -> B[k, j]; S1[i, j, k] -> toto[]; S0[i, j, k] -> A[i, k]; S2[i, j, k] -> toto[]; S2[i, j, k] -> A[i, k]; S0[i, j, k] -> B[k, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[M, N, P] -> { S2[i, j, k] -> S0[-1 + M, -2 + M, -1 + P] : k >= 0 and k <= -1 + P and j >= i and j <= -1 + N and i >= 0 and i <= -1 + M and M >= 11 and P <= 7; S1[i, j, k] -> S0[i, j, k] : k >= 0 and k <= -1 + P and j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + M and M >= 1 and N >= 1 and P >= 1 }";
		String valueBasedPlutoSchedule = "[M, N, P] -> { S1[i, j, k] -> [i, j, k, 2]; S0[i, j, k] -> [i - j, j, k, 0]; S2[i, j, k] -> [1 + i, M + j, 6 + k, 1] }";
		String valueBasedFeautrierSchedule = "[M, N, P] -> { S2[i, j, k] -> [i, j, k]; S0[i, j, k] -> [i, j, k]; S1[i, j, k] -> [i, j, k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[M, N, P] -> { S0[i, j, k] -> S1[i, j, -1 + k] : N >= 1 and i <= -1 + M and j >= 0 and j <= -1 + i and k >= 1 and k <= -1 + P; S0[i, j, 0] -> S1[i, -1 + j, -1 + P] : N >= 1 and P >= 1 and i <= -1 + M and j >= 1 and j <= -1 + i; S0[i, 0, 0] -> S1[-1 + i, -2 + i, -1 + P] : N >= 1 and P >= 1 and i >= 2 and i <= -1 + M; S1[i, j, k] -> S0[i, j, k] : k >= 0 and k <= -1 + P and j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + M and M >= 1 and N >= 1 and P >= 1; S2[i, j, k] -> S0[-1 + M, -2 + M, -1 + P] : k >= 0 and k <= -1 + P and j >= i and j <= -1 + N and i >= 0 and i <= -1 + M and M >= 11 and P <= 7; S0[i, j, k] -> S0[i, j, -1 + k] : k >= 1 and k <= -1 + P and j >= 0 and j <= -1 + i and N >= 1 and i <= -1 + M; S0[i, j, 0] -> S0[i, -1 + j, -1 + P] : P >= 1 and N >= 1 and i <= -1 + M and j <= -1 + i and j >= 1; S0[i, 0, 0] -> S0[-1 + i, -2 + i, -1 + P] : P >= 1 and N >= 1 and i <= -1 + M and i >= 2; S2[i, j, k] -> S2[i, j, -1 + k] : k >= 1 and k <= -1 + P and j >= i and j <= -1 + N and i >= 0 and i <= -1 + M and M >= 11 and P <= 7; S1[i, j, k] -> S1[i, j, -1 + k] : k >= 1 and k <= -1 + P and j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + M and M >= 1 and N >= 1 and P >= 1 }";
		String memoryBasedPlutoSchedule = "[M, N, P] -> { S1[i, j, k] -> [i, j, k, 1]; S0[i, j, k] -> [i, j, k, 0]; S2[i, j, k] -> [M + i, j, k, 0] }";
		String memoryBasedFeautrierSchedule = "[M, N, P] -> { S1[i, j, k] -> [i, j, k, 1]; S0[i, j, k] -> [i, j, k, 0]; S2[i, j, k] -> [M + k, j, i, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #55
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : durbin_durbin/scop_0
	 *   block : 
	 *     S0 (depth = 0) [y[0][0]=r[0]]
	 *     ([y[0][0]]) = f([r[0]])
	 *     (Domain = [N] -> {S0[] : })
	 *     S1 (depth = 0) [beta[0]=IntExpr(1)]
	 *     ([beta[0]]) = f([])
	 *     (Domain = [N] -> {S1[] : })
	 *     S2 (depth = 0) [alpha[0]=r[0]]
	 *     ([alpha[0]]) = f([r[0]])
	 *     (Domain = [N] -> {S2[] : })
	 *     for : 1 <= k <= N-1 (stride = 1)
	 *       block : 
	 *         S3 (depth = 1) [beta[k]=sub(beta[k-1],mul(mul(beta[k-1],alpha[k-1]),alpha[k-1]))]
	 *         ([beta[k]]) = f([beta[k-1], beta[k-1], alpha[k-1], alpha[k-1]])
	 *         (Domain = [N] -> {S3[k] : (k-1 >= 0) and (-k+N-1 >= 0)})
	 *         S4 (depth = 1) [sum[0][k]=r[k]]
	 *         ([sum[0][k]]) = f([r[k]])
	 *         (Domain = [N] -> {S4[k] : (k-1 >= 0) and (-k+N-1 >= 0)})
	 *         for : 0 <= i <= k-1 (stride = 1)
	 *           S5 (depth = 2) [sum[i+1][k]=add(sum[i][k],mul(r[k-i-1],y[i][k-1]))]
	 *           ([sum[i+1][k]]) = f([sum[i][k], r[k-i-1], y[i][k-1]])
	 *           (Domain = [N] -> {S5[k,i] : (i >= 0) and (k-i-1 >= 0) and (k-1 >= 0) and (-k+N-1 >= 0)})
	 *         S6 (depth = 1) [alpha[k]=mul(neg(sum[k][k]),beta[k])]
	 *         ([alpha[k]]) = f([sum[k][k], beta[k]])
	 *         (Domain = [N] -> {S6[k] : (k-1 >= 0) and (-k+N-1 >= 0)})
	 *         for : 0 <= i <= k-1 (stride = 1)
	 *           S7 (depth = 2) [y[i][k]=add(y[i][k-1],mul(alpha[k],y[k-i-1][k-1]))]
	 *           ([y[i][k]]) = f([y[i][k-1], alpha[k], y[k-i-1][k-1]])
	 *           (Domain = [N] -> {S7[k,i] : (i >= 0) and (k-i-1 >= 0) and (k-1 >= 0) and (-k+N-1 >= 0)})
	 *         S8 (depth = 1) [y[k][k]=alpha[k]]
	 *         ([y[k][k]]) = f([alpha[k]])
	 *         (Domain = [N] -> {S8[k] : (k-1 >= 0) and (-k+N-1 >= 0)})
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       S9 (depth = 1) [out[i]=y[i][N-1]]
	 *       ([out[i]]) = f([y[i][N-1]])
	 *       (Domain = [N] -> {S9[i] : (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_durbin_GScopRoot_durbin_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_durbin_GScopRoot_durbin_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S9[i] : i >= 0 and i <= -1 + N; S3[k] : k >= 1 and k <= -1 + N; S4[k] : k >= 1 and k <= -1 + N; S7[k, i] : i >= 0 and i <= -1 + k and k >= 1 and k <= -1 + N; S0[]; S2[]; S1[]; S5[k, i] : i >= 0 and i <= -1 + k and k >= 1 and k <= -1 + N; S8[k] : k >= 1 and k <= -1 + N; S6[k] : k >= 1 and k <= -1 + N }";
		String idSchedules = "[N] -> { S1[] -> [1, 0, 0, 0, 0]; S3[k] -> [3, k, 0, 0, 0]; S0[] -> [0, 0, 0, 0, 0]; S4[k] -> [3, k, 1, 0, 0]; S6[k] -> [3, k, 3, 0, 0]; S2[] -> [2, 0, 0, 0, 0]; S8[k] -> [3, k, 5, 0, 0]; S7[k, i] -> [3, k, 4, i, 0]; S9[i] -> [4, i, 0, 0, 0]; S5[k, i] -> [3, k, 2, i, 0] }";
		String writes = "[N] -> { S6[k] -> alpha[k]; S9[i] -> out[i]; S3[k] -> beta[k]; S8[k] -> y[k, k]; S1[] -> beta[0]; S2[] -> alpha[0]; S0[] -> y[0, 0]; S7[k, i] -> y[i, k]; S4[k] -> sum[0, k]; S5[k, i] -> sum[1 + i, k] }";
		String reads = "[N] -> { S5[k, i] -> r[-1 + k - i]; S9[i] -> y[i, -1 + N]; S2[] -> r[0]; S6[k] -> beta[k]; S3[k] -> beta[-1 + k]; S3[k] -> alpha[-1 + k]; S0[] -> r[0]; S5[k, i] -> y[i, -1 + k]; S7[k, i] -> y[i, -1 + k]; S7[k, i] -> y[-1 + k - i, -1 + k]; S4[k] -> r[k]; S5[k, i] -> sum[i, k]; S6[k] -> sum[k, k]; S7[k, i] -> alpha[k]; S8[k] -> alpha[k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S9[i] -> S7[-1 + N, i] : i >= 0 and i <= -2 + N; S7[k, i] -> S7[-1 + k, i] : (k <= -1 + N and i >= 0 and 2i <= -2 + k) or (k <= -1 + N and 2i >= k and i <= -2 + k); S7[k, i] -> S7[-1 + k, -1 + k - i] : k <= -1 + N and i <= -1 + k and i >= 1; S3[1] -> S2[] : N >= 2; S7[1, 0] -> S0[] : N >= 2; S6[k] -> S5[k, -1 + k] : k >= 1 and k <= -1 + N; S9[-1 + N] -> S8[-1 + N] : N >= 2; S5[k, -1 + k] -> S8[-1 + k] : k >= 2 and k <= -1 + N; S7[k, 0] -> S8[-1 + k] : k >= 2 and k <= -1 + N; S7[k, -1 + k] -> S8[-1 + k] : k >= 2 and k <= -1 + N; S5[k, 0] -> S4[k] : k <= -1 + N and k >= 1; S3[1] -> S1[] : N >= 2; S6[k] -> S3[k] : k >= 1 and k <= -1 + N; S3[k] -> S6[-1 + k] : k >= 2 and k <= -1 + N; S3[k] -> S3[-1 + k] : k >= 2 and k <= -1 + N; S8[k] -> S6[k] : k >= 1 and k <= -1 + N; S5[k, i] -> S7[-1 + k, i] : i >= 0 and k <= -1 + N and i <= -2 + k and k >= 1; S5[k, i] -> S5[k, -1 + i] : k <= -1 + N and i <= -1 + k and i >= 1 and k >= 1; S7[k, i] -> S6[k] : i >= 0 and i <= -1 + k and k >= 1 and k <= -1 + N; S9[0] -> S0[] : N = 1; S5[1, 0] -> S0[] : N >= 2 }";
		String valueBasedPlutoSchedule = "[N] -> { S0[] -> [0, 0, 0]; S7[k, i] -> [k, i, 4]; S3[k] -> [k, 0, 0]; S6[k] -> [k, 0, 1]; S5[k, i] -> [k, -k + i, 3]; S1[] -> [0, 0, 0]; S4[k] -> [0, k, 0]; S2[] -> [0, 0, 0]; S9[i] -> [N, i, 0]; S8[k] -> [k, 0, 2] }";
		String valueBasedFeautrierSchedule = "[N] -> { S0[] -> [0, 0, 0]; S7[k, i] -> [k, 2, i]; S3[k] -> [k, 0, 0]; S6[k] -> [k, 1, 0]; S5[k, i] -> [k, 0, i]; S1[] -> [0, 0, 0]; S4[k] -> [0, 0, k]; S2[] -> [0, 0, 0]; S9[i] -> [N, 0, i]; S8[k] -> [k, 2, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S9[i] -> S7[-1 + N, i] : i >= 0 and i <= -2 + N; S7[k, i] -> S7[-1 + k, i] : (k <= -1 + N and i >= 0 and 2i <= -2 + k) or (k <= -1 + N and 2i >= k and i <= -2 + k); S7[k, i] -> S7[-1 + k, -1 + k - i] : k <= -1 + N and i <= -1 + k and i >= 1; S3[1] -> S2[] : N >= 2; S7[1, 0] -> S0[] : N >= 2; S6[k] -> S5[k, -1 + k] : k >= 1 and k <= -1 + N; S9[-1 + N] -> S8[-1 + N] : N >= 2; S5[k, -1 + k] -> S8[-1 + k] : k >= 2 and k <= -1 + N; S7[k, 0] -> S8[-1 + k] : k >= 2 and k <= -1 + N; S7[k, -1 + k] -> S8[-1 + k] : k >= 2 and k <= -1 + N; S5[k, 0] -> S4[k] : k <= -1 + N and k >= 1; S3[1] -> S1[] : N >= 2; S6[k] -> S3[k] : k >= 1 and k <= -1 + N; S3[k] -> S6[-1 + k] : k >= 2 and k <= -1 + N; S3[k] -> S3[-1 + k] : k >= 2 and k <= -1 + N; S8[k] -> S6[k] : k >= 1 and k <= -1 + N; S5[k, i] -> S7[-1 + k, i] : i >= 0 and k <= -1 + N and i <= -2 + k and k >= 1; S5[k, i] -> S5[k, -1 + i] : k <= -1 + N and i <= -1 + k and i >= 1 and k >= 1; S7[k, i] -> S6[k] : i >= 0 and i <= -1 + k and k >= 1 and k <= -1 + N; S9[0] -> S0[] : N = 1; S5[1, 0] -> S0[] : N >= 2 }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[] -> [0, 0, 0]; S7[k, i] -> [k, i, 4]; S3[k] -> [k, 0, 0]; S6[k] -> [k, 0, 1]; S5[k, i] -> [k, -k + i, 3]; S1[] -> [0, 0, 0]; S4[k] -> [0, k, 0]; S2[] -> [0, 0, 0]; S9[i] -> [N, i, 0]; S8[k] -> [k, 0, 2] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S0[] -> [0, 0, 0]; S7[k, i] -> [k, 2, i]; S3[k] -> [k, 0, 0]; S6[k] -> [k, 1, 0]; S5[k, i] -> [k, 0, i]; S1[] -> [0, 0, 0]; S4[k] -> [0, 0, k]; S2[] -> [0, 0, 0]; S9[i] -> [N, 0, i]; S8[k] -> [k, 2, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #56
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_jacobi_2d_imper_kernel_jacobi_2d_imper/scop_0
	 *   for : 0 <= t <= tsteps-1 (stride = 1)
	 *     block : 
	 *       for : 1 <= i <= n-2 (stride = 1)
	 *         for : 1 <= j <= n-2 (stride = 1)
	 *           S0 (depth = 3) [B[i][j]=mul(0.20000000298023224,add(add(add(add(A[i][j],A[i][j-1]),A[i][j+1]),A[i+1][j]),A[i-1][j]))]
	 *           ([B[i][j]]) = f([A[i][j], A[i][j-1], A[i][j+1], A[i+1][j], A[i-1][j]])
	 *           (Domain = [tsteps,n] -> {S0[t,i,j] : (j-1 >= 0) and (-j+n-2 >= 0) and (i-1 >= 0) and (-i+n-2 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *       for : 1 <= i <= n-2 (stride = 1)
	 *         for : 1 <= j <= n-2 (stride = 1)
	 *           S1 (depth = 3) [A[i][j]=B[i][j]]
	 *           ([A[i][j]]) = f([B[i][j]])
	 *           (Domain = [tsteps,n] -> {S1[t,i,j] : (j-1 >= 0) and (-j+n-2 >= 0) and (i-1 >= 0) and (-i+n-2 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_stencils_jacobi_2d_imper_jacobi_2d_imper_GScopRoot_kernel_jacobi_2d_imper_scop_0_ () {
		String path = "polybench_gecos_stencils_jacobi_2d_imper_jacobi_2d_imper_GScopRoot_kernel_jacobi_2d_imper_scop_0_";
		// SCoP Extraction Data
		String domains = "[tsteps, n] -> { S1[t, i, j] : j >= 1 and j <= -2 + n and i >= 1 and i <= -2 + n and t >= 0 and t <= -1 + tsteps; S0[t, i, j] : j >= 1 and j <= -2 + n and i >= 1 and i <= -2 + n and t >= 0 and t <= -1 + tsteps }";
		String idSchedules = "[tsteps, n] -> { S0[t, i, j] -> [0, t, 0, i, 0, j, 0]; S1[t, i, j] -> [0, t, 1, i, 0, j, 0] }";
		String writes = "[tsteps, n] -> { S0[t, i, j] -> B[i, j]; S1[t, i, j] -> A[i, j] }";
		String reads = "[tsteps, n] -> { S0[t, i, j] -> A[i, 1 + j]; S0[t, i, j] -> A[1 + i, j]; S0[t, i, j] -> A[i, j]; S0[t, i, j] -> A[-1 + i, j]; S0[t, i, j] -> A[i, -1 + j]; S1[t, i, j] -> B[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[tsteps, n] -> { S1[t, i, j] -> S0[t, i, j] : j >= 1 and j <= -2 + n and i >= 1 and i <= -2 + n and t >= 0 and t <= -1 + tsteps; S0[t, i, j] -> S1[-1 + t, i', j'] : j' >= 1 and i' >= 1 and t >= 1 and t <= -1 + tsteps and j <= -2 + n and j' >= -1 + i + j - i' and i <= -2 + n and j' >= -1 - i + j + i' and i >= 1 and j >= 1 and j' <= 1 + i + j - i' and j' <= -2 + n and j' <= 1 - i + j + i' and i' <= -2 + n }";
		String valueBasedPlutoSchedule = "[tsteps, n] -> { S0[t, i, j] -> [t, t + i, t + i + j, 0]; S1[t, i, j] -> [t, t + i, t + i + j, 1] }";
		String valueBasedFeautrierSchedule = "[tsteps, n] -> { S0[t, i, j] -> [t, 0, i, j]; S1[t, i, j] -> [t, 1, i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[tsteps, n] -> { S1[t, i, j] -> S0[t, i', j'] : t <= -1 + tsteps and i' <= -2 + n and j' <= -2 + n and j >= 1 and i >= 1 and j' <= 1 + i + j - i' and i' >= 1 and j' >= -1 - i + j + i' and t >= 0 and j' <= 1 - i + j + i' and j <= -2 + n and j' >= 1 and i <= -2 + n and j' >= -1 + i + j - i'; S1[t, i, j] -> S1[-1 + t, i, j] : t >= 1 and t <= -1 + tsteps and i >= 1 and i <= -2 + n and j >= 1 and j <= -2 + n; S0[t, i, j] -> S0[-1 + t, i, j] : j >= 1 and j <= -2 + n and i >= 1 and i <= -2 + n and t >= 1 and t <= -1 + tsteps; S0[t, i, j] -> S1[-1 + t, i', j'] : t >= 1 and t <= -1 + tsteps and i' <= -2 + n and i >= 1 and j >= 1 and j' <= 1 + i + j - i' and j' <= -2 + n and i <= -2 + n and j' <= 1 - i + j + i' and j' >= -1 + i + j - i' and j' >= 1 and j' >= -1 - i + j + i' and j <= -2 + n and i' >= 1 }";
		String memoryBasedPlutoSchedule = "[tsteps, n] -> { S0[t, i, j] -> [t, 2t + i, 2t + i + j, 0]; S1[t, i, j] -> [t, 1 + 2t + i, 1 + 2t + i + j, 1] }";
		String memoryBasedFeautrierSchedule = "[tsteps, n] -> { S0[t, i, j] -> [t, 0, i, j]; S1[t, i, j] -> [t, 1, i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #57
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : first_difference_first_difference/scop_0
	 *   block : 
	 *     for : 1 <= l <= loop (stride = 1)
	 *       for : 0 <= k <= n-1 (stride = 1)
	 *         S0 (depth = 2) [x[k]=sub(y[k+1],y[k])]
	 *         ([x[k]]) = f([y[k+1], y[k]])
	 *         (Domain = [loop,n] -> {S0[l,k] : (k >= 0) and (-k+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_12_first_difference_GScopRoot_first_difference_scop_0_ () {
		String path = "polymodel_src_livermore_loops_12_first_difference_GScopRoot_first_difference_scop_0_";
		// SCoP Extraction Data
		String domains = "[loop, n] -> { S0[l, k] : k >= 0 and k <= -1 + n and l >= 1 and l <= loop }";
		String idSchedules = "[loop, n] -> { S0[l, k] -> [0, l, 0, k, 0] }";
		String writes = "[loop, n] -> { S0[l, k] -> x[k] }";
		String reads = "[loop, n] -> { S0[l, k] -> y[1 + k]; S0[l, k] -> y[k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[loop, n] -> {  }";
		String valueBasedPlutoSchedule = "[loop, n] -> { S0[l, k] -> [l, k] }";
		String valueBasedFeautrierSchedule = "[loop, n] -> { S0[l, k] -> [l, k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[loop, n] -> { S0[l, k] -> S0[-1 + l, k] : k >= 0 and k <= -1 + n and l >= 2 and l <= loop }";
		String memoryBasedPlutoSchedule = "[loop, n] -> { S0[l, k] -> [l, k] }";
		String memoryBasedFeautrierSchedule = "[loop, n] -> { S0[l, k] -> [l, k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #58
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : filtre_filtre/scop_0
	 *   block : 
	 *     for : 0 <= i <= n (stride = 1)
	 *       for : i <= j <= n-i (stride = 1)
	 *         for : 0 <= k <= n-i+j (stride = 1)
	 *           S0 (depth = 3) [a[i][j+k-i][i]=IntExpr(3)]
	 *           ([a[i][j+k-i][i]]) = f([])
	 *           (Domain = [n] -> {S0[i,j,k] : (k >= 0) and (-i+j-k+n >= 0) and (-i+j >= 0) and (-i-j+n >= 0) and (i >= 0) and (-i+n >= 0)})
	 *     for : 0 <= i <= n (stride = 1)
	 *       for : i <= j <= n (stride = 1)
	 *         for : 0 <= k <= n (stride = 1)
	 *           S1 (depth = 3) [x=a[i][j][k]]
	 *           ([x]) = f([a[i][j][k]])
	 *           (Domain = [n] -> {S1[i,j,k] : (k >= 0) and (-k+n >= 0) and (-i+j >= 0) and (-j+n >= 0) and (i >= 0) and (-i+n >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_filtre_GScopRoot_filtre_scop_0_ () {
		String path = "polymodel_src_corner_cases_filtre_GScopRoot_filtre_scop_0_";
		// SCoP Extraction Data
		String domains = "[n] -> { S0[i, j, k] : k >= 0 and k <= n - i + j and j >= i and j <= n - i and i >= 0 and i <= n; S1[i, j, k] : k >= 0 and k <= n and j >= i and j <= n and i >= 0 and i <= n }";
		String idSchedules = "[n] -> { S0[i, j, k] -> [0, i, 0, j, 0, k, 0]; S1[i, j, k] -> [1, i, 0, j, 0, k, 0] }";
		String writes = "[n] -> { S0[i, j, k] -> a[i, -i + j + k, i]; S1[i, j, k] -> x[] }";
		String reads = "[n] -> { S1[i, j, k] -> a[i, j, k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S1[i, j, i] -> S0[i, i + j, 0] : i >= 0 and j <= n - 2i and j >= i; S1[i, j, i] -> S0[i, n - i, -n + 2i + j] : 2i <= n and j >= 1 + n - 2i and j >= i and j <= n }";
		String valueBasedPlutoSchedule = "[n] -> { S0[i, j, k] -> [i, j, k, 0]; S1[i, j, k] -> [k, i + j, 2i, 1] }";
		String valueBasedFeautrierSchedule = "[n] -> { S0[i, j, k] -> [i, j, k]; S1[i, j, k] -> [i, j, k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S0[i, j, k] -> S0[i, -1 + j, 1 + k] : k >= 0 and k <= -2 + n - i + j and j >= 1 + i and j <= n - i and i >= 0 and i <= n; S1[i, j, i] -> S0[i, i + j, 0] : i >= 0 and j <= n - 2i and j >= i; S1[i, j, i] -> S0[i, n - i, -n + 2i + j] : 2i <= n and j >= 1 + n - 2i and j >= i and j <= n; S1[i, j, k] -> S1[i, j, -1 + k] : k >= 1 and k <= n and j >= i and j <= n and i >= 0; S1[i, j, 0] -> S1[i, -1 + j, n] : j <= n and i >= 0 and j >= 1 + i; S1[i, i, 0] -> S1[-1 + i, n, n] : i <= n and i >= 1 }";
		String memoryBasedPlutoSchedule = "[n] -> { S0[i, j, k] -> [i, j, k, 0]; S1[i, j, k] -> [i, 2j, k, 1] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S0[i, j, k] -> [-k, j, i]; S1[i, j, k] -> [1 + i, j, k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #59
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : main_main/scop_0
	 *   block : 
	 *     for : 0 <= i <= 9 (stride = 1)
	 *       if : i > 5
	 *         block : 
	 *           S0 (depth = 1) [x=IntExpr(4)]
	 *           ([x]) = f([])
	 *           (Domain = [] -> {S0[i] : (i-6 >= 0) and (-i+9 >= 0)})
	 *           S1 (depth = 1) [a[i]=a[i-1]]
	 *           ([a[i]]) = f([a[i-1]])
	 *           (Domain = [] -> {S1[i] : (i-6 >= 0) and (-i+9 >= 0)})
	 *       else
	 *         block : 
	 *           S2 (depth = 1) [x=IntExpr(6)]
	 *           ([x]) = f([])
	 *           (Domain = [] -> {S2[i] : (-i+5 >= 0) and (i >= 0)})
	 *           S3 (depth = 1) [a[i]=a[i-2]]
	 *           ([a[i]]) = f([a[i-2]])
	 *           (Domain = [] -> {S3[i] : (-i+5 >= 0) and (i >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_ifInsideFor_GScopRoot_main_scop_0_ () {
		String path = "polymodel_src_corner_cases_ifInsideFor_GScopRoot_main_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S0[i] : i >= 6 and i <= 9; S3[i] : i <= 5 and i >= 0; S1[i] : i >= 6 and i <= 9; S2[i] : i <= 5 and i >= 0 }";
		String idSchedules = "{ S3[i] -> [0, i, 3]; S0[i] -> [0, i, 0]; S2[i] -> [0, i, 2]; S1[i] -> [0, i, 1] }";
		String writes = "{ S2[i] -> x[]; S0[i] -> x[]; S3[i] -> a[i]; S1[i] -> a[i] }";
		String reads = "{ S3[i] -> a[-2 + i]; S1[i] -> a[-1 + i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S3[i] -> S3[-2 + i] : i <= 5 and i >= 2; S1[6] -> S3[5]; S1[i] -> S1[-1 + i] : i >= 7 and i <= 9 }";
		String valueBasedPlutoSchedule = "{ S1[i] -> [i, 0]; S3[i] -> [i, 1]; S2[i] -> [i, 0]; S0[i] -> [i, 0] }";
		String valueBasedFeautrierSchedule = "{ S2[i] -> [i]; S3[i] -> [i]; S1[i] -> [2i]; S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S1[6] -> S3[5]; S0[i] -> S0[-1 + i] : i >= 7 and i <= 9; S0[6] -> S2[5]; S2[i] -> S2[-1 + i] : i <= 5 and i >= 1; S1[i] -> S1[-1 + i] : i >= 7 and i <= 9; S3[i] -> S3[-2 + i] : i <= 5 and i >= 2 }";
		String memoryBasedPlutoSchedule = "{ S1[i] -> [i, 0]; S3[i] -> [i, 1]; S2[i] -> [i, 0]; S0[i] -> [i, 1] }";
		String memoryBasedFeautrierSchedule = "{ S2[i] -> [i]; S3[i] -> [i]; S1[i] -> [2i]; S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #60
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_gemver_scop_new
	 *   block : 
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       for : 0 <= j <= n-1 (stride = 1)
	 *         S0 (depth = 2) [A[i][j]=add(add(A[i][j],mul(u1[i],v1[j])),mul(u2[i],v2[j]))]
	 *         ([A[i][j]]) = f([A[i][j], u1[i], v1[j], u2[i], v2[j]])
	 *         (Domain = [n,beta,alpha] -> {S0[i,j] : (j >= 0) and (-j+n-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       for : 0 <= j <= n-1 (stride = 1)
	 *         S1 (depth = 2) [x[i]=add(x[i],mul(mul(IntExpr(beta),A[j][i]),y[j]))]
	 *         ([x[i]]) = f([x[i], A[j][i], y[j]])
	 *         (Domain = [n,beta,alpha] -> {S1[i,j] : (j >= 0) and (-j+n-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       S2 (depth = 1) [x[i]=add(x[i],z[i])]
	 *       ([x[i]]) = f([x[i], z[i]])
	 *       (Domain = [n,beta,alpha] -> {S2[i] : (i >= 0) and (-i+n-1 >= 0)})
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       for : 0 <= j <= n-1 (stride = 1)
	 *         S3 (depth = 2) [w[i]=add(w[i],mul(mul(IntExpr(alpha),A[i][j]),x[j]))]
	 *         ([w[i]]) = f([w[i], A[i][j], x[j]])
	 *         (Domain = [n,beta,alpha] -> {S3[i,j] : (j >= 0) and (-j+n-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_gemver_gemver_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_linear_algebra_kernels_gemver_gemver_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[n, beta, alpha] -> { S1[i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n; S0[i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n; S2[i] : i >= 0 and i <= -1 + n; S3[i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n }";
		String idSchedules = "[n, beta, alpha] -> { S3[i, j] -> [3, i, 0, j, 0]; S2[i] -> [2, i, 0, 0, 0]; S0[i, j] -> [0, i, 0, j, 0]; S1[i, j] -> [1, i, 0, j, 0] }";
		String writes = "[n, beta, alpha] -> { S2[i] -> x[i]; S3[i, j] -> w[i]; S0[i, j] -> A[i, j]; S1[i, j] -> x[i] }";
		String reads = "[n, beta, alpha] -> { S2[i] -> z[i]; S2[i] -> x[i]; S1[i, j] -> A[j, i]; S3[i, j] -> A[i, j]; S3[i, j] -> x[j]; S0[i, j] -> u2[i]; S3[i, j] -> w[i]; S0[i, j] -> A[i, j]; S0[i, j] -> v2[j]; S0[i, j] -> u1[i]; S1[i, j] -> x[i]; S0[i, j] -> v1[j]; S1[i, j] -> y[j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n, beta, alpha] -> { S2[i] -> S1[i, -1 + n] : i >= 0 and i <= -1 + n; S1[i, j] -> S0[j, i] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n; S3[i, j] -> S0[i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n; S1[i, j] -> S1[i, -1 + j] : j >= 1 and j <= -1 + n and i >= 0 and i <= -1 + n; S3[i, j] -> S2[j] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n; S3[i, j] -> S3[i, -1 + j] : j >= 1 and j <= -1 + n and i >= 0 and i <= -1 + n }";
		String valueBasedPlutoSchedule = "[n, beta, alpha] -> { S3[i, j] -> [j, n + i + j, 1]; S1[i, j] -> [i, i + j, 3]; S2[i] -> [i, n + i, 0]; S0[i, j] -> [-n + i, j, 2] }";
		String valueBasedFeautrierSchedule = "[n, beta, alpha] -> { S1[i, j] -> [j, i]; S3[i, j] -> [j, i]; S0[i, j] -> [i, j]; S2[i] -> [i, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n, beta, alpha] -> { S2[i] -> S1[i, -1 + n] : i >= 0 and i <= -1 + n; S1[i, j] -> S0[j, i] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n; S3[i, j] -> S0[i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n; S1[i, j] -> S1[i, -1 + j] : i >= 0 and i <= -1 + n and j >= 1 and j <= -1 + n; S3[i, j] -> S2[j] : j >= 0 and j <= -1 + n and i >= 0 and i <= -1 + n; S3[i, j] -> S3[i, -1 + j] : i >= 0 and i <= -1 + n and j >= 1 and j <= -1 + n }";
		String memoryBasedPlutoSchedule = "[n, beta, alpha] -> { S3[i, j] -> [j, n + i + j, 1]; S1[i, j] -> [i, i + j, 3]; S2[i] -> [i, n + i, 0]; S0[i, j] -> [-n + i, j, 2] }";
		String memoryBasedFeautrierSchedule = "[n, beta, alpha] -> { S1[i, j] -> [j, i]; S3[i, j] -> [j, i]; S0[i, j] -> [i, j]; S2[i] -> [i, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #61
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : general_linear_recurrence_equations_general_linear_recurrence_equations/scop_0
	 *   block : 
	 *     for : 1 <= l <= loop (stride = 1)
	 *       for : 1 <= i <= n-1 (stride = 1)
	 *         for : 0 <= k <= i-1 (stride = 1)
	 *           S0 (depth = 3) [w[i]=add(w[i],mul(b[k][i],w[i-k-1]))]
	 *           ([w[i]]) = f([w[i], b[k][i], w[i-k-1]])
	 *           (Domain = [loop,n] -> {S0[l,i,k] : (k >= 0) and (i-k-1 >= 0) and (i-1 >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_06_general_linear_recurrence_equations_GScopRoot_general_linear_recurrence_equations_scop_0_ () {
		String path = "polymodel_src_livermore_loops_06_general_linear_recurrence_equations_GScopRoot_general_linear_recurrence_equations_scop_0_";
		// SCoP Extraction Data
		String domains = "[loop, n] -> { S0[l, i, k] : k >= 0 and k <= -1 + i and i >= 1 and i <= -1 + n and l >= 1 and l <= loop }";
		String idSchedules = "[loop, n] -> { S0[l, i, k] -> [0, l, 0, i, 0, k, 0] }";
		String writes = "[loop, n] -> { S0[l, i, k] -> w[i] }";
		String reads = "[loop, n] -> { S0[l, i, k] -> b[k, i]; S0[l, i, k] -> w[i]; S0[l, i, k] -> w[-1 + i - k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[loop, n] -> { S0[l, i, k] -> S0[l, i, -1 + k] : l <= loop and k <= -1 + i and k >= 1 and i <= -1 + n and l >= 1; S0[l, i, k] -> S0[l, -1 + i - k, -2 + i - k] : k >= 0 and l <= loop and k <= -2 + i and i <= -1 + n and l >= 1; S0[l, i, 0] -> S0[-1 + l, i, -1 + i] : l <= loop and i >= 1 and l >= 2 and i <= -1 + n }";
		String valueBasedPlutoSchedule = "[loop, n] -> { S0[l, i, k] -> [l, i, k] }";
		String valueBasedFeautrierSchedule = "[loop, n] -> { S0[l, i, k] -> [l + i, k, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[loop, n] -> { S0[l, i, k] -> S0[l, i, -1 + k] : l >= 1 and l <= loop and i <= -1 + n and k >= 1 and k <= -1 + i; S0[l, i, 0] -> S0[-1 + l, i', -1 - i + i'] : l >= 2 and l <= loop and i' <= -1 + n and i' >= 1 + i and i >= 1; S0[l, i, k] -> S0[l, -1 + i - k, -2 + i - k] : l >= 1 and l <= loop and k <= -2 + i and k >= 0 and i <= -1 + n; S0[l, i, 0] -> S0[-1 + l, i, -1 + i] : l >= 2 and l <= loop and i >= 1 and i <= -1 + n }";
		String memoryBasedPlutoSchedule = "[loop, n] -> { S0[l, i, k] -> [l, i, k] }";
		String memoryBasedFeautrierSchedule = "[loop, n] -> { S0[l, i, k] -> [l, i, k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #62
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : fir_shuffle_scop_new
	 *   block : 
	 *     S0 (depth = 0) [z[0]=IntExpr(input)]
	 *     ([z[0]]) = f([])
	 *     (Domain = [input,ntaps] -> {S0[] : })
	 *     S1 (depth = 0) [accum=mul(h[ntaps-1],z[ntaps-1])]
	 *     ([accum]) = f([h[ntaps-1], z[ntaps-1]])
	 *     (Domain = [input,ntaps] -> {S1[] : })
	 *     for : 0 <= ii <= ntaps-2 (stride = -1)
	 *       block : 
	 *         S2 (depth = 1) [accum=add(accum,mul(h[ii],z[ii]))]
	 *         ([accum]) = f([accum, h[ii], z[ii]])
	 *         (Domain = [input,ntaps] -> {S2[ii] : (ii >= 0) and (-ii+ntaps-2 >= 0)})
	 *         S3 (depth = 1) [z[ii+1]=z[ii]]
	 *         ([z[ii+1]]) = f([z[ii]])
	 *         (Domain = [input,ntaps] -> {S3[ii] : (ii >= 0) and (-ii+ntaps-2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_algos_FirAlgs_GScopRoot_scop_new_ () {
		String path = "basics_algos_FirAlgs_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[input, ntaps] -> { S0[]; S3[ii] : ii >= 0 and ii <= -2 + ntaps; S1[]; S2[ii] : ii >= 0 and ii <= -2 + ntaps }";
		String idSchedules = "[input, ntaps] -> { S1[] -> [1, 0, 0]; S3[ii] -> [2, -ii, 1]; S0[] -> [0, 0, 0]; S2[ii] -> [2, -ii, 0] }";
		String writes = "[input, ntaps] -> { S0[] -> z[0]; S1[] -> accum[]; S2[ii] -> accum[]; S3[ii] -> z[1 + ii] }";
		String reads = "[input, ntaps] -> { S1[] -> z[-1 + ntaps]; S1[] -> h[-1 + ntaps]; S2[ii] -> accum[]; S2[ii] -> z[ii]; S3[ii] -> z[ii]; S2[ii] -> h[ii] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[input, ntaps] -> { S1[] -> S0[] : ntaps = 1; S2[0] -> S0[] : ntaps >= 2; S2[-2 + ntaps] -> S1[] : ntaps >= 2; S3[0] -> S0[] : ntaps >= 2; S2[ii] -> S2[1 + ii] : ii >= 0 and ii <= -3 + ntaps }";
		String valueBasedPlutoSchedule = "[input, ntaps] -> { S0[] -> [0, 1]; S2[ii] -> [ntaps - ii, 0]; S3[ii] -> [ii, 3]; S1[] -> [0, 2] }";
		String valueBasedFeautrierSchedule = "[input, ntaps] -> { S1[] -> [0]; S3[ii] -> [ii]; S0[] -> [0]; S2[ii] -> [-ii] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[input, ntaps] -> { S1[] -> S0[] : ntaps = 1; S3[ii] -> S3[1 + ii] : ii >= 0 and ii <= -3 + ntaps; S2[-2 + ntaps] -> S1[] : ntaps >= 2; S2[ii] -> S2[1 + ii] : ii >= 0 and ii <= -3 + ntaps; S3[-2 + ntaps] -> S1[] : ntaps >= 2; S3[0] -> S0[] : ntaps >= 2; S2[0] -> S0[] : ntaps >= 2; S3[ii] -> S2[1 + ii] : ii >= 0 and ii <= -3 + ntaps }";
		String memoryBasedPlutoSchedule = "[input, ntaps] -> { S0[] -> [0, 0]; S2[ii] -> [-ii, 1]; S3[ii] -> [-ii, 3]; S1[] -> [1 - ntaps, 2] }";
		String memoryBasedFeautrierSchedule = "[input, ntaps] -> { S1[] -> [2 - ntaps]; S3[ii] -> [1 - ii]; S0[] -> [0]; S2[ii] -> [1 - ii] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #63
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : fir_basic_scop_new
	 *   block : 
	 *     S0 (depth = 0) [z[0]=IntExpr(input)]
	 *     ([z[0]]) = f([])
	 *     (Domain = [input,ntaps] -> {S0[] : })
	 *     S1 (depth = 0) [accum=IntExpr(0)]
	 *     ([accum]) = f([])
	 *     (Domain = [input,ntaps] -> {S1[] : })
	 *     for : 0 <= ii <= ntaps-1 (stride = 1)
	 *       S2 (depth = 1) [accum=add(accum,mul(h[ii],z[ii]))]
	 *       ([accum]) = f([accum, h[ii], z[ii]])
	 *       (Domain = [input,ntaps] -> {S2[ii] : (ii >= 0) and (-ii+ntaps-1 >= 0)})
	 *     for : 0 <= ii <= ntaps-2 (stride = -1)
	 *       S3 (depth = 1) [z[ii+1]=z[ii]]
	 *       ([z[ii+1]]) = f([z[ii]])
	 *       (Domain = [input,ntaps] -> {S3[ii] : (ii >= 0) and (-ii+ntaps-2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_algos_FirAlgs_GScopRoot_scop_new__0 () {
		String path = "basics_algos_FirAlgs_GScopRoot_scop_new__0";
		// SCoP Extraction Data
		String domains = "[input, ntaps] -> { S0[]; S3[ii] : ii >= 0 and ii <= -2 + ntaps; S1[]; S2[ii] : ii >= 0 and ii <= -1 + ntaps }";
		String idSchedules = "[input, ntaps] -> { S1[] -> [1, 0, 0]; S3[ii] -> [3, -ii, 0]; S0[] -> [0, 0, 0]; S2[ii] -> [2, ii, 0] }";
		String writes = "[input, ntaps] -> { S0[] -> z[0]; S1[] -> accum[]; S2[ii] -> accum[]; S3[ii] -> z[1 + ii] }";
		String reads = "[input, ntaps] -> { S2[ii] -> accum[]; S2[ii] -> z[ii]; S3[ii] -> z[ii]; S2[ii] -> h[ii] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[input, ntaps] -> { S2[0] -> S0[] : ntaps >= 1; S2[0] -> S1[] : ntaps >= 1; S3[0] -> S0[] : ntaps >= 2; S2[ii] -> S2[-1 + ii] : ii >= 1 and ii <= -1 + ntaps }";
		String valueBasedPlutoSchedule = "[input, ntaps] -> { S0[] -> [0, 1]; S2[ii] -> [ii, 2]; S3[ii] -> [ii, 3]; S1[] -> [0, 0] }";
		String valueBasedFeautrierSchedule = "[input, ntaps] -> { S1[] -> [0]; S3[ii] -> [ii]; S0[] -> [0]; S2[ii] -> [ii] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[input, ntaps] -> { S3[ii] -> S3[1 + ii] : ii >= 0 and ii <= -3 + ntaps; S2[0] -> S1[] : ntaps >= 1; S2[ii] -> S2[-1 + ii] : ii >= 1 and ii <= -1 + ntaps; S3[0] -> S0[] : ntaps >= 2; S2[0] -> S0[] : ntaps >= 1; S3[ii] -> S2[1 + ii] : ii >= 0 and ii <= -2 + ntaps }";
		String memoryBasedPlutoSchedule = "[input, ntaps] -> { S0[] -> [0, 1]; S2[ii] -> [ii, 2]; S3[ii] -> [2ntaps - ii, 3]; S1[] -> [0, 0] }";
		String memoryBasedFeautrierSchedule = "[input, ntaps] -> { S1[] -> [0]; S3[ii] -> [2ntaps - ii]; S0[] -> [0]; S2[ii] -> [1 + ii] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #64
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : main_main/scop_0
	 *   for : 0 <= ii <= 5 (stride = 1)
	 *     block : 
	 *       S0 (depth = 1) [h2[ii+6]=h[ii]]
	 *       ([h2[ii+6]]) = f([h[ii]])
	 *       (Domain = [] -> {S0[ii] : (ii >= 0) and (-ii+5 >= 0)})
	 *       S1 (depth = 1) [h2[ii]=h2[ii+6]]
	 *       ([h2[ii]]) = f([h2[ii+6]])
	 *       (Domain = [] -> {S1[ii] : (ii >= 0) and (-ii+5 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_algos_FirAlgs_GScopRoot_main_scop_0_ () {
		String path = "basics_algos_FirAlgs_GScopRoot_main_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S0[ii] : ii >= 0 and ii <= 5; S1[ii] : ii >= 0 and ii <= 5 }";
		String idSchedules = "{ S0[ii] -> [0, ii, 0]; S1[ii] -> [0, ii, 1] }";
		String writes = "{ S1[ii] -> h2[ii]; S0[ii] -> h2[6 + ii] }";
		String reads = "{ S1[ii] -> h2[6 + ii]; S0[ii] -> h[ii] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S1[ii] -> S0[ii] : ii >= 0 and ii <= 5 }";
		String valueBasedPlutoSchedule = "{ S1[ii] -> [ii, 1]; S0[ii] -> [ii, 0] }";
		String valueBasedFeautrierSchedule = "{ S1[ii] -> [ii]; S0[ii] -> [ii] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S1[ii] -> S0[ii] : ii >= 0 and ii <= 5 }";
		String memoryBasedPlutoSchedule = "{ S1[ii] -> [ii, 1]; S0[ii] -> [ii, 0] }";
		String memoryBasedFeautrierSchedule = "{ S1[ii] -> [ii]; S0[ii] -> [ii] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #65
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : clear_clear/scop_0
	 *   block : 
	 *     for : 0 <= ii <= ntaps-1 (stride = 1)
	 *       S0 (depth = 1) [z[ii]=IntExpr(0)]
	 *       ([z[ii]]) = f([])
	 *       (Domain = [ntaps] -> {S0[ii] : (ii >= 0) and (-ii+ntaps-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_algos_FirAlgs_GScopRoot_clear_scop_0_ () {
		String path = "basics_algos_FirAlgs_GScopRoot_clear_scop_0_";
		// SCoP Extraction Data
		String domains = "[ntaps] -> { S0[ii] : ii >= 0 and ii <= -1 + ntaps }";
		String idSchedules = "[ntaps] -> { S0[ii] -> [0, ii, 0] }";
		String writes = "[ntaps] -> { S0[ii] -> z[ii] }";
		String reads = "[ntaps] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[ntaps] -> {  }";
		String valueBasedPlutoSchedule = "[ntaps] -> { S0[ii] -> [ii] }";
		String valueBasedFeautrierSchedule = "[ntaps] -> { S0[ii] -> [ii] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[ntaps] -> {  }";
		String memoryBasedPlutoSchedule = "[ntaps] -> { S0[ii] -> [ii] }";
		String memoryBasedFeautrierSchedule = "[ntaps] -> { S0[ii] -> [ii] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #66
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_ludcmp_scop_new
	 *   block : 
	 *     S0 (depth = 0) [b[0]=1.0]
	 *     ([b[0]]) = f([])
	 *     (Domain = [n] -> {S0[] : })
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       block : 
	 *         for : i+1 <= j <= n (stride = 1)
	 *           block : 
	 *             S1 (depth = 2) [w=A[j][i]]
	 *             ([w]) = f([A[j][i]])
	 *             (Domain = [n] -> {S1[i,j] : (-i+j-1 >= 0) and (-j+n >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *             for : 0 <= k <= i-1 (stride = 1)
	 *               S2 (depth = 3) [w=sub(w,mul(A[j][k],A[k][i]))]
	 *               ([w]) = f([w, A[j][k], A[k][i]])
	 *               (Domain = [n] -> {S2[i,j,k] : (k >= 0) and (i-k-1 >= 0) and (-i+j-1 >= 0) and (-j+n >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *             S3 (depth = 2) [A[j][i]=div(w,A[i][i])]
	 *             ([A[j][i]]) = f([w, A[i][i]])
	 *             (Domain = [n] -> {S3[i,j] : (-i+j-1 >= 0) and (-j+n >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *         for : i+1 <= j <= n (stride = 1)
	 *           block : 
	 *             S4 (depth = 2) [w=A[i+1][j]]
	 *             ([w]) = f([A[i+1][j]])
	 *             (Domain = [n] -> {S4[i,j] : (-i+j-1 >= 0) and (-j+n >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *             for : 0 <= k <= i (stride = 1)
	 *               S5 (depth = 3) [w=sub(w,mul(A[i+1][k],A[k][j]))]
	 *               ([w]) = f([w, A[i+1][k], A[k][j]])
	 *               (Domain = [n] -> {S5[i,j,k] : (k >= 0) and (i-k >= 0) and (-i+j-1 >= 0) and (-j+n >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *             S6 (depth = 2) [A[i+1][j]=w]
	 *             ([A[i+1][j]]) = f([w])
	 *             (Domain = [n] -> {S6[i,j] : (-i+j-1 >= 0) and (-j+n >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *     S7 (depth = 0) [y[0]=b[0]]
	 *     ([y[0]]) = f([b[0]])
	 *     (Domain = [n] -> {S7[] : })
	 *     for : 1 <= i <= n (stride = 1)
	 *       block : 
	 *         S8 (depth = 1) [w=b[i]]
	 *         ([w]) = f([b[i]])
	 *         (Domain = [n] -> {S8[i] : (i-1 >= 0) and (-i+n >= 0)})
	 *         for : 0 <= j <= i-1 (stride = 1)
	 *           S9 (depth = 2) [w=sub(w,mul(A[i][j],y[j]))]
	 *           ([w]) = f([w, A[i][j], y[j]])
	 *           (Domain = [n] -> {S9[i,j] : (j >= 0) and (i-j-1 >= 0) and (i-1 >= 0) and (-i+n >= 0)})
	 *         S10 (depth = 1) [y[i]=w]
	 *         ([y[i]]) = f([w])
	 *         (Domain = [n] -> {S10[i] : (i-1 >= 0) and (-i+n >= 0)})
	 *     S11 (depth = 0) [x[n]=div(y[n],A[n][n])]
	 *     ([x[n]]) = f([y[n], A[n][n]])
	 *     (Domain = [n] -> {S11[] : })
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       block : 
	 *         S12 (depth = 1) [w=y[n-i-1]]
	 *         ([w]) = f([y[n-i-1]])
	 *         (Domain = [n] -> {S12[i] : (i >= 0) and (-i+n-1 >= 0)})
	 *         for : n-i <= j <= n (stride = 1)
	 *           S13 (depth = 2) [w=sub(w,mul(A[n-i-1][j],x[j]))]
	 *           ([w]) = f([w, A[n-i-1][j], x[j]])
	 *           (Domain = [n] -> {S13[i,j] : (i+j-n >= 0) and (-j+n >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *         S14 (depth = 1) [x[n-i-1]=div(w,A[n-i-1][n-i-1])]
	 *         ([x[n-i-1]]) = f([w, A[n-i-1][n-i-1]])
	 *         (Domain = [n] -> {S14[i] : (i >= 0) and (-i+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_solvers_ludcmp_ludcmp_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_linear_algebra_solvers_ludcmp_ludcmp_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[n] -> { S1[i, j] : j >= 1 + i and j <= n and i >= 0 and i <= -1 + n; S0[]; S12[i] : i >= 0 and i <= -1 + n; S13[i, j] : j >= n - i and j <= n and i >= 0 and i <= -1 + n; S7[]; S9[i, j] : j >= 0 and j <= -1 + i and i >= 1 and i <= n; S10[i] : i >= 1 and i <= n; S8[i] : i >= 1 and i <= n; S4[i, j] : j >= 1 + i and j <= n and i >= 0 and i <= -1 + n; S3[i, j] : j >= 1 + i and j <= n and i >= 0 and i <= -1 + n; S5[i, j, k] : k >= 0 and k <= i and j >= 1 + i and j <= n and i >= 0 and i <= -1 + n; S6[i, j] : j >= 1 + i and j <= n and i >= 0 and i <= -1 + n; S11[]; S2[i, j, k] : k >= 0 and k <= -1 + i and j >= 1 + i and j <= n and i >= 0 and i <= -1 + n; S14[i] : i >= 0 and i <= -1 + n }";
		String idSchedules = "[n] -> { S1[i, j] -> [1, i, 0, j, 0, 0, 0]; S4[i, j] -> [1, i, 1, j, 0, 0, 0]; S0[] -> [0, 0, 0, 0, 0, 0, 0]; S7[] -> [2, 0, 0, 0, 0, 0, 0]; S8[i] -> [3, i, 0, 0, 0, 0, 0]; S13[i, j] -> [5, i, 1, j, 0, 0, 0]; S6[i, j] -> [1, i, 1, j, 2, 0, 0]; S9[i, j] -> [3, i, 1, j, 0, 0, 0]; S10[i] -> [3, i, 2, 0, 0, 0, 0]; S11[] -> [4, 0, 0, 0, 0, 0, 0]; S12[i] -> [5, i, 0, 0, 0, 0, 0]; S5[i, j, k] -> [1, i, 1, j, 1, k, 0]; S2[i, j, k] -> [1, i, 0, j, 1, k, 0]; S14[i] -> [5, i, 2, 0, 0, 0, 0]; S3[i, j] -> [1, i, 0, j, 2, 0, 0] }";
		String writes = "[n] -> { S6[i, j] -> A[1 + i, j]; S5[i, j, k] -> w[]; S11[] -> x[n]; S12[i] -> w[]; S8[i] -> w[]; S10[i] -> y[i]; S2[i, j, k] -> w[]; S14[i] -> x[-1 + n - i]; S3[i, j] -> A[j, i]; S0[] -> b[0]; S9[i, j] -> w[]; S4[i, j] -> w[]; S13[i, j] -> w[]; S1[i, j] -> w[]; S7[] -> y[0] }";
		String reads = "[n] -> { S13[i, j] -> A[-1 + n - i, j]; S13[i, j] -> x[j]; S14[i] -> A[-1 + n - i, -1 + n - i]; S9[i, j] -> y[j]; S5[i, j, k] -> w[]; S5[i, j, k] -> A[k, j]; S5[i, j, k] -> A[1 + i, k]; S7[] -> b[0]; S4[i, j] -> A[1 + i, j]; S14[i] -> w[]; S10[i] -> w[]; S12[i] -> y[-1 + n - i]; S1[i, j] -> A[j, i]; S2[i, j, k] -> w[]; S2[i, j, k] -> A[k, i]; S2[i, j, k] -> A[j, k]; S3[i, j] -> A[i, i]; S9[i, j] -> w[]; S9[i, j] -> A[i, j]; S11[] -> A[n, n]; S6[i, j] -> w[]; S11[] -> y[n]; S13[i, j] -> w[]; S3[i, j] -> w[]; S8[i] -> b[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S2[i, j, k] -> S3[k, j] : k >= 0 and k <= -1 + i and j >= 1 + i and j <= n and i >= 0 and i <= -1 + n; S10[i] -> S9[i, -1 + i] : i >= 1 and i <= n; S11[] -> S7[] : n = 0; S14[i] -> S6[-2 + n - i, -1 + n - i] : i >= 0 and i <= -2 + n; S13[i, j] -> S14[-1 + n - j] : j >= n - i and i <= -1 + n and j <= -1 + n and i >= 0; S9[i, j] -> S10[j] : i <= n and j <= -1 + i and j >= 1 and i >= 1; S13[i, j] -> S13[i, -1 + j] : i <= -1 + n and j <= n and j >= 1 + n - i and i >= 0; S14[i] -> S13[i, n] : i >= 0 and i <= -1 + n; S13[i, n - i] -> S12[i] : i <= -1 + n and i >= 0; S9[i, 0] -> S7[] : i <= n and i >= 1; S2[i, j, 0] -> S1[i, j] : i >= 1 and j <= n and j >= 1 + i and i <= -1 + n; S6[i, j] -> S5[i, j, i] : j >= 1 + i and j <= n and i >= 0 and i <= -1 + n; S11[] -> S6[-1 + n, n] : n >= 1; S12[i] -> S10[-1 + n - i] : i >= 0 and i <= -2 + n; S5[i, j, k] -> S5[i, j, -1 + k] : k >= 1 and k <= i and j >= 1 + i and j <= n and i <= -1 + n and i >= 0; S11[] -> S10[n] : n >= 1; S5[i, j, k] -> S6[-1 + k, j] : k >= 1 and k <= i and j >= 1 + i and j <= n and i <= -1 + n and i >= 0; S5[i, j, k] -> S3[k, 1 + i] : k >= 0 and k <= i and j >= 1 + i and j <= n; S5[i, j, 0] -> S4[i, j] : i >= 0 and j <= n and j >= 1 + i and i <= -1 + n; S3[i, j] -> S2[i, j, -1 + i] : j >= 1 + i and j <= n and i >= 1 and i <= -1 + n; S2[i, j, k] -> S6[-1 + k, i] : k >= 1 and k <= -1 + i and j >= 1 + i and j <= n and i <= -1 + n and i >= 0; S13[i, j] -> S6[-2 + n - i, j] : j >= n - i and j <= n and i >= 0 and i <= -2 + n; S9[i, 0] -> S8[i] : i <= n and i >= 1; S9[i, j] -> S3[j, i] : j >= 0 and j <= -1 + i and i >= 1 and i <= n; S13[i, n] -> S11[] : i >= 0 and i <= -1 + n; S2[i, j, k] -> S2[i, j, -1 + k] : k >= 1 and k <= -1 + i and j >= 1 + i and j <= n and i <= -1 + n and i >= 0; S12[-1 + n] -> S7[] : n >= 1; S3[0, j] -> S1[0, j] : j >= 1 and j <= n and n >= 1; S9[i, j] -> S9[i, -1 + j] : i <= n and j <= -1 + i and j >= 1 and i >= 1; S3[i, j] -> S6[-1 + i, i] : j >= 1 + i and j <= n and i >= 1 and i <= -1 + n; S7[] -> S0[] }";
		String valueBasedPlutoSchedule = "[n] -> { S1[i, j] -> [0, i, j, 0]; S7[] -> [0, 0, 0, 5]; S9[i, j] -> [i, j, 0, 2]; S4[i, j] -> [0, i, j, 0]; S11[] -> [n, n, 0, 6]; S0[] -> [0, 0, 0, 4]; S2[i, j, k] -> [j, i, -i + k, 0]; S6[i, j] -> [1 + i, j, 0, 2]; S12[i] -> [n, n, i, 8]; S13[i, j] -> [n, n + i, j, 0]; S10[i] -> [i, i, 0, 3]; S5[i, j, k] -> [1 + i, j, -j + k, 7]; S8[i] -> [0, 0, i, 0]; S3[i, j] -> [j, i, 0, 1]; S14[i] -> [n, n + i, n, 1] }";
		String valueBasedFeautrierSchedule = "[n] -> { S7[] -> [1, 0, 0]; S11[] -> [2 + 3n, 0, 0]; S0[] -> [0, 0, 0]; S13[i, j] -> [3 + 3n + 2i, j, 0]; S4[i, j] -> [0, i, j]; S8[i] -> [0, i, 0]; S9[i, j] -> [2i + j, i, 0]; S10[i] -> [3i, 0, 0]; S12[i] -> [3n, i, 0]; S5[i, j, k] -> [1 + j + 2k, i, k]; S6[i, j] -> [2 + 2i + j, i, 0]; S14[i] -> [4 + 3n + 2i, 0, 0]; S1[i, j] -> [0, i, j]; S2[i, j, k] -> [i + j + k, j, k]; S3[i, j] -> [2i + j, i, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S12[i] -> S14[-1 + i] : i >= 1 and i <= -1 + n; S5[i, j, k] -> S5[i, j, -1 + k] : j >= 1 + i and j <= n and k >= 1 and k <= i; S13[i, j] -> S13[i, -1 + j] : i <= -1 + n and j >= 1 + n - i and j <= n; S4[i, j] -> S5[i, -1 + j, i] : j >= 2 + i and j <= n and i >= 0 and i <= -1 + n; S1[0, j] -> S1[0, -1 + j] : j >= 2 and j <= n and n >= 1; S5[i, j, 0] -> S4[i, j] : i >= 0 and j >= 1 + i and j <= n; S13[i, j] -> S14[-1 + n - j] : j >= n - i and i <= -1 + n and j <= -1 + n and i >= 0; S1[i, 1 + i] -> S6[-1 + i, n] : i >= 1 and i <= -1 + n; S14[i] -> S13[i, n] : i >= 0 and i <= -1 + n; S2[i, j, 0] -> S1[i, j] : i >= 1 and j >= 1 + i and j <= n; S5[i, j, k] -> S3[k, 1 + i] : k >= 0 and k <= i and j >= 1 + i and j <= n; S3[i, j] -> S6[-1 + i, i] : j >= 1 + i and j <= n and i >= 1 and i <= -1 + n; S2[i, j, k] -> S3[k, j] : k >= 0 and k <= -1 + i and j >= 1 + i and j <= n and i >= 0 and i <= -1 + n; S11[] -> S6[-1 + n, n] : n >= 1; S12[i] -> S13[-1 + i, n] : i >= 1 and i <= -1 + n; S14[i] -> S6[-2 + n - i, -1 + n - i] : i >= 0 and i <= -2 + n; S4[i, j] -> S6[i, -1 + j] : i >= 0 and j >= 2 + i and j <= n and i <= -1 + n; S8[1] -> S5[-1 + n, n, -1 + n] : n >= 1; S8[1] -> S6[-1 + n, n] : n >= 1; S12[i] -> S10[-1 + n - i] : i <= -2 + n and i >= 0; S12[0] -> S10[n] : n >= 1; S13[i, n - i] -> S12[i] : i >= 0 and i <= -1 + n; S8[i] -> S9[-1 + i, -2 + i] : i >= 2 and i <= n; S6[i, j] -> S5[i, j, i] : j >= 1 + i and j <= n and i >= 0 and i <= -1 + n; S8[i] -> S10[-1 + i] : i >= 2 and i <= n; S4[i, 1 + i] -> S2[i, n, -1 + i] : i >= 1 and i <= -1 + n; S9[i, j] -> S3[j, i] : j >= 0 and j <= -1 + i and i >= 1 and i <= n; S13[i, n] -> S11[] : i >= 0 and i <= -1 + n; S7[] -> S0[]; S4[i, 1 + i] -> S3[i, n] : i >= 0 and i <= -1 + n; S3[i, j] -> S2[i, j, -1 + i] : j >= 1 + i and j <= n and i >= 1 and i <= -1 + n; S11[] -> S7[] : n = 0; S1[i, 1 + i] -> S5[-1 + i, n, -1 + i] : i >= 1 and i <= -1 + n; S12[-1 + n] -> S7[] : n >= 1; S4[0, 1] -> S1[0, n] : n >= 1; S6[i, j] -> S4[i, j] : i >= 0 and j >= 1 + i and j <= n and i <= -1 + n; S2[i, j, k] -> S6[-1 + k, i] : k >= 1 and k <= -1 + i and j >= 1 + i and j <= n and i <= -1 + n and i >= 0; S11[] -> S10[n] : n >= 1; S1[i, j] -> S2[i, -1 + j, -1 + i] : i >= 1 and j <= n and j >= 2 + i and i <= -1 + n; S2[i, j, k] -> S2[i, j, -1 + k] : j >= 1 + i and j <= n and k >= 1 and k <= -1 + i; S9[i, 0] -> S8[i] : i >= 1 and i <= n; S1[i, j] -> S3[i, -1 + j] : i >= 0 and j >= 2 + i and j <= n; S12[0] -> S9[n, -1 + n] : n >= 1; S13[i, j] -> S6[-2 + n - i, j] : j >= n - i and j <= n and i >= 0 and i <= -2 + n; S9[i, j] -> S9[i, -1 + j] : i <= n and j >= 1 and j <= -1 + i; S10[i] -> S9[i, -1 + i] : i >= 1 and i <= n; S5[i, j, k] -> S6[-1 + k, j] : k >= 1 and k <= i and j >= 1 + i and j <= n and i <= -1 + n and i >= 0; S9[i, j] -> S10[j] : i <= n and j <= -1 + i and j >= 1 and i >= 1; S9[i, 0] -> S7[] : i <= n and i >= 1; S3[i, j] -> S1[i, j] : i >= 0 and j >= 1 + i and j <= n }";
		String memoryBasedPlutoSchedule = "[n] -> { S1[i, j] -> [-n + i, j, 0, 0]; S7[] -> [0, 0, 0, 4]; S9[i, j] -> [0, i, -i + j, 7]; S4[i, j] -> [-n + i, n + j, 0, 1]; S11[] -> [0, n, 0, 5]; S0[] -> [0, 0, 0, 3]; S2[i, j, k] -> [-n + i, j, k, 1]; S6[i, j] -> [-n + i, n + j, j, 0]; S12[i] -> [0, n + i, 0, 8]; S13[i, j] -> [0, n + i, j, 0]; S10[i] -> [0, i, 0, 2]; S5[i, j, k] -> [-n + i, n + j, k, 2]; S8[i] -> [0, i, -i, 6]; S3[i, j] -> [-n + i, j, j, 2]; S14[i] -> [0, n + i, n, 1] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S7[] -> [1, 0, 0]; S11[] -> [3 + 3n, 0, 0]; S0[] -> [0, 0, 0]; S13[i, j] -> [4 + 3n + 3i, j, 0]; S4[i, j] -> [1 - 2n + 2i, 3j, 0]; S8[i] -> [3i, 0, 0]; S9[i, j] -> [1 + 3i, j, 0]; S10[i] -> [2 + 3i, 0, 0]; S12[i] -> [3 + 3n + 3i, 0, 0]; S5[i, j, k] -> [1 - 2n + 2i, 1 + 3j, k]; S6[i, j] -> [1 - 2n + 2i, 2 + 3j, 0]; S14[i] -> [5 + 3n + 3i, 0, 0]; S1[i, j] -> [-2n + 2i, 3j, 0]; S2[i, j, k] -> [-2n + 2i, 1 + 3j, k]; S3[i, j] -> [-2n + 2i, 2 + 3j, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #67
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : matrix_product_matrix_product/scop_0
	 *   block : 
	 *     for : 1 <= l <= loop (stride = 1)
	 *       for : 0 <= k <= 24 (stride = 1)
	 *         for : 0 <= i <= 24 (stride = 1)
	 *           for : 0 <= j <= n-1 (stride = 1)
	 *             S0 (depth = 4) [px[j][i]=add(px[j][i],mul(vy[k][i],cx[j][k]))]
	 *             ([px[j][i]]) = f([px[j][i], vy[k][i], cx[j][k]])
	 *             (Domain = [loop,n] -> {S0[l,k,i,j] : (j >= 0) and (-j+n-1 >= 0) and (i >= 0) and (-i+24 >= 0) and (k >= 0) and (-k+24 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_21_matrix_product_GScopRoot_matrix_product_scop_0_ () {
		String path = "polymodel_src_livermore_loops_21_matrix_product_GScopRoot_matrix_product_scop_0_";
		// SCoP Extraction Data
		String domains = "[loop, n] -> { S0[l, k, i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= 24 and k >= 0 and k <= 24 and l >= 1 and l <= loop }";
		String idSchedules = "[loop, n] -> { S0[l, k, i, j] -> [0, l, 0, k, 0, i, 0, j, 0] }";
		String writes = "[loop, n] -> { S0[l, k, i, j] -> px[j, i] }";
		String reads = "[loop, n] -> { S0[l, k, i, j] -> px[j, i]; S0[l, k, i, j] -> cx[j, k]; S0[l, k, i, j] -> vy[k, i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[loop, n] -> { S0[l, k, i, j] -> S0[l, -1 + k, i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= 24 and k >= 1 and k <= 24 and l >= 1 and l <= loop; S0[l, 0, i, j] -> S0[-1 + l, 24, i, j] : j >= 0 and j <= -1 + n and i >= 0 and i <= 24 and l >= 2 and l <= loop }";
		String valueBasedPlutoSchedule = "[loop, n] -> { S0[l, k, i, j] -> [l, 24l + k, i, j] }";
		String valueBasedFeautrierSchedule = "[loop, n] -> { S0[l, k, i, j] -> [25l + k, l, i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[loop, n] -> { S0[l, k, i, j] -> S0[l, -1 + k, i, j] : l >= 1 and l <= loop and k <= 24 and k >= 1 and i <= 24 and i >= 0 and j >= 0 and j <= -1 + n; S0[l, 0, i, j] -> S0[-1 + l, 24, i, j] : l >= 2 and l <= loop and i <= 24 and i >= 0 and j >= 0 and j <= -1 + n }";
		String memoryBasedPlutoSchedule = "[loop, n] -> { S0[l, k, i, j] -> [l, 24l + k, i, j] }";
		String memoryBasedFeautrierSchedule = "[loop, n] -> { S0[l, k, i, j] -> [25l + k, l, i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #68
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : _1_D_PIC__1_D_PIC/scop_0
	 *   for : 0 <= k <= n-1 (stride = 1)
	 *     block : 
	 *       S0 (depth = 1) [vx[k]=add(add(vx[k],ex1[k]),mul(sub(xx[k],xi[k]),dex1[k]))]
	 *       ([vx[k]]) = f([vx[k], ex1[k], xx[k], xi[k], dex1[k]])
	 *       (Domain = [n,flx] -> {S0[k] : (k >= 0) and (-k+n-1 >= 0)})
	 *       S1 (depth = 1) [xx[k]=add(add(xx[k],vx[k]),IntExpr(flx))]
	 *       ([xx[k]]) = f([xx[k], vx[k]])
	 *       (Domain = [n,flx] -> {S1[k] : (k >= 0) and (-k+n-1 >= 0)})
	 *       S2 (depth = 1) [ir[k]=xx[k]]
	 *       ([ir[k]]) = f([xx[k]])
	 *       (Domain = [n,flx] -> {S2[k] : (k >= 0) and (-k+n-1 >= 0)})
	 *       S3 (depth = 1) [rx[k]=sub(xx[k],ir[k])]
	 *       ([rx[k]]) = f([xx[k], ir[k]])
	 *       (Domain = [n,flx] -> {S3[k] : (k >= 0) and (-k+n-1 >= 0)})
	 *       S4 (depth = 1) [ir[k]=add(and(ir[k],IntExpr(2047)),IntExpr(1))]
	 *       ([ir[k]]) = f([ir[k]])
	 *       (Domain = [n,flx] -> {S4[k] : (k >= 0) and (-k+n-1 >= 0)})
	 *       S5 (depth = 1) [xx[k]=add(rx[k],ir[k])]
	 *       ([xx[k]]) = f([rx[k], ir[k]])
	 *       (Domain = [n,flx] -> {S5[k] : (k >= 0) and (-k+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_14_1_D_PIC_GScopRoot__1_D_PIC_scop_0_ () {
		String path = "polymodel_src_livermore_loops_14_1_D_PIC_GScopRoot__1_D_PIC_scop_0_";
		// SCoP Extraction Data
		String domains = "[n, flx] -> { S3[k] : k >= 0 and k <= -1 + n; S5[k] : k >= 0 and k <= -1 + n; S1[k] : k >= 0 and k <= -1 + n; S4[k] : k >= 0 and k <= -1 + n; S0[k] : k >= 0 and k <= -1 + n; S2[k] : k >= 0 and k <= -1 + n }";
		String idSchedules = "[n, flx] -> { S0[k] -> [0, k, 0]; S3[k] -> [0, k, 3]; S5[k] -> [0, k, 5]; S2[k] -> [0, k, 2]; S1[k] -> [0, k, 1]; S4[k] -> [0, k, 4] }";
		String writes = "[n, flx] -> { S1[k] -> xx[k]; S0[k] -> vx[k]; S3[k] -> rx[k]; S5[k] -> xx[k]; S4[k] -> ir[k]; S2[k] -> ir[k] }";
		String reads = "[n, flx] -> { S1[k] -> xx[k]; S0[k] -> xx[k]; S0[k] -> vx[k]; S5[k] -> rx[k]; S3[k] -> xx[k]; S3[k] -> ir[k]; S0[k] -> xi[k]; S2[k] -> xx[k]; S0[k] -> dex1[k]; S4[k] -> ir[k]; S0[k] -> ex1[k]; S1[k] -> vx[k]; S5[k] -> ir[k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n, flx] -> { S5[k] -> S4[k] : k >= 0 and k <= -1 + n; S3[k] -> S2[k] : k >= 0 and k <= -1 + n; S4[k] -> S2[k] : k >= 0 and k <= -1 + n; S5[k] -> S3[k] : k >= 0 and k <= -1 + n; S2[k] -> S1[k] : k >= 0 and k <= -1 + n; S3[k] -> S1[k] : k >= 0 and k <= -1 + n; S1[k] -> S0[k] : k >= 0 and k <= -1 + n }";
		String valueBasedPlutoSchedule = "[n, flx] -> { S3[k] -> [k, 4]; S1[k] -> [k, 1]; S5[k] -> [k, 5]; S2[k] -> [k, 2]; S0[k] -> [k, 0]; S4[k] -> [k, 3] }";
		String valueBasedFeautrierSchedule = "[n, flx] -> { S1[k] -> [k]; S5[k] -> [k]; S4[k] -> [k]; S2[k] -> [k]; S3[k] -> [k]; S0[k] -> [k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n, flx] -> { S5[k] -> S4[k] : k >= 0 and k <= -1 + n; S5[k] -> S1[k] : k >= 0 and k <= -1 + n; S3[k] -> S2[k] : k >= 0 and k <= -1 + n; S4[k] -> S2[k] : k >= 0 and k <= -1 + n; S5[k] -> S2[k] : k >= 0 and k <= -1 + n; S4[k] -> S3[k] : k >= 0 and k <= -1 + n; S5[k] -> S3[k] : k >= 0 and k <= -1 + n; S2[k] -> S1[k] : k >= 0 and k <= -1 + n; S3[k] -> S1[k] : k >= 0 and k <= -1 + n; S1[k] -> S0[k] : k >= 0 and k <= -1 + n }";
		String memoryBasedPlutoSchedule = "[n, flx] -> { S3[k] -> [k, 3]; S1[k] -> [k, 1]; S5[k] -> [k, 5]; S2[k] -> [k, 2]; S0[k] -> [k, 0]; S4[k] -> [k, 4] }";
		String memoryBasedFeautrierSchedule = "[n, flx] -> { S1[k] -> [k]; S5[k] -> [k]; S4[k] -> [k]; S2[k] -> [k]; S3[k] -> [k]; S0[k] -> [k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #69
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : pureFunctionTest_pureFunctionTest/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= N-1 (stride = 1)
	 *         S0 (depth = 2) [call func(x[i][j],x[j][i],((*SF64)&(out[i][j])),((*SF64)&(out[j][i])))]
	 *         ([out[i][j], out[j][i]]) = f([x[i][j], x[j][i]])
	 *         (Domain = [N] -> {S0[i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_pureFunctionTest_GScopRoot_pureFunctionTest_scop_0_ () {
		String path = "polymodel_src_corner_cases_pureFunctionTest_GScopRoot_pureFunctionTest_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[N] -> { S0[i, j] -> out[j, i]; S0[i, j] -> out[i, j] }";
		String reads = "[N] -> { S0[i, j] -> x[j, i]; S0[i, j] -> x[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> {  }";
		String valueBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S0[i, j] -> S0[j, i] : j >= 0 and i <= -1 + N and j <= -1 + i }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, i + j] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S0[i, j] -> [-j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #70
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test1_test1/scop_0
	 *   for : 0 <= j <= 3 (stride = 1)
	 *     S0 (depth = 1) [b=add(b,y[i][j])]
	 *     ([b]) = f([b, y[i][j]])
	 *     (Domain = [i] -> {S0[j] : (j >= 0) and (-j+3 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_simple_pragma_GScopRoot_test1_scop_0_ () {
		String path = "basics_simple_pragma_GScopRoot_test1_scop_0_";
		// SCoP Extraction Data
		String domains = "[i] -> { S0[j] : j >= 0 and j <= 3 }";
		String idSchedules = "[i] -> { S0[j] -> [0, j, 0] }";
		String writes = "[i] -> { S0[j] -> b[] }";
		String reads = "[i] -> { S0[j] -> y[i, j]; S0[j] -> b[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[i] -> { S0[j] -> S0[-1 + j] : j >= 1 and j <= 3 }";
		String valueBasedPlutoSchedule = "[i] -> { S0[j] -> [j] }";
		String valueBasedFeautrierSchedule = "[i] -> { S0[j] -> [j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[i] -> { S0[j] -> S0[-1 + j] : j <= 3 and j >= 1 }";
		String memoryBasedPlutoSchedule = "[i] -> { S0[j] -> [j] }";
		String memoryBasedFeautrierSchedule = "[i] -> { S0[j] -> [j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #71
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : adi_core_adi_core/scop_0
	 *   block : 
	 *     for : 0 <= t <= tsteps-1 (stride = 1)
	 *       block : 
	 *         for : 0 <= i1 <= n-1 (stride = 1)
	 *           for : 1 <= i2 <= n-1 (stride = 1)
	 *             block : 
	 *               S0 (depth = 3) [X[i1][i2]=sub(X[i1][i2],div(mul(X[i1][i2-1],A[i1][i2]),B[i1][i2-1]))]
	 *               ([X[i1][i2]]) = f([X[i1][i2], X[i1][i2-1], A[i1][i2], B[i1][i2-1]])
	 *               (Domain = [tsteps,n] -> {S0[t,i1,i2] : (i2-1 >= 0) and (-i2+n-1 >= 0) and (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *               S1 (depth = 3) [B[i1][i2]=sub(B[i1][i2],div(mul(A[i1][i2],A[i1][i2]),B[i1][i2-1]))]
	 *               ([B[i1][i2]]) = f([B[i1][i2], A[i1][i2], A[i1][i2], B[i1][i2-1]])
	 *               (Domain = [tsteps,n] -> {S1[t,i1,i2] : (i2-1 >= 0) and (-i2+n-1 >= 0) and (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *         for : 0 <= i1 <= n-1 (stride = 1)
	 *           S2 (depth = 2) [X[i1][n-1]=div(X[i1][n-1],B[i1][n-1])]
	 *           ([X[i1][n-1]]) = f([X[i1][n-1], B[i1][n-1]])
	 *           (Domain = [tsteps,n] -> {S2[t,i1] : (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *         for : 0 <= i1 <= n-1 (stride = 1)
	 *           for : 0 <= i2 <= n-3 (stride = 1)
	 *             S3 (depth = 3) [X[i1][n-i2-2]=div(sub(X[i1][n-i2-2],mul(X[i1][n-i2-3],A[i1][n-i2-3])),B[i1][n-i2-3])]
	 *             ([X[i1][n-i2-2]]) = f([X[i1][n-i2-2], X[i1][n-i2-3], A[i1][n-i2-3], B[i1][n-i2-3]])
	 *             (Domain = [tsteps,n] -> {S3[t,i1,i2] : (i2 >= 0) and (-i2+n-3 >= 0) and (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *         for : 1 <= i1 <= n-1 (stride = 1)
	 *           for : 0 <= i2 <= n-1 (stride = 1)
	 *             block : 
	 *               S4 (depth = 3) [X[i1][i2]=sub(X[i1][i2],div(mul(X[i1-1][i2],A[i1][i2]),B[i1-1][i2]))]
	 *               ([X[i1][i2]]) = f([X[i1][i2], X[i1-1][i2], A[i1][i2], B[i1-1][i2]])
	 *               (Domain = [tsteps,n] -> {S4[t,i1,i2] : (i2 >= 0) and (-i2+n-1 >= 0) and (i1-1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *               S5 (depth = 3) [B[i1][i2]=sub(B[i1][i2],div(mul(A[i1][i2],A[i1][i2]),B[i1-1][i2]))]
	 *               ([B[i1][i2]]) = f([B[i1][i2], A[i1][i2], A[i1][i2], B[i1-1][i2]])
	 *               (Domain = [tsteps,n] -> {S5[t,i1,i2] : (i2 >= 0) and (-i2+n-1 >= 0) and (i1-1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *         for : 0 <= i1 <= n-1 (stride = 1)
	 *           S6 (depth = 2) [X[n-1][i1]=div(X[n-1][i1],B[n-1][i1])]
	 *           ([X[n-1][i1]]) = f([X[n-1][i1], B[n-1][i1]])
	 *           (Domain = [tsteps,n] -> {S6[t,i1] : (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *         for : 0 <= i1 <= n-3 (stride = 1)
	 *           for : 0 <= i2 <= n-1 (stride = 1)
	 *             S7 (depth = 3) [X[n-i1-2][i2]=div(sub(X[n-i1-2][i2],mul(X[n-i1-3][i2],A[n-i1-3][i2])),B[n-i1-2][i2])]
	 *             ([X[n-i1-2][i2]]) = f([X[n-i1-2][i2], X[n-i1-3][i2], A[n-i1-3][i2], B[n-i1-2][i2]])
	 *             (Domain = [tsteps,n] -> {S7[t,i1,i2] : (i2 >= 0) and (-i2+n-1 >= 0) and (i1 >= 0) and (-i1+n-3 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_adi_core_patched_fail_indices_GScopRoot_adi_core_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_adi_core_patched_fail_indices_GScopRoot_adi_core_scop_0_";
		// SCoP Extraction Data
		String domains = "[tsteps, n] -> { S6[t, i1] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S1[t, i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S5[t, i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 1 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S2[t, i1] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S0[t, i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S3[t, i1, i2] : i2 >= 0 and i2 <= -3 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S4[t, i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 1 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S7[t, i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 0 and i1 <= -3 + n and t >= 0 and t <= -1 + tsteps }";
		String idSchedules = "[tsteps, n] -> { S4[t, i1, i2] -> [0, t, 3, i1, 0, i2, 0]; S7[t, i1, i2] -> [0, t, 5, i1, 0, i2, 0]; S0[t, i1, i2] -> [0, t, 0, i1, 0, i2, 0]; S6[t, i1] -> [0, t, 4, i1, 0, 0, 0]; S2[t, i1] -> [0, t, 1, i1, 0, 0, 0]; S1[t, i1, i2] -> [0, t, 0, i1, 0, i2, 1]; S3[t, i1, i2] -> [0, t, 2, i1, 0, i2, 0]; S5[t, i1, i2] -> [0, t, 3, i1, 0, i2, 1] }";
		String writes = "[tsteps, n] -> { S0[t, i1, i2] -> X[i1, i2]; S7[t, i1, i2] -> X[-2 + n - i1, i2]; S3[t, i1, i2] -> X[i1, -2 + n - i2]; S1[t, i1, i2] -> B[i1, i2]; S5[t, i1, i2] -> B[i1, i2]; S2[t, i1] -> X[i1, -1 + n]; S4[t, i1, i2] -> X[i1, i2]; S6[t, i1] -> X[-1 + n, i1] }";
		String reads = "[tsteps, n] -> { S0[t, i1, i2] -> X[i1, i2]; S0[t, i1, i2] -> X[i1, -1 + i2]; S0[t, i1, i2] -> A[i1, i2]; S3[t, i1, i2] -> X[i1, -2 + n - i2]; S3[t, i1, i2] -> X[i1, -3 + n - i2]; S4[t, i1, i2] -> B[-1 + i1, i2]; S7[t, i1, i2] -> X[-2 + n - i1, i2]; S7[t, i1, i2] -> X[-3 + n - i1, i2]; S4[t, i1, i2] -> A[i1, i2]; S1[t, i1, i2] -> B[i1, i2]; S1[t, i1, i2] -> B[i1, -1 + i2]; S5[t, i1, i2] -> B[i1, i2]; S5[t, i1, i2] -> B[-1 + i1, i2]; S2[t, i1] -> X[i1, -1 + n]; S5[t, i1, i2] -> A[i1, i2]; S6[t, i1] -> B[-1 + n, i1]; S7[t, i1, i2] -> B[-2 + n - i1, i2]; S4[t, i1, i2] -> X[i1, i2]; S4[t, i1, i2] -> X[-1 + i1, i2]; S3[t, i1, i2] -> A[i1, -3 + n - i2]; S3[t, i1, i2] -> B[i1, -3 + n - i2]; S7[t, i1, i2] -> A[-3 + n - i1, i2]; S2[t, i1] -> B[i1, -1 + n]; S0[t, i1, i2] -> B[i1, -1 + i2]; S6[t, i1] -> X[-1 + n, i1]; S1[t, i1, i2] -> A[i1, i2] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[tsteps, n] -> { S1[t, i1, i2] -> S1[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S1[t, 0, i2] -> S1[-1 + t, 0, i2] : i2 >= 1 and i2 <= -1 + n and t <= -1 + tsteps and t >= 1; S2[t, i1] -> S0[t, i1, -1 + n] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S0[t, i1, 1] -> S5[-1 + t, i1, 0] : t <= -1 + tsteps and n >= 2 and i1 >= 1 and i1 <= -1 + n and t >= 1; S4[t, i1, i2] -> S4[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S0[t, 0, -1 + n] -> S2[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S4[t, i1, 0] -> S7[-1 + t, -2 + n - i1, 0] : t <= -1 + tsteps and t >= 1 and i1 >= 1 and i1 <= -2 + n and n >= 1; S7[t, i1, i2] -> S4[t, i1', i2] : i2 >= 0 and i2 <= -1 + n and t >= 0 and i1' >= 1 and i1' >= -3 + n - i1 and t <= -1 + tsteps and i1 >= 0 and i1' <= -2 + n - i1; S4[t, i1, i2] -> S3[t, i1, -2 + n - i2] : i2 >= 1 and i2 <= -2 + n and i1 >= 1 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S4[t, 1, i2] -> S3[t, 0, -2 + n - i2] : i2 >= 1 and t >= 0 and t <= -1 + tsteps and i2 <= -2 + n; S0[t, i1, i2] -> S1[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S3[t, -1 + n, -3 + n] -> S6[-1 + t, 0] : n >= 3 and t <= -1 + tsteps and t >= 1; S2[t, 0] -> S6[-1 + t, 0] : n = 1 and t >= 1 and t <= -1 + tsteps; S5[t, i1, i2] -> S1[t, i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 1 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S5[t, 1, i2] -> S1[t, 0, i2] : i2 >= 1 and i2 <= -1 + n and t <= -1 + tsteps and t >= 0; S3[t, i1, -3 + n] -> S7[-1 + t, -2 + n - i1, 0] : n >= 3 and t <= -1 + tsteps and i1 >= 1 and i1 <= -2 + n and t >= 1; S3[t, i1, i2] -> S1[t, i1, -3 + n - i2] : i2 >= 0 and i2 <= -4 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S0[t, -1 + n, i2] -> S6[-1 + t, i2] : i2 >= 1 and i2 <= -1 + n and t >= 1 and t <= -1 + tsteps; S0[t, -1 + n, 1] -> S6[-1 + t, 0] : t <= -1 + tsteps and n >= 2 and t >= 1; S5[t, i1, i2] -> S5[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S5[t, i1, 0] -> S5[-1 + t, i1, 0] : t <= -1 + tsteps and t >= 1 and i1 >= 1 and i1 <= -1 + n; S6[t, i1] -> S5[t, -1 + n, i1] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S2[t, i1] -> S1[t, i1, -1 + n] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S4[t, 1, i2] -> S1[t, 0, i2] : i2 >= 1 and i2 <= -1 + n and t <= -1 + tsteps and n >= 2 and t >= 0; S6[t, i1] -> S4[t, -1 + n, i1] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S6[t, 0] -> S2[t, 0] : n = 1 and t >= 0 and t <= -1 + tsteps; S7[t, -3 + n, i2] -> S3[t, 0, -2 + n - i2] : i2 >= 1 and i2 <= -2 + n and n >= 3 and t <= -1 + tsteps and t >= 0; S0[t, i1, i2] -> S7[-1 + t, -2 + n - i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 1 and i1 <= -2 + n and t >= 1 and t <= -1 + tsteps; S0[t, i1, 1] -> S7[-1 + t, -2 + n - i1, 0] : t <= -1 + tsteps and i1 >= 1 and t >= 1 and i1 <= -2 + n; S7[t, i1, i2] -> S5[t, -2 + n - i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 0 and i1 <= -3 + n and t >= 0 and t <= -1 + tsteps; S4[t, i1, -1 + n] -> S2[t, i1] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S4[t, 1, -1 + n] -> S2[t, 0] : n >= 2 and t >= 0 and t <= -1 + tsteps; S0[t, 0, i2] -> S3[-1 + t, 0, -2 + n - i2] : i2 >= 1 and t >= 1 and t <= -1 + tsteps and i2 <= -2 + n and n >= 1; S4[t, i1, i2] -> S5[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S7[t, -3 + n, -1 + n] -> S2[t, 0] : n >= 3 and t >= 0 and t <= -1 + tsteps; S3[t, i1, i2] -> S0[t, i1, i2'] : i2' <= -2 + n - i2 and i2' >= 1 and i1 <= -1 + n and t >= 0 and i2' >= -3 + n - i2 and t <= -1 + tsteps and i2 >= 0 and i1 >= 0; S0[t, i1, i2] -> S0[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S4[t, -1 + n, 0] -> S6[-1 + t, 0] : t <= -1 + tsteps and t >= 1 and n >= 2; S1[t, i1, i2] -> S5[-1 + t, i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 1 and i1 <= -1 + n and t >= 1 and t <= -1 + tsteps; S1[t, i1, 1] -> S5[-1 + t, i1, 0] : t <= -1 + tsteps and t >= 1 and i1 >= 1 and i1 <= -1 + n; S3[t, i1, -3 + n] -> S5[-1 + t, i1, 0] : n >= 3 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and t >= 1 }";
		String valueBasedPlutoSchedule = "[tsteps, n] -> { S5[t, i1, i2] -> [t, i1, i2, 6]; S6[t, i1] -> [t, 1 + n, 2 + i1, 7]; S0[t, i1, i2] -> [t, 2 + i1, 2 + i2, 0]; S4[t, i1, i2] -> [t, 2 + i1, 2 + i2, 3]; S1[t, i1, i2] -> [t, i1, i2, 5]; S7[t, i1, i2] -> [t, n - i1, 2 + i2, 4]; S2[t, i1] -> [t, 2 + i1, 1 + n, 2]; S3[t, i1, i2] -> [t, 2 + i1, n - i2, 1] }";
		String valueBasedFeautrierSchedule = "[tsteps, n] -> { S3[t, i1, i2] -> [n + 4t + i1 - i2, t, i2]; S7[t, i1, i2] -> [2 + n + 4t - i1 + i2, t, i2]; S2[t, i1] -> [1 + n + 4t + i1, t, 0]; S0[t, i1, i2] -> [1 + 4t + i1 + i2, t, i2]; S4[t, i1, i2] -> [3 + 4t + i1 + i2, t, i2]; S6[t, i1] -> [3 + n + 4t + i1, t, 0]; S1[t, i1, i2] -> [2t + i1 + i2, t, i2]; S5[t, i1, i2] -> [1 + 2t + i1 + i2, t, i2] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[tsteps, n] -> { S3[t, i1, i2] -> S0[t, i1, i2'] : t <= -1 + tsteps and i2' >= 1 and i2' >= -3 + n - i2 and i2 >= 0 and i2' <= -1 + n and i2 <= -3 + n and i2' <= -1 + n - i2 and t >= 0 and i1 >= 0 and i1 <= -1 + n; S2[t, i1] -> S1[t, i1, -1 + n] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S7[t, i1, i2] -> S7[t, -1 + i1, i2] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -3 + n and i2 >= 0 and i2 <= -1 + n; S4[t, i1, i2] -> S3[t, i1, -2 + n - i2] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and i2 <= -2 + n and i2 >= 1; S4[t, 1, i2] -> S3[t, 0, -2 + n - i2] : t >= 0 and t <= -1 + tsteps and i2 <= -2 + n and i2 >= 1; S4[t, i1, 0] -> S3[t, i1, -3 + n] : n >= 3 and t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S1[t, i1, i2] -> S4[-1 + t, 1 + i1, i2] : t >= 1 and t <= -1 + tsteps and i1 >= 0 and i1 <= -2 + n and i2 >= 1 and i2 <= -1 + n; S0[t, i1, 1] -> S5[-1 + t, i1, 0] : t <= -1 + tsteps and n >= 2 and i1 >= 1 and i1 <= -1 + n and t >= 1; S3[t, i1, -3 + n] -> S7[-1 + t, -2 + n - i1, 0] : n >= 3 and t <= -1 + tsteps and i1 >= 1 and i1 <= -2 + n and t >= 1; S7[t, i1, i2] -> S5[t, -2 + n - i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 0 and i1 <= -3 + n and t >= 0 and t <= -1 + tsteps; S2[t, i1] -> S0[t, i1, -1 + n] : n >= 2 and t >= 0 and t <= -1 + tsteps and i1 >= 0 and i1 <= -1 + n; S1[t, i1, i2] -> S7[-1 + t, -2 + n - i1, i2] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1 and i2 >= 1 and i2 <= -1 + n; S5[t, -1 + n, 0] -> S6[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S0[t, i1, i2] -> S7[-1 + t, -2 + n - i1, i2] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1 and i2 >= 1 and i2 <= -1 + n; S0[t, 0, i2] -> S7[-1 + t, -3 + n, i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -2 + n; S0[t, i1, 1] -> S7[-1 + t, -2 + n - i1, 0] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1; S0[t, 0, -1 + n] -> S7[-1 + t, -3 + n, -1 + n] : n >= 3 and t >= 1 and t <= -1 + tsteps; S4[t, -1 + n, 0] -> S6[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S4[t, 1, i2] -> S1[t, 0, i2] : i2 >= 1 and i2 <= -1 + n and t <= -1 + tsteps and n >= 2 and t >= 0; S1[t, 0, i2] -> S0[-1 + t, 0, 1 + i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -2 + n and n >= 1; S2[t, 0] -> S6[-1 + t, 0] : n = 1 and t >= 1 and t <= -1 + tsteps; S0[t, i1, i2] -> S1[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S0[t, 0, i2] -> S3[-1 + t, 0, -2 + n - i2] : t >= 1 and t <= -1 + tsteps and i2 <= -2 + n and i2 >= 1; S6[t, 0] -> S2[t, 0] : n = 1 and t >= 0 and t <= -1 + tsteps; S4[t, i1, -1 + n] -> S2[t, i1] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S4[t, 1, -1 + n] -> S2[t, 0] : n >= 2 and t >= 0 and t <= -1 + tsteps; S0[t, 0, -1 + n] -> S2[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S5[t, i1, 0] -> S7[-1 + t, -2 + n - i1, 0] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1 and n >= 1; S5[t, i1, i2] -> S1[t, i1, i2'] : i2' >= i2 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and i2' >= 1 and i2' <= -1 + n and i2' <= 1 + i2 and t >= 0; S5[t, 1, i2] -> S1[t, 0, i2] : t >= 0 and t <= -1 + tsteps and i2 >= 1 and i2 <= -1 + n; S7[t, -3 + n, i2] -> S3[t, 0, -2 + n - i2] : i2 >= 1 and i2 <= -2 + n and n >= 3 and t <= -1 + tsteps and t >= 0; S1[t, 0, -1 + n] -> S2[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S4[t, i1, 0] -> S0[t, i1, 1] : n >= 2 and t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S7[t, i1, i2] -> S4[t, i1', i2] : t <= -1 + tsteps and i1' >= 1 and i1' >= -3 + n - i1 and i1 >= 0 and i2 >= 0 and i1' <= -1 + n and i1' <= -1 + n - i1 and i2 <= -1 + n and t >= 0 and i1 <= -3 + n; S4[t, i1, i2] -> S4[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S1[t, -1 + n, i2] -> S6[-1 + t, i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -1 + n and n >= 1; S6[t, i1] -> S5[t, -1 + n, i1] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S5[t, i1, i2] -> S3[t, i1, -3 + n - i2] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and i2 <= -3 + n and i2 >= 0; S5[t, i1, i2] -> S5[t, -1 + i1, i2] : t >= 0 and t <= -1 + tsteps and i1 >= 2 and i1 <= -1 + n and i2 >= 0 and i2 <= -1 + n; S5[t, i1, 0] -> S5[-1 + t, i1', 0] : t >= 1 and t <= -1 + tsteps and i1' <= -1 + n and i1 >= 1 and i1' >= i1 and i1' <= 1 + i1; S7[t, -3 + n, -1 + n] -> S2[t, 0] : n >= 3 and t >= 0 and t <= -1 + tsteps; S5[t, i1, -1 + n] -> S2[t, i1] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and n >= 1; S5[t, i1, i2] -> S0[t, i1, 1 + i2] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and i2 >= 0 and i2 <= -2 + n; S6[t, i1] -> S4[t, -1 + n, i1] : n >= 2 and t >= 0 and t <= -1 + tsteps and i1 >= 0 and i1 <= -1 + n; S3[t, -1 + n, -3 + n] -> S6[-1 + t, 0] : n >= 3 and t <= -1 + tsteps and t >= 1; S0[t, 0, i2] -> S4[-1 + t, 1, i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -1 + n; S1[t, 0, i2] -> S1[-1 + t, 0, i2'] : t >= 1 and t <= -1 + tsteps and i2' <= -1 + n and i2 >= 1 and i2' >= i2 and i2' <= 1 + i2; S1[t, i1, i2] -> S1[t, i1, -1 + i2] : t >= 0 and t <= -1 + tsteps and i1 >= 0 and i1 <= -1 + n and i2 >= 2 and i2 <= -1 + n; S0[t, i1, i2] -> S0[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S3[t, i1, i2] -> S1[t, i1, -3 + n - i2] : i2 >= 0 and i2 <= -4 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S1[t, 0, i2] -> S3[-1 + t, 0, -3 + n - i2] : t >= 1 and t <= -1 + tsteps and i2 <= -3 + n and i2 >= 1 and n >= 1; S1[t, i1, i2] -> S5[-1 + t, i1', i2] : t >= 1 and t <= -1 + tsteps and i1' >= 1 and i1' <= -1 + n and i2 >= 1 and i2 <= -1 + n and i1' >= i1 and i1' <= 1 + i1; S1[t, i1, 1] -> S5[-1 + t, i1, 0] : t >= 1 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S3[t, i1, i2] -> S3[t, i1, -1 + i2] : t >= 0 and t <= -1 + tsteps and i1 >= 0 and i1 <= -1 + n and i2 >= 1 and i2 <= -3 + n; S0[t, -1 + n, i2] -> S6[-1 + t, i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -1 + n; S0[t, -1 + n, 1] -> S6[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S4[t, i1, i2] -> S5[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S5[t, i1, 0] -> S4[-1 + t, 1 + i1, 0] : t >= 1 and t <= -1 + tsteps and i1 >= 1 and i1 <= -2 + n and n >= 1; S3[t, i1, -3 + n] -> S5[-1 + t, i1, 0] : n >= 3 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and t >= 1; S4[t, i1, 0] -> S7[-1 + t, -2 + n - i1, 0] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1 }";
		String memoryBasedPlutoSchedule = "[tsteps, n] -> { S5[t, i1, i2] -> [t, i1, i2, 5]; S6[t, i1] -> [t, n, i1, 7]; S0[t, i1, i2] -> [t, i1, -2n + i2, 3]; S4[t, i1, i2] -> [t, i1, i2, 1]; S1[t, i1, i2] -> [t, i1, -2n + i2, 6]; S7[t, i1, i2] -> [t, n + i1, i2, 0]; S2[t, i1] -> [t, i1, 0, 4]; S3[t, i1, i2] -> [t, i1, -n + i2, 2] }";
		String memoryBasedFeautrierSchedule = "[tsteps, n] -> { S5[t, i1, i2] -> [t, 3, i1, i2]; S6[t, i1] -> [t, 5, i1, 0]; S0[t, i1, i2] -> [t, 1, i2, i1]; S4[t, i1, i2] -> [t, 4, i1, i2]; S1[t, i1, i2] -> [t, 0, i2, i1]; S7[t, i1, i2] -> [t, 5, i1, i2]; S2[t, i1] -> [t, 2, i1, 0]; S3[t, i1, i2] -> [t, 2, i2, i1] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #72
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : toto_toto/scop_0
	 *   block : 
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       S0 (depth = 1) [tab[i]=add(tab[i-2],tab[i-1])]
	 *       ([tab[i]]) = f([tab[i-2], tab[i-1]])
	 *       (Domain = [n] -> {S0[i] : (i >= 0) and (-i+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_makePRDGfail_GScopRoot_toto_scop_0_ () {
		String path = "polymodel_others_makePRDGfail_GScopRoot_toto_scop_0_";
		// SCoP Extraction Data
		String domains = "[n] -> { S0[i] : i >= 0 and i <= -1 + n }";
		String idSchedules = "[n] -> { S0[i] -> [0, i, 0] }";
		String writes = "[n] -> { S0[i] -> tab[i] }";
		String reads = "[n] -> { S0[i] -> tab[-1 + i]; S0[i] -> tab[-2 + i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S0[i] -> S0[i'] : i' >= -2 + i and i' >= 0 and i <= -1 + n and i' <= -1 + i }";
		String valueBasedPlutoSchedule = "[n] -> { S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "[n] -> { S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S0[i] -> S0[i'] : i' >= -2 + i and i' >= 0 and i <= -1 + n and i' <= -1 + i }";
		String memoryBasedPlutoSchedule = "[n] -> { S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #73
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : bar_bar/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= i-1 (stride = 1)
	 *         block : 
	 *           if : ((i < N) and (j < N)) or ((i < j) and (i > 0))
	 *             S0 (depth = 2) [x[N-j]=IntExpr(a)]
	 *             ([x[N-j]]) = f([])
	 *             (Domain = [N,a] -> {S0[i,j] : (-i+N-1 >= 0) and (j >= 0) and (i-j-1 >= 0)})
	 *           S1 (depth = 2) [x[i+j]=IntExpr(a)]
	 *           ([x[i+j]]) = f([])
	 *           (Domain = [N,a] -> {S1[i,j] : (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_scop10_GScopRoot_bar_scop_0_ () {
		String path = "polymodel_src_generic_for_loops_scop10_GScopRoot_bar_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, a] -> { S0[i, j] : i <= -1 + N and j >= 0 and j <= -1 + i; S1[i, j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N }";
		String idSchedules = "[N, a] -> { S0[i, j] -> [0, i, 0, j, 0]; S1[i, j] -> [0, i, 0, j, 1] }";
		String writes = "[N, a] -> { S1[i, j] -> x[i + j]; S0[i, j] -> x[N - j] }";
		String reads = "[N, a] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, a] -> {  }";
		String valueBasedPlutoSchedule = "[N, a] -> { S1[i, j] -> [i, j]; S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[N, a] -> { S1[i, j] -> [i, j]; S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, a] -> { S0[i, j] -> S1[-1 + i, 1 + N - i - j] : i <= -1 + N and j >= 0 and j >= 3 + N - 2i and 2j <= N - i; S0[i, j] -> S1[i, N - i - j] : j <= N - i and 2j >= 1 + N - i and j <= -1 + i; S0[i, -1 + i] -> S1[1 + N - i, 0] : i <= -1 + N and 2i >= 2 + N; S1[i, j] -> S1[-1 + i, j'] : 2j = -1 + N - i and 2j' = 1 + N - i and i <= -1 + N and 3i >= 5 + N; S1[i, j] -> S1[-1 + i, 1 + j] : (j >= 0 and j <= -3 + i and j <= 1 + N - 2i) or (i <= -1 + N and j >= 1 + N - i and j <= -3 + i); S1[i, j] -> S0[i, j'] : 2j = N - i and 2j' = N - i and i <= -1 + N and 3i >= 2 + N; S1[i, j] -> S0[i, N - i - j] : j <= N - i and j <= -1 + i and 2j >= 1 + N - i; S1[i, j] -> S0[-1 + i, N - i - j] : j >= 0 and j >= 2 + N - 2i and 2j <= -2 + N - i; S0[i, j] -> S0[-1 + i, j] : (i <= -1 + N and j >= 1 + N - i and j <= -2 + i) or (i <= -1 + N and j >= 0 and j <= -2 + i and j <= 2 + N - 2i) }";
		String memoryBasedPlutoSchedule = "[N, a] -> { S1[i, j] -> [i, N - i - j, 0]; S0[i, j] -> [i, j, 0] }";
		String memoryBasedFeautrierSchedule = "[N, a] -> { S1[i, j] -> [i, j, 1]; S0[i, j] -> [i, j, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #74
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : bar_bar/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       S0 (depth = 1) [x[N-i]=IntExpr(a)]
	 *       ([x[N-i]]) = f([])
	 *       (Domain = [N,a] -> {S0[i] : (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_scop11_GScopRoot_bar_scop_0_ () {
		String path = "polymodel_src_generic_for_loops_scop11_GScopRoot_bar_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, a] -> { S0[i] : i >= 0 and i <= -1 + N }";
		String idSchedules = "[N, a] -> { S0[i] -> [0, i, 0] }";
		String writes = "[N, a] -> { S0[i] -> x[N - i] }";
		String reads = "[N, a] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, a] -> {  }";
		String valueBasedPlutoSchedule = "[N, a] -> { S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "[N, a] -> { S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, a] -> {  }";
		String memoryBasedPlutoSchedule = "[N, a] -> { S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "[N, a] -> { S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #75
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : simple_simple/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [A[i]=add(A[i],IntExpr(i))]
	 *         ([A[i]]) = f([A[i]])
	 *         (Domain = [N] -> {S0[i] : (i >= 0) and (-i+N-1 >= 0)})
	 *         S1 (depth = 1) [A[i]=add(A[i],IntExpr(3))]
	 *         ([A[i]]) = f([A[i]])
	 *         (Domain = [N] -> {S1[i] : (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_simple_GScopRoot_simple_scop_0_ () {
		String path = "polymodel_src_corner_cases_simple_GScopRoot_simple_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S1[i] : i >= 0 and i <= -1 + N; S0[i] : i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S0[i] -> [0, i, 0]; S1[i] -> [0, i, 1] }";
		String writes = "[N] -> { S1[i] -> A[i]; S0[i] -> A[i] }";
		String reads = "[N] -> { S1[i] -> A[i]; S0[i] -> A[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S1[i] -> S0[i] : i >= 0 and i <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S1[i] -> [i, 1]; S0[i] -> [i, 0] }";
		String valueBasedFeautrierSchedule = "[N] -> { S1[i] -> [i]; S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S1[i] -> S0[i] : i >= 0 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S1[i] -> [i, 1]; S0[i] -> [i, 0] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S1[i] -> [i]; S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #76
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : sor_2D_sor_2D/scop_0
	 *   for : 0 <= t <= M (stride = 1)
	 *     block : 
	 *       for : t <= i <= L-t (stride = 1)
	 *         for : t <= j <= L-t (stride = 1)
	 *           S0 (depth = 3) [X[i][j]=add(div(add(Y[i-1][j],add(Y[i+1][j],add(Y[i][j-1],Y[i][j+1]))),IntExpr(8)),div(Y[i][j],IntExpr(2)))]
	 *           ([X[i][j]]) = f([Y[i-1][j], Y[i+1][j], Y[i][j-1], Y[i][j+1], Y[i][j]])
	 *           (Domain = [M,L] -> {S0[t,i,j] : (-t+j >= 0) and (-t-j+L >= 0) and (-t+i >= 0) and (-t-i+L >= 0) and (t >= 0) and (-t+M >= 0)})
	 *       for : t+1 <= i <= L-t+1 (stride = 1)
	 *         for : t+1 <= j <= L-t+1 (stride = 1)
	 *           S1 (depth = 3) [Y[i][j]=add(div(add(X[i-1][j],add(X[i+1][j],add(X[i][j-1],X[i][j+1]))),IntExpr(8)),div(X[i][j],IntExpr(2)))]
	 *           ([Y[i][j]]) = f([X[i-1][j], X[i+1][j], X[i][j-1], X[i][j+1], X[i][j]])
	 *           (Domain = [M,L] -> {S1[t,i,j] : (-t+j-1 >= 0) and (-t-j+L+1 >= 0) and (-t+i-1 >= 0) and (-t-i+L+1 >= 0) and (t >= 0) and (-t+M >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_sor_2D_GScopRoot_sor_2D_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_sor_2D_GScopRoot_sor_2D_scop_0_";
		// SCoP Extraction Data
		String domains = "[M, L] -> { S0[t, i, j] : j >= t and j <= L - t and i >= t and i <= L - t and t >= 0 and t <= M; S1[t, i, j] : j >= 1 + t and j <= 1 + L - t and i >= 1 + t and i <= 1 + L - t and t >= 0 and t <= M }";
		String idSchedules = "[M, L] -> { S0[t, i, j] -> [0, t, 0, i, 0, j, 0]; S1[t, i, j] -> [0, t, 1, i, 0, j, 0] }";
		String writes = "[M, L] -> { S0[t, i, j] -> X[i, j]; S1[t, i, j] -> Y[i, j] }";
		String reads = "[M, L] -> { S1[t, i, j] -> X[i, 1 + j]; S1[t, i, j] -> X[1 + i, j]; S1[t, i, j] -> X[i, j]; S1[t, i, j] -> X[-1 + i, j]; S1[t, i, j] -> X[i, -1 + j]; S0[t, i, j] -> Y[i, 1 + j]; S0[t, i, j] -> Y[1 + i, j]; S0[t, i, j] -> Y[i, j]; S0[t, i, j] -> Y[-1 + i, j]; S0[t, i, j] -> Y[i, -1 + j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[M, L] -> { S1[t, i, j] -> S0[t', i', L - t'] : t' <= -1 + t and i' <= 1 + L - t and t' >= 0 and t <= M and j >= 1 + t and j <= 1 + L - t and i' <= i and i' >= L + t - j - t' and t' <= -2 + i and t' <= L - i and i' >= -1 + L + i - j - t'; S1[t, i, j] -> S0[t, i', j'] : t >= 0 and j >= 1 + t and i >= 1 + t and i' <= L - t and j' >= -1 + i + j - i' and j' <= L - t and j' <= 1 + i + j - i' and j' >= -1 - i + j + i' and j' <= 1 - i + j + i' and t <= M; S1[t, 1 + L - t, j] -> S0[-1 + t, 1 + L - t, j'] : j' >= -1 + j and j' <= L - t and t <= M and t >= 1 and j' <= 1 + j and j >= 1 + t; S1[t, i, j] -> S0[-1 + L - i, 1 + i, j] : j >= 1 + t and j <= 1 + L - t and i <= -1 + L and t <= M and i >= L - t and i >= 1 + t and i <= 1 + L - t; S1[t, i, 1 + L - t] -> S0[-1 + t, 1 + i, 1 + L - t] : t >= 1 and t <= M and i >= 1 + t and i <= -1 + L - t; S0[t, i, j] -> S1[-1 + t, i', j'] : i <= L - t and j <= L - t and t >= 1 and t <= M and j' >= j and j >= t and i' >= i and j' <= 1 + i + j - i' and i >= t; S0[t, i, j] -> S1[-1 + t, -1 + i, j] : j >= t and j <= L - t and i >= 2 + t and i <= L - t and t >= 1 and t <= M; S0[t, i, j] -> S1[-2 + i, -1 + i, j] : j >= t and j <= L - t and i >= t and i <= L - t and i <= 1 + t and t <= M and i >= 2; S0[t, i, j] -> S1[-1 + t, i, -1 + j] : j >= 2 + t and j <= L - t and i >= t and i <= L - t and t >= 1 and t <= M; S0[t, i, j] -> S1[-2 + j, i, -1 + j] : j >= t and j <= L - t and i >= t and i <= L - t and j <= 1 + t and t <= M and j >= 2 }";
		String valueBasedPlutoSchedule = "[M, L] -> { S0[t, i, j] -> [t, 2t + i, 2t + i + j, 0]; S1[t, i, j] -> [t, 1 + 2t + i, 1 + 2t + i + j, 1] }";
		String valueBasedFeautrierSchedule = "[M, L] -> { S0[t, i, j] -> [t, 0, i, j]; S1[t, i, j] -> [t, 1, i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[M, L] -> { S0[t, i, j] -> S0[-1 + t, i, j] : j >= t and j <= L - t and i >= t and i <= L - t and t >= 1 and t <= M; S0[t, i, j] -> S1[-1 + t, i', j'] : t >= 1 and t <= M and i >= t and i <= L - t and j <= L - t and j' <= 1 + i + j - i' and j' <= 1 - i + j + i' and j' >= -1 + i + j - i' and j >= t and j' >= t and j' >= -1 - i + j + i' and i' >= t; S0[t, i, j] -> S1[-2 + i, -1 + i, j] : i >= 2 and i <= 1 + t and j >= t and i >= t and i <= L - t and j <= L - t and t <= M; S0[t, i, j] -> S1[-2 + j, i, -1 + j] : j >= 2 and j <= 1 + t and i >= t and j >= t and j <= L - t and i <= L - t and t <= M; S1[t, i, j] -> S0[t', i', L - t'] : t' >= 0 and t' <= -1 + t and j <= 1 + L - t and j >= 1 + t and t <= M and i' <= 1 + L - t and i' >= -1 + L + i - j - t' and i' <= i and t' <= -2 + i and t' <= L - i and i' >= L + t - j - t'; S1[t, i, j] -> S0[t', L - t', j'] : t' >= 0 and t' <= -1 + t and t' <= -2 + j and j' <= L - t and t <= M and i <= 1 + L - t and j' >= -1 + L - i + j - t' and j' <= 1 - L + i + j + t' and j' >= L + t - i - t'; S1[t, i, j] -> S0[t, i', j'] : t >= 0 and i >= 1 + t and j >= 1 + t and j' <= 1 + i + j - i' and i' <= L - t and t <= M and j' <= 1 - i + j + i' and j' <= L - t and j' >= -1 - i + j + i' and j' >= -1 + i + j - i'; S1[t, i, 1 + L - t] -> S0[-1 + L - i, 1 + i, 1 + L - t] : i <= -1 + L and i <= 1 + L - t and i >= 1 + t and i >= L - t and t <= M; S1[t, i, 1 + L - t] -> S0[-1 + t, 1 + i, 1 + L - t] : t >= 1 and t <= M and i >= 1 + t and i <= -1 + L - t; S1[t, i, j] -> S1[-1 + t, i, j] : t >= 1 and t <= M and i >= 1 + t and i <= 1 + L - t and j >= 1 + t and j <= 1 + L - t }";
		String memoryBasedPlutoSchedule = "[M, L] -> { S0[t, i, j] -> [t, 2t + i, 2t + i + j, 0]; S1[t, i, j] -> [t, 1 + 2t + i, 1 + 2t + i + j, 1] }";
		String memoryBasedFeautrierSchedule = "[M, L] -> { S0[t, i, j] -> [t, 0, i, j]; S1[t, i, j] -> [t, 1, i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #77
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : testScalarize_testScalarize/scop_0
	 *   block : 
	 *     for : 0 <= i <= 3 (stride = 1)
	 *       S0 (depth = 1) [scal_tab[i]=IntExpr(i)]
	 *       ([scal_tab[i]]) = f([])
	 *       (Domain = [] -> {S0[i] : (i >= 0) and (-i+3 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_test_GScopRoot_testScalarize_scop_0_ () {
		String path = "polymodel_others_test_GScopRoot_testScalarize_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S0[i] : i >= 0 and i <= 3 }";
		String idSchedules = "{ S0[i] -> [0, i, 0] }";
		String writes = "{ S0[i] -> scal_tab[i] }";
		String reads = "{  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{  }";
		String valueBasedPlutoSchedule = "{ S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "{ S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{  }";
		String memoryBasedPlutoSchedule = "{ S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "{ S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #78
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : main_main/scop_0
	 *   for : 0 <= i <= 15 (stride = 1)
	 *     if : y > x
	 *       block : 
	 *         S0 (depth = 1) [z=IntExpr(x-1)]
	 *         ([z]) = f([])
	 *         (Domain = [y,x] -> {S0[i] : (y-x-1 >= 0) and (i >= 0) and (-i+15 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_blockremover_test_GScopRoot_main_scop_0_ () {
		String path = "polymodel_others_blockremover_test_GScopRoot_main_scop_0_";
		// SCoP Extraction Data
		String domains = "[y, x] -> { S0[i] : x <= -1 + y and i >= 0 and i <= 15 }";
		String idSchedules = "[y, x] -> { S0[i] -> [0, i, 0] }";
		String writes = "[y, x] -> { S0[i] -> z[] }";
		String reads = "[y, x] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[y, x] -> {  }";
		String valueBasedPlutoSchedule = "[y, x] -> { S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "[y, x] -> { S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[y, x] -> { S0[i] -> S0[-1 + i] : x <= -1 + y and i >= 1 and i <= 15 }";
		String memoryBasedPlutoSchedule = "[y, x] -> { S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "[y, x] -> { S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #79
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test2_test2/scop_2
	 *   for : 0 <= i <= 9 (stride = 1)
	 *     S0 (depth = 1) [tab[i]=IntExpr(i)]
	 *     ([tab[i]]) = f([])
	 *     (Domain = [] -> {S0[i] : (i >= 0) and (-i+9 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_external_tests_test_GScopRoot_test2_scop_2_ () {
		String path = "polymodel_others_external_tests_test_GScopRoot_test2_scop_2_";
		// SCoP Extraction Data
		String domains = "{ S0[i] : i >= 0 and i <= 9 }";
		String idSchedules = "{ S0[i] -> [0, i, 0] }";
		String writes = "{ S0[i] -> tab[i] }";
		String reads = "{  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{  }";
		String valueBasedPlutoSchedule = "{ S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "{ S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{  }";
		String memoryBasedPlutoSchedule = "{ S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "{ S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #80
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test2_test2/scop_1
	 *   for : 1 <= i <= 6 (stride = 1)
	 *     S0 (depth = 1) [tab[i]=add(tab[i-1],IntExpr(1))]
	 *     ([tab[i]]) = f([tab[i-1]])
	 *     (Domain = [] -> {S0[i] : (i-1 >= 0) and (-i+6 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_external_tests_test_GScopRoot_test2_scop_1_ () {
		String path = "polymodel_others_external_tests_test_GScopRoot_test2_scop_1_";
		// SCoP Extraction Data
		String domains = "{ S0[i] : i >= 1 and i <= 6 }";
		String idSchedules = "{ S0[i] -> [0, i, 0] }";
		String writes = "{ S0[i] -> tab[i] }";
		String reads = "{ S0[i] -> tab[-1 + i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S0[i] -> S0[-1 + i] : i >= 2 and i <= 6 }";
		String valueBasedPlutoSchedule = "{ S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "{ S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S0[i] -> S0[-1 + i] : i >= 2 and i <= 6 }";
		String memoryBasedPlutoSchedule = "{ S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "{ S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #81
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test2_test2/scop_0
	 *   for : 4 <= i <= 9 (stride = 1)
	 *     S0 (depth = 1) [tab[i]=mul(tab[i],IntExpr(2))]
	 *     ([tab[i]]) = f([tab[i]])
	 *     (Domain = [] -> {S0[i] : (i-4 >= 0) and (-i+9 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_external_tests_test_GScopRoot_test2_scop_0_ () {
		String path = "polymodel_others_external_tests_test_GScopRoot_test2_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S0[i] : i >= 4 and i <= 9 }";
		String idSchedules = "{ S0[i] -> [0, i, 0] }";
		String writes = "{ S0[i] -> tab[i] }";
		String reads = "{ S0[i] -> tab[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{  }";
		String valueBasedPlutoSchedule = "{ S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "{ S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{  }";
		String memoryBasedPlutoSchedule = "{ S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "{ S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #82
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : orig_orig/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= N-i-1 (stride = 1)
	 *         S0 (depth = 2) [x[i]=div(x[i],IntExpr(a))]
	 *         ([x[i]]) = f([x[i]])
	 *         (Domain = [N,a] -> {S0[i,j] : (j >= 0) and (-i-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_LoopCoalescing_test_GScopRoot_orig_scop_0_ () {
		String path = "polymodel_others_LoopCoalescing_test_GScopRoot_orig_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, a] -> { S0[i, j] : j >= 0 and j <= -1 + N - i and i >= 0 and i <= -1 + N }";
		String idSchedules = "[N, a] -> { S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[N, a] -> { S0[i, j] -> x[i] }";
		String reads = "[N, a] -> { S0[i, j] -> x[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, a] -> { S0[i, j] -> S0[i, -1 + j] : j >= 1 and j <= -1 + N - i and i >= 0 and i <= -1 + N }";
		String valueBasedPlutoSchedule = "[N, a] -> { S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[N, a] -> { S0[i, j] -> [j, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, a] -> { S0[i, j] -> S0[i, -1 + j] : i >= 0 and j >= 1 and j <= -1 + N - i }";
		String memoryBasedPlutoSchedule = "[N, a] -> { S0[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "[N, a] -> { S0[i, j] -> [j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #83
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : dontpass_dontpass/scop_0
	 *   block : 
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       if : (i < 0) or (i > 0)
	 *         S0 (depth = 1) [a=add(a,IntExpr(i))]
	 *         ([a]) = f([a])
	 *         (Domain = [n] -> {S0[i] : (i-1 >= 0) and (-i+n-1 >= 0)})
	 *       else
	 *         S1 (depth = 1) [a=IntExpr(0)]
	 *         ([a]) = f([])
	 *         (Domain = [n] -> {S1[i] : (i = 0) and (n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_testif_GScopRoot_dontpass_scop_0_ () {
		String path = "polymodel_src_corner_cases_testif_GScopRoot_dontpass_scop_0_";
		// SCoP Extraction Data
		String domains = "[n] -> { S1[0] : n >= 1; S0[i] : i >= 1 and i <= -1 + n }";
		String idSchedules = "[n] -> { S1[i] -> [0, i, 1]; S0[i] -> [0, i, 0] }";
		String writes = "[n] -> { S0[i] -> a[]; S1[i] -> a[] }";
		String reads = "[n] -> { S0[i] -> a[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S0[1] -> S1[0] : n >= 2; S0[i] -> S0[-1 + i] : i >= 2 and i <= -1 + n }";
		String valueBasedPlutoSchedule = "[n] -> { S0[i] -> [i, 0]; S1[i] -> [0, 1] }";
		String valueBasedFeautrierSchedule = "[n] -> { S0[i] -> [i]; S1[i] -> [0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S0[1] -> S1[0] : n >= 2; S0[i] -> S0[-1 + i] : i >= 2 and i <= -1 + n }";
		String memoryBasedPlutoSchedule = "[n] -> { S0[i] -> [i, 0]; S1[i] -> [0, 1] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S0[i] -> [i]; S1[i] -> [0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #84
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : pass2_pass2/scop_0
	 *   block : 
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       if : i != 0
	 *         S0 (depth = 1) [a=add(a,IntExpr(i))]
	 *         ([a]) = f([a])
	 *         (Domain = [n] -> {S0[i] : (i-1 >= 0) and (-i+n-1 >= 0)})
	 *       else
	 *         S1 (depth = 1) [a=IntExpr(0)]
	 *         ([a]) = f([])
	 *         (Domain = [n] -> {S1[i] : (i = 0) and (n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_testif_GScopRoot_pass2_scop_0_ () {
		String path = "polymodel_src_corner_cases_testif_GScopRoot_pass2_scop_0_";
		// SCoP Extraction Data
		String domains = "[n] -> { S1[0] : n >= 1; S0[i] : i >= 1 and i <= -1 + n }";
		String idSchedules = "[n] -> { S1[i] -> [0, i, 1]; S0[i] -> [0, i, 0] }";
		String writes = "[n] -> { S0[i] -> a[]; S1[i] -> a[] }";
		String reads = "[n] -> { S0[i] -> a[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S0[1] -> S1[0] : n >= 2; S0[i] -> S0[-1 + i] : i >= 2 and i <= -1 + n }";
		String valueBasedPlutoSchedule = "[n] -> { S0[i] -> [i, 0]; S1[i] -> [0, 1] }";
		String valueBasedFeautrierSchedule = "[n] -> { S0[i] -> [i]; S1[i] -> [0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S0[1] -> S1[0] : n >= 2; S0[i] -> S0[-1 + i] : i >= 2 and i <= -1 + n }";
		String memoryBasedPlutoSchedule = "[n] -> { S0[i] -> [i, 0]; S1[i] -> [0, 1] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S0[i] -> [i]; S1[i] -> [0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #85
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : pass1_pass1/scop_0
	 *   block : 
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       if : (i < 0) or (i > 0)
	 *         S0 (depth = 1) [a=add(a,IntExpr(i))]
	 *         ([a]) = f([a])
	 *         (Domain = [n] -> {S0[i] : (i-1 >= 0) and (-i+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_testif_GScopRoot_pass1_scop_0_ () {
		String path = "polymodel_src_corner_cases_testif_GScopRoot_pass1_scop_0_";
		// SCoP Extraction Data
		String domains = "[n] -> { S0[i] : i >= 1 and i <= -1 + n }";
		String idSchedules = "[n] -> { S0[i] -> [0, i, 0] }";
		String writes = "[n] -> { S0[i] -> a[] }";
		String reads = "[n] -> { S0[i] -> a[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S0[i] -> S0[-1 + i] : i >= 2 and i <= -1 + n }";
		String valueBasedPlutoSchedule = "[n] -> { S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "[n] -> { S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S0[i] -> S0[-1 + i] : i >= 2 and i <= -1 + n }";
		String memoryBasedPlutoSchedule = "[n] -> { S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #86
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : func_func/scop_0
	 *   block : 
	 *     S0 (depth = 0) [x[0]=IntExpr(0)]
	 *     ([x[0]]) = f([])
	 *     (Domain = [] -> {S0[] : })
	 *     for : 0 <= i <= 11 (stride = 1)
	 *       for : 0 <= j <= (-1) * 1 + [(i) / 2] (stride = 1)
	 *         S1 (depth = 2) [x[j]=x[i-j]]
	 *         ([x[j]]) = f([x[i-j]])
	 *         (Domain = [] -> {S1[i,j] : (j >= 0) and (i >= 0) and (-i+11 >= 0) and (i-2*j-2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_divs_divFailure_GScopRoot_func_scop_0_ () {
		String path = "polymodel_divs_divFailure_GScopRoot_func_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S1[i, j] : j >= 0 and i >= 0 and i <= 11 and 2j <= -2 + i; S0[] }";
		String idSchedules = "{ S0[] -> [0, 0, 0, 0, 0]; S1[i, j] -> [1, i, 0, j, 0] }";
		String writes = "{ S1[i, j] -> x[j]; S0[] -> x[0] }";
		String reads = "{ S1[i, j] -> x[i - j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{  }";
		String valueBasedPlutoSchedule = "{ S1[i, j] -> [i, j]; S0[] -> [0, 0] }";
		String valueBasedFeautrierSchedule = "{ S1[i, j] -> [i, j]; S0[] -> [0, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S1[i, j] -> S1[i', j'] : 2j = -2 + i and 2j' = 2 - i + 2i' and i <= 10 and 2i' >= -2 + i and i' <= -4 + i; S1[i, j] -> S1[-1 + i, j] : i <= 11 and j >= 0 and 2j <= -3 + i; S1[2, 0] -> S0[] }";
		String memoryBasedPlutoSchedule = "{ S0[] -> [0, 0, 0]; S1[i, j] -> [i, j, 1] }";
		String memoryBasedFeautrierSchedule = "{ S1[i, j] -> [i, j]; S0[] -> [0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #87
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : pragma_test_scop_new
	 *   block : 
	 *     S0 (depth = 0) [i=IntExpr(2)]
	 *     ([i]) = f([])
	 *     (Domain = [] -> {S0[] : })
	 *     for : 0 <= i <= 49 (stride = 1)
	 *       S1 (depth = 1) [res[i]=IntExpr(i+2)]
	 *       ([res[i]]) = f([])
	 *       (Domain = [] -> {S1[i] : (i >= 0) and (-i+49 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_test_pragma_GScopRoot_scop_new_ () {
		String path = "basics_oldFiles_test_pragma_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "{ S1[i] : i >= 0 and i <= 49; S0[] }";
		String idSchedules = "{ S0[] -> [0, 0, 0]; S1[i] -> [1, i, 0] }";
		String writes = "{ S1[i] -> res[i]; S0[] -> i[] }";
		String reads = "{  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{  }";
		String valueBasedPlutoSchedule = "{ S0[] -> [0]; S1[i] -> [i] }";
		String valueBasedFeautrierSchedule = "{ S0[] -> [0]; S1[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{  }";
		String memoryBasedPlutoSchedule = "{ S0[] -> [0]; S1[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "{ S0[] -> [0]; S1[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #88
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : sobel7x7_sobel7x7/scop_0
	 *   block : 
	 *     for : [(7) / 2] <= i <= ([(7) / 2]) * -1 + (W-1) * 1 (stride = 1)
	 *       for : [(7) / 2] <= j <= ([(7) / 2]) * -1 + (W-1) * 1 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [tmp=IntExpr(0)]
	 *           ([tmp]) = f([])
	 *           (Domain = [W] -> {S0[i,j] : (j-4 >= 0) and (-j+W-5 >= 0) and (i-4 >= 0) and (-i+W-5 >= 0)})
	 *           for : 1 <= ii <= (-1) * 1 + [(7) / 2] (stride = 1)
	 *             for : 1 <= jj <= (-1) * 1 + [(7) / 2] (stride = 1)
	 *               S1 (depth = 4) [tmp=add(tmp,mul(sub(sub(add(im_in[i-ii][j-jj],im_in[i-ii][j+jj]),im_in[i+ii][j-jj]),im_in[i+ii][j+jj]),sobel[ii][jj]))]
	 *               ([tmp]) = f([tmp, im_in[i-ii][j-jj], im_in[i-ii][j+jj], im_in[i+ii][j-jj], im_in[i+ii][j+jj], sobel[ii][jj]])
	 *               (Domain = [W] -> {S1[i,j,ii,jj] : (jj-1 >= 0) and (ii-1 >= 0) and (-jj+2 >= 0) and (-ii+2 >= 0) and (j-4 >= 0) and (-j+W-5 >= 0) and (i-4 >= 0) and (-i+W-5 >= 0)})
	 *           S2 (depth = 2) [im_out[i][j]=add(tmp,mul(im_in[i][j],sobel[0][0]))]
	 *           ([im_out[i][j]]) = f([tmp, im_in[i][j], sobel[0][0]])
	 *           (Domain = [W] -> {S2[i,j] : (j-4 >= 0) and (-j+W-5 >= 0) and (i-4 >= 0) and (-i+W-5 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_sobel7x7_GScopRoot_sobel7x7_scop_0_ () {
		String path = "basics_oldFiles_sobel7x7_GScopRoot_sobel7x7_scop_0_";
		// SCoP Extraction Data
		String domains = "[W] -> { S1[i, j, ii, jj] : jj >= 1 and ii >= 1 and jj <= 2 and ii <= 2 and j >= 4 and j <= -5 + W and i >= 4 and i <= -5 + W; S0[i, j] : j >= 4 and j <= -5 + W and i >= 4 and i <= -5 + W; S2[i, j] : j >= 4 and j <= -5 + W and i >= 4 and i <= -5 + W }";
		String idSchedules = "[W] -> { S1[i, j, ii, jj] -> [0, i, 0, j, 1, ii, 0, jj, 0]; S2[i, j] -> [0, i, 0, j, 2, 0, 0, 0, 0]; S0[i, j] -> [0, i, 0, j, 0, 0, 0, 0, 0] }";
		String writes = "[W] -> { S2[i, j] -> im_out[i, j]; S0[i, j] -> tmp[]; S1[i, j, ii, jj] -> tmp[] }";
		String reads = "[W] -> { S2[i, j] -> im_in[i, j]; S2[i, j] -> tmp[]; S1[i, j, ii, jj] -> im_in[i + ii, j + jj]; S1[i, j, ii, jj] -> im_in[i - ii, j + jj]; S1[i, j, ii, jj] -> im_in[i + ii, j - jj]; S1[i, j, ii, jj] -> im_in[i - ii, j - jj]; S1[i, j, ii, jj] -> tmp[]; S1[i, j, ii, jj] -> sobel[ii, jj]; S2[i, j] -> sobel[0, 0] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[W] -> { S2[i, j] -> S1[i, j, 2, 2] : j >= 4 and j <= -5 + W and i >= 4 and i <= -5 + W; S1[i, j, 1, 1] -> S0[i, j] : i >= 4 and j >= 4 and i <= -5 + W and j <= -5 + W; S1[i, j, ii, 2] -> S1[i, j, ii, 1] : i >= 4 and ii >= 1 and i <= -5 + W and ii <= 2 and j >= 4 and j <= -5 + W; S1[i, j, 2, 1] -> S1[i, j, 1, 2] : i >= 4 and j >= 4 and i <= -5 + W and j <= -5 + W }";
		String valueBasedPlutoSchedule = "[W] -> { S2[i, j] -> [i, j, 2 + i, 4 + i + j, 1]; S1[i, j, ii, jj] -> [i, j, i + ii, i + j + ii + jj, 0]; S0[i, j] -> [0, 0, i, j, 2] }";
		String valueBasedFeautrierSchedule = "[W] -> { S1[i, j, ii, jj] -> [2ii + jj, j, ii, i]; S0[i, j] -> [0, i, j, 0]; S2[i, j] -> [7, i, j, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[W] -> { S1[i, j, ii, 2] -> S1[i, j, ii, 1] : i >= 4 and i <= -5 + W and j >= 4 and j <= -5 + W and ii <= 2 and ii >= 1; S1[i, j, 2, 1] -> S1[i, j, 1, 2] : i >= 4 and i <= -5 + W and j >= 4 and j <= -5 + W; S1[i, j, 1, 1] -> S0[i, j] : i >= 4 and i <= -5 + W and j >= 4 and j <= -5 + W; S2[i, j] -> S1[i, j, 2, 2] : j >= 4 and j <= -5 + W and i >= 4 and i <= -5 + W; S0[i, j] -> S2[i, -1 + j] : i >= 4 and i <= -5 + W and j >= 5 and j <= -5 + W; S0[i, 4] -> S2[-1 + i, -5 + W] : i >= 5 and i <= -5 + W; S0[i, j] -> S1[i, -1 + j, 2, 2] : j >= 5 and j <= -5 + W and i >= 4 and i <= -5 + W; S0[i, 4] -> S1[-1 + i, -5 + W, 2, 2] : i <= -5 + W and i >= 5 }";
		String memoryBasedPlutoSchedule = "[W] -> { S2[i, j] -> [i, j, 2 + j, 4 + 2j, 2]; S1[i, j, ii, jj] -> [i, j, j + ii, 2j + ii + jj, 1]; S0[i, j] -> [i, j, 1 + j, 2 + 2j, 0] }";
		String memoryBasedFeautrierSchedule = "[W] -> { S1[i, j, ii, jj] -> [i, 6j + 2ii + jj, ii, j]; S0[i, j] -> [i, 2 + 6j, 0, 0]; S2[i, j] -> [i, 7 + 6j, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #89
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : STAP_Mat_Invert_STAP_Mat_Invert/scop_0
	 *   block : 
	 *     for : 0 <= i <= mat_ch_dim1-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [pivot[0]=inv[i][i][0]]
	 *         ([pivot[0]]) = f([inv[i][i][0]])
	 *         (Domain = [mat_ch_dim1,mat_ch_dim2,matinv_ch_dim1,matinv_ch_dim2] -> {S0[i] : (i >= 0) and (-i+mat_ch_dim1-1 >= 0)})
	 *         S1 (depth = 1) [pivot[1]=inv[i][i][1]]
	 *         ([pivot[1]]) = f([inv[i][i][1]])
	 *         (Domain = [mat_ch_dim1,mat_ch_dim2,matinv_ch_dim1,matinv_ch_dim2] -> {S1[i] : (i >= 0) and (-i+mat_ch_dim1-1 >= 0)})
	 *         for : i <= j <= 2*mat_ch_dim2-1 (stride = 1)
	 *           block : 
	 *             S2 (depth = 2) [re=inv[i][j][0]]
	 *             ([re]) = f([inv[i][j][0]])
	 *             (Domain = [mat_ch_dim1,mat_ch_dim2,matinv_ch_dim1,matinv_ch_dim2] -> {S2[i,j] : (-i+j >= 0) and (-j+2*mat_ch_dim2-1 >= 0) and (i >= 0) and (-i+mat_ch_dim1-1 >= 0)})
	 *             S3 (depth = 2) [im=inv[i][j][1]]
	 *             ([im]) = f([inv[i][j][1]])
	 *             (Domain = [mat_ch_dim1,mat_ch_dim2,matinv_ch_dim1,matinv_ch_dim2] -> {S3[i,j] : (-i+j >= 0) and (-j+2*mat_ch_dim2-1 >= 0) and (i >= 0) and (-i+mat_ch_dim1-1 >= 0)})
	 *             S4 (depth = 2) [inv[i][j][0]=div(add(mul(re,pivot[0]),mul(im,pivot[1])),add(mul(pivot[0],pivot[0]),mul(pivot[1],pivot[1])))]
	 *             ([inv[i][j][0]]) = f([re, pivot[0], im, pivot[1], pivot[0], pivot[0], pivot[1], pivot[1]])
	 *             (Domain = [mat_ch_dim1,mat_ch_dim2,matinv_ch_dim1,matinv_ch_dim2] -> {S4[i,j] : (-i+j >= 0) and (-j+2*mat_ch_dim2-1 >= 0) and (i >= 0) and (-i+mat_ch_dim1-1 >= 0)})
	 *             S5 (depth = 2) [inv[i][j][1]=div(sub(mul(im,pivot[0]),mul(re,pivot[1])),add(mul(pivot[0],pivot[0]),mul(pivot[1],pivot[1])))]
	 *             ([inv[i][j][1]]) = f([im, pivot[0], re, pivot[1], pivot[0], pivot[0], pivot[1], pivot[1]])
	 *             (Domain = [mat_ch_dim1,mat_ch_dim2,matinv_ch_dim1,matinv_ch_dim2] -> {S5[i,j] : (-i+j >= 0) and (-j+2*mat_ch_dim2-1 >= 0) and (i >= 0) and (-i+mat_ch_dim1-1 >= 0)})
	 *         for : 0 <= k <= mat_ch_dim2-1 (stride = 1)
	 *           if : (i > k) or (i < k)
	 *             block : 
	 *               S6 (depth = 2) [coef[0]=inv[k][i][0]]
	 *               ([coef[0]]) = f([inv[k][i][0]])
	 *               (Domain = [mat_ch_dim1,mat_ch_dim2,matinv_ch_dim1,matinv_ch_dim2] -> {S6[i,k] : ((-i+mat_ch_dim1-1 >= 0) and (k >= 0) and (i-k-1 >= 0) and (-k+mat_ch_dim2-1 >= 0)) or ((i >= 0) and (-i+mat_ch_dim1-1 >= 0) and (-i+k-1 >= 0) and (-k+mat_ch_dim2-1 >= 0))})
	 *               S7 (depth = 2) [coef[1]=inv[k][i][1]]
	 *               ([coef[1]]) = f([inv[k][i][1]])
	 *               (Domain = [mat_ch_dim1,mat_ch_dim2,matinv_ch_dim1,matinv_ch_dim2] -> {S7[i,k] : ((-i+mat_ch_dim1-1 >= 0) and (k >= 0) and (i-k-1 >= 0) and (-k+mat_ch_dim2-1 >= 0)) or ((i >= 0) and (-i+mat_ch_dim1-1 >= 0) and (-i+k-1 >= 0) and (-k+mat_ch_dim2-1 >= 0))})
	 *               for : i <= l <= 2*mat_ch_dim2-1 (stride = 1)
	 *                 block : 
	 *                   S8 (depth = 3) [inv[k][l][0]=sub(inv[k][l][0],sub(mul(coef[0],inv[i][l][0]),mul(coef[1],inv[i][l][1])))]
	 *                   ([inv[k][l][0]]) = f([inv[k][l][0], coef[0], inv[i][l][0], coef[1], inv[i][l][1]])
	 *                   (Domain = [mat_ch_dim1,mat_ch_dim2,matinv_ch_dim1,matinv_ch_dim2] -> {S8[i,k,l] : ((-i+mat_ch_dim1-1 >= 0) and (k >= 0) and (i-k-1 >= 0) and (-k+mat_ch_dim2-1 >= 0) and (-i+l >= 0) and (-l+2*mat_ch_dim2-1 >= 0)) or ((i >= 0) and (-i+mat_ch_dim1-1 >= 0) and (-i+k-1 >= 0) and (-k+mat_ch_dim2-1 >= 0) and (-i+l >= 0) and (-l+2*mat_ch_dim2-1 >= 0))})
	 *                   S9 (depth = 3) [inv[k][l][1]=sub(inv[k][l][1],add(mul(coef[0],inv[i][l][1]),mul(coef[1],inv[i][l][0])))]
	 *                   ([inv[k][l][1]]) = f([inv[k][l][1], coef[0], inv[i][l][1], coef[1], inv[i][l][0]])
	 *                   (Domain = [mat_ch_dim1,mat_ch_dim2,matinv_ch_dim1,matinv_ch_dim2] -> {S9[i,k,l] : ((-i+mat_ch_dim1-1 >= 0) and (k >= 0) and (i-k-1 >= 0) and (-k+mat_ch_dim2-1 >= 0) and (-i+l >= 0) and (-l+2*mat_ch_dim2-1 >= 0)) or ((i >= 0) and (-i+mat_ch_dim1-1 >= 0) and (-i+k-1 >= 0) and (-k+mat_ch_dim2-1 >= 0) and (-i+l >= 0) and (-l+2*mat_ch_dim2-1 >= 0))})
	 *     for : 0 <= i <= matinv_ch_dim1-1 (stride = 1)
	 *       for : 0 <= j <= matinv_ch_dim2-1 (stride = 1)
	 *         block : 
	 *           S10 (depth = 2) [matinvptr[i][j][0]=inv[i][j+mat_ch_dim2][0]]
	 *           ([matinvptr[i][j][0]]) = f([inv[i][j+mat_ch_dim2][0]])
	 *           (Domain = [mat_ch_dim1,mat_ch_dim2,matinv_ch_dim1,matinv_ch_dim2] -> {S10[i,j] : (j >= 0) and (-j+matinv_ch_dim2-1 >= 0) and (i >= 0) and (-i+matinv_ch_dim1-1 >= 0)})
	 *           S11 (depth = 2) [matinvptr[i][j][1]=inv[i][j+mat_ch_dim2][1]]
	 *           ([matinvptr[i][j][1]]) = f([inv[i][j+mat_ch_dim2][1]])
	 *           (Domain = [mat_ch_dim1,mat_ch_dim2,matinv_ch_dim1,matinv_ch_dim2] -> {S11[i,j] : (j >= 0) and (-j+matinv_ch_dim2-1 >= 0) and (i >= 0) and (-i+matinv_ch_dim1-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_STAP_Mat_Invert_GScopRoot_STAP_Mat_Invert_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_STAP_Mat_Invert_GScopRoot_STAP_Mat_Invert_scop_0_";
		// SCoP Extraction Data
		String domains = "[mat_ch_dim1, mat_ch_dim2, matinv_ch_dim1, matinv_ch_dim2] -> { S0[i] : i >= 0 and i <= -1 + mat_ch_dim1; S2[i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S4[i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S7[i, k] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2); S10[i, j] : j >= 0 and j <= -1 + matinv_ch_dim2 and i >= 0 and i <= -1 + matinv_ch_dim1; S3[i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S11[i, j] : j >= 0 and j <= -1 + matinv_ch_dim2 and i >= 0 and i <= -1 + matinv_ch_dim1; S9[i, k, l] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S6[i, k] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2); S8[i, k, l] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S1[i] : i >= 0 and i <= -1 + mat_ch_dim1; S5[i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1 }";
		String idSchedules = "[mat_ch_dim1, mat_ch_dim2, matinv_ch_dim1, matinv_ch_dim2] -> { S10[i, j] -> [1, i, 0, j, 0, 0, 0]; S1[i] -> [0, i, 1, 0, 0, 0, 0]; S9[i, k, l] -> [0, i, 3, k, 2, l, 1]; S11[i, j] -> [1, i, 0, j, 1, 0, 0]; S6[i, k] -> [0, i, 3, k, 0, 0, 0]; S5[i, j] -> [0, i, 2, j, 3, 0, 0]; S2[i, j] -> [0, i, 2, j, 0, 0, 0]; S3[i, j] -> [0, i, 2, j, 1, 0, 0]; S4[i, j] -> [0, i, 2, j, 2, 0, 0]; S0[i] -> [0, i, 0, 0, 0, 0, 0]; S7[i, k] -> [0, i, 3, k, 1, 0, 0]; S8[i, k, l] -> [0, i, 3, k, 2, l, 0] }";
		String writes = "[mat_ch_dim1, mat_ch_dim2, matinv_ch_dim1, matinv_ch_dim2] -> { S1[i] -> pivot[1]; S4[i, j] -> inv[i, j, 0]; S6[i, k] -> coef[0]; S11[i, j] -> matinvptr[i, j, 1]; S2[i, j] -> re[]; S0[i] -> pivot[0]; S3[i, j] -> im[]; S5[i, j] -> inv[i, j, 1]; S7[i, k] -> coef[1]; S8[i, k, l] -> inv[k, l, 0]; S9[i, k, l] -> inv[k, l, 1]; S10[i, j] -> matinvptr[i, j, 0] }";
		String reads = "[mat_ch_dim1, mat_ch_dim2, matinv_ch_dim1, matinv_ch_dim2] -> { S9[i, k, l] -> coef[1]; S9[i, k, l] -> coef[0]; S2[i, j] -> inv[i, j, 0]; S4[i, j] -> re[]; S7[i, k] -> inv[k, i, 1]; S5[i, j] -> pivot[1]; S5[i, j] -> pivot[0]; S4[i, j] -> im[]; S10[i, j] -> inv[i, mat_ch_dim2 + j, 0]; S3[i, j] -> inv[i, j, 1]; S6[i, k] -> inv[k, i, 0]; S0[i] -> inv[i, i, 0]; S5[i, j] -> im[]; S8[i, k, l] -> coef[1]; S8[i, k, l] -> coef[0]; S11[i, j] -> inv[i, mat_ch_dim2 + j, 1]; S8[i, k, l] -> inv[i, l, 1]; S8[i, k, l] -> inv[i, l, 0]; S8[i, k, l] -> inv[k, l, 0]; S9[i, k, l] -> inv[i, l, 1]; S9[i, k, l] -> inv[k, l, 1]; S9[i, k, l] -> inv[i, l, 0]; S4[i, j] -> pivot[1]; S4[i, j] -> pivot[0]; S1[i] -> inv[i, i, 1]; S5[i, j] -> re[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[mat_ch_dim1, mat_ch_dim2, matinv_ch_dim1, matinv_ch_dim2] -> { S11[i, j] -> S9[mat_ch_dim2 + j, i, mat_ch_dim2 + j] : j >= 0 and j <= -1 + matinv_ch_dim2 and i <= -1 + mat_ch_dim2 and i <= -1 + matinv_ch_dim1 and j <= -2 + mat_ch_dim1 - mat_ch_dim2 and i >= 0 and j <= -1 + mat_ch_dim2; S11[i, j] -> S9[-1 + mat_ch_dim1, i, mat_ch_dim2 + j] : (i >= 0 and i <= -1 + matinv_ch_dim1 and i <= -1 + mat_ch_dim2 and i <= -2 + mat_ch_dim1 and j >= -1 + mat_ch_dim1 - mat_ch_dim2 and j >= 0 and j <= -1 + matinv_ch_dim2 and j <= -1 + mat_ch_dim2) or (mat_ch_dim1 >= 1 and i >= mat_ch_dim1 and i <= -1 + matinv_ch_dim1 and i <= -1 + mat_ch_dim2 and j >= 0 and j <= -1 + matinv_ch_dim2 and j <= -1 + mat_ch_dim2); S10[i, j] -> S4[i, mat_ch_dim2 + j] : j <= -1 + mat_ch_dim2 and j <= -1 + matinv_ch_dim2 and i <= -1 + mat_ch_dim1 and i <= -1 + matinv_ch_dim1 and i >= mat_ch_dim2 and j >= -mat_ch_dim2 + i; S10[-1 + mat_ch_dim1, j] -> S4[-1 + mat_ch_dim1, mat_ch_dim2 + j] : j >= 0 and j <= -1 + matinv_ch_dim2 and mat_ch_dim2 >= mat_ch_dim1 and matinv_ch_dim1 >= mat_ch_dim1 and j <= -1 + mat_ch_dim2 and mat_ch_dim1 >= 1; S7[i, -1 + i] -> S5[-1 + i, i] : i <= -1 + mat_ch_dim1 and i <= mat_ch_dim2 and i >= 1; S3[i, j] -> S9[-1 + i, i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and i >= 1; S5[i, j] -> S2[i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S10[i, j] -> S8[mat_ch_dim2 + j, i, mat_ch_dim2 + j] : j >= 0 and j <= -1 + matinv_ch_dim2 and i <= -1 + mat_ch_dim2 and i <= -1 + matinv_ch_dim1 and j <= -2 + mat_ch_dim1 - mat_ch_dim2 and i >= 0 and j <= -1 + mat_ch_dim2; S10[i, j] -> S8[-1 + mat_ch_dim1, i, mat_ch_dim2 + j] : (i >= 0 and i <= -1 + matinv_ch_dim1 and i <= -1 + mat_ch_dim2 and i <= -2 + mat_ch_dim1 and j >= -1 + mat_ch_dim1 - mat_ch_dim2 and j >= 0 and j <= -1 + matinv_ch_dim2 and j <= -1 + mat_ch_dim2) or (mat_ch_dim1 >= 1 and i >= mat_ch_dim1 and i <= -1 + matinv_ch_dim1 and i <= -1 + mat_ch_dim2 and j >= 0 and j <= -1 + matinv_ch_dim2 and j <= -1 + mat_ch_dim2); S9[i, k, l] -> S4[i, l] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S8[i, k, l] -> S7[i, k] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S0[i] -> S8[-1 + i, i, i] : i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and i >= 1; S5[i, j] -> S1[i] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S11[i, j] -> S5[i, mat_ch_dim2 + j] : j <= -1 + mat_ch_dim2 and j <= -1 + matinv_ch_dim2 and i <= -1 + mat_ch_dim1 and i <= -1 + matinv_ch_dim1 and i >= mat_ch_dim2 and j >= -mat_ch_dim2 + i; S11[-1 + mat_ch_dim1, j] -> S5[-1 + mat_ch_dim1, mat_ch_dim2 + j] : j >= 0 and j <= -1 + matinv_ch_dim2 and mat_ch_dim2 >= mat_ch_dim1 and matinv_ch_dim1 >= mat_ch_dim1 and j <= -1 + mat_ch_dim2 and mat_ch_dim1 >= 1; S9[i, k, l] -> S9[-1 + i, k, l] : (i >= 1 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -2 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S5[i, j] -> S0[i] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S4[i, j] -> S2[i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S4[i, j] -> S0[i] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S7[i, k] -> S9[-1 + i, k, i] : (i >= 1 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2) or (i <= -1 + 2mat_ch_dim2 and i <= -1 + mat_ch_dim1 and k >= 0 and k <= -2 + i and k <= -1 + mat_ch_dim2); S9[i, k, l] -> S5[i, l] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S9[i, -1 + i, l] -> S5[-1 + i, l] : i <= -1 + mat_ch_dim1 and i >= 1 and l <= -1 + 2mat_ch_dim2 and i <= mat_ch_dim2 and l >= i; S4[i, j] -> S1[i] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S2[i, j] -> S8[-1 + i, i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and i >= 1; S4[i, j] -> S3[i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S8[i, k, l] -> S6[i, k] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S5[i, j] -> S3[i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S8[i, k, l] -> S5[i, l] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S6[i, -1 + i] -> S4[-1 + i, i] : i <= -1 + mat_ch_dim1 and i <= mat_ch_dim2 and i >= 1; S8[i, k, l] -> S4[i, l] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S8[i, -1 + i, l] -> S4[-1 + i, l] : i <= -1 + mat_ch_dim1 and i >= 1 and l <= -1 + 2mat_ch_dim2 and i <= mat_ch_dim2 and l >= i; S8[i, k, l] -> S8[-1 + i, k, l] : (i >= 1 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -2 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S9[i, k, l] -> S7[i, k] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S6[i, k] -> S8[-1 + i, k, i] : (i >= 1 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2) or (i <= -1 + 2mat_ch_dim2 and i <= -1 + mat_ch_dim1 and k >= 0 and k <= -2 + i and k <= -1 + mat_ch_dim2); S1[i] -> S9[-1 + i, i, i] : i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and i >= 1; S9[i, k, l] -> S6[i, k] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) }";
		String valueBasedPlutoSchedule = "[mat_ch_dim1, mat_ch_dim2, matinv_ch_dim1, matinv_ch_dim2] -> { S0[i] -> [i, i, 0, 3]; S11[i, j] -> [mat_ch_dim1, mat_ch_dim2 + i + j, j, 0]; S8[i, k, l] -> [i, l, k, 8]; S3[i, j] -> [i, j, 0, 1]; S6[i, k] -> [i, i, k, 5]; S4[i, j] -> [i, j, 0, 7]; S2[i, j] -> [i, j, 0, 2]; S7[i, k] -> [i, i, k, 6]; S1[i] -> [i, i, 0, 0]; S5[i, j] -> [i, j, 0, 4]; S9[i, k, l] -> [i, l, k, 9]; S10[i, j] -> [mat_ch_dim1, mat_ch_dim2 + i + j, j, 0] }";
		String valueBasedFeautrierSchedule = "[mat_ch_dim1, mat_ch_dim2, matinv_ch_dim1, matinv_ch_dim2] -> { S0[i] -> [i, 0, 0, 0]; S11[i, j] -> [mat_ch_dim1, 0, i, j]; S8[i, k, l] -> [i, 2, k, l]; S3[i, j] -> [i, 0, j, 0]; S6[i, k] -> [i, 0, k, 0]; S4[i, j] -> [i, 1, j, 0]; S2[i, j] -> [i, 0, j, 0]; S7[i, k] -> [i, 0, k, 0]; S1[i] -> [i, 0, 0, 0]; S5[i, j] -> [i, 1, j, 0]; S9[i, k, l] -> [i, 2, k, l]; S10[i, j] -> [mat_ch_dim1, 0, i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[mat_ch_dim1, mat_ch_dim2, matinv_ch_dim1, matinv_ch_dim2] -> { S8[i, k, l] -> S5[i, l] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S6[i, k] -> S9[i, -1 + k, l] : (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 2 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i <= -1 + mat_ch_dim1 and k >= 1 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S6[i, 0] -> S9[-1 + i, -2 + i, l] : mat_ch_dim2 >= 2 and i >= mat_ch_dim2 and i <= 1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= -1 + i and l <= -1 + 2mat_ch_dim2; S6[i, 0] -> S9[-1 + i, -1 + mat_ch_dim2, l] : (i >= 2 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= -1 + i and l <= -1 + 2mat_ch_dim2) or (i >= 1 and i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= -1 + i and l <= -1 + 2mat_ch_dim2); S6[2, 0] -> S9[1, 0, 1] : mat_ch_dim2 = 1 and mat_ch_dim1 >= 3; S6[i, 1 + i] -> S9[i, -1 + i, l] : i >= 1 and i <= -2 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2; S4[i, j] -> S2[i, j] : i >= 0 and i <= -1 + mat_ch_dim1 and j >= i and j <= -1 + 2mat_ch_dim2; S11[i, j] -> S5[i, mat_ch_dim2 + j] : j <= -1 + mat_ch_dim2 and j <= -1 + matinv_ch_dim2 and i <= -1 + mat_ch_dim1 and i <= -1 + matinv_ch_dim1 and i >= mat_ch_dim2 and j >= -mat_ch_dim2 + i; S11[-1 + mat_ch_dim1, j] -> S5[-1 + mat_ch_dim1, mat_ch_dim2 + j] : j >= 0 and j <= -1 + matinv_ch_dim2 and mat_ch_dim2 >= mat_ch_dim1 and matinv_ch_dim1 >= mat_ch_dim1 and j <= -1 + mat_ch_dim2 and mat_ch_dim1 >= 1; S7[i, k] -> S7[i, -1 + k] : (i <= -1 + mat_ch_dim1 and k >= 1 and k <= -1 + i and k <= -1 + mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 2 + i and k <= -1 + mat_ch_dim2); S7[i, 1 + i] -> S7[i, -1 + i] : i >= 1 and i <= -2 + mat_ch_dim2 and i <= -1 + mat_ch_dim1; S7[i, 0] -> S7[-1 + i, -1 + mat_ch_dim2] : (mat_ch_dim2 >= 1 and i >= 2 + mat_ch_dim2 and i <= -1 + mat_ch_dim1) or (i >= 1 and i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1); S7[i, 0] -> S7[-1 + i, -2 + i] : i >= mat_ch_dim2 and i >= 2 and i <= -1 + mat_ch_dim1 and i <= 1 + mat_ch_dim2; S8[i, k, l] -> S4[i, l] : (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S8[i, -1 + i, l] -> S4[-1 + i, l] : i >= 1 and i <= mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2; S5[i, j] -> S9[-1 + i, i, j] : i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and j >= i and j <= -1 + 2mat_ch_dim2 and i >= 1; S4[i, j] -> S3[i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S5[i, j] -> S3[i, j] : i >= 0 and i <= -1 + mat_ch_dim1 and j >= i and j <= -1 + 2mat_ch_dim2; S0[i] -> S8[-1 + i, i, i] : i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and i >= 1; S3[i, j] -> S3[i, -1 + j] : j >= 1 + i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S3[i, i] -> S3[-1 + i, -1 + 2mat_ch_dim2] : i <= -1 + mat_ch_dim1 and i <= -1 + 2mat_ch_dim2 and i >= 1; S4[i, j] -> S8[-1 + i, i, j] : i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and j >= i and j <= -1 + 2mat_ch_dim2 and i >= 1; S9[i, k, l] -> S5[i, l] : (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S9[i, -1 + i, l] -> S5[-1 + i, l] : i >= 1 and i <= mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2; S5[i, j] -> S2[i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S1[i] -> S9[-1 + i, i, i] : i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and i >= 1; S8[i, k, l] -> S8[-1 + i, k, l] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -2 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 1 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S8[i, -1 + i, l] -> S8[-1 + i, k', l] : (i >= 1 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2 and k' >= i and k' <= -1 + mat_ch_dim2) or (i <= mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2 and k' >= 0 and k' <= -2 + i); S9[i, -1 + i, l] -> S8[-1 + i, k', l] : (i <= mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2 and k' >= 0 and k' <= -2 + i) or (i >= 1 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2 and k' >= i and k' <= -1 + mat_ch_dim2); S1[i] -> S1[-1 + i] : i >= 1 and i <= -1 + mat_ch_dim1; S2[i, j] -> S2[i, -1 + j] : j >= 1 + i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S2[i, i] -> S2[-1 + i, -1 + 2mat_ch_dim2] : i <= -1 + mat_ch_dim1 and i <= -1 + 2mat_ch_dim2 and i >= 1; S0[i] -> S4[-1 + i, j] : i >= 1 and i <= -1 + mat_ch_dim1 and j >= -1 + i and j <= -1 + 2mat_ch_dim2; S9[i, k, l] -> S7[i, k] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S3[i, j] -> S5[i, -1 + j] : i >= 0 and i <= -1 + mat_ch_dim1 and j >= 1 + i and j <= -1 + 2mat_ch_dim2; S3[i, i] -> S5[-1 + i, -1 + 2mat_ch_dim2] : i >= 1 and i <= -1 + 2mat_ch_dim2 and i <= -1 + mat_ch_dim1; S7[i, -1 + i] -> S5[-1 + i, i] : i <= -1 + mat_ch_dim1 and i <= mat_ch_dim2 and i >= 1; S9[i, k, l] -> S6[i, k] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S11[i, j] -> S9[mat_ch_dim2 + j, i, mat_ch_dim2 + j] : j >= 0 and j <= -1 + matinv_ch_dim2 and i <= -1 + mat_ch_dim2 and i <= -1 + matinv_ch_dim1 and j <= -2 + mat_ch_dim1 - mat_ch_dim2 and i >= 0 and j <= -1 + mat_ch_dim2; S11[i, j] -> S9[-1 + mat_ch_dim1, i, mat_ch_dim2 + j] : (i >= 0 and i <= -1 + matinv_ch_dim1 and i <= -1 + mat_ch_dim2 and i <= -2 + mat_ch_dim1 and j >= -1 + mat_ch_dim1 - mat_ch_dim2 and j >= 0 and j <= -1 + matinv_ch_dim2 and j <= -1 + mat_ch_dim2) or (mat_ch_dim1 >= 1 and i >= mat_ch_dim1 and i <= -1 + matinv_ch_dim1 and i <= -1 + mat_ch_dim2 and j >= 0 and j <= -1 + matinv_ch_dim2 and j <= -1 + mat_ch_dim2); S7[i, k] -> S9[i, -1 + k, l] : (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 2 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i <= -1 + mat_ch_dim1 and k >= 1 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S7[i, 1 + i] -> S9[i, -1 + i, l] : i >= 1 and i <= -2 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2; S7[i, k] -> S9[-1 + i, k, i] : (i >= 1 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2) or (i <= -1 + 2mat_ch_dim2 and i <= -1 + mat_ch_dim1 and k >= 0 and k <= -2 + i and k <= -1 + mat_ch_dim2); S7[i, 0] -> S9[-1 + i, -2 + i, l] : mat_ch_dim2 >= 2 and i >= mat_ch_dim2 and i <= 1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= -1 + i and l <= -1 + 2mat_ch_dim2; S7[i, 0] -> S9[-1 + i, -1 + mat_ch_dim2, l] : (i >= 2 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= -1 + i and l <= -1 + 2mat_ch_dim2) or (i >= 1 and i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= -1 + i and l <= -1 + 2mat_ch_dim2); S7[2, 0] -> S9[1, 0, 1] : mat_ch_dim2 = 1 and mat_ch_dim1 >= 3; S5[i, j] -> S1[i] : i >= 0 and i <= -1 + mat_ch_dim1 and j >= i and j <= -1 + 2mat_ch_dim2; S6[i, k] -> S8[i, -1 + k, l] : (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 2 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i <= -1 + mat_ch_dim1 and k >= 1 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S6[i, 1 + i] -> S8[i, -1 + i, l] : i >= 1 and i <= -2 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2; S6[i, k] -> S8[-1 + i, k, i] : (i >= 1 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2) or (i <= -1 + 2mat_ch_dim2 and i <= -1 + mat_ch_dim1 and k >= 0 and k <= -2 + i and k <= -1 + mat_ch_dim2); S6[i, 0] -> S8[-1 + i, -2 + i, l] : mat_ch_dim2 >= 2 and i >= mat_ch_dim2 and i <= 1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= -1 + i and l <= -1 + 2mat_ch_dim2; S6[i, 0] -> S8[-1 + i, -1 + mat_ch_dim2, l] : (i >= 2 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= -1 + i and l <= -1 + 2mat_ch_dim2) or (i >= 1 and i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= -1 + i and l <= -1 + 2mat_ch_dim2); S6[2, 0] -> S8[1, 0, 1] : mat_ch_dim2 = 1 and mat_ch_dim1 >= 3; S3[i, j] -> S4[i, -1 + j] : i >= 0 and i <= -1 + mat_ch_dim1 and j >= 1 + i and j <= -1 + 2mat_ch_dim2; S3[i, i] -> S4[-1 + i, -1 + 2mat_ch_dim2] : i >= 1 and i <= -1 + 2mat_ch_dim2 and i <= -1 + mat_ch_dim1; S1[i] -> S4[-1 + i, j] : i >= 1 and i <= -1 + mat_ch_dim1 and j >= -1 + i and j <= -1 + 2mat_ch_dim2; S8[i, -1 + i, l] -> S9[-1 + i, k', l] : (i >= 1 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2 and k' >= i and k' <= -1 + mat_ch_dim2) or (i <= mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2 and k' >= 0 and k' <= -2 + i); S4[i, j] -> S1[i] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S2[i, j] -> S4[i, -1 + j] : i >= 0 and i <= -1 + mat_ch_dim1 and j >= 1 + i and j <= -1 + 2mat_ch_dim2; S2[i, i] -> S4[-1 + i, -1 + 2mat_ch_dim2] : i >= 1 and i <= -1 + 2mat_ch_dim2 and i <= -1 + mat_ch_dim1; S7[i, k] -> S8[i, -1 + k, l] : (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 2 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i <= -1 + mat_ch_dim1 and k >= 1 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S7[i, 0] -> S8[-1 + i, -2 + i, l] : mat_ch_dim2 >= 2 and i >= mat_ch_dim2 and i <= 1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= -1 + i and l <= -1 + 2mat_ch_dim2; S7[i, 0] -> S8[-1 + i, -1 + mat_ch_dim2, l] : (i >= 2 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= -1 + i and l <= -1 + 2mat_ch_dim2) or (i >= 1 and i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= -1 + i and l <= -1 + 2mat_ch_dim2); S7[2, 0] -> S8[1, 0, 1] : mat_ch_dim2 = 1 and mat_ch_dim1 >= 3; S7[i, 1 + i] -> S8[i, -1 + i, l] : i >= 1 and i <= -2 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2; S2[i, j] -> S5[i, -1 + j] : i >= 0 and i <= -1 + mat_ch_dim1 and j >= 1 + i and j <= -1 + 2mat_ch_dim2; S2[i, i] -> S5[-1 + i, -1 + 2mat_ch_dim2] : i >= 1 and i <= -1 + 2mat_ch_dim2 and i <= -1 + mat_ch_dim1; S8[i, k, l] -> S6[i, k] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S10[i, j] -> S8[mat_ch_dim2 + j, i, mat_ch_dim2 + j] : j >= 0 and j <= -1 + matinv_ch_dim2 and i <= -1 + mat_ch_dim2 and i <= -1 + matinv_ch_dim1 and j <= -2 + mat_ch_dim1 - mat_ch_dim2 and i >= 0 and j <= -1 + mat_ch_dim2; S10[i, j] -> S8[-1 + mat_ch_dim1, i, mat_ch_dim2 + j] : (i >= 0 and i <= -1 + matinv_ch_dim1 and i <= -1 + mat_ch_dim2 and i <= -2 + mat_ch_dim1 and j >= -1 + mat_ch_dim1 - mat_ch_dim2 and j >= 0 and j <= -1 + matinv_ch_dim2 and j <= -1 + mat_ch_dim2) or (mat_ch_dim1 >= 1 and i >= mat_ch_dim1 and i <= -1 + matinv_ch_dim1 and i <= -1 + mat_ch_dim2 and j >= 0 and j <= -1 + matinv_ch_dim2 and j <= -1 + mat_ch_dim2); S5[i, j] -> S0[i] : j >= i and j <= -1 + 2mat_ch_dim2 and i >= 0 and i <= -1 + mat_ch_dim1; S0[i] -> S0[-1 + i] : i >= 1 and i <= -1 + mat_ch_dim1; S3[i, j] -> S9[-1 + i, i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and i >= 1; S10[i, j] -> S4[i, mat_ch_dim2 + j] : j <= -1 + mat_ch_dim2 and j <= -1 + matinv_ch_dim2 and i <= -1 + mat_ch_dim1 and i <= -1 + matinv_ch_dim1 and i >= mat_ch_dim2 and j >= -mat_ch_dim2 + i; S10[-1 + mat_ch_dim1, j] -> S4[-1 + mat_ch_dim1, mat_ch_dim2 + j] : j >= 0 and j <= -1 + matinv_ch_dim2 and mat_ch_dim2 >= mat_ch_dim1 and matinv_ch_dim1 >= mat_ch_dim1 and j <= -1 + mat_ch_dim2 and mat_ch_dim1 >= 1; S8[i, k, l] -> S7[i, k] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S4[i, j] -> S0[i] : i >= 0 and i <= -1 + mat_ch_dim1 and j >= i and j <= -1 + 2mat_ch_dim2; S6[i, -1 + i] -> S4[-1 + i, i] : i <= -1 + mat_ch_dim1 and i <= mat_ch_dim2 and i >= 1; S1[i] -> S5[-1 + i, j] : i >= 1 and i <= -1 + mat_ch_dim1 and j >= -1 + i and j <= -1 + 2mat_ch_dim2; S2[i, j] -> S8[-1 + i, i, j] : j >= i and j <= -1 + 2mat_ch_dim2 and i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1 and i >= 1; S9[i, k, l] -> S9[-1 + i, k, l] : (i >= 1 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -2 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S9[i, -1 + i, l] -> S9[-1 + i, k', l] : (i >= 1 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2 and k' >= i and k' <= -1 + mat_ch_dim2) or (i <= mat_ch_dim2 and i <= -1 + mat_ch_dim1 and l >= i and l <= -1 + 2mat_ch_dim2 and k' >= 0 and k' <= -2 + i); S0[i] -> S5[-1 + i, j] : i >= 1 and i <= -1 + mat_ch_dim1 and j >= -1 + i and j <= -1 + 2mat_ch_dim2; S9[i, k, l] -> S4[i, l] : (i <= -1 + mat_ch_dim1 and k >= 0 and k <= -1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 1 + i and k <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2); S6[i, k] -> S6[i, -1 + k] : (i <= -1 + mat_ch_dim1 and k >= 1 and k <= -1 + i and k <= -1 + mat_ch_dim2) or (i >= 0 and i <= -1 + mat_ch_dim1 and k >= 2 + i and k <= -1 + mat_ch_dim2); S6[i, 1 + i] -> S6[i, -1 + i] : i >= 1 and i <= -2 + mat_ch_dim2 and i <= -1 + mat_ch_dim1; S6[i, 0] -> S6[-1 + i, -1 + mat_ch_dim2] : (mat_ch_dim2 >= 1 and i >= 2 + mat_ch_dim2 and i <= -1 + mat_ch_dim1) or (i >= 1 and i <= -1 + mat_ch_dim2 and i <= -1 + mat_ch_dim1); S6[i, 0] -> S6[-1 + i, -2 + i] : i >= mat_ch_dim2 and i >= 2 and i <= -1 + mat_ch_dim1 and i <= 1 + mat_ch_dim2 }";
		String memoryBasedPlutoSchedule = "[mat_ch_dim1, mat_ch_dim2, matinv_ch_dim1, matinv_ch_dim2] -> { S0[i] -> [i, 0, 0, 3]; S11[i, j] -> [mat_ch_dim1, i, j, 0]; S8[i, k, l] -> [i, k, l, 8]; S3[i, j] -> [i, 0, j, 1]; S6[i, k] -> [i, k, 0, 5]; S4[i, j] -> [i, 0, j, 7]; S2[i, j] -> [i, 0, j, 2]; S7[i, k] -> [i, k, 0, 6]; S1[i] -> [i, 0, 0, 0]; S5[i, j] -> [i, 0, j, 4]; S9[i, k, l] -> [i, k, l, 9]; S10[i, j] -> [mat_ch_dim1, i, j, 0] }";
		String memoryBasedFeautrierSchedule = "[mat_ch_dim1, mat_ch_dim2, matinv_ch_dim1, matinv_ch_dim2] -> { S8[i, k, l] -> [i, 2, k, 1, l]; S2[i, j] -> [i, 1, j, 0, 0]; S9[i, k, l] -> [i, 2, k, 1, l]; S0[i] -> [i, 0, 0, 0, 0]; S5[i, j] -> [i, 1, j, 1, 0]; S4[i, j] -> [i, 1, j, 1, 0]; S7[i, k] -> [i, 2, k, 0, 0]; S3[i, j] -> [i, 1, j, 0, 0]; S6[i, k] -> [i, 2, k, 0, 0]; S11[i, j] -> [mat_ch_dim1, 0, i, j, 0]; S1[i] -> [i, 0, 0, 0, 0]; S10[i, j] -> [mat_ch_dim1, 0, i, j, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #90
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : Planckian_distribution_Planckian_distribution/scop_0
	 *   block : 
	 *     S0 (depth = 0) [expmax=20.0]
	 *     ([expmax]) = f([])
	 *     (Domain = [n,loop] -> {S0[] : })
	 *     S1 (depth = 0) [u[n-1]=mul(mul(0.9900000095367432,expmax),v[n-1])]
	 *     ([u[n-1]]) = f([expmax, v[n-1]])
	 *     (Domain = [n,loop] -> {S1[] : })
	 *     for : 1 <= l <= loop (stride = 1)
	 *       for : 0 <= k <= n-1 (stride = 1)
	 *         block : 
	 *           S2 (depth = 2) [y[k]=div(u[k],v[k])]
	 *           ([y[k]]) = f([u[k], v[k]])
	 *           (Domain = [n,loop] -> {S2[l,k] : (k >= 0) and (-k+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S3 (depth = 2) [w[k]=div(x[k],sub(mul(y[k],y[k]),1.0))]
	 *           ([w[k]]) = f([x[k], y[k], y[k]])
	 *           (Domain = [n,loop] -> {S3[l,k] : (k >= 0) and (-k+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_22_Planckian_distribution_GScopRoot_Planckian_distribution_scop_0_ () {
		String path = "polymodel_src_livermore_loops_22_Planckian_distribution_GScopRoot_Planckian_distribution_scop_0_";
		// SCoP Extraction Data
		String domains = "[n, loop] -> { S0[]; S2[l, k] : k >= 0 and k <= -1 + n and l >= 1 and l <= loop; S1[]; S3[l, k] : k >= 0 and k <= -1 + n and l >= 1 and l <= loop }";
		String idSchedules = "[n, loop] -> { S2[l, k] -> [2, l, 0, k, 0]; S1[] -> [1, 0, 0, 0, 0]; S3[l, k] -> [2, l, 0, k, 1]; S0[] -> [0, 0, 0, 0, 0] }";
		String writes = "[n, loop] -> { S0[] -> expmax[]; S3[l, k] -> w[k]; S1[] -> u[-1 + n]; S2[l, k] -> y[k] }";
		String reads = "[n, loop] -> { S1[] -> expmax[]; S3[l, k] -> y[k]; S2[l, k] -> u[k]; S2[l, k] -> v[k]; S1[] -> v[-1 + n]; S3[l, k] -> x[k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n, loop] -> { S2[l, -1 + n] -> S1[] : n >= 1 and l <= loop and l >= 1; S1[] -> S0[]; S3[l, k] -> S2[l, k] : k >= 0 and k <= -1 + n and l >= 1 and l <= loop }";
		String valueBasedPlutoSchedule = "[n, loop] -> { S1[] -> [0, 0, 3]; S2[l, k] -> [l, k, 0]; S0[] -> [0, 0, 2]; S3[l, k] -> [l, k, 1] }";
		String valueBasedFeautrierSchedule = "[n, loop] -> { S0[] -> [0, 0]; S2[l, k] -> [l, k]; S1[] -> [0, 0]; S3[l, k] -> [l, k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n, loop] -> { S2[l, k] -> S3[-1 + l, k] : l >= 2 and l <= loop and k >= 0 and k <= -1 + n; S3[l, k] -> S3[-1 + l, k] : k >= 0 and k <= -1 + n and l >= 2 and l <= loop; S2[l, k] -> S2[-1 + l, k] : k >= 0 and k <= -1 + n and l >= 2 and l <= loop; S3[l, k] -> S2[l, k] : k >= 0 and k <= -1 + n and l >= 1 and l <= loop; S1[] -> S0[]; S2[l, -1 + n] -> S1[] : n >= 1 and l <= loop and l >= 1 }";
		String memoryBasedPlutoSchedule = "[n, loop] -> { S1[] -> [0, 0, 3]; S2[l, k] -> [l, k, 0]; S0[] -> [0, 0, 2]; S3[l, k] -> [l, k, 1] }";
		String memoryBasedFeautrierSchedule = "[n, loop] -> { S1[] -> [0, 1, 0]; S2[l, k] -> [l, 0, k]; S0[] -> [0, 0, 0]; S3[l, k] -> [l, 1, k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #91
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_scalar_scop_new
	 *   block : 
	 *     S0 (depth = 0) [y=IntExpr(0)]
	 *     ([y]) = f([])
	 *     (Domain = [] -> {S0[] : })
	 *     for : 0 <= i <= 2 (stride = 1)
	 *       block : 
	 *         S1 (depth = 1) [y=add(y,IntExpr(1))]
	 *         ([y]) = f([y])
	 *         (Domain = [] -> {S1[i] : (i >= 0) and (-i+2 >= 0)})
	 *         for : 0 <= j <= 2 (stride = 1)
	 *           S2 (depth = 2) [y=add(y,IntExpr(2))]
	 *           ([y]) = f([y])
	 *           (Domain = [] -> {S2[i,j] : (j >= 0) and (-j+2 >= 0) and (i >= 0) and (-i+2 >= 0)})
	 *         for : 0 <= k <= 2 (stride = 1)
	 *           S3 (depth = 2) [y=add(y,IntExpr(3))]
	 *           ([y]) = f([y])
	 *           (Domain = [] -> {S3[i,k] : (k >= 0) and (-k+2 >= 0) and (i >= 0) and (-i+2 >= 0)})
	 *         S4 (depth = 1) [y=add(y,IntExpr(4))]
	 *         ([y]) = f([y])
	 *         (Domain = [] -> {S4[i] : (i >= 0) and (-i+2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_test_scalar_GScopRoot_scop_new_ () {
		String path = "basics_oldFiles_test_scalar_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "{ S4[i] : i >= 0 and i <= 2; S2[i, j] : j >= 0 and j <= 2 and i >= 0 and i <= 2; S1[i] : i >= 0 and i <= 2; S3[i, k] : k >= 0 and k <= 2 and i >= 0 and i <= 2; S0[] }";
		String idSchedules = "{ S0[] -> [0, 0, 0, 0, 0]; S4[i] -> [1, i, 3, 0, 0]; S3[i, k] -> [1, i, 2, k, 0]; S1[i] -> [1, i, 0, 0, 0]; S2[i, j] -> [1, i, 1, j, 0] }";
		String writes = "{ S2[i, j] -> y[]; S3[i, k] -> y[]; S1[i] -> y[]; S0[] -> y[]; S4[i] -> y[] }";
		String reads = "{ S2[i, j] -> y[]; S3[i, k] -> y[]; S1[i] -> y[]; S4[i] -> y[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S1[i] -> S4[-1 + i] : i >= 1 and i <= 2; S3[i, k] -> S3[i, -1 + k] : k >= 1 and k <= 2 and i >= 0 and i <= 2; S2[i, 0] -> S1[i] : i >= 0 and i <= 2; S3[i, 0] -> S2[i, 2] : i >= 0 and i <= 2; S2[i, j] -> S2[i, -1 + j] : j >= 1 and j <= 2 and i >= 0 and i <= 2; S1[0] -> S0[]; S4[i] -> S3[i, 2] : i >= 0 and i <= 2 }";
		String valueBasedPlutoSchedule = "{ S3[i, k] -> [i, 2 + 4i + k, 3]; S0[] -> [0, 0, 0]; S4[i] -> [i, 4 + 4i, 4]; S2[i, j] -> [i, 4i + j, 2]; S1[i] -> [i, 4i, 1] }";
		String valueBasedFeautrierSchedule = "{ S3[i, k] -> [5 + 8i + k, i]; S4[i] -> [8 + 8i, 0]; S1[i] -> [1 + 8i, 0]; S2[i, j] -> [2 + 8i + j, i]; S0[] -> [0, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S1[i] -> S4[-1 + i] : i <= 2 and i >= 1; S3[i, k] -> S3[i, -1 + k] : i <= 2 and i >= 0 and k <= 2 and k >= 1; S2[i, 0] -> S1[i] : i <= 2 and i >= 0; S3[i, 0] -> S2[i, 2] : i <= 2 and i >= 0; S2[i, j] -> S2[i, -1 + j] : i <= 2 and i >= 0 and j <= 2 and j >= 1; S1[0] -> S0[]; S4[i] -> S3[i, 2] : i <= 2 and i >= 0 }";
		String memoryBasedPlutoSchedule = "{ S3[i, k] -> [i, 2 + 4i + k, 3]; S0[] -> [0, 0, 0]; S4[i] -> [i, 4 + 4i, 4]; S2[i, j] -> [i, 4i + j, 2]; S1[i] -> [i, 4i, 1] }";
		String memoryBasedFeautrierSchedule = "{ S3[i, k] -> [5 + 8i + k, i]; S4[i] -> [8 + 8i, 0]; S1[i] -> [1 + 8i, 0]; S2[i, j] -> [2 + 8i + j, i]; S0[] -> [0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #92
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_seidel_2d_kernel_seidel_2d/scop_0
	 *   for : 0 <= t <= tsteps-1 (stride = 1)
	 *     for : 1 <= i <= n-2 (stride = 1)
	 *       for : 1 <= j <= n-2 (stride = 1)
	 *         S0 (depth = 3) [A[i][j]=div(add(add(add(add(add(add(add(add(A[i-1][j-1],A[i-1][j]),A[i-1][j+1]),A[i][j-1]),A[i][j]),A[i][j+1]),A[i+1][j-1]),A[i+1][j]),A[i+1][j+1]),9.0)]
	 *         ([A[i][j]]) = f([A[i-1][j-1], A[i-1][j], A[i-1][j+1], A[i][j-1], A[i][j], A[i][j+1], A[i+1][j-1], A[i+1][j], A[i+1][j+1]])
	 *         (Domain = [tsteps,n] -> {S0[t,i,j] : (j-1 >= 0) and (-j+n-2 >= 0) and (i-1 >= 0) and (-i+n-2 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_stencils_seidel_2d_seidel_2d_GScopRoot_kernel_seidel_2d_scop_0_ () {
		String path = "polybench_gecos_stencils_seidel_2d_seidel_2d_GScopRoot_kernel_seidel_2d_scop_0_";
		// SCoP Extraction Data
		String domains = "[tsteps, n] -> { S0[t, i, j] : j >= 1 and j <= -2 + n and i >= 1 and i <= -2 + n and t >= 0 and t <= -1 + tsteps }";
		String idSchedules = "[tsteps, n] -> { S0[t, i, j] -> [0, t, 0, i, 0, j, 0] }";
		String writes = "[tsteps, n] -> { S0[t, i, j] -> A[i, j] }";
		String reads = "[tsteps, n] -> { S0[t, i, j] -> A[1 + i, 1 + j]; S0[t, i, j] -> A[i, 1 + j]; S0[t, i, j] -> A[-1 + i, 1 + j]; S0[t, i, j] -> A[1 + i, j]; S0[t, i, j] -> A[i, j]; S0[t, i, j] -> A[-1 + i, j]; S0[t, i, j] -> A[1 + i, -1 + j]; S0[t, i, j] -> A[i, -1 + j]; S0[t, i, j] -> A[-1 + i, -1 + j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[tsteps, n] -> { S0[t, i, j] -> S0[-1 + t, i', j'] : j' <= -2 + n and i' <= 1 + i and t >= 1 and t <= -1 + tsteps and j >= 1 and j' <= -i + j + i' and j' >= i + j - i' and i >= 1 and i' <= -2 + n and j' >= 1 and j <= -2 + n; S0[t, i, j] -> S0[t, -1 + i, j'] : j' >= 1 and i >= 2 and i <= -2 + n and t <= -1 + tsteps and j' >= -1 + j and j <= -2 + n and t >= 0 and j' <= -2 + n and j' <= 1 + j and j >= 1; S0[t, i, j] -> S0[-1 + t, i, 1 + j] : j >= 1 and j <= -3 + n and i >= 1 and i <= -2 + n and t >= 1 and t <= -1 + tsteps; S0[t, i, j] -> S0[t, i, -1 + j] : j >= 2 and j <= -2 + n and i >= 1 and i <= -2 + n and t >= 0 and t <= -1 + tsteps }";
		String valueBasedPlutoSchedule = "[tsteps, n] -> { S0[t, i, j] -> [t, t + i, 2t + i + j] }";
		String valueBasedFeautrierSchedule = "[tsteps, n] -> { S0[t, i, j] -> [4t + 2i + j, i, t] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[tsteps, n] -> { S0[t, i, j] -> S0[t', i', j'] : t' <= t and i' >= 1 and i <= -2 + n and t <= -1 + tsteps and j <= -2 + n and t' <= -2 + t + j and t' >= 0 and j' <= -1 + t + i + j - t' - i' and i' >= -1 + t + i - t' and j' >= -1 + t + j - t'; S0[t, i, j] -> S0[-1 + t, i', j'] : t >= 1 and t <= -1 + tsteps and i' <= -2 + n and i >= 1 and j >= 1 and j' <= -2 + n and j' <= 1 + j and i' <= 1 + i and j' >= 1 + i + j - i'; S0[t, i, j] -> S0[t, -1 + i, j'] : t <= -1 + tsteps and i >= 2 and i <= -2 + n and j' <= 1 + j and j >= 1 and j' <= -2 + n and j' >= 1 and j <= -2 + n and j' >= -1 + j and t >= 0; S0[t, i, j] -> S0[-1 + t, 1 + i, -1 + j] : t >= 1 and t <= -1 + tsteps and i >= 1 and i <= -3 + n and j >= 2 and j <= -2 + n }";
		String memoryBasedPlutoSchedule = "[tsteps, n] -> { S0[t, i, j] -> [t, t + i, 2t + i + j] }";
		String memoryBasedFeautrierSchedule = "[tsteps, n] -> { S0[t, i, j] -> [4t + 2i + j, i, t] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #93
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : comp_data_comp_data/scop_0
	 *   block : 
	 *     for : 0 <= x <= 14 (stride = 1)
	 *       for : 0 <= y <= height-1 (stride = 1)
	 *         for : 0 <= v <= 15 (stride = 1)
	 *           S0 (depth = 3) [data[x][y][v]=IntExpr(4080)]
	 *           ([data[x][y][v]]) = f([])
	 *           (Domain = [height,width] -> {S0[x,y,v] : (v >= 0) and (-v+15 >= 0) and (y >= 0) and (-y+height-1 >= 0) and (x >= 0) and (-x+14 >= 0)})
	 *     for : 15 <= x <= width-1 (stride = 1)
	 *       for : 0 <= y <= height-1 (stride = 1)
	 *         block : 
	 *           S1 (depth = 2) [tmp[x][y]=IntExpr(255)]
	 *           ([tmp[x][y]]) = f([])
	 *           (Domain = [height,width] -> {S1[x,y] : (y >= 0) and (-y+height-1 >= 0) and (x-15 >= 0) and (-x+width-1 >= 0)})
	 *           for : 0 <= v <= 15 (stride = 1)
	 *             block : 
	 *               S2 (depth = 3) [val=call abs(sub(out1[x][y],out2[x-v][y]))]
	 *               ([val]) = f([out1[x][y], out2[x-v][y]])
	 *               (Domain = [height,width] -> {S2[x,y,v] : (v >= 0) and (-v+15 >= 0) and (y >= 0) and (-y+height-1 >= 0) and (x-15 >= 0) and (-x+width-1 >= 0)})
	 *               S3 (depth = 3) [data[x][y][v]=mul(IntExpr(16),call min(val,IntExpr(16)))]
	 *               ([data[x][y][v]]) = f([val])
	 *               (Domain = [height,width] -> {S3[x,y,v] : (v >= 0) and (-v+15 >= 0) and (y >= 0) and (-y+height-1 >= 0) and (x-15 >= 0) and (-x+width-1 >= 0)})
	 *               S4 (depth = 3) [tmp[x][y]=call min(tmp[x][y],data[x][y][v])]
	 *               ([tmp[x][y]]) = f([tmp[x][y], data[x][y][v]])
	 *               (Domain = [height,width] -> {S4[x,y,v] : (v >= 0) and (-v+15 >= 0) and (y >= 0) and (-y+height-1 >= 0) and (x-15 >= 0) and (-x+width-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_divs_stereo_scop_GScopRoot_comp_data_scop_0_ () {
		String path = "polymodel_divs_stereo_scop_GScopRoot_comp_data_scop_0_";
		// SCoP Extraction Data
		String domains = "[height, width] -> { S3[x, y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height and x >= 15 and x <= -1 + width; S0[x, y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height and x >= 0 and x <= 14; S4[x, y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height and x >= 15 and x <= -1 + width; S1[x, y] : y >= 0 and y <= -1 + height and x >= 15 and x <= -1 + width; S2[x, y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height and x >= 15 and x <= -1 + width }";
		String idSchedules = "[height, width] -> { S4[x, y, v] -> [1, x, 0, y, 1, v, 2]; S3[x, y, v] -> [1, x, 0, y, 1, v, 1]; S0[x, y, v] -> [0, x, 0, y, 0, v, 0]; S2[x, y, v] -> [1, x, 0, y, 1, v, 0]; S1[x, y] -> [1, x, 0, y, 0, 0, 0] }";
		String writes = "[height, width] -> { S2[x, y, v] -> val[]; S0[x, y, v] -> data[x, y, v]; S1[x, y] -> tmp[x, y]; S3[x, y, v] -> data[x, y, v]; S4[x, y, v] -> tmp[x, y] }";
		String reads = "[height, width] -> { S3[x, y, v] -> val[]; S2[x, y, v] -> out2[x - v, y]; S4[x, y, v] -> data[x, y, v]; S4[x, y, v] -> tmp[x, y]; S2[x, y, v] -> out1[x, y] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[height, width] -> { S4[x, y, 0] -> S1[x, y] : x >= 15 and x <= -1 + width and y >= 0 and y <= -1 + height; S4[x, y, v] -> S3[x, y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height and x >= 15 and x <= -1 + width; S4[x, y, v] -> S4[x, y, -1 + v] : v >= 1 and v <= 15 and y >= 0 and y <= -1 + height and x >= 15 and x <= -1 + width; S3[x, y, v] -> S2[x, y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height and x >= 15 and x <= -1 + width }";
		String valueBasedPlutoSchedule = "[height, width] -> { S0[x, y, v] -> [x, y, v, 0]; S3[x, y, v] -> [x, y, v, 3]; S1[x, y] -> [0, x, y, 1]; S4[x, y, v] -> [x, x + y, x + y + v, 2]; S2[x, y, v] -> [x, y, v, 0] }";
		String valueBasedFeautrierSchedule = "[height, width] -> { S0[x, y, v] -> [x, y, v]; S1[x, y] -> [x, y, 0]; S4[x, y, v] -> [v, y, x]; S2[x, y, v] -> [x, y, v]; S3[x, y, v] -> [x, y, v] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[height, width] -> { S2[x, y, v] -> S2[x, y, -1 + v] : v >= 1 and v <= 15 and y >= 0 and y <= -1 + height and x >= 15 and x <= -1 + width; S2[x, y, 0] -> S2[x, -1 + y, 15] : x >= 15 and x <= -1 + width and y >= 1 and y <= -1 + height; S2[x, 0, 0] -> S2[-1 + x, -1 + height, 15] : x >= 16 and x <= -1 + width and height >= 1; S4[x, y, v] -> S4[x, y, -1 + v] : x >= 15 and x <= -1 + width and y >= 0 and y <= -1 + height and v <= 15 and v >= 1; S2[x, y, v] -> S3[x, y, -1 + v] : x >= 15 and x <= -1 + width and y >= 0 and y <= -1 + height and v <= 15 and v >= 1; S2[x, y, 0] -> S3[x, -1 + y, 15] : x >= 15 and x <= -1 + width and y >= 1 and y <= -1 + height; S2[x, 0, 0] -> S3[-1 + x, -1 + height, 15] : height >= 1 and x >= 16 and x <= -1 + width; S4[x, y, v] -> S3[x, y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height and x >= 15 and x <= -1 + width; S3[x, y, v] -> S2[x, y, v] : v >= 0 and v <= 15 and y >= 0 and y <= -1 + height and x >= 15 and x <= -1 + width; S4[x, y, 0] -> S1[x, y] : x >= 15 and x <= -1 + width and y >= 0 and y <= -1 + height }";
		String memoryBasedPlutoSchedule = "[height, width] -> { S0[x, y, v] -> [x, y, v, 0]; S3[x, y, v] -> [x, y, 15y + v, 1]; S1[x, y] -> [0, x, y, 0]; S4[x, y, v] -> [x, y, 15y + v, 2]; S2[x, y, v] -> [x, y, 15y + v, 0] }";
		String memoryBasedFeautrierSchedule = "[height, width] -> { S0[x, y, v] -> [x, y, v, 0]; S3[x, y, v] -> [x, 16y + v, 1, y]; S1[x, y] -> [0, x, y, 0]; S4[x, y, v] -> [1 + x + v, y, v, 0]; S2[x, y, v] -> [x, 16y + v, 0, y] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #94
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : stereo_ms_stereo_ms/scop_0
	 *   for : 0 <= y <= cur_height-1 (stride = 1)
	 *     for : 0 <= x <= cur_width-1 (stride = 1)
	 *       for : 0 <= v <= 15 (stride = 1)
	 *         block : 
	 *           S0 (depth = 3) [u[i][x][y][v]=u[i+1][[(x) / 2]][[(y) / 2]][v]]
	 *           ([u[i][x][y][v]]) = f([u[i+1][[(x) / 2]][[(y) / 2]][v]])
	 *           (Domain = [cur_height,cur_width,i] -> {S0[y,x,v] : (v >= 0) and (-v+15 >= 0) and (x >= 0) and (-x+cur_width-1 >= 0) and (y >= 0) and (-y+cur_height-1 >= 0)})
	 *           S1 (depth = 3) [d[i][x][y][v]=d[i+1][[(x) / 2]][[(y) / 2]][v]]
	 *           ([d[i][x][y][v]]) = f([d[i+1][[(x) / 2]][[(y) / 2]][v]])
	 *           (Domain = [cur_height,cur_width,i] -> {S1[y,x,v] : (v >= 0) and (-v+15 >= 0) and (x >= 0) and (-x+cur_width-1 >= 0) and (y >= 0) and (-y+cur_height-1 >= 0)})
	 *           S2 (depth = 3) [l[i][x][y][v]=l[i+1][[(x) / 2]][[(y) / 2]][v]]
	 *           ([l[i][x][y][v]]) = f([l[i+1][[(x) / 2]][[(y) / 2]][v]])
	 *           (Domain = [cur_height,cur_width,i] -> {S2[y,x,v] : (v >= 0) and (-v+15 >= 0) and (x >= 0) and (-x+cur_width-1 >= 0) and (y >= 0) and (-y+cur_height-1 >= 0)})
	 *           S3 (depth = 3) [r[i][x][y][v]=r[i+1][[(x) / 2]][[(y) / 2]][v]]
	 *           ([r[i][x][y][v]]) = f([r[i+1][[(x) / 2]][[(y) / 2]][v]])
	 *           (Domain = [cur_height,cur_width,i] -> {S3[y,x,v] : (v >= 0) and (-v+15 >= 0) and (x >= 0) and (-x+cur_width-1 >= 0) and (y >= 0) and (-y+cur_height-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_divs_stereo_scop_GScopRoot_stereo_ms_scop_0_ () {
		String path = "polymodel_divs_stereo_scop_GScopRoot_stereo_ms_scop_0_";
		// SCoP Extraction Data
		String domains = "[cur_height, cur_width, i] -> { S2[y, x, v] : v >= 0 and v <= 15 and x >= 0 and x <= -1 + cur_width and y >= 0 and y <= -1 + cur_height; S0[y, x, v] : v >= 0 and v <= 15 and x >= 0 and x <= -1 + cur_width and y >= 0 and y <= -1 + cur_height; S1[y, x, v] : v >= 0 and v <= 15 and x >= 0 and x <= -1 + cur_width and y >= 0 and y <= -1 + cur_height; S3[y, x, v] : v >= 0 and v <= 15 and x >= 0 and x <= -1 + cur_width and y >= 0 and y <= -1 + cur_height }";
		String idSchedules = "[cur_height, cur_width, i] -> { S0[y, x, v] -> [0, y, 0, x, 0, v, 0]; S1[y, x, v] -> [0, y, 0, x, 0, v, 1]; S2[y, x, v] -> [0, y, 0, x, 0, v, 2]; S3[y, x, v] -> [0, y, 0, x, 0, v, 3] }";
		String writes = "[cur_height, cur_width, i] -> { S1[y, x, v] -> d[i, x, y, v]; S0[y, x, v] -> u[i, x, y, v]; S3[y, x, v] -> r[i, x, y, v]; S2[y, x, v] -> l[i, x, y, v] }";
		String reads = "[cur_height, cur_width, i] -> { S1[y, x, v] -> d[1 + i, out_j1, out_j2, v] : 2out_j2 = y and 2out_j1 = x; S0[y, x, v] -> u[1 + i, out_j1, out_j2, v] : 2out_j2 = y and 2out_j1 = x; S3[y, x, v] -> r[1 + i, out_j1, out_j2, v] : 2out_j2 = y and 2out_j1 = x; S2[y, x, v] -> l[1 + i, out_j1, out_j2, v] : 2out_j2 = y and 2out_j1 = x }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[cur_height, cur_width, i] -> {  }";
		String valueBasedPlutoSchedule = "[cur_height, cur_width, i] -> { S2[y, x, v] -> [y, x, v]; S0[y, x, v] -> [y, x, v]; S1[y, x, v] -> [y, x, v]; S3[y, x, v] -> [y, x, v] }";
		String valueBasedFeautrierSchedule = "[cur_height, cur_width, i] -> { S2[y, x, v] -> [y, x, v]; S0[y, x, v] -> [y, x, v]; S1[y, x, v] -> [y, x, v]; S3[y, x, v] -> [y, x, v] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[cur_height, cur_width, i] -> {  }";
		String memoryBasedPlutoSchedule = "[cur_height, cur_width, i] -> { S2[y, x, v] -> [y, x, v]; S0[y, x, v] -> [y, x, v]; S1[y, x, v] -> [y, x, v]; S3[y, x, v] -> [y, x, v] }";
		String memoryBasedFeautrierSchedule = "[cur_height, cur_width, i] -> { S2[y, x, v] -> [y, x, v]; S0[y, x, v] -> [y, x, v]; S1[y, x, v] -> [y, x, v]; S3[y, x, v] -> [y, x, v] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #95
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : stereo_ms_stereo_ms/scop_1
	 *   for : 0 <= y <= old_height-1 (stride = 1)
	 *     for : 0 <= x <= old_width-1 (stride = 1)
	 *       for : 0 <= value <= 15 (stride = 1)
	 *         S0 (depth = 3) [data[i][[(x) / 2]][[(y) / 2]][value]=add(data[i][[(x) / 2]][[(y) / 2]][value],data[i-1][x][y][value])]
	 *         ([data[i][[(x) / 2]][[(y) / 2]][value]]) = f([data[i][[(x) / 2]][[(y) / 2]][value], data[i-1][x][y][value]])
	 *         (Domain = [old_height,old_width,i] -> {S0[y,x,value] : (value >= 0) and (-value+15 >= 0) and (x >= 0) and (-x+old_width-1 >= 0) and (y >= 0) and (-y+old_height-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_divs_stereo_scop_GScopRoot_stereo_ms_scop_1_ () {
		String path = "polymodel_divs_stereo_scop_GScopRoot_stereo_ms_scop_1_";
		// SCoP Extraction Data
		String domains = "[old_height, old_width, i] -> { S0[y, x, value] : value >= 0 and value <= 15 and x >= 0 and x <= -1 + old_width and y >= 0 and y <= -1 + old_height }";
		String idSchedules = "[old_height, old_width, i] -> { S0[y, x, value] -> [0, y, 0, x, 0, value, 0] }";
		String writes = "[old_height, old_width, i] -> { S0[y, x, value] -> data[i, out_j1, out_j2, value] : 2out_j2 = y and 2out_j1 = x }";
		String reads = "[old_height, old_width, i] -> { S0[y, x, value] -> data[i, out_j1, out_j2, value] : 2out_j2 = y and 2out_j1 = x; S0[y, x, value] -> data[-1 + i, x, y, value] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[old_height, old_width, i] -> {  }";
		String valueBasedPlutoSchedule = "[old_height, old_width, i] -> { S0[y, x, value] -> [y, x, value] }";
		String valueBasedFeautrierSchedule = "[old_height, old_width, i] -> { S0[y, x, value] -> [y, x, value] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[old_height, old_width, i] -> {  }";
		String memoryBasedPlutoSchedule = "[old_height, old_width, i] -> { S0[y, x, value] -> [y, x, value] }";
		String memoryBasedFeautrierSchedule = "[old_height, old_width, i] -> { S0[y, x, value] -> [y, x, value] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #96
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : msg_scop_new
	 *   block : 
	 *     S0 (depth = 0) [val=IntExpr(0)]
	 *     ([val]) = f([])
	 *     (Domain = [] -> {S0[] : })
	 *     for : 0 <= value <= 15 (stride = 1)
	 *       S1 (depth = 1) [val=add(val,dst[value])]
	 *       ([val]) = f([val, dst[value]])
	 *       (Domain = [] -> {S1[value] : (value >= 0) and (-value+15 >= 0)})
	 *     S2 (depth = 0) [val=div(val,IntExpr(16))]
	 *     ([val]) = f([val])
	 *     (Domain = [] -> {S2[] : })
	 *     for : 0 <= value <= 15 (stride = 1)
	 *       S3 (depth = 1) [dst[value]=sub(dst[value],val)]
	 *       ([dst[value]]) = f([dst[value], val])
	 *       (Domain = [] -> {S3[value] : (value >= 0) and (-value+15 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_divs_stereo_scop_GScopRoot_scop_new_ () {
		String path = "polymodel_divs_stereo_scop_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "{ S2[]; S3[value] : value >= 0 and value <= 15; S1[value] : value >= 0 and value <= 15; S0[] }";
		String idSchedules = "{ S3[value] -> [3, value, 0]; S0[] -> [0, 0, 0]; S2[] -> [2, 0, 0]; S1[value] -> [1, value, 0] }";
		String writes = "{ S1[value] -> val[]; S0[] -> val[]; S2[] -> val[]; S3[value] -> dst[value] }";
		String reads = "{ S1[value] -> val[]; S2[] -> val[]; S1[value] -> dst[value]; S3[value] -> val[]; S3[value] -> dst[value] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S3[value] -> S2[] : value >= 0 and value <= 15; S2[] -> S1[15]; S1[0] -> S0[]; S1[value] -> S1[-1 + value] : value >= 1 and value <= 15 }";
		String valueBasedPlutoSchedule = "{ S2[] -> [15, 2]; S1[value] -> [value, 1]; S0[] -> [0, 0]; S3[value] -> [15 + value, 3] }";
		String valueBasedFeautrierSchedule = "{ S2[] -> [0]; S0[] -> [0]; S3[value] -> [value]; S1[value] -> [value] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S3[value] -> S2[] : value >= 0 and value <= 15; S2[] -> S1[15]; S1[0] -> S0[]; S3[value] -> S1[value] : value <= 15 and value >= 0; S1[value] -> S1[-1 + value] : value <= 15 and value >= 1 }";
		String memoryBasedPlutoSchedule = "{ S2[] -> [15, 2]; S1[value] -> [value, 1]; S0[] -> [0, 0]; S3[value] -> [15 + value, 3] }";
		String memoryBasedFeautrierSchedule = "{ S2[] -> [0]; S0[] -> [0]; S3[value] -> [value]; S1[value] -> [value] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #97
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : simpleForSwitchIndices_simpleForSwitchIndices/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= N-1 (stride = 1)
	 *         S0 (depth = 2) [tab[i]=tab[j]]
	 *         ([tab[i]]) = f([tab[j]])
	 *         (Domain = [N] -> {S0[i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 *     for : 0 <= j <= N-1 (stride = 1)
	 *       S1 (depth = 1) [tab[j]=add(tab[j],IntExpr(j))]
	 *       ([tab[j]]) = f([tab[j]])
	 *       (Domain = [N] -> {S1[j] : (j >= 0) and (-j+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_simpleForSwitchIndices_GScopRoot_simpleForSwitchIndices_scop_0_ () {
		String path = "polymodel_others_simpleForSwitchIndices_GScopRoot_simpleForSwitchIndices_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S1[j] : j >= 0 and j <= -1 + N; S0[i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S0[i, j] -> [0, i, 0, j, 0]; S1[j] -> [1, j, 0, 0, 0] }";
		String writes = "[N] -> { S0[i, j] -> tab[i]; S1[j] -> tab[j] }";
		String reads = "[N] -> { S0[i, j] -> tab[j]; S1[j] -> tab[j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S0[i, j] -> S0[j, -1 + N] : j >= 0 and i <= -1 + N and j <= -1 + i; S0[i, i] -> S0[i, -1 + i] : i <= -1 + N and i >= 1; S1[j] -> S0[j, -1 + N] : j >= 0 and j <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S1[j] -> [j, N, 1]; S0[i, j] -> [i, j, 0] }";
		String valueBasedFeautrierSchedule = "[N] -> { S1[j] -> [1 + j, 0]; S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S0[i, 0] -> S0[i', i] : i' >= 0 and i' <= -1 + i and i <= -1 + N; S0[i, j] -> S0[j, -1 + N] : j >= 0 and j <= -1 + i and i <= -1 + N; S0[i, j] -> S0[i, -1 + j] : i >= 0 and i <= -1 + N and j >= 1 and j <= -1 + N; S1[j] -> S0[i, j] : i <= -1 + N and j >= 0 and i >= 1 + j; S1[j] -> S0[j, -1 + N] : j >= 0 and j <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S1[j] -> [N, j]; S0[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S1[j] -> [N, j]; S0[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #98
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : pass1_pass1/scop_0
	 *   block : 
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       if : ((i >= 0) and (i <= 2)) or ((i >= 4) and (i < 7))
	 *         S0 (depth = 1) [a=add(a,IntExpr(i))]
	 *         ([a]) = f([a])
	 *         (Domain = [n] -> {S0[i] : ((-i+2 >= 0) and (i >= 0) and (-i+n-1 >= 0)) or ((-i+6 >= 0) and (i-4 >= 0) and (-i+n-1 >= 0))})
	 *       else
	 *         block : 
	 *           S1 (depth = 1) [a=add(a,IntExpr(1))]
	 *           ([a]) = f([a])
	 *           (Domain = [n] -> {S1[i] : ((i-3 = 0) and (n-4 >= 0)) or ((i-7 >= 0) and (-i+n-1 >= 0))})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_testIfCond_GScopRoot_pass1_scop_0_ () {
		String path = "polymodel_src_corner_cases_testIfCond_GScopRoot_pass1_scop_0_";
		// SCoP Extraction Data
		String domains = "[n] -> { S1[i] : i >= 7 and i <= -1 + n; S1[3] : n >= 4; S0[i] : (i <= 2 and i >= 0 and i <= -1 + n) or (i <= 6 and i >= 4 and i <= -1 + n) }";
		String idSchedules = "[n] -> { S1[i] -> [0, i, 1]; S0[i] -> [0, i, 0] }";
		String writes = "[n] -> { S0[i] -> a[]; S1[i] -> a[] }";
		String reads = "[n] -> { S0[i] -> a[]; S1[i] -> a[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S0[4] -> S1[3] : n >= 5; S0[i] -> S0[-1 + i] : (i <= 6 and i >= 5 and i <= -1 + n) or (i <= 2 and i >= 1 and i <= -1 + n); S1[i] -> S1[-1 + i] : i >= 8 and i <= -1 + n; S1[7] -> S0[6] : n >= 8; S1[3] -> S0[2] : n >= 4 }";
		String valueBasedPlutoSchedule = "[n] -> { S0[i] -> [i, 0]; S1[i] -> [i, 1] }";
		String valueBasedFeautrierSchedule = "[n] -> { S0[i] -> [i]; S1[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S0[4] -> S1[3] : n >= 5; S0[i] -> S0[-1 + i] : (i <= 2 and i >= 1 and i <= -1 + n) or (i <= 6 and i >= 5 and i <= -1 + n); S1[i] -> S1[-1 + i] : i >= 8 and i <= -1 + n; S1[7] -> S0[6] : n >= 8; S1[3] -> S0[2] : n >= 4 }";
		String memoryBasedPlutoSchedule = "[n] -> { S0[i] -> [i, 0]; S1[i] -> [i, 1] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S0[i] -> [i]; S1[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #99
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : FPT_FPT/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= N-i-1 (stride = 1)
	 *         S0 (depth = 2) [y[j]=add(y[j],IntExpr(1))]
	 *         ([y[j]]) = f([y[j]])
	 *         (Domain = [N] -> {S0[i,j] : (j >= 0) and (-i-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_complex_types_mentor_GScopRoot_FPT_scop_0_ () {
		String path = "basics_complex_types_mentor_GScopRoot_FPT_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[i, j] : j >= 0 and j <= -1 + N - i and i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[N] -> { S0[i, j] -> y[j] }";
		String reads = "[N] -> { S0[i, j] -> y[j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S0[i, j] -> S0[-1 + i, j] : j >= 0 and j <= -1 + N - i and i >= 1 and i <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S0[i, j] -> S0[-1 + i, j] : i >= 1 and j >= 0 and j <= -1 + N - i }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #100
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : matmul_matmul/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= N-1 (stride = 1)
	 *         for : 0 <= k <= N-1 (stride = 1)
	 *           S0 (depth = 3) [C[i][j]=add(C[i][j],mul(A[i][k],B[k][j]))]
	 *           ([C[i][j]]) = f([C[i][j], A[i][k], B[k][j]])
	 *           (Domain = [N] -> {S0[i,j,k] : (k >= 0) and (-k+N-1 >= 0) and (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= N-1 (stride = 1)
	 *         for : 0 <= k <= N-1 (stride = 1)
	 *           S1 (depth = 3) [F[i][j]=add(F[i][j],mul(D[i][k],E[k][j]))]
	 *           ([F[i][j]]) = f([F[i][j], D[i][k], E[k][j]])
	 *           (Domain = [N] -> {S1[i,j,k] : (k >= 0) and (-k+N-1 >= 0) and (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= N-1 (stride = 1)
	 *         for : 0 <= k <= N-1 (stride = 1)
	 *           S2 (depth = 3) [G[i][j]=add(G[i][j],mul(C[i][k],F[k][j]))]
	 *           ([G[i][j]]) = f([G[i][j], C[i][k], F[k][j]])
	 *           (Domain = [N] -> {S2[i,j,k] : (k >= 0) and (-k+N-1 >= 0) and (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_tripleProdMat_GScopRoot_matmul_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_tripleProdMat_GScopRoot_matmul_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[i, j, k] : k >= 0 and k <= -1 + N and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N; S1[i, j, k] : k >= 0 and k <= -1 + N and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N; S2[i, j, k] : k >= 0 and k <= -1 + N and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S0[i, j, k] -> [0, i, 0, j, 0, k, 0]; S1[i, j, k] -> [1, i, 0, j, 0, k, 0]; S2[i, j, k] -> [2, i, 0, j, 0, k, 0] }";
		String writes = "[N] -> { S2[i, j, k] -> G[i, j]; S1[i, j, k] -> F[i, j]; S0[i, j, k] -> C[i, j] }";
		String reads = "[N] -> { S0[i, j, k] -> B[k, j]; S2[i, j, k] -> G[i, j]; S1[i, j, k] -> D[i, k]; S2[i, j, k] -> C[i, k]; S0[i, j, k] -> A[i, k]; S1[i, j, k] -> F[i, j]; S0[i, j, k] -> C[i, j]; S1[i, j, k] -> E[k, j]; S2[i, j, k] -> F[k, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S2[i, j, k] -> S2[i, j, -1 + k] : k >= 1 and k <= -1 + N and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N; S1[i, j, k] -> S1[i, j, -1 + k] : k >= 1 and k <= -1 + N and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N; S0[i, j, k] -> S0[i, j, -1 + k] : k >= 1 and k <= -1 + N and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N; S2[i, j, k] -> S1[k, j, -1 + N] : k >= 0 and k <= -1 + N and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N; S2[i, j, k] -> S0[i, k, -1 + N] : k >= 0 and k <= -1 + N and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S2[i, j, k] -> [i + k, j + k, N + k, 0]; S0[i, j, k] -> [i, j, k, 2]; S1[i, j, k] -> [i, j, k, 1] }";
		String valueBasedFeautrierSchedule = "[N] -> { S2[i, j, k] -> [N + k, j, i]; S1[i, j, k] -> [k, j, i]; S0[i, j, k] -> [k, j, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S2[i, j, k] -> S2[i, j, -1 + k] : i >= 0 and i <= -1 + N and j >= 0 and j <= -1 + N and k >= 1 and k <= -1 + N; S1[i, j, k] -> S1[i, j, -1 + k] : i >= 0 and i <= -1 + N and j >= 0 and j <= -1 + N and k >= 1 and k <= -1 + N; S0[i, j, k] -> S0[i, j, -1 + k] : i >= 0 and i <= -1 + N and j >= 0 and j <= -1 + N and k >= 1 and k <= -1 + N; S2[i, j, k] -> S1[k, j, -1 + N] : k >= 0 and k <= -1 + N and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N; S2[i, j, k] -> S0[i, k, -1 + N] : k >= 0 and k <= -1 + N and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S2[i, j, k] -> [i + k, j + k, N + k, 0]; S0[i, j, k] -> [i, j, k, 2]; S1[i, j, k] -> [i, j, k, 1] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S2[i, j, k] -> [N + k, j, i]; S1[i, j, k] -> [k, j, i]; S0[i, j, k] -> [k, j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #101
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : matrix_transpose_matrix_transpose/scop_0
	 *   block : 
	 *     for : 0 <= p1 <= N-1 (stride = 1)
	 *       for : 0 <= p2 <= p1-1 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [temp=A[p1][p2]]
	 *           ([temp]) = f([A[p1][p2]])
	 *           (Domain = [N] -> {S0[p1,p2] : (p2 >= 0) and (p1-p2-1 >= 0) and (p1 >= 0) and (-p1+N-1 >= 0)})
	 *           S1 (depth = 2) [A[p1][p2]=A[p2][p1]]
	 *           ([A[p1][p2]]) = f([A[p2][p1]])
	 *           (Domain = [N] -> {S1[p1,p2] : (p2 >= 0) and (p1-p2-1 >= 0) and (p1 >= 0) and (-p1+N-1 >= 0)})
	 *           S2 (depth = 2) [A[p2][p1]=temp]
	 *           ([A[p2][p1]]) = f([temp])
	 *           (Domain = [N] -> {S2[p1,p2] : (p2 >= 0) and (p1-p2-1 >= 0) and (p1 >= 0) and (-p1+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_openMP_MatrixTranspose_GScopRoot_matrix_transpose_scop_0_ () {
		String path = "polymodel_others_openMP_MatrixTranspose_GScopRoot_matrix_transpose_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[p1, p2] : p2 >= 0 and p2 <= -1 + p1 and p1 >= 0 and p1 <= -1 + N; S1[p1, p2] : p2 >= 0 and p2 <= -1 + p1 and p1 >= 0 and p1 <= -1 + N; S2[p1, p2] : p2 >= 0 and p2 <= -1 + p1 and p1 >= 0 and p1 <= -1 + N }";
		String idSchedules = "[N] -> { S0[p1, p2] -> [0, p1, 0, p2, 0]; S2[p1, p2] -> [0, p1, 0, p2, 2]; S1[p1, p2] -> [0, p1, 0, p2, 1] }";
		String writes = "[N] -> { S2[p1, p2] -> A[p2, p1]; S0[p1, p2] -> temp[]; S1[p1, p2] -> A[p1, p2] }";
		String reads = "[N] -> { S2[p1, p2] -> temp[]; S0[p1, p2] -> A[p1, p2]; S1[p1, p2] -> A[p2, p1] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S2[p1, p2] -> S0[p1, p2] : p2 >= 0 and p2 <= -1 + p1 and p1 >= 0 and p1 <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S2[p1, p2] -> [p1, p2, 1]; S0[p1, p2] -> [p1, p2, 0]; S1[p1, p2] -> [p1, p2, 0] }";
		String valueBasedFeautrierSchedule = "[N] -> { S1[p1, p2] -> [p1, p2]; S2[p1, p2] -> [p1, p2]; S0[p1, p2] -> [p1, p2] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S1[p1, p2] -> S0[p1, p2] : p1 <= -1 + N and p2 >= 0 and p2 <= -1 + p1 and p1 >= 0; S0[p1, p2] -> S0[p1, -1 + p2] : p1 <= -1 + N and p2 <= -1 + p1 and p2 >= 1; S0[p1, 0] -> S0[-1 + p1, -2 + p1] : p1 <= -1 + N and p1 >= 2; S2[p1, p2] -> S0[p1, p2] : p2 >= 0 and p2 <= -1 + p1 and p1 >= 0 and p1 <= -1 + N; S2[p1, p2] -> S1[p1, p2] : p1 <= -1 + N and p2 >= 0 and p2 <= -1 + p1 and p1 >= 0; S0[p1, p2] -> S2[p1, -1 + p2] : p1 <= -1 + N and p2 >= 1 and p2 <= -1 + p1; S0[p1, 0] -> S2[-1 + p1, -2 + p1] : p1 >= 2 and p1 <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S2[p1, p2] -> [p1, p2, 2]; S0[p1, p2] -> [p1, p2, 0]; S1[p1, p2] -> [p1, p2, 1] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S2[p1, p2] -> [p1, p2, 2]; S0[p1, p2] -> [p1, p2, 0]; S1[p1, p2] -> [p1, p2, 1] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #102
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : private_gaussian_filter_3_private_gaussian_filter_3/scop_0
	 *   block : 
	 *     for : 2 <= i <= M-1 (stride = 1)
	 *       S0 (depth = 1) [out[0][i-1]=sub(sub(mul(IntExpr(3),tmp[0][i-1]),tmp[0][i-2]),tmp[0][i])]
	 *       ([out[0][i-1]]) = f([tmp[0][i-1], tmp[0][i-2], tmp[0][i]])
	 *       (Domain = [M,N] -> {S0[i] : (i-2 >= 0) and (-i+M-1 >= 0)})
	 *     for : 1 <= i <= N-2 (stride = 1)
	 *       block : 
	 *         for : 0 <= j <= 1 (stride = 1)
	 *           S1 (depth = 2) [tmp[i][j]=sub(sub(mul(IntExpr(3),in[i][j]),in[i-1][j]),in[i+1][j])]
	 *           ([tmp[i][j]]) = f([in[i][j], in[i-1][j], in[i+1][j]])
	 *           (Domain = [M,N] -> {S1[i,j] : (j >= 0) and (-j+1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *         for : 2 <= j <= M-1 (stride = 1)
	 *           block : 
	 *             S2 (depth = 2) [tmp[i][j]=sub(sub(mul(IntExpr(3),in[i][j]),in[i-1][j]),in[i+1][j])]
	 *             ([tmp[i][j]]) = f([in[i][j], in[i-1][j], in[i+1][j]])
	 *             (Domain = [M,N] -> {S2[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *             S3 (depth = 2) [out[i][j-1]=sub(sub(mul(IntExpr(3),tmp[i][j-1]),tmp[i][j-2]),tmp[i][j])]
	 *             ([out[i][j-1]]) = f([tmp[i][j-1], tmp[i][j-2], tmp[i][j]])
	 *             (Domain = [M,N] -> {S3[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *     for : 2 <= i <= M-1 (stride = 1)
	 *       S4 (depth = 1) [out[N-1][i-1]=sub(sub(mul(IntExpr(3),tmp[N-1][i-1]),tmp[N-1][i-2]),tmp[N-1][i])]
	 *       ([out[N-1][i-1]]) = f([tmp[N-1][i-1], tmp[N-1][i-2], tmp[N-1][i]])
	 *       (Domain = [M,N] -> {S4[i] : (i-2 >= 0) and (-i+M-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_2D_gaussian_filter_GScopRoot_private_gaussian_filter_3_scop_0_ () {
		String path = "polymodel_others_2D_gaussian_filter_GScopRoot_private_gaussian_filter_3_scop_0_";
		// SCoP Extraction Data
		String domains = "[M, N] -> { S3[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S1[i, j] : j >= 0 and j <= 1 and i >= 1 and i <= -2 + N; S2[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S0[i] : i >= 2 and i <= -1 + M; S4[i] : i >= 2 and i <= -1 + M }";
		String idSchedules = "[M, N] -> { S4[i] -> [2, i, 0, 0, 0]; S1[i, j] -> [1, i, 0, j, 0]; S0[i] -> [0, i, 0, 0, 0]; S2[i, j] -> [1, i, 1, j, 0]; S3[i, j] -> [1, i, 1, j, 1] }";
		String writes = "[M, N] -> { S3[i, j] -> out[i, -1 + j]; S0[i] -> out[0, -1 + i]; S4[i] -> out[-1 + N, -1 + i]; S1[i, j] -> tmp[i, j]; S2[i, j] -> tmp[i, j] }";
		String reads = "[M, N] -> { S0[i] -> tmp[0, i]; S0[i] -> tmp[0, -1 + i]; S0[i] -> tmp[0, -2 + i]; S4[i] -> tmp[-1 + N, i]; S4[i] -> tmp[-1 + N, -1 + i]; S4[i] -> tmp[-1 + N, -2 + i]; S2[i, j] -> in[1 + i, j]; S2[i, j] -> in[i, j]; S2[i, j] -> in[-1 + i, j]; S3[i, j] -> tmp[i, j]; S3[i, j] -> tmp[i, -1 + j]; S3[i, j] -> tmp[i, -2 + j]; S1[i, j] -> in[1 + i, j]; S1[i, j] -> in[i, j]; S1[i, j] -> in[-1 + i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[M, N] -> { S3[i, j] -> S2[i, j'] : j' >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N and j' >= -2 + j and j' <= j; S3[i, j] -> S1[i, j'] : j' >= -2 + j and i >= 1 and i <= -2 + N and j' <= 1 and j >= 2 and j <= -1 + M }";
		String valueBasedPlutoSchedule = "[M, N] -> { S2[i, j] -> [i, j, 0]; S0[i] -> [i, 0, 0]; S3[i, j] -> [i, j, 2]; S1[i, j] -> [i, j, 1]; S4[i] -> [i, 0, 0] }";
		String valueBasedFeautrierSchedule = "[M, N] -> { S3[i, j] -> [i, j]; S1[i, j] -> [i, j]; S2[i, j] -> [i, j]; S4[i] -> [i, 0]; S0[i] -> [i, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[M, N] -> { S3[i, j] -> S1[i, j'] : j' >= -2 + j and i >= 1 and i <= -2 + N and j' <= 1 and j >= 2 and j <= -1 + M; S3[i, j] -> S2[i, j'] : j' >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N and j' >= -2 + j and j' <= j; S4[i] -> S0[i] : N = 1 and i >= 2 and i <= -1 + M }";
		String memoryBasedPlutoSchedule = "[M, N] -> { S2[i, j] -> [i, j, 0]; S0[i] -> [i, 0, 0]; S3[i, j] -> [i, j, 2]; S1[i, j] -> [i, j, 1]; S4[i] -> [i, 1, 0] }";
		String memoryBasedFeautrierSchedule = "[M, N] -> { S3[i, j] -> [i, j]; S1[i, j] -> [i, j]; S2[i, j] -> [i, j]; S4[i] -> [i, 0]; S0[i] -> [i, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #103
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : hybrid_jacobi_gauss_seidel_hybrid_jacobi_gauss_seidel/scop_0
	 *   for : 0 <= t <= newT-1 (stride = 1)
	 *     block : 
	 *       for : 0 <= i <= N-1 (stride = 1)
	 *         for : 0 <= j <= N-1 (stride = 1)
	 *           S0 (depth = 3) [A[i][j]=mul(add(add(add(add(B[i][j],A[i-1][j]),B[i+1][j]),B[i][j-1]),B[i][j+1]),0.20000000298023224)]
	 *           ([A[i][j]]) = f([B[i][j], A[i-1][j], B[i+1][j], B[i][j-1], B[i][j+1]])
	 *           (Domain = [newT,N] -> {S0[t,i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0) and (t >= 0) and (-t+newT-1 >= 0)})
	 *       for : 0 <= i <= N-1 (stride = 1)
	 *         for : 0 <= j <= N-1 (stride = 1)
	 *           S1 (depth = 3) [B[i][j]=mul(add(add(add(add(A[i][j],B[i-1][j]),A[i+1][j]),A[i][j-1]),A[i][j+1]),0.20000000298023224)]
	 *           ([B[i][j]]) = f([A[i][j], B[i-1][j], A[i+1][j], A[i][j-1], A[i][j+1]])
	 *           (Domain = [newT,N] -> {S1[t,i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0) and (t >= 0) and (-t+newT-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_openMP_HybridJacobiGaussSeidel_GScopRoot_hybrid_jacobi_gauss_seidel_scop_0_ () {
		String path = "polymodel_others_openMP_HybridJacobiGaussSeidel_GScopRoot_hybrid_jacobi_gauss_seidel_scop_0_";
		// SCoP Extraction Data
		String domains = "[newT, N] -> { S0[t, i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N and t >= 0 and t <= -1 + newT; S1[t, i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N and t >= 0 and t <= -1 + newT }";
		String idSchedules = "[newT, N] -> { S0[t, i, j] -> [0, t, 0, i, 0, j, 0]; S1[t, i, j] -> [0, t, 1, i, 0, j, 0] }";
		String writes = "[newT, N] -> { S1[t, i, j] -> B[i, j]; S0[t, i, j] -> A[i, j] }";
		String reads = "[newT, N] -> { S1[t, i, j] -> A[i, 1 + j]; S1[t, i, j] -> A[1 + i, j]; S1[t, i, j] -> A[i, j]; S1[t, i, j] -> A[i, -1 + j]; S1[t, i, j] -> B[-1 + i, j]; S0[t, i, j] -> B[i, 1 + j]; S0[t, i, j] -> B[1 + i, j]; S0[t, i, j] -> B[i, j]; S0[t, i, j] -> B[i, -1 + j]; S0[t, i, j] -> A[-1 + i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[newT, N] -> { S1[t, i, j] -> S1[t, -1 + i, j] : j >= 0 and j <= -1 + N and i >= 1 and i <= -1 + N and t >= 0 and t <= -1 + newT; S0[t, i, j] -> S1[-1 + t, i', j'] : j' >= 0 and j' <= -1 + N and j <= -1 + N and t >= 1 and t <= -1 + newT and j' <= 1 + i + j - i' and j >= 0 and i' >= i and i' <= -1 + N and j' >= -1 - i + j + i' and i >= 0; S0[t, i, j] -> S0[t, -1 + i, j] : j >= 0 and j <= -1 + N and i >= 1 and i <= -1 + N and t >= 0 and t <= -1 + newT; S1[t, i, j] -> S0[t, i', j'] : j' >= 0 and j' <= -1 + N and j' <= 1 + i + j - i' and j' >= -1 - i + j + i' and t <= -1 + newT and i >= 0 and i' >= i and t >= 0 and i' <= -1 + N and j <= -1 + N and j >= 0 }";
		String valueBasedPlutoSchedule = "[newT, N] -> { S0[t, i, j] -> [t, 2t + i, 2t + i + j, 0]; S1[t, i, j] -> [t, 1 + 2t + i, 1 + 2t + i + j, 1] }";
		String valueBasedFeautrierSchedule = "[newT, N] -> { S0[t, i, j] -> [4t + i, t, j]; S1[t, i, j] -> [2 + 4t + i, t, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[newT, N] -> { S1[t, i, j] -> S1[-1 + t, i', j] : t >= 1 and t <= -1 + newT and i' <= -1 + N and i' <= 1 + i and j >= 1 - i and i' >= i and j >= 0 and i >= 0 and j <= -1 + N; S1[t, i, j] -> S1[t, -1 + i, j] : t >= 0 and t <= -1 + newT and i >= 1 and i <= -1 + N and j >= 0 and j <= -1 + N; S1[t, 0, 0] -> S1[-1 + t, i', 0] : t >= 1 and t <= -1 + newT and i' >= 0 and i' <= -1 + N and i' <= 1; S0[t, i, j] -> S1[-1 + t, i', j'] : t >= 1 and t <= -1 + newT and i' <= -1 + N and j' <= 1 - i + j + i' and j' <= 1 + i + j - i' and j' <= -1 + N and j' >= -1 + i + j - i' and j' >= -1 - i + j + i' and i >= 0 and j <= -1 + N and j' >= 0 and i' >= 0 and i <= -1 + N and j >= 0; S0[t, i, j] -> S0[-1 + t, i', j] : t >= 1 and t <= -1 + newT and i >= 0 and i' <= -1 + N and i' >= i and j <= -1 + N and i' <= 1 + i and j >= 0; S0[t, i, j] -> S0[t, -1 + i, j] : t >= 0 and t <= -1 + newT and i >= 1 and i <= -1 + N and j >= 0 and j <= -1 + N; S1[t, i, j] -> S0[t, i', j'] : t <= -1 + newT and i' <= -1 + N and j' >= -1 + i + j - i' and j' <= 1 + i + j - i' and t >= 0 and i' >= 0 and j <= -1 + N and i <= -1 + N and i >= 0 and j' >= -1 - i + j + i' and j >= 0 and j' >= 0 and j' <= 1 - i + j + i' and j' <= -1 + N }";
		String memoryBasedPlutoSchedule = "[newT, N] -> { S0[t, i, j] -> [t, 2t + i, 2t + i + j, 0]; S1[t, i, j] -> [t, 1 + 2t + i, 1 + 2t + i + j, 1] }";
		String memoryBasedFeautrierSchedule = "[newT, N] -> { S0[t, i, j] -> [4t + i, t, j]; S1[t, i, j] -> [2 + 4t + i, t, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #104
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : reg_detect_scop_new
	 *   block : 
	 *     for : 0 <= j <= N-1 (stride = 1)
	 *       for : j <= i <= N-1 (stride = 1)
	 *         for : 0 <= k <= P-1 (stride = 1)
	 *           S0 (depth = 3) [diff[j][i][k]=sum_t[j][i]]
	 *           ([diff[j][i][k]]) = f([sum_t[j][i]])
	 *           (Domain = [N,P] -> {S0[j,i,k] : (k >= 0) and (-k+P-1 >= 0) and (-j+i >= 0) and (-i+N-1 >= 0) and (j >= 0) and (-j+N-1 >= 0)})
	 *     for : 0 <= j <= N-1 (stride = 1)
	 *       for : j <= i <= N-1 (stride = 1)
	 *         S1 (depth = 2) [sum_d[j][i][0]=diff[j][i][0]]
	 *         ([sum_d[j][i][0]]) = f([diff[j][i][0]])
	 *         (Domain = [N,P] -> {S1[j,i] : (-j+i >= 0) and (-i+N-1 >= 0) and (j >= 0) and (-j+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_reg_detect_GScopRoot_scop_new_ () {
		String path = "basics_oldFiles_reg_detect_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[N, P] -> { S1[j, i] : i >= j and i <= -1 + N and j >= 0 and j <= -1 + N; S0[j, i, k] : k >= 0 and k <= -1 + P and i >= j and i <= -1 + N and j >= 0 and j <= -1 + N }";
		String idSchedules = "[N, P] -> { S1[j, i] -> [1, j, 0, i, 0, 0, 0]; S0[j, i, k] -> [0, j, 0, i, 0, k, 0] }";
		String writes = "[N, P] -> { S0[j, i, k] -> diff[j, i, k]; S1[j, i] -> sum_d[j, i, 0] }";
		String reads = "[N, P] -> { S0[j, i, k] -> sum_t[j, i]; S1[j, i] -> diff[j, i, 0] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, P] -> { S1[j, i] -> S0[j, i, 0] : i >= j and i <= -1 + N and j >= 0 and j <= -1 + N and P >= 1 }";
		String valueBasedPlutoSchedule = "[N, P] -> { S0[j, i, k] -> [j, i, k, 0]; S1[j, i] -> [i, i, j, 1] }";
		String valueBasedFeautrierSchedule = "[N, P] -> { S0[j, i, k] -> [j, i, k]; S1[j, i] -> [j, i, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, P] -> { S1[j, i] -> S0[j, i, 0] : i >= j and i <= -1 + N and j >= 0 and j <= -1 + N and P >= 1 }";
		String memoryBasedPlutoSchedule = "[N, P] -> { S0[j, i, k] -> [j, i, k, 0]; S1[j, i] -> [i, i, j, 1] }";
		String memoryBasedFeautrierSchedule = "[N, P] -> { S0[j, i, k] -> [j, i, k]; S1[j, i] -> [j, i, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #105
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : reg_detect_scop_new
	 *   block : 
	 *     for : 1 <= k <= P-1 (stride = 1)
	 *       S0 (depth = 1) [sum_d[j][i][k]=add(sum_d[j][i][k-1],diff[j][i][k])]
	 *       ([sum_d[j][i][k]]) = f([sum_d[j][i][k-1], diff[j][i][k]])
	 *       (Domain = [P,j,i] -> {S0[k] : (k-1 >= 0) and (-k+P-1 >= 0)})
	 *     S1 (depth = 0) [mean[j][i]=sum_d[j][i][P-1]]
	 *     ([mean[j][i]]) = f([sum_d[j][i][P-1]])
	 *     (Domain = [P,j,i] -> {S1[] : })
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_reg_detect_GScopRoot_scop_new__0 () {
		String path = "basics_oldFiles_reg_detect_GScopRoot_scop_new__0";
		// SCoP Extraction Data
		String domains = "[P, j, i] -> { S1[]; S0[k] : k >= 1 and k <= -1 + P }";
		String idSchedules = "[P, j, i] -> { S1[] -> [1, 0, 0]; S0[k] -> [0, k, 0] }";
		String writes = "[P, j, i] -> { S0[k] -> sum_d[j, i, k]; S1[] -> mean[j, i] }";
		String reads = "[P, j, i] -> { S0[k] -> diff[j, i, k]; S1[] -> sum_d[j, i, -1 + P]; S0[k] -> sum_d[j, i, -1 + k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[P, j, i] -> { S1[] -> S0[-1 + P] : P >= 2; S0[k] -> S0[-1 + k] : k >= 2 and k <= -1 + P }";
		String valueBasedPlutoSchedule = "[P, j, i] -> { S0[k] -> [k, 0]; S1[] -> [P, 1] }";
		String valueBasedFeautrierSchedule = "[P, j, i] -> { S0[k] -> [k]; S1[] -> [P] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[P, j, i] -> { S1[] -> S0[-1 + P] : P >= 2; S0[k] -> S0[-1 + k] : k >= 2 and k <= -1 + P }";
		String memoryBasedPlutoSchedule = "[P, j, i] -> { S0[k] -> [k, 0]; S1[] -> [P, 1] }";
		String memoryBasedFeautrierSchedule = "[P, j, i] -> { S0[k] -> [k]; S1[] -> [P] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #106
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : reg_detect_reg_detect/scop_0
	 *   for : 0 <= j <= N-1 (stride = 1)
	 *     for : j <= i <= N-1 (stride = 1)
	 *       if : j > 0
	 *         S0 (depth = 2) [path[j][i]=add(path[j-1][i-1],mean[j][i])]
	 *         ([path[j][i]]) = f([path[j-1][i-1], mean[j][i]])
	 *         (Domain = [N] -> {S0[j,i] : (j-1 >= 0) and (-j+i >= 0) and (-i+N-1 >= 0) and (-j+N-1 >= 0)})
	 *       else
	 *         S1 (depth = 2) [path[j][i]=mean[j][i]]
	 *         ([path[j][i]]) = f([mean[j][i]])
	 *         (Domain = [N] -> {S1[j,i] : (j = 0) and (N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	// Lock the process in jenkins
//	public void test_basics_oldFiles_reg_detect_GScopRoot_reg_detect_scop_0_ () {
//		String path = "basics_oldFiles_reg_detect_GScopRoot_reg_detect_scop_0_";
//		// SCoP Extraction Data
//		String domains = "[N] -> { S0[j, i] : j >= 1 and i >= j and i <= -1 + N and j <= -1 + N; S1[0, i] : N >= 1 and i >= 0 and i <= -1 + N }";
//		String idSchedules = "[N] -> { S0[j, i] -> [0, j, 0, i, 0]; S1[j, i] -> [0, j, 0, i, 1] }";
//		String writes = "[N] -> { S0[j, i] -> path[j, i]; S1[j, i] -> path[j, i] }";
//		String reads = "[N] -> { S1[j, i] -> mean[j, i]; S0[j, i] -> path[-1 + j, -1 + i]; S0[j, i] -> mean[j, i] }";
//		// Value Based PRDG & Schedules
//		String valueBasedPrdg = "[N] -> { S0[j, i] -> S0[-1 + j, -1 + i] : j >= 2 and i >= j and i <= -1 + N and j <= -1 + N; S0[1, i] -> S1[0, -1 + i] : i <= -1 + N and i >= 1 and N >= 2 }";
//		String valueBasedPlutoSchedule = "[N] -> { S0[j, i] -> [j, i, 1]; S1[j, i] -> [0, i, 0] }";
//		String valueBasedFeautrierSchedule = "[N] -> { S1[j, i] -> [0, i]; S0[j, i] -> [i, j] }";
//		// Memory Based PRDG & Schedules
//		String memoryBasedPrdg = "[N] -> { S0[j, i] -> S0[-1 + j, -1 + i] : j >= 2 and i >= j and i <= -1 + N and j <= -1 + N; S0[1, i] -> S1[0, -1 + i] : i <= -1 + N and i >= 1 and N >= 2 }";
//		String memoryBasedPlutoSchedule = "[N] -> { S0[j, i] -> [j, i, 1]; S1[j, i] -> [0, i, 0] }";
//		String memoryBasedFeautrierSchedule = "[N] -> { S1[j, i] -> [0, i]; S0[j, i] -> [i, j] }";
//		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
//	}
	
	// SCoP #107
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : FIR_scop_new
	 *   block : 
	 *     for : 0 <= i <= BLOCK-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [y[i]=IntExpr(0)]
	 *         ([y[i]]) = f([])
	 *         (Domain = [BLOCK,NTAP] -> {S0[i] : (i >= 0) and (-i+BLOCK-1 >= 0)})
	 *         for : 0 <= j <= NTAP-1 (stride = 1)
	 *           if : i >= j
	 *             S1 (depth = 2) [y[i]=add(y[i],mul(x[i-j],c[j]))]
	 *             ([y[i]]) = f([y[i], x[i-j], c[j]])
	 *             (Domain = [BLOCK,NTAP] -> {S1[i,j] : (i-j >= 0) and (j >= 0) and (-j+NTAP-1 >= 0) and (i >= 0) and (-i+BLOCK-1 >= 0)})
	 *     for : 0 <= i <= BLOCK-1 (stride = 1)
	 *       S2 (depth = 1) [y[i]=add(IntExpr(6),call min())]
	 *       ([y[i]]) = f([])
	 *       (Domain = [BLOCK,NTAP] -> {S2[i] : (i >= 0) and (-i+BLOCK-1 >= 0)})
	 *     for : 0 <= i <= BLOCK-1 (stride = 1)
	 *       block : 
	 *         S3 (depth = 1) [y[i]=IntExpr(0)]
	 *         ([y[i]]) = f([])
	 *         (Domain = [BLOCK,NTAP] -> {S3[i] : (i >= 0) and (-i+BLOCK-1 >= 0)})
	 *         for : 0 <= j <= NTAP-1 (stride = 1)
	 *           if : i >= j
	 *             S4 (depth = 2) [y[i]=add(y[i],mul(x[i-j],c[j]))]
	 *             ([y[i]]) = f([y[i], x[i-j], c[j]])
	 *             (Domain = [BLOCK,NTAP] -> {S4[i,j] : (i-j >= 0) and (j >= 0) and (-j+NTAP-1 >= 0) and (i >= 0) and (-i+BLOCK-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_BlockBasedFIR_GScopRoot_scop_new_ () {
		String path = "polymodel_src_polybenchs_perso_BlockBasedFIR_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[BLOCK, NTAP] -> { S0[i] : i >= 0 and i <= -1 + BLOCK; S2[i] : i >= 0 and i <= -1 + BLOCK; S1[i, j] : j <= i and j >= 0 and j <= -1 + NTAP and i >= 0 and i <= -1 + BLOCK; S3[i] : i >= 0 and i <= -1 + BLOCK; S4[i, j] : j <= i and j >= 0 and j <= -1 + NTAP and i >= 0 and i <= -1 + BLOCK }";
		String idSchedules = "[BLOCK, NTAP] -> { S4[i, j] -> [2, i, 1, j, 0]; S2[i] -> [1, i, 0, 0, 0]; S0[i] -> [0, i, 0, 0, 0]; S1[i, j] -> [0, i, 1, j, 0]; S3[i] -> [2, i, 0, 0, 0] }";
		String writes = "[BLOCK, NTAP] -> { S3[i] -> y[i]; S0[i] -> y[i]; S1[i, j] -> y[i]; S4[i, j] -> y[i]; S2[i] -> y[i] }";
		String reads = "[BLOCK, NTAP] -> { S1[i, j] -> x[i - j]; S1[i, j] -> c[j]; S1[i, j] -> y[i]; S4[i, j] -> x[i - j]; S4[i, j] -> c[j]; S4[i, j] -> y[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[BLOCK, NTAP] -> { S1[i, 0] -> S0[i] : i >= 0 and i <= -1 + BLOCK and NTAP >= 1; S1[i, j] -> S1[i, -1 + j] : j <= i and i <= -1 + BLOCK and j <= -1 + NTAP and j >= 1 and i >= 0; S4[i, j] -> S4[i, -1 + j] : j <= i and i <= -1 + BLOCK and j <= -1 + NTAP and j >= 1 and i >= 0; S4[i, 0] -> S3[i] : i >= 0 and i <= -1 + BLOCK and NTAP >= 1 }";
		String valueBasedPlutoSchedule = "[BLOCK, NTAP] -> { S3[i] -> [0, i, 0]; S1[i, j] -> [i, i + j, 1]; S2[i] -> [i, 0, 0]; S4[i, j] -> [i, i + j, 1]; S0[i] -> [0, i, 0] }";
		String valueBasedFeautrierSchedule = "[BLOCK, NTAP] -> { S4[i, j] -> [1 + j, i]; S0[i] -> [0, i]; S3[i] -> [0, i]; S2[i] -> [i, 0]; S1[i, j] -> [1 + j, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[BLOCK, NTAP] -> { S1[i, 0] -> S0[i] : NTAP >= 1 and i >= 0 and i <= -1 + BLOCK; S2[i] -> S1[i, i] : i >= 0 and i <= -1 + BLOCK and i <= -2 + NTAP; S2[i] -> S1[i, -1 + NTAP] : i >= -1 + NTAP and i <= -1 + BLOCK and NTAP >= 1; S1[i, j] -> S1[i, -1 + j] : i <= -1 + BLOCK and j >= 1 and j <= i and j <= -1 + NTAP; S2[i] -> S0[i] : i >= 0 and i <= -1 + BLOCK and i >= -1 + NTAP and NTAP <= 0; S3[i] -> S2[i] : i >= 0 and i <= -1 + BLOCK; S4[i, j] -> S4[i, -1 + j] : i <= -1 + BLOCK and j >= 1 and j <= i and j <= -1 + NTAP; S4[i, 0] -> S3[i] : NTAP >= 1 and i >= 0 and i <= -1 + BLOCK }";
		String memoryBasedPlutoSchedule = "[BLOCK, NTAP] -> { S3[i] -> [i, 2i, 3]; S1[i, j] -> [i, i + j, 1]; S2[i] -> [i, 2i, 2]; S4[i, j] -> [i, 2i + j, 4]; S0[i] -> [0, i, 0] }";
		String memoryBasedFeautrierSchedule = "[BLOCK, NTAP] -> { S4[i, j] -> [4 + j, i]; S0[i] -> [-i, 0]; S3[i] -> [3, i]; S2[i] -> [2, i]; S1[i, j] -> [1 - i + j, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #108
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : hydro_fragment_hydro_fragment/scop_0
	 *   block : 
	 *     for : 1 <= l <= loop (stride = 1)
	 *       for : 0 <= k <= n-1 (stride = 1)
	 *         S0 (depth = 2) [x[k]=add(IntExpr(q),mul(y[k],add(mul(IntExpr(r),z[k+10]),mul(IntExpr(t),z[k+11]))))]
	 *         ([x[k]]) = f([y[k], z[k+10], z[k+11]])
	 *         (Domain = [loop,n,q,r,t] -> {S0[l,k] : (k >= 0) and (-k+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_01_hydro_fragment_GScopRoot_hydro_fragment_scop_0_ () {
		String path = "polymodel_src_livermore_loops_01_hydro_fragment_GScopRoot_hydro_fragment_scop_0_";
		// SCoP Extraction Data
		String domains = "[loop, n, q, r, t] -> { S0[l, k] : k >= 0 and k <= -1 + n and l >= 1 and l <= loop }";
		String idSchedules = "[loop, n, q, r, t] -> { S0[l, k] -> [0, l, 0, k, 0] }";
		String writes = "[loop, n, q, r, t] -> { S0[l, k] -> x[k] }";
		String reads = "[loop, n, q, r, t] -> { S0[l, k] -> y[k]; S0[l, k] -> z[11 + k]; S0[l, k] -> z[10 + k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[loop, n, q, r, t] -> {  }";
		String valueBasedPlutoSchedule = "[loop, n, q, r, t] -> { S0[l, k] -> [l, k] }";
		String valueBasedFeautrierSchedule = "[loop, n, q, r, t] -> { S0[l, k] -> [l, k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[loop, n, q, r, t] -> { S0[l, k] -> S0[-1 + l, k] : k >= 0 and k <= -1 + n and l >= 2 and l <= loop }";
		String memoryBasedPlutoSchedule = "[loop, n, q, r, t] -> { S0[l, k] -> [l, k] }";
		String memoryBasedFeautrierSchedule = "[loop, n, q, r, t] -> { S0[l, k] -> [l, k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #109
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : _2_D_explicit_hydrodynamics_fragment__2_D_explicit_hydrodynamics_fragment/scop_2
	 *   for : 1 <= k <= kn-1 (stride = 1)
	 *     for : 1 <= j <= jn-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 2) [za[k][j]=div(mul(sub(sub(add(zp[k+1][j-1],zq[k+1][j-1]),zp[k][j-1]),zq[k][j-1]),add(zr[k][j],zr[k][j-1])),add(zm[k][j-1],zm[k+1][j-1]))]
	 *         ([za[k][j]]) = f([zp[k+1][j-1], zq[k+1][j-1], zp[k][j-1], zq[k][j-1], zr[k][j], zr[k][j-1], zm[k][j-1], zm[k+1][j-1]])
	 *         (Domain = [kn,jn] -> {S0[k,j] : (j-1 >= 0) and (-j+jn-1 >= 0) and (k-1 >= 0) and (-k+kn-1 >= 0)})
	 *         S1 (depth = 2) [zb[k][j]=div(mul(sub(sub(add(zp[k][j-1],zq[k][j-1]),zp[k][j]),zq[k][j]),add(zr[k][j],zr[k-1][j])),add(zm[k][j],zm[k][j-1]))]
	 *         ([zb[k][j]]) = f([zp[k][j-1], zq[k][j-1], zp[k][j], zq[k][j], zr[k][j], zr[k-1][j], zm[k][j], zm[k][j-1]])
	 *         (Domain = [kn,jn] -> {S1[k,j] : (j-1 >= 0) and (-j+jn-1 >= 0) and (k-1 >= 0) and (-k+kn-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_18_2_D_explicit_hydrodynamics_fragment_GScopRoot__2_D_explicit_hydrodynamics_fragment_scop_2_ () {
		String path = "polymodel_src_livermore_loops_18_2_D_explicit_hydrodynamics_fragment_GScopRoot__2_D_explicit_hydrodynamics_fragment_scop_2_";
		// SCoP Extraction Data
		String domains = "[kn, jn] -> { S1[k, j] : j >= 1 and j <= -1 + jn and k >= 1 and k <= -1 + kn; S0[k, j] : j >= 1 and j <= -1 + jn and k >= 1 and k <= -1 + kn }";
		String idSchedules = "[kn, jn] -> { S1[k, j] -> [0, k, 0, j, 1]; S0[k, j] -> [0, k, 0, j, 0] }";
		String writes = "[kn, jn] -> { S0[k, j] -> za[k, j]; S1[k, j] -> zb[k, j] }";
		String reads = "[kn, jn] -> { S1[k, j] -> zq[k, j]; S1[k, j] -> zq[k, -1 + j]; S0[k, j] -> zr[k, j]; S0[k, j] -> zr[k, -1 + j]; S1[k, j] -> zm[k, j]; S1[k, j] -> zm[k, -1 + j]; S1[k, j] -> zp[k, j]; S1[k, j] -> zp[k, -1 + j]; S0[k, j] -> zm[1 + k, -1 + j]; S0[k, j] -> zm[k, -1 + j]; S0[k, j] -> zq[1 + k, -1 + j]; S0[k, j] -> zq[k, -1 + j]; S0[k, j] -> zp[1 + k, -1 + j]; S0[k, j] -> zp[k, -1 + j]; S1[k, j] -> zr[k, j]; S1[k, j] -> zr[-1 + k, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[kn, jn] -> {  }";
		String valueBasedPlutoSchedule = "[kn, jn] -> { S1[k, j] -> [k, j]; S0[k, j] -> [k, j] }";
		String valueBasedFeautrierSchedule = "[kn, jn] -> { S1[k, j] -> [k, j]; S0[k, j] -> [k, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[kn, jn] -> {  }";
		String memoryBasedPlutoSchedule = "[kn, jn] -> { S1[k, j] -> [k, j]; S0[k, j] -> [k, j] }";
		String memoryBasedFeautrierSchedule = "[kn, jn] -> { S1[k, j] -> [k, j]; S0[k, j] -> [k, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #110
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : _2_D_explicit_hydrodynamics_fragment__2_D_explicit_hydrodynamics_fragment/scop_0
	 *   for : 1 <= k <= kn-1 (stride = 1)
	 *     for : 1 <= j <= jn-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 2) [zr[k][j]=add(zr[k][j],mul(IntExpr(t),zu[k][j]))]
	 *         ([zr[k][j]]) = f([zr[k][j], zu[k][j]])
	 *         (Domain = [kn,jn,t] -> {S0[k,j] : (j-1 >= 0) and (-j+jn-1 >= 0) and (k-1 >= 0) and (-k+kn-1 >= 0)})
	 *         S1 (depth = 2) [zz[k][j]=add(zz[k][j],mul(IntExpr(t),zv[k][j]))]
	 *         ([zz[k][j]]) = f([zz[k][j], zv[k][j]])
	 *         (Domain = [kn,jn,t] -> {S1[k,j] : (j-1 >= 0) and (-j+jn-1 >= 0) and (k-1 >= 0) and (-k+kn-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_18_2_D_explicit_hydrodynamics_fragment_GScopRoot__2_D_explicit_hydrodynamics_fragment_scop_0_ () {
		String path = "polymodel_src_livermore_loops_18_2_D_explicit_hydrodynamics_fragment_GScopRoot__2_D_explicit_hydrodynamics_fragment_scop_0_";
		// SCoP Extraction Data
		String domains = "[kn, jn, t] -> { S1[k, j] : j >= 1 and j <= -1 + jn and k >= 1 and k <= -1 + kn; S0[k, j] : j >= 1 and j <= -1 + jn and k >= 1 and k <= -1 + kn }";
		String idSchedules = "[kn, jn, t] -> { S0[k, j] -> [0, k, 0, j, 0]; S1[k, j] -> [0, k, 0, j, 1] }";
		String writes = "[kn, jn, t] -> { S0[k, j] -> zr[k, j]; S1[k, j] -> zz[k, j] }";
		String reads = "[kn, jn, t] -> { S0[k, j] -> zr[k, j]; S0[k, j] -> zu[k, j]; S1[k, j] -> zv[k, j]; S1[k, j] -> zz[k, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[kn, jn, t] -> {  }";
		String valueBasedPlutoSchedule = "[kn, jn, t] -> { S1[k, j] -> [k, j]; S0[k, j] -> [k, j] }";
		String valueBasedFeautrierSchedule = "[kn, jn, t] -> { S1[k, j] -> [k, j]; S0[k, j] -> [k, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[kn, jn, t] -> {  }";
		String memoryBasedPlutoSchedule = "[kn, jn, t] -> { S1[k, j] -> [k, j]; S0[k, j] -> [k, j] }";
		String memoryBasedFeautrierSchedule = "[kn, jn, t] -> { S1[k, j] -> [k, j]; S0[k, j] -> [k, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #111
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : _2_D_explicit_hydrodynamics_fragment__2_D_explicit_hydrodynamics_fragment/scop_1
	 *   for : 1 <= k <= kn-1 (stride = 1)
	 *     for : 1 <= j <= jn-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 2) [zu[k][j]=add(zu[k][j],mul(IntExpr(s),add(sub(sub(mul(za[k][j],sub(zz[k][j],zz[k][j+1])),mul(za[k][j-1],sub(zz[k][j],zz[k][j-1]))),mul(zb[k][j],sub(zz[k][j],zz[k-1][j]))),mul(zb[k+1][j],sub(zz[k][j],zz[k+1][j])))))]
	 *         ([zu[k][j]]) = f([zu[k][j], za[k][j], zz[k][j], zz[k][j+1], za[k][j-1], zz[k][j], zz[k][j-1], zb[k][j], zz[k][j], zz[k-1][j], zb[k+1][j], zz[k][j], zz[k+1][j]])
	 *         (Domain = [kn,jn,s] -> {S0[k,j] : (j-1 >= 0) and (-j+jn-1 >= 0) and (k-1 >= 0) and (-k+kn-1 >= 0)})
	 *         S1 (depth = 2) [zv[k][j]=add(zv[k][j],mul(IntExpr(s),add(sub(sub(mul(za[k][j],sub(zr[k][j],zr[k][j+1])),mul(za[k][j-1],sub(zr[k][j],zr[k][j-1]))),mul(zb[k][j],sub(zr[k][j],zr[k-1][j]))),mul(zb[k+1][j],sub(zr[k][j],zr[k+1][j])))))]
	 *         ([zv[k][j]]) = f([zv[k][j], za[k][j], zr[k][j], zr[k][j+1], za[k][j-1], zr[k][j], zr[k][j-1], zb[k][j], zr[k][j], zr[k-1][j], zb[k+1][j], zr[k][j], zr[k+1][j]])
	 *         (Domain = [kn,jn,s] -> {S1[k,j] : (j-1 >= 0) and (-j+jn-1 >= 0) and (k-1 >= 0) and (-k+kn-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_18_2_D_explicit_hydrodynamics_fragment_GScopRoot__2_D_explicit_hydrodynamics_fragment_scop_1_ () {
		String path = "polymodel_src_livermore_loops_18_2_D_explicit_hydrodynamics_fragment_GScopRoot__2_D_explicit_hydrodynamics_fragment_scop_1_";
		// SCoP Extraction Data
		String domains = "[kn, jn, s] -> { S1[k, j] : j >= 1 and j <= -1 + jn and k >= 1 and k <= -1 + kn; S0[k, j] : j >= 1 and j <= -1 + jn and k >= 1 and k <= -1 + kn }";
		String idSchedules = "[kn, jn, s] -> { S0[k, j] -> [0, k, 0, j, 0]; S1[k, j] -> [0, k, 0, j, 1] }";
		String writes = "[kn, jn, s] -> { S1[k, j] -> zv[k, j]; S0[k, j] -> zu[k, j] }";
		String reads = "[kn, jn, s] -> { S0[k, j] -> za[k, j]; S0[k, j] -> za[k, -1 + j]; S1[k, j] -> zv[k, j]; S1[k, j] -> zr[k, 1 + j]; S1[k, j] -> zr[1 + k, j]; S1[k, j] -> zr[k, j]; S1[k, j] -> zr[-1 + k, j]; S1[k, j] -> zr[k, -1 + j]; S1[k, j] -> za[k, j]; S1[k, j] -> za[k, -1 + j]; S0[k, j] -> zb[1 + k, j]; S0[k, j] -> zb[k, j]; S0[k, j] -> zu[k, j]; S0[k, j] -> zz[k, 1 + j]; S0[k, j] -> zz[1 + k, j]; S0[k, j] -> zz[k, j]; S0[k, j] -> zz[-1 + k, j]; S0[k, j] -> zz[k, -1 + j]; S1[k, j] -> zb[1 + k, j]; S1[k, j] -> zb[k, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[kn, jn, s] -> {  }";
		String valueBasedPlutoSchedule = "[kn, jn, s] -> { S1[k, j] -> [k, j]; S0[k, j] -> [k, j] }";
		String valueBasedFeautrierSchedule = "[kn, jn, s] -> { S1[k, j] -> [k, j]; S0[k, j] -> [k, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[kn, jn, s] -> {  }";
		String memoryBasedPlutoSchedule = "[kn, jn, s] -> { S1[k, j] -> [k, j]; S0[k, j] -> [k, j] }";
		String memoryBasedFeautrierSchedule = "[kn, jn, s] -> { S1[k, j] -> [k, j]; S0[k, j] -> [k, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #112
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_doitgen_kernel_doitgen/scop_0
	 *   for : 0 <= r <= nr-1 (stride = 1)
	 *     for : 0 <= q <= nq-1 (stride = 1)
	 *       block : 
	 *         for : 0 <= p <= np-1 (stride = 1)
	 *           block : 
	 *             S0 (depth = 3) [sum[r][q][p]=IntExpr(0)]
	 *             ([sum[r][q][p]]) = f([])
	 *             (Domain = [nr,nq,np] -> {S0[r,q,p] : (p >= 0) and (-p+np-1 >= 0) and (q >= 0) and (-q+nq-1 >= 0) and (r >= 0) and (-r+nr-1 >= 0)})
	 *             for : 0 <= s <= np-1 (stride = 1)
	 *               S1 (depth = 4) [sum[r][q][p]=add(sum[r][q][p],mul(A[r][q][s],C4[s][p]))]
	 *               ([sum[r][q][p]]) = f([sum[r][q][p], A[r][q][s], C4[s][p]])
	 *               (Domain = [nr,nq,np] -> {S1[r,q,p,s] : (s >= 0) and (-s+np-1 >= 0) and (p >= 0) and (-p+np-1 >= 0) and (q >= 0) and (-q+nq-1 >= 0) and (r >= 0) and (-r+nr-1 >= 0)})
	 *         for : 0 <= p <= nr-1 (stride = 1)
	 *           S2 (depth = 3) [A[r][q][p]=sum[r][q][p]]
	 *           ([A[r][q][p]]) = f([sum[r][q][p]])
	 *           (Domain = [nr,nq,np] -> {S2[r,q,p] : (p >= 0) and (-p+nr-1 >= 0) and (q >= 0) and (-q+nq-1 >= 0) and (r >= 0) and (-r+nr-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_doitgen_doitgen_GScopRoot_kernel_doitgen_scop_0_ () {
		String path = "polybench_gecos_linear_algebra_kernels_doitgen_doitgen_GScopRoot_kernel_doitgen_scop_0_";
		// SCoP Extraction Data
		String domains = "[nr, nq, np] -> { S0[r, q, p] : p >= 0 and p <= -1 + np and q >= 0 and q <= -1 + nq and r >= 0 and r <= -1 + nr; S2[r, q, p] : p >= 0 and p <= -1 + nr and q >= 0 and q <= -1 + nq and r >= 0 and r <= -1 + nr; S1[r, q, p, s] : s >= 0 and s <= -1 + np and p >= 0 and p <= -1 + np and q >= 0 and q <= -1 + nq and r >= 0 and r <= -1 + nr }";
		String idSchedules = "[nr, nq, np] -> { S1[r, q, p, s] -> [0, r, 0, q, 0, p, 1, s, 0]; S2[r, q, p] -> [0, r, 0, q, 1, p, 0, 0, 0]; S0[r, q, p] -> [0, r, 0, q, 0, p, 0, 0, 0] }";
		String writes = "[nr, nq, np] -> { S1[r, q, p, s] -> sum[r, q, p]; S2[r, q, p] -> A[r, q, p]; S0[r, q, p] -> sum[r, q, p] }";
		String reads = "[nr, nq, np] -> { S1[r, q, p, s] -> sum[r, q, p]; S2[r, q, p] -> sum[r, q, p]; S1[r, q, p, s] -> C4[s, p]; S1[r, q, p, s] -> A[r, q, s] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[nr, nq, np] -> { S1[r, q, p, 0] -> S0[r, q, p] : r <= -1 + nr and np >= 1 and p >= 0 and p <= -1 + np and q >= 0 and q <= -1 + nq and r >= 0; S1[r, q, p, s] -> S1[r, q, p, -1 + s] : s >= 1 and s <= -1 + np and p >= 0 and p <= -1 + np and q >= 0 and q <= -1 + nq and r >= 0 and r <= -1 + nr; S2[r, q, p] -> S1[r, q, p, -1 + np] : p >= 0 and p <= -1 + nr and q >= 0 and q <= -1 + nq and r >= 0 and r <= -1 + nr and p <= -1 + np }";
		String valueBasedPlutoSchedule = "[nr, nq, np] -> { S1[r, q, p, s] -> [r, r + q, r + q + p, r + q + p + s, 1]; S0[r, q, p] -> [0, r, q, p, 0]; S2[r, q, p] -> [r, r + q, r + q + p, np + r + q + p, 2] }";
		String valueBasedFeautrierSchedule = "[nr, nq, np] -> { S1[r, q, p, s] -> [1 + s, q, p, r]; S0[r, q, p] -> [0, r, q, p]; S2[r, q, p] -> [1 + np, r, q, p] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[nr, nq, np] -> { S1[r, q, p, 0] -> S0[r, q, p] : r >= 0 and r <= -1 + nr and q >= 0 and q <= -1 + nq and p >= 0 and p <= -1 + np; S2[r, q, p] -> S1[r, q, p', p] : r >= 0 and r <= -1 + nr and q >= 0 and q <= -1 + nq and p' >= 0 and p' <= -1 + np and p >= 0 and p <= -1 + np and p <= -1 + nr; S2[r, q, p] -> S1[r, q, p, -1 + np] : r >= 0 and r <= -1 + nr and q >= 0 and q <= -1 + nq and p >= 0 and p <= -1 + np and p <= -1 + nr; S1[r, q, p, s] -> S1[r, q, p, -1 + s] : r >= 0 and r <= -1 + nr and q >= 0 and q <= -1 + nq and p >= 0 and p <= -1 + np and s >= 1 and s <= -1 + np }";
		String memoryBasedPlutoSchedule = "[nr, nq, np] -> { S1[r, q, p, s] -> [r, r + q, r + q + p, r + q + p + s, 1]; S0[r, q, p] -> [0, r, q, p, 0]; S2[r, q, p] -> [r, r + q, np + r + q, np + r + q + p, 2] }";
		String memoryBasedFeautrierSchedule = "[nr, nq, np] -> { S1[r, q, p, s] -> [1 + s, q, p, r]; S0[r, q, p] -> [0, r, q, p]; S2[r, q, p] -> [1 + np, r, q, p] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #113
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_cholesky_kernel_cholesky/scop_0
	 *   for : 0 <= i <= n-1 (stride = 1)
	 *     block : 
	 *       S0 (depth = 1) [x=A[i][i]]
	 *       ([x]) = f([A[i][i]])
	 *       (Domain = [n] -> {S0[i] : (i >= 0) and (-i+n-1 >= 0)})
	 *       for : 0 <= j <= i-1 (stride = 1)
	 *         S1 (depth = 2) [x=sub(x,mul(A[i][j],A[i][j]))]
	 *         ([x]) = f([x, A[i][j], A[i][j]])
	 *         (Domain = [n] -> {S1[i,j] : (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *       S2 (depth = 1) [p[i]=div(1.0,call sqrt(x))]
	 *       ([p[i]]) = f([x])
	 *       (Domain = [n] -> {S2[i] : (i >= 0) and (-i+n-1 >= 0)})
	 *       for : i+1 <= j <= n-1 (stride = 1)
	 *         block : 
	 *           S3 (depth = 2) [x=A[i][j]]
	 *           ([x]) = f([A[i][j]])
	 *           (Domain = [n] -> {S3[i,j] : (-i+j-1 >= 0) and (-j+n-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *           for : 0 <= k <= i-1 (stride = 1)
	 *             S4 (depth = 3) [x=sub(x,mul(A[j][k],A[i][k]))]
	 *             ([x]) = f([x, A[j][k], A[i][k]])
	 *             (Domain = [n] -> {S4[i,j,k] : (k >= 0) and (i-k-1 >= 0) and (-i+j-1 >= 0) and (-j+n-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *           S5 (depth = 2) [A[j][i]=mul(x,p[i])]
	 *           ([A[j][i]]) = f([x, p[i]])
	 *           (Domain = [n] -> {S5[i,j] : (-i+j-1 >= 0) and (-j+n-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_cholesky_cholesky_GScopRoot_kernel_cholesky_scop_0_ () {
		String path = "polybench_gecos_linear_algebra_kernels_cholesky_cholesky_GScopRoot_kernel_cholesky_scop_0_";
		// SCoP Extraction Data
		String domains = "[n] -> { S1[i, j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + n; S0[i] : i >= 0 and i <= -1 + n; S2[i] : i >= 0 and i <= -1 + n; S3[i, j] : j >= 1 + i and j <= -1 + n and i >= 0 and i <= -1 + n; S4[i, j, k] : k >= 0 and k <= -1 + i and j >= 1 + i and j <= -1 + n and i >= 0 and i <= -1 + n; S5[i, j] : j >= 1 + i and j <= -1 + n and i >= 0 and i <= -1 + n }";
		String idSchedules = "[n] -> { S1[i, j] -> [0, i, 1, j, 0, 0, 0]; S2[i] -> [0, i, 2, 0, 0, 0, 0]; S5[i, j] -> [0, i, 3, j, 2, 0, 0]; S4[i, j, k] -> [0, i, 3, j, 1, k, 0]; S0[i] -> [0, i, 0, 0, 0, 0, 0]; S3[i, j] -> [0, i, 3, j, 0, 0, 0] }";
		String writes = "[n] -> { S3[i, j] -> x[]; S4[i, j, k] -> x[]; S0[i] -> x[]; S2[i] -> p[i]; S5[i, j] -> A[j, i]; S1[i, j] -> x[] }";
		String reads = "[n] -> { S0[i] -> A[i, i]; S4[i, j, k] -> x[]; S1[i, j] -> x[]; S1[i, j] -> A[i, j]; S3[i, j] -> A[i, j]; S4[i, j, k] -> A[i, k]; S4[i, j, k] -> A[j, k]; S5[i, j] -> x[]; S5[i, j] -> p[i]; S2[i] -> x[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S4[i, j, k] -> S4[i, j, -1 + k] : k >= 1 and k <= -1 + i and j >= 1 + i and j <= -1 + n and i <= -1 + n and i >= 0; S4[i, j, 0] -> S3[i, j] : i >= 1 and j <= -1 + n and j >= 1 + i and i <= -1 + n; S1[i, j] -> S1[i, -1 + j] : i <= -1 + n and j <= -1 + i and j >= 1 and i >= 0; S1[i, j] -> S5[j, i] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + n; S5[i, j] -> S2[i] : j >= 1 + i and j <= -1 + n and i >= 0 and i <= -1 + n; S2[i] -> S1[i, -1 + i] : i >= 1 and i <= -1 + n; S2[0] -> S0[0] : n >= 1; S4[i, j, k] -> S5[k, i] : k >= 0 and k <= -1 + i and j >= 1 + i and j <= -1 + n; S4[i, j, k] -> S5[k, j] : k >= 0 and k <= -1 + i and j >= 1 + i and j <= -1 + n; S5[i, j] -> S4[i, j, -1 + i] : j >= 1 + i and j <= -1 + n and i >= 1 and i <= -1 + n; S1[i, 0] -> S0[i] : i <= -1 + n and i >= 1; S5[0, j] -> S3[0, j] : j >= 1 and j <= -1 + n and n >= 1 }";
		String valueBasedPlutoSchedule = "[n] -> { S1[i, j] -> [j, i, 2i, 5]; S2[i] -> [i, i, 2i, 4]; S4[i, j, k] -> [i, j, j + k, 1]; S0[i] -> [0, 0, i, 3]; S3[i, j] -> [0, i, j, 2]; S5[i, j] -> [i, j, i + j, 0] }";
		String valueBasedFeautrierSchedule = "[n] -> { S2[i] -> [1 + 3i, 0, 0]; S0[i] -> [0, i, 0]; S4[i, j, k] -> [1 + i + j + k, j, k]; S1[i, j] -> [1 + 2i + j, i, 0]; S5[i, j] -> [1 + 2i + j, i, 0]; S3[i, j] -> [0, i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S4[i, j, k] -> S4[i, j, -1 + k] : j >= 1 + i and j <= -1 + n and k >= 1 and k <= -1 + i; S3[i, 1 + i] -> S2[i] : i >= 0 and i <= -2 + n; S0[1] -> S3[0, -1 + n] : n >= 2; S2[i] -> S1[i, -1 + i] : i >= 1 and i <= -1 + n; S1[i, j] -> S5[j, i] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + n; S5[0, j] -> S3[0, j] : j >= 1 and j <= -1 + n and n >= 1; S5[i, j] -> S2[i] : j >= 1 + i and j <= -1 + n and i >= 0 and i <= -1 + n; S1[i, j] -> S1[i, -1 + j] : i <= -1 + n and j >= 1 and j <= -1 + i; S5[i, j] -> S4[i, j, -1 + i] : j >= 1 + i and j <= -1 + n and i >= 1 and i <= -1 + n; S2[0] -> S0[0] : n >= 1; S3[i, j] -> S4[i, -1 + j, -1 + i] : i >= 1 and j <= -1 + n and j >= 2 + i and i <= -1 + n; S3[i, 1 + i] -> S1[i, -1 + i] : i >= 1 and i <= -2 + n; S1[i, 0] -> S0[i] : i >= 1 and i <= -1 + n; S0[i] -> S4[-1 + i, -1 + n, -2 + i] : i >= 2 and i <= -1 + n; S3[i, j] -> S5[i, -1 + j] : i >= 0 and j >= 2 + i and j <= -1 + n; S4[i, j, k] -> S5[k, i] : k >= 0 and k <= -1 + i and j >= 1 + i and j <= -1 + n; S4[i, j, k] -> S5[k, j] : k >= 0 and k <= -1 + i and j >= 1 + i and j <= -1 + n; S4[i, j, 0] -> S3[i, j] : i >= 1 and j >= 1 + i and j <= -1 + n; S0[i] -> S5[-1 + i, -1 + n] : i >= 1 and i <= -1 + n; S3[0, 1] -> S0[0] : n >= 2; S3[0, j] -> S3[0, -1 + j] : j >= 2 and j <= -1 + n and n >= 1 }";
		String memoryBasedPlutoSchedule = "[n] -> { S1[i, j] -> [i, 0, j, 2]; S2[i] -> [i, 0, i, 1]; S4[i, j, k] -> [i, j, k, 2]; S0[i] -> [i, 0, 0, 0]; S3[i, j] -> [i, j, 0, 1]; S5[i, j] -> [i, j, j, 0] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S5[i, j] -> [i, 3, j, 2, 0]; S3[i, j] -> [i, 3, j, 0, 0]; S4[i, j, k] -> [i, 3, j, 1, k]; S0[i] -> [i, 0, 0, 0, 0]; S1[i, j] -> [i, 1, j, 0, 0]; S2[i] -> [i, 2, 0, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #114
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : tri_diagonal_elimination_tri_diagonal_elimination/scop_0
	 *   block : 
	 *     for : 1 <= l <= loop (stride = 1)
	 *       for : 1 <= i <= n-1 (stride = 1)
	 *         S0 (depth = 2) [x[i]=mul(z[i],sub(y[i],x[i-1]))]
	 *         ([x[i]]) = f([z[i], y[i], x[i-1]])
	 *         (Domain = [loop,n] -> {S0[l,i] : (i-1 >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_05_tri_diagonal_elimination_GScopRoot_tri_diagonal_elimination_scop_0_ () {
		String path = "polymodel_src_livermore_loops_05_tri_diagonal_elimination_GScopRoot_tri_diagonal_elimination_scop_0_";
		// SCoP Extraction Data
		String domains = "[loop, n] -> { S0[l, i] : i >= 1 and i <= -1 + n and l >= 1 and l <= loop }";
		String idSchedules = "[loop, n] -> { S0[l, i] -> [0, l, 0, i, 0] }";
		String writes = "[loop, n] -> { S0[l, i] -> x[i] }";
		String reads = "[loop, n] -> { S0[l, i] -> x[-1 + i]; S0[l, i] -> z[i]; S0[l, i] -> y[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[loop, n] -> { S0[l, i] -> S0[l, -1 + i] : i >= 2 and i <= -1 + n and l >= 1 and l <= loop }";
		String valueBasedPlutoSchedule = "[loop, n] -> { S0[l, i] -> [l, i] }";
		String valueBasedFeautrierSchedule = "[loop, n] -> { S0[l, i] -> [i, l] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[loop, n] -> { S0[l, i] -> S0[-1 + l, i'] : l >= 2 and l <= loop and i' <= 1 + i and i >= 1 and i' >= i and i' <= -1 + n; S0[l, i] -> S0[l, -1 + i] : l >= 1 and l <= loop and i >= 2 and i <= -1 + n }";
		String memoryBasedPlutoSchedule = "[loop, n] -> { S0[l, i] -> [l, l + i] }";
		String memoryBasedFeautrierSchedule = "[loop, n] -> { S0[l, i] -> [2l + i, l] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #115
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_covariance_scop_new
	 *   block : 
	 *     for : 0 <= j <= m-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [mean[j]=0.0]
	 *         ([mean[j]]) = f([])
	 *         (Domain = [m,n,float_n] -> {S0[j] : (j >= 0) and (-j+m-1 >= 0)})
	 *         for : 0 <= i <= n-1 (stride = 1)
	 *           S1 (depth = 2) [mean[j]=add(mean[j],data[i][j])]
	 *           ([mean[j]]) = f([mean[j], data[i][j]])
	 *           (Domain = [m,n,float_n] -> {S1[j,i] : (i >= 0) and (-i+n-1 >= 0) and (j >= 0) and (-j+m-1 >= 0)})
	 *         S2 (depth = 1) [mean[j]=div(mean[j],IntExpr(float_n))]
	 *         ([mean[j]]) = f([mean[j]])
	 *         (Domain = [m,n,float_n] -> {S2[j] : (j >= 0) and (-j+m-1 >= 0)})
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       for : 0 <= j <= m-1 (stride = 1)
	 *         S3 (depth = 2) [data[i][j]=sub(data[i][j],mean[j])]
	 *         ([data[i][j]]) = f([data[i][j], mean[j]])
	 *         (Domain = [m,n,float_n] -> {S3[i,j] : (j >= 0) and (-j+m-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *     for : 0 <= j1 <= m-1 (stride = 1)
	 *       for : j1 <= j2 <= m-1 (stride = 1)
	 *         block : 
	 *           S4 (depth = 2) [symmat[j1][j2]=0.0]
	 *           ([symmat[j1][j2]]) = f([])
	 *           (Domain = [m,n,float_n] -> {S4[j1,j2] : (-j1+j2 >= 0) and (-j2+m-1 >= 0) and (j1 >= 0) and (-j1+m-1 >= 0)})
	 *           for : 0 <= i <= n-1 (stride = 1)
	 *             S5 (depth = 3) [symmat[j1][j2]=add(symmat[j1][j2],mul(data[i][j1],data[i][j2]))]
	 *             ([symmat[j1][j2]]) = f([symmat[j1][j2], data[i][j1], data[i][j2]])
	 *             (Domain = [m,n,float_n] -> {S5[j1,j2,i] : (i >= 0) and (-i+n-1 >= 0) and (-j1+j2 >= 0) and (-j2+m-1 >= 0) and (j1 >= 0) and (-j1+m-1 >= 0)})
	 *           S6 (depth = 2) [symmat[j2][j1]=symmat[j1][j2]]
	 *           ([symmat[j2][j1]]) = f([symmat[j1][j2]])
	 *           (Domain = [m,n,float_n] -> {S6[j1,j2] : (-j1+j2 >= 0) and (-j2+m-1 >= 0) and (j1 >= 0) and (-j1+m-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_datamining_covariance_covariance_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_datamining_covariance_covariance_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[m, n, float_n] -> { S6[j1, j2] : j2 >= j1 and j2 <= -1 + m and j1 >= 0 and j1 <= -1 + m; S3[i, j] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n; S2[j] : j >= 0 and j <= -1 + m; S4[j1, j2] : j2 >= j1 and j2 <= -1 + m and j1 >= 0 and j1 <= -1 + m; S1[j, i] : i >= 0 and i <= -1 + n and j >= 0 and j <= -1 + m; S5[j1, j2, i] : i >= 0 and i <= -1 + n and j2 >= j1 and j2 <= -1 + m and j1 >= 0 and j1 <= -1 + m; S0[j] : j >= 0 and j <= -1 + m }";
		String idSchedules = "[m, n, float_n] -> { S4[j1, j2] -> [2, j1, 0, j2, 0, 0, 0]; S2[j] -> [0, j, 2, 0, 0, 0, 0]; S6[j1, j2] -> [2, j1, 0, j2, 2, 0, 0]; S5[j1, j2, i] -> [2, j1, 0, j2, 1, i, 0]; S3[i, j] -> [1, i, 0, j, 0, 0, 0]; S1[j, i] -> [0, j, 1, i, 0, 0, 0]; S0[j] -> [0, j, 0, 0, 0, 0, 0] }";
		String writes = "[m, n, float_n] -> { S3[i, j] -> data[i, j]; S2[j] -> mean[j]; S1[j, i] -> mean[j]; S6[j1, j2] -> symmat[j2, j1]; S4[j1, j2] -> symmat[j1, j2]; S0[j] -> mean[j]; S5[j1, j2, i] -> symmat[j1, j2] }";
		String reads = "[m, n, float_n] -> { S3[i, j] -> mean[j]; S3[i, j] -> data[i, j]; S2[j] -> mean[j]; S1[j, i] -> mean[j]; S6[j1, j2] -> symmat[j1, j2]; S5[j1, j2, i] -> data[i, j1]; S5[j1, j2, i] -> data[i, j2]; S1[j, i] -> data[i, j]; S5[j1, j2, i] -> symmat[j1, j2] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[m, n, float_n] -> { S2[j] -> S1[j, -1 + n] : j >= 0 and j <= -1 + m and n >= 1; S6[j1, j2] -> S5[j1, j2, -1 + n] : j2 >= j1 and j2 <= -1 + m and j1 >= 0 and j1 <= -1 + m and n >= 1; S2[j] -> S0[j] : j >= 0 and j <= -1 + m and n <= 0; S6[j1, j2] -> S4[j1, j2] : j2 >= j1 and j2 <= -1 + m and j1 >= 0 and j1 <= -1 + m and n <= 0; S5[j1, j2, 0] -> S4[j1, j2] : j1 <= -1 + m and n >= 1 and j2 >= j1 and j2 <= -1 + m and j1 >= 0; S1[j, 0] -> S0[j] : j <= -1 + m and n >= 1 and j >= 0; S1[j, i] -> S1[j, -1 + i] : i >= 1 and i <= -1 + n and j >= 0 and j <= -1 + m; S5[j1, j2, i] -> S3[i, j1] : i >= 0 and i <= -1 + n and j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0; S5[j1, j2, i] -> S3[i, j2] : i >= 0 and i <= -1 + n and j2 >= j1 and j2 <= -1 + m and j1 >= 0; S5[j1, j2, i] -> S5[j1, j2, -1 + i] : i >= 1 and i <= -1 + n and j2 >= j1 and j2 <= -1 + m and j1 >= 0 and j1 <= -1 + m; S3[i, j] -> S2[j] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n }";
		String valueBasedPlutoSchedule = "[m, n, float_n] -> { S5[j1, j2, i] -> [j1, n + j2, 1, i, 0]; S6[j1, j2] -> [j2, n + j2, 1, n + j1, 1]; S3[i, j] -> [0, i + j, 1, j, 1]; S1[j, i] -> [0, j, 0, i, 1]; S4[j1, j2] -> [0, n + j1, 0, j2, 0]; S2[j] -> [0, j, 1, 0, 0]; S0[j] -> [0, 0, 0, j, 0] }";
		String valueBasedFeautrierSchedule = "[m, n, float_n] -> { S5[j1, j2, i] -> [i, j2, j1]; S3[i, j] -> [i, j, 0]; S6[j1, j2] -> [j1, j2, 0]; S2[j] -> [j, 0, 0]; S1[j, i] -> [i, j, 0]; S0[j] -> [j, 0, 0]; S4[j1, j2] -> [j1, j2, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[m, n, float_n] -> { S2[j] -> S1[j, -1 + n] : n >= 1 and j >= 0 and j <= -1 + m; S6[j1, j2] -> S5[j1, j2, -1 + n] : n >= 1 and j1 >= 0 and j2 >= j1 and j2 <= -1 + m; S2[j] -> S0[j] : n <= 0 and j >= 0 and j <= -1 + m; S6[j1, j2] -> S4[j1, j2] : n <= 0 and j1 >= 0 and j2 >= j1 and j2 <= -1 + m; S5[j1, j2, 0] -> S4[j1, j2] : n >= 1 and j1 >= 0 and j2 >= j1 and j2 <= -1 + m; S3[i, j] -> S1[j, i] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n; S1[j, 0] -> S0[j] : n >= 1 and j >= 0 and j <= -1 + m; S1[j, i] -> S1[j, -1 + i] : j >= 0 and j <= -1 + m and i >= 1 and i <= -1 + n; S5[j1, j2, i] -> S3[i, j1] : i >= 0 and i <= -1 + n and j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0; S5[j1, j2, i] -> S3[i, j2] : i >= 0 and i <= -1 + n and j2 >= j1 and j2 <= -1 + m and j1 >= 0; S5[j1, j2, i] -> S5[j1, j2, -1 + i] : j1 >= 0 and j2 >= j1 and j2 <= -1 + m and i >= 1 and i <= -1 + n; S3[i, j] -> S2[j] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n }";
		String memoryBasedPlutoSchedule = "[m, n, float_n] -> { S5[j1, j2, i] -> [j1, n + j2, 1, i, 0]; S6[j1, j2] -> [j2, n + j2, 1, n + j1, 1]; S3[i, j] -> [0, i + j, 1, j, 1]; S1[j, i] -> [0, j, 0, i, 1]; S4[j1, j2] -> [0, n + j1, 0, j2, 0]; S2[j] -> [0, j, 1, 0, 0]; S0[j] -> [0, 0, 0, j, 0] }";
		String memoryBasedFeautrierSchedule = "[m, n, float_n] -> { S5[j1, j2, i] -> [i, j2, j1]; S3[i, j] -> [i, j, 0]; S6[j1, j2] -> [j1, j2, 0]; S2[j] -> [j, 0, 0]; S1[j, i] -> [i, j, 0]; S0[j] -> [j, 0, 0]; S4[j1, j2] -> [j1, j2, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #116
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : toto_toto/scop_0
	 *   block : 
	 *     for : 0 <= i <= 7 (stride = 1)
	 *       S0 (depth = 1) [a=add(a,IntExpr(i))]
	 *       ([a]) = f([a])
	 *       (Domain = [] -> {S0[i] : (i >= 0) and (-i+7 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_simple2_GScopRoot_toto_scop_0_ () {
		String path = "polymodel_src_corner_cases_simple2_GScopRoot_toto_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S0[i] : i >= 0 and i <= 7 }";
		String idSchedules = "{ S0[i] -> [0, i, 0] }";
		String writes = "{ S0[i] -> a[] }";
		String reads = "{ S0[i] -> a[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S0[i] -> S0[-1 + i] : i >= 1 and i <= 7 }";
		String valueBasedPlutoSchedule = "{ S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "{ S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S0[i] -> S0[-1 + i] : i <= 7 and i >= 1 }";
		String memoryBasedPlutoSchedule = "{ S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "{ S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #117
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : func_func/scop_0
	 *   for : 1 <= t <= NBRE_IT (stride = 1)
	 *     for : 1 <= i <= NR (stride = 1)
	 *       for : 1 <= j <= NZ (stride = 1)
	 *         block : 
	 *           S0 (depth = 3) [Hr[i][j]=add(add(mul(hr_cof01[i][j],Hr[i][j]),mul(hr_cof02[i][j],sub(Eq[i][j+1],Eq[i][j]))),mul(hr_cof03[i][j],Ez[i][j]))]
	 *           ([Hr[i][j]]) = f([hr_cof01[i][j], Hr[i][j], hr_cof02[i][j], Eq[i][j+1], Eq[i][j], hr_cof03[i][j], Ez[i][j]])
	 *           (Domain = [NBRE_IT,NR,NZ,N_EX_HR,N_EX_HQ,N_EX_HZ,N_EX_ER,N_EX_EQ,N_EX_EZ] -> {S0[t,i,j] : (j-1 >= 0) and (-j+NZ >= 0) and (i-1 >= 0) and (-i+NR >= 0) and (t-1 >= 0) and (-t+NBRE_IT >= 0)})
	 *           S1 (depth = 3) [Hq[i][j]=add(add(mul(hq_cof01[i][j],Hq[i][j]),mul(hq_cof02[i][j],sub(Er[i][j+1],Er[i][j]))),mul(hq_cof03[i][j],sub(Ez[i+1][j],Ez[i][j])))]
	 *           ([Hq[i][j]]) = f([hq_cof01[i][j], Hq[i][j], hq_cof02[i][j], Er[i][j+1], Er[i][j], hq_cof03[i][j], Ez[i+1][j], Ez[i][j]])
	 *           (Domain = [NBRE_IT,NR,NZ,N_EX_HR,N_EX_HQ,N_EX_HZ,N_EX_ER,N_EX_EQ,N_EX_EZ] -> {S1[t,i,j] : (j-1 >= 0) and (-j+NZ >= 0) and (i-1 >= 0) and (-i+NR >= 0) and (t-1 >= 0) and (-t+NBRE_IT >= 0)})
	 *           S2 (depth = 3) [Hz[i][j]=add(sub(add(mul(hz_cof01[i][j],Hz[i][j]),mul(hz_cof02[i][j],Eq[i+1][j])),mul(hz_cof02b[i][j],Eq[i][j])),mul(hz_cof03[i][j],Er[i][j]))]
	 *           ([Hz[i][j]]) = f([hz_cof01[i][j], Hz[i][j], hz_cof02[i][j], Eq[i+1][j], hz_cof02b[i][j], Eq[i][j], hz_cof03[i][j], Er[i][j]])
	 *           (Domain = [NBRE_IT,NR,NZ,N_EX_HR,N_EX_HQ,N_EX_HZ,N_EX_ER,N_EX_EQ,N_EX_EZ] -> {S2[t,i,j] : (j-1 >= 0) and (-j+NZ >= 0) and (i-1 >= 0) and (-i+NR >= 0) and (t-1 >= 0) and (-t+NBRE_IT >= 0)})
	 *           for : 0 <= n_ex <= N_EX_HR-1 (stride = 1)
	 *             S3 (depth = 4) [Hr[i][j]=call exciteHr(IntExpr(t),IntExpr(i),IntExpr(j),IntExpr(n_ex),Hr[i][j])]
	 *             ([Hr[i][j]]) = f([Hr[i][j]])
	 *             (Domain = [NBRE_IT,NR,NZ,N_EX_HR,N_EX_HQ,N_EX_HZ,N_EX_ER,N_EX_EQ,N_EX_EZ] -> {S3[t,i,j,n_ex] : (n_ex >= 0) and (-n_ex+N_EX_HR-1 >= 0) and (j-1 >= 0) and (-j+NZ >= 0) and (i-1 >= 0) and (-i+NR >= 0) and (t-1 >= 0) and (-t+NBRE_IT >= 0)})
	 *           for : 0 <= n_ex <= N_EX_HQ-1 (stride = 1)
	 *             S4 (depth = 4) [Hq[i][j]=call exciteHq(IntExpr(t),IntExpr(i),IntExpr(j),IntExpr(n_ex),Hq[i][j])]
	 *             ([Hq[i][j]]) = f([Hq[i][j]])
	 *             (Domain = [NBRE_IT,NR,NZ,N_EX_HR,N_EX_HQ,N_EX_HZ,N_EX_ER,N_EX_EQ,N_EX_EZ] -> {S4[t,i,j,n_ex] : (n_ex >= 0) and (-n_ex+N_EX_HQ-1 >= 0) and (j-1 >= 0) and (-j+NZ >= 0) and (i-1 >= 0) and (-i+NR >= 0) and (t-1 >= 0) and (-t+NBRE_IT >= 0)})
	 *           for : 0 <= n_ex <= N_EX_HZ-1 (stride = 1)
	 *             S5 (depth = 4) [Hz[i][j]=call exciteHz(IntExpr(t),IntExpr(i),IntExpr(j),IntExpr(n_ex),Hz[i][j])]
	 *             ([Hz[i][j]]) = f([Hz[i][j]])
	 *             (Domain = [NBRE_IT,NR,NZ,N_EX_HR,N_EX_HQ,N_EX_HZ,N_EX_ER,N_EX_EQ,N_EX_EZ] -> {S5[t,i,j,n_ex] : (n_ex >= 0) and (-n_ex+N_EX_HZ-1 >= 0) and (j-1 >= 0) and (-j+NZ >= 0) and (i-1 >= 0) and (-i+NR >= 0) and (t-1 >= 0) and (-t+NBRE_IT >= 0)})
	 *           S6 (depth = 3) [Er[i][j]=add(add(mul(er_cof01[i][j],Er[i][j]),mul(er_cof02[i][j],sub(Hq[i][j],Hq[i][j-1]))),mul(er_cof03[i][j],Hz[i][j]))]
	 *           ([Er[i][j]]) = f([er_cof01[i][j], Er[i][j], er_cof02[i][j], Hq[i][j], Hq[i][j-1], er_cof03[i][j], Hz[i][j]])
	 *           (Domain = [NBRE_IT,NR,NZ,N_EX_HR,N_EX_HQ,N_EX_HZ,N_EX_ER,N_EX_EQ,N_EX_EZ] -> {S6[t,i,j] : (j-1 >= 0) and (-j+NZ >= 0) and (i-1 >= 0) and (-i+NR >= 0) and (t-1 >= 0) and (-t+NBRE_IT >= 0)})
	 *           S7 (depth = 3) [Eq[i][j]=add(add(mul(eq_cof01[i][j],Eq[i][j]),mul(eq_cof02[i][j],sub(Hr[i][j],Hr[i][j-1]))),mul(eq_cof03[i][j],sub(Hz[i][j],Hz[i-1][j])))]
	 *           ([Eq[i][j]]) = f([eq_cof01[i][j], Eq[i][j], eq_cof02[i][j], Hr[i][j], Hr[i][j-1], eq_cof03[i][j], Hz[i][j], Hz[i-1][j]])
	 *           (Domain = [NBRE_IT,NR,NZ,N_EX_HR,N_EX_HQ,N_EX_HZ,N_EX_ER,N_EX_EQ,N_EX_EZ] -> {S7[t,i,j] : (j-1 >= 0) and (-j+NZ >= 0) and (i-1 >= 0) and (-i+NR >= 0) and (t-1 >= 0) and (-t+NBRE_IT >= 0)})
	 *           S8 (depth = 3) [Ez[i][j]=add(sub(add(mul(ez_cof01[i][j],Ez[i][j]),mul(ez_cof02[i][j],Hq[i][j])),mul(ez_cof02b[i][j],Hq[i-1][j])),mul(ez_cof03[i][j],Hr[i][j]))]
	 *           ([Ez[i][j]]) = f([ez_cof01[i][j], Ez[i][j], ez_cof02[i][j], Hq[i][j], ez_cof02b[i][j], Hq[i-1][j], ez_cof03[i][j], Hr[i][j]])
	 *           (Domain = [NBRE_IT,NR,NZ,N_EX_HR,N_EX_HQ,N_EX_HZ,N_EX_ER,N_EX_EQ,N_EX_EZ] -> {S8[t,i,j] : (j-1 >= 0) and (-j+NZ >= 0) and (i-1 >= 0) and (-i+NR >= 0) and (t-1 >= 0) and (-t+NBRE_IT >= 0)})
	 *           for : 0 <= n_ex <= N_EX_ER-1 (stride = 1)
	 *             S9 (depth = 4) [Er[i][j]=call exciteEr(IntExpr(t),IntExpr(i),IntExpr(j),IntExpr(n_ex),Er[i][j])]
	 *             ([Er[i][j]]) = f([Er[i][j]])
	 *             (Domain = [NBRE_IT,NR,NZ,N_EX_HR,N_EX_HQ,N_EX_HZ,N_EX_ER,N_EX_EQ,N_EX_EZ] -> {S9[t,i,j,n_ex] : (n_ex >= 0) and (-n_ex+N_EX_ER-1 >= 0) and (j-1 >= 0) and (-j+NZ >= 0) and (i-1 >= 0) and (-i+NR >= 0) and (t-1 >= 0) and (-t+NBRE_IT >= 0)})
	 *           for : 0 <= n_ex <= N_EX_EQ-1 (stride = 1)
	 *             S10 (depth = 4) [Eq[i][j]=call exciteEq(IntExpr(t),IntExpr(i),IntExpr(j),IntExpr(n_ex),Eq[i][j])]
	 *             ([Eq[i][j]]) = f([Eq[i][j]])
	 *             (Domain = [NBRE_IT,NR,NZ,N_EX_HR,N_EX_HQ,N_EX_HZ,N_EX_ER,N_EX_EQ,N_EX_EZ] -> {S10[t,i,j,n_ex] : (n_ex >= 0) and (-n_ex+N_EX_EQ-1 >= 0) and (j-1 >= 0) and (-j+NZ >= 0) and (i-1 >= 0) and (-i+NR >= 0) and (t-1 >= 0) and (-t+NBRE_IT >= 0)})
	 *           for : 0 <= n_ex <= N_EX_EZ-1 (stride = 1)
	 *             S11 (depth = 4) [Ez[i][j]=call exciteEz(IntExpr(t),IntExpr(i),IntExpr(j),IntExpr(n_ex),Ez[i][j])]
	 *             ([Ez[i][j]]) = f([Ez[i][j]])
	 *             (Domain = [NBRE_IT,NR,NZ,N_EX_HR,N_EX_HQ,N_EX_HZ,N_EX_ER,N_EX_EQ,N_EX_EZ] -> {S11[t,i,j,n_ex] : (n_ex >= 0) and (-n_ex+N_EX_EZ-1 >= 0) and (j-1 >= 0) and (-j+NZ >= 0) and (i-1 >= 0) and (-i+NR >= 0) and (t-1 >= 0) and (-t+NBRE_IT >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_bor_fdtd_GScopRoot_func_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_bor_fdtd_GScopRoot_func_scop_0_";
		// SCoP Extraction Data
		String domains = "[NBRE_IT, NR, NZ, N_EX_HR, N_EX_HQ, N_EX_HZ, N_EX_ER, N_EX_EQ, N_EX_EZ] -> { S3[t, i, j, n_ex] : n_ex >= 0 and n_ex <= -1 + N_EX_HR and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S1[t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S4[t, i, j, n_ex] : n_ex >= 0 and n_ex <= -1 + N_EX_HQ and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S8[t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S6[t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S9[t, i, j, n_ex] : n_ex >= 0 and n_ex <= -1 + N_EX_ER and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S7[t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S10[t, i, j, n_ex] : n_ex >= 0 and n_ex <= -1 + N_EX_EQ and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S0[t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S5[t, i, j, n_ex] : n_ex >= 0 and n_ex <= -1 + N_EX_HZ and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S11[t, i, j, n_ex] : n_ex >= 0 and n_ex <= -1 + N_EX_EZ and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S2[t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT }";
		String idSchedules = "[NBRE_IT, NR, NZ, N_EX_HR, N_EX_HQ, N_EX_HZ, N_EX_ER, N_EX_EQ, N_EX_EZ] -> { S6[t, i, j] -> [0, t, 0, i, 0, j, 6, 0, 0]; S7[t, i, j] -> [0, t, 0, i, 0, j, 7, 0, 0]; S0[t, i, j] -> [0, t, 0, i, 0, j, 0, 0, 0]; S10[t, i, j, n_ex] -> [0, t, 0, i, 0, j, 10, n_ex, 0]; S5[t, i, j, n_ex] -> [0, t, 0, i, 0, j, 5, n_ex, 0]; S2[t, i, j] -> [0, t, 0, i, 0, j, 2, 0, 0]; S11[t, i, j, n_ex] -> [0, t, 0, i, 0, j, 11, n_ex, 0]; S3[t, i, j, n_ex] -> [0, t, 0, i, 0, j, 3, n_ex, 0]; S4[t, i, j, n_ex] -> [0, t, 0, i, 0, j, 4, n_ex, 0]; S8[t, i, j] -> [0, t, 0, i, 0, j, 8, 0, 0]; S9[t, i, j, n_ex] -> [0, t, 0, i, 0, j, 9, n_ex, 0]; S1[t, i, j] -> [0, t, 0, i, 0, j, 1, 0, 0] }";
		String writes = "[NBRE_IT, NR, NZ, N_EX_HR, N_EX_HQ, N_EX_HZ, N_EX_ER, N_EX_EQ, N_EX_EZ] -> { S8[t, i, j] -> Ez[i, j]; S10[t, i, j, n_ex] -> Eq[i, j]; S1[t, i, j] -> Hq[i, j]; S2[t, i, j] -> Hz[i, j]; S5[t, i, j, n_ex] -> Hz[i, j]; S11[t, i, j, n_ex] -> Ez[i, j]; S3[t, i, j, n_ex] -> Hr[i, j]; S9[t, i, j, n_ex] -> Er[i, j]; S4[t, i, j, n_ex] -> Hq[i, j]; S0[t, i, j] -> Hr[i, j]; S7[t, i, j] -> Eq[i, j]; S6[t, i, j] -> Er[i, j] }";
		String reads = "[NBRE_IT, NR, NZ, N_EX_HR, N_EX_HQ, N_EX_HZ, N_EX_ER, N_EX_EQ, N_EX_EZ] -> { S2[t, i, j] -> Er[i, j]; S0[t, i, j] -> Ez[i, j]; S6[t, i, j] -> er_cof01[i, j]; S2[t, i, j] -> hz_cof03[i, j]; S4[t, i, j, n_ex] -> Hq[i, j]; S7[t, i, j] -> eq_cof01[i, j]; S1[t, i, j] -> Hq[i, j]; S0[t, i, j] -> Eq[i, 1 + j]; S0[t, i, j] -> Eq[i, j]; S6[t, i, j] -> Hq[i, j]; S6[t, i, j] -> Hq[i, -1 + j]; S6[t, i, j] -> Er[i, j]; S6[t, i, j] -> er_cof03[i, j]; S2[t, i, j] -> hz_cof02[i, j]; S0[t, i, j] -> hr_cof01[i, j]; S8[t, i, j] -> Hq[i, j]; S8[t, i, j] -> Hq[-1 + i, j]; S7[t, i, j] -> eq_cof03[i, j]; S2[t, i, j] -> hz_cof01[i, j]; S0[t, i, j] -> hr_cof02[i, j]; S1[t, i, j] -> hq_cof01[i, j]; S3[t, i, j, n_ex] -> Hr[i, j]; S7[t, i, j] -> eq_cof02[i, j]; S8[t, i, j] -> ez_cof02[i, j]; S6[t, i, j] -> er_cof02[i, j]; S2[t, i, j] -> hz_cof02b[i, j]; S8[t, i, j] -> Hr[i, j]; S9[t, i, j, n_ex] -> Er[i, j]; S1[t, i, j] -> hq_cof03[i, j]; S2[t, i, j] -> Hz[i, j]; S8[t, i, j] -> ez_cof02b[i, j]; S5[t, i, j, n_ex] -> Hz[i, j]; S7[t, i, j] -> Eq[i, j]; S8[t, i, j] -> ez_cof01[i, j]; S10[t, i, j, n_ex] -> Eq[i, j]; S1[t, i, j] -> Er[i, 1 + j]; S1[t, i, j] -> Er[i, j]; S7[t, i, j] -> Hr[i, j]; S7[t, i, j] -> Hr[i, -1 + j]; S0[t, i, j] -> hr_cof03[i, j]; S1[t, i, j] -> hq_cof02[i, j]; S2[t, i, j] -> Eq[1 + i, j]; S2[t, i, j] -> Eq[i, j]; S8[t, i, j] -> ez_cof03[i, j]; S0[t, i, j] -> Hr[i, j]; S8[t, i, j] -> Ez[i, j]; S11[t, i, j, n_ex] -> Ez[i, j]; S7[t, i, j] -> Hz[i, j]; S7[t, i, j] -> Hz[-1 + i, j]; S6[t, i, j] -> Hz[i, j]; S1[t, i, j] -> Ez[1 + i, j]; S1[t, i, j] -> Ez[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[NBRE_IT, NR, NZ, N_EX_HR, N_EX_HQ, N_EX_HZ, N_EX_ER, N_EX_EQ, N_EX_EZ] -> { S0[t, i, j] -> S8[-1 + t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_EZ <= 0 and t <= NBRE_IT and t >= 2; S1[t, i, j] -> S6[-1 + t, i, j'] : t >= 2 and i >= 1 and i <= NR and j' >= j and t <= NBRE_IT and j' <= NZ and j >= 1 and j' <= 1 + j and N_EX_ER <= 0; S1[t, i, j] -> S8[-1 + t, i', j] : j >= 1 and j <= NZ and t >= 2 and i' >= i and t <= NBRE_IT and i' <= NR and i' <= 1 + i and N_EX_EZ <= 0 and i >= 1; S3[t, i, j, n_ex] -> S3[t, i, j, -1 + n_ex] : n_ex >= 1 and n_ex <= -1 + N_EX_HR and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S8[t, i, j] -> S3[t, i, j, -1 + N_EX_HR] : j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT and N_EX_HR >= 1; S8[t, i, j] -> S0[t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT and N_EX_HR <= 0; S6[t, i, j] -> S1[t, i, j'] : j' >= 1 and j' >= -1 + j and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT and j' <= j and j <= NZ and N_EX_HQ <= 0; S4[t, i, j, 0] -> S1[t, i, j] : t <= NBRE_IT and N_EX_HQ >= 1 and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1; S11[t, i, j, n_ex] -> S11[t, i, j, -1 + n_ex] : n_ex >= 1 and n_ex <= -1 + N_EX_EZ and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S8[t, i, j] -> S4[t, i', j, -1 + N_EX_HQ] : j >= 1 and j <= NZ and i' >= 1 and i' >= -1 + i and t >= 1 and t <= NBRE_IT and i <= NR and N_EX_HQ >= 1 and i' <= i; S2[t, i, j] -> S9[-1 + t, i, j, -1 + N_EX_ER] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_ER >= 1 and t <= NBRE_IT and t >= 2; S10[t, i, j, n_ex] -> S10[t, i, j, -1 + n_ex] : n_ex >= 1 and n_ex <= -1 + N_EX_EQ and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S1[t, i, j] -> S1[-1 + t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_HQ <= 0 and t <= NBRE_IT and t >= 2; S6[t, i, j] -> S5[t, i, j, -1 + N_EX_HZ] : j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT and N_EX_HZ >= 1; S1[t, i, j] -> S11[-1 + t, i', j, -1 + N_EX_EZ] : j >= 1 and j <= NZ and t >= 2 and i' >= i and t <= NBRE_IT and i' <= NR and i' <= 1 + i and N_EX_EZ >= 1 and i >= 1; S7[t, i, j] -> S0[t, i, j'] : j' >= 1 and j' >= -1 + j and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT and j' <= j and j <= NZ and N_EX_HR <= 0; S9[t, i, j, n_ex] -> S9[t, i, j, -1 + n_ex] : n_ex >= 1 and n_ex <= -1 + N_EX_ER and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S2[t, i, j] -> S5[-1 + t, i, j, -1 + N_EX_HZ] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_HZ >= 1 and t <= NBRE_IT and t >= 2; S8[t, i, j] -> S1[t, i', j] : j >= 1 and j <= NZ and i' >= 1 and i' >= -1 + i and t >= 1 and t <= NBRE_IT and i <= NR and N_EX_HQ <= 0 and i' <= i; S6[t, i, j] -> S4[t, i, j', -1 + N_EX_HQ] : j' >= 1 and j' >= -1 + j and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT and j' <= j and j <= NZ and N_EX_HQ >= 1; S8[t, i, j] -> S8[-1 + t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_EZ <= 0 and t <= NBRE_IT and t >= 2; S2[t, i, j] -> S2[-1 + t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_HZ <= 0 and t <= NBRE_IT and t >= 2; S0[t, i, j] -> S3[-1 + t, i, j, -1 + N_EX_HR] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_HR >= 1 and t <= NBRE_IT and t >= 2; S0[t, i, j] -> S11[-1 + t, i, j, -1 + N_EX_EZ] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_EZ >= 1 and t <= NBRE_IT and t >= 2; S5[t, i, j, n_ex] -> S5[t, i, j, -1 + n_ex] : n_ex >= 1 and n_ex <= -1 + N_EX_HZ and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT; S6[t, i, j] -> S9[-1 + t, i, j, -1 + N_EX_ER] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_ER >= 1 and t <= NBRE_IT and t >= 2; S3[t, i, j, 0] -> S0[t, i, j] : t <= NBRE_IT and N_EX_HR >= 1 and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1; S7[t, i, j] -> S3[t, i, j', -1 + N_EX_HR] : j' >= 1 and j' >= -1 + j and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT and j' <= j and j <= NZ and N_EX_HR >= 1; S7[t, i, j] -> S7[-1 + t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_EQ <= 0 and t <= NBRE_IT and t >= 2; S7[t, i, j] -> S5[t, i', j, -1 + N_EX_HZ] : j >= 1 and j <= NZ and i' >= 1 and i' >= -1 + i and t >= 1 and t <= NBRE_IT and i <= NR and N_EX_HZ >= 1 and i' <= i; S11[t, i, j, 0] -> S8[t, i, j] : t <= NBRE_IT and N_EX_EZ >= 1 and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1; S7[t, i, j] -> S10[-1 + t, i, j, -1 + N_EX_EQ] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_EQ >= 1 and t <= NBRE_IT and t >= 2; S9[t, i, j, 0] -> S6[t, i, j] : t <= NBRE_IT and N_EX_ER >= 1 and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1; S0[t, i, j] -> S7[-1 + t, i, j'] : t >= 2 and i >= 1 and i <= NR and j' >= j and t <= NBRE_IT and j' <= NZ and j >= 1 and j' <= 1 + j and N_EX_EQ <= 0; S6[t, i, j] -> S2[t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT and N_EX_HZ <= 0; S5[t, i, j, 0] -> S2[t, i, j] : t <= NBRE_IT and N_EX_HZ >= 1 and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1; S7[t, i, j] -> S2[t, i', j] : j >= 1 and j <= NZ and i' >= 1 and i' >= -1 + i and t >= 1 and t <= NBRE_IT and i <= NR and N_EX_HZ <= 0 and i' <= i; S1[t, i, j] -> S9[-1 + t, i, j', -1 + N_EX_ER] : t >= 2 and i >= 1 and i <= NR and j' >= j and t <= NBRE_IT and j' <= NZ and j >= 1 and j' <= 1 + j and N_EX_ER >= 1; S0[t, i, j] -> S10[-1 + t, i, j', -1 + N_EX_EQ] : t >= 2 and i >= 1 and i <= NR and j' >= j and t <= NBRE_IT and j' <= NZ and j >= 1 and j' <= 1 + j and N_EX_EQ >= 1; S2[t, i, j] -> S7[-1 + t, i', j] : j >= 1 and j <= NZ and t >= 2 and i' >= i and t <= NBRE_IT and i' <= NR and i' <= 1 + i and N_EX_EQ <= 0 and i >= 1; S0[t, i, j] -> S0[-1 + t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_HR <= 0 and t <= NBRE_IT and t >= 2; S2[t, i, j] -> S10[-1 + t, i', j, -1 + N_EX_EQ] : j >= 1 and j <= NZ and t >= 2 and i' >= i and t <= NBRE_IT and i' <= NR and i' <= 1 + i and N_EX_EQ >= 1 and i >= 1; S6[t, i, j] -> S6[-1 + t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_ER <= 0 and t <= NBRE_IT and t >= 2; S1[t, i, j] -> S4[-1 + t, i, j, -1 + N_EX_HQ] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_HQ >= 1 and t <= NBRE_IT and t >= 2; S10[t, i, j, 0] -> S7[t, i, j] : t <= NBRE_IT and N_EX_EQ >= 1 and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1; S2[t, i, j] -> S6[-1 + t, i, j] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_ER <= 0 and t <= NBRE_IT and t >= 2; S8[t, i, j] -> S11[-1 + t, i, j, -1 + N_EX_EZ] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_EZ >= 1 and t <= NBRE_IT and t >= 2; S4[t, i, j, n_ex] -> S4[t, i, j, -1 + n_ex] : n_ex >= 1 and n_ex <= -1 + N_EX_HQ and j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT }";
		String valueBasedPlutoSchedule = "[NBRE_IT, NR, NZ, N_EX_HR, N_EX_HQ, N_EX_HZ, N_EX_ER, N_EX_EQ, N_EX_EZ] -> { S5[t, i, j, n_ex] -> [t, t + i, t + i + j, 1, 1, -N_EX_HZ + n_ex, 0]; S7[t, i, j] -> [t, t + i, t + i + j, 1, 1, 0, 1]; S11[t, i, j, n_ex] -> [t, t + i, t + i + j, 1, 1, n_ex, 1]; S9[t, i, j, n_ex] -> [t, t + i, t + i + j, 1, 1, n_ex, 4]; S10[t, i, j, n_ex] -> [t, t + i, t + i + j, 1, 1, n_ex, 2]; S2[t, i, j] -> [t, t + i, t + i + j, 0, 0, 0, 0]; S4[t, i, j, n_ex] -> [t, t + i, t + i + j, 1, 0, n_ex, 1]; S3[t, i, j, n_ex] -> [t, t + i, t + i + j, 1, 0, n_ex, 0]; S1[t, i, j] -> [t, t + i, t + i + j, 1, 0, 0, 0]; S6[t, i, j] -> [t, t + i, t + i + j, 1, 1, 0, 3]; S0[t, i, j] -> [t, t + i, t + i + j, 0, 0, 0, 0]; S8[t, i, j] -> [t, t + i, t + i + j, 1, 1, 0, 0] }";
		String valueBasedFeautrierSchedule = "[NBRE_IT, NR, NZ, N_EX_HR, N_EX_HQ, N_EX_HZ, N_EX_ER, N_EX_EQ, N_EX_EZ] -> { S0[t, i, j] -> [t, 0, i, j, 0]; S9[t, i, j, n_ex] -> [t, 3, n_ex, j, i]; S2[t, i, j] -> [t, 0, i, j, 0]; S3[t, i, j, n_ex] -> [t, 1, n_ex, j, i]; S8[t, i, j] -> [t, 2, i, j, 0]; S11[t, i, j, n_ex] -> [t, 3, n_ex, j, i]; S1[t, i, j] -> [t, 0, i, j, 0]; S4[t, i, j, n_ex] -> [t, 1, n_ex, j, i]; S7[t, i, j] -> [t, 2, i, j, 0]; S6[t, i, j] -> [t, 2, i, j, 0]; S10[t, i, j, n_ex] -> [t, 3, n_ex, j, i]; S5[t, i, j, n_ex] -> [t, 1, n_ex, j, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[NBRE_IT, NR, NZ, N_EX_HR, N_EX_HQ, N_EX_HZ, N_EX_ER, N_EX_EQ, N_EX_EZ] -> { S0[t, i, j] -> S8[-1 + t, i, j] : t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S1[t, i, j] -> S6[-1 + t, i, j'] : t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j' <= 1 + j and j' <= NZ and j >= 1 and j' >= j; S1[t, i, j] -> S8[-1 + t, i', j] : t >= 2 and t <= NBRE_IT and i' <= 1 + i and i' <= NR and j >= 1 and j <= NZ and i >= 1 and i' >= i; S8[t, i, j] -> S3[t, i, j, -1 + N_EX_HR] : j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT and N_EX_HR >= 1; S3[t, i, j, n_ex] -> S3[t, i, j, -1 + n_ex] : t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ and n_ex >= 1 and n_ex <= -1 + N_EX_HR; S8[t, i, j] -> S0[t, i, j] : t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S6[t, i, j] -> S1[t, i, j'] : t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j' >= 1 and j' <= j and j' >= -1 + j and j <= NZ; S4[t, i, j, 0] -> S1[t, i, j] : N_EX_HQ >= 1 and t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S11[t, i, j, n_ex] -> S11[t, i, j, -1 + n_ex] : t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ and n_ex >= 1 and n_ex <= -1 + N_EX_EZ; S8[t, i, j] -> S4[t, i', j, -1 + N_EX_HQ] : j >= 1 and j <= NZ and i' >= 1 and i' >= -1 + i and t >= 1 and t <= NBRE_IT and i <= NR and N_EX_HQ >= 1 and i' <= i; S2[t, i, j] -> S9[-1 + t, i, j, -1 + N_EX_ER] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_ER >= 1 and t <= NBRE_IT and t >= 2; S10[t, i, j, n_ex] -> S10[t, i, j, -1 + n_ex] : t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ and n_ex >= 1 and n_ex <= -1 + N_EX_EQ; S6[t, i, j] -> S5[t, i, j, -1 + N_EX_HZ] : j >= 1 and j <= NZ and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT and N_EX_HZ >= 1; S1[t, i, j] -> S1[-1 + t, i, j] : N_EX_HQ <= 0 and t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S1[t, i, j] -> S11[-1 + t, i', j, -1 + N_EX_EZ] : j >= 1 and j <= NZ and t >= 2 and i' >= i and t <= NBRE_IT and i' <= NR and i' <= 1 + i and N_EX_EZ >= 1 and i >= 1; S7[t, i, j] -> S0[t, i, j'] : t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j' >= 1 and j' <= j and j' >= -1 + j and j <= NZ; S9[t, i, j, n_ex] -> S9[t, i, j, -1 + n_ex] : t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ and n_ex >= 1 and n_ex <= -1 + N_EX_ER; S2[t, i, j] -> S5[-1 + t, i, j, -1 + N_EX_HZ] : N_EX_HZ >= 1 and t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S8[t, i, j] -> S1[t, i', j] : t >= 1 and t <= NBRE_IT and i' >= 1 and j >= 1 and j <= NZ and i' <= i and i' >= -1 + i and i <= NR; S6[t, i, j] -> S4[t, i, j', -1 + N_EX_HQ] : j' >= 1 and j' >= -1 + j and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT and j' <= j and j <= NZ and N_EX_HQ >= 1; S8[t, i, j] -> S8[-1 + t, i, j] : N_EX_EZ <= 0 and t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S0[t, i, j] -> S3[-1 + t, i, j, -1 + N_EX_HR] : N_EX_HR >= 1 and t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S2[t, i, j] -> S2[-1 + t, i, j] : N_EX_HZ <= 0 and t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S0[t, i, j] -> S11[-1 + t, i, j, -1 + N_EX_EZ] : j >= 1 and j <= NZ and i >= 1 and i <= NR and N_EX_EZ >= 1 and t <= NBRE_IT and t >= 2; S5[t, i, j, n_ex] -> S5[t, i, j, -1 + n_ex] : t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ and n_ex >= 1 and n_ex <= -1 + N_EX_HZ; S6[t, i, j] -> S9[-1 + t, i, j, -1 + N_EX_ER] : N_EX_ER >= 1 and t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S3[t, i, j, 0] -> S0[t, i, j] : N_EX_HR >= 1 and t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S7[t, i, j] -> S3[t, i, j', -1 + N_EX_HR] : j' >= 1 and j' >= -1 + j and i >= 1 and i <= NR and t >= 1 and t <= NBRE_IT and j' <= j and j <= NZ and N_EX_HR >= 1; S7[t, i, j] -> S7[-1 + t, i, j] : N_EX_EQ <= 0 and t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S7[t, i, j] -> S5[t, i', j, -1 + N_EX_HZ] : j >= 1 and j <= NZ and i' >= 1 and i' >= -1 + i and t >= 1 and t <= NBRE_IT and i <= NR and N_EX_HZ >= 1 and i' <= i; S11[t, i, j, 0] -> S8[t, i, j] : N_EX_EZ >= 1 and t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S7[t, i, j] -> S10[-1 + t, i, j, -1 + N_EX_EQ] : N_EX_EQ >= 1 and t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S9[t, i, j, 0] -> S6[t, i, j] : N_EX_ER >= 1 and t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S0[t, i, j] -> S7[-1 + t, i, j'] : t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j' <= 1 + j and j' <= NZ and j >= 1 and j' >= j; S6[t, i, j] -> S2[t, i, j] : t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S5[t, i, j, 0] -> S2[t, i, j] : N_EX_HZ >= 1 and t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S7[t, i, j] -> S2[t, i', j] : t >= 1 and t <= NBRE_IT and i' >= 1 and j >= 1 and j <= NZ and i' <= i and i' >= -1 + i and i <= NR; S1[t, i, j] -> S9[-1 + t, i, j', -1 + N_EX_ER] : t >= 2 and i >= 1 and i <= NR and j' >= j and t <= NBRE_IT and j' <= NZ and j >= 1 and j' <= 1 + j and N_EX_ER >= 1; S0[t, i, j] -> S10[-1 + t, i, j', -1 + N_EX_EQ] : t >= 2 and i >= 1 and i <= NR and j' >= j and t <= NBRE_IT and j' <= NZ and j >= 1 and j' <= 1 + j and N_EX_EQ >= 1; S2[t, i, j] -> S7[-1 + t, i', j] : t >= 2 and t <= NBRE_IT and i' <= 1 + i and i' <= NR and j >= 1 and j <= NZ and i >= 1 and i' >= i; S0[t, i, j] -> S0[-1 + t, i, j] : N_EX_HR <= 0 and t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S2[t, i, j] -> S10[-1 + t, i', j, -1 + N_EX_EQ] : j >= 1 and j <= NZ and t >= 2 and i' >= i and t <= NBRE_IT and i' <= NR and i' <= 1 + i and N_EX_EQ >= 1 and i >= 1; S6[t, i, j] -> S6[-1 + t, i, j] : N_EX_ER <= 0 and t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S10[t, i, j, 0] -> S7[t, i, j] : N_EX_EQ >= 1 and t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S1[t, i, j] -> S4[-1 + t, i, j, -1 + N_EX_HQ] : N_EX_HQ >= 1 and t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S2[t, i, j] -> S6[-1 + t, i, j] : t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ; S4[t, i, j, n_ex] -> S4[t, i, j, -1 + n_ex] : t >= 1 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ and n_ex >= 1 and n_ex <= -1 + N_EX_HQ; S8[t, i, j] -> S11[-1 + t, i, j, -1 + N_EX_EZ] : N_EX_EZ >= 1 and t >= 2 and t <= NBRE_IT and i >= 1 and i <= NR and j >= 1 and j <= NZ }";
		String memoryBasedPlutoSchedule = "[NBRE_IT, NR, NZ, N_EX_HR, N_EX_HQ, N_EX_HZ, N_EX_ER, N_EX_EQ, N_EX_EZ] -> { S3[t, i, j, n_ex] -> [t, t + i, t + i + j, 1, -N_EX_HR + n_ex, 8]; S5[t, i, j, n_ex] -> [t, t + i, t + i + j, 1, -N_EX_HZ + n_ex, 2]; S4[t, i, j, n_ex] -> [t, t + i, t + i + j, 1, -N_EX_HQ + n_ex, 7]; S11[t, i, j, n_ex] -> [t, t + i, t + i + j, 1, n_ex, 1]; S1[t, i, j] -> [t, t + i, t + i + j, 0, 0, 0]; S2[t, i, j] -> [t, t + i, t + i + j, 0, 0, 0]; S6[t, i, j] -> [t, t + i, t + i + j, 1, 0, 5]; S9[t, i, j, n_ex] -> [t, t + i, t + i + j, 1, n_ex, 6]; S7[t, i, j] -> [t, t + i, t + i + j, 1, 0, 3]; S0[t, i, j] -> [t, t + i, t + i + j, 0, 0, 0]; S10[t, i, j, n_ex] -> [t, t + i, t + i + j, 1, n_ex, 4]; S8[t, i, j] -> [t, t + i, t + i + j, 1, 0, 0] }";
		String memoryBasedFeautrierSchedule = "[NBRE_IT, NR, NZ, N_EX_HR, N_EX_HQ, N_EX_HZ, N_EX_ER, N_EX_EQ, N_EX_EZ] -> { S0[t, i, j] -> [t, 0, i, j, 0]; S9[t, i, j, n_ex] -> [t, 3, n_ex, j, i]; S2[t, i, j] -> [t, 0, i, j, 0]; S3[t, i, j, n_ex] -> [t, 1, n_ex, j, i]; S8[t, i, j] -> [t, 2, i, j, 0]; S11[t, i, j, n_ex] -> [t, 3, n_ex, j, i]; S1[t, i, j] -> [t, 0, i, j, 0]; S4[t, i, j, n_ex] -> [t, 1, n_ex, j, i]; S7[t, i, j] -> [t, 2, i, j, 0]; S6[t, i, j] -> [t, 2, i, j, 0]; S10[t, i, j, n_ex] -> [t, 3, n_ex, j, i]; S5[t, i, j, n_ex] -> [t, 1, n_ex, j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #118
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_dynprog_scop_new
	 *   block : 
	 *     S0 (depth = 0) [out_l=IntExpr(0)]
	 *     ([out_l]) = f([])
	 *     (Domain = [tsteps,length] -> {S0[] : })
	 *     for : 0 <= iter <= tsteps-1 (stride = 1)
	 *       block : 
	 *         for : 0 <= i <= length-1 (stride = 1)
	 *           for : 0 <= j <= length-1 (stride = 1)
	 *             S1 (depth = 3) [c[i][j]=IntExpr(0)]
	 *             ([c[i][j]]) = f([])
	 *             (Domain = [tsteps,length] -> {S1[iter,i,j] : (j >= 0) and (-j+length-1 >= 0) and (i >= 0) and (-i+length-1 >= 0) and (iter >= 0) and (-iter+tsteps-1 >= 0)})
	 *         for : 0 <= i <= length-2 (stride = 1)
	 *           for : i+1 <= j <= length-1 (stride = 1)
	 *             block : 
	 *               S2 (depth = 3) [sum_c[i][j][i]=IntExpr(0)]
	 *               ([sum_c[i][j][i]]) = f([])
	 *               (Domain = [tsteps,length] -> {S2[iter,i,j] : (-i+j-1 >= 0) and (-j+length-1 >= 0) and (i >= 0) and (-i+length-2 >= 0) and (iter >= 0) and (-iter+tsteps-1 >= 0)})
	 *               for : i+1 <= k <= j-1 (stride = 1)
	 *                 S3 (depth = 4) [sum_c[i][j][k]=add(add(sum_c[i][j][k-1],c[i][k]),c[k][j])]
	 *                 ([sum_c[i][j][k]]) = f([sum_c[i][j][k-1], c[i][k], c[k][j]])
	 *                 (Domain = [tsteps,length] -> {S3[iter,i,j,k] : (-i+k-1 >= 0) and (j-k-1 >= 0) and (-i+j-1 >= 0) and (-j+length-1 >= 0) and (i >= 0) and (-i+length-2 >= 0) and (iter >= 0) and (-iter+tsteps-1 >= 0)})
	 *               S4 (depth = 3) [c[i][j]=add(sum_c[i][j][j-1],W[i][j])]
	 *               ([c[i][j]]) = f([sum_c[i][j][j-1], W[i][j]])
	 *               (Domain = [tsteps,length] -> {S4[iter,i,j] : (-i+j-1 >= 0) and (-j+length-1 >= 0) and (i >= 0) and (-i+length-2 >= 0) and (iter >= 0) and (-iter+tsteps-1 >= 0)})
	 *         S5 (depth = 1) [out_l=add(out_l,c[0][length-1])]
	 *         ([out_l]) = f([out_l, c[0][length-1]])
	 *         (Domain = [tsteps,length] -> {S5[iter] : (iter >= 0) and (-iter+tsteps-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_solvers_dynprog_dynprog_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_linear_algebra_solvers_dynprog_dynprog_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[tsteps, length] -> { S3[iter, i, j, k] : k >= 1 + i and k <= -1 + j and j >= 1 + i and j <= -1 + length and i >= 0 and i <= -2 + length and iter >= 0 and iter <= -1 + tsteps; S2[iter, i, j] : j >= 1 + i and j <= -1 + length and i >= 0 and i <= -2 + length and iter >= 0 and iter <= -1 + tsteps; S4[iter, i, j] : j >= 1 + i and j <= -1 + length and i >= 0 and i <= -2 + length and iter >= 0 and iter <= -1 + tsteps; S1[iter, i, j] : j >= 0 and j <= -1 + length and i >= 0 and i <= -1 + length and iter >= 0 and iter <= -1 + tsteps; S0[]; S5[iter] : iter >= 0 and iter <= -1 + tsteps }";
		String idSchedules = "[tsteps, length] -> { S1[iter, i, j] -> [1, iter, 0, i, 0, j, 0, 0, 0]; S3[iter, i, j, k] -> [1, iter, 1, i, 0, j, 1, k, 0]; S4[iter, i, j] -> [1, iter, 1, i, 0, j, 2, 0, 0]; S2[iter, i, j] -> [1, iter, 1, i, 0, j, 0, 0, 0]; S0[] -> [0, 0, 0, 0, 0, 0, 0, 0, 0]; S5[iter] -> [1, iter, 2, 0, 0, 0, 0, 0, 0] }";
		String writes = "[tsteps, length] -> { S5[iter] -> out_l[]; S2[iter, i, j] -> sum_c[i, j, i]; S1[iter, i, j] -> c[i, j]; S4[iter, i, j] -> c[i, j]; S3[iter, i, j, k] -> sum_c[i, j, k]; S0[] -> out_l[] }";
		String reads = "[tsteps, length] -> { S4[iter, i, j] -> sum_c[i, j, -1 + j]; S5[iter] -> out_l[]; S4[iter, i, j] -> W[i, j]; S3[iter, i, j, k] -> c[k, j]; S3[iter, i, j, k] -> c[i, k]; S5[iter] -> c[0, -1 + length]; S3[iter, i, j, k] -> sum_c[i, j, -1 + k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[tsteps, length] -> { S5[iter] -> S5[-1 + iter] : iter >= 1 and iter <= -1 + tsteps; S3[iter, i, j, k] -> S1[iter, k, j] : k >= 1 + i and k <= -1 + j and iter >= 0 and j <= -1 + length and i >= 0 and iter <= -1 + tsteps and i <= -2 + length and j >= 1 + i; S5[iter] -> S1[iter, 0, 0] : length = 1 and iter >= 0 and iter <= -1 + tsteps; S5[0] -> S0[] : tsteps >= 1; S3[iter, i, j, k] -> S3[iter, i, j, -1 + k] : iter >= 0 and k <= -1 + j and iter <= -1 + tsteps and j <= -1 + length and i >= 0 and k >= 2 + i and i <= -2 + length and j >= 1 + i; S3[iter, i, j, k] -> S4[iter, i, k] : k >= 1 + i and k <= -1 + j and iter >= 0 and j <= -1 + length and i >= 0 and iter <= -1 + tsteps and i <= -2 + length and j >= 1 + i; S3[iter, i, j, 1 + i] -> S2[iter, i, j] : iter >= 0 and j >= 2 + i and iter <= -1 + tsteps and j <= -1 + length and i >= 0 and i <= -2 + length; S4[iter, i, 1 + i] -> S2[iter, i, 1 + i] : iter >= 0 and i <= -2 + length and i >= 0 and iter <= -1 + tsteps; S4[iter, i, j] -> S3[iter, i, j, -1 + j] : iter <= -1 + tsteps and j <= -1 + length and i >= 0 and j >= 2 + i and iter >= 0 and i <= -2 + length; S5[iter] -> S4[iter, 0, -1 + length] : iter >= 0 and iter <= -1 + tsteps and length >= 2 }";
		String valueBasedPlutoSchedule = "[tsteps, length] -> { S0[] -> [0, 0, length, length, 0]; S5[iter] -> [iter, iter, length, length, 1]; S1[iter, i, j] -> [0, iter, i, -length + j, 2]; S2[iter, i, j] -> [0, iter, i, -length + j, 4]; S4[iter, i, j] -> [iter, iter + i, j, j, 3]; S3[iter, i, j, k] -> [iter, iter + i, j, k, 5] }";
		String valueBasedFeautrierSchedule = "[tsteps, length] -> { S5[iter] -> [1 + 2length + iter, 0, 0, 0]; S2[iter, i, j] -> [0, iter, i, j]; S3[iter, i, j, k] -> [j + k, i, iter, k]; S4[iter, i, j] -> [2j, i, iter, 0]; S1[iter, i, j] -> [0, iter, i, j]; S0[] -> [2length, 0, 0, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[tsteps, length] -> { S2[iter, i, j] -> S2[-1 + iter, i, j] : j >= 1 + i and j <= -1 + length and i >= 0 and iter <= -1 + tsteps and iter >= 1; S5[0] -> S0[] : tsteps >= 1; S3[iter, i, j, k] -> S3[-1 + iter, i, j, k'] : iter >= 1 and iter <= -1 + tsteps and j <= -1 + length and i >= 0 and k' <= 1 + k and k >= 1 + i and k' >= k and k' <= -1 + j; S3[iter, i, j, k] -> S3[iter, i, j, -1 + k] : iter >= 0 and iter <= -1 + tsteps and i >= 0 and j <= -1 + length and k >= 2 + i and k <= -1 + j; S1[iter, i, j] -> S3[-1 + iter, i, j', j] : iter >= 1 and iter <= -1 + tsteps and i >= 0 and j' <= -1 + length and j >= 1 + i and j' >= 1 + j and j >= 0 and j <= -1 + length and i <= -1 + length; S3[iter, i, j, k] -> S1[iter, k, j] : k >= 1 + i and k <= -1 + j and iter >= 0 and j <= -1 + length and i >= 0 and iter <= -1 + tsteps and i <= -2 + length and j >= 1 + i; S3[iter, i, j, 1 + i] -> S2[iter, i, j] : iter >= 0 and j >= 2 + i and iter <= -1 + tsteps and j <= -1 + length and i >= 0 and i <= -2 + length; S3[iter, i, j, k] -> S4[iter, i, k] : iter >= 0 and iter <= -1 + tsteps and i >= 0 and k >= 1 + i and k <= -1 + j and j <= -1 + length; S3[iter, i, j, -1 + j] -> S4[-1 + iter, i, j] : iter >= 1 and iter <= -1 + tsteps and i >= 0 and j >= 2 + i and j <= -1 + length; S4[iter, i, 1 + i] -> S2[iter, i, 1 + i] : iter >= 0 and i <= -2 + length and i >= 0 and iter <= -1 + tsteps; S5[iter] -> S5[-1 + iter] : iter >= 1 and iter <= -1 + tsteps; S1[iter, i, j] -> S1[-1 + iter, i, j] : length >= 1 and iter >= 1 and iter <= -1 + tsteps and i <= -1 + length and j >= 0 and j <= i; S5[iter] -> S1[iter, 0, 0] : length = 1 and iter >= 0 and iter <= -1 + tsteps; S4[iter, i, j] -> S1[iter, i, j] : iter >= 0 and iter <= -1 + tsteps and i >= 0 and j >= 1 + i and j <= -1 + length; S1[iter, i, j] -> S4[-1 + iter, i, j] : iter >= 1 and iter <= -1 + tsteps and i >= 0 and j >= 1 + i and j <= -1 + length; S4[iter, i, j] -> S3[iter, i', j, i] : iter >= 0 and iter <= -1 + tsteps and i' >= 0 and j <= -1 + length and i' <= -1 + i and j >= 1 + i; S4[iter, i, j] -> S3[iter, i, j, -1 + j] : iter >= 0 and iter <= -1 + tsteps and i >= 0 and j >= 2 + i and j <= -1 + length; S5[iter] -> S4[iter, 0, -1 + length] : iter >= 0 and iter <= -1 + tsteps and length >= 2; S2[iter, i, j] -> S3[-1 + iter, i, j, 1 + i] : iter >= 1 and iter <= -1 + tsteps and i >= 0 and j >= 2 + i and j <= -1 + length and i <= -2 + length; S2[iter, i, 1 + i] -> S4[-1 + iter, i, 1 + i] : iter >= 1 and iter <= -1 + tsteps and i >= 0 and i <= -2 + length; S1[iter, 0, -1 + length] -> S5[-1 + iter] : length >= 1 and iter >= 1 and iter <= -1 + tsteps }";
		String memoryBasedPlutoSchedule = "[tsteps, length] -> { S0[] -> [0, 0, length, length, 0]; S5[iter] -> [iter, 0, length, length, 1]; S1[iter, i, j] -> [iter, 0, i, -length + j, 2]; S2[iter, i, j] -> [iter, 0, i, -length + j, 4]; S4[iter, i, j] -> [iter, i, j, j, 3]; S3[iter, i, j, k] -> [iter, i, j, k, 5] }";
		String memoryBasedFeautrierSchedule = "[tsteps, length] -> { S0[] -> [0, 0, 0, 0, 0]; S5[iter] -> [iter, 2, 0, 0, 0]; S1[iter, i, j] -> [iter, 0, i, j, 0]; S2[iter, i, j] -> [iter, 0, i, j, 0]; S4[iter, i, j] -> [iter, 1, 2j, i, 0]; S3[iter, i, j, k] -> [iter, 1, j + k, i, k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #119
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : difference_predictors_difference_predictors/scop_0
	 *   block : 
	 *     for : 1 <= l <= loop (stride = 1)
	 *       for : 0 <= i <= n-1 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [ar=cx[i][4]]
	 *           ([ar]) = f([cx[i][4]])
	 *           (Domain = [loop,n] -> {S0[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S1 (depth = 2) [br=sub(ar,px[i][4])]
	 *           ([br]) = f([ar, px[i][4]])
	 *           (Domain = [loop,n] -> {S1[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S2 (depth = 2) [px[i][4]=ar]
	 *           ([px[i][4]]) = f([ar])
	 *           (Domain = [loop,n] -> {S2[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S3 (depth = 2) [cr=sub(br,px[i][5])]
	 *           ([cr]) = f([br, px[i][5]])
	 *           (Domain = [loop,n] -> {S3[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S4 (depth = 2) [px[i][5]=br]
	 *           ([px[i][5]]) = f([br])
	 *           (Domain = [loop,n] -> {S4[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S5 (depth = 2) [ar=sub(cr,px[i][6])]
	 *           ([ar]) = f([cr, px[i][6]])
	 *           (Domain = [loop,n] -> {S5[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S6 (depth = 2) [px[i][6]=cr]
	 *           ([px[i][6]]) = f([cr])
	 *           (Domain = [loop,n] -> {S6[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S7 (depth = 2) [br=sub(ar,px[i][7])]
	 *           ([br]) = f([ar, px[i][7]])
	 *           (Domain = [loop,n] -> {S7[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S8 (depth = 2) [px[i][7]=ar]
	 *           ([px[i][7]]) = f([ar])
	 *           (Domain = [loop,n] -> {S8[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S9 (depth = 2) [cr=sub(br,px[i][8])]
	 *           ([cr]) = f([br, px[i][8]])
	 *           (Domain = [loop,n] -> {S9[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S10 (depth = 2) [px[i][8]=br]
	 *           ([px[i][8]]) = f([br])
	 *           (Domain = [loop,n] -> {S10[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S11 (depth = 2) [ar=sub(cr,px[i][9])]
	 *           ([ar]) = f([cr, px[i][9]])
	 *           (Domain = [loop,n] -> {S11[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S12 (depth = 2) [px[i][9]=cr]
	 *           ([px[i][9]]) = f([cr])
	 *           (Domain = [loop,n] -> {S12[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S13 (depth = 2) [br=sub(ar,px[i][10])]
	 *           ([br]) = f([ar, px[i][10]])
	 *           (Domain = [loop,n] -> {S13[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S14 (depth = 2) [px[i][10]=ar]
	 *           ([px[i][10]]) = f([ar])
	 *           (Domain = [loop,n] -> {S14[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S15 (depth = 2) [cr=sub(br,px[i][11])]
	 *           ([cr]) = f([br, px[i][11]])
	 *           (Domain = [loop,n] -> {S15[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S16 (depth = 2) [px[i][11]=br]
	 *           ([px[i][11]]) = f([br])
	 *           (Domain = [loop,n] -> {S16[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S17 (depth = 2) [px[i][13]=sub(cr,px[i][12])]
	 *           ([px[i][13]]) = f([cr, px[i][12]])
	 *           (Domain = [loop,n] -> {S17[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *           S18 (depth = 2) [px[i][12]=cr]
	 *           ([px[i][12]]) = f([cr])
	 *           (Domain = [loop,n] -> {S18[l,i] : (i >= 0) and (-i+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_10_difference_predictors_GScopRoot_difference_predictors_scop_0_ () {
		String path = "polymodel_src_livermore_loops_10_difference_predictors_GScopRoot_difference_predictors_scop_0_";
		// SCoP Extraction Data
		String domains = "[loop, n] -> { S10[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S2[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S0[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S3[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S4[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S9[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S12[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S14[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S11[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S16[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S7[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S5[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S6[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S17[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S15[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S18[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S1[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S13[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S8[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop }";
		String idSchedules = "[loop, n] -> { S7[l, i] -> [0, l, 0, i, 7]; S16[l, i] -> [0, l, 0, i, 16]; S3[l, i] -> [0, l, 0, i, 3]; S5[l, i] -> [0, l, 0, i, 5]; S18[l, i] -> [0, l, 0, i, 18]; S9[l, i] -> [0, l, 0, i, 9]; S11[l, i] -> [0, l, 0, i, 11]; S12[l, i] -> [0, l, 0, i, 12]; S1[l, i] -> [0, l, 0, i, 1]; S4[l, i] -> [0, l, 0, i, 4]; S6[l, i] -> [0, l, 0, i, 6]; S13[l, i] -> [0, l, 0, i, 13]; S2[l, i] -> [0, l, 0, i, 2]; S15[l, i] -> [0, l, 0, i, 15]; S8[l, i] -> [0, l, 0, i, 8]; S17[l, i] -> [0, l, 0, i, 17]; S14[l, i] -> [0, l, 0, i, 14]; S10[l, i] -> [0, l, 0, i, 10]; S0[l, i] -> [0, l, 0, i, 0] }";
		String writes = "[loop, n] -> { S0[l, i] -> ar[]; S16[l, i] -> px[i, 11]; S5[l, i] -> ar[]; S6[l, i] -> px[i, 6]; S13[l, i] -> br[]; S10[l, i] -> px[i, 8]; S1[l, i] -> br[]; S4[l, i] -> px[i, 5]; S15[l, i] -> cr[]; S8[l, i] -> px[i, 7]; S3[l, i] -> cr[]; S9[l, i] -> cr[]; S12[l, i] -> px[i, 9]; S17[l, i] -> px[i, 13]; S2[l, i] -> px[i, 4]; S7[l, i] -> br[]; S18[l, i] -> px[i, 12]; S14[l, i] -> px[i, 10]; S11[l, i] -> ar[] }";
		String reads = "[loop, n] -> { S15[l, i] -> br[]; S3[l, i] -> br[]; S12[l, i] -> cr[]; S17[l, i] -> cr[]; S11[l, i] -> cr[]; S3[l, i] -> px[i, 5]; S10[l, i] -> br[]; S1[l, i] -> px[i, 4]; S6[l, i] -> cr[]; S18[l, i] -> cr[]; S1[l, i] -> ar[]; S4[l, i] -> br[]; S5[l, i] -> px[i, 6]; S8[l, i] -> ar[]; S13[l, i] -> px[i, 10]; S17[l, i] -> px[i, 12]; S0[l, i] -> cx[i, 4]; S7[l, i] -> px[i, 7]; S2[l, i] -> ar[]; S15[l, i] -> px[i, 11]; S11[l, i] -> px[i, 9]; S9[l, i] -> br[]; S7[l, i] -> ar[]; S9[l, i] -> px[i, 8]; S16[l, i] -> br[]; S5[l, i] -> cr[]; S14[l, i] -> ar[]; S13[l, i] -> ar[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[loop, n] -> { S17[l, i] -> S15[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S12[l, i] -> S9[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S18[l, i] -> S15[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S5[l, i] -> S3[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S15[l, i] -> S13[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S3[l, i] -> S1[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S13[l, i] -> S11[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S9[l, i] -> S10[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S11[l, i] -> S12[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S13[l, i] -> S14[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S17[l, i] -> S18[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S9[l, i] -> S7[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S16[l, i] -> S13[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S14[l, i] -> S11[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S2[l, i] -> S0[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S15[l, i] -> S16[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S3[l, i] -> S4[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S6[l, i] -> S3[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S8[l, i] -> S5[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S1[l, i] -> S0[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S11[l, i] -> S9[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S7[l, i] -> S8[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S5[l, i] -> S6[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S1[l, i] -> S2[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S7[l, i] -> S5[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S10[l, i] -> S7[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S4[l, i] -> S1[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop }";
		String valueBasedPlutoSchedule = "[loop, n] -> { S18[l, i] -> [l, i, 10]; S9[l, i] -> [l, i, 6]; S14[l, i] -> [l, i, 14]; S12[l, i] -> [l, i, 15]; S16[l, i] -> [l, i, 13]; S17[l, i] -> [l, i, 11]; S2[l, i] -> [l, i, 17]; S6[l, i] -> [l, i, 12]; S10[l, i] -> [l, i, 18]; S3[l, i] -> [l, i, 2]; S0[l, i] -> [l, i, 0]; S15[l, i] -> [l, i, 9]; S5[l, i] -> [l, i, 3]; S1[l, i] -> [l, i, 1]; S13[l, i] -> [l, i, 8]; S8[l, i] -> [l, i, 4]; S4[l, i] -> [l, i, 16]; S11[l, i] -> [l, i, 7]; S7[l, i] -> [l, i, 5] }";
		String valueBasedFeautrierSchedule = "[loop, n] -> { S3[l, i] -> [l, i]; S13[l, i] -> [l, i]; S2[l, i] -> [l, i]; S11[l, i] -> [l, i]; S15[l, i] -> [l, i]; S12[l, i] -> [l, i]; S17[l, i] -> [l, i]; S6[l, i] -> [l, i]; S1[l, i] -> [l, i]; S10[l, i] -> [l, i]; S9[l, i] -> [l, i]; S8[l, i] -> [l, i]; S16[l, i] -> [l, i]; S4[l, i] -> [l, i]; S0[l, i] -> [l, i]; S7[l, i] -> [l, i]; S18[l, i] -> [l, i]; S14[l, i] -> [l, i]; S5[l, i] -> [l, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[loop, n] -> { S6[l, i] -> S3[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S8[l, i] -> S5[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S16[l, i] -> S13[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S9[l, i] -> S6[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S9[l, i] -> S3[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S18[l, i] -> S17[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S5[l, i] -> S1[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S10[l, i] -> S9[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S0[l, i] -> S11[l, -1 + i] : i >= 1 and i <= -1 + n and l >= 1 and l <= loop; S0[l, 0] -> S11[-1 + l, -1 + n] : l <= loop and n >= 1 and l >= 2; S1[l, i] -> S13[l, -1 + i] : i >= 1 and i <= -1 + n and l >= 1 and l <= loop; S1[l, 0] -> S13[-1 + l, -1 + n] : l <= loop and n >= 1 and l >= 2; S1[l, i] -> S0[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S5[l, i] -> S6[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S1[l, i] -> S15[l, -1 + i] : l >= 1 and l <= loop and i >= 1 and i <= -1 + n; S1[l, 0] -> S15[-1 + l, -1 + n] : n >= 1 and l >= 2 and l <= loop; S18[l, i] -> S18[-1 + l, i] : l >= 2 and l <= loop and i >= 0 and i <= -1 + n; S11[l, i] -> S12[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S13[l, i] -> S11[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S6[l, i] -> S6[-1 + l, i] : l >= 2 and l <= loop and i >= 0 and i <= -1 + n; S4[l, i] -> S1[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S12[l, i] -> S9[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S9[l, i] -> S10[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S3[l, i] -> S1[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S2[l, i] -> S1[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S11[l, i] -> S8[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S13[l, i] -> S7[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S8[l, i] -> S7[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S7[l, i] -> S3[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S9[l, i] -> S5[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S9[l, i] -> S7[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S10[l, i] -> S7[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S14[l, i] -> S13[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S15[l, i] -> S12[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S11[l, i] -> S5[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S11[l, i] -> S9[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S13[l, i] -> S9[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S17[l, i] -> S17[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S16[l, i] -> S16[-1 + l, i] : l >= 2 and l <= loop and i >= 0 and i <= -1 + n; S0[l, i] -> S13[l, -1 + i] : l >= 1 and l <= loop and i >= 1 and i <= -1 + n; S0[l, 0] -> S13[-1 + l, -1 + n] : n >= 1 and l >= 2 and l <= loop; S4[l, i] -> S3[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S3[l, i] -> S18[l, -1 + i] : l >= 1 and l <= loop and i >= 1 and i <= -1 + n; S3[l, 0] -> S18[-1 + l, -1 + n] : n >= 1 and l >= 2 and l <= loop; S15[l, i] -> S11[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S7[l, i] -> S8[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S5[l, i] -> S3[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S15[l, i] -> S16[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S13[l, i] -> S14[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S10[l, i] -> S10[-1 + l, i] : l >= 2 and l <= loop and i >= 0 and i <= -1 + n; S5[l, i] -> S0[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S14[l, i] -> S14[-1 + l, i] : l >= 2 and l <= loop and i >= 0 and i <= -1 + n; S12[l, i] -> S11[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S3[l, i] -> S15[l, -1 + i] : i >= 1 and i <= -1 + n and l >= 1 and l <= loop; S3[l, 0] -> S15[-1 + l, -1 + n] : l <= loop and n >= 1 and l >= 2; S15[l, i] -> S9[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S18[l, i] -> S15[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S3[l, i] -> S17[l, -1 + i] : l >= 1 and l <= loop and i >= 1 and i <= -1 + n; S3[l, 0] -> S17[-1 + l, -1 + n] : n >= 1 and l >= 2 and l <= loop; S1[l, i] -> S2[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S6[l, i] -> S5[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S17[l, i] -> S18[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S14[l, i] -> S11[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S8[l, i] -> S8[-1 + l, i] : l >= 2 and l <= loop and i >= 0 and i <= -1 + n; S11[l, i] -> S7[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S7[l, i] -> S4[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S1[l, i] -> S16[l, -1 + i] : l >= 1 and l <= loop and i >= 1 and i <= -1 + n; S1[l, 0] -> S16[-1 + l, -1 + n] : n >= 1 and l >= 2 and l <= loop; S12[l, i] -> S12[-1 + l, i] : l >= 2 and l <= loop and i >= 0 and i <= -1 + n; S16[l, i] -> S15[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S13[l, i] -> S10[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S3[l, i] -> S4[-1 + l, i] : i >= 0 and i <= -1 + n and l >= 2 and l <= loop; S7[l, i] -> S5[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S15[l, i] -> S13[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S17[l, i] -> S15[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S5[l, i] -> S2[l, i] : l >= 1 and l <= loop and i >= 0 and i <= -1 + n; S7[l, i] -> S1[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S0[l, i] -> S14[l, -1 + i] : l >= 1 and l <= loop and i >= 1 and i <= -1 + n; S0[l, 0] -> S14[-1 + l, -1 + n] : n >= 1 and l >= 2 and l <= loop; S2[l, i] -> S0[l, i] : i >= 0 and i <= -1 + n and l >= 1 and l <= loop; S2[l, i] -> S2[-1 + l, i] : l >= 2 and l <= loop and i >= 0 and i <= -1 + n; S4[l, i] -> S4[-1 + l, i] : l >= 2 and l <= loop and i >= 0 and i <= -1 + n }";
		String memoryBasedPlutoSchedule = "[loop, n] -> { S18[l, i] -> [l, i, 16]; S9[l, i] -> [l, i, 9]; S14[l, i] -> [l, i, 18]; S12[l, i] -> [l, i, 13]; S16[l, i] -> [l, i, 17]; S17[l, i] -> [l, i, 15]; S2[l, i] -> [l, i, 3]; S6[l, i] -> [l, i, 8]; S10[l, i] -> [l, i, 11]; S3[l, i] -> [l, i, 2]; S0[l, i] -> [l, i, 0]; S15[l, i] -> [l, i, 14]; S5[l, i] -> [l, i, 4]; S1[l, i] -> [l, i, 1]; S13[l, i] -> [l, i, 12]; S8[l, i] -> [l, i, 7]; S4[l, i] -> [l, i, 5]; S11[l, i] -> [l, i, 10]; S7[l, i] -> [l, i, 6] }";
		String memoryBasedFeautrierSchedule = "[loop, n] -> { S18[l, i] -> [l, 1 + i, 1]; S9[l, i] -> [l, i, 5]; S14[l, i] -> [l, i, 8]; S12[l, i] -> [l, i, 7]; S16[l, i] -> [l, 1 + i, 0]; S17[l, i] -> [l, 1 + i, 0]; S2[l, i] -> [l, i, 2]; S6[l, i] -> [l, i, 4]; S10[l, i] -> [l, i, 6]; S3[l, i] -> [l, i, 2]; S0[l, i] -> [l, i, 0]; S15[l, i] -> [l, i, 8]; S5[l, i] -> [l, i, 3]; S1[l, i] -> [l, i, 1]; S13[l, i] -> [l, i, 7]; S8[l, i] -> [l, i, 5]; S4[l, i] -> [l, i, 3]; S11[l, i] -> [l, i, 6]; S7[l, i] -> [l, i, 4] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #120
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_durbin_scop_new
	 *   block : 
	 *     S0 (depth = 0) [y[0][0]=r[0]]
	 *     ([y[0][0]]) = f([r[0]])
	 *     (Domain = [n] -> {S0[] : })
	 *     S1 (depth = 0) [beta[0]=IntExpr(1)]
	 *     ([beta[0]]) = f([])
	 *     (Domain = [n] -> {S1[] : })
	 *     S2 (depth = 0) [alpha[0]=r[0]]
	 *     ([alpha[0]]) = f([r[0]])
	 *     (Domain = [n] -> {S2[] : })
	 *     for : 1 <= k <= n-1 (stride = 1)
	 *       block : 
	 *         S3 (depth = 1) [beta[k]=sub(beta[k-1],mul(mul(alpha[k-1],alpha[k-1]),beta[k-1]))]
	 *         ([beta[k]]) = f([beta[k-1], alpha[k-1], alpha[k-1], beta[k-1]])
	 *         (Domain = [n] -> {S3[k] : (k-1 >= 0) and (-k+n-1 >= 0)})
	 *         S4 (depth = 1) [sum[0][k]=r[k]]
	 *         ([sum[0][k]]) = f([r[k]])
	 *         (Domain = [n] -> {S4[k] : (k-1 >= 0) and (-k+n-1 >= 0)})
	 *         for : 0 <= i <= k-1 (stride = 1)
	 *           S5 (depth = 2) [sum[i+1][k]=add(sum[i][k],mul(r[k-i-1],y[i][k-1]))]
	 *           ([sum[i+1][k]]) = f([sum[i][k], r[k-i-1], y[i][k-1]])
	 *           (Domain = [n] -> {S5[k,i] : (i >= 0) and (k-i-1 >= 0) and (k-1 >= 0) and (-k+n-1 >= 0)})
	 *         S6 (depth = 1) [alpha[k]=mul(neg(sum[k][k]),beta[k])]
	 *         ([alpha[k]]) = f([sum[k][k], beta[k]])
	 *         (Domain = [n] -> {S6[k] : (k-1 >= 0) and (-k+n-1 >= 0)})
	 *         for : 0 <= i <= k-1 (stride = 1)
	 *           S7 (depth = 2) [y[i][k]=add(y[i][k-1],mul(alpha[k],y[k-i-1][k-1]))]
	 *           ([y[i][k]]) = f([y[i][k-1], alpha[k], y[k-i-1][k-1]])
	 *           (Domain = [n] -> {S7[k,i] : (i >= 0) and (k-i-1 >= 0) and (k-1 >= 0) and (-k+n-1 >= 0)})
	 *         S8 (depth = 1) [y[k][k]=alpha[k]]
	 *         ([y[k][k]]) = f([alpha[k]])
	 *         (Domain = [n] -> {S8[k] : (k-1 >= 0) and (-k+n-1 >= 0)})
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       S9 (depth = 1) [out[i]=y[i][n-1]]
	 *       ([out[i]]) = f([y[i][n-1]])
	 *       (Domain = [n] -> {S9[i] : (i >= 0) and (-i+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_solvers_durbin_durbin_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_linear_algebra_solvers_durbin_durbin_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[n] -> { S0[]; S1[]; S2[]; S8[k] : k >= 1 and k <= -1 + n; S9[i] : i >= 0 and i <= -1 + n; S4[k] : k >= 1 and k <= -1 + n; S7[k, i] : i >= 0 and i <= -1 + k and k >= 1 and k <= -1 + n; S6[k] : k >= 1 and k <= -1 + n; S3[k] : k >= 1 and k <= -1 + n; S5[k, i] : i >= 0 and i <= -1 + k and k >= 1 and k <= -1 + n }";
		String idSchedules = "[n] -> { S5[k, i] -> [3, k, 2, i, 0]; S0[] -> [0, 0, 0, 0, 0]; S7[k, i] -> [3, k, 4, i, 0]; S6[k] -> [3, k, 3, 0, 0]; S3[k] -> [3, k, 0, 0, 0]; S4[k] -> [3, k, 1, 0, 0]; S1[] -> [1, 0, 0, 0, 0]; S2[] -> [2, 0, 0, 0, 0]; S9[i] -> [4, i, 0, 0, 0]; S8[k] -> [3, k, 5, 0, 0] }";
		String writes = "[n] -> { S0[] -> y[0, 0]; S1[] -> beta[0]; S3[k] -> beta[k]; S9[i] -> out[i]; S8[k] -> y[k, k]; S2[] -> alpha[0]; S5[k, i] -> sum[1 + i, k]; S6[k] -> alpha[k]; S7[k, i] -> y[i, k]; S4[k] -> sum[0, k] }";
		String reads = "[n] -> { S0[] -> r[0]; S6[k] -> beta[k]; S9[i] -> y[i, -1 + n]; S5[k, i] -> r[-1 + k - i]; S3[k] -> beta[-1 + k]; S6[k] -> sum[k, k]; S7[k, i] -> alpha[k]; S8[k] -> alpha[k]; S3[k] -> alpha[-1 + k]; S5[k, i] -> y[i, -1 + k]; S2[] -> r[0]; S5[k, i] -> sum[i, k]; S7[k, i] -> y[i, -1 + k]; S7[k, i] -> y[-1 + k - i, -1 + k]; S4[k] -> r[k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S9[0] -> S0[] : n = 1; S7[k, i] -> S7[-1 + k, i] : (k <= -1 + n and i >= 0 and 2i <= -2 + k) or (k <= -1 + n and 2i >= k and i <= -2 + k); S7[k, i] -> S7[-1 + k, -1 + k - i] : k <= -1 + n and i <= -1 + k and i >= 1; S5[1, 0] -> S0[] : n >= 2; S3[1] -> S2[] : n >= 2; S6[k] -> S5[k, -1 + k] : k >= 1 and k <= -1 + n; S9[-1 + n] -> S8[-1 + n] : n >= 2; S9[i] -> S7[-1 + n, i] : i >= 0 and i <= -2 + n; S5[k, i] -> S7[-1 + k, i] : i >= 0 and k <= -1 + n and i <= -2 + k and k >= 1; S3[k] -> S6[-1 + k] : k >= 2 and k <= -1 + n; S3[k] -> S3[-1 + k] : k >= 2 and k <= -1 + n; S7[k, 0] -> S8[-1 + k] : k >= 2 and k <= -1 + n; S7[k, -1 + k] -> S8[-1 + k] : k >= 2 and k <= -1 + n; S8[k] -> S6[k] : k >= 1 and k <= -1 + n; S6[k] -> S3[k] : k >= 1 and k <= -1 + n; S5[k, 0] -> S4[k] : k <= -1 + n and k >= 1; S5[k, -1 + k] -> S8[-1 + k] : k >= 2 and k <= -1 + n; S7[k, i] -> S6[k] : i >= 0 and i <= -1 + k and k >= 1 and k <= -1 + n; S3[1] -> S1[] : n >= 2; S5[k, i] -> S5[k, -1 + i] : k <= -1 + n and i <= -1 + k and i >= 1 and k >= 1; S7[1, 0] -> S0[] : n >= 2 }";
		String valueBasedPlutoSchedule = "[n] -> { S1[] -> [0, 0, 0]; S0[] -> [0, 0, 0]; S4[k] -> [0, k, 0]; S2[] -> [0, 0, 0]; S8[k] -> [k, 0, 4]; S7[k, i] -> [k, i, 3]; S9[i] -> [n, i, 0]; S3[k] -> [k, 0, 1]; S5[k, i] -> [k, -k + i, 0]; S6[k] -> [k, 0, 2] }";
		String valueBasedFeautrierSchedule = "[n] -> { S1[] -> [0, 0, 0]; S0[] -> [0, 0, 0]; S4[k] -> [0, 0, k]; S2[] -> [0, 0, 0]; S8[k] -> [k, 2, 0]; S7[k, i] -> [k, 2, i]; S9[i] -> [n, 0, i]; S3[k] -> [k, 0, 0]; S5[k, i] -> [k, 0, i]; S6[k] -> [k, 1, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S9[0] -> S0[] : n = 1; S7[k, i] -> S7[-1 + k, i] : (k <= -1 + n and i >= 0 and 2i <= -2 + k) or (k <= -1 + n and 2i >= k and i <= -2 + k); S7[k, i] -> S7[-1 + k, -1 + k - i] : k <= -1 + n and i <= -1 + k and i >= 1; S5[1, 0] -> S0[] : n >= 2; S3[1] -> S2[] : n >= 2; S6[k] -> S5[k, -1 + k] : k >= 1 and k <= -1 + n; S9[-1 + n] -> S8[-1 + n] : n >= 2; S9[i] -> S7[-1 + n, i] : i >= 0 and i <= -2 + n; S5[k, i] -> S7[-1 + k, i] : i >= 0 and k <= -1 + n and i <= -2 + k and k >= 1; S3[k] -> S6[-1 + k] : k >= 2 and k <= -1 + n; S3[k] -> S3[-1 + k] : k >= 2 and k <= -1 + n; S7[k, 0] -> S8[-1 + k] : k >= 2 and k <= -1 + n; S7[k, -1 + k] -> S8[-1 + k] : k >= 2 and k <= -1 + n; S8[k] -> S6[k] : k >= 1 and k <= -1 + n; S6[k] -> S3[k] : k >= 1 and k <= -1 + n; S5[k, 0] -> S4[k] : k <= -1 + n and k >= 1; S5[k, -1 + k] -> S8[-1 + k] : k >= 2 and k <= -1 + n; S7[k, i] -> S6[k] : i >= 0 and i <= -1 + k and k >= 1 and k <= -1 + n; S3[1] -> S1[] : n >= 2; S5[k, i] -> S5[k, -1 + i] : k <= -1 + n and i <= -1 + k and i >= 1 and k >= 1; S7[1, 0] -> S0[] : n >= 2 }";
		String memoryBasedPlutoSchedule = "[n] -> { S1[] -> [0, 0, 0]; S0[] -> [0, 0, 0]; S4[k] -> [0, k, 0]; S2[] -> [0, 0, 0]; S8[k] -> [k, 0, 4]; S7[k, i] -> [k, i, 3]; S9[i] -> [n, i, 0]; S3[k] -> [k, 0, 1]; S5[k, i] -> [k, -k + i, 0]; S6[k] -> [k, 0, 2] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S1[] -> [0, 0, 0]; S0[] -> [0, 0, 0]; S4[k] -> [0, 0, k]; S2[] -> [0, 0, 0]; S8[k] -> [k, 2, 0]; S7[k, i] -> [k, 2, i]; S9[i] -> [n, 0, i]; S3[k] -> [k, 0, 0]; S5[k, i] -> [k, 0, i]; S6[k] -> [k, 1, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #121
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : Deriche_Derivateur_C99_Deriche_Derivateur_C99/scop_0
	 *   block : 
	 *     for : i0 <= i <= i1-1 (stride = 1)
	 *       for : j0 <= j <= j1-1 (stride = 1)
	 *         S0 (depth = 2) [Y[i][j]=mul(div(sub(add(sub(X[i][j],X[i][j+1]),X[i+1][j]),X[i+1][j+1]),IntExpr(2)),IntExpr(f))]
	 *         ([Y[i][j]]) = f([X[i][j], X[i][j+1], X[i+1][j], X[i+1][j+1]])
	 *         (Domain = [i0,i1,j0,j1,f] -> {S0[i,j] : (j-j0 >= 0) and (-j+j1-1 >= 0) and (i-i0 >= 0) and (-i+i1-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_Deriche_Derivateur_C99_GScopRoot_Deriche_Derivateur_C99_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_Deriche_Derivateur_C99_GScopRoot_Deriche_Derivateur_C99_scop_0_";
		// SCoP Extraction Data
		String domains = "[i0, i1, j0, j1, f] -> { S0[i, j] : j >= j0 and j <= -1 + j1 and i >= i0 and i <= -1 + i1 }";
		String idSchedules = "[i0, i1, j0, j1, f] -> { S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[i0, i1, j0, j1, f] -> { S0[i, j] -> Y[i, j] }";
		String reads = "[i0, i1, j0, j1, f] -> { S0[i, j] -> X[1 + i, 1 + j]; S0[i, j] -> X[i, 1 + j]; S0[i, j] -> X[1 + i, j]; S0[i, j] -> X[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[i0, i1, j0, j1, f] -> {  }";
		String valueBasedPlutoSchedule = "[i0, i1, j0, j1, f] -> { S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[i0, i1, j0, j1, f] -> { S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[i0, i1, j0, j1, f] -> {  }";
		String memoryBasedPlutoSchedule = "[i0, i1, j0, j1, f] -> { S0[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "[i0, i1, j0, j1, f] -> { S0[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #122
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : private_gaussian_filter_3_private_gaussian_filter_3/scop_0
	 *   block : 
	 *     for : 2 <= j <= M-1 (stride = 1)
	 *       S0 (depth = 1) [out[0][j-1]=sub(sub(mul(IntExpr(3),tmp[0][j-1]),tmp[0][j-2]),tmp[0][j])]
	 *       ([out[0][j-1]]) = f([tmp[0][j-1], tmp[0][j-2], tmp[0][j]])
	 *       (Domain = [M,N] -> {S0[j] : (j-2 >= 0) and (-j+M-1 >= 0)})
	 *     for : 1 <= i <= N-2 (stride = 1)
	 *       block : 
	 *         for : 0 <= j <= 1 (stride = 1)
	 *           S1 (depth = 2) [tmp[i][j]=sub(sub(mul(IntExpr(3),in[i][j]),in[i-1][j]),in[i+1][j])]
	 *           ([tmp[i][j]]) = f([in[i][j], in[i-1][j], in[i+1][j]])
	 *           (Domain = [M,N] -> {S1[i,j] : (j >= 0) and (-j+1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *         for : 2 <= j <= M-1 (stride = 1)
	 *           block : 
	 *             S2 (depth = 2) [tmp[i][j]=sub(sub(mul(IntExpr(3),in[i][j]),in[i-1][j]),in[i+1][j])]
	 *             ([tmp[i][j]]) = f([in[i][j], in[i-1][j], in[i+1][j]])
	 *             (Domain = [M,N] -> {S2[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *             S3 (depth = 2) [out[i][j-1]=sub(sub(mul(IntExpr(3),tmp[i][j-1]),tmp[i][j-2]),tmp[i][j])]
	 *             ([out[i][j-1]]) = f([tmp[i][j-1], tmp[i][j-2], tmp[i][j]])
	 *             (Domain = [M,N] -> {S3[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *     for : 2 <= j <= M-1 (stride = 1)
	 *       S4 (depth = 1) [out[N-1][j-1]=sub(sub(mul(IntExpr(3),tmp[N-1][j-1]),tmp[N-1][j-2]),tmp[N-1][j])]
	 *       ([out[N-1][j-1]]) = f([tmp[N-1][j-1], tmp[N-1][j-2], tmp[N-1][j]])
	 *       (Domain = [M,N] -> {S4[j] : (j-2 >= 0) and (-j+M-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_2D_gaussian_filter_fail_indices_GScopRoot_private_gaussian_filter_3_scop_0_ () {
		String path = "polymodel_others_2D_gaussian_filter_fail_indices_GScopRoot_private_gaussian_filter_3_scop_0_";
		// SCoP Extraction Data
		String domains = "[M, N] -> { S3[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S1[i, j] : j >= 0 and j <= 1 and i >= 1 and i <= -2 + N; S2[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S0[j] : j >= 2 and j <= -1 + M; S4[j] : j >= 2 and j <= -1 + M }";
		String idSchedules = "[M, N] -> { S4[j] -> [2, j, 0, 0, 0]; S1[i, j] -> [1, i, 0, j, 0]; S0[j] -> [0, j, 0, 0, 0]; S2[i, j] -> [1, i, 1, j, 0]; S3[i, j] -> [1, i, 1, j, 1] }";
		String writes = "[M, N] -> { S3[i, j] -> out[i, -1 + j]; S0[j] -> out[0, -1 + j]; S4[j] -> out[-1 + N, -1 + j]; S1[i, j] -> tmp[i, j]; S2[i, j] -> tmp[i, j] }";
		String reads = "[M, N] -> { S0[j] -> tmp[0, j]; S0[j] -> tmp[0, -1 + j]; S0[j] -> tmp[0, -2 + j]; S4[j] -> tmp[-1 + N, j]; S4[j] -> tmp[-1 + N, -1 + j]; S4[j] -> tmp[-1 + N, -2 + j]; S2[i, j] -> in[1 + i, j]; S2[i, j] -> in[i, j]; S2[i, j] -> in[-1 + i, j]; S3[i, j] -> tmp[i, j]; S3[i, j] -> tmp[i, -1 + j]; S3[i, j] -> tmp[i, -2 + j]; S1[i, j] -> in[1 + i, j]; S1[i, j] -> in[i, j]; S1[i, j] -> in[-1 + i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[M, N] -> { S3[i, j] -> S2[i, j'] : j' >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N and j' >= -2 + j and j' <= j; S3[i, j] -> S1[i, j'] : j' >= -2 + j and i >= 1 and i <= -2 + N and j' <= 1 and j >= 2 and j <= -1 + M }";
		String valueBasedPlutoSchedule = "[M, N] -> { S2[i, j] -> [i, j, 0]; S0[j] -> [j, 0, 0]; S3[i, j] -> [i, j, 2]; S1[i, j] -> [i, j, 1]; S4[j] -> [j, 0, 0] }";
		String valueBasedFeautrierSchedule = "[M, N] -> { S3[i, j] -> [i, j]; S1[i, j] -> [i, j]; S2[i, j] -> [i, j]; S4[j] -> [j, 0]; S0[j] -> [j, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[M, N] -> { S3[i, j] -> S1[i, j'] : j' >= -2 + j and i >= 1 and i <= -2 + N and j' <= 1 and j >= 2 and j <= -1 + M; S3[i, j] -> S2[i, j'] : j' >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N and j' >= -2 + j and j' <= j; S4[j] -> S0[j] : N = 1 and j >= 2 and j <= -1 + M }";
		String memoryBasedPlutoSchedule = "[M, N] -> { S2[i, j] -> [i, j, 0]; S0[j] -> [j, 0, 0]; S3[i, j] -> [i, j, 2]; S1[i, j] -> [i, j, 1]; S4[j] -> [j, 1, 0] }";
		String memoryBasedFeautrierSchedule = "[M, N] -> { S3[i, j] -> [i, j]; S1[i, j] -> [i, j]; S2[i, j] -> [i, j]; S4[j] -> [j, 0]; S0[j] -> [j, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #123
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : SubBandSynthesis_SubBandSynthesis/scop_0
	 *   for : 0 <= i <= 63 (stride = 1)
	 *     block : 
	 *       S0 (depth = 1) [isum=IntExpr(0)]
	 *       ([isum]) = f([])
	 *       (Domain = [] -> {S0[i] : (i >= 0) and (-i+63 >= 0)})
	 *       for : 0 <= k <= 31 (stride = 1)
	 *         S1 (depth = 2) [isum=add(isum,mul(bandPtr[k],filterInt[i][k]))]
	 *         ([isum]) = f([isum, bandPtr[k], filterInt[i][k]])
	 *         (Domain = [] -> {S1[i,k] : (k >= 0) and (-k+31 >= 0) and (i >= 0) and (-i+63 >= 0)})
	 *       S2 (depth = 1) [ibufOffsetPtr[i]=isum]
	 *       ([ibufOffsetPtr[i]]) = f([isum])
	 *       (Domain = [] -> {S2[i] : (i >= 0) and (-i+63 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_subband_GScopRoot_SubBandSynthesis_scop_0_ () {
		String path = "basics_oldFiles_subband_GScopRoot_SubBandSynthesis_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S0[i] : i >= 0 and i <= 63; S2[i] : i >= 0 and i <= 63; S1[i, k] : k >= 0 and k <= 31 and i >= 0 and i <= 63 }";
		String idSchedules = "{ S1[i, k] -> [0, i, 1, k, 0]; S2[i] -> [0, i, 2, 0, 0]; S0[i] -> [0, i, 0, 0, 0] }";
		String writes = "{ S2[i] -> ibufOffsetPtr[i]; S0[i] -> isum[]; S1[i, k] -> isum[] }";
		String reads = "{ S1[i, k] -> bandPtr[k]; S1[i, k] -> filterInt[i, k]; S1[i, k] -> isum[]; S2[i] -> isum[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S2[i] -> S1[i, 31] : i >= 0 and i <= 63; S1[i, 0] -> S0[i] : i >= 0 and i <= 63; S1[i, k] -> S1[i, -1 + k] : k >= 1 and k <= 31 and i >= 0 and i <= 63 }";
		String valueBasedPlutoSchedule = "{ S1[i, k] -> [i, 63 + k, 0]; S0[i] -> [0, i, 2]; S2[i] -> [63, 94 + i, 1] }";
		String valueBasedFeautrierSchedule = "{ S1[i, k] -> [1 + k, i]; S2[i] -> [33, i]; S0[i] -> [0, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S0[i] -> S2[-1 + i] : i <= 63 and i >= 1; S2[i] -> S1[i, 31] : i >= 0 and i <= 63; S1[i, 0] -> S0[i] : i <= 63 and i >= 0; S0[i] -> S1[-1 + i, 31] : i >= 1 and i <= 63; S1[i, k] -> S1[i, -1 + k] : i <= 63 and i >= 0 and k <= 31 and k >= 1 }";
		String memoryBasedPlutoSchedule = "{ S1[i, k] -> [i, 31i + k, 1]; S0[i] -> [i, 31i, 0]; S2[i] -> [i, 31 + 31i, 2] }";
		String memoryBasedFeautrierSchedule = "{ S1[i, k] -> [1 + 34i + k, i]; S2[i] -> [33 + 34i, 0]; S0[i] -> [34i, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #124
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : SubBandSynthesis_SubBandSynthesis/scop_1
	 *   block : 
	 *     for : 0 <= i <= 1 (stride = 1)
	 *       for : 0 <= j <= 2*HAN_SIZE-1 (stride = 1)
	 *         S0 (depth = 2) [ibuf[i][j]=IntExpr(0)]
	 *         ([ibuf[i][j]]) = f([])
	 *         (Domain = [HAN_SIZE] -> {S0[i,j] : (j >= 0) and (-j+2*HAN_SIZE-1 >= 0) and (i >= 0) and (-i+1 >= 0)})
	 *     S1 (depth = 0) [init=IntExpr(0)]
	 *     ([init]) = f([])
	 *     (Domain = [HAN_SIZE] -> {S1[] : })
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_subband_GScopRoot_SubBandSynthesis_scop_1_ () {
		String path = "basics_oldFiles_subband_GScopRoot_SubBandSynthesis_scop_1_";
		// SCoP Extraction Data
		String domains = "[HAN_SIZE] -> { S1[]; S0[i, j] : j >= 0 and j <= -1 + 2HAN_SIZE and i >= 0 and i <= 1 }";
		String idSchedules = "[HAN_SIZE] -> { S0[i, j] -> [0, i, 0, j, 0]; S1[] -> [1, 0, 0, 0, 0] }";
		String writes = "[HAN_SIZE] -> { S0[i, j] -> ibuf[i, j]; S1[] -> init[] }";
		String reads = "[HAN_SIZE] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[HAN_SIZE] -> {  }";
		String valueBasedPlutoSchedule = "[HAN_SIZE] -> { S1[] -> [0, 0]; S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[HAN_SIZE] -> { S1[] -> [0, 0]; S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[HAN_SIZE] -> {  }";
		String memoryBasedPlutoSchedule = "[HAN_SIZE] -> { S1[] -> [0, 0]; S0[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "[HAN_SIZE] -> { S1[] -> [0, 0]; S0[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #125
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : hybrid_jacobi_gauss_seidel_original_hybrid_jacobi_gauss_seidel_original/scop_0
	 *   for : 0 <= i <= N-1 (stride = 1)
	 *     for : 0 <= j <= N-1 (stride = 1)
	 *       S0 (depth = 2) [A[i][j]=mul(add(add(add(add(B[i][j],A[i-1][j]),B[i+1][j]),B[i][j-1]),B[i][j+1]),0.20000000298023224)]
	 *       ([A[i][j]]) = f([B[i][j], A[i-1][j], B[i+1][j], B[i][j-1], B[i][j+1]])
	 *       (Domain = [N] -> {S0[i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_openMP_HybridJacobiGaussSeidelOriginal_GScopRoot_hybrid_jacobi_gauss_seidel_original_scop_0_ () {
		String path = "polymodel_others_openMP_HybridJacobiGaussSeidelOriginal_GScopRoot_hybrid_jacobi_gauss_seidel_original_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[N] -> { S0[i, j] -> A[i, j] }";
		String reads = "[N] -> { S0[i, j] -> B[i, 1 + j]; S0[i, j] -> B[1 + i, j]; S0[i, j] -> B[i, j]; S0[i, j] -> B[i, -1 + j]; S0[i, j] -> A[-1 + i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S0[i, j] -> S0[-1 + i, j] : j >= 0 and j <= -1 + N and i >= 1 and i <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S0[i, j] -> S0[-1 + i, j] : j >= 0 and j <= -1 + N and i >= 1 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #126
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : stencil_stencil/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-1 (stride = 1)
	 *       for : 1 <= j <= M-1 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [resid=div(add(add(add(add(uold[i][j],uold[i-1][j]),uold[i+1][j]),uold[i][j-1]),uold[i][j+1]),IntExpr(b))]
	 *           ([resid]) = f([uold[i][j], uold[i-1][j], uold[i+1][j], uold[i][j-1], uold[i][j+1]])
	 *           (Domain = [N,M,b,omega] -> {S0[i,j] : (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *           S1 (depth = 2) [u[i][j]=sub(uold[i][j],mul(IntExpr(omega),resid))]
	 *           ([u[i][j]]) = f([uold[i][j], resid])
	 *           (Domain = [N,M,b,omega] -> {S1[i,j] : (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 *           S2 (depth = 2) [error=add(error,mul(resid,resid))]
	 *           ([error]) = f([error, resid, resid])
	 *           (Domain = [N,M,b,omega] -> {S2[i,j] : (j-1 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_divs_ompverifier_examples_GScopRoot_stencil_scop_0_ () {
		String path = "polymodel_divs_ompverifier_examples_GScopRoot_stencil_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M, b, omega] -> { S1[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S0[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S2[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N }";
		String idSchedules = "[N, M, b, omega] -> { S0[i, j] -> [0, i, 0, j, 0]; S1[i, j] -> [0, i, 0, j, 1]; S2[i, j] -> [0, i, 0, j, 2] }";
		String writes = "[N, M, b, omega] -> { S2[i, j] -> error[]; S1[i, j] -> u[i, j]; S0[i, j] -> resid[] }";
		String reads = "[N, M, b, omega] -> { S0[i, j] -> uold[i, 1 + j]; S0[i, j] -> uold[1 + i, j]; S0[i, j] -> uold[i, j]; S0[i, j] -> uold[-1 + i, j]; S0[i, j] -> uold[i, -1 + j]; S2[i, j] -> error[]; S2[i, j] -> resid[]; S1[i, j] -> resid[]; S1[i, j] -> uold[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M, b, omega] -> { S1[i, j] -> S0[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S2[i, j] -> S0[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S2[i, j] -> S2[i, -1 + j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -1 + N; S2[i, 1] -> S2[-1 + i, -1 + M] : i <= -1 + N and M >= 2 and i >= 2 }";
		String valueBasedPlutoSchedule = "[N, M, b, omega] -> { S2[i, j] -> [i, j, 1]; S0[i, j] -> [i, j, 0]; S1[i, j] -> [i, j, 2] }";
		String valueBasedFeautrierSchedule = "[N, M, b, omega] -> { S1[i, j] -> [i, j]; S2[i, j] -> [i, j]; S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M, b, omega] -> { S2[i, j] -> S2[i, -1 + j] : i >= 1 and i <= -1 + N and j >= 2 and j <= -1 + M; S2[i, 1] -> S2[-1 + i, -1 + M] : M >= 2 and i >= 2 and i <= -1 + N; S2[i, j] -> S0[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S0[i, j] -> S2[i, -1 + j] : i >= 1 and i <= -1 + N and j >= 2 and j <= -1 + M; S0[i, 1] -> S2[-1 + i, -1 + M] : M >= 2 and i >= 2 and i <= -1 + N; S0[i, j] -> S0[i, -1 + j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -1 + N; S0[i, 1] -> S0[-1 + i, -1 + M] : i <= -1 + N and M >= 2 and i >= 2; S1[i, j] -> S0[i, j] : j >= 1 and j <= -1 + M and i >= 1 and i <= -1 + N; S0[i, j] -> S1[i, -1 + j] : i >= 1 and i <= -1 + N and j >= 2 and j <= -1 + M; S0[i, 1] -> S1[-1 + i, -1 + M] : M >= 2 and i >= 2 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N, M, b, omega] -> { S2[i, j] -> [i, j, 1]; S0[i, j] -> [i, j, 0]; S1[i, j] -> [i, j, 2] }";
		String memoryBasedFeautrierSchedule = "[N, M, b, omega] -> { S2[i, j] -> [i, j, 1]; S0[i, j] -> [i, j, 0]; S1[i, j] -> [i, j, 1] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #127
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : hybrid_jacobi_gauss_seidel_hybrid_jacobi_gauss_seidel/scop_0
	 *   block : 
	 *     for : 0 <= t <= (-1) * 1 + [(T) / 2] (stride = 1)
	 *       block : 
	 *         for : 0 <= i <= N-1 (stride = 1)
	 *           for : 0 <= j <= N-1 (stride = 1)
	 *             S0 (depth = 3) [A[i][j]=mul(add(add(add(add(B[i][j],A[i-1][j]),B[i+1][j]),B[i][j-1]),B[i][j+1]),0.20000000298023224)]
	 *             ([A[i][j]]) = f([B[i][j], A[i-1][j], B[i+1][j], B[i][j-1], B[i][j+1]])
	 *             (Domain = [T,N] -> {S0[t,i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0) and (t >= 0) and (-2*t+T-2 >= 0)})
	 *         for : 0 <= i <= N-1 (stride = 1)
	 *           for : 0 <= j <= N-1 (stride = 1)
	 *             S1 (depth = 3) [B[i][j]=mul(add(add(add(add(A[i][j],B[i-1][j]),A[i+1][j]),A[i][j-1]),A[i][j+1]),0.20000000298023224)]
	 *             ([B[i][j]]) = f([A[i][j], B[i-1][j], A[i+1][j], A[i][j-1], A[i][j+1]])
	 *             (Domain = [T,N] -> {S1[t,i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0) and (t >= 0) and (-2*t+T-2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_divs_ompverifier_examples_GScopRoot_hybrid_jacobi_gauss_seidel_scop_0_ () {
		String path = "polymodel_divs_ompverifier_examples_GScopRoot_hybrid_jacobi_gauss_seidel_scop_0_";
		// SCoP Extraction Data
		String domains = "[T, N] -> { S1[t, i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N and t >= 0 and 2t <= -2 + T; S0[t, i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N and t >= 0 and 2t <= -2 + T }";
		String idSchedules = "[T, N] -> { S1[t, i, j] -> [0, t, 1, i, 0, j, 0]; S0[t, i, j] -> [0, t, 0, i, 0, j, 0] }";
		String writes = "[T, N] -> { S0[t, i, j] -> A[i, j]; S1[t, i, j] -> B[i, j] }";
		String reads = "[T, N] -> { S0[t, i, j] -> A[-1 + i, j]; S1[t, i, j] -> B[-1 + i, j]; S0[t, i, j] -> B[i, 1 + j]; S0[t, i, j] -> B[1 + i, j]; S0[t, i, j] -> B[i, j]; S0[t, i, j] -> B[i, -1 + j]; S1[t, i, j] -> A[i, 1 + j]; S1[t, i, j] -> A[1 + i, j]; S1[t, i, j] -> A[i, j]; S1[t, i, j] -> A[i, -1 + j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[T, N] -> { S1[t, i, j] -> S1[t, -1 + i, j] : j >= 0 and j <= -1 + N and i >= 1 and i <= -1 + N and t >= 0 and 2t <= -2 + T; S0[t, i, j] -> S0[t, -1 + i, j] : j >= 0 and j <= -1 + N and i >= 1 and i <= -1 + N and t >= 0 and 2t <= -2 + T; S0[t, i, j] -> S1[-1 + t, i, 1 + j] : j >= 0 and j <= -2 + N and i >= 0 and i <= -1 + N and t >= 1 and 2t <= -2 + T; S0[t, i, j] -> S1[-1 + t, 1 + i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -2 + N and t >= 1 and 2t <= -2 + T; S0[t, i, j] -> S1[-1 + t, i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N and t >= 1 and 2t <= -2 + T; S0[t, i, j] -> S1[-1 + t, i, -1 + j] : j >= 1 and j <= -1 + N and i >= 0 and i <= -1 + N and t >= 1 and 2t <= -2 + T; S1[t, i, j] -> S0[t, i, 1 + j] : j >= 0 and j <= -2 + N and i >= 0 and i <= -1 + N and t >= 0 and 2t <= -2 + T; S1[t, i, j] -> S0[t, 1 + i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -2 + N and t >= 0 and 2t <= -2 + T; S1[t, i, j] -> S0[t, i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N and t >= 0 and 2t <= -2 + T; S1[t, i, j] -> S0[t, i, -1 + j] : j >= 1 and j <= -1 + N and i >= 0 and i <= -1 + N and t >= 0 and 2t <= -2 + T }";
		String valueBasedPlutoSchedule = "[T, N] -> { S1[t, i, j] -> [t, 1 + 2t + i, 1 + 2t + i + j, 1]; S0[t, i, j] -> [t, 2t + i, 2t + i + j, 0] }";
		String valueBasedFeautrierSchedule = "[T, N] -> { S0[t, i, j] -> [4t + i, t, j]; S1[t, i, j] -> [2 + 4t + i, t, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[T, N] -> { S1[t, i, j] -> S1[-1 + t, 1 + i, j] : t >= 1 and 2t <= -2 + T and i >= 1 and i <= -2 + N and j >= 0 and j <= -1 + N; S1[t, i, j] -> S1[-1 + t, i, j] : t >= 1 and 2t <= -2 + T and i >= 0 and i <= -1 + N and j >= 0 and j <= -1 + N; S1[t, i, j] -> S1[t, -1 + i, j] : t >= 0 and 2t <= -2 + T and i >= 1 and i <= -1 + N and j >= 0 and j <= -1 + N; S1[t, 0, j] -> S1[-1 + t, 1, j] : t >= 1 and 2t <= -2 + T and j >= 1 and j <= -1 + N; S1[t, 0, 0] -> S1[-1 + t, 1, 0] : N >= 2 and t >= 1 and 2t <= -2 + T; S0[t, i, j] -> S0[-1 + t, 1 + i, j] : t >= 1 and 2t <= -2 + T and i >= 0 and i <= -2 + N and j >= 0 and j <= -1 + N; S0[t, i, j] -> S0[-1 + t, i, j] : t >= 1 and 2t <= -2 + T and i >= 0 and i <= -1 + N and j >= 0 and j <= -1 + N; S0[t, i, j] -> S0[t, -1 + i, j] : t >= 0 and 2t <= -2 + T and i >= 1 and i <= -1 + N and j >= 0 and j <= -1 + N; S0[t, i, j] -> S1[-1 + t, i, 1 + j] : t >= 1 and 2t <= -2 + T and i >= 0 and i <= -1 + N and j >= 0 and j <= -2 + N; S0[t, i, j] -> S1[-1 + t, 1 + i, j] : t >= 1 and 2t <= -2 + T and i >= 0 and i <= -2 + N and j >= 0 and j <= -1 + N; S0[t, i, j] -> S1[-1 + t, i, j] : t >= 1 and 2t <= -2 + T and i >= 0 and i <= -1 + N and j >= 0 and j <= -1 + N; S0[t, i, j] -> S1[-1 + t, -1 + i, j] : t >= 1 and 2t <= -2 + T and i >= 1 and i <= -1 + N and j >= 0 and j <= -1 + N; S0[t, i, j] -> S1[-1 + t, i, -1 + j] : t >= 1 and 2t <= -2 + T and i >= 0 and i <= -1 + N and j >= 1 and j <= -1 + N; S1[t, i, j] -> S0[t, i, 1 + j] : t >= 0 and 2t <= -2 + T and i >= 0 and i <= -1 + N and j >= 0 and j <= -2 + N; S1[t, i, j] -> S0[t, 1 + i, j] : t >= 0 and 2t <= -2 + T and i >= 0 and i <= -2 + N and j >= 0 and j <= -1 + N; S1[t, i, j] -> S0[t, i, j] : t >= 0 and 2t <= -2 + T and i >= 0 and i <= -1 + N and j >= 0 and j <= -1 + N; S1[t, i, j] -> S0[t, -1 + i, j] : t >= 0 and 2t <= -2 + T and i >= 1 and i <= -1 + N and j >= 0 and j <= -1 + N; S1[t, i, j] -> S0[t, i, -1 + j] : t >= 0 and 2t <= -2 + T and i >= 0 and i <= -1 + N and j >= 1 and j <= -1 + N }";
		String memoryBasedPlutoSchedule = "[T, N] -> { S1[t, i, j] -> [t, 1 + 2t + i, 1 + 2t + i + j, 1]; S0[t, i, j] -> [t, 2t + i, 2t + i + j, 0] }";
		String memoryBasedFeautrierSchedule = "[T, N] -> { S0[t, i, j] -> [4t + i, t, j]; S1[t, i, j] -> [2 + 4t + i, t, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #128
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : forward_substitution_original_forward_substitution_original/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [x[i]=b[i]]
	 *         ([x[i]]) = f([b[i]])
	 *         (Domain = [N] -> {S0[i] : (i >= 0) and (-i+N-1 >= 0)})
	 *         for : 0 <= j <= i-1 (stride = 1)
	 *           S1 (depth = 2) [x[i]=sub(x[i],mul(L[i][j],x[j]))]
	 *           ([x[i]]) = f([x[i], L[i][j], x[j]])
	 *           (Domain = [N] -> {S1[i,j] : (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 *         S2 (depth = 1) [x[i]=div(x[i],L[i][i])]
	 *         ([x[i]]) = f([x[i], L[i][i]])
	 *         (Domain = [N] -> {S2[i] : (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_divs_ompverifier_examples_GScopRoot_forward_substitution_original_scop_0_ () {
		String path = "polymodel_divs_ompverifier_examples_GScopRoot_forward_substitution_original_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[i] : i >= 0 and i <= -1 + N; S1[i, j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N; S2[i] : i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S2[i] -> [0, i, 2, 0, 0]; S1[i, j] -> [0, i, 1, j, 0]; S0[i] -> [0, i, 0, 0, 0] }";
		String writes = "[N] -> { S1[i, j] -> x[i]; S0[i] -> x[i]; S2[i] -> x[i] }";
		String reads = "[N] -> { S0[i] -> b[i]; S1[i, j] -> L[i, j]; S1[i, j] -> x[i]; S1[i, j] -> x[j]; S2[i] -> L[i, i]; S2[i] -> x[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S1[i, j] -> S2[j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N; S1[i, j] -> S1[i, -1 + j] : i <= -1 + N and j <= -1 + i and j >= 1 and i >= 0; S2[0] -> S0[0] : N >= 1; S2[i] -> S1[i, -1 + i] : i >= 1 and i <= -1 + N; S1[i, 0] -> S0[i] : i <= -1 + N and i >= 1 }";
		String valueBasedPlutoSchedule = "[N] -> { S0[i] -> [0, i, 0]; S2[i] -> [i, 2i, 1]; S1[i, j] -> [i, i + j, 2] }";
		String valueBasedFeautrierSchedule = "[N] -> { S1[i, j] -> [1 + i + j, j]; S2[i] -> [1 + 2i, 0]; S0[i] -> [0, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S1[i, j] -> S2[j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N; S1[i, j] -> S1[i, -1 + j] : i <= -1 + N and j >= 1 and j <= -1 + i; S2[0] -> S0[0] : N >= 1; S2[i] -> S1[i, -1 + i] : i >= 1 and i <= -1 + N; S1[i, 0] -> S0[i] : i >= 1 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[i] -> [0, i, 0]; S2[i] -> [i, 2i, 1]; S1[i, j] -> [i, i + j, 2] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S1[i, j] -> [1 + i + j, j]; S2[i] -> [1 + 2i, 0]; S0[i] -> [0, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #129
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : hybrid_jacobi_gauss_seidel_original_not_SCOP_hybrid_jacobi_gauss_seidel_original_not_SCOP/scop_0
	 *   for : 0 <= i <= N-1 (stride = 1)
	 *     for : 0 <= j <= N-1 (stride = 1)
	 *       S0 (depth = 2) [A[i][j]=mul(add(add(add(add(B[i][j],A[i-1][j]),B[i+1][j]),B[i][j-1]),B[i][j+1]),0.20000000298023224)]
	 *       ([A[i][j]]) = f([B[i][j], A[i-1][j], B[i+1][j], B[i][j-1], B[i][j+1]])
	 *       (Domain = [N] -> {S0[i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_divs_ompverifier_examples_GScopRoot_hybrid_jacobi_gauss_seidel_original_not_SCOP_scop_0_ () {
		String path = "polymodel_divs_ompverifier_examples_GScopRoot_hybrid_jacobi_gauss_seidel_original_not_SCOP_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[N] -> { S0[i, j] -> A[i, j] }";
		String reads = "[N] -> { S0[i, j] -> B[i, 1 + j]; S0[i, j] -> B[1 + i, j]; S0[i, j] -> B[i, j]; S0[i, j] -> B[i, -1 + j]; S0[i, j] -> A[-1 + i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S0[i, j] -> S0[-1 + i, j] : j >= 0 and j <= -1 + N and i >= 1 and i <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S0[i, j] -> S0[-1 + i, j] : j >= 0 and j <= -1 + N and i >= 1 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S0[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #130
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : matrix_transpose_matrix_transpose/scop_0
	 *   block : 
	 *     for : 0 <= p1 <= N-1 (stride = 1)
	 *       for : 0 <= p2 <= p1-1 (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [temp=A[p1][p2]]
	 *           ([temp]) = f([A[p1][p2]])
	 *           (Domain = [N] -> {S0[p1,p2] : (p2 >= 0) and (p1-p2-1 >= 0) and (p1 >= 0) and (-p1+N-1 >= 0)})
	 *           S1 (depth = 2) [A[p1][p2]=A[p2][p1]]
	 *           ([A[p1][p2]]) = f([A[p2][p1]])
	 *           (Domain = [N] -> {S1[p1,p2] : (p2 >= 0) and (p1-p2-1 >= 0) and (p1 >= 0) and (-p1+N-1 >= 0)})
	 *           S2 (depth = 2) [A[p2][p1]=temp]
	 *           ([A[p2][p1]]) = f([temp])
	 *           (Domain = [N] -> {S2[p1,p2] : (p2 >= 0) and (p1-p2-1 >= 0) and (p1 >= 0) and (-p1+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_divs_ompverifier_examples_GScopRoot_matrix_transpose_scop_0_ () {
		String path = "polymodel_divs_ompverifier_examples_GScopRoot_matrix_transpose_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[p1, p2] : p2 >= 0 and p2 <= -1 + p1 and p1 >= 0 and p1 <= -1 + N; S1[p1, p2] : p2 >= 0 and p2 <= -1 + p1 and p1 >= 0 and p1 <= -1 + N; S2[p1, p2] : p2 >= 0 and p2 <= -1 + p1 and p1 >= 0 and p1 <= -1 + N }";
		String idSchedules = "[N] -> { S0[p1, p2] -> [0, p1, 0, p2, 0]; S2[p1, p2] -> [0, p1, 0, p2, 2]; S1[p1, p2] -> [0, p1, 0, p2, 1] }";
		String writes = "[N] -> { S2[p1, p2] -> A[p2, p1]; S0[p1, p2] -> temp[]; S1[p1, p2] -> A[p1, p2] }";
		String reads = "[N] -> { S2[p1, p2] -> temp[]; S0[p1, p2] -> A[p1, p2]; S1[p1, p2] -> A[p2, p1] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S2[p1, p2] -> S0[p1, p2] : p2 >= 0 and p2 <= -1 + p1 and p1 >= 0 and p1 <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S2[p1, p2] -> [p1, p2, 1]; S0[p1, p2] -> [p1, p2, 0]; S1[p1, p2] -> [p1, p2, 0] }";
		String valueBasedFeautrierSchedule = "[N] -> { S1[p1, p2] -> [p1, p2]; S2[p1, p2] -> [p1, p2]; S0[p1, p2] -> [p1, p2] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S1[p1, p2] -> S0[p1, p2] : p1 <= -1 + N and p2 >= 0 and p2 <= -1 + p1 and p1 >= 0; S0[p1, p2] -> S0[p1, -1 + p2] : p1 <= -1 + N and p2 <= -1 + p1 and p2 >= 1; S0[p1, 0] -> S0[-1 + p1, -2 + p1] : p1 <= -1 + N and p1 >= 2; S2[p1, p2] -> S0[p1, p2] : p2 >= 0 and p2 <= -1 + p1 and p1 >= 0 and p1 <= -1 + N; S2[p1, p2] -> S1[p1, p2] : p1 <= -1 + N and p2 >= 0 and p2 <= -1 + p1 and p1 >= 0; S0[p1, p2] -> S2[p1, -1 + p2] : p1 <= -1 + N and p2 >= 1 and p2 <= -1 + p1; S0[p1, 0] -> S2[-1 + p1, -2 + p1] : p1 >= 2 and p1 <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S2[p1, p2] -> [p1, p2, 2]; S0[p1, p2] -> [p1, p2, 0]; S1[p1, p2] -> [p1, p2, 1] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S2[p1, p2] -> [p1, p2, 2]; S0[p1, p2] -> [p1, p2, 0]; S1[p1, p2] -> [p1, p2, 1] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #131
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : nussinov_nussinov/scop_0
	 *   block : 
	 *     for : 0 <= d <= N-1 (stride = 1)
	 *       for : 1 <= i <= N-d (stride = 1)
	 *         block : 
	 *           S0 (depth = 2) [tmpMax=IntExpr(0)]
	 *           ([tmpMax]) = f([])
	 *           (Domain = [N] -> {S0[d,i] : (i-1 >= 0) and (-d-i+N >= 0) and (d >= 0) and (-d+N-1 >= 0)})
	 *           for : i+1 <= k <= i+d-1 (stride = 1)
	 *             block : 
	 *               S1 (depth = 3) [tmp=add(X[i][k],X[k+1][i+d])]
	 *               ([tmp]) = f([X[i][k], X[k+1][i+d]])
	 *               (Domain = [N] -> {S1[d,i,k] : (-i+k-1 >= 0) and (d+i-k-1 >= 0) and (i-1 >= 0) and (-d-i+N >= 0) and (d >= 0) and (-d+N-1 >= 0)})
	 *               S2 (depth = 3) [tmpMax=mux(gt(tmpMax,tmp),tmpMax,tmp)]
	 *               ([tmpMax]) = f([tmpMax, tmp, tmpMax, tmp])
	 *               (Domain = [N] -> {S2[d,i,k] : (-i+k-1 >= 0) and (d+i-k-1 >= 0) and (i-1 >= 0) and (-d-i+N >= 0) and (d >= 0) and (-d+N-1 >= 0)})
	 *           S3 (depth = 2) [X[i][i+d]=mux(gt(X[i+1][i+d],X[i][i+d-1]),X[i+1][i+d],mux(gt(gt(X[i][i+d-1],add(X[i+1][i+d-1],delta[i][i+d])),tmpMax),add(X[i+1][i+d-1],delta[i][i+d]),mux(tmpMax,mux(gt(X[i+1][i+d],X[i][i+d-1]),X[i+1][i+d],X[i][i+d-1]),mux(gt(add(X[i+1][i+d-1],delta[i][i+d]),tmpMax),add(X[i+1][i+d-1],delta[i][i+d]),tmpMax))))]
	 *           ([X[i][i+d]]) = f([X[i+1][i+d], X[i][i+d-1], X[i+1][i+d], X[i][i+d-1], X[i+1][i+d-1], delta[i][i+d], tmpMax, X[i+1][i+d-1], delta[i][i+d], tmpMax, X[i+1][i+d], X[i][i+d-1], X[i+1][i+d], X[i][i+d-1], X[i+1][i+d-1], delta[i][i+d], tmpMax, X[i+1][i+d-1], delta[i][i+d], tmpMax])
	 *           (Domain = [N] -> {S3[d,i] : (i-1 >= 0) and (-d-i+N >= 0) and (d >= 0) and (-d+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_nussinov_GScopRoot_nussinov_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_nussinov_GScopRoot_nussinov_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S1[d, i, k] : k >= 1 + i and k <= -1 + d + i and i >= 1 and i <= N - d and d >= 0 and d <= -1 + N; S2[d, i, k] : k >= 1 + i and k <= -1 + d + i and i >= 1 and i <= N - d and d >= 0 and d <= -1 + N; S0[d, i] : i >= 1 and i <= N - d and d >= 0 and d <= -1 + N; S3[d, i] : i >= 1 and i <= N - d and d >= 0 and d <= -1 + N }";
		String idSchedules = "[N] -> { S3[d, i] -> [0, d, 0, i, 2, 0, 0]; S0[d, i] -> [0, d, 0, i, 0, 0, 0]; S1[d, i, k] -> [0, d, 0, i, 1, k, 0]; S2[d, i, k] -> [0, d, 0, i, 1, k, 1] }";
		String writes = "[N] -> { S2[d, i, k] -> tmpMax[]; S0[d, i] -> tmpMax[]; S3[d, i] -> X[i, d + i]; S1[d, i, k] -> tmp[] }";
		String reads = "[N] -> { S2[d, i, k] -> tmpMax[]; S3[d, i] -> tmpMax[]; S3[d, i] -> delta[i, d + i]; S1[d, i, k] -> X[1 + k, d + i]; S1[d, i, k] -> X[i, k]; S2[d, i, k] -> tmp[]; S3[d, i] -> X[1 + i, d + i]; S3[d, i] -> X[1 + i, -1 + d + i]; S3[d, i] -> X[i, -1 + d + i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S3[d, i] -> S0[d, i] : i >= 1 and i <= N - d and d >= 0 and d <= 1 and d <= -1 + N; S2[d, i, k] -> S2[d, i, -1 + k] : k >= 2 + i and k <= -1 + d + i and i >= 1 and i <= N - d and d <= -1 + N and d >= 0; S1[d, i, k] -> S3[-1 + d + i - k, 1 + k] : k >= 1 + i and k <= -1 + d + i and i >= 1 and i <= N - d; S1[d, i, k] -> S3[-i + k, i] : k >= 1 + i and k <= -1 + d + i and i >= 1 and i <= N - d; S2[d, i, k] -> S1[d, i, k] : k >= 1 + i and k <= -1 + d + i and i >= 1 and i <= N - d and d <= -1 + N and d >= 0; S3[d, i] -> S2[d, i, -1 + d + i] : i >= 1 and i <= N - d and d >= 2 and d <= -1 + N; S3[d, i] -> S3[d', i'] : i <= N - d and d' <= -1 + d and i >= 1 and i' <= 1 + i and d' >= 0 and i' >= -1 + d + i - d'; S2[d, i, 1 + i] -> S0[d, i] : i <= N - d and d >= 2 and i >= 1 and d <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S0[d, i] -> [0, d, i, 0]; S2[d, i, k] -> [d, d + i, k, 2]; S3[d, i] -> [d, d + i, d + i, 0]; S1[d, i, k] -> [d, d + i, k, 1] }";
		String valueBasedFeautrierSchedule = "[N] -> { S0[d, i] -> [0, 0, d, i]; S2[d, i, k] -> [d, 1, k, i]; S3[d, i] -> [d, 2, i, 0]; S1[d, i, k] -> [d, 0, i, k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S3[d, i] -> S3[d', i'] : i <= N - d and d' <= -1 + d and i >= 1 and i' <= 1 + i and d' >= 0 and i' >= -1 + d + i - d'; S2[d, i, k] -> S1[d, i, k] : k >= 1 + i and k <= -1 + d + i and i >= 1 and i <= N - d and d <= -1 + N and d >= 0; S3[d, i] -> S2[d, i, -1 + d + i] : i >= 1 and i <= N - d and d >= 2 and d <= -1 + N; S1[d, i, k] -> S3[-1 + d + i - k, 1 + k] : k >= 1 + i and k <= -1 + d + i and i >= 1 and i <= N - d; S1[d, i, k] -> S3[-i + k, i] : k >= 1 + i and k <= -1 + d + i and i >= 1 and i <= N - d; S0[d, i] -> S0[d, -1 + i] : i >= 2 and i <= N - d and d >= 0 and d <= 1; S0[d, 1] -> S0[-1 + d, 1 + N - d] : d >= 1 and d <= -1 + N and d <= 2; S1[d, i, k] -> S1[d, i, -1 + k] : k >= 2 + i and k <= -1 + d + i and i >= 1 and i <= N - d; S1[d, i, 1 + i] -> S1[d, -1 + i, -2 + d + i] : i <= N - d and d >= 2 and i >= 2; S1[d, 1, 2] -> S1[-1 + d, 1 + N - d, -1 + N] : d <= -1 + N and d >= 3; S2[d, i, k] -> S2[d, i, -1 + k] : i >= 1 and i <= N - d and k >= 2 + i and k <= -1 + d + i; S0[d, i] -> S3[d, -1 + i] : i >= 2 and i <= N - d and d >= 0; S0[d, 1] -> S3[-1 + d, 1 + N - d] : d <= -1 + N and d >= 1; S0[d, i] -> S2[d, -1 + i, -2 + d + i] : d >= 2 and i <= N - d and i >= 2; S0[d, 1] -> S2[-1 + d, 1 + N - d, -1 + N] : d >= 3 and d <= -1 + N; S1[d, i, k] -> S2[d, i, -1 + k] : i >= 1 and i <= N - d and k >= 2 + i and k <= -1 + d + i; S1[d, i, 1 + i] -> S2[d, -1 + i, -2 + d + i] : d >= 2 and i >= 2 and i <= N - d; S1[d, 1, 2] -> S2[-1 + d, 1 + N - d, -1 + N] : d >= 3 and d <= -1 + N; S3[d, i] -> S0[d, i] : i >= 1 and i <= N - d and d >= 0 and d <= 1 and d <= -1 + N; S2[d, i, 1 + i] -> S0[d, i] : d >= 2 and i >= 1 and i <= N - d }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[d, i] -> [d, i, 0, 1]; S2[d, i, k] -> [d, i, k, 3]; S3[d, i] -> [d, i, d + i, 0]; S1[d, i, k] -> [d, i, k, 2] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S0[d, i] -> [d, i, 0, 0, 0]; S1[d, i, k] -> [d, i, 1, k, 0]; S2[d, i, k] -> [d, i, 1, k, 1]; S3[d, i] -> [d, i, 2, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #132
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_fdtd_apml_kernel_fdtd_apml/scop_0
	 *   for : 0 <= iz <= cz-1 (stride = 1)
	 *     for : 0 <= iy <= cym-1 (stride = 1)
	 *       block : 
	 *         for : 0 <= ix <= cxm-1 (stride = 1)
	 *           block : 
	 *             S0 (depth = 3) [clf[iz][iy]=sub(add(sub(Ex[iz][iy][ix],Ex[iz][iy+1][ix]),Ey[iz][iy][ix+1]),Ey[iz][iy][ix])]
	 *             ([clf[iz][iy]]) = f([Ex[iz][iy][ix], Ex[iz][iy+1][ix], Ey[iz][iy][ix+1], Ey[iz][iy][ix]])
	 *             (Domain = [cz,cym,cxm,ch,mui] -> {S0[iz,iy,ix] : (ix >= 0) and (-ix+cxm-1 >= 0) and (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *             S1 (depth = 3) [tmp[iz][iy]=sub(mul(div(cymh[iy],cyph[iy]),Bza[iz][iy][ix]),mul(div(IntExpr(ch),cyph[iy]),clf[iz][iy]))]
	 *             ([tmp[iz][iy]]) = f([cymh[iy], cyph[iy], Bza[iz][iy][ix], cyph[iy], clf[iz][iy]])
	 *             (Domain = [cz,cym,cxm,ch,mui] -> {S1[iz,iy,ix] : (ix >= 0) and (-ix+cxm-1 >= 0) and (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *             S2 (depth = 3) [Hz[iz][iy][ix]=sub(add(mul(div(cxmh[ix],cxph[ix]),Hz[iz][iy][ix]),mul(div(mul(IntExpr(mui),czp[iz]),cxph[ix]),tmp[iz][iy])),mul(div(mul(IntExpr(mui),czm[iz]),cxph[ix]),Bza[iz][iy][ix]))]
	 *             ([Hz[iz][iy][ix]]) = f([cxmh[ix], cxph[ix], Hz[iz][iy][ix], czp[iz], cxph[ix], tmp[iz][iy], czm[iz], cxph[ix], Bza[iz][iy][ix]])
	 *             (Domain = [cz,cym,cxm,ch,mui] -> {S2[iz,iy,ix] : (ix >= 0) and (-ix+cxm-1 >= 0) and (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *             S3 (depth = 3) [Bza[iz][iy][ix]=tmp[iz][iy]]
	 *             ([Bza[iz][iy][ix]]) = f([tmp[iz][iy]])
	 *             (Domain = [cz,cym,cxm,ch,mui] -> {S3[iz,iy,ix] : (ix >= 0) and (-ix+cxm-1 >= 0) and (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *         S4 (depth = 2) [clf[iz][iy]=sub(add(sub(Ex[iz][iy][cxm],Ex[iz][iy+1][cxm]),Ry[iz][iy]),Ey[iz][iy][cxm])]
	 *         ([clf[iz][iy]]) = f([Ex[iz][iy][cxm], Ex[iz][iy+1][cxm], Ry[iz][iy], Ey[iz][iy][cxm]])
	 *         (Domain = [cz,cym,cxm,ch,mui] -> {S4[iz,iy] : (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *         S5 (depth = 2) [tmp[iz][iy]=sub(mul(div(cymh[iy],cyph[iy]),Bza[iz][iy][cxm]),mul(div(IntExpr(ch),cyph[iy]),clf[iz][iy]))]
	 *         ([tmp[iz][iy]]) = f([cymh[iy], cyph[iy], Bza[iz][iy][cxm], cyph[iy], clf[iz][iy]])
	 *         (Domain = [cz,cym,cxm,ch,mui] -> {S5[iz,iy] : (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *         S6 (depth = 2) [Hz[iz][iy][cxm]=sub(add(mul(div(cxmh[cxm],cxph[cxm]),Hz[iz][iy][cxm]),mul(div(mul(IntExpr(mui),czp[iz]),cxph[cxm]),tmp[iz][iy])),mul(div(mul(IntExpr(mui),czm[iz]),cxph[cxm]),Bza[iz][iy][cxm]))]
	 *         ([Hz[iz][iy][cxm]]) = f([cxmh[cxm], cxph[cxm], Hz[iz][iy][cxm], czp[iz], cxph[cxm], tmp[iz][iy], czm[iz], cxph[cxm], Bza[iz][iy][cxm]])
	 *         (Domain = [cz,cym,cxm,ch,mui] -> {S6[iz,iy] : (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *         S7 (depth = 2) [Bza[iz][iy][cxm]=tmp[iz][iy]]
	 *         ([Bza[iz][iy][cxm]]) = f([tmp[iz][iy]])
	 *         (Domain = [cz,cym,cxm,ch,mui] -> {S7[iz,iy] : (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *         for : 0 <= ix <= cxm-1 (stride = 1)
	 *           block : 
	 *             S8 (depth = 3) [clf[iz][iy]=sub(add(sub(Ex[iz][cym][ix],Ax[iz][ix]),Ey[iz][cym][ix+1]),Ey[iz][cym][ix])]
	 *             ([clf[iz][iy]]) = f([Ex[iz][cym][ix], Ax[iz][ix], Ey[iz][cym][ix+1], Ey[iz][cym][ix]])
	 *             (Domain = [cz,cym,cxm,ch,mui] -> {S8[iz,iy,ix] : (ix >= 0) and (-ix+cxm-1 >= 0) and (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *             S9 (depth = 3) [tmp[iz][iy]=sub(mul(div(cymh[cym],cyph[iy]),Bza[iz][iy][ix]),mul(div(IntExpr(ch),cyph[iy]),clf[iz][iy]))]
	 *             ([tmp[iz][iy]]) = f([cymh[cym], cyph[iy], Bza[iz][iy][ix], cyph[iy], clf[iz][iy]])
	 *             (Domain = [cz,cym,cxm,ch,mui] -> {S9[iz,iy,ix] : (ix >= 0) and (-ix+cxm-1 >= 0) and (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *             S10 (depth = 3) [Hz[iz][cym][ix]=sub(add(mul(div(cxmh[ix],cxph[ix]),Hz[iz][cym][ix]),mul(div(mul(IntExpr(mui),czp[iz]),cxph[ix]),tmp[iz][iy])),mul(div(mul(IntExpr(mui),czm[iz]),cxph[ix]),Bza[iz][cym][ix]))]
	 *             ([Hz[iz][cym][ix]]) = f([cxmh[ix], cxph[ix], Hz[iz][cym][ix], czp[iz], cxph[ix], tmp[iz][iy], czm[iz], cxph[ix], Bza[iz][cym][ix]])
	 *             (Domain = [cz,cym,cxm,ch,mui] -> {S10[iz,iy,ix] : (ix >= 0) and (-ix+cxm-1 >= 0) and (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *             S11 (depth = 3) [Bza[iz][cym][ix]=tmp[iz][iy]]
	 *             ([Bza[iz][cym][ix]]) = f([tmp[iz][iy]])
	 *             (Domain = [cz,cym,cxm,ch,mui] -> {S11[iz,iy,ix] : (ix >= 0) and (-ix+cxm-1 >= 0) and (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *         S12 (depth = 2) [clf[iz][iy]=sub(add(sub(Ex[iz][cym][cxm],Ax[iz][cxm]),Ry[iz][cym]),Ey[iz][cym][cxm])]
	 *         ([clf[iz][iy]]) = f([Ex[iz][cym][cxm], Ax[iz][cxm], Ry[iz][cym], Ey[iz][cym][cxm]])
	 *         (Domain = [cz,cym,cxm,ch,mui] -> {S12[iz,iy] : (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *         S13 (depth = 2) [tmp[iz][iy]=sub(mul(div(cymh[cym],cyph[cym]),Bza[iz][cym][cxm]),mul(div(IntExpr(ch),cyph[cym]),clf[iz][iy]))]
	 *         ([tmp[iz][iy]]) = f([cymh[cym], cyph[cym], Bza[iz][cym][cxm], cyph[cym], clf[iz][iy]])
	 *         (Domain = [cz,cym,cxm,ch,mui] -> {S13[iz,iy] : (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *         S14 (depth = 2) [Hz[iz][cym][cxm]=sub(add(mul(div(cxmh[cxm],cxph[cxm]),Hz[iz][cym][cxm]),mul(div(mul(IntExpr(mui),czp[iz]),cxph[cxm]),tmp[iz][iy])),mul(div(mul(IntExpr(mui),czm[iz]),cxph[cxm]),Bza[iz][cym][cxm]))]
	 *         ([Hz[iz][cym][cxm]]) = f([cxmh[cxm], cxph[cxm], Hz[iz][cym][cxm], czp[iz], cxph[cxm], tmp[iz][iy], czm[iz], cxph[cxm], Bza[iz][cym][cxm]])
	 *         (Domain = [cz,cym,cxm,ch,mui] -> {S14[iz,iy] : (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 *         S15 (depth = 2) [Bza[iz][cym][cxm]=tmp[iz][iy]]
	 *         ([Bza[iz][cym][cxm]]) = f([tmp[iz][iy]])
	 *         (Domain = [cz,cym,cxm,ch,mui] -> {S15[iz,iy] : (iy >= 0) and (-iy+cym-1 >= 0) and (iz >= 0) and (-iz+cz-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_stencils_fdtd_apml_fdtd_apml_GScopRoot_kernel_fdtd_apml_scop_0_ () {
		String path = "polybench_gecos_stencils_fdtd_apml_fdtd_apml_GScopRoot_kernel_fdtd_apml_scop_0_";
		// SCoP Extraction Data
		String domains = "[cz, cym, cxm, ch, mui] -> { S0[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S2[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S9[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S13[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S11[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S8[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S1[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S5[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S4[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S6[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S3[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S10[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S12[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S14[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S7[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S15[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz }";
		String idSchedules = "[cz, cym, cxm, ch, mui] -> { S7[iz, iy] -> [0, iz, 0, iy, 4, 0, 0]; S6[iz, iy] -> [0, iz, 0, iy, 3, 0, 0]; S1[iz, iy, ix] -> [0, iz, 0, iy, 0, ix, 1]; S9[iz, iy, ix] -> [0, iz, 0, iy, 5, ix, 1]; S2[iz, iy, ix] -> [0, iz, 0, iy, 0, ix, 2]; S14[iz, iy] -> [0, iz, 0, iy, 8, 0, 0]; S12[iz, iy] -> [0, iz, 0, iy, 6, 0, 0]; S11[iz, iy, ix] -> [0, iz, 0, iy, 5, ix, 3]; S13[iz, iy] -> [0, iz, 0, iy, 7, 0, 0]; S0[iz, iy, ix] -> [0, iz, 0, iy, 0, ix, 0]; S4[iz, iy] -> [0, iz, 0, iy, 1, 0, 0]; S10[iz, iy, ix] -> [0, iz, 0, iy, 5, ix, 2]; S3[iz, iy, ix] -> [0, iz, 0, iy, 0, ix, 3]; S5[iz, iy] -> [0, iz, 0, iy, 2, 0, 0]; S8[iz, iy, ix] -> [0, iz, 0, iy, 5, ix, 0]; S15[iz, iy] -> [0, iz, 0, iy, 9, 0, 0] }";
		String writes = "[cz, cym, cxm, ch, mui] -> { S12[iz, iy] -> clf[iz, iy]; S15[iz, iy] -> Bza[iz, cym, cxm]; S0[iz, iy, ix] -> clf[iz, iy]; S7[iz, iy] -> Bza[iz, iy, cxm]; S6[iz, iy] -> Hz[iz, iy, cxm]; S5[iz, iy] -> tmp[iz, iy]; S14[iz, iy] -> Hz[iz, cym, cxm]; S9[iz, iy, ix] -> tmp[iz, iy]; S11[iz, iy, ix] -> Bza[iz, cym, ix]; S2[iz, iy, ix] -> Hz[iz, iy, ix]; S1[iz, iy, ix] -> tmp[iz, iy]; S13[iz, iy] -> tmp[iz, iy]; S3[iz, iy, ix] -> Bza[iz, iy, ix]; S8[iz, iy, ix] -> clf[iz, iy]; S10[iz, iy, ix] -> Hz[iz, cym, ix]; S4[iz, iy] -> clf[iz, iy] }";
		String reads = "[cz, cym, cxm, ch, mui] -> { S4[iz, iy] -> Ey[iz, iy, cxm]; S14[iz, iy] -> tmp[iz, iy]; S8[iz, iy, ix] -> Ey[iz, cym, 1 + ix]; S8[iz, iy, ix] -> Ey[iz, cym, ix]; S2[iz, iy, ix] -> Hz[iz, iy, ix]; S12[iz, iy] -> Ey[iz, cym, cxm]; S11[iz, iy, ix] -> tmp[iz, iy]; S1[iz, iy, ix] -> cymh[iy]; S14[iz, iy] -> czm[iz]; S9[iz, iy, ix] -> Bza[iz, iy, ix]; S14[iz, iy] -> Hz[iz, cym, cxm]; S12[iz, iy] -> Ax[iz, cxm]; S12[iz, iy] -> Ry[iz, cym]; S13[iz, iy] -> cymh[cym]; S10[iz, iy, ix] -> czm[iz]; S6[iz, iy] -> Bza[iz, iy, cxm]; S6[iz, iy] -> Hz[iz, iy, cxm]; S6[iz, iy] -> czm[iz]; S10[iz, iy, ix] -> czp[iz]; S14[iz, iy] -> cxph[cxm]; S10[iz, iy, ix] -> Bza[iz, cym, ix]; S5[iz, iy] -> Bza[iz, iy, cxm]; S2[iz, iy, ix] -> cxph[ix]; S10[iz, iy, ix] -> cxph[ix]; S1[iz, iy, ix] -> cyph[iy]; S8[iz, iy, ix] -> Ax[iz, ix]; S9[iz, iy, ix] -> cyph[iy]; S6[iz, iy] -> tmp[iz, iy]; S9[iz, iy, ix] -> clf[iz, iy]; S5[iz, iy] -> clf[iz, iy]; S2[iz, iy, ix] -> Bza[iz, iy, ix]; S2[iz, iy, ix] -> tmp[iz, iy]; S14[iz, iy] -> czp[iz]; S14[iz, iy] -> Bza[iz, cym, cxm]; S2[iz, iy, ix] -> cxmh[ix]; S4[iz, iy] -> Ry[iz, iy]; S3[iz, iy, ix] -> tmp[iz, iy]; S14[iz, iy] -> cxmh[cxm]; S6[iz, iy] -> czp[iz]; S1[iz, iy, ix] -> clf[iz, iy]; S0[iz, iy, ix] -> Ex[iz, 1 + iy, ix]; S0[iz, iy, ix] -> Ex[iz, iy, ix]; S10[iz, iy, ix] -> cxmh[ix]; S13[iz, iy] -> clf[iz, iy]; S9[iz, iy, ix] -> cymh[cym]; S0[iz, iy, ix] -> Ey[iz, iy, 1 + ix]; S0[iz, iy, ix] -> Ey[iz, iy, ix]; S10[iz, iy, ix] -> tmp[iz, iy]; S6[iz, iy] -> cxph[cxm]; S12[iz, iy] -> Ex[iz, cym, cxm]; S8[iz, iy, ix] -> Ex[iz, cym, ix]; S15[iz, iy] -> tmp[iz, iy]; S7[iz, iy] -> tmp[iz, iy]; S2[iz, iy, ix] -> czm[iz]; S5[iz, iy] -> cymh[iy]; S13[iz, iy] -> cyph[cym]; S1[iz, iy, ix] -> Bza[iz, iy, ix]; S5[iz, iy] -> cyph[iy]; S10[iz, iy, ix] -> Hz[iz, cym, ix]; S2[iz, iy, ix] -> czp[iz]; S4[iz, iy] -> Ex[iz, 1 + iy, cxm]; S4[iz, iy] -> Ex[iz, iy, cxm]; S6[iz, iy] -> cxmh[cxm]; S13[iz, iy] -> Bza[iz, cym, cxm] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[cz, cym, cxm, ch, mui] -> { S13[iz, iy] -> S12[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S10[iz, iy, ix] -> S10[iz, -1 + iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 1 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S9[iz, iy, ix] -> S8[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S1[iz, iy, ix] -> S0[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S3[iz, iy, ix] -> S1[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S6[iz, iy] -> S5[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S5[iz, iy] -> S4[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S9[iz, iy, ix] -> S3[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S15[iz, iy] -> S13[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S7[iz, iy] -> S5[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S10[iz, iy, ix] -> S11[iz, -1 + iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 1 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S14[iz, iy] -> S14[iz, -1 + iy] : iy >= 1 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S11[iz, iy, ix] -> S9[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S13[iz, iy] -> S15[iz, -1 + iy] : iy >= 1 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S2[iz, iy, ix] -> S1[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S14[iz, iy] -> S15[iz, -1 + iy] : iy >= 1 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S10[iz, iy, ix] -> S9[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S14[iz, iy] -> S13[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz }";
		String valueBasedPlutoSchedule = "[cz, cym, cxm, ch, mui] -> { S10[iz, iy, ix] -> [iz, iy, ix, 5]; S12[iz, iy] -> [iz, iy, 0, 0]; S0[iz, iy, ix] -> [iz, iy, ix, 0]; S1[iz, iy, ix] -> [iz, iy, ix, 1]; S11[iz, iy, ix] -> [iz, iy, ix, 6]; S2[iz, iy, ix] -> [iz, iy, ix, 7]; S14[iz, iy] -> [iz, iy, 3, 0]; S13[iz, iy] -> [iz, iy, 1, 0]; S7[iz, iy] -> [iz, iy, 2, 0]; S4[iz, iy] -> [iz, iy, 0, 0]; S9[iz, iy, ix] -> [iz, iy, ix, 4]; S5[iz, iy] -> [iz, iy, 1, 0]; S3[iz, iy, ix] -> [iz, iy, ix, 2]; S15[iz, iy] -> [iz, iy, 2, 0]; S8[iz, iy, ix] -> [iz, iy, ix, 3]; S6[iz, iy] -> [iz, iy, 3, 0] }";
		String valueBasedFeautrierSchedule = "[cz, cym, cxm, ch, mui] -> { S13[iz, iy] -> [iy, 0, iz]; S10[iz, iy, ix] -> [iy, iz, ix]; S5[iz, iy] -> [iz, iy, 0]; S9[iz, iy, ix] -> [iz, iy, ix]; S3[iz, iy, ix] -> [iz, iy, ix]; S2[iz, iy, ix] -> [iz, iy, ix]; S15[iz, iy] -> [iy, 1, iz]; S0[iz, iy, ix] -> [iz, iy, ix]; S1[iz, iy, ix] -> [iz, iy, ix]; S4[iz, iy] -> [iz, iy, 0]; S6[iz, iy] -> [iz, iy, 0]; S14[iz, iy] -> [iy, iz, 0]; S7[iz, iy] -> [iz, iy, 0]; S12[iz, iy] -> [iz, iy, 0]; S8[iz, iy, ix] -> [iz, iy, ix]; S11[iz, iy, ix] -> [iz, iy, ix] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[cz, cym, cxm, ch, mui] -> { S13[iz, iy] -> S11[iz, iy, -1 + cxm] : cxm >= 1 and iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S13[iz, iy] -> S5[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz and cxm <= 0; S8[iz, iy, 0] -> S5[iz, iy] : cxm >= 1 and iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S5[iz, iy] -> S2[iz, iy, -1 + cxm] : cxm >= 1 and iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S9[iz, iy, ix] -> S11[iz, iy, -1 + ix] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym and ix >= 1 and ix <= -1 + cxm; S5[iz, iy] -> S1[iz, iy, -1 + cxm] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz and cxm >= 1; S13[iz, iy] -> S12[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S13[iz, iy] -> S15[iz, -1 + iy] : iy >= 1 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S9[iz, iy, ix] -> S3[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S13[iz, iy] -> S6[iz, iy] : cxm <= 0 and iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S11[iz, iy, ix] -> S9[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S15[iz, iy] -> S14[iz, iy] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S12[iz, iy] -> S8[iz, iy, -1 + cxm] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz and cxm >= 1; S12[iz, iy] -> S4[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz and cxm <= 0; S9[iz, iy, 0] -> S7[iz, iy] : cxm >= 1 and iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S9[iz, iy, ix] -> S9[iz, iy, -1 + ix] : ix >= 1 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S15[iz, iy] -> S15[iz, -1 + iy] : iz >= 0 and iz <= -1 + cz and iy >= 1 and iy <= -1 + cym; S12[iz, iy] -> S5[iz, iy] : cxm <= 0 and iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S4[iz, iy] -> S1[iz, iy, -1 + cxm] : cxm >= 1 and iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S8[iz, iy, 0] -> S4[iz, iy] : iz <= -1 + cz and cxm >= 1 and iy >= 0 and iy <= -1 + cym and iz >= 0; S1[iz, iy, ix] -> S1[iz, iy, -1 + ix] : ix >= 1 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S11[iz, iy, ix] -> S10[iz, iy, ix] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym and ix >= 0 and ix <= -1 + cxm; S1[iz, iy, ix] -> S3[iz, iy, -1 + ix] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym and ix >= 1 and ix <= -1 + cxm; S0[iz, iy, ix] -> S0[iz, iy, -1 + ix] : ix >= 1 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S14[iz, iy] -> S14[iz, -1 + iy] : iz >= 0 and iz <= -1 + cz and iy >= 1 and iy <= -1 + cym; S14[iz, iy] -> S13[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S1[iz, iy, ix] -> S2[iz, iy, -1 + ix] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym and ix >= 1 and ix <= -1 + cxm; S9[iz, iy, 0] -> S6[iz, iy] : cxm >= 1 and iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S9[iz, iy, 0] -> S5[iz, iy] : iz <= -1 + cz and cxm >= 1 and iy >= 0 and iy <= -1 + cym and iz >= 0; S12[iz, iy] -> S9[iz, iy, -1 + cxm] : cxm >= 1 and iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S10[iz, iy, ix] -> S11[iz, -1 + iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 1 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S2[iz, iy, ix] -> S1[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S8[iz, iy, ix] -> S9[iz, iy, -1 + ix] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym and ix >= 1 and ix <= -1 + cxm; S1[iz, iy, ix] -> S0[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S10[iz, iy, ix] -> S9[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S7[iz, iy] -> S6[iz, iy] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S3[iz, iy, ix] -> S1[iz, iy, ix] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym and ix >= 0 and ix <= -1 + cxm; S5[iz, iy] -> S4[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S3[iz, iy, ix] -> S2[iz, iy, ix] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym and ix >= 0 and ix <= -1 + cxm; S5[iz, iy] -> S3[iz, iy, -1 + cxm] : cxm >= 1 and iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S9[iz, iy, ix] -> S8[iz, iy, ix] : ix >= 0 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S13[iz, iy] -> S9[iz, iy, -1 + cxm] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz and cxm >= 1; S8[iz, iy, ix] -> S8[iz, iy, -1 + ix] : ix >= 1 and ix <= -1 + cxm and iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S13[iz, iy] -> S7[iz, iy] : cxm <= 0 and iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S11[iz, iy, ix] -> S11[iz, -1 + iy, ix] : iz >= 0 and iz <= -1 + cz and iy >= 1 and iy <= -1 + cym and ix >= 0 and ix <= -1 + cxm; S6[iz, iy] -> S5[iz, iy] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S4[iz, iy] -> S0[iz, iy, -1 + cxm] : iy >= 0 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz and cxm >= 1; S15[iz, iy] -> S13[iz, iy] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S7[iz, iy] -> S5[iz, iy] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym; S0[iz, iy, ix] -> S1[iz, iy, -1 + ix] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym and ix >= 1 and ix <= -1 + cxm; S10[iz, iy, ix] -> S10[iz, -1 + iy, ix] : iz >= 0 and iz <= -1 + cz and iy >= 1 and iy <= -1 + cym and ix >= 0 and ix <= -1 + cxm; S9[iz, iy, ix] -> S10[iz, iy, -1 + ix] : iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym and ix >= 1 and ix <= -1 + cxm; S14[iz, iy] -> S15[iz, -1 + iy] : iy >= 1 and iy <= -1 + cym and iz >= 0 and iz <= -1 + cz; S13[iz, iy] -> S10[iz, iy, -1 + cxm] : cxm >= 1 and iz >= 0 and iz <= -1 + cz and iy >= 0 and iy <= -1 + cym }";
		String memoryBasedPlutoSchedule = "[cz, cym, cxm, ch, mui] -> { S11[iz, iy, ix] -> [iz, iy, 1, 1, 1, 0, ix, 5]; S12[iz, iy] -> [iz, iy, 1, 1, 1, 1, 0, 0]; S5[iz, iy] -> [iz, iy, 1, 1, 0, 0, 0, 0]; S2[iz, iy, ix] -> [iz, iy, 0, ix, 2, 0, 0, 0]; S6[iz, iy] -> [iz, iy, 1, 1, 1, 0, 0, 0]; S1[iz, iy, ix] -> [iz, iy, 0, ix, 1, 0, 0, 0]; S15[iz, iy] -> [iz, iy, 1, 1, 1, 1, 3, 0]; S4[iz, iy] -> [iz, iy, 1, 0, 0, 0, 0, 0]; S3[iz, iy, ix] -> [iz, iy, 0, ix, 3, 0, 0, 0]; S10[iz, iy, ix] -> [iz, iy, 1, 1, 1, 0, ix, 4]; S14[iz, iy] -> [iz, iy, 1, 1, 1, 1, 2, 0]; S7[iz, iy] -> [iz, iy, 1, 1, 1, 0, 0, 1]; S0[iz, iy, ix] -> [iz, iy, 0, ix, 0, 0, 0, 0]; S9[iz, iy, ix] -> [iz, iy, 1, 1, 1, 0, ix, 3]; S13[iz, iy] -> [iz, iy, 1, 1, 1, 1, 1, 0]; S8[iz, iy, ix] -> [iz, iy, 1, 1, 1, 0, ix, 2] }";
		String memoryBasedFeautrierSchedule = "[cz, cym, cxm, ch, mui] -> { S10[iz, iy, ix] -> [4 + iy, ix, 2, iz]; S12[iz, iy] -> [5 + iy, iz, 0, 0]; S0[iz, iy, ix] -> [-3cxm + 3ix, iy, iz, 0]; S1[iz, iy, ix] -> [1 - 3cxm + 3ix, iy, iz, 0]; S11[iz, iy, ix] -> [4 + iy, 1 + ix, 0, iz]; S2[iz, iy, ix] -> [2 - 3cxm + 3ix, iy, iz, 0]; S14[iz, iy] -> [7 + 3iy, iz, 0, 0]; S13[iz, iy] -> [6 + 3iy, iz, 0, 0]; S7[iz, iy] -> [3, iz, iy, 0]; S4[iz, iy] -> [0, iz, iy, 0]; S9[iz, iy, ix] -> [4 + iy, ix, 1, iz]; S5[iz, iy] -> [1, iz, iy, 0]; S3[iz, iy, ix] -> [3 - 3cxm + 3ix, iy, iz, 0]; S15[iz, iy] -> [8 + 3iy, iz, 0, 0]; S8[iz, iy, ix] -> [4 + iy, ix, 0, iz]; S6[iz, iy] -> [2, iz, iy, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #133
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_vo_test_vo/scop_0
	 *   block : 
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= N-1 (stride = 1)
	 *         S0 (depth = 2) [tmp[i][j]=sub(mul(a[i][j],IntExpr(3)),IntExpr(4))]
	 *         ([tmp[i][j]]) = f([a[i][j]])
	 *         (Domain = [N] -> {S0[i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= N-2 (stride = 1)
	 *         S1 (depth = 2) [b[i][j]=add(mul(tmp[i][j+1],a[i][j]),IntExpr(3))]
	 *         ([b[i][j]]) = f([tmp[i][j+1], a[i][j]])
	 *         (Domain = [N] -> {S1[i,j] : (j >= 0) and (-j+N-2 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_test1_GScopRoot_test_vo_scop_0_ () {
		String path = "polymodel_others_test1_GScopRoot_test_vo_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N; S1[i, j] : j >= 0 and j <= -2 + N and i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S0[i, j] -> [0, i, 0, j, 0]; S1[i, j] -> [1, i, 0, j, 0] }";
		String writes = "[N] -> { S0[i, j] -> tmp[i, j]; S1[i, j] -> b[i, j] }";
		String reads = "[N] -> { S0[i, j] -> a[i, j]; S1[i, j] -> tmp[i, 1 + j]; S1[i, j] -> a[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S1[i, j] -> S0[i, 1 + j] : j >= 0 and j <= -2 + N and i >= 0 and i <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j, 0]; S1[i, j] -> [i, 1 + j, 1] }";
		String valueBasedFeautrierSchedule = "[N] -> { S1[i, j] -> [i, j]; S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S1[i, j] -> S0[i, 1 + j] : j >= 0 and j <= -2 + N and i >= 0 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j, 0]; S1[i, j] -> [i, 1 + j, 1] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S1[i, j] -> [i, j]; S0[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #134
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : equation_of_state_fragment_equation_of_state_fragment/scop_0
	 *   block : 
	 *     for : 1 <= l <= loop (stride = 1)
	 *       for : 0 <= k <= n-1 (stride = 1)
	 *         S0 (depth = 2) [x[k]=add(add(u[k],mul(IntExpr(r),add(z[k],mul(IntExpr(r),y[k])))),mul(IntExpr(t),add(add(u[k+3],mul(IntExpr(r),add(u[k+2],mul(IntExpr(r),u[k+1])))),mul(IntExpr(t),add(u[k+6],mul(IntExpr(r),add(u[k+5],mul(IntExpr(r),u[k+4]))))))))]
	 *         ([x[k]]) = f([u[k], z[k], y[k], u[k+3], u[k+2], u[k+1], u[k+6], u[k+5], u[k+4]])
	 *         (Domain = [loop,n,r,t] -> {S0[l,k] : (k >= 0) and (-k+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_07_equation_of_state_fragment_GScopRoot_equation_of_state_fragment_scop_0_ () {
		String path = "polymodel_src_livermore_loops_07_equation_of_state_fragment_GScopRoot_equation_of_state_fragment_scop_0_";
		// SCoP Extraction Data
		String domains = "[loop, n, r, t] -> { S0[l, k] : k >= 0 and k <= -1 + n and l >= 1 and l <= loop }";
		String idSchedules = "[loop, n, r, t] -> { S0[l, k] -> [0, l, 0, k, 0] }";
		String writes = "[loop, n, r, t] -> { S0[l, k] -> x[k] }";
		String reads = "[loop, n, r, t] -> { S0[l, k] -> z[k]; S0[l, k] -> u[6 + k]; S0[l, k] -> u[5 + k]; S0[l, k] -> u[4 + k]; S0[l, k] -> u[3 + k]; S0[l, k] -> u[2 + k]; S0[l, k] -> u[1 + k]; S0[l, k] -> u[k]; S0[l, k] -> y[k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[loop, n, r, t] -> {  }";
		String valueBasedPlutoSchedule = "[loop, n, r, t] -> { S0[l, k] -> [l, k] }";
		String valueBasedFeautrierSchedule = "[loop, n, r, t] -> { S0[l, k] -> [l, k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[loop, n, r, t] -> { S0[l, k] -> S0[-1 + l, k] : k >= 0 and k <= -1 + n and l >= 2 and l <= loop }";
		String memoryBasedPlutoSchedule = "[loop, n, r, t] -> { S0[l, k] -> [l, k] }";
		String memoryBasedFeautrierSchedule = "[loop, n, r, t] -> { S0[l, k] -> [l, k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #135
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : adi_core_adi_core/scop_0
	 *   block : 
	 *     for : 0 <= t <= tsteps-1 (stride = 1)
	 *       block : 
	 *         for : 0 <= i1 <= n-1 (stride = 1)
	 *           for : 1 <= i2 <= n-1 (stride = 1)
	 *             block : 
	 *               S0 (depth = 3) [X[i1][i2]=sub(X[i1][i2],div(mul(X[i1][i2-1],A[i1][i2]),B[i1][i2-1]))]
	 *               ([X[i1][i2]]) = f([X[i1][i2], X[i1][i2-1], A[i1][i2], B[i1][i2-1]])
	 *               (Domain = [tsteps,n] -> {S0[t,i1,i2] : (i2-1 >= 0) and (-i2+n-1 >= 0) and (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *               S1 (depth = 3) [B[i1][i2]=sub(B[i1][i2],div(mul(A[i1][i2],A[i1][i2]),B[i1][i2-1]))]
	 *               ([B[i1][i2]]) = f([B[i1][i2], A[i1][i2], A[i1][i2], B[i1][i2-1]])
	 *               (Domain = [tsteps,n] -> {S1[t,i1,i2] : (i2-1 >= 0) and (-i2+n-1 >= 0) and (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *         for : 0 <= i1 <= n-1 (stride = 1)
	 *           S2 (depth = 2) [X[i1][n-1]=div(X[i1][n-1],B[i1][n-1])]
	 *           ([X[i1][n-1]]) = f([X[i1][n-1], B[i1][n-1]])
	 *           (Domain = [tsteps,n] -> {S2[t,i1] : (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *         for : 0 <= i1 <= n-1 (stride = 1)
	 *           for : 0 <= i2 <= n-3 (stride = 1)
	 *             S3 (depth = 3) [X[i1][n-i2-2]=div(sub(X[i1][n-i2-2],mul(X[i1][n-i2-3],A[i1][n-i2-3])),B[i1][n-i2-3])]
	 *             ([X[i1][n-i2-2]]) = f([X[i1][n-i2-2], X[i1][n-i2-3], A[i1][n-i2-3], B[i1][n-i2-3]])
	 *             (Domain = [tsteps,n] -> {S3[t,i1,i2] : (i2 >= 0) and (-i2+n-3 >= 0) and (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *         for : 1 <= i1 <= n-1 (stride = 1)
	 *           for : 0 <= i2 <= n-1 (stride = 1)
	 *             block : 
	 *               S4 (depth = 3) [X[i1][i2]=sub(X[i1][i2],div(mul(X[i1-1][i2],A[i1][i2]),B[i1-1][i2]))]
	 *               ([X[i1][i2]]) = f([X[i1][i2], X[i1-1][i2], A[i1][i2], B[i1-1][i2]])
	 *               (Domain = [tsteps,n] -> {S4[t,i1,i2] : (i2 >= 0) and (-i2+n-1 >= 0) and (i1-1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *               S5 (depth = 3) [B[i1][i2]=sub(B[i1][i2],div(mul(A[i1][i2],A[i1][i2]),B[i1-1][i2]))]
	 *               ([B[i1][i2]]) = f([B[i1][i2], A[i1][i2], A[i1][i2], B[i1-1][i2]])
	 *               (Domain = [tsteps,n] -> {S5[t,i1,i2] : (i2 >= 0) and (-i2+n-1 >= 0) and (i1-1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *         for : 0 <= i1 <= n-1 (stride = 1)
	 *           S6 (depth = 2) [X[n-1][i1]=div(X[n-1][i1],B[n-1][i1])]
	 *           ([X[n-1][i1]]) = f([X[n-1][i1], B[n-1][i1]])
	 *           (Domain = [tsteps,n] -> {S6[t,i1] : (i1 >= 0) and (-i1+n-1 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *         for : 0 <= i1 <= n-3 (stride = 1)
	 *           for : 0 <= i2 <= n-1 (stride = 1)
	 *             S7 (depth = 3) [X[n-i1-2][i2]=div(sub(X[n-i1-2][i2],mul(X[n-i1-3][i2],A[n-i1-3][i2])),B[n-i1-2][i2])]
	 *             ([X[n-i1-2][i2]]) = f([X[n-i1-2][i2], X[n-i1-3][i2], A[n-i1-3][i2], B[n-i1-2][i2]])
	 *             (Domain = [tsteps,n] -> {S7[t,i1,i2] : (i2 >= 0) and (-i2+n-1 >= 0) and (i1 >= 0) and (-i1+n-3 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_adi_core_patched_GScopRoot_adi_core_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_adi_core_patched_GScopRoot_adi_core_scop_0_";
		// SCoP Extraction Data
		String domains = "[tsteps, n] -> { S6[t, i1] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S1[t, i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S5[t, i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 1 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S2[t, i1] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S0[t, i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S3[t, i1, i2] : i2 >= 0 and i2 <= -3 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S4[t, i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 1 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S7[t, i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 0 and i1 <= -3 + n and t >= 0 and t <= -1 + tsteps }";
		String idSchedules = "[tsteps, n] -> { S4[t, i1, i2] -> [0, t, 3, i1, 0, i2, 0]; S7[t, i1, i2] -> [0, t, 5, i1, 0, i2, 0]; S0[t, i1, i2] -> [0, t, 0, i1, 0, i2, 0]; S6[t, i1] -> [0, t, 4, i1, 0, 0, 0]; S2[t, i1] -> [0, t, 1, i1, 0, 0, 0]; S1[t, i1, i2] -> [0, t, 0, i1, 0, i2, 1]; S3[t, i1, i2] -> [0, t, 2, i1, 0, i2, 0]; S5[t, i1, i2] -> [0, t, 3, i1, 0, i2, 1] }";
		String writes = "[tsteps, n] -> { S0[t, i1, i2] -> X[i1, i2]; S7[t, i1, i2] -> X[-2 + n - i1, i2]; S3[t, i1, i2] -> X[i1, -2 + n - i2]; S1[t, i1, i2] -> B[i1, i2]; S5[t, i1, i2] -> B[i1, i2]; S2[t, i1] -> X[i1, -1 + n]; S4[t, i1, i2] -> X[i1, i2]; S6[t, i1] -> X[-1 + n, i1] }";
		String reads = "[tsteps, n] -> { S0[t, i1, i2] -> X[i1, i2]; S0[t, i1, i2] -> X[i1, -1 + i2]; S0[t, i1, i2] -> A[i1, i2]; S3[t, i1, i2] -> X[i1, -2 + n - i2]; S3[t, i1, i2] -> X[i1, -3 + n - i2]; S4[t, i1, i2] -> B[-1 + i1, i2]; S7[t, i1, i2] -> X[-2 + n - i1, i2]; S7[t, i1, i2] -> X[-3 + n - i1, i2]; S4[t, i1, i2] -> A[i1, i2]; S1[t, i1, i2] -> B[i1, i2]; S1[t, i1, i2] -> B[i1, -1 + i2]; S5[t, i1, i2] -> B[i1, i2]; S5[t, i1, i2] -> B[-1 + i1, i2]; S2[t, i1] -> X[i1, -1 + n]; S5[t, i1, i2] -> A[i1, i2]; S6[t, i1] -> B[-1 + n, i1]; S7[t, i1, i2] -> B[-2 + n - i1, i2]; S4[t, i1, i2] -> X[i1, i2]; S4[t, i1, i2] -> X[-1 + i1, i2]; S3[t, i1, i2] -> A[i1, -3 + n - i2]; S3[t, i1, i2] -> B[i1, -3 + n - i2]; S7[t, i1, i2] -> A[-3 + n - i1, i2]; S2[t, i1] -> B[i1, -1 + n]; S0[t, i1, i2] -> B[i1, -1 + i2]; S6[t, i1] -> X[-1 + n, i1]; S1[t, i1, i2] -> A[i1, i2] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[tsteps, n] -> { S1[t, i1, i2] -> S1[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S1[t, 0, i2] -> S1[-1 + t, 0, i2] : i2 >= 1 and i2 <= -1 + n and t <= -1 + tsteps and t >= 1; S2[t, i1] -> S0[t, i1, -1 + n] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S0[t, i1, 1] -> S5[-1 + t, i1, 0] : t <= -1 + tsteps and n >= 2 and i1 >= 1 and i1 <= -1 + n and t >= 1; S4[t, i1, i2] -> S4[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S0[t, 0, -1 + n] -> S2[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S4[t, i1, 0] -> S7[-1 + t, -2 + n - i1, 0] : t <= -1 + tsteps and t >= 1 and i1 >= 1 and i1 <= -2 + n and n >= 1; S7[t, i1, i2] -> S4[t, i1', i2] : i2 >= 0 and i2 <= -1 + n and t >= 0 and i1' >= 1 and i1' >= -3 + n - i1 and t <= -1 + tsteps and i1 >= 0 and i1' <= -2 + n - i1; S4[t, i1, i2] -> S3[t, i1, -2 + n - i2] : i2 >= 1 and i2 <= -2 + n and i1 >= 1 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S4[t, 1, i2] -> S3[t, 0, -2 + n - i2] : i2 >= 1 and t >= 0 and t <= -1 + tsteps and i2 <= -2 + n; S0[t, i1, i2] -> S1[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S3[t, -1 + n, -3 + n] -> S6[-1 + t, 0] : n >= 3 and t <= -1 + tsteps and t >= 1; S2[t, 0] -> S6[-1 + t, 0] : n = 1 and t >= 1 and t <= -1 + tsteps; S5[t, i1, i2] -> S1[t, i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 1 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S5[t, 1, i2] -> S1[t, 0, i2] : i2 >= 1 and i2 <= -1 + n and t <= -1 + tsteps and t >= 0; S3[t, i1, -3 + n] -> S7[-1 + t, -2 + n - i1, 0] : n >= 3 and t <= -1 + tsteps and i1 >= 1 and i1 <= -2 + n and t >= 1; S3[t, i1, i2] -> S1[t, i1, -3 + n - i2] : i2 >= 0 and i2 <= -4 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S0[t, -1 + n, i2] -> S6[-1 + t, i2] : i2 >= 1 and i2 <= -1 + n and t >= 1 and t <= -1 + tsteps; S0[t, -1 + n, 1] -> S6[-1 + t, 0] : t <= -1 + tsteps and n >= 2 and t >= 1; S5[t, i1, i2] -> S5[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S5[t, i1, 0] -> S5[-1 + t, i1, 0] : t <= -1 + tsteps and t >= 1 and i1 >= 1 and i1 <= -1 + n; S6[t, i1] -> S5[t, -1 + n, i1] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S2[t, i1] -> S1[t, i1, -1 + n] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S4[t, 1, i2] -> S1[t, 0, i2] : i2 >= 1 and i2 <= -1 + n and t <= -1 + tsteps and n >= 2 and t >= 0; S6[t, i1] -> S4[t, -1 + n, i1] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S6[t, 0] -> S2[t, 0] : n = 1 and t >= 0 and t <= -1 + tsteps; S7[t, -3 + n, i2] -> S3[t, 0, -2 + n - i2] : i2 >= 1 and i2 <= -2 + n and n >= 3 and t <= -1 + tsteps and t >= 0; S0[t, i1, i2] -> S7[-1 + t, -2 + n - i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 1 and i1 <= -2 + n and t >= 1 and t <= -1 + tsteps; S0[t, i1, 1] -> S7[-1 + t, -2 + n - i1, 0] : t <= -1 + tsteps and i1 >= 1 and t >= 1 and i1 <= -2 + n; S7[t, i1, i2] -> S5[t, -2 + n - i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 0 and i1 <= -3 + n and t >= 0 and t <= -1 + tsteps; S4[t, i1, -1 + n] -> S2[t, i1] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S4[t, 1, -1 + n] -> S2[t, 0] : n >= 2 and t >= 0 and t <= -1 + tsteps; S0[t, 0, i2] -> S3[-1 + t, 0, -2 + n - i2] : i2 >= 1 and t >= 1 and t <= -1 + tsteps and i2 <= -2 + n and n >= 1; S4[t, i1, i2] -> S5[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S7[t, -3 + n, -1 + n] -> S2[t, 0] : n >= 3 and t >= 0 and t <= -1 + tsteps; S3[t, i1, i2] -> S0[t, i1, i2'] : i2' <= -2 + n - i2 and i2' >= 1 and i1 <= -1 + n and t >= 0 and i2' >= -3 + n - i2 and t <= -1 + tsteps and i2 >= 0 and i1 >= 0; S0[t, i1, i2] -> S0[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S4[t, -1 + n, 0] -> S6[-1 + t, 0] : t <= -1 + tsteps and t >= 1 and n >= 2; S1[t, i1, i2] -> S5[-1 + t, i1, i2] : i2 >= 1 and i2 <= -1 + n and i1 >= 1 and i1 <= -1 + n and t >= 1 and t <= -1 + tsteps; S1[t, i1, 1] -> S5[-1 + t, i1, 0] : t <= -1 + tsteps and t >= 1 and i1 >= 1 and i1 <= -1 + n; S3[t, i1, -3 + n] -> S5[-1 + t, i1, 0] : n >= 3 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and t >= 1 }";
		String valueBasedPlutoSchedule = "[tsteps, n] -> { S5[t, i1, i2] -> [t, i1, i2, 6]; S6[t, i1] -> [t, 1 + n, 2 + i1, 7]; S0[t, i1, i2] -> [t, 2 + i1, 2 + i2, 0]; S4[t, i1, i2] -> [t, 2 + i1, 2 + i2, 3]; S1[t, i1, i2] -> [t, i1, i2, 5]; S7[t, i1, i2] -> [t, n - i1, 2 + i2, 4]; S2[t, i1] -> [t, 2 + i1, 1 + n, 2]; S3[t, i1, i2] -> [t, 2 + i1, n - i2, 1] }";
		String valueBasedFeautrierSchedule = "[tsteps, n] -> { S3[t, i1, i2] -> [n + 4t + i1 - i2, t, i2]; S7[t, i1, i2] -> [2 + n + 4t - i1 + i2, t, i2]; S2[t, i1] -> [1 + n + 4t + i1, t, 0]; S0[t, i1, i2] -> [1 + 4t + i1 + i2, t, i2]; S4[t, i1, i2] -> [3 + 4t + i1 + i2, t, i2]; S6[t, i1] -> [3 + n + 4t + i1, t, 0]; S1[t, i1, i2] -> [2t + i1 + i2, t, i2]; S5[t, i1, i2] -> [1 + 2t + i1 + i2, t, i2] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[tsteps, n] -> { S3[t, i1, i2] -> S0[t, i1, i2'] : t <= -1 + tsteps and i2' >= 1 and i2' >= -3 + n - i2 and i2 >= 0 and i2' <= -1 + n and i2 <= -3 + n and i2' <= -1 + n - i2 and t >= 0 and i1 >= 0 and i1 <= -1 + n; S2[t, i1] -> S1[t, i1, -1 + n] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S7[t, i1, i2] -> S7[t, -1 + i1, i2] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -3 + n and i2 >= 0 and i2 <= -1 + n; S4[t, i1, i2] -> S3[t, i1, -2 + n - i2] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and i2 <= -2 + n and i2 >= 1; S4[t, 1, i2] -> S3[t, 0, -2 + n - i2] : t >= 0 and t <= -1 + tsteps and i2 <= -2 + n and i2 >= 1; S4[t, i1, 0] -> S3[t, i1, -3 + n] : n >= 3 and t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S1[t, i1, i2] -> S4[-1 + t, 1 + i1, i2] : t >= 1 and t <= -1 + tsteps and i1 >= 0 and i1 <= -2 + n and i2 >= 1 and i2 <= -1 + n; S0[t, i1, 1] -> S5[-1 + t, i1, 0] : t <= -1 + tsteps and n >= 2 and i1 >= 1 and i1 <= -1 + n and t >= 1; S3[t, i1, -3 + n] -> S7[-1 + t, -2 + n - i1, 0] : n >= 3 and t <= -1 + tsteps and i1 >= 1 and i1 <= -2 + n and t >= 1; S7[t, i1, i2] -> S5[t, -2 + n - i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 0 and i1 <= -3 + n and t >= 0 and t <= -1 + tsteps; S2[t, i1] -> S0[t, i1, -1 + n] : n >= 2 and t >= 0 and t <= -1 + tsteps and i1 >= 0 and i1 <= -1 + n; S1[t, i1, i2] -> S7[-1 + t, -2 + n - i1, i2] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1 and i2 >= 1 and i2 <= -1 + n; S5[t, -1 + n, 0] -> S6[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S0[t, i1, i2] -> S7[-1 + t, -2 + n - i1, i2] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1 and i2 >= 1 and i2 <= -1 + n; S0[t, 0, i2] -> S7[-1 + t, -3 + n, i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -2 + n; S0[t, i1, 1] -> S7[-1 + t, -2 + n - i1, 0] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1; S0[t, 0, -1 + n] -> S7[-1 + t, -3 + n, -1 + n] : n >= 3 and t >= 1 and t <= -1 + tsteps; S4[t, -1 + n, 0] -> S6[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S4[t, 1, i2] -> S1[t, 0, i2] : i2 >= 1 and i2 <= -1 + n and t <= -1 + tsteps and n >= 2 and t >= 0; S1[t, 0, i2] -> S0[-1 + t, 0, 1 + i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -2 + n and n >= 1; S2[t, 0] -> S6[-1 + t, 0] : n = 1 and t >= 1 and t <= -1 + tsteps; S0[t, i1, i2] -> S1[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S0[t, 0, i2] -> S3[-1 + t, 0, -2 + n - i2] : t >= 1 and t <= -1 + tsteps and i2 <= -2 + n and i2 >= 1; S6[t, 0] -> S2[t, 0] : n = 1 and t >= 0 and t <= -1 + tsteps; S4[t, i1, -1 + n] -> S2[t, i1] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S4[t, 1, -1 + n] -> S2[t, 0] : n >= 2 and t >= 0 and t <= -1 + tsteps; S0[t, 0, -1 + n] -> S2[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S5[t, i1, 0] -> S7[-1 + t, -2 + n - i1, 0] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1 and n >= 1; S5[t, i1, i2] -> S1[t, i1, i2'] : i2' >= i2 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and i2' >= 1 and i2' <= -1 + n and i2' <= 1 + i2 and t >= 0; S5[t, 1, i2] -> S1[t, 0, i2] : t >= 0 and t <= -1 + tsteps and i2 >= 1 and i2 <= -1 + n; S7[t, -3 + n, i2] -> S3[t, 0, -2 + n - i2] : i2 >= 1 and i2 <= -2 + n and n >= 3 and t <= -1 + tsteps and t >= 0; S1[t, 0, -1 + n] -> S2[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S4[t, i1, 0] -> S0[t, i1, 1] : n >= 2 and t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S7[t, i1, i2] -> S4[t, i1', i2] : t <= -1 + tsteps and i1' >= 1 and i1' >= -3 + n - i1 and i1 >= 0 and i2 >= 0 and i1' <= -1 + n and i1' <= -1 + n - i1 and i2 <= -1 + n and t >= 0 and i1 <= -3 + n; S4[t, i1, i2] -> S4[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S1[t, -1 + n, i2] -> S6[-1 + t, i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -1 + n and n >= 1; S6[t, i1] -> S5[t, -1 + n, i1] : i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps and n >= 2; S5[t, i1, i2] -> S3[t, i1, -3 + n - i2] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and i2 <= -3 + n and i2 >= 0; S5[t, i1, i2] -> S5[t, -1 + i1, i2] : t >= 0 and t <= -1 + tsteps and i1 >= 2 and i1 <= -1 + n and i2 >= 0 and i2 <= -1 + n; S5[t, i1, 0] -> S5[-1 + t, i1', 0] : t >= 1 and t <= -1 + tsteps and i1' <= -1 + n and i1 >= 1 and i1' >= i1 and i1' <= 1 + i1; S7[t, -3 + n, -1 + n] -> S2[t, 0] : n >= 3 and t >= 0 and t <= -1 + tsteps; S5[t, i1, -1 + n] -> S2[t, i1] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and n >= 1; S5[t, i1, i2] -> S0[t, i1, 1 + i2] : t >= 0 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and i2 >= 0 and i2 <= -2 + n; S6[t, i1] -> S4[t, -1 + n, i1] : n >= 2 and t >= 0 and t <= -1 + tsteps and i1 >= 0 and i1 <= -1 + n; S3[t, -1 + n, -3 + n] -> S6[-1 + t, 0] : n >= 3 and t <= -1 + tsteps and t >= 1; S0[t, 0, i2] -> S4[-1 + t, 1, i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -1 + n; S1[t, 0, i2] -> S1[-1 + t, 0, i2'] : t >= 1 and t <= -1 + tsteps and i2' <= -1 + n and i2 >= 1 and i2' >= i2 and i2' <= 1 + i2; S1[t, i1, i2] -> S1[t, i1, -1 + i2] : t >= 0 and t <= -1 + tsteps and i1 >= 0 and i1 <= -1 + n and i2 >= 2 and i2 <= -1 + n; S0[t, i1, i2] -> S0[t, i1, -1 + i2] : i2 >= 2 and i2 <= -1 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S3[t, i1, i2] -> S1[t, i1, -3 + n - i2] : i2 >= 0 and i2 <= -4 + n and i1 >= 0 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S1[t, 0, i2] -> S3[-1 + t, 0, -3 + n - i2] : t >= 1 and t <= -1 + tsteps and i2 <= -3 + n and i2 >= 1 and n >= 1; S1[t, i1, i2] -> S5[-1 + t, i1', i2] : t >= 1 and t <= -1 + tsteps and i1' >= 1 and i1' <= -1 + n and i2 >= 1 and i2 <= -1 + n and i1' >= i1 and i1' <= 1 + i1; S1[t, i1, 1] -> S5[-1 + t, i1, 0] : t >= 1 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n; S3[t, i1, i2] -> S3[t, i1, -1 + i2] : t >= 0 and t <= -1 + tsteps and i1 >= 0 and i1 <= -1 + n and i2 >= 1 and i2 <= -3 + n; S0[t, -1 + n, i2] -> S6[-1 + t, i2] : t >= 1 and t <= -1 + tsteps and i2 >= 1 and i2 <= -1 + n; S0[t, -1 + n, 1] -> S6[-1 + t, 0] : n >= 2 and t >= 1 and t <= -1 + tsteps; S4[t, i1, i2] -> S5[t, -1 + i1, i2] : i2 >= 0 and i2 <= -1 + n and i1 >= 2 and i1 <= -1 + n and t >= 0 and t <= -1 + tsteps; S5[t, i1, 0] -> S4[-1 + t, 1 + i1, 0] : t >= 1 and t <= -1 + tsteps and i1 >= 1 and i1 <= -2 + n and n >= 1; S3[t, i1, -3 + n] -> S5[-1 + t, i1, 0] : n >= 3 and t <= -1 + tsteps and i1 >= 1 and i1 <= -1 + n and t >= 1; S4[t, i1, 0] -> S7[-1 + t, -2 + n - i1, 0] : t >= 1 and t <= -1 + tsteps and i1 <= -2 + n and i1 >= 1 }";
		String memoryBasedPlutoSchedule = "[tsteps, n] -> { S5[t, i1, i2] -> [t, i1, i2, 5]; S6[t, i1] -> [t, n, i1, 7]; S0[t, i1, i2] -> [t, i1, -2n + i2, 3]; S4[t, i1, i2] -> [t, i1, i2, 1]; S1[t, i1, i2] -> [t, i1, -2n + i2, 6]; S7[t, i1, i2] -> [t, n + i1, i2, 0]; S2[t, i1] -> [t, i1, 0, 4]; S3[t, i1, i2] -> [t, i1, -n + i2, 2] }";
		String memoryBasedFeautrierSchedule = "[tsteps, n] -> { S5[t, i1, i2] -> [t, 3, i1, i2]; S6[t, i1] -> [t, 5, i1, 0]; S0[t, i1, i2] -> [t, 1, i2, i1]; S4[t, i1, i2] -> [t, 4, i1, i2]; S1[t, i1, i2] -> [t, 0, i2, i1]; S7[t, i1, i2] -> [t, 5, i1, i2]; S2[t, i1] -> [t, 2, i1, 0]; S3[t, i1, i2] -> [t, 2, i2, i1] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #136
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : foo_foo/scop_0
	 *   block : 
	 *     S0 (depth = 0) [a=IntExpr(10)]
	 *     ([a]) = f([])
	 *     (Domain = [N] -> {S0[] : })
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       block : 
	 *         S1 (depth = 1) [a=IntExpr(i)]
	 *         ([a]) = f([])
	 *         (Domain = [N] -> {S1[i] : (i >= 0) and (-i+N-1 >= 0)})
	 *         for : 0 <= j <= i-1 (stride = 1)
	 *           block : 
	 *             S2 (depth = 2) [a=x[j-N]]
	 *             ([a]) = f([x[j-N]])
	 *             (Domain = [N] -> {S2[i,j] : (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 *             if : (j+2*i+8 >= 0) and (N > 0)
	 *               S3 (depth = 2) [x[7*i+2][i]=add(x[i][0],a)]
	 *               ([x[7*i+2][i]]) = f([x[i][0], a])
	 *               (Domain = [N] -> {S3[i,j] : (2*i+j+8 >= 0) and (N-1 >= 0) and (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_example_GScopRoot_foo_scop_0_ () {
		String path = "polymodel_src_generic_for_loops_example_GScopRoot_foo_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S1[i] : i >= 0 and i <= -1 + N; S0[]; S3[i, j] : j >= -8 - 2i and N >= 1 and j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N; S2[i, j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N }";
		String idSchedules = "[N] -> { S2[i, j] -> [1, i, 1, j, 0]; S1[i] -> [1, i, 0, 0, 0]; S0[] -> [0, 0, 0, 0, 0]; S3[i, j] -> [1, i, 1, j, 1] }";
		String writes = "[N] -> { S3[i, j] -> x[2 + 7i, i]; S2[i, j] -> a[]; S0[] -> a[]; S1[i] -> a[] }";
		String reads = "[N] -> { S3[i, j] -> x[i, 0]; S3[i, j] -> a[]; S2[i, j] -> x[-N + j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S3[i, j] -> S2[i, j] : j >= -8 - 2i and N >= 1 and j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N }";
		String valueBasedPlutoSchedule = "[N] -> { S0[] -> [0, 0, 0]; S1[i] -> [i, 0, 0]; S2[i, j] -> [i, j, 0]; S3[i, j] -> [i, j, 1] }";
		String valueBasedFeautrierSchedule = "[N] -> { S3[i, j] -> [i, j]; S1[i] -> [i, 0]; S2[i, j] -> [i, j]; S0[] -> [0, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S2[i, j] -> S3[i, -1 + j] : i <= -1 + N and j >= 1 and j <= -1 + i and i >= 0; S1[i] -> S3[-1 + i, -2 + i] : i >= 2 and i <= -1 + N; S1[1] -> S1[0] : N >= 2; S1[i] -> S2[-1 + i, -2 + i] : i >= 2 and i <= -1 + N and N >= 1; S2[i, j] -> S2[i, -1 + j] : i <= -1 + N and j <= -1 + i and j >= 1 and N >= 1 and j >= -7 - 2i and i >= 0; S1[0] -> S0[] : N >= 1; S2[i, 0] -> S1[i] : i <= -1 + N and i >= 1; S3[i, j] -> S3[i, -1 + j] : j <= -1 + i and j >= 1 and i <= -1 + N and j >= -8 - 2i and N >= 1 and i >= 0; S3[i, j] -> S2[i, j] : j >= -8 - 2i and N >= 1 and j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[] -> [0, 0, 0]; S1[i] -> [i, 0, 1]; S2[i, j] -> [i, j, 2]; S3[i, j] -> [i, j, 3] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S3[i, j] -> [1 + i, 0, j, 1]; S2[i, j] -> [1 + i, 0, j, 0]; S1[i] -> [i, 1, 0, 0]; S0[] -> [0, 0, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #137
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : _2_D_implicit_hydrodynamics_fragment__2_D_implicit_hydrodynamics_fragment/scop_0
	 *   block : 
	 *     for : 1 <= l <= loop (stride = 1)
	 *       for : 1 <= j <= 5 (stride = 1)
	 *         for : 1 <= k <= n-1 (stride = 1)
	 *           block : 
	 *             S0 (depth = 3) [qa=add(add(add(add(mul(za[j+1][k],zr[j][k]),mul(za[j-1][k],zb[j][k])),mul(za[j][k+1],zu[j][k])),mul(za[j][k-1],zv[j][k])),zz[j][k])]
	 *             ([qa]) = f([za[j+1][k], zr[j][k], za[j-1][k], zb[j][k], za[j][k+1], zu[j][k], za[j][k-1], zv[j][k], zz[j][k]])
	 *             (Domain = [loop,n] -> {S0[l,j,k] : (k-1 >= 0) and (-k+n-1 >= 0) and (j-1 >= 0) and (-j+5 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 *             S1 (depth = 3) [za[j][k]=add(za[j][k],mul(0.17499999701976776,sub(qa,za[j][k])))]
	 *             ([za[j][k]]) = f([za[j][k], qa, za[j][k]])
	 *             (Domain = [loop,n] -> {S1[l,j,k] : (k-1 >= 0) and (-k+n-1 >= 0) and (j-1 >= 0) and (-j+5 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_23_2_D_implicit_hydrodynamics_fragment_GScopRoot__2_D_implicit_hydrodynamics_fragment_scop_0_ () {
		String path = "polymodel_src_livermore_loops_23_2_D_implicit_hydrodynamics_fragment_GScopRoot__2_D_implicit_hydrodynamics_fragment_scop_0_";
		// SCoP Extraction Data
		String domains = "[loop, n] -> { S0[l, j, k] : k >= 1 and k <= -1 + n and j >= 1 and j <= 5 and l >= 1 and l <= loop; S1[l, j, k] : k >= 1 and k <= -1 + n and j >= 1 and j <= 5 and l >= 1 and l <= loop }";
		String idSchedules = "[loop, n] -> { S1[l, j, k] -> [0, l, 0, j, 0, k, 1]; S0[l, j, k] -> [0, l, 0, j, 0, k, 0] }";
		String writes = "[loop, n] -> { S0[l, j, k] -> qa[]; S1[l, j, k] -> za[j, k] }";
		String reads = "[loop, n] -> { S0[l, j, k] -> zv[j, k]; S1[l, j, k] -> qa[]; S0[l, j, k] -> za[j, 1 + k]; S0[l, j, k] -> za[1 + j, k]; S0[l, j, k] -> za[-1 + j, k]; S0[l, j, k] -> za[j, -1 + k]; S0[l, j, k] -> zu[j, k]; S0[l, j, k] -> zz[j, k]; S1[l, j, k] -> za[j, k]; S0[l, j, k] -> zb[j, k]; S0[l, j, k] -> zr[j, k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[loop, n] -> { S1[l, j, k] -> S1[-1 + l, j, k] : k >= 1 and k <= -1 + n and j >= 1 and j <= 5 and l >= 2 and l <= loop; S0[l, j, k] -> S1[-1 + l, j, 1 + k] : k >= 1 and k <= -2 + n and j >= 1 and j <= 5 and l >= 2 and l <= loop; S0[l, j, k] -> S1[-1 + l, 1 + j, k] : k >= 1 and k <= -1 + n and j >= 1 and j <= 4 and l >= 2 and l <= loop; S0[l, j, k] -> S1[l, -1 + j, k] : k >= 1 and k <= -1 + n and j >= 2 and j <= 5 and l >= 1 and l <= loop; S0[l, j, k] -> S1[l, j, -1 + k] : k >= 2 and k <= -1 + n and j >= 1 and j <= 5 and l >= 1 and l <= loop; S1[l, j, k] -> S0[l, j, k] : k >= 1 and k <= -1 + n and j >= 1 and j <= 5 and l >= 1 and l <= loop }";
		String valueBasedPlutoSchedule = "[loop, n] -> { S1[l, j, k] -> [l, l + j, l + j + k, 1]; S0[l, j, k] -> [l, l + j, l + j + k, 0] }";
		String valueBasedFeautrierSchedule = "[loop, n] -> { S1[l, j, k] -> [2l + j + k, 1, l, k]; S0[l, j, k] -> [2l + j + k, 0, l, k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[loop, n] -> { S1[l, j, k] -> S1[-1 + l, j, k] : l >= 2 and l <= loop and j <= 5 and j >= 1 and k >= 1 and k <= -1 + n; S1[l, j, k] -> S0[l, j', k'] : l >= 1 and l <= loop and k' >= -1 + j + k - j' and j' <= j and k' >= 1 and j <= 5 and k <= -1 + n and j' >= 1 and k' <= k; S1[l, j, k] -> S0[-1 + l, j, 1 + k] : l >= 2 and l <= loop and j <= 5 and j >= 1 and k >= 1 and k <= -2 + n; S1[l, j, k] -> S0[-1 + l, 1 + j, k] : l >= 2 and l <= loop and j <= 4 and j >= 1 and k >= 1 and k <= -1 + n; S0[l, j, k] -> S1[-1 + l, j, 1 + k] : l >= 2 and l <= loop and j <= 5 and j >= 1 and k >= 1 and k <= -2 + n; S0[l, j, k] -> S1[-1 + l, 1 + j, k] : l >= 2 and l <= loop and j <= 4 and j >= 1 and k >= 1 and k <= -1 + n; S0[l, j, k] -> S1[l, -1 + j, k] : l >= 1 and l <= loop and j <= 5 and j >= 2 and k >= 1 and k <= -1 + n; S0[l, j, k] -> S1[l, j, -1 + k] : l >= 1 and l <= loop and j <= 5 and j >= 1 and k >= 2 and k <= -1 + n; S0[l, j, 1] -> S1[l, -1 + j, -1 + n] : n >= 2 and l >= 1 and l <= loop and j <= 5 and j >= 2; S0[l, 1, 1] -> S1[-1 + l, 5, -1 + n] : n >= 2 and l >= 2 and l <= loop; S0[l, j, k] -> S0[l, j, -1 + k] : k >= 2 and k <= -1 + n and j >= 1 and j <= 5 and l >= 1 and l <= loop; S0[l, j, 1] -> S0[l, -1 + j, -1 + n] : l <= loop and n >= 2 and j >= 2 and j <= 5 and l >= 1; S0[l, 1, 1] -> S0[-1 + l, 5, -1 + n] : l <= loop and n >= 2 and l >= 2 }";
		String memoryBasedPlutoSchedule = "[loop, n] -> { S1[l, j, k] -> [l, 4l + j, k, 1]; S0[l, j, k] -> [l, 4l + j, k, 0] }";
		String memoryBasedFeautrierSchedule = "[loop, n] -> { S1[l, j, k] -> [5l + j, k, 1, l]; S0[l, j, k] -> [5l + j, k, 0, l] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #138
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : first_sum_first_sum/scop_0
	 *   block : 
	 *     for : 1 <= l <= loop (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [x[0]=y[0]]
	 *         ([x[0]]) = f([y[0]])
	 *         (Domain = [loop,n] -> {S0[l] : (l-1 >= 0) and (-l+loop >= 0)})
	 *         for : 1 <= k <= n-1 (stride = 1)
	 *           S1 (depth = 2) [x[k]=add(x[k-1],y[k])]
	 *           ([x[k]]) = f([x[k-1], y[k]])
	 *           (Domain = [loop,n] -> {S1[l,k] : (k-1 >= 0) and (-k+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_11_first_sum_GScopRoot_first_sum_scop_0_ () {
		String path = "polymodel_src_livermore_loops_11_first_sum_GScopRoot_first_sum_scop_0_";
		// SCoP Extraction Data
		String domains = "[loop, n] -> { S0[l] : l >= 1 and l <= loop; S1[l, k] : k >= 1 and k <= -1 + n and l >= 1 and l <= loop }";
		String idSchedules = "[loop, n] -> { S0[l] -> [0, l, 0, 0, 0]; S1[l, k] -> [0, l, 1, k, 0] }";
		String writes = "[loop, n] -> { S1[l, k] -> x[k]; S0[l] -> x[0] }";
		String reads = "[loop, n] -> { S1[l, k] -> y[k]; S0[l] -> y[0]; S1[l, k] -> x[-1 + k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[loop, n] -> { S1[l, 1] -> S0[l] : l <= loop and n >= 2 and l >= 1; S1[l, k] -> S1[l, -1 + k] : k >= 2 and k <= -1 + n and l >= 1 and l <= loop }";
		String valueBasedPlutoSchedule = "[loop, n] -> { S1[l, k] -> [l, l + k, 0]; S0[l] -> [0, l, 1] }";
		String valueBasedFeautrierSchedule = "[loop, n] -> { S1[l, k] -> [k, l]; S0[l] -> [0, l] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[loop, n] -> { S1[l, 1] -> S0[l] : l <= loop and n >= 2 and l >= 1; S0[l] -> S0[-1 + l] : l >= 2 and l <= loop; S1[l, k] -> S1[-1 + l, k'] : l >= 2 and l <= loop and k' <= 1 + k and k >= 1 and k' >= k and k' <= -1 + n; S1[l, k] -> S1[l, -1 + k] : l >= 1 and l <= loop and k >= 2 and k <= -1 + n; S0[l] -> S1[-1 + l, 1] : n >= 2 and l >= 2 and l <= loop }";
		String memoryBasedPlutoSchedule = "[loop, n] -> { S1[l, k] -> [l, l + k, 0]; S0[l] -> [l, l, 1] }";
		String memoryBasedFeautrierSchedule = "[loop, n] -> { S1[l, k] -> [2l + k, l]; S0[l] -> [2l, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #139
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_jacobi_1d_imper_kernel_jacobi_1d_imper/scop_0
	 *   for : 0 <= t <= tsteps-1 (stride = 1)
	 *     block : 
	 *       for : 1 <= i <= n-2 (stride = 1)
	 *         S0 (depth = 2) [B[i]=mul(0.3333300054073334,add(add(A[i-1],A[i]),A[i+1]))]
	 *         ([B[i]]) = f([A[i-1], A[i], A[i+1]])
	 *         (Domain = [tsteps,n] -> {S0[t,i] : (i-1 >= 0) and (-i+n-2 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 *       for : 1 <= j <= n-2 (stride = 1)
	 *         S1 (depth = 2) [A[j]=B[j]]
	 *         ([A[j]]) = f([B[j]])
	 *         (Domain = [tsteps,n] -> {S1[t,j] : (j-1 >= 0) and (-j+n-2 >= 0) and (t >= 0) and (-t+tsteps-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_stencils_jacobi_1d_imper_jacobi_1d_imper_GScopRoot_kernel_jacobi_1d_imper_scop_0_ () {
		String path = "polybench_gecos_stencils_jacobi_1d_imper_jacobi_1d_imper_GScopRoot_kernel_jacobi_1d_imper_scop_0_";
		// SCoP Extraction Data
		String domains = "[tsteps, n] -> { S0[t, i] : i >= 1 and i <= -2 + n and t >= 0 and t <= -1 + tsteps; S1[t, j] : j >= 1 and j <= -2 + n and t >= 0 and t <= -1 + tsteps }";
		String idSchedules = "[tsteps, n] -> { S1[t, j] -> [0, t, 1, j, 0]; S0[t, i] -> [0, t, 0, i, 0] }";
		String writes = "[tsteps, n] -> { S1[t, j] -> A[j]; S0[t, i] -> B[i] }";
		String reads = "[tsteps, n] -> { S1[t, j] -> B[j]; S0[t, i] -> A[1 + i]; S0[t, i] -> A[i]; S0[t, i] -> A[-1 + i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[tsteps, n] -> { S0[t, i] -> S1[-1 + t, j] : j >= 1 and t >= 1 and t <= -1 + tsteps and j >= -1 + i and i <= -2 + n and j <= -2 + n and j <= 1 + i and i >= 1; S1[t, j] -> S0[t, j] : j >= 1 and j <= -2 + n and t >= 0 and t <= -1 + tsteps }";
		String valueBasedPlutoSchedule = "[tsteps, n] -> { S0[t, i] -> [t, t + i, 0]; S1[t, j] -> [t, t + j, 1] }";
		String valueBasedFeautrierSchedule = "[tsteps, n] -> { S0[t, i] -> [t, 0, i]; S1[t, j] -> [t, 1, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[tsteps, n] -> { S1[t, j] -> S0[t, i] : t <= -1 + tsteps and i <= -2 + n and j >= 1 and i <= 1 + j and i >= -1 + j and j <= -2 + n and t >= 0 and i >= 1; S1[t, j] -> S1[-1 + t, j] : t >= 1 and t <= -1 + tsteps and j >= 1 and j <= -2 + n; S0[t, i] -> S0[-1 + t, i] : i >= 1 and i <= -2 + n and t >= 1 and t <= -1 + tsteps; S0[t, i] -> S1[-1 + t, j] : t >= 1 and t <= -1 + tsteps and j <= -2 + n and j <= 1 + i and i >= 1 and j >= -1 + i and i <= -2 + n and j >= 1 }";
		String memoryBasedPlutoSchedule = "[tsteps, n] -> { S0[t, i] -> [t, 2t + i, 0]; S1[t, j] -> [t, 1 + 2t + j, 1] }";
		String memoryBasedFeautrierSchedule = "[tsteps, n] -> { S0[t, i] -> [t, 0, i]; S1[t, j] -> [t, 1, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #140
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : matrix_product_test_fada_matrix_product_test_fada/scop_0
	 *   block : 
	 *     if : (M > 0) and (N > 0) and (P > 0)
	 *       for : 0 <= i <= M-1 (stride = 1)
	 *         for : 0 <= j <= N-1 (stride = 1)
	 *           block : 
	 *             S0 (depth = 2) [s=IntExpr(0)]
	 *             ([s]) = f([])
	 *             (Domain = [M,N,P] -> {S0[i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+M-1 >= 0) and (M-1 >= 0) and (N-1 >= 0) and (P-1 >= 0)})
	 *             for : 0 <= k <= P-1 (stride = 1)
	 *               S1 (depth = 3) [s=add(s,mul(A[i][k],B[k][j]))]
	 *               ([s]) = f([s, A[i][k], B[k][j]])
	 *               (Domain = [M,N,P] -> {S1[i,j,k] : (k >= 0) and (-k+P-1 >= 0) and (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+M-1 >= 0) and (M-1 >= 0) and (N-1 >= 0) and (P-1 >= 0)})
	 *             S2 (depth = 2) [res[i][j]=s]
	 *             ([res[i][j]]) = f([s])
	 *             (Domain = [M,N,P] -> {S2[i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+M-1 >= 0) and (M-1 >= 0) and (N-1 >= 0) and (P-1 >= 0)})
	 *     S3 (depth = 0) [a=s]
	 *     ([a]) = f([s])
	 *     (Domain = [M,N,P] -> {S3[] : })
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_corner_cases_testFada_GScopRoot_matrix_product_test_fada_scop_0_ () {
		String path = "polymodel_src_corner_cases_testFada_GScopRoot_matrix_product_test_fada_scop_0_";
		// SCoP Extraction Data
		String domains = "[M, N, P] -> { S1[i, j, k] : k >= 0 and k <= -1 + P and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + M and M >= 1 and N >= 1 and P >= 1; S2[i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + M and M >= 1 and N >= 1 and P >= 1; S0[i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + M and M >= 1 and N >= 1 and P >= 1; S3[] }";
		String idSchedules = "[M, N, P] -> { S3[] -> [1, 0, 0, 0, 0, 0, 0]; S0[i, j] -> [0, i, 0, j, 0, 0, 0]; S1[i, j, k] -> [0, i, 0, j, 1, k, 0]; S2[i, j] -> [0, i, 0, j, 2, 0, 0] }";
		String writes = "[M, N, P] -> { S3[] -> a[]; S2[i, j] -> res[i, j]; S1[i, j, k] -> s[]; S0[i, j] -> s[] }";
		String reads = "[M, N, P] -> { S1[i, j, k] -> A[i, k]; S3[] -> s[]; S1[i, j, k] -> B[k, j]; S1[i, j, k] -> s[]; S2[i, j] -> s[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[M, N, P] -> { S3[] -> S1[-1 + M, -1 + N, -1 + P] : P >= 1 and N >= 1 and M >= 1; S1[i, j, 0] -> S0[i, j] : P >= 1 and N >= 1 and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + M and M >= 1; S2[i, j] -> S1[i, j, -1 + P] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + M and M >= 1 and N >= 1 and P >= 1; S1[i, j, k] -> S1[i, j, -1 + k] : k >= 1 and k <= -1 + P and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + M and M >= 1 and N >= 1 and P >= 1 }";
		String valueBasedPlutoSchedule = "[M, N, P] -> { S1[i, j, k] -> [i, j, k, 3]; S2[i, j] -> [i, j, P, 2]; S0[i, j] -> [0, -M + i, -N + j, 1]; S3[] -> [M, N, P, 0] }";
		String valueBasedFeautrierSchedule = "[M, N, P] -> { S2[i, j] -> [1 + P, i, j]; S0[i, j] -> [0, i, j]; S3[] -> [1 + P, 0, 0]; S1[i, j, k] -> [1 + k, j, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[M, N, P] -> { S0[i, j] -> S2[i, -1 + j] : P >= 1 and i >= 0 and i <= -1 + M and j >= 1 and j <= -1 + N; S0[i, 0] -> S2[-1 + i, -1 + N] : N >= 1 and P >= 1 and i >= 1 and i <= -1 + M; S0[i, j] -> S1[i, -1 + j, -1 + P] : P >= 1 and j <= -1 + N and i >= 0 and i <= -1 + M and j >= 1; S0[i, 0] -> S1[-1 + i, -1 + N, -1 + P] : P >= 1 and N >= 1 and i >= 1 and i <= -1 + M; S2[i, j] -> S1[i, j, -1 + P] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + M and M >= 1 and N >= 1 and P >= 1; S1[i, j, 0] -> S0[i, j] : P >= 1 and i >= 0 and i <= -1 + M and j >= 0 and j <= -1 + N; S1[i, j, k] -> S1[i, j, -1 + k] : i >= 0 and i <= -1 + M and j >= 0 and j <= -1 + N and k >= 1 and k <= -1 + P; S3[] -> S1[-1 + M, -1 + N, -1 + P] : P >= 1 and N >= 1 and M >= 1 }";
		String memoryBasedPlutoSchedule = "[M, N, P] -> { S1[i, j, k] -> [i, j, k, 2]; S2[i, j] -> [i, j, P, 1]; S0[i, j] -> [i, j, 0, 0]; S3[] -> [M, 0, 0, 0] }";
		String memoryBasedFeautrierSchedule = "[M, N, P] -> { S1[i, j, k] -> [i, j, 1, k]; S2[i, j] -> [i, j, 2, 0]; S0[i, j] -> [i, j, 0, 0]; S3[] -> [M, 0, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #141
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_trisolv_kernel_trisolv/scop_0
	 *   for : 0 <= i <= n-1 (stride = 1)
	 *     block : 
	 *       S0 (depth = 1) [x[i]=c[i]]
	 *       ([x[i]]) = f([c[i]])
	 *       (Domain = [n] -> {S0[i] : (i >= 0) and (-i+n-1 >= 0)})
	 *       for : 0 <= j <= i-1 (stride = 1)
	 *         S1 (depth = 2) [x[i]=sub(x[i],mul(A[i][j],x[j]))]
	 *         ([x[i]]) = f([x[i], A[i][j], x[j]])
	 *         (Domain = [n] -> {S1[i,j] : (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *       S2 (depth = 1) [x[i]=div(x[i],A[i][i])]
	 *       ([x[i]]) = f([x[i], A[i][i]])
	 *       (Domain = [n] -> {S2[i] : (i >= 0) and (-i+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_trisolv_trisolv_GScopRoot_kernel_trisolv_scop_0_ () {
		String path = "polybench_gecos_linear_algebra_kernels_trisolv_trisolv_GScopRoot_kernel_trisolv_scop_0_";
		// SCoP Extraction Data
		String domains = "[n] -> { S1[i, j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + n; S0[i] : i >= 0 and i <= -1 + n; S2[i] : i >= 0 and i <= -1 + n }";
		String idSchedules = "[n] -> { S0[i] -> [0, i, 0, 0, 0]; S1[i, j] -> [0, i, 1, j, 0]; S2[i] -> [0, i, 2, 0, 0] }";
		String writes = "[n] -> { S2[i] -> x[i]; S1[i, j] -> x[i]; S0[i] -> x[i] }";
		String reads = "[n] -> { S2[i] -> x[i]; S0[i] -> c[i]; S1[i, j] -> A[i, j]; S1[i, j] -> x[i]; S1[i, j] -> x[j]; S2[i] -> A[i, i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n] -> { S2[i] -> S1[i, -1 + i] : i >= 1 and i <= -1 + n; S1[i, 0] -> S0[i] : i <= -1 + n and i >= 1; S1[i, j] -> S2[j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + n; S2[0] -> S0[0] : n >= 1; S1[i, j] -> S1[i, -1 + j] : i <= -1 + n and j <= -1 + i and j >= 1 and i >= 0 }";
		String valueBasedPlutoSchedule = "[n] -> { S2[i] -> [i, 2i, 1]; S0[i] -> [0, i, 0]; S1[i, j] -> [i, i + j, 2] }";
		String valueBasedFeautrierSchedule = "[n] -> { S1[i, j] -> [1 + i + j, j]; S0[i] -> [0, i]; S2[i] -> [1 + 2i, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n] -> { S2[i] -> S1[i, -1 + i] : i >= 1 and i <= -1 + n; S1[i, 0] -> S0[i] : i >= 1 and i <= -1 + n; S1[i, j] -> S2[j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + n; S2[0] -> S0[0] : n >= 1; S1[i, j] -> S1[i, -1 + j] : i <= -1 + n and j >= 1 and j <= -1 + i }";
		String memoryBasedPlutoSchedule = "[n] -> { S2[i] -> [i, 2i, 1]; S0[i] -> [0, i, 0]; S1[i, j] -> [i, i + j, 2] }";
		String memoryBasedFeautrierSchedule = "[n] -> { S1[i, j] -> [1 + i + j, j]; S0[i] -> [0, i]; S2[i] -> [1 + 2i, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #142
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_correlation_scop_new
	 *   block : 
	 *     S0 (depth = 0) [eps=0.0]
	 *     ([eps]) = f([])
	 *     (Domain = [m,n,float_n] -> {S0[] : })
	 *     for : 0 <= j <= m-1 (stride = 1)
	 *       block : 
	 *         S1 (depth = 1) [mean[j]=0.0]
	 *         ([mean[j]]) = f([])
	 *         (Domain = [m,n,float_n] -> {S1[j] : (j >= 0) and (-j+m-1 >= 0)})
	 *         for : 0 <= i <= n-1 (stride = 1)
	 *           S2 (depth = 2) [mean[j]=add(mean[j],data[i][j])]
	 *           ([mean[j]]) = f([mean[j], data[i][j]])
	 *           (Domain = [m,n,float_n] -> {S2[j,i] : (i >= 0) and (-i+n-1 >= 0) and (j >= 0) and (-j+m-1 >= 0)})
	 *         S3 (depth = 1) [mean[j]=div(mean[j],IntExpr(float_n))]
	 *         ([mean[j]]) = f([mean[j]])
	 *         (Domain = [m,n,float_n] -> {S3[j] : (j >= 0) and (-j+m-1 >= 0)})
	 *     for : 0 <= j <= m-1 (stride = 1)
	 *       block : 
	 *         S4 (depth = 1) [stddev[j]=0.0]
	 *         ([stddev[j]]) = f([])
	 *         (Domain = [m,n,float_n] -> {S4[j] : (j >= 0) and (-j+m-1 >= 0)})
	 *         for : 0 <= i <= n-1 (stride = 1)
	 *           S5 (depth = 2) [stddev[j]=add(stddev[j],mul(sub(data[i][j],mean[j]),sub(data[i][j],mean[j])))]
	 *           ([stddev[j]]) = f([stddev[j], data[i][j], mean[j], data[i][j], mean[j]])
	 *           (Domain = [m,n,float_n] -> {S5[j,i] : (i >= 0) and (-i+n-1 >= 0) and (j >= 0) and (-j+m-1 >= 0)})
	 *         S6 (depth = 1) [stddev[j]=div(stddev[j],IntExpr(float_n))]
	 *         ([stddev[j]]) = f([stddev[j]])
	 *         (Domain = [m,n,float_n] -> {S6[j] : (j >= 0) and (-j+m-1 >= 0)})
	 *         S7 (depth = 1) [stddev[j]=call sqrt(stddev[j])]
	 *         ([stddev[j]]) = f([stddev[j]])
	 *         (Domain = [m,n,float_n] -> {S7[j] : (j >= 0) and (-j+m-1 >= 0)})
	 *         S8 (depth = 1) [stddev[j]=mux(le(stddev[j],eps),1.0,stddev[j])]
	 *         ([stddev[j]]) = f([stddev[j], eps, stddev[j]])
	 *         (Domain = [m,n,float_n] -> {S8[j] : (j >= 0) and (-j+m-1 >= 0)})
	 *     for : 0 <= i <= n-1 (stride = 1)
	 *       for : 0 <= j <= m-1 (stride = 1)
	 *         block : 
	 *           S9 (depth = 2) [data[i][j]=sub(data[i][j],mean[j])]
	 *           ([data[i][j]]) = f([data[i][j], mean[j]])
	 *           (Domain = [m,n,float_n] -> {S9[i,j] : (j >= 0) and (-j+m-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *           S10 (depth = 2) [data[i][j]=div(data[i][j],mul(call sqrt(IntExpr(float_n)),stddev[j]))]
	 *           ([data[i][j]]) = f([data[i][j], stddev[j]])
	 *           (Domain = [m,n,float_n] -> {S10[i,j] : (j >= 0) and (-j+m-1 >= 0) and (i >= 0) and (-i+n-1 >= 0)})
	 *     for : 0 <= j1 <= m-2 (stride = 1)
	 *       block : 
	 *         S11 (depth = 1) [symmat[j1][j1]=1.0]
	 *         ([symmat[j1][j1]]) = f([])
	 *         (Domain = [m,n,float_n] -> {S11[j1] : (j1 >= 0) and (-j1+m-2 >= 0)})
	 *         for : j1+1 <= j2 <= m-1 (stride = 1)
	 *           block : 
	 *             S12 (depth = 2) [symmat[j1][j2]=0.0]
	 *             ([symmat[j1][j2]]) = f([])
	 *             (Domain = [m,n,float_n] -> {S12[j1,j2] : (-j1+j2-1 >= 0) and (-j2+m-1 >= 0) and (j1 >= 0) and (-j1+m-2 >= 0)})
	 *             for : 0 <= i <= n-1 (stride = 1)
	 *               S13 (depth = 3) [symmat[j1][j2]=add(symmat[j1][j2],mul(data[i][j1],data[i][j2]))]
	 *               ([symmat[j1][j2]]) = f([symmat[j1][j2], data[i][j1], data[i][j2]])
	 *               (Domain = [m,n,float_n] -> {S13[j1,j2,i] : (i >= 0) and (-i+n-1 >= 0) and (-j1+j2-1 >= 0) and (-j2+m-1 >= 0) and (j1 >= 0) and (-j1+m-2 >= 0)})
	 *             S14 (depth = 2) [symmat[j2][j1]=symmat[j1][j2]]
	 *             ([symmat[j2][j1]]) = f([symmat[j1][j2]])
	 *             (Domain = [m,n,float_n] -> {S14[j1,j2] : (-j1+j2-1 >= 0) and (-j2+m-1 >= 0) and (j1 >= 0) and (-j1+m-2 >= 0)})
	 *     S15 (depth = 0) [symmat[m-1][m-1]=1.0]
	 *     ([symmat[m-1][m-1]]) = f([])
	 *     (Domain = [m,n,float_n] -> {S15[] : })
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_datamining_correlation_correlation_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_datamining_correlation_correlation_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[m, n, float_n] -> { S12[j1, j2] : j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0 and j1 <= -2 + m; S4[j] : j >= 0 and j <= -1 + m; S14[j1, j2] : j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0 and j1 <= -2 + m; S0[]; S10[i, j] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n; S11[j1] : j1 >= 0 and j1 <= -2 + m; S2[j, i] : i >= 0 and i <= -1 + n and j >= 0 and j <= -1 + m; S1[j] : j >= 0 and j <= -1 + m; S3[j] : j >= 0 and j <= -1 + m; S8[j] : j >= 0 and j <= -1 + m; S7[j] : j >= 0 and j <= -1 + m; S6[j] : j >= 0 and j <= -1 + m; S9[i, j] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n; S13[j1, j2, i] : i >= 0 and i <= -1 + n and j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0 and j1 <= -2 + m; S15[]; S5[j, i] : i >= 0 and i <= -1 + n and j >= 0 and j <= -1 + m }";
		String idSchedules = "[m, n, float_n] -> { S2[j, i] -> [1, j, 1, i, 0, 0, 0]; S12[j1, j2] -> [4, j1, 1, j2, 0, 0, 0]; S0[] -> [0, 0, 0, 0, 0, 0, 0]; S1[j] -> [1, j, 0, 0, 0, 0, 0]; S5[j, i] -> [2, j, 1, i, 0, 0, 0]; S6[j] -> [2, j, 2, 0, 0, 0, 0]; S7[j] -> [2, j, 3, 0, 0, 0, 0]; S4[j] -> [2, j, 0, 0, 0, 0, 0]; S3[j] -> [1, j, 2, 0, 0, 0, 0]; S14[j1, j2] -> [4, j1, 1, j2, 2, 0, 0]; S10[i, j] -> [3, i, 0, j, 1, 0, 0]; S15[] -> [5, 0, 0, 0, 0, 0, 0]; S8[j] -> [2, j, 4, 0, 0, 0, 0]; S13[j1, j2, i] -> [4, j1, 1, j2, 1, i, 0]; S11[j1] -> [4, j1, 0, 0, 0, 0, 0]; S9[i, j] -> [3, i, 0, j, 0, 0, 0] }";
		String writes = "[m, n, float_n] -> { S7[j] -> stddev[j]; S10[i, j] -> data[i, j]; S9[i, j] -> data[i, j]; S2[j, i] -> mean[j]; S11[j1] -> symmat[j1, j1]; S14[j1, j2] -> symmat[j2, j1]; S3[j] -> mean[j]; S6[j] -> stddev[j]; S0[] -> eps[]; S4[j] -> stddev[j]; S8[j] -> stddev[j]; S12[j1, j2] -> symmat[j1, j2]; S5[j, i] -> stddev[j]; S15[] -> symmat[-1 + m, -1 + m]; S1[j] -> mean[j]; S13[j1, j2, i] -> symmat[j1, j2] }";
		String reads = "[m, n, float_n] -> { S5[j, i] -> data[i, j]; S8[j] -> eps[]; S7[j] -> stddev[j]; S13[j1, j2, i] -> data[i, j1]; S13[j1, j2, i] -> data[i, j2]; S10[i, j] -> data[i, j]; S9[i, j] -> data[i, j]; S2[j, i] -> mean[j]; S5[j, i] -> mean[j]; S9[i, j] -> mean[j]; S14[j1, j2] -> symmat[j1, j2]; S3[j] -> mean[j]; S6[j] -> stddev[j]; S8[j] -> stddev[j]; S5[j, i] -> stddev[j]; S10[i, j] -> stddev[j]; S13[j1, j2, i] -> symmat[j1, j2]; S2[j, i] -> data[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[m, n, float_n] -> { S13[j1, j2, i] -> S10[i, j1] : i >= 0 and i <= -1 + n and j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0; S13[j1, j2, i] -> S10[i, j2] : i >= 0 and i <= -1 + n and j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0; S13[j1, j2, i] -> S13[j1, j2, -1 + i] : i >= 1 and i <= -1 + n and j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0 and j1 <= -2 + m; S2[j, i] -> S2[j, -1 + i] : i >= 1 and i <= -1 + n and j >= 0 and j <= -1 + m; S8[j] -> S0[] : j >= 0 and j <= -1 + m; S6[j] -> S5[j, -1 + n] : j >= 0 and j <= -1 + m and n >= 1; S5[j, 0] -> S4[j] : j <= -1 + m and n >= 1 and j >= 0; S5[j, i] -> S5[j, -1 + i] : i >= 1 and i <= -1 + n and j >= 0 and j <= -1 + m; S9[i, j] -> S3[j] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n; S8[j] -> S7[j] : j >= 0 and j <= -1 + m; S3[j] -> S2[j, -1 + n] : j >= 0 and j <= -1 + m and n >= 1; S5[j, i] -> S3[j] : i >= 0 and i <= -1 + n and j >= 0 and j <= -1 + m; S3[j] -> S1[j] : j >= 0 and j <= -1 + m and n <= 0; S7[j] -> S6[j] : j >= 0 and j <= -1 + m; S10[i, j] -> S8[j] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n; S2[j, 0] -> S1[j] : j <= -1 + m and n >= 1 and j >= 0; S13[j1, j2, 0] -> S12[j1, j2] : j1 >= 0 and n >= 1 and j2 >= 1 + j1 and j2 <= -1 + m and j1 <= -2 + m; S14[j1, j2] -> S13[j1, j2, -1 + n] : j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0 and n >= 1 and j1 <= -2 + m; S10[i, j] -> S9[i, j] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n; S6[j] -> S4[j] : j >= 0 and j <= -1 + m and n <= 0; S14[j1, j2] -> S12[j1, j2] : j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0 and n <= 0 and j1 <= -2 + m }";
		String valueBasedPlutoSchedule = "[m, n, float_n] -> { S2[j, i] -> [0, j, 0, 0, 0, i, 1]; S12[j1, j2] -> [0, n + j1, j2, 0, 0, 0, 0]; S0[] -> [0, 0, 1, 0, 3, 0, 0]; S1[j] -> [0, 0, 0, 0, 0, j, 0]; S6[j] -> [0, j, 1, 0, 1, 0, 0]; S5[j, i] -> [0, j, 0, 1, i, 0, 0]; S7[j] -> [0, j, 1, 0, 2, 0, 0]; S4[j] -> [0, 0, 0, 0, j, 0, 0]; S3[j] -> [0, j, 0, 0, 1, 0, 0]; S14[j1, j2] -> [j2, n + j2, j1, 0, 0, 0, 0]; S10[i, j] -> [0, i + j, 1, j, 5, 0, 0]; S15[] -> [0, 0, 0, 0, 0, 0, 0]; S8[j] -> [0, j, 1, 0, 4, 0, 0]; S13[j1, j2, i] -> [j1, n + j2, i, 0, 0, 0, 0]; S11[j1] -> [j1, 0, 0, 0, 0, 0, 0]; S9[i, j] -> [0, j, 1, i, 0, 0, 0] }";
		String valueBasedFeautrierSchedule = "[m, n, float_n] -> { S10[i, j] -> [i, j, 0]; S11[j1] -> [j1, 0, 0]; S7[j] -> [j, 0, 0]; S12[j1, j2] -> [j1, j2, 0]; S9[i, j] -> [i, j, 0]; S15[] -> [0, 0, 0]; S4[j] -> [j, 0, 0]; S2[j, i] -> [i, j, 0]; S1[j] -> [j, 0, 0]; S6[j] -> [j, 0, 0]; S5[j, i] -> [i, j, 0]; S8[j] -> [j, 0, 0]; S3[j] -> [j, 0, 0]; S14[j1, j2] -> [j1, j2, 0]; S0[] -> [0, 0, 0]; S13[j1, j2, i] -> [i, j2, j1] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[m, n, float_n] -> { S13[j1, j2, i] -> S10[i, j1] : i >= 0 and i <= -1 + n and j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0; S13[j1, j2, i] -> S10[i, j2] : i >= 0 and i <= -1 + n and j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0; S13[j1, j2, i] -> S13[j1, j2, -1 + i] : j1 >= 0 and j2 >= 1 + j1 and j2 <= -1 + m and i >= 1 and i <= -1 + n; S2[j, i] -> S2[j, -1 + i] : j >= 0 and j <= -1 + m and i >= 1 and i <= -1 + n; S8[j] -> S0[] : j >= 0 and j <= -1 + m; S6[j] -> S5[j, -1 + n] : n >= 1 and j >= 0 and j <= -1 + m; S5[j, 0] -> S4[j] : n >= 1 and j >= 0 and j <= -1 + m; S5[j, i] -> S5[j, -1 + i] : j >= 0 and j <= -1 + m and i >= 1 and i <= -1 + n; S9[i, j] -> S3[j] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n; S8[j] -> S7[j] : j >= 0 and j <= -1 + m; S3[j] -> S2[j, -1 + n] : n >= 1 and j >= 0 and j <= -1 + m; S5[j, i] -> S3[j] : i >= 0 and i <= -1 + n and j >= 0 and j <= -1 + m; S3[j] -> S1[j] : n <= 0 and j >= 0 and j <= -1 + m; S7[j] -> S6[j] : j >= 0 and j <= -1 + m; S10[i, j] -> S8[j] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n; S9[i, j] -> S2[j, i] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n; S2[j, 0] -> S1[j] : n >= 1 and j >= 0 and j <= -1 + m; S9[i, j] -> S5[j, i] : j >= 0 and j <= -1 + m and i >= 0 and i <= -1 + n; S13[j1, j2, 0] -> S12[j1, j2] : n >= 1 and j1 >= 0 and j2 >= 1 + j1 and j2 <= -1 + m; S14[j1, j2] -> S13[j1, j2, -1 + n] : j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0 and n >= 1 and j1 <= -2 + m; S10[i, j] -> S9[i, j] : i >= 0 and i <= -1 + n and j >= 0 and j <= -1 + m; S6[j] -> S4[j] : n <= 0 and j >= 0 and j <= -1 + m; S14[j1, j2] -> S12[j1, j2] : j2 >= 1 + j1 and j2 <= -1 + m and j1 >= 0 and n <= 0 and j1 <= -2 + m }";
		String memoryBasedPlutoSchedule = "[m, n, float_n] -> { S2[j, i] -> [0, j, 0, 0, 0, i, 1]; S12[j1, j2] -> [0, n + j1, j2, 0, 0, 0, 0]; S0[] -> [0, 0, 1, 0, 3, 0, 0]; S1[j] -> [0, 0, 0, 0, 0, j, 0]; S6[j] -> [0, j, 1, 0, 1, 0, 0]; S5[j, i] -> [0, j, 0, 1, i, 0, 0]; S7[j] -> [0, j, 1, 0, 2, 0, 0]; S4[j] -> [0, 0, 0, 0, j, 0, 0]; S3[j] -> [0, j, 0, 0, 1, 0, 0]; S14[j1, j2] -> [j2, n + j2, j1, 0, 0, 0, 0]; S10[i, j] -> [0, i + j, 1, j, 5, 0, 0]; S15[] -> [0, 0, 0, 0, 0, 0, 0]; S8[j] -> [0, j, 1, 0, 4, 0, 0]; S13[j1, j2, i] -> [j1, n + j2, i, 0, 0, 0, 0]; S11[j1] -> [j1, 0, 0, 0, 0, 0, 0]; S9[i, j] -> [0, j, 1, i, 0, 0, 0] }";
		String memoryBasedFeautrierSchedule = "[m, n, float_n] -> { S10[i, j] -> [i, j, 0]; S11[j1] -> [j1, 0, 0]; S7[j] -> [j, 0, 0]; S12[j1, j2] -> [j1, j2, 0]; S9[i, j] -> [i, j, 0]; S15[] -> [0, 0, 0]; S4[j] -> [j, 0, 0]; S2[j, i] -> [i, j, 0]; S1[j] -> [j, 0, 0]; S6[j] -> [j, 0, 0]; S5[j, i] -> [i, j, 0]; S8[j] -> [j, 0, 0]; S3[j] -> [j, 0, 0]; S14[j1, j2] -> [j1, j2, 0]; S0[] -> [0, 0, 0]; S13[j1, j2, i] -> [i, j2, j1] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #143
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_fdtd_2d_kernel_fdtd_2d/scop_0
	 *   for : 0 <= t <= tmax-1 (stride = 1)
	 *     block : 
	 *       for : 0 <= j <= ny-1 (stride = 1)
	 *         S0 (depth = 2) [ey[0][j]=_fict_[t]]
	 *         ([ey[0][j]]) = f([_fict_[t]])
	 *         (Domain = [tmax,ny,nx] -> {S0[t,j] : (j >= 0) and (-j+ny-1 >= 0) and (t >= 0) and (-t+tmax-1 >= 0)})
	 *       for : 1 <= i <= nx-1 (stride = 1)
	 *         for : 0 <= j <= ny-1 (stride = 1)
	 *           S1 (depth = 3) [ey[i][j]=sub(ey[i][j],mul(0.5,sub(hz[i][j],hz[i-1][j])))]
	 *           ([ey[i][j]]) = f([ey[i][j], hz[i][j], hz[i-1][j]])
	 *           (Domain = [tmax,ny,nx] -> {S1[t,i,j] : (j >= 0) and (-j+ny-1 >= 0) and (i-1 >= 0) and (-i+nx-1 >= 0) and (t >= 0) and (-t+tmax-1 >= 0)})
	 *       for : 0 <= i <= nx-1 (stride = 1)
	 *         for : 1 <= j <= ny-1 (stride = 1)
	 *           S2 (depth = 3) [ex[i][j]=sub(ex[i][j],mul(0.5,sub(hz[i][j],hz[i][j-1])))]
	 *           ([ex[i][j]]) = f([ex[i][j], hz[i][j], hz[i][j-1]])
	 *           (Domain = [tmax,ny,nx] -> {S2[t,i,j] : (j-1 >= 0) and (-j+ny-1 >= 0) and (i >= 0) and (-i+nx-1 >= 0) and (t >= 0) and (-t+tmax-1 >= 0)})
	 *       for : 0 <= i <= nx-2 (stride = 1)
	 *         for : 0 <= j <= ny-2 (stride = 1)
	 *           S3 (depth = 3) [hz[i][j]=sub(hz[i][j],mul(0.699999988079071,sub(add(sub(ex[i][j+1],ex[i][j]),ey[i+1][j]),ey[i][j])))]
	 *           ([hz[i][j]]) = f([hz[i][j], ex[i][j+1], ex[i][j], ey[i+1][j], ey[i][j]])
	 *           (Domain = [tmax,ny,nx] -> {S3[t,i,j] : (j >= 0) and (-j+ny-2 >= 0) and (i >= 0) and (-i+nx-2 >= 0) and (t >= 0) and (-t+tmax-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_stencils_fdtd_2d_fdtd_2d_GScopRoot_kernel_fdtd_2d_scop_0_ () {
		String path = "polybench_gecos_stencils_fdtd_2d_fdtd_2d_GScopRoot_kernel_fdtd_2d_scop_0_";
		// SCoP Extraction Data
		String domains = "[tmax, ny, nx] -> { S2[t, i, j] : j >= 1 and j <= -1 + ny and i >= 0 and i <= -1 + nx and t >= 0 and t <= -1 + tmax; S3[t, i, j] : j >= 0 and j <= -2 + ny and i >= 0 and i <= -2 + nx and t >= 0 and t <= -1 + tmax; S0[t, j] : j >= 0 and j <= -1 + ny and t >= 0 and t <= -1 + tmax; S1[t, i, j] : j >= 0 and j <= -1 + ny and i >= 1 and i <= -1 + nx and t >= 0 and t <= -1 + tmax }";
		String idSchedules = "[tmax, ny, nx] -> { S1[t, i, j] -> [0, t, 1, i, 0, j, 0]; S3[t, i, j] -> [0, t, 3, i, 0, j, 0]; S2[t, i, j] -> [0, t, 2, i, 0, j, 0]; S0[t, j] -> [0, t, 0, j, 0, 0, 0] }";
		String writes = "[tmax, ny, nx] -> { S3[t, i, j] -> hz[i, j]; S0[t, j] -> ey[0, j]; S2[t, i, j] -> ex[i, j]; S1[t, i, j] -> ey[i, j] }";
		String reads = "[tmax, ny, nx] -> { S2[t, i, j] -> hz[i, j]; S2[t, i, j] -> hz[i, -1 + j]; S3[t, i, j] -> ex[i, 1 + j]; S3[t, i, j] -> ex[i, j]; S3[t, i, j] -> hz[i, j]; S3[t, i, j] -> ey[1 + i, j]; S3[t, i, j] -> ey[i, j]; S1[t, i, j] -> hz[i, j]; S1[t, i, j] -> hz[-1 + i, j]; S0[t, j] -> _fict_[t]; S1[t, i, j] -> ey[i, j]; S2[t, i, j] -> ex[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[tmax, ny, nx] -> { S1[t, i, j] -> S1[-1 + t, i, j] : j >= 0 and j <= -1 + ny and i >= 1 and i <= -1 + nx and t >= 1 and t <= -1 + tmax; S1[t, i, j] -> S3[-1 + t, i', j] : j >= 0 and j <= -2 + ny and i' <= -2 + nx and t >= 1 and t <= -1 + tmax and i' <= i and i >= 1 and i' >= -1 + i; S3[t, i, j] -> S1[t, i', j] : j >= 0 and j <= -2 + ny and i' >= 1 and t >= 0 and i <= -2 + nx and t <= -1 + tmax and i' >= i and i' <= 1 + i; S3[t, i, j] -> S3[-1 + t, i, j] : j >= 0 and j <= -2 + ny and i >= 0 and i <= -2 + nx and t >= 1 and t <= -1 + tmax; S3[t, 0, j] -> S0[t, j] : j >= 0 and j <= -2 + ny and t <= -1 + tmax and nx >= 2 and t >= 0; S2[t, i, j] -> S3[-1 + t, i, j'] : j' <= -2 + ny and i >= 0 and i <= -2 + nx and t >= 1 and t <= -1 + tmax and j' <= j and j >= 1 and j' >= -1 + j; S3[t, i, j] -> S2[t, i, j'] : j' >= 1 and j <= -2 + ny and i >= 0 and i <= -2 + nx and t >= 0 and t <= -1 + tmax and j' >= j and j' <= 1 + j; S2[t, i, j] -> S2[-1 + t, i, j] : j >= 1 and j <= -1 + ny and i >= 0 and i <= -1 + nx and t >= 1 and t <= -1 + tmax }";
		String valueBasedPlutoSchedule = "[tmax, ny, nx] -> { S3[t, i, j] -> [t, 1 + t + i, 1 + t + i + j, 2]; S1[t, i, j] -> [t, t + i, t + i + j, 0]; S0[t, j] -> [0, t, j, 1]; S2[t, i, j] -> [t, t + i, t + i + j, 3] }";
		String valueBasedFeautrierSchedule = "[tmax, ny, nx] -> { S3[t, i, j] -> [t, 1, i, j]; S1[t, i, j] -> [t, 0, i, j]; S0[t, j] -> [0, 0, t, j]; S2[t, i, j] -> [t, 0, i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[tmax, ny, nx] -> { S1[t, i, j] -> S1[-1 + t, i, j] : t >= 1 and t <= -1 + tmax and i >= 1 and i <= -1 + nx and j >= 0 and j <= -1 + ny; S1[t, i, j] -> S3[-1 + t, i', j] : t >= 1 and t <= -1 + tmax and j >= 0 and i' <= -2 + nx and i' >= -1 + i and j <= -2 + ny and i >= 1 and i' <= i; S3[t, i, j] -> S1[t, i', j] : j >= 0 and t <= -1 + tmax and i' >= 1 and i' >= i and j <= -2 + ny and t >= 0 and i' <= 1 + i and i <= -2 + nx; S3[t, i, j] -> S3[-1 + t, i, j] : t >= 1 and t <= -1 + tmax and i >= 0 and i <= -2 + nx and j >= 0 and j <= -2 + ny; S0[t, j] -> S3[-1 + t, 0, j] : nx >= 2 and t >= 1 and t <= -1 + tmax and j >= 0 and j <= -2 + ny; S3[t, 0, j] -> S0[t, j] : j >= 0 and j <= -2 + ny and t <= -1 + tmax and nx >= 2 and t >= 0; S2[t, i, j] -> S3[-1 + t, i, j'] : t >= 1 and t <= -1 + tmax and j' >= -1 + j and i <= -2 + nx and j >= 1 and j' <= -2 + ny and i >= 0 and j' <= j; S3[t, i, j] -> S2[t, i, j'] : t >= 0 and t <= -1 + tmax and i >= 0 and i <= -2 + nx and j' >= 1 and j <= -2 + ny and j' <= 1 + j and j' >= j; S0[t, j] -> S0[-1 + t, j] : t >= 1 and t <= -1 + tmax and j >= 0 and j <= -1 + ny; S2[t, i, j] -> S2[-1 + t, i, j] : t >= 1 and t <= -1 + tmax and i >= 0 and i <= -1 + nx and j >= 1 and j <= -1 + ny }";
		String memoryBasedPlutoSchedule = "[tmax, ny, nx] -> { S3[t, i, j] -> [t, 1 + t + i, 1 + t + i + j, 2]; S1[t, i, j] -> [t, t + i, t + i + j, 0]; S0[t, j] -> [t, t, t + j, 1]; S2[t, i, j] -> [t, t + i, t + i + j, 3] }";
		String memoryBasedFeautrierSchedule = "[tmax, ny, nx] -> { S3[t, i, j] -> [t, 1, i, j]; S1[t, i, j] -> [t, 0, i, j]; S0[t, j] -> [t, 0, j, 0]; S2[t, i, j] -> [t, 0, i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #144
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : burg2_burg2/scop_0
	 *   for : 2 <= n <= pp (stride = 1)
	 *     block : 
	 *       S0 (depth = 1) [s1=IntExpr(0)]
	 *       ([s1]) = f([])
	 *       (Domain = [pp,m] -> {S0[n] : (n-2 >= 0) and (-n+pp >= 0)})
	 *       S1 (depth = 1) [s2=IntExpr(0)]
	 *       ([s2]) = f([])
	 *       (Domain = [pp,m] -> {S1[n] : (n-2 >= 0) and (-n+pp >= 0)})
	 *       for : n+1 <= i <= m (stride = 1)
	 *         block : 
	 *           S2 (depth = 2) [s1=add(s1,mul(e[i],b[i-n]))]
	 *           ([s1]) = f([s1, e[i], b[i-n]])
	 *           (Domain = [pp,m] -> {S2[n,i] : (-n+i-1 >= 0) and (-i+m >= 0) and (n-2 >= 0) and (-n+pp >= 0)})
	 *           S3 (depth = 2) [s2=add(add(s2,mul(e[i],IntExpr(2))),mul(b[i-n],IntExpr(2)))]
	 *           ([s2]) = f([s2, e[i], b[i-n]])
	 *           (Domain = [pp,m] -> {S3[n,i] : (-n+i-1 >= 0) and (-i+m >= 0) and (n-2 >= 0) and (-n+pp >= 0)})
	 *       S4 (depth = 1) [c[n]=div(mul(IntExpr(-2),s1),s2)]
	 *       ([c[n]]) = f([s1, s2])
	 *       (Domain = [pp,m] -> {S4[n] : (n-2 >= 0) and (-n+pp >= 0)})
	 *       for : 1 <= i <= n-1 (stride = 1)
	 *         S5 (depth = 2) [a1[i]=add(a[i],mul(c[n],a[n-i]))]
	 *         ([a1[i]]) = f([a[i], c[n], a[n-i]])
	 *         (Domain = [pp,m] -> {S5[n,i] : (i-1 >= 0) and (n-i-1 >= 0) and (n-2 >= 0) and (-n+pp >= 0)})
	 *       for : 1 <= i <= n-1 (stride = 1)
	 *         S6 (depth = 2) [a[i]=a1[i]]
	 *         ([a[i]]) = f([a1[i]])
	 *         (Domain = [pp,m] -> {S6[n,i] : (i-1 >= 0) and (n-i-1 >= 0) and (n-2 >= 0) and (-n+pp >= 0)})
	 *       S7 (depth = 1) [a[n]=c[n]]
	 *       ([a[n]]) = f([c[n]])
	 *       (Domain = [pp,m] -> {S7[n] : (n-2 >= 0) and (-n+pp >= 0)})
	 *       for : n+1 <= i <= m (stride = 1)
	 *         block : 
	 *           S8 (depth = 2) [temp=add(e[i],mul(c[n],b[i-n]))]
	 *           ([temp]) = f([e[i], c[n], b[i-n]])
	 *           (Domain = [pp,m] -> {S8[n,i] : (-n+i-1 >= 0) and (-i+m >= 0) and (n-2 >= 0) and (-n+pp >= 0)})
	 *           S9 (depth = 2) [b[i-n]=add(b[i-n],mul(c[n],e[i]))]
	 *           ([b[i-n]]) = f([b[i-n], c[n], e[i]])
	 *           (Domain = [pp,m] -> {S9[n,i] : (-n+i-1 >= 0) and (-i+m >= 0) and (n-2 >= 0) and (-n+pp >= 0)})
	 *           S10 (depth = 2) [e[i]=temp]
	 *           ([e[i]]) = f([temp])
	 *           (Domain = [pp,m] -> {S10[n,i] : (-n+i-1 >= 0) and (-i+m >= 0) and (n-2 >= 0) and (-n+pp >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_polybenchs_perso_burg2_GScopRoot_burg2_scop_0_ () {
		String path = "polymodel_src_polybenchs_perso_burg2_GScopRoot_burg2_scop_0_";
		// SCoP Extraction Data
		String domains = "[pp, m] -> { S1[n] : n >= 2 and n <= pp; S7[n] : n >= 2 and n <= pp; S8[n, i] : i >= 1 + n and i <= m and n >= 2 and n <= pp; S5[n, i] : i >= 1 and i <= -1 + n and n >= 2 and n <= pp; S9[n, i] : i >= 1 + n and i <= m and n >= 2 and n <= pp; S3[n, i] : i >= 1 + n and i <= m and n >= 2 and n <= pp; S0[n] : n >= 2 and n <= pp; S2[n, i] : i >= 1 + n and i <= m and n >= 2 and n <= pp; S6[n, i] : i >= 1 and i <= -1 + n and n >= 2 and n <= pp; S4[n] : n >= 2 and n <= pp; S10[n, i] : i >= 1 + n and i <= m and n >= 2 and n <= pp }";
		String idSchedules = "[pp, m] -> { S7[n] -> [0, n, 6, 0, 0]; S8[n, i] -> [0, n, 7, i, 0]; S10[n, i] -> [0, n, 7, i, 2]; S1[n] -> [0, n, 1, 0, 0]; S5[n, i] -> [0, n, 4, i, 0]; S3[n, i] -> [0, n, 2, i, 1]; S4[n] -> [0, n, 3, 0, 0]; S6[n, i] -> [0, n, 5, i, 0]; S2[n, i] -> [0, n, 2, i, 0]; S0[n] -> [0, n, 0, 0, 0]; S9[n, i] -> [0, n, 7, i, 1] }";
		String writes = "[pp, m] -> { S0[n] -> s1[]; S8[n, i] -> temp[]; S4[n] -> c[n]; S3[n, i] -> s2[]; S6[n, i] -> a[i]; S2[n, i] -> s1[]; S7[n] -> a[n]; S10[n, i] -> e[i]; S1[n] -> s2[]; S9[n, i] -> b[-n + i]; S5[n, i] -> a1[i] }";
		String reads = "[pp, m] -> { S3[n, i] -> e[i]; S8[n, i] -> b[-n + i]; S9[n, i] -> c[n]; S9[n, i] -> e[i]; S10[n, i] -> temp[]; S3[n, i] -> b[-n + i]; S4[n] -> s1[]; S8[n, i] -> e[i]; S8[n, i] -> c[n]; S3[n, i] -> s2[]; S6[n, i] -> a1[i]; S2[n, i] -> s1[]; S5[n, i] -> c[n]; S2[n, i] -> b[-n + i]; S7[n] -> c[n]; S4[n] -> s2[]; S2[n, i] -> e[i]; S9[n, i] -> b[-n + i]; S5[n, i] -> a[n - i]; S5[n, i] -> a[i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[pp, m] -> { S9[n, i] -> S10[-1 + n, i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S3[n, i] -> S9[-1 + n, -1 + i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S7[n] -> S4[n] : n >= 2 and n <= pp; S3[n, 1 + n] -> S1[n] : n <= pp and n <= -1 + m and n >= 2; S3[n, i] -> S10[-1 + n, i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S5[n, i] -> S4[n] : i >= 1 and i <= -1 + n and n <= pp and n >= 2; S6[n, i] -> S5[n, i] : i >= 1 and i <= -1 + n and n <= pp and n >= 2; S9[n, i] -> S4[n] : i >= 1 + n and i <= m and n >= 2 and n <= pp; S8[n, i] -> S9[-1 + n, -1 + i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S9[n, i] -> S9[-1 + n, -1 + i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S4[n] -> S3[n, m] : n >= 2 and n <= pp and n <= -1 + m; S10[n, i] -> S8[n, i] : i >= 1 + n and i <= m and n >= 2 and n <= pp; S8[n, i] -> S10[-1 + n, i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S5[n, 1] -> S7[-1 + n] : n >= 3 and n <= pp; S5[n, -1 + n] -> S7[-1 + n] : n <= pp and n >= 3; S2[n, i] -> S10[-1 + n, i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S4[n] -> S2[n, m] : n >= 2 and n <= pp and n <= -1 + m; S4[n] -> S0[n] : n >= 2 and n <= pp and n >= m; S3[n, i] -> S3[n, -1 + i] : i >= 2 + n and i <= m and n >= 2 and n <= pp; S4[n] -> S1[n] : n >= 2 and n <= pp and n >= m; S2[n, i] -> S2[n, -1 + i] : i >= 2 + n and i <= m and n >= 2 and n <= pp; S8[n, i] -> S4[n] : i >= 1 + n and i <= m and n >= 2 and n <= pp; S5[n, i] -> S6[-1 + n, n - i] : n <= pp and i <= -1 + n and i >= 2; S5[n, i] -> S6[-1 + n, i] : (n <= pp and i >= 1 and 2i <= -1 + n) or (n <= pp and 2i >= 1 + n and i <= -2 + n); S2[n, i] -> S9[-1 + n, -1 + i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S2[n, 1 + n] -> S0[n] : n <= pp and n <= -1 + m and n >= 2 }";
		String valueBasedPlutoSchedule = "[pp, m] -> { S5[n, i] -> [n, i, 5]; S2[n, i] -> [n, -m + i, 2]; S1[n] -> [0, n, 0]; S3[n, i] -> [n, -m + i, 3]; S10[n, i] -> [n, i, 1]; S8[n, i] -> [n, i, 0]; S9[n, i] -> [n, i, 7]; S6[n, i] -> [n, i, 6]; S7[n] -> [n, 0, 8]; S0[n] -> [0, n, 0]; S4[n] -> [n, 0, 4] }";
		String valueBasedFeautrierSchedule = "[pp, m] -> { S5[n, i] -> [n, 2, i]; S2[n, i] -> [n, 0, i]; S1[n] -> [0, 0, n]; S3[n, i] -> [n, 0, i]; S10[n, i] -> [n, 3, i]; S8[n, i] -> [n, 2, i]; S9[n, i] -> [n, 2, i]; S6[n, i] -> [n, 3, i]; S7[n] -> [n, 2, 0]; S0[n] -> [0, 0, n]; S4[n] -> [n, 1, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[pp, m] -> { S9[n, i] -> S2[n, i] : n >= 2 and n <= pp and i >= 1 + n and i <= m; S0[n] -> S2[-1 + n, m] : n >= 3 and n <= pp and n <= m; S2[n, i] -> S10[-1 + n, i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S0[n] -> S0[-1 + n] : n <= pp and n >= 1 + m and n >= 3; S9[n, i] -> S10[-1 + n, i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S9[n, i] -> S3[n, i] : n >= 2 and n <= pp and i >= 1 + n and i <= m; S3[n, i] -> S9[-1 + n, -1 + i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S7[n] -> S4[n] : n >= 2 and n <= pp; S3[n, 1 + n] -> S1[n] : n >= 2 and n <= -1 + m and n <= pp; S1[n] -> S4[-1 + n] : n <= pp and n >= 3; S3[n, i] -> S10[-1 + n, i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S5[n, i] -> S5[-1 + n, i] : i >= 1 and n <= pp and i <= -2 + n and n >= 3; S10[n, i] -> S3[n, i] : n >= 2 and n <= pp and i >= 1 + n and i <= m; S5[n, i] -> S4[n] : i >= 1 and i <= -1 + n and n <= pp and n >= 2; S6[n, i] -> S5[n, n - i] : n <= pp and i <= -1 + n and i >= 1; S6[n, i] -> S5[n, i] : n <= pp and i >= 1 and i <= -1 + n; S6[n, -1 + n] -> S7[-1 + n] : n <= pp and n >= 3; S9[n, i] -> S4[n] : i >= 1 + n and i <= m and n >= 2 and n <= pp; S8[n, i] -> S9[-1 + n, -1 + i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S10[n, i] -> S9[n, i] : n >= 2 and n <= pp and i >= 1 + n and i <= m; S9[n, i] -> S9[-1 + n, -1 + i] : n >= 3 and n <= pp and i >= 1 + n and i <= m; S4[n] -> S3[n, m] : n >= 2 and n <= pp and n <= -1 + m; S0[n] -> S4[-1 + n] : n <= pp and n >= 3; S1[n] -> S1[-1 + n] : n <= pp and n >= 1 + m and n >= 3; S8[n, i] -> S8[n, -1 + i] : i >= 2 + n and i <= m and n >= 2 and n <= pp; S8[n, 1 + n] -> S8[-1 + n, m] : n <= pp and n <= -1 + m and n >= 3; S8[n, i] -> S10[-1 + n, i] : n >= 3 and n <= pp and i >= 1 + n and i <= m; S8[n, i] -> S10[n, -1 + i] : n >= 2 and n <= pp and i >= 2 + n and i <= m; S8[n, 1 + n] -> S10[-1 + n, m] : n >= 3 and n <= -1 + m and n <= pp; S9[n, i] -> S8[n, i] : n >= 2 and n <= pp and i >= 1 + n and i <= m; S5[n, 1] -> S7[-1 + n] : n >= 3 and n <= pp; S5[n, -1 + n] -> S7[-1 + n] : n <= pp and n >= 3; S10[n, i] -> S10[-1 + n, i] : n >= 3 and n <= pp and i >= 1 + n and i <= m; S10[n, i] -> S8[n, i] : n >= 2 and n <= pp and i >= 1 + n and i <= m; S4[n] -> S2[n, m] : n >= 2 and n <= pp and n <= -1 + m; S4[n] -> S0[n] : n >= 2 and n <= pp and n >= m; S10[n, i] -> S2[n, i] : n >= 2 and n <= pp and i >= 1 + n and i <= m; S3[n, i] -> S3[n, -1 + i] : n >= 2 and n <= pp and i >= 2 + n and i <= m; S4[n] -> S1[n] : n >= 2 and n <= pp and n >= m; S6[n, i] -> S6[-1 + n, i] : n <= pp and i >= 1 and i <= -2 + n; S2[n, i] -> S2[n, -1 + i] : n >= 2 and n <= pp and i >= 2 + n and i <= m; S8[n, i] -> S4[n] : i >= 1 + n and i <= m and n >= 2 and n <= pp; S1[n] -> S3[-1 + n, m] : n >= 3 and n <= pp and n <= m; S5[n, i] -> S6[-1 + n, n - i] : (n <= pp and i >= 2 and 2i <= -1 + n) or (n <= pp and 2i >= 1 + n and i <= -1 + n); S5[n, i] -> S6[-1 + n, i] : n <= pp and i >= 1 and i <= -2 + n; S2[n, i] -> S9[-1 + n, -1 + i] : i >= 1 + n and i <= m and n >= 3 and n <= pp; S2[n, 1 + n] -> S0[n] : n >= 2 and n <= -1 + m and n <= pp }";
		String memoryBasedPlutoSchedule = "[pp, m] -> { S0[n] -> [n, 0, 0, 1]; S1[n] -> [n, 0, 0, 0]; S8[n, i] -> [n, 1, i, 0]; S9[n, i] -> [n, 1, i, 1]; S3[n, i] -> [n, 1, -m + i, 3]; S4[n] -> [n, 1, 0, 4]; S2[n, i] -> [n, 0, i, 0]; S6[n, i] -> [n, 1, n + i, 5]; S5[n, i] -> [n, 1, i, 6]; S10[n, i] -> [n, 1, i, 2]; S7[n] -> [n, 1, 0, 7] }";
		String memoryBasedFeautrierSchedule = "[pp, m] -> { S0[n] -> [n, 0, 0, 0]; S1[n] -> [n, 0, 0, 0]; S8[n, i] -> [1 + n, 0, i, 0]; S9[n, i] -> [1 + n, 0, i, 1]; S3[n, i] -> [n, 1, i, 0]; S4[n] -> [n, 2, 0, 0]; S2[n, i] -> [n, 1, i, 0]; S6[n, i] -> [1 + n, 1, i, 0]; S5[n, i] -> [1 + n, 0, i, 0]; S10[n, i] -> [1 + n, 0, i, 2]; S7[n] -> [1 + n, 0, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #145
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : main_scop_new
	 *   block : 
	 *     for : 0 <= lc_3 <= N_3-1 (stride = 1)
	 *       for : 0 <= lc_2 <= N_2-1 (stride = 1)
	 *         block : 
	 *           for : 0 <= lc_1 <= N_1-1 (stride = 1)
	 *             S0 (depth = 3) [result=add(result,IntExpr(T))]
	 *             ([result]) = f([result])
	 *             (Domain = [N_3,N_2,N_1,T] -> {S0[lc_3,lc_2,lc_1] : (lc_1 >= 0) and (-lc_1+N_1-1 >= 0) and (lc_2 >= 0) and (-lc_2+N_2-1 >= 0) and (lc_3 >= 0) and (-lc_3+N_3-1 >= 0)})
	 *           S1 (depth = 2) [result=mul(result,IntExpr(T))]
	 *           ([result]) = f([result])
	 *           (Domain = [N_3,N_2,N_1,T] -> {S1[lc_3,lc_2] : (lc_2 >= 0) and (-lc_2+N_2-1 >= 0) and (lc_3 >= 0) and (-lc_3+N_3-1 >= 0)})
	 *           for : 0 <= lc_1 <= N_1-1 (stride = 1)
	 *             S2 (depth = 3) [result=sub(result,IntExpr(T))]
	 *             ([result]) = f([result])
	 *             (Domain = [N_3,N_2,N_1,T] -> {S2[lc_3,lc_2,lc_1] : (lc_1 >= 0) and (-lc_1+N_1-1 >= 0) and (lc_2 >= 0) and (-lc_2+N_2-1 >= 0) and (lc_3 >= 0) and (-lc_3+N_3-1 >= 0)})
	 *     for : 0 <= lc_3 <= N_3-1 (stride = 1)
	 *       block : 
	 *         for : 0 <= lc_2 <= N_2-1 (stride = 1)
	 *           block : 
	 *             S3 (depth = 2) [result=add(result,IntExpr(T))]
	 *             ([result]) = f([result])
	 *             (Domain = [N_3,N_2,N_1,T] -> {S3[lc_3,lc_2] : (lc_2 >= 0) and (-lc_2+N_2-1 >= 0) and (lc_3 >= 0) and (-lc_3+N_3-1 >= 0)})
	 *             for : 0 <= lc_1 <= N_1-1 (stride = 1)
	 *               S4 (depth = 3) [result=add(result,IntExpr(T))]
	 *               ([result]) = f([result])
	 *               (Domain = [N_3,N_2,N_1,T] -> {S4[lc_3,lc_2,lc_1] : (lc_1 >= 0) and (-lc_1+N_1-1 >= 0) and (lc_2 >= 0) and (-lc_2+N_2-1 >= 0) and (lc_3 >= 0) and (-lc_3+N_3-1 >= 0)})
	 *         S5 (depth = 1) [result=mul(result,IntExpr(T))]
	 *         ([result]) = f([result])
	 *         (Domain = [N_3,N_2,N_1,T] -> {S5[lc_3] : (lc_3 >= 0) and (-lc_3+N_3-1 >= 0)})
	 *         for : 0 <= lc_2 <= N_2-1 (stride = 1)
	 *           block : 
	 *             S6 (depth = 2) [result=add(result,IntExpr(T))]
	 *             ([result]) = f([result])
	 *             (Domain = [N_3,N_2,N_1,T] -> {S6[lc_3,lc_2] : (lc_2 >= 0) and (-lc_2+N_2-1 >= 0) and (lc_3 >= 0) and (-lc_3+N_3-1 >= 0)})
	 *             for : 0 <= lc_1 <= N_1-1 (stride = 1)
	 *               S7 (depth = 3) [result=sub(result,IntExpr(T))]
	 *               ([result]) = f([result])
	 *               (Domain = [N_3,N_2,N_1,T] -> {S7[lc_3,lc_2,lc_1] : (lc_1 >= 0) and (-lc_1+N_1-1 >= 0) and (lc_2 >= 0) and (-lc_2+N_2-1 >= 0) and (lc_3 >= 0) and (-lc_3+N_3-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_for12_GScopRoot_scop_new_ () {
		String path = "polymodel_src_generic_for_loops_for12_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[N_3, N_2, N_1, T] -> { S4[lc_3, lc_2, lc_1] : lc_1 >= 0 and lc_1 <= -1 + N_1 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3; S1[lc_3, lc_2] : lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3; S2[lc_3, lc_2, lc_1] : lc_1 >= 0 and lc_1 <= -1 + N_1 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3; S7[lc_3, lc_2, lc_1] : lc_1 >= 0 and lc_1 <= -1 + N_1 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3; S0[lc_3, lc_2, lc_1] : lc_1 >= 0 and lc_1 <= -1 + N_1 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3; S3[lc_3, lc_2] : lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3; S6[lc_3, lc_2] : lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3; S5[lc_3] : lc_3 >= 0 and lc_3 <= -1 + N_3 }";
		String idSchedules = "[N_3, N_2, N_1, T] -> { S0[lc_3, lc_2, lc_1] -> [0, lc_3, 0, lc_2, 0, lc_1, 0]; S2[lc_3, lc_2, lc_1] -> [0, lc_3, 0, lc_2, 2, lc_1, 0]; S5[lc_3] -> [1, lc_3, 1, 0, 0, 0, 0]; S7[lc_3, lc_2, lc_1] -> [1, lc_3, 2, lc_2, 1, lc_1, 0]; S3[lc_3, lc_2] -> [1, lc_3, 0, lc_2, 0, 0, 0]; S4[lc_3, lc_2, lc_1] -> [1, lc_3, 0, lc_2, 1, lc_1, 0]; S1[lc_3, lc_2] -> [0, lc_3, 0, lc_2, 1, 0, 0]; S6[lc_3, lc_2] -> [1, lc_3, 2, lc_2, 0, 0, 0] }";
		String writes = "[N_3, N_2, N_1, T] -> { S2[lc_3, lc_2, lc_1] -> result[]; S5[lc_3] -> result[]; S6[lc_3, lc_2] -> result[]; S0[lc_3, lc_2, lc_1] -> result[]; S4[lc_3, lc_2, lc_1] -> result[]; S3[lc_3, lc_2] -> result[]; S1[lc_3, lc_2] -> result[]; S7[lc_3, lc_2, lc_1] -> result[] }";
		String reads = "[N_3, N_2, N_1, T] -> { S2[lc_3, lc_2, lc_1] -> result[]; S5[lc_3] -> result[]; S6[lc_3, lc_2] -> result[]; S0[lc_3, lc_2, lc_1] -> result[]; S4[lc_3, lc_2, lc_1] -> result[]; S3[lc_3, lc_2] -> result[]; S1[lc_3, lc_2] -> result[]; S7[lc_3, lc_2, lc_1] -> result[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N_3, N_2, N_1, T] -> { S7[lc_3, lc_2, lc_1] -> S7[lc_3, lc_2, -1 + lc_1] : lc_1 >= 1 and lc_1 <= -1 + N_1 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3; S3[lc_3, 0] -> S6[-1 + lc_3, -1 + N_2] : N_1 <= 0 and N_2 >= 1 and lc_3 >= 1 and lc_3 <= -1 + N_3; S6[lc_3, lc_2] -> S7[lc_3, -1 + lc_2, -1 + N_1] : lc_2 >= 1 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3 and N_1 >= 1; S7[lc_3, lc_2, 0] -> S6[lc_3, lc_2] : lc_3 <= -1 + N_3 and N_1 >= 1 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0; S5[lc_3] -> S4[lc_3, -1 + N_2, -1 + N_1] : lc_3 >= 0 and lc_3 <= -1 + N_3 and N_1 >= 1 and N_2 >= 1; S4[lc_3, lc_2, lc_1] -> S4[lc_3, lc_2, -1 + lc_1] : lc_1 >= 1 and lc_1 <= -1 + N_1 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3; S3[lc_3, 0] -> S7[-1 + lc_3, -1 + N_2, -1 + N_1] : N_1 >= 1 and N_2 >= 1 and lc_3 >= 1 and lc_3 <= -1 + N_3; S2[lc_3, lc_2, 0] -> S1[lc_3, lc_2] : lc_3 <= -1 + N_3 and N_1 >= 1 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0; S3[lc_3, lc_2] -> S4[lc_3, -1 + lc_2, -1 + N_1] : lc_2 >= 1 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3 and N_1 >= 1; S1[lc_3, lc_2] -> S0[lc_3, lc_2, -1 + N_1] : lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3 and N_1 >= 1; S6[lc_3, lc_2] -> S6[lc_3, -1 + lc_2] : lc_2 >= 1 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3 and N_1 <= 0; S0[lc_3, lc_2, lc_1] -> S0[lc_3, lc_2, -1 + lc_1] : lc_1 >= 1 and lc_1 <= -1 + N_1 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3; S5[lc_3] -> S5[-1 + lc_3] : lc_3 >= 1 and lc_3 <= -1 + N_3 and N_2 <= 0; S5[lc_3] -> S3[lc_3, -1 + N_2] : lc_3 >= 0 and lc_3 <= -1 + N_3 and N_1 <= 0 and N_2 >= 1; S2[lc_3, lc_2, lc_1] -> S2[lc_3, lc_2, -1 + lc_1] : lc_1 >= 1 and lc_1 <= -1 + N_1 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3; S4[lc_3, lc_2, 0] -> S3[lc_3, lc_2] : lc_3 <= -1 + N_3 and N_1 >= 1 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_3 >= 0; S3[lc_3, lc_2] -> S3[lc_3, -1 + lc_2] : lc_2 >= 1 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3 and N_1 <= 0; S3[0, 0] -> S2[-1 + N_3, -1 + N_2, -1 + N_1] : N_1 >= 1 and N_2 >= 1 and N_3 >= 1; S6[lc_3, 0] -> S5[lc_3] : N_2 >= 1 and lc_3 >= 0 and lc_3 <= -1 + N_3; S1[lc_3, lc_2] -> S1[lc_3, -1 + lc_2] : lc_2 >= 1 and lc_2 <= -1 + N_2 and lc_3 >= 0 and lc_3 <= -1 + N_3 and N_1 <= 0; S1[lc_3, 0] -> S1[-1 + lc_3, -1 + N_2] : N_1 <= 0 and N_2 >= 1 and lc_3 >= 1 and lc_3 <= -1 + N_3; S3[0, 0] -> S1[-1 + N_3, -1 + N_2] : N_1 <= 0 and N_2 >= 1 and N_3 >= 1; S0[lc_3, lc_2, 0] -> S2[lc_3, -1 + lc_2, -1 + N_1] : lc_3 <= -1 + N_3 and N_1 >= 1 and lc_2 >= 1 and lc_2 <= -1 + N_2 and lc_3 >= 0; S0[lc_3, 0, 0] -> S2[-1 + lc_3, -1 + N_2, -1 + N_1] : lc_3 <= -1 + N_3 and N_1 >= 1 and lc_3 >= 1 and N_2 >= 1 }";
		String valueBasedPlutoSchedule = "[N_3, N_2, N_1, T] -> { S5[lc_3] -> [lc_3, 0, 0, 0]; S1[lc_3, lc_2] -> [-N_3 + lc_3, lc_2, 0, 1]; S4[lc_3, lc_2, lc_1] -> [lc_3, -N_2 + lc_2, lc_1, 1]; S3[lc_3, lc_2] -> [lc_3, -N_2 + lc_2, 0, 0]; S6[lc_3, lc_2] -> [lc_3, lc_2, 0, 1]; S2[lc_3, lc_2, lc_1] -> [-N_3 + lc_3, lc_2, lc_1, 2]; S0[lc_3, lc_2, lc_1] -> [-N_3 + lc_3, lc_2, -N_1 + lc_1, 0]; S7[lc_3, lc_2, lc_1] -> [lc_3, lc_2, lc_1, 2] }";
		String valueBasedFeautrierSchedule = "[N_3, N_2, N_1, T] -> { S5[lc_3] -> [1 + 3lc_3, 0, 0, 0]; S1[lc_3, lc_2] -> [-N_3 + lc_3, lc_2, 1, 0]; S4[lc_3, lc_2, lc_1] -> [3lc_3, lc_2, 1, lc_1]; S3[lc_3, lc_2] -> [3lc_3, lc_2, 0, 0]; S6[lc_3, lc_2] -> [2 + 3lc_3, lc_2, 0, 0]; S2[lc_3, lc_2, lc_1] -> [-N_3 + lc_3, lc_2, 2, lc_1]; S0[lc_3, lc_2, lc_1] -> [-N_3 + lc_3, lc_2, 0, lc_1]; S7[lc_3, lc_2, lc_1] -> [2 + 3lc_3, lc_2, 1, lc_1] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N_3, N_2, N_1, T] -> { S7[lc_3, lc_2, lc_1] -> S7[lc_3, lc_2, -1 + lc_1] : lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_1 >= 1 and lc_1 <= -1 + N_1; S3[lc_3, 0] -> S6[-1 + lc_3, -1 + N_2] : N_2 >= 1 and N_1 <= 0 and lc_3 >= 1 and lc_3 <= -1 + N_3; S6[lc_3, lc_2] -> S7[lc_3, -1 + lc_2, -1 + N_1] : N_1 >= 1 and lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 1 and lc_2 <= -1 + N_2; S7[lc_3, lc_2, 0] -> S6[lc_3, lc_2] : N_1 >= 1 and lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 0 and lc_2 <= -1 + N_2; S5[lc_3] -> S4[lc_3, -1 + N_2, -1 + N_1] : N_2 >= 1 and N_1 >= 1 and lc_3 >= 0 and lc_3 <= -1 + N_3; S4[lc_3, lc_2, lc_1] -> S4[lc_3, lc_2, -1 + lc_1] : lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_1 >= 1 and lc_1 <= -1 + N_1; S3[lc_3, 0] -> S7[-1 + lc_3, -1 + N_2, -1 + N_1] : N_2 >= 1 and N_1 >= 1 and lc_3 >= 1 and lc_3 <= -1 + N_3; S2[lc_3, lc_2, 0] -> S1[lc_3, lc_2] : N_1 >= 1 and lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 0 and lc_2 <= -1 + N_2; S3[lc_3, lc_2] -> S4[lc_3, -1 + lc_2, -1 + N_1] : N_1 >= 1 and lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 1 and lc_2 <= -1 + N_2; S1[lc_3, lc_2] -> S0[lc_3, lc_2, -1 + N_1] : N_1 >= 1 and lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 0 and lc_2 <= -1 + N_2; S6[lc_3, lc_2] -> S6[lc_3, -1 + lc_2] : N_1 <= 0 and lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 1 and lc_2 <= -1 + N_2; S0[lc_3, lc_2, lc_1] -> S0[lc_3, lc_2, -1 + lc_1] : lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_1 >= 1 and lc_1 <= -1 + N_1; S5[lc_3] -> S5[-1 + lc_3] : N_2 <= 0 and lc_3 >= 1 and lc_3 <= -1 + N_3; S5[lc_3] -> S3[lc_3, -1 + N_2] : N_2 >= 1 and N_1 <= 0 and lc_3 >= 0 and lc_3 <= -1 + N_3; S2[lc_3, lc_2, lc_1] -> S2[lc_3, lc_2, -1 + lc_1] : lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 0 and lc_2 <= -1 + N_2 and lc_1 >= 1 and lc_1 <= -1 + N_1; S4[lc_3, lc_2, 0] -> S3[lc_3, lc_2] : N_1 >= 1 and lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 0 and lc_2 <= -1 + N_2; S3[lc_3, lc_2] -> S3[lc_3, -1 + lc_2] : N_1 <= 0 and lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 1 and lc_2 <= -1 + N_2; S3[0, 0] -> S2[-1 + N_3, -1 + N_2, -1 + N_1] : N_3 >= 1 and N_2 >= 1 and N_1 >= 1; S6[lc_3, 0] -> S5[lc_3] : N_2 >= 1 and lc_3 >= 0 and lc_3 <= -1 + N_3; S1[lc_3, lc_2] -> S1[lc_3, -1 + lc_2] : N_1 <= 0 and lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 1 and lc_2 <= -1 + N_2; S1[lc_3, 0] -> S1[-1 + lc_3, -1 + N_2] : N_2 >= 1 and N_1 <= 0 and lc_3 >= 1 and lc_3 <= -1 + N_3; S3[0, 0] -> S1[-1 + N_3, -1 + N_2] : N_3 >= 1 and N_2 >= 1 and N_1 <= 0; S0[lc_3, lc_2, 0] -> S2[lc_3, -1 + lc_2, -1 + N_1] : N_1 >= 1 and lc_3 >= 0 and lc_3 <= -1 + N_3 and lc_2 >= 1 and lc_2 <= -1 + N_2; S0[lc_3, 0, 0] -> S2[-1 + lc_3, -1 + N_2, -1 + N_1] : N_2 >= 1 and N_1 >= 1 and lc_3 >= 1 and lc_3 <= -1 + N_3 }";
		String memoryBasedPlutoSchedule = "[N_3, N_2, N_1, T] -> { S5[lc_3] -> [lc_3, 0, 0, 0]; S1[lc_3, lc_2] -> [-N_3 + lc_3, lc_2, 0, 1]; S4[lc_3, lc_2, lc_1] -> [lc_3, -N_2 + lc_2, lc_1, 1]; S3[lc_3, lc_2] -> [lc_3, -N_2 + lc_2, 0, 0]; S6[lc_3, lc_2] -> [lc_3, lc_2, 0, 1]; S2[lc_3, lc_2, lc_1] -> [-N_3 + lc_3, lc_2, lc_1, 2]; S0[lc_3, lc_2, lc_1] -> [-N_3 + lc_3, lc_2, -N_1 + lc_1, 0]; S7[lc_3, lc_2, lc_1] -> [lc_3, lc_2, lc_1, 2] }";
		String memoryBasedFeautrierSchedule = "[N_3, N_2, N_1, T] -> { S5[lc_3] -> [1 + 3lc_3, 0, 0, 0]; S1[lc_3, lc_2] -> [-N_3 + lc_3, lc_2, 1, 0]; S4[lc_3, lc_2, lc_1] -> [3lc_3, lc_2, 1, lc_1]; S3[lc_3, lc_2] -> [3lc_3, lc_2, 0, 0]; S6[lc_3, lc_2] -> [2 + 3lc_3, lc_2, 0, 0]; S2[lc_3, lc_2, lc_1] -> [-N_3 + lc_3, lc_2, 2, lc_1]; S0[lc_3, lc_2, lc_1] -> [-N_3 + lc_3, lc_2, 0, lc_1]; S7[lc_3, lc_2, lc_1] -> [2 + 3lc_3, lc_2, 1, lc_1] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #146
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : T2_T2/scop_0
	 *   block : 
	 *     for : 0 <= k <= N-2 (stride = 1)
	 *       block : 
	 *         for : N-2 <= i <= k (stride = 1)
	 *           block : 
	 *             S0 (depth = 2) [angle=add(M[i+1][k],M[i][k])]
	 *             ([angle]) = f([M[i+1][k], M[i][k]])
	 *             (Domain = [N] -> {S0[k,i] : (i-N+2 >= 0) and (k-i >= 0) and (k >= 0) and (-k+N-2 >= 0)})
	 *             for : k <= j <= N-1 (stride = 1)
	 *               S1 (depth = 3) [M[i][j]=add(add(M[i][j],M[i+1][j]),angle)]
	 *               ([M[i][j]]) = f([M[i][j], M[i+1][j], angle])
	 *               (Domain = [N] -> {S1[k,i,j] : (-k+j >= 0) and (-j+N-1 >= 0) and (i-N+2 >= 0) and (k-i >= 0) and (k >= 0) and (-k+N-2 >= 0)})
	 *         S2 (depth = 1) [angle=add(add(add(angle,IntExpr(1)),M[k+1][k]),M[k][k])]
	 *         ([angle]) = f([angle, M[k+1][k], M[k][k]])
	 *         (Domain = [N] -> {S2[k] : (k >= 0) and (-k+N-2 >= 0)})
	 *         for : N-2 <= i <= k (stride = 1)
	 *           block : 
	 *             S3 (depth = 2) [angle=add(M[i+1][k],M[i][k])]
	 *             ([angle]) = f([M[i+1][k], M[i][k]])
	 *             (Domain = [N] -> {S3[k,i] : (i-N+2 >= 0) and (k-i >= 0) and (k >= 0) and (-k+N-2 >= 0)})
	 *             for : k <= j <= N-1 (stride = 1)
	 *               S4 (depth = 3) [M[i][j]=add(add(M[i][j],M[i+1][j]),angle)]
	 *               ([M[i][j]]) = f([M[i][j], M[i+1][j], angle])
	 *               (Domain = [N] -> {S4[k,i,j] : (-k+j >= 0) and (-j+N-1 >= 0) and (i-N+2 >= 0) and (k-i >= 0) and (k >= 0) and (-k+N-2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	//Lock the process in Jenkins
//	public void test_polymodel_src_generic_for_loops_for13_GScopRoot_T2_scop_0_ () {
//		String path = "polymodel_src_generic_for_loops_for13_GScopRoot_T2_scop_0_";
//		// SCoP Extraction Data
//		String domains = "[N] -> { S1[-2 + N, -2 + N, j] : j >= -2 + N and j <= -1 + N and N >= 2; S0[-2 + N, -2 + N] : N >= 2; S3[-2 + N, -2 + N] : N >= 2; S4[-2 + N, -2 + N, j] : j >= -2 + N and j <= -1 + N and N >= 2; S2[k] : k >= 0 and k <= -2 + N }";
//		String idSchedules = "[N] -> { S3[k, i] -> [0, k, 2, i, 0, 0, 0]; S0[k, i] -> [0, k, 0, i, 0, 0, 0]; S4[k, i, j] -> [0, k, 2, i, 1, j, 0]; S1[k, i, j] -> [0, k, 0, i, 1, j, 0]; S2[k] -> [0, k, 1, 0, 0, 0, 0] }";
//		String writes = "[N] -> { S1[k, i, j] -> M[i, j]; S4[k, i, j] -> M[i, j]; S0[k, i] -> angle[]; S2[k] -> angle[]; S3[k, i] -> angle[] }";
//		String reads = "[N] -> { S2[k] -> M[1 + k, k]; S2[k] -> M[k, k]; S1[k, i, j] -> M[1 + i, j]; S1[k, i, j] -> M[i, j]; S1[k, i, j] -> angle[]; S4[k, i, j] -> M[1 + i, j]; S4[k, i, j] -> M[i, j]; S0[k, i] -> M[1 + i, k]; S0[k, i] -> M[i, k]; S2[k] -> angle[]; S4[k, i, j] -> angle[]; S3[k, i] -> M[1 + i, k]; S3[k, i] -> M[i, k] }";
//		// Value Based PRDG & Schedules
//		String valueBasedPrdg = "[N] -> { S4[-2 + N, -2 + N, j] -> S3[-2 + N, -2 + N] : j >= -2 + N and j <= -1 + N and N >= 2; S2[-2 + N] -> S0[-2 + N, -2 + N] : N >= 2; S4[-2 + N, -2 + N, j] -> S1[-2 + N, -2 + N, j] : j >= -2 + N and j <= -1 + N and N >= 2; S2[k] -> S2[-1 + k] : k >= 1 and k <= -3 + N; S3[-2 + N, -2 + N] -> S1[-2 + N, -2 + N, -2 + N] : N >= 2; S2[-2 + N] -> S1[-2 + N, -2 + N, -2 + N] : N >= 2; S1[-2 + N, -2 + N, j] -> S0[-2 + N, -2 + N] : j >= -2 + N and j <= -1 + N and N >= 2 }";
//		String valueBasedPlutoSchedule = "[N] -> { S3[k, i] -> [N, 3]; S2[k] -> [k, 2]; S1[k, i, j] -> [j, 1]; S0[k, i] -> [0, 0]; S4[k, i, j] -> [2 + j, 4] }";
//		String valueBasedFeautrierSchedule = "[N] -> { S4[k, i, j] -> [j]; S0[k, i] -> [0]; S1[k, i, j] -> [j]; S3[k, i] -> [0]; S2[k] -> [k] }";
//		// Memory Based PRDG & Schedules
//		String memoryBasedPrdg = "[N] -> { S4[-2 + N, -2 + N, j] -> S3[-2 + N, -2 + N] : N >= 2 and j >= -2 + N and j <= -1 + N; S4[-2 + N, -2 + N, -2 + N] -> S2[-2 + N] : N >= 2; S3[-2 + N, -2 + N] -> S2[-2 + N] : N >= 2; S0[-2 + N, -2 + N] -> S2[-3 + N] : N >= 3; S2[-2 + N] -> S0[-2 + N, -2 + N] : N >= 2; S4[-2 + N, -2 + N, j] -> S1[-2 + N, -2 + N, j] : N >= 2 and j >= -2 + N and j <= -1 + N; S2[k] -> S2[-1 + k] : k >= 1 and k <= -3 + N; S3[-2 + N, -2 + N] -> S1[-2 + N, -2 + N, -2 + N] : N >= 2; S2[-2 + N] -> S1[-2 + N, -2 + N, j] : N >= 2 and j >= -2 + N and j <= -1 + N; S1[-2 + N, -2 + N, j] -> S0[-2 + N, -2 + N] : N >= 2 and j >= -2 + N and j <= -1 + N }";
//		String memoryBasedPlutoSchedule = "[N] -> { S3[k, i] -> [1 + N, 1]; S2[k] -> [3 + k, 0]; S1[k, i, j] -> [2 + j, 0]; S0[k, i] -> [N, 0]; S4[k, i, j] -> [3 + j, 2] }";
//		String memoryBasedFeautrierSchedule = "[N] -> { S4[k, i, j] -> [j, 0, 0]; S0[k, i] -> [0, 0, 0]; S1[k, i, j] -> [0, 1, j]; S2[k] -> [2 - N + k, 2, 0]; S3[k, i] -> [0, 0, 0] }";
//		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
//	}
	
	// SCoP #147
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : main_main/scop_0
	 *   for : 1 <= f <= 9 (stride = 1)
	 *     block : 
	 *       S0 (depth = 1) [t=2.5]
	 *       ([t]) = f([])
	 *       (Domain = [] -> {S0[f] : (f-1 >= 0) and (-f+9 >= 0)})
	 *       S1 (depth = 1) [a=mul(a,IntExpr(f))]
	 *       ([a]) = f([a])
	 *       (Domain = [] -> {S1[f] : (f-1 >= 0) and (-f+9 >= 0)})
	 *       S2 (depth = 1) [d=mul(d,t)]
	 *       ([d]) = f([d, t])
	 *       (Domain = [] -> {S2[f] : (f-1 >= 0) and (-f+9 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_elementary_for_GScopRoot_main_scop_0_ () {
		String path = "basics_elementary_for_GScopRoot_main_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S0[f] : f >= 1 and f <= 9; S1[f] : f >= 1 and f <= 9; S2[f] : f >= 1 and f <= 9 }";
		String idSchedules = "{ S0[f] -> [0, f, 0]; S2[f] -> [0, f, 2]; S1[f] -> [0, f, 1] }";
		String writes = "{ S0[f] -> t[]; S1[f] -> a[]; S2[f] -> d[] }";
		String reads = "{ S1[f] -> a[]; S2[f] -> d[]; S2[f] -> t[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S2[f] -> S2[-1 + f] : f >= 2 and f <= 9; S1[f] -> S1[-1 + f] : f >= 2 and f <= 9; S2[f] -> S0[f] : f >= 1 and f <= 9 }";
		String valueBasedPlutoSchedule = "{ S1[f] -> [f, 0]; S2[f] -> [f, 1]; S0[f] -> [f, 0] }";
		String valueBasedFeautrierSchedule = "{ S2[f] -> [f]; S1[f] -> [f]; S0[f] -> [f] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S2[f] -> S2[-1 + f] : f <= 9 and f >= 2; S0[f] -> S2[-1 + f] : f <= 9 and f >= 2; S1[f] -> S1[-1 + f] : f <= 9 and f >= 2; S0[f] -> S0[-1 + f] : f >= 2 and f <= 9; S2[f] -> S0[f] : f >= 1 and f <= 9 }";
		String memoryBasedPlutoSchedule = "{ S1[f] -> [f, 0]; S2[f] -> [f, 1]; S0[f] -> [f, 0] }";
		String memoryBasedFeautrierSchedule = "{ S1[f] -> [f, 0]; S2[f] -> [f, 1]; S0[f] -> [f, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #148
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : main_scop_new
	 *   block : 
	 *     S0 (depth = 0) [a=IntExpr(1)]
	 *     ([a]) = f([])
	 *     (Domain = [] -> {S0[] : })
	 *     S1 (depth = 0) [i=IntExpr(0)]
	 *     ([i]) = f([])
	 *     (Domain = [] -> {S1[] : })
	 *     S2 (depth = 0) [d=IntExpr(0)]
	 *     ([d]) = f([])
	 *     (Domain = [] -> {S2[] : })
	 *     for : 1 <= i <= 9 (stride = 1)
	 *       block : 
	 *         S3 (depth = 1) [t=2.5]
	 *         ([t]) = f([])
	 *         (Domain = [] -> {S3[i] : (i-1 >= 0) and (-i+9 >= 0)})
	 *         S4 (depth = 1) [a=mul(a,IntExpr(i))]
	 *         ([a]) = f([a])
	 *         (Domain = [] -> {S4[i] : (i-1 >= 0) and (-i+9 >= 0)})
	 *         S5 (depth = 1) [d=mul(d,t)]
	 *         ([d]) = f([d, t])
	 *         (Domain = [] -> {S5[i] : (i-1 >= 0) and (-i+9 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_elementary_for_GScopRoot_scop_new_ () {
		String path = "basics_elementary_for_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "{ S2[]; S5[i] : i >= 1 and i <= 9; S1[]; S4[i] : i >= 1 and i <= 9; S3[i] : i >= 1 and i <= 9; S0[] }";
		String idSchedules = "{ S3[i] -> [3, i, 0]; S1[] -> [1, 0, 0]; S5[i] -> [3, i, 2]; S0[] -> [0, 0, 0]; S4[i] -> [3, i, 1]; S2[] -> [2, 0, 0] }";
		String writes = "{ S4[i] -> a[]; S5[i] -> d[]; S2[] -> d[]; S1[] -> i[]; S0[] -> a[]; S3[i] -> t[] }";
		String reads = "{ S4[i] -> a[]; S5[i] -> t[]; S5[i] -> d[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{ S5[1] -> S2[]; S5[i] -> S3[i] : i >= 1 and i <= 9; S4[1] -> S0[]; S4[i] -> S4[-1 + i] : i >= 2 and i <= 9; S5[i] -> S5[-1 + i] : i >= 2 and i <= 9 }";
		String valueBasedPlutoSchedule = "{ S1[] -> [0, 0]; S2[] -> [0, 2]; S4[i] -> [i, 1]; S0[] -> [0, 0]; S3[i] -> [i, 0]; S5[i] -> [i, 1] }";
		String valueBasedFeautrierSchedule = "{ S2[] -> [0]; S0[] -> [0]; S3[i] -> [i]; S4[i] -> [i]; S5[i] -> [i]; S1[] -> [0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{ S5[i] -> S5[-1 + i] : i <= 9 and i >= 2; S4[i] -> S4[-1 + i] : i <= 9 and i >= 2; S3[i] -> S5[-1 + i] : i <= 9 and i >= 2; S5[i] -> S3[i] : i >= 1 and i <= 9; S4[1] -> S0[]; S5[1] -> S2[]; S3[i] -> S3[-1 + i] : i >= 2 and i <= 9 }";
		String memoryBasedPlutoSchedule = "{ S1[] -> [0, 0]; S2[] -> [0, 2]; S4[i] -> [i, 1]; S0[] -> [0, 0]; S3[i] -> [i, 0]; S5[i] -> [i, 1] }";
		String memoryBasedFeautrierSchedule = "{ S1[] -> [0, 0]; S2[] -> [0, 0]; S4[i] -> [i, 0]; S0[] -> [0, 0]; S3[i] -> [i, 0]; S5[i] -> [i, 1] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #149
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : gauss_gauss/scop_0
	 *   block : 
	 *     S0 (depth = 0) [tot[0]=IntExpr(0)]
	 *     ([tot[0]]) = f([])
	 *     (Domain = [T,N,M] -> {S0[] : })
	 *     for : T-1 <= k <= T+1 (stride = 1)
	 *       S1 (depth = 1) [tot[k-T+2]=add(tot[k-T+1],Gauss[k-T])]
	 *       ([tot[k-T+2]]) = f([tot[k-T+1], Gauss[k-T]])
	 *       (Domain = [T,N,M] -> {S1[k] : (k-T+1 >= 0) and (-k+T+1 >= 0)})
	 *     for : T-1 <= k <= T+1 (stride = 1)
	 *       S2 (depth = 1) [tot[k-T+2]=add(tot[k-T+1],Gauss[k-T])]
	 *       ([tot[k-T+2]]) = f([tot[k-T+1], Gauss[k-T]])
	 *       (Domain = [T,N,M] -> {S2[k] : (k-T+1 >= 0) and (-k+T+1 >= 0)})
	 *     for : 1 <= x <= N-2 (stride = 1)
	 *       for : 0 <= y <= M-1 (stride = 1)
	 *         block : 
	 *           S3 (depth = 2) [g_acc1[x][y][0]=IntExpr(0)]
	 *           ([g_acc1[x][y][0]]) = f([])
	 *           (Domain = [T,N,M] -> {S3[x,y] : (y >= 0) and (-y+M-1 >= 0) and (x-1 >= 0) and (-x+N-2 >= 0)})
	 *           for : T-1 <= k <= T+1 (stride = 1)
	 *             S4 (depth = 3) [g_acc1[x][y][k-T+2]=add(g_acc1[x][y][k-T+1],mul(in_image[x+k][y],Gauss[k-T]))]
	 *             ([g_acc1[x][y][k-T+2]]) = f([g_acc1[x][y][k-T+1], in_image[x+k][y], Gauss[k-T]])
	 *             (Domain = [T,N,M] -> {S4[x,y,k] : (k-T+1 >= 0) and (-k+T+1 >= 0) and (y >= 0) and (-y+M-1 >= 0) and (x-1 >= 0) and (-x+N-2 >= 0)})
	 *           S5 (depth = 2) [g_tmp[x][y]=div(g_acc1[x][y][3],tot[3])]
	 *           ([g_tmp[x][y]]) = f([g_acc1[x][y][3], tot[3]])
	 *           (Domain = [T,N,M] -> {S5[x,y] : (y >= 0) and (-y+M-1 >= 0) and (x-1 >= 0) and (-x+N-2 >= 0)})
	 *     for : 1 <= x <= N-2 (stride = 1)
	 *       for : 1 <= y <= M-2 (stride = 1)
	 *         block : 
	 *           S6 (depth = 2) [g_acc2[x][y][0]=IntExpr(0)]
	 *           ([g_acc2[x][y][0]]) = f([])
	 *           (Domain = [T,N,M] -> {S6[x,y] : (y-1 >= 0) and (-y+M-2 >= 0) and (x-1 >= 0) and (-x+N-2 >= 0)})
	 *           for : T-1 <= k <= T+1 (stride = 1)
	 *             S7 (depth = 3) [g_acc2[x][y][k-T+2]=add(g_acc2[x][y][k-T+1],mul(g_tmp[x][y+k-T],Gauss[k-T]))]
	 *             ([g_acc2[x][y][k-T+2]]) = f([g_acc2[x][y][k-T+1], g_tmp[x][y+k-T], Gauss[k-T]])
	 *             (Domain = [T,N,M] -> {S7[x,y,k] : (k-T+1 >= 0) and (-k+T+1 >= 0) and (y-1 >= 0) and (-y+M-2 >= 0) and (x-1 >= 0) and (-x+N-2 >= 0)})
	 *           S8 (depth = 2) [gauss_image[x][y]=div(g_acc2[x][y][3],tot[3])]
	 *           ([gauss_image[x][y]]) = f([g_acc2[x][y][3], tot[3]])
	 *           (Domain = [T,N,M] -> {S8[x,y] : (y-1 >= 0) and (-y+M-2 >= 0) and (x-1 >= 0) and (-x+N-2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_gauss_GScopRoot_gauss_scop_0_ () {
		String path = "basics_oldFiles_gauss_GScopRoot_gauss_scop_0_";
		// SCoP Extraction Data
		String domains = "[T, N, M] -> { S0[]; S3[x, y] : y >= 0 and y <= -1 + M and x >= 1 and x <= -2 + N; S1[k] : k >= -1 + T and k <= 1 + T; S5[x, y] : y >= 0 and y <= -1 + M and x >= 1 and x <= -2 + N; S2[k] : k >= -1 + T and k <= 1 + T; S6[x, y] : y >= 1 and y <= -2 + M and x >= 1 and x <= -2 + N; S8[x, y] : y >= 1 and y <= -2 + M and x >= 1 and x <= -2 + N; S4[x, y, k] : k >= -1 + T and k <= 1 + T and y >= 0 and y <= -1 + M and x >= 1 and x <= -2 + N; S7[x, y, k] : k >= -1 + T and k <= 1 + T and y >= 1 and y <= -2 + M and x >= 1 and x <= -2 + N }";
		String idSchedules = "[T, N, M] -> { S2[k] -> [2, k, 0, 0, 0, 0, 0]; S5[x, y] -> [3, x, 0, y, 2, 0, 0]; S3[x, y] -> [3, x, 0, y, 0, 0, 0]; S6[x, y] -> [4, x, 0, y, 0, 0, 0]; S7[x, y, k] -> [4, x, 0, y, 1, k, 0]; S4[x, y, k] -> [3, x, 0, y, 1, k, 0]; S8[x, y] -> [4, x, 0, y, 2, 0, 0]; S0[] -> [0, 0, 0, 0, 0, 0, 0]; S1[k] -> [1, k, 0, 0, 0, 0, 0] }";
		String writes = "[T, N, M] -> { S4[x, y, k] -> g_acc1[x, y, 2 - T + k]; S0[] -> tot[0]; S5[x, y] -> g_tmp[x, y]; S6[x, y] -> g_acc2[x, y, 0]; S1[k] -> tot[2 - T + k]; S8[x, y] -> gauss_image[x, y]; S2[k] -> tot[2 - T + k]; S3[x, y] -> g_acc1[x, y, 0]; S7[x, y, k] -> g_acc2[x, y, 2 - T + k] }";
		String reads = "[T, N, M] -> { S4[x, y, k] -> g_acc1[x, y, 1 - T + k]; S2[k] -> Gauss[-T + k]; S5[x, y] -> g_acc1[x, y, 3]; S5[x, y] -> tot[3]; S8[x, y] -> g_acc2[x, y, 3]; S1[k] -> tot[1 - T + k]; S7[x, y, k] -> Gauss[-T + k]; S1[k] -> Gauss[-T + k]; S7[x, y, k] -> g_tmp[x, -T + y + k]; S8[x, y] -> tot[3]; S2[k] -> tot[1 - T + k]; S7[x, y, k] -> g_acc2[x, y, 1 - T + k]; S4[x, y, k] -> in_image[x + k, y]; S4[x, y, k] -> Gauss[-T + k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[T, N, M] -> { S7[x, y, -1 + T] -> S6[x, y] : x >= 1 and x <= -2 + N and y >= 1 and y <= -2 + M; S5[x, y] -> S2[1 + T] : y >= 0 and y <= -1 + M and x >= 1 and x <= -2 + N; S1[k] -> S1[-1 + k] : k >= T and k <= 1 + T; S8[x, y] -> S7[x, y, 1 + T] : y >= 1 and y <= -2 + M and x >= 1 and x <= -2 + N; S7[x, y, k] -> S7[x, y, -1 + k] : k >= T and k <= 1 + T and y >= 1 and y <= -2 + M and x >= 1 and x <= -2 + N; S4[x, y, -1 + T] -> S3[x, y] : x >= 1 and x <= -2 + N and y >= 0 and y <= -1 + M; S2[-1 + T] -> S0[]; S1[-1 + T] -> S0[]; S8[x, y] -> S2[1 + T] : y >= 1 and y <= -2 + M and x >= 1 and x <= -2 + N; S5[x, y] -> S4[x, y, 1 + T] : y >= 0 and y <= -1 + M and x >= 1 and x <= -2 + N; S4[x, y, k] -> S4[x, y, -1 + k] : k >= T and k <= 1 + T and y >= 0 and y <= -1 + M and x >= 1 and x <= -2 + N; S2[k] -> S2[-1 + k] : k >= T and k <= 1 + T; S7[x, y, k] -> S5[x, -T + y + k] : k >= -1 + T and k <= 1 + T and y >= 1 and y <= -2 + M and x >= 1 and x <= -2 + N }";
		String valueBasedPlutoSchedule = "[T, N, M] -> { S7[x, y, k] -> [x, 1 + x + y, 1 - T + x + y + k, 2]; S2[k] -> [0, 0, 1 - T + k, 6]; S8[x, y] -> [x, 1 + x + y, 2 + x + y, 3]; S5[x, y] -> [x, x + y, 1 + x + y, 1]; S6[x, y] -> [0, x, y, 4]; S1[k] -> [0, 0, 1 - T + k, 7]; S0[] -> [0, 0, 0, 5]; S3[x, y] -> [0, x, y, 8]; S4[x, y, k] -> [x, x + y, -T + x + y + k, 0] }";
		String valueBasedFeautrierSchedule = "[T, N, M] -> { S5[x, y] -> [4, x, y]; S0[] -> [0, 0, 0]; S8[x, y] -> [8, x, y]; S3[x, y] -> [0, x, y]; S4[x, y, k] -> [2 - T + k, y, x]; S6[x, y] -> [0, x, y]; S2[k] -> [2 - T + k, 0, 0]; S7[x, y, k] -> [6 - T + k, y, x]; S1[k] -> [2 - T + k, 0, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[T, N, M] -> { S7[x, y, -1 + T] -> S6[x, y] : x >= 1 and x <= -2 + N and y >= 1 and y <= -2 + M; S5[x, y] -> S2[1 + T] : y >= 0 and y <= -1 + M and x >= 1 and x <= -2 + N; S1[k] -> S1[-1 + k] : k >= T and k <= 1 + T; S8[x, y] -> S7[x, y, 1 + T] : y >= 1 and y <= -2 + M and x >= 1 and x <= -2 + N; S7[x, y, k] -> S7[x, y, -1 + k] : k >= T and k <= 1 + T and y >= 1 and y <= -2 + M and x >= 1 and x <= -2 + N; S4[x, y, -1 + T] -> S3[x, y] : x >= 1 and x <= -2 + N and y >= 0 and y <= -1 + M; S2[-1 + T] -> S0[]; S1[-1 + T] -> S0[]; S8[x, y] -> S2[1 + T] : y >= 1 and y <= -2 + M and x >= 1 and x <= -2 + N; S5[x, y] -> S4[x, y, 1 + T] : y >= 0 and y <= -1 + M and x >= 1 and x <= -2 + N; S4[x, y, k] -> S4[x, y, -1 + k] : k >= T and k <= 1 + T and y >= 0 and y <= -1 + M and x >= 1 and x <= -2 + N; S2[k] -> S2[-1 + k] : k >= T and k <= 1 + T; S2[k] -> S1[k'] : k' <= 1 + k and k' <= 1 + T and k >= -1 + T and k' >= k; S7[x, y, k] -> S5[x, -T + y + k] : k >= -1 + T and k <= 1 + T and y >= 1 and y <= -2 + M and x >= 1 and x <= -2 + N }";
		String memoryBasedPlutoSchedule = "[T, N, M] -> { S7[x, y, k] -> [x, 1 + x + y, 2 - T + x + y + k, 1]; S2[k] -> [0, 0, 2 - T + k, 7]; S8[x, y] -> [x, 1 + x + y, 3 + x + y, 3]; S5[x, y] -> [x, x + y, 2 + x + y, 0]; S6[x, y] -> [0, x, y, 4]; S1[k] -> [0, 0, 1 - T + k, 6]; S0[] -> [0, 0, 0, 5]; S3[x, y] -> [0, x, y, 8]; S4[x, y, k] -> [x, x + y, -T + x + y + k, 2] }";
		String memoryBasedFeautrierSchedule = "[T, N, M] -> { S5[x, y] -> [6, x, y]; S0[] -> [0, 0, 0]; S8[x, y] -> [10, x, y]; S3[x, y] -> [0, x, y]; S4[x, y, k] -> [2 - T + k, y, x]; S6[x, y] -> [0, x, y]; S2[k] -> [4 - T + k, 0, 0]; S7[x, y, k] -> [8 - T + k, y, x]; S1[k] -> [2 - T + k, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #150
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : P7Viterbi_scop_new
	 *   block : 
	 *     S0 (depth = 0) [XTN_LOOP=IntExpr(655)]
	 *     ([XTN_LOOP]) = f([])
	 *     (Domain = [M] -> {S0[] : })
	 *     S1 (depth = 0) [XTE_LOOP=IntExpr(16735)]
	 *     ([XTE_LOOP]) = f([])
	 *     (Domain = [M] -> {S1[] : })
	 *     S2 (depth = 0) [XTJ_LOOP=IntExpr(7534)]
	 *     ([XTJ_LOOP]) = f([])
	 *     (Domain = [M] -> {S2[] : })
	 *     S3 (depth = 0) [XTC_LOOP=IntExpr(1633)]
	 *     ([XTC_LOOP]) = f([])
	 *     (Domain = [M] -> {S3[] : })
	 *     S4 (depth = 0) [XTC_MOVE=IntExpr(7893)]
	 *     ([XTC_MOVE]) = f([])
	 *     (Domain = [M] -> {S4[] : })
	 *     S5 (depth = 0) [XTN_MOVE=IntExpr(12845)]
	 *     ([XTN_MOVE]) = f([])
	 *     (Domain = [M] -> {S5[] : })
	 *     S6 (depth = 0) [XTJ_MOVE=IntExpr(32125)]
	 *     ([XTJ_MOVE]) = f([])
	 *     (Domain = [M] -> {S6[] : })
	 *     S7 (depth = 0) [XTE_MOVE=IntExpr(4291)]
	 *     ([XTE_MOVE]) = f([])
	 *     (Domain = [M] -> {S7[] : })
	 *     S8 (depth = 0) [INFTY=IntExpr(1985229328)]
	 *     ([INFTY]) = f([])
	 *     (Domain = [M] -> {S8[] : })
	 *     for : 1 <= k <= M (stride = 1)
	 *       block : 
	 *         S9 (depth = 1) [mmx[0][k]=neg(INFTY)]
	 *         ([mmx[0][k]]) = f([INFTY])
	 *         (Domain = [M] -> {S9[k] : (k-1 >= 0) and (-k+M >= 0)})
	 *         S10 (depth = 1) [imx[0][k]=neg(INFTY)]
	 *         ([imx[0][k]]) = f([INFTY])
	 *         (Domain = [M] -> {S10[k] : (k-1 >= 0) and (-k+M >= 0)})
	 *         S11 (depth = 1) [dmx[0][k]=neg(INFTY)]
	 *         ([dmx[0][k]]) = f([INFTY])
	 *         (Domain = [M] -> {S11[k] : (k-1 >= 0) and (-k+M >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_hmmer_GScopRoot_scop_new_ () {
		String path = "basics_oldFiles_hmmer_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[M] -> { S10[k] : k >= 1 and k <= M; S9[k] : k >= 1 and k <= M; S0[]; S7[]; S3[]; S11[k] : k >= 1 and k <= M; S4[]; S6[]; S5[]; S1[]; S8[]; S2[] }";
		String idSchedules = "[M] -> { S11[k] -> [9, k, 2]; S0[] -> [0, 0, 0]; S6[] -> [6, 0, 0]; S9[k] -> [9, k, 0]; S1[] -> [1, 0, 0]; S3[] -> [3, 0, 0]; S4[] -> [4, 0, 0]; S2[] -> [2, 0, 0]; S5[] -> [5, 0, 0]; S7[] -> [7, 0, 0]; S8[] -> [8, 0, 0]; S10[k] -> [9, k, 1] }";
		String writes = "[M] -> { S8[] -> INFTY[]; S4[] -> XTC_MOVE[]; S9[k] -> mmx[0, k]; S1[] -> XTE_LOOP[]; S5[] -> XTN_MOVE[]; S2[] -> XTJ_LOOP[]; S6[] -> XTJ_MOVE[]; S10[k] -> imx[0, k]; S11[k] -> dmx[0, k]; S7[] -> XTE_MOVE[]; S3[] -> XTC_LOOP[]; S0[] -> XTN_LOOP[] }";
		String reads = "[M] -> { S10[k] -> INFTY[]; S9[k] -> INFTY[]; S11[k] -> INFTY[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[M] -> { S11[k] -> S8[] : k >= 1 and k <= M; S10[k] -> S8[] : k >= 1 and k <= M; S9[k] -> S8[] : k >= 1 and k <= M }";
		String valueBasedPlutoSchedule = "[M] -> { S3[] -> [0, 0]; S11[k] -> [k, 1]; S2[] -> [0, 0]; S0[] -> [0, 0]; S1[] -> [0, 0]; S6[] -> [0, 0]; S7[] -> [0, 0]; S8[] -> [0, 0]; S9[k] -> [k, 2]; S10[k] -> [k, 3]; S5[] -> [0, 0]; S4[] -> [0, 0] }";
		String valueBasedFeautrierSchedule = "[M] -> { S7[] -> [0]; S6[] -> [0]; S10[k] -> [k]; S8[] -> [0]; S0[] -> [0]; S11[k] -> [k]; S4[] -> [0]; S3[] -> [0]; S5[] -> [0]; S2[] -> [0]; S9[k] -> [k]; S1[] -> [0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[M] -> { S11[k] -> S8[] : k >= 1 and k <= M; S10[k] -> S8[] : k >= 1 and k <= M; S9[k] -> S8[] : k >= 1 and k <= M }";
		String memoryBasedPlutoSchedule = "[M] -> { S3[] -> [0, 0]; S11[k] -> [k, 1]; S2[] -> [0, 0]; S0[] -> [0, 0]; S1[] -> [0, 0]; S6[] -> [0, 0]; S7[] -> [0, 0]; S8[] -> [0, 0]; S9[k] -> [k, 2]; S10[k] -> [k, 3]; S5[] -> [0, 0]; S4[] -> [0, 0] }";
		String memoryBasedFeautrierSchedule = "[M] -> { S7[] -> [0]; S6[] -> [0]; S10[k] -> [k]; S8[] -> [0]; S0[] -> [0]; S11[k] -> [k]; S4[] -> [0]; S3[] -> [0]; S5[] -> [0]; S2[] -> [0]; S9[k] -> [k]; S1[] -> [0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #151
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : inv_mdct_inv_mdct/scop_0
	 *   for : 0 <= p <= 11 (stride = 1)
	 *     S0 (depth = 1) [iout[6*i+p+6]=add(iout[6*i+p+6],itmp[p])]
	 *     ([iout[6*i+p+6]]) = f([iout[6*i+p+6], itmp[p]])
	 *     (Domain = [i] -> {S0[p] : (p >= 0) and (-p+11 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_imdct_GScopRoot_inv_mdct_scop_0_ () {
		String path = "basics_oldFiles_imdct_GScopRoot_inv_mdct_scop_0_";
		// SCoP Extraction Data
		String domains = "[i] -> { S0[p] : p >= 0 and p <= 11 }";
		String idSchedules = "[i] -> { S0[p] -> [0, p, 0] }";
		String writes = "[i] -> { S0[p] -> iout[6 + 6i + p] }";
		String reads = "[i] -> { S0[p] -> iout[6 + 6i + p]; S0[p] -> itmp[p] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[i] -> {  }";
		String valueBasedPlutoSchedule = "[i] -> { S0[p] -> [p] }";
		String valueBasedFeautrierSchedule = "[i] -> { S0[p] -> [p] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[i] -> {  }";
		String memoryBasedPlutoSchedule = "[i] -> { S0[p] -> [p] }";
		String memoryBasedFeautrierSchedule = "[i] -> { S0[p] -> [p] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #152
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : inv_mdct_inv_mdct/scop_1
	 *   for : 0 <= i <= 35 (stride = 1)
	 *     S0 (depth = 1) [iout[i]=IntExpr(0)]
	 *     ([iout[i]]) = f([])
	 *     (Domain = [] -> {S0[i] : (i >= 0) and (-i+35 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_imdct_GScopRoot_inv_mdct_scop_1_ () {
		String path = "basics_oldFiles_imdct_GScopRoot_inv_mdct_scop_1_";
		// SCoP Extraction Data
		String domains = "{ S0[i] : i >= 0 and i <= 35 }";
		String idSchedules = "{ S0[i] -> [0, i, 0] }";
		String writes = "{ S0[i] -> iout[i] }";
		String reads = "{  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{  }";
		String valueBasedPlutoSchedule = "{ S0[i] -> [i] }";
		String valueBasedFeautrierSchedule = "{ S0[i] -> [i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{  }";
		String memoryBasedPlutoSchedule = "{ S0[i] -> [i] }";
		String memoryBasedFeautrierSchedule = "{ S0[i] -> [i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #153
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : mowry_mowry/scop_0
	 *   block : 
	 *     for : 0 <= i <= 2 (stride = 1)
	 *       for : 0 <= j <= 99 (stride = 1)
	 *         S0 (depth = 2) [A[i][j]=add(B[j][0],B[j+1][0])]
	 *         ([A[i][j]]) = f([B[j][0], B[j+1][0]])
	 *         (Domain = [] -> {S0[i,j] : (j >= 0) and (-j+99 >= 0) and (i >= 0) and (-i+2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_mowry_GScopRoot_mowry_scop_0_ () {
		String path = "polymodel_others_mowry_GScopRoot_mowry_scop_0_";
		// SCoP Extraction Data
		String domains = "{ S0[i, j] : j >= 0 and j <= 99 and i >= 0 and i <= 2 }";
		String idSchedules = "{ S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "{ S0[i, j] -> A[i, j] }";
		String reads = "{ S0[i, j] -> B[1 + j, 0]; S0[i, j] -> B[j, 0] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "{  }";
		String valueBasedPlutoSchedule = "{ S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "{ S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "{  }";
		String memoryBasedPlutoSchedule = "{ S0[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "{ S0[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #154
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : bar_bar/scop_0
	 *   block : 
	 *     for : 0 <= k <= N-1 (stride = 1)
	 *       S0 (depth = 1) [x[k+2]=IntExpr(a)]
	 *       ([x[k+2]]) = f([])
	 *       (Domain = [N,a] -> {S0[k] : (k >= 0) and (-k+N-1 >= 0)})
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= i-1 (stride = 1)
	 *         block : 
	 *           S1 (depth = 2) [x[N-j]=IntExpr(a)]
	 *           ([x[N-j]]) = f([])
	 *           (Domain = [N,a] -> {S1[i,j] : (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 *           S2 (depth = 2) [x[i+j]=IntExpr(a)]
	 *           ([x[i+j]]) = f([])
	 *           (Domain = [N,a] -> {S2[i,j] : (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_scop1_GScopRoot_bar_scop_0_ () {
		String path = "polymodel_src_generic_for_loops_scop1_GScopRoot_bar_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, a] -> { S1[i, j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N; S0[k] : k >= 0 and k <= -1 + N; S2[i, j] : j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N }";
		String idSchedules = "[N, a] -> { S0[k] -> [0, k, 0, 0, 0]; S2[i, j] -> [1, i, 0, j, 1]; S1[i, j] -> [1, i, 0, j, 0] }";
		String writes = "[N, a] -> { S1[i, j] -> x[N - j]; S0[k] -> x[2 + k]; S2[i, j] -> x[i + j] }";
		String reads = "[N, a] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, a] -> {  }";
		String valueBasedPlutoSchedule = "[N, a] -> { S2[i, j] -> [i, j]; S1[i, j] -> [i, j]; S0[k] -> [k, 0] }";
		String valueBasedFeautrierSchedule = "[N, a] -> { S2[i, j] -> [i, j]; S1[i, j] -> [i, j]; S0[k] -> [k, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, a] -> { S2[i, j] -> S0[-2 + i + j] : 2j <= -1 + N - i and j <= -1 + i and j >= -2 + i and j >= 2 - i; S2[i, 1 + N - i] -> S0[-1 + N] : i <= -1 + N and 2i >= 2 + N and 2i <= 3 + N; S1[i, j] -> S2[-1 + i, 1 + N - i - j] : j >= 0 and 2j <= N - i and j >= 3 + N - 2i and i <= -1 + N; S1[i, j] -> S2[i, N - i - j] : 2j >= 1 + N - i and j <= -1 + i and j <= N - i; S1[i, -1 + i] -> S2[1 + N - i, 0] : i <= -1 + N and 2i >= 2 + N; S2[i, j] -> S2[-1 + i, j'] : 2j = -1 + N - i and 2j' = 1 + N - i and i <= -1 + N and 3i >= 5 + N; S2[i, j] -> S2[-1 + i, 1 + j] : (j >= 0 and j <= -3 + i and j <= 1 + N - 2i) or (i <= -1 + N and j >= 1 + N - i and j <= -3 + i); S1[i, -1 + i] -> S0[-1 + N - i] : i >= 1 and 3i <= 2 + N and i <= -1 + N; S1[i, j] -> S1[-1 + i, j] : (i <= -1 + N and j >= 1 + N - i and j <= -2 + i) or (i <= -1 + N and j >= 0 and j <= -2 + i and j <= 2 + N - 2i); S2[i, j] -> S1[i, j'] : 2j = N - i and 2j' = N - i and i <= -1 + N and 3i >= 2 + N; S2[i, j] -> S1[i, N - i - j] : j <= N - i and j <= -1 + i and 2j >= 1 + N - i; S2[i, j] -> S1[-1 + i, N - i - j] : j >= 0 and j >= 2 + N - 2i and 2j <= -2 + N - i }";
		String memoryBasedPlutoSchedule = "[N, a] -> { S1[i, j] -> [i, j, 0]; S2[i, j] -> [i, N - i - j, 0]; S0[k] -> [0, -N + k, 1] }";
		String memoryBasedFeautrierSchedule = "[N, a] -> { S1[i, j] -> [i, j, 0]; S2[i, j] -> [i, j, 1]; S0[k] -> [0, k, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #155
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : lu_lu/scop_0
	 *   for : 0 <= k <= N-1 (stride = 1)
	 *     block : 
	 *       for : k+1 <= j <= N-1 (stride = 1)
	 *         S0 (depth = 2) [a[k][j]=div(a[k][j],a[k][k])]
	 *         ([a[k][j]]) = f([a[k][j], a[k][k]])
	 *         (Domain = [N] -> {S0[k,j] : (-k+j-1 >= 0) and (-j+N-1 >= 0) and (k >= 0) and (-k+N-1 >= 0)})
	 *       for : k+1 <= i <= N-1 (stride = 1)
	 *         for : k+1 <= j <= N-1 (stride = 1)
	 *           S1 (depth = 3) [a[i][j]=sub(a[i][j],mul(a[i][k],a[k][j]))]
	 *           ([a[i][j]]) = f([a[i][j], a[i][k], a[k][j]])
	 *           (Domain = [N] -> {S1[k,i,j] : (-k+j-1 >= 0) and (-j+N-1 >= 0) and (-k+i-1 >= 0) and (-i+N-1 >= 0) and (k >= 0) and (-k+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_scop2_GScopRoot_lu_scop_0_ () {
		String path = "polymodel_src_generic_for_loops_scop2_GScopRoot_lu_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S1[k, i, j] : j >= 1 + k and j <= -1 + N and i >= 1 + k and i <= -1 + N and k >= 0 and k <= -1 + N; S0[k, j] : j >= 1 + k and j <= -1 + N and k >= 0 and k <= -1 + N }";
		String idSchedules = "[N] -> { S0[k, j] -> [0, k, 0, j, 0, 0, 0]; S1[k, i, j] -> [0, k, 1, i, 0, j, 0] }";
		String writes = "[N] -> { S0[k, j] -> a[k, j]; S1[k, i, j] -> a[i, j] }";
		String reads = "[N] -> { S0[k, j] -> a[k, k]; S0[k, j] -> a[k, j]; S1[k, i, j] -> a[i, k]; S1[k, i, j] -> a[k, j]; S1[k, i, j] -> a[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S1[k, i, j] -> S0[k, j] : j >= 1 + k and j <= -1 + N and i >= 1 + k and i <= -1 + N and k >= 0 and k <= -1 + N; S1[k, i, j] -> S1[-1 + k, i, k] : j >= 1 + k and j <= -1 + N and i >= 1 + k and i <= -1 + N and k >= 1; S1[k, i, j] -> S1[-1 + k, i, j] : j >= 1 + k and j <= -1 + N and i >= 1 + k and i <= -1 + N and k >= 1; S0[k, j] -> S1[-1 + k, k, k] : j >= 1 + k and j <= -1 + N and k >= 1; S0[k, j] -> S1[-1 + k, k, j] : j >= 1 + k and j <= -1 + N and k >= 1 }";
		String valueBasedPlutoSchedule = "[N] -> { S0[k, j] -> [k, k, j, 0]; S1[k, i, j] -> [k, i, j, 1] }";
		String valueBasedFeautrierSchedule = "[N] -> { S0[k, j] -> [2k, j, 0]; S1[k, i, j] -> [k + i, i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S1[k, i, j] -> S0[k, j] : j >= 1 + k and j <= -1 + N and i >= 1 + k and i <= -1 + N and k >= 0 and k <= -1 + N; S1[k, i, j] -> S1[-1 + k, i, k] : k >= 1 and i >= 1 + k and i <= -1 + N and j >= 1 + k and j <= -1 + N; S1[k, i, j] -> S1[-1 + k, i, j] : k >= 1 and i >= 1 + k and i <= -1 + N and j >= 1 + k and j <= -1 + N; S0[k, j] -> S1[-1 + k, k, k] : k >= 1 and j >= 1 + k and j <= -1 + N; S0[k, j] -> S1[-1 + k, k, j] : k >= 1 and j >= 1 + k and j <= -1 + N }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[k, j] -> [k, k, j, 0]; S1[k, i, j] -> [k, i, j, 1] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S0[k, j] -> [2k, j, 0]; S1[k, i, j] -> [k + i, i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #156
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : bar_bar/scop_0
	 *   block : 
	 *     S0 (depth = 0) [c=IntExpr(0)]
	 *     ([c]) = f([])
	 *     (Domain = [N,a] -> {S0[] : })
	 *     for : 0 <= i <= N-1 (stride = 1)
	 *       for : 0 <= j <= i-1 (stride = 1)
	 *         if : (j+2*i+8 >= 0) and (N > 0)
	 *           S1 (depth = 2) [x[i]=IntExpr(a)]
	 *           ([x[i]]) = f([])
	 *           (Domain = [N,a] -> {S1[i,j] : (2*i+j+8 >= 0) and (N-1 >= 0) and (j >= 0) and (i-j-1 >= 0) and (i >= 0) and (-i+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_scop3_GScopRoot_bar_scop_0_ () {
		String path = "polymodel_src_generic_for_loops_scop3_GScopRoot_bar_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, a] -> { S1[i, j] : j >= -8 - 2i and N >= 1 and j >= 0 and j <= -1 + i and i >= 0 and i <= -1 + N; S0[] }";
		String idSchedules = "[N, a] -> { S1[i, j] -> [1, i, 0, j, 0]; S0[] -> [0, 0, 0, 0, 0] }";
		String writes = "[N, a] -> { S1[i, j] -> x[i]; S0[] -> c[] }";
		String reads = "[N, a] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, a] -> {  }";
		String valueBasedPlutoSchedule = "[N, a] -> { S0[] -> [0, 0]; S1[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[N, a] -> { S0[] -> [0, 0]; S1[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, a] -> { S1[i, j] -> S1[i, -1 + j] : j <= -1 + i and j >= 1 and i <= -1 + N and j >= -8 - 2i and N >= 1 and i >= 0 }";
		String memoryBasedPlutoSchedule = "[N, a] -> { S0[] -> [0, 0]; S1[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "[N, a] -> { S0[] -> [0, 0]; S1[i, j] -> [j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #157
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : bar_bar/scop_0
	 *   block : 
	 *     S0 (depth = 0) [i=IntExpr(0)]
	 *     ([i]) = f([])
	 *     (Domain = [N,a] -> {S0[] : })
	 *     for : 0 <= j <= N-1 (stride = 1)
	 *       for : 0 <= k <= j-1 (stride = 1)
	 *         if : (j+2*k+8 >= 0) and (N > 0)
	 *           S1 (depth = 2) [x[k]=IntExpr(a)]
	 *           ([x[k]]) = f([])
	 *           (Domain = [N,a] -> {S1[j,k] : (j+2*k+8 >= 0) and (N-1 >= 0) and (k >= 0) and (j-k-1 >= 0) and (j >= 0) and (-j+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_scop4_GScopRoot_bar_scop_0_ () {
		String path = "polymodel_src_generic_for_loops_scop4_GScopRoot_bar_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, a] -> { S1[j, k] : 2k >= -8 - j and N >= 1 and k >= 0 and k <= -1 + j and j >= 0 and j <= -1 + N; S0[] }";
		String idSchedules = "[N, a] -> { S1[j, k] -> [1, j, 0, k, 0]; S0[] -> [0, 0, 0, 0, 0] }";
		String writes = "[N, a] -> { S1[j, k] -> x[k]; S0[] -> i[] }";
		String reads = "[N, a] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, a] -> {  }";
		String valueBasedPlutoSchedule = "[N, a] -> { S0[] -> [0, 0]; S1[j, k] -> [j, k] }";
		String valueBasedFeautrierSchedule = "[N, a] -> { S0[] -> [0, 0]; S1[j, k] -> [j, k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, a] -> { S1[j, k] -> S1[-1 + j, k] : j <= -1 + N and k <= -2 + j and k >= 0 and 2k >= -8 - j and N >= 1 and j >= 0 }";
		String memoryBasedPlutoSchedule = "[N, a] -> { S0[] -> [0, 0]; S1[j, k] -> [j, k] }";
		String memoryBasedFeautrierSchedule = "[N, a] -> { S0[] -> [0, 0]; S1[j, k] -> [j, k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #158
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : bar_bar/scop_0
	 *   for : 0 <= j <= N-1 (stride = 1)
	 *     S0 (depth = 1) [x[j+2]=IntExpr(a)]
	 *     ([x[j+2]]) = f([])
	 *     (Domain = [N,a] -> {S0[j] : (j >= 0) and (-j+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_scop5_GScopRoot_bar_scop_0_ () {
		String path = "polymodel_src_generic_for_loops_scop5_GScopRoot_bar_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, a] -> { S0[j] : j >= 0 and j <= -1 + N }";
		String idSchedules = "[N, a] -> { S0[j] -> [0, j, 0] }";
		String writes = "[N, a] -> { S0[j] -> x[2 + j] }";
		String reads = "[N, a] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, a] -> {  }";
		String valueBasedPlutoSchedule = "[N, a] -> { S0[j] -> [j] }";
		String valueBasedFeautrierSchedule = "[N, a] -> { S0[j] -> [j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, a] -> {  }";
		String memoryBasedPlutoSchedule = "[N, a] -> { S0[j] -> [j] }";
		String memoryBasedFeautrierSchedule = "[N, a] -> { S0[j] -> [j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #159
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : bar_bar/scop_0
	 *   for : 0 <= k <= N-1 (stride = 1)
	 *     S0 (depth = 1) [x[k+2]=IntExpr(a)]
	 *     ([x[k+2]]) = f([])
	 *     (Domain = [N,a] -> {S0[k] : (k >= 0) and (-k+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_scop6_GScopRoot_bar_scop_0_ () {
		String path = "polymodel_src_generic_for_loops_scop6_GScopRoot_bar_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, a] -> { S0[k] : k >= 0 and k <= -1 + N }";
		String idSchedules = "[N, a] -> { S0[k] -> [0, k, 0] }";
		String writes = "[N, a] -> { S0[k] -> x[2 + k] }";
		String reads = "[N, a] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, a] -> {  }";
		String valueBasedPlutoSchedule = "[N, a] -> { S0[k] -> [k] }";
		String valueBasedFeautrierSchedule = "[N, a] -> { S0[k] -> [k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, a] -> {  }";
		String memoryBasedPlutoSchedule = "[N, a] -> { S0[k] -> [k] }";
		String memoryBasedFeautrierSchedule = "[N, a] -> { S0[k] -> [k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #160
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : bar_bar/scop_0
	 *   for : 0 <= k <= N-1 (stride = 1)
	 *     S0 (depth = 1) [x[k]=IntExpr(a)]
	 *     ([x[k]]) = f([])
	 *     (Domain = [N,a] -> {S0[k] : (k >= 0) and (-k+N-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_scop7_GScopRoot_bar_scop_0_ () {
		String path = "polymodel_src_generic_for_loops_scop7_GScopRoot_bar_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, a] -> { S0[k] : k >= 0 and k <= -1 + N }";
		String idSchedules = "[N, a] -> { S0[k] -> [0, k, 0] }";
		String writes = "[N, a] -> { S0[k] -> x[k] }";
		String reads = "[N, a] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, a] -> {  }";
		String valueBasedPlutoSchedule = "[N, a] -> { S0[k] -> [k] }";
		String valueBasedFeautrierSchedule = "[N, a] -> { S0[k] -> [k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, a] -> {  }";
		String memoryBasedPlutoSchedule = "[N, a] -> { S0[k] -> [k] }";
		String memoryBasedFeautrierSchedule = "[N, a] -> { S0[k] -> [k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #161
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : bar_bar/scop_0
	 *   for : 0 <= j <= i-1 (stride = 1)
	 *     for : 0 <= k <= N-1 (stride = 1)
	 *       S0 (depth = 2) [x[k]=IntExpr(a)]
	 *       ([x[k]]) = f([])
	 *       (Domain = [i,N,a] -> {S0[j,k] : (k >= 0) and (-k+N-1 >= 0) and (j >= 0) and (-j+i-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_scop8_GScopRoot_bar_scop_0_ () {
		String path = "polymodel_src_generic_for_loops_scop8_GScopRoot_bar_scop_0_";
		// SCoP Extraction Data
		String domains = "[i, N, a] -> { S0[j, k] : k >= 0 and k <= -1 + N and j >= 0 and j <= -1 + i }";
		String idSchedules = "[i, N, a] -> { S0[j, k] -> [0, j, 0, k, 0] }";
		String writes = "[i, N, a] -> { S0[j, k] -> x[k] }";
		String reads = "[i, N, a] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[i, N, a] -> {  }";
		String valueBasedPlutoSchedule = "[i, N, a] -> { S0[j, k] -> [j, k] }";
		String valueBasedFeautrierSchedule = "[i, N, a] -> { S0[j, k] -> [j, k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[i, N, a] -> { S0[j, k] -> S0[-1 + j, k] : k >= 0 and k <= -1 + N and j >= 1 and j <= -1 + i }";
		String memoryBasedPlutoSchedule = "[i, N, a] -> { S0[j, k] -> [j, k] }";
		String memoryBasedFeautrierSchedule = "[i, N, a] -> { S0[j, k] -> [j, k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #162
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : bar_bar/scop_0
	 *   for : 0 <= j <= i-1 (stride = 1)
	 *     for : 0 <= k <= N-1 (stride = 1)
	 *       S0 (depth = 2) [x[k]=IntExpr(a)]
	 *       ([x[k]]) = f([])
	 *       (Domain = [i,N,a] -> {S0[j,k] : (k >= 0) and (-k+N-1 >= 0) and (j >= 0) and (-j+i-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_generic_for_loops_scop9_GScopRoot_bar_scop_0_ () {
		String path = "polymodel_src_generic_for_loops_scop9_GScopRoot_bar_scop_0_";
		// SCoP Extraction Data
		String domains = "[i, N, a] -> { S0[j, k] : k >= 0 and k <= -1 + N and j >= 0 and j <= -1 + i }";
		String idSchedules = "[i, N, a] -> { S0[j, k] -> [0, j, 0, k, 0] }";
		String writes = "[i, N, a] -> { S0[j, k] -> x[k] }";
		String reads = "[i, N, a] -> {  }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[i, N, a] -> {  }";
		String valueBasedPlutoSchedule = "[i, N, a] -> { S0[j, k] -> [j, k] }";
		String valueBasedFeautrierSchedule = "[i, N, a] -> { S0[j, k] -> [j, k] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[i, N, a] -> { S0[j, k] -> S0[-1 + j, k] : k >= 0 and k <= -1 + N and j >= 1 and j <= -1 + i }";
		String memoryBasedPlutoSchedule = "[i, N, a] -> { S0[j, k] -> [j, k] }";
		String memoryBasedFeautrierSchedule = "[i, N, a] -> { S0[j, k] -> [j, k] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #163
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : sobel7x7_sobel7x7/scop_0
	 *   block : 
	 *     for : [(7) / 2] <= i <= ([(7) / 2]) * -1 + (W-1) * 1 (stride = 1)
	 *       block : 
	 *         for : [(7) / 2] <= j <= ([(7) / 2]) * -1 + (W-1) * 1 (stride = 1)
	 *           block : 
	 *             for : 1 <= ii <= (-1) * 1 + [(7) / 2] (stride = 1)
	 *               for : 1 <= jj <= (-1) * 1 + [(7) / 2] (stride = 1)
	 *                 block : 
	 *                   S0 (depth = 4) [tmp=mul(sub(sub(add(im_in[i-ii][j-jj],im_in[i-ii][j+jj]),im_in[i+ii][j-jj]),im_in[i+ii][j+jj]),sobel[ii][jj])]
	 *                   ([tmp]) = f([im_in[i-ii][j-jj], im_in[i-ii][j+jj], im_in[i+ii][j-jj], im_in[i+ii][j+jj], sobel[ii][jj]])
	 *                   (Domain = [W] -> {S0[i,j,ii,jj] : (jj-1 >= 0) and (ii-1 >= 0) and (-jj+2 >= 0) and (-ii+2 >= 0) and (j-4 >= 0) and (-j+W-5 >= 0) and (i-4 >= 0) and (-i+W-5 >= 0)})
	 *                   S1 (depth = 4) [sobel[0][0]=IntExpr(1)]
	 *                   ([sobel[0][0]]) = f([])
	 *                   (Domain = [W] -> {S1[i,j,ii,jj] : (jj-1 >= 0) and (ii-1 >= 0) and (-jj+2 >= 0) and (-ii+2 >= 0) and (j-4 >= 0) and (-j+W-5 >= 0) and (i-4 >= 0) and (-i+W-5 >= 0)})
	 *             S2 (depth = 2) [im_out[i][j]=add(tmp,mul(im_in[i][j],sobel[0][0]))]
	 *             ([im_out[i][j]]) = f([tmp, im_in[i][j], sobel[0][0]])
	 *             (Domain = [W] -> {S2[i,j] : (j-4 >= 0) and (-j+W-5 >= 0) and (i-4 >= 0) and (-i+W-5 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_basics_oldFiles_sobel_GScopRoot_sobel7x7_scop_0_ () {
		String path = "basics_oldFiles_sobel_GScopRoot_sobel7x7_scop_0_";
		// SCoP Extraction Data
		String domains = "[W] -> { S0[i, j, ii, jj] : jj >= 1 and ii >= 1 and jj <= 2 and ii <= 2 and j >= 4 and j <= -5 + W and i >= 4 and i <= -5 + W; S1[i, j, ii, jj] : jj >= 1 and ii >= 1 and jj <= 2 and ii <= 2 and j >= 4 and j <= -5 + W and i >= 4 and i <= -5 + W; S2[i, j] : j >= 4 and j <= -5 + W and i >= 4 and i <= -5 + W }";
		String idSchedules = "[W] -> { S0[i, j, ii, jj] -> [0, i, 0, j, 0, ii, 0, jj, 0]; S1[i, j, ii, jj] -> [0, i, 0, j, 0, ii, 0, jj, 1]; S2[i, j] -> [0, i, 0, j, 1, 0, 0, 0, 0] }";
		String writes = "[W] -> { S2[i, j] -> im_out[i, j]; S0[i, j, ii, jj] -> tmp[]; S1[i, j, ii, jj] -> sobel[0, 0] }";
		String reads = "[W] -> { S0[i, j, ii, jj] -> sobel[ii, jj]; S2[i, j] -> im_in[i, j]; S0[i, j, ii, jj] -> im_in[i + ii, j + jj]; S0[i, j, ii, jj] -> im_in[i - ii, j + jj]; S0[i, j, ii, jj] -> im_in[i + ii, j - jj]; S0[i, j, ii, jj] -> im_in[i - ii, j - jj]; S2[i, j] -> tmp[]; S2[i, j] -> sobel[0, 0] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[W] -> { S2[i, j] -> S1[i, j, 2, 2] : j >= 4 and j <= -5 + W and i >= 4 and i <= -5 + W; S2[i, j] -> S0[i, j, 2, 2] : j >= 4 and j <= -5 + W and i >= 4 and i <= -5 + W }";
		String valueBasedPlutoSchedule = "[W] -> { S0[i, j, ii, jj] -> [i, j, ii, jj, 1]; S2[i, j] -> [i, j, 2, 2, 2]; S1[i, j, ii, jj] -> [i, j, ii, jj, 0] }";
		String valueBasedFeautrierSchedule = "[W] -> { S1[i, j, ii, jj] -> [i, j, ii, jj]; S0[i, j, ii, jj] -> [i, j, ii, jj]; S2[i, j] -> [i, j, 0, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[W] -> { S2[i, j] -> S0[i, j, 2, 2] : j >= 4 and j <= -5 + W and i >= 4 and i <= -5 + W; S2[i, j] -> S1[i, j, 2, 2] : j >= 4 and j <= -5 + W and i >= 4 and i <= -5 + W; S1[i, j, 1, 1] -> S2[i, -1 + j] : i >= 4 and i <= -5 + W and j >= 5 and j <= -5 + W; S1[i, 4, 1, 1] -> S2[-1 + i, -5 + W] : i >= 5 and i <= -5 + W; S1[i, j, ii, 2] -> S1[i, j, ii, 1] : i >= 4 and i <= -5 + W and j >= 4 and j <= -5 + W and ii <= 2 and ii >= 1; S1[i, j, 1, 1] -> S1[i, -1 + j, 2, 2] : i >= 4 and i <= -5 + W and j >= 5 and j <= -5 + W; S1[i, j, 2, 1] -> S1[i, j, 1, 2] : i >= 4 and i <= -5 + W and j >= 4 and j <= -5 + W; S1[i, 4, 1, 1] -> S1[-1 + i, -5 + W, 2, 2] : i >= 5 and i <= -5 + W; S0[i, j, 1, 1] -> S2[i, -1 + j] : i >= 4 and i <= -5 + W and j >= 5 and j <= -5 + W; S0[i, 4, 1, 1] -> S2[-1 + i, -5 + W] : i >= 5 and i <= -5 + W; S0[i, j, ii, 2] -> S0[i, j, ii, 1] : i >= 4 and i <= -5 + W and j >= 4 and j <= -5 + W and ii <= 2 and ii >= 1; S0[i, j, 1, 1] -> S0[i, -1 + j, 2, 2] : i >= 4 and i <= -5 + W and j >= 5 and j <= -5 + W; S0[i, j, 2, 1] -> S0[i, j, 1, 2] : i >= 4 and i <= -5 + W and j >= 4 and j <= -5 + W; S0[i, 4, 1, 1] -> S0[-1 + i, -5 + W, 2, 2] : i >= 5 and i <= -5 + W }";
		String memoryBasedPlutoSchedule = "[W] -> { S0[i, j, ii, jj] -> [i, j, j + ii, 2j + ii + jj, 1]; S2[i, j] -> [i, j, 2 + j, 4 + 2j, 2]; S1[i, j, ii, jj] -> [i, j, j + ii, 2j + ii + jj, 0] }";
		String memoryBasedFeautrierSchedule = "[W] -> { S1[i, j, ii, jj] -> [i, 5j + 2ii + jj, ii, j]; S0[i, j, ii, jj] -> [i, 5j + 2ii + jj, ii, j]; S2[i, j] -> [i, 7 + 5j, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #164
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_syr2k_scop_new
	 *   block : 
	 *     for : 0 <= i <= ni-1 (stride = 1)
	 *       for : 0 <= j <= ni-1 (stride = 1)
	 *         S0 (depth = 2) [C[i][j]=mul(C[i][j],IntExpr(beta))]
	 *         ([C[i][j]]) = f([C[i][j]])
	 *         (Domain = [ni,beta,nj,alpha] -> {S0[i,j] : (j >= 0) and (-j+ni-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *     for : 0 <= i <= ni-1 (stride = 1)
	 *       for : 0 <= j <= ni-1 (stride = 1)
	 *         for : 0 <= k <= nj-1 (stride = 1)
	 *           block : 
	 *             S1 (depth = 3) [C[i][j]=add(C[i][j],mul(mul(IntExpr(alpha),A[i][k]),B[j][k]))]
	 *             ([C[i][j]]) = f([C[i][j], A[i][k], B[j][k]])
	 *             (Domain = [ni,beta,nj,alpha] -> {S1[i,j,k] : (k >= 0) and (-k+nj-1 >= 0) and (j >= 0) and (-j+ni-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 *             S2 (depth = 3) [C[i][j]=add(C[i][j],mul(mul(IntExpr(alpha),B[i][k]),A[j][k]))]
	 *             ([C[i][j]]) = f([C[i][j], B[i][k], A[j][k]])
	 *             (Domain = [ni,beta,nj,alpha] -> {S2[i,j,k] : (k >= 0) and (-k+nj-1 >= 0) and (j >= 0) and (-j+ni-1 >= 0) and (i >= 0) and (-i+ni-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_linear_algebra_kernels_syr2k_syr2k_GScopRoot_scop_new_ () {
		String path = "polybench_gecos_linear_algebra_kernels_syr2k_syr2k_GScopRoot_scop_new_";
		// SCoP Extraction Data
		String domains = "[ni, beta, nj, alpha] -> { S2[i, j, k] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + ni and i >= 0 and i <= -1 + ni; S1[i, j, k] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + ni and i >= 0 and i <= -1 + ni; S0[i, j] : j >= 0 and j <= -1 + ni and i >= 0 and i <= -1 + ni }";
		String idSchedules = "[ni, beta, nj, alpha] -> { S1[i, j, k] -> [1, i, 0, j, 0, k, 0]; S2[i, j, k] -> [1, i, 0, j, 0, k, 1]; S0[i, j] -> [0, i, 0, j, 0, 0, 0] }";
		String writes = "[ni, beta, nj, alpha] -> { S0[i, j] -> C[i, j]; S1[i, j, k] -> C[i, j]; S2[i, j, k] -> C[i, j] }";
		String reads = "[ni, beta, nj, alpha] -> { S1[i, j, k] -> A[i, k]; S2[i, j, k] -> B[i, k]; S1[i, j, k] -> B[j, k]; S2[i, j, k] -> A[j, k]; S0[i, j] -> C[i, j]; S1[i, j, k] -> C[i, j]; S2[i, j, k] -> C[i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[ni, beta, nj, alpha] -> { S1[i, j, 0] -> S0[i, j] : i <= -1 + ni and nj >= 1 and j >= 0 and j <= -1 + ni and i >= 0; S1[i, j, k] -> S2[i, j, -1 + k] : k >= 1 and k <= -1 + nj and j >= 0 and j <= -1 + ni and i >= 0 and i <= -1 + ni; S2[i, j, k] -> S1[i, j, k] : k >= 0 and k <= -1 + nj and j >= 0 and j <= -1 + ni and i >= 0 and i <= -1 + ni }";
		String valueBasedPlutoSchedule = "[ni, beta, nj, alpha] -> { S2[i, j, k] -> [i, i + j, i + j + k, 2]; S0[i, j] -> [0, i, j, 0]; S1[i, j, k] -> [i, i + j, i + j + k, 1] }";
		String valueBasedFeautrierSchedule = "[ni, beta, nj, alpha] -> { S2[i, j, k] -> [1 + k, 0, j, i]; S0[i, j] -> [0, 0, i, j]; S1[i, j, k] -> [k, 1, j, i] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[ni, beta, nj, alpha] -> { S1[i, j, 0] -> S0[i, j] : nj >= 1 and i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + ni; S1[i, j, k] -> S2[i, j, -1 + k] : i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + ni and k >= 1 and k <= -1 + nj; S2[i, j, k] -> S1[i, j, k] : i >= 0 and i <= -1 + ni and j >= 0 and j <= -1 + ni and k >= 0 and k <= -1 + nj }";
		String memoryBasedPlutoSchedule = "[ni, beta, nj, alpha] -> { S2[i, j, k] -> [i, i + j, i + j + k, 2]; S0[i, j] -> [0, i, j, 0]; S1[i, j, k] -> [i, i + j, i + j + k, 1] }";
		String memoryBasedFeautrierSchedule = "[ni, beta, nj, alpha] -> { S2[i, j, k] -> [1 + k, 0, j, i]; S0[i, j] -> [0, 0, i, j]; S1[i, j, k] -> [k, 1, j, i] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #165
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : private_gaussian_filter_1_private_gaussian_filter_1/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-2 (stride = 1)
	 *       for : 0 <= j <= M-1 (stride = 1)
	 *         S0 (depth = 2) [tmp[i][j]=sub(sub(mul(IntExpr(3),in[i][j]),in[i-1][j]),in[i+1][j])]
	 *         ([tmp[i][j]]) = f([in[i][j], in[i-1][j], in[i+1][j]])
	 *         (Domain = [N,M] -> {S0[i,j] : (j >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *     for : 1 <= i <= N-2 (stride = 1)
	 *       for : 1 <= j <= M-2 (stride = 1)
	 *         S1 (depth = 2) [out[i][j]=sub(sub(mul(IntExpr(3),tmp[i][j]),tmp[i][j-1]),tmp[i][j+1])]
	 *         ([out[i][j]]) = f([tmp[i][j], tmp[i][j-1], tmp[i][j+1]])
	 *         (Domain = [N,M] -> {S1[i,j] : (j-1 >= 0) and (-j+M-2 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_forunroller_test1_GScopRoot_private_gaussian_filter_1_scop_0_ () {
		String path = "polymodel_others_forunroller_test1_GScopRoot_private_gaussian_filter_1_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S1[i, j] : j >= 1 and j <= -2 + M and i >= 1 and i <= -2 + N; S0[i, j] : j >= 0 and j <= -1 + M and i >= 1 and i <= -2 + N }";
		String idSchedules = "[N, M] -> { S1[i, j] -> [1, i, 0, j, 0]; S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[N, M] -> { S1[i, j] -> out[i, j]; S0[i, j] -> tmp[i, j] }";
		String reads = "[N, M] -> { S0[i, j] -> in[1 + i, j]; S0[i, j] -> in[i, j]; S0[i, j] -> in[-1 + i, j]; S1[i, j] -> tmp[i, 1 + j]; S1[i, j] -> tmp[i, j]; S1[i, j] -> tmp[i, -1 + j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> { S1[i, j] -> S0[i, j'] : j' >= -1 + j and i >= 1 and i <= -2 + N and j >= 1 and j <= -2 + M and j' <= 1 + j }";
		String valueBasedPlutoSchedule = "[N, M] -> { S1[i, j] -> [i, 1 + j, 1]; S0[i, j] -> [i, j, 0] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S0[i, j] -> [i, j]; S1[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> { S1[i, j] -> S0[i, j'] : j' >= -1 + j and i >= 1 and i <= -2 + N and j >= 1 and j <= -2 + M and j' <= 1 + j }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S1[i, j] -> [i, 1 + j, 1]; S0[i, j] -> [i, j, 0] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S0[i, j] -> [i, j]; S1[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #166
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : private_gaussian_filter_3_private_gaussian_filter_3/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-2 (stride = 1)
	 *       block : 
	 *         for : 0 <= j <= 1 (stride = 1)
	 *           S0 (depth = 2) [tmp[i][j]=sub(sub(mul(IntExpr(3),in[i][j]),in[i-1][j]),in[i+1][j])]
	 *           ([tmp[i][j]]) = f([in[i][j], in[i-1][j], in[i+1][j]])
	 *           (Domain = [N,M] -> {S0[i,j] : (j >= 0) and (-j+1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *         for : 2 <= j <= M-1 (stride = 1)
	 *           block : 
	 *             S1 (depth = 2) [tmp[i][j]=sub(sub(mul(IntExpr(3),in[i][j]),in[i-1][j]),in[i+1][j])]
	 *             ([tmp[i][j]]) = f([in[i][j], in[i-1][j], in[i+1][j]])
	 *             (Domain = [N,M] -> {S1[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 *             S2 (depth = 2) [out[i][j-1]=sub(sub(mul(IntExpr(3),tmp[i][j-1]),tmp[i][j-2]),tmp[i][j])]
	 *             ([out[i][j-1]]) = f([tmp[i][j-1], tmp[i][j-2], tmp[i][j]])
	 *             (Domain = [N,M] -> {S2[i,j] : (j-2 >= 0) and (-j+M-1 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_forunroller_test1_GScopRoot_private_gaussian_filter_3_scop_0_ () {
		String path = "polymodel_others_forunroller_test1_GScopRoot_private_gaussian_filter_3_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S2[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S1[i, j] : j >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N; S0[i, j] : j >= 0 and j <= 1 and i >= 1 and i <= -2 + N }";
		String idSchedules = "[N, M] -> { S1[i, j] -> [0, i, 1, j, 0]; S2[i, j] -> [0, i, 1, j, 1]; S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[N, M] -> { S1[i, j] -> tmp[i, j]; S2[i, j] -> out[i, -1 + j]; S0[i, j] -> tmp[i, j] }";
		String reads = "[N, M] -> { S2[i, j] -> tmp[i, j]; S2[i, j] -> tmp[i, -1 + j]; S2[i, j] -> tmp[i, -2 + j]; S0[i, j] -> in[1 + i, j]; S0[i, j] -> in[i, j]; S0[i, j] -> in[-1 + i, j]; S1[i, j] -> in[1 + i, j]; S1[i, j] -> in[i, j]; S1[i, j] -> in[-1 + i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> { S2[i, j] -> S0[i, j'] : j' >= -2 + j and i >= 1 and i <= -2 + N and j' <= 1 and j >= 2 and j <= -1 + M; S2[i, j] -> S1[i, j'] : j' >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N and j' >= -2 + j and j' <= j }";
		String valueBasedPlutoSchedule = "[N, M] -> { S2[i, j] -> [i, j, 2]; S1[i, j] -> [i, j, 1]; S0[i, j] -> [i, j, 0] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S0[i, j] -> [i, j]; S2[i, j] -> [i, j]; S1[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> { S2[i, j] -> S0[i, j'] : j' >= -2 + j and i >= 1 and i <= -2 + N and j' <= 1 and j >= 2 and j <= -1 + M; S2[i, j] -> S1[i, j'] : j' >= 2 and j <= -1 + M and i >= 1 and i <= -2 + N and j' >= -2 + j and j' <= j }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S2[i, j] -> [i, j, 2]; S1[i, j] -> [i, j, 1]; S0[i, j] -> [i, j, 0] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S0[i, j] -> [i, j]; S2[i, j] -> [i, j]; S1[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #167
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : private_gaussian_filter_2_private_gaussian_filter_2/scop_0
	 *   block : 
	 *     for : 1 <= i <= N-2 (stride = 1)
	 *       for : 1 <= j <= M-2 (stride = 1)
	 *         S0 (depth = 2) [out[i][j]=add(sub(mul(IntExpr(9),in[i][j]),mul(IntExpr(3),add(add(add(in[i-1][j],in[i+1][j]),in[i][j-1]),in[i][j+1]))),add(add(add(in[i-1][j-1],in[i-1][j+1]),in[i+1][j-1]),in[i+1][j+1]))]
	 *         ([out[i][j]]) = f([in[i][j], in[i-1][j], in[i+1][j], in[i][j-1], in[i][j+1], in[i-1][j-1], in[i-1][j+1], in[i+1][j-1], in[i+1][j+1]])
	 *         (Domain = [N,M] -> {S0[i,j] : (j-1 >= 0) and (-j+M-2 >= 0) and (i-1 >= 0) and (-i+N-2 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_forunroller_test1_GScopRoot_private_gaussian_filter_2_scop_0_ () {
		String path = "polymodel_others_forunroller_test1_GScopRoot_private_gaussian_filter_2_scop_0_";
		// SCoP Extraction Data
		String domains = "[N, M] -> { S0[i, j] : j >= 1 and j <= -2 + M and i >= 1 and i <= -2 + N }";
		String idSchedules = "[N, M] -> { S0[i, j] -> [0, i, 0, j, 0] }";
		String writes = "[N, M] -> { S0[i, j] -> out[i, j] }";
		String reads = "[N, M] -> { S0[i, j] -> in[1 + i, 1 + j]; S0[i, j] -> in[i, 1 + j]; S0[i, j] -> in[-1 + i, 1 + j]; S0[i, j] -> in[1 + i, j]; S0[i, j] -> in[i, j]; S0[i, j] -> in[-1 + i, j]; S0[i, j] -> in[1 + i, -1 + j]; S0[i, j] -> in[i, -1 + j]; S0[i, j] -> in[-1 + i, -1 + j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N, M] -> {  }";
		String valueBasedPlutoSchedule = "[N, M] -> { S0[i, j] -> [i, j] }";
		String valueBasedFeautrierSchedule = "[N, M] -> { S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N, M] -> {  }";
		String memoryBasedPlutoSchedule = "[N, M] -> { S0[i, j] -> [i, j] }";
		String memoryBasedFeautrierSchedule = "[N, M] -> { S0[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #168
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : test_test/scop_0
	 *   block : 
	 *     if : (N > 2) and (N < 2048)
	 *       block : 
	 *         for : 0 <= i <= N-1 (stride = 1)
	 *           for : 0 <= j <= N-1 (stride = 1)
	 *             S0 (depth = 2) [t1[i][j]=IntExpr(0)]
	 *             ([t1[i][j]]) = f([])
	 *             (Domain = [N] -> {S0[i,j] : (j >= 0) and (-j+N-1 >= 0) and (i >= 0) and (-i+N-1 >= 0) and (N-3 >= 0) and (-N+2047 >= 0)})
	 *         for : 1 <= i <= N (stride = 1)
	 *           for : 0 <= j <= N-1 (stride = 1)
	 *             S1 (depth = 2) [t2[i-1][j]=t1[i-1][j]]
	 *             ([t2[i-1][j]]) = f([t1[i-1][j]])
	 *             (Domain = [N] -> {S1[i,j] : (j >= 0) and (-j+N-1 >= 0) and (i-1 >= 0) and (-i+N >= 0) and (N-3 >= 0) and (-N+2047 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_others_test2_GScopRoot_test_scop_0_ () {
		String path = "polymodel_others_test2_GScopRoot_test_scop_0_";
		// SCoP Extraction Data
		String domains = "[N] -> { S0[i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + N and N >= 3 and N <= 2047; S1[i, j] : j >= 0 and j <= -1 + N and i >= 1 and i <= N and N >= 3 and N <= 2047 }";
		String idSchedules = "[N] -> { S0[i, j] -> [0, i, 0, j, 0]; S1[i, j] -> [1, i, 0, j, 0] }";
		String writes = "[N] -> { S0[i, j] -> t1[i, j]; S1[i, j] -> t2[-1 + i, j] }";
		String reads = "[N] -> { S1[i, j] -> t1[-1 + i, j] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[N] -> { S1[i, j] -> S0[-1 + i, j] : j >= 0 and j <= -1 + N and i >= 1 and i <= N and N >= 3 and N <= 2047 }";
		String valueBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j, 1]; S1[i, j] -> [i, j, 0] }";
		String valueBasedFeautrierSchedule = "[N] -> { S1[i, j] -> [i, j]; S0[i, j] -> [i, j] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[N] -> { S1[i, j] -> S0[-1 + i, j] : j >= 0 and j <= -1 + N and i >= 1 and i <= N and N >= 3 and N <= 2047 }";
		String memoryBasedPlutoSchedule = "[N] -> { S0[i, j] -> [i, j, 1]; S1[i, j] -> [i, j, 0] }";
		String memoryBasedFeautrierSchedule = "[N] -> { S1[i, j] -> [i, j]; S0[i, j] -> [i, j] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #169
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : kernel_reg_detect_kernel_reg_detect/scop_0
	 *   for : 0 <= t <= niter-1 (stride = 1)
	 *     block : 
	 *       for : 0 <= j <= maxgrid-1 (stride = 1)
	 *         for : j <= i <= maxgrid-1 (stride = 1)
	 *           for : 0 <= cnt <= length-1 (stride = 1)
	 *             S0 (depth = 4) [diff[j][i][cnt]=sum_tang[j][i]]
	 *             ([diff[j][i][cnt]]) = f([sum_tang[j][i]])
	 *             (Domain = [niter,maxgrid,length] -> {S0[t,j,i,cnt] : (cnt >= 0) and (-cnt+length-1 >= 0) and (-j+i >= 0) and (-i+maxgrid-1 >= 0) and (j >= 0) and (-j+maxgrid-1 >= 0) and (t >= 0) and (-t+niter-1 >= 0)})
	 *       for : 0 <= j <= maxgrid-1 (stride = 1)
	 *         for : j <= i <= maxgrid-1 (stride = 1)
	 *           block : 
	 *             S1 (depth = 3) [sum_diff[j][i][0]=diff[j][i][0]]
	 *             ([sum_diff[j][i][0]]) = f([diff[j][i][0]])
	 *             (Domain = [niter,maxgrid,length] -> {S1[t,j,i] : (-j+i >= 0) and (-i+maxgrid-1 >= 0) and (j >= 0) and (-j+maxgrid-1 >= 0) and (t >= 0) and (-t+niter-1 >= 0)})
	 *             for : 1 <= cnt <= length-1 (stride = 1)
	 *               S2 (depth = 4) [sum_diff[j][i][cnt]=add(sum_diff[j][i][cnt-1],diff[j][i][cnt])]
	 *               ([sum_diff[j][i][cnt]]) = f([sum_diff[j][i][cnt-1], diff[j][i][cnt]])
	 *               (Domain = [niter,maxgrid,length] -> {S2[t,j,i,cnt] : (cnt-1 >= 0) and (-cnt+length-1 >= 0) and (-j+i >= 0) and (-i+maxgrid-1 >= 0) and (j >= 0) and (-j+maxgrid-1 >= 0) and (t >= 0) and (-t+niter-1 >= 0)})
	 *             S3 (depth = 3) [mean[j][i]=sum_diff[j][i][length-1]]
	 *             ([mean[j][i]]) = f([sum_diff[j][i][length-1]])
	 *             (Domain = [niter,maxgrid,length] -> {S3[t,j,i] : (-j+i >= 0) and (-i+maxgrid-1 >= 0) and (j >= 0) and (-j+maxgrid-1 >= 0) and (t >= 0) and (-t+niter-1 >= 0)})
	 *       for : 0 <= i <= maxgrid-1 (stride = 1)
	 *         S4 (depth = 2) [path[0][i]=mean[0][i]]
	 *         ([path[0][i]]) = f([mean[0][i]])
	 *         (Domain = [niter,maxgrid,length] -> {S4[t,i] : (i >= 0) and (-i+maxgrid-1 >= 0) and (t >= 0) and (-t+niter-1 >= 0)})
	 *       for : 1 <= j <= maxgrid-1 (stride = 1)
	 *         for : j <= i <= maxgrid-1 (stride = 1)
	 *           S5 (depth = 3) [path[j][i]=add(path[j-1][i-1],mean[j][i])]
	 *           ([path[j][i]]) = f([path[j-1][i-1], mean[j][i]])
	 *           (Domain = [niter,maxgrid,length] -> {S5[t,j,i] : (-j+i >= 0) and (-i+maxgrid-1 >= 0) and (j-1 >= 0) and (-j+maxgrid-1 >= 0) and (t >= 0) and (-t+niter-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polybench_gecos_medley_reg_detect_reg_detect_GScopRoot_kernel_reg_detect_scop_0_ () {
		String path = "polybench_gecos_medley_reg_detect_reg_detect_GScopRoot_kernel_reg_detect_scop_0_";
		// SCoP Extraction Data
		String domains = "[niter, maxgrid, length] -> { S0[t, j, i, cnt] : cnt >= 0 and cnt <= -1 + length and i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter; S4[t, i] : i >= 0 and i <= -1 + maxgrid and t >= 0 and t <= -1 + niter; S1[t, j, i] : i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter; S3[t, j, i] : i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter; S5[t, j, i] : i >= j and i <= -1 + maxgrid and j >= 1 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter; S2[t, j, i, cnt] : cnt >= 1 and cnt <= -1 + length and i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter }";
		String idSchedules = "[niter, maxgrid, length] -> { S2[t, j, i, cnt] -> [0, t, 1, j, 0, i, 1, cnt, 0]; S1[t, j, i] -> [0, t, 1, j, 0, i, 0, 0, 0]; S4[t, i] -> [0, t, 2, i, 0, 0, 0, 0, 0]; S3[t, j, i] -> [0, t, 1, j, 0, i, 2, 0, 0]; S0[t, j, i, cnt] -> [0, t, 0, j, 0, i, 0, cnt, 0]; S5[t, j, i] -> [0, t, 3, j, 0, i, 0, 0, 0] }";
		String writes = "[niter, maxgrid, length] -> { S3[t, j, i] -> mean[j, i]; S5[t, j, i] -> path[j, i]; S2[t, j, i, cnt] -> sum_diff[j, i, cnt]; S1[t, j, i] -> sum_diff[j, i, 0]; S0[t, j, i, cnt] -> diff[j, i, cnt]; S4[t, i] -> path[0, i] }";
		String reads = "[niter, maxgrid, length] -> { S2[t, j, i, cnt] -> diff[j, i, cnt]; S0[t, j, i, cnt] -> sum_tang[j, i]; S5[t, j, i] -> path[-1 + j, -1 + i]; S4[t, i] -> mean[0, i]; S3[t, j, i] -> sum_diff[j, i, -1 + length]; S2[t, j, i, cnt] -> sum_diff[j, i, -1 + cnt]; S1[t, j, i] -> diff[j, i, 0]; S5[t, j, i] -> mean[j, i] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[niter, maxgrid, length] -> { S3[t, j, i] -> S1[t, j, i] : length = 1 and i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter; S5[t, j, i] -> S3[t, j, i] : i >= j and i <= -1 + maxgrid and j >= 1 and t <= -1 + niter and t >= 0 and j <= -1 + maxgrid; S1[t, j, i] -> S0[t, j, i, 0] : i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter and length >= 1; S4[t, i] -> S3[t, 0, i] : i >= 0 and i <= -1 + maxgrid and t >= 0 and t <= -1 + niter; S2[t, j, i, cnt] -> S2[t, j, i, -1 + cnt] : cnt >= 2 and cnt <= -1 + length and i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter; S5[t, j, i] -> S5[t, -1 + j, -1 + i] : i >= j and i <= -1 + maxgrid and t <= -1 + niter and j >= 2 and t >= 0 and j <= -1 + maxgrid; S2[t, j, i, 1] -> S1[t, j, i] : t <= -1 + niter and length >= 2 and i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0; S2[t, j, i, cnt] -> S0[t, j, i, cnt] : cnt >= 1 and cnt <= -1 + length and i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter; S5[t, 1, i] -> S4[t, -1 + i] : i >= 1 and i <= -1 + maxgrid and t >= 0 and t <= -1 + niter and maxgrid >= 2; S3[t, j, i] -> S2[t, j, i, -1 + length] : i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter and length >= 2 }";
		String valueBasedPlutoSchedule = "[niter, maxgrid, length] -> { S2[t, j, i, cnt] -> [t, j, i, cnt, 1]; S4[t, i] -> [t, 0, i, length, 5]; S1[t, j, i] -> [t, j, i, 0, 4]; S3[t, j, i] -> [t, j, i, length, 3]; S5[t, j, i] -> [t, i, i, length + j, 2]; S0[t, j, i, cnt] -> [t, j, i, cnt, 0] }";
		String valueBasedFeautrierSchedule = "[niter, maxgrid, length] -> { S5[t, j, i] -> [i, j, t, 0]; S2[t, j, i, cnt] -> [cnt, j, i, t]; S1[t, j, i] -> [t, j, i, 0]; S3[t, j, i] -> [t, j, i, 0]; S0[t, j, i, cnt] -> [t, j, i, cnt]; S4[t, i] -> [t, i, 0, 0] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[niter, maxgrid, length] -> { S3[t, j, i] -> S1[t, j, i] : length = 1 and i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter; S1[t, j, i] -> S3[-1 + t, j, i] : length = 1 and t >= 1 and t <= -1 + niter and j >= 0 and i >= j and i <= -1 + maxgrid and j <= -1 + maxgrid; S5[t, j, i] -> S5[-1 + t, 1 + j, 1 + i] : t >= 1 and t <= -1 + niter and j >= 1 and i >= j and i <= -2 + maxgrid; S5[t, j, i] -> S5[-1 + t, j, i] : t >= 1 and t <= -1 + niter and j >= 1 and i >= j and i <= -1 + maxgrid; S5[t, j, i] -> S5[t, -1 + j, -1 + i] : t >= 0 and t <= -1 + niter and j >= 2 and i >= j and i <= -1 + maxgrid; S1[t, j, i] -> S0[t, j, i, 0] : i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter and length >= 1; S0[t, j, i, cnt] -> S2[-1 + t, j, i, cnt] : t >= 1 and t <= -1 + niter and j >= 0 and i >= j and i <= -1 + maxgrid and cnt >= 1 and cnt <= -1 + length and j <= -1 + maxgrid; S1[t, j, i] -> S1[-1 + t, j, i] : t >= 1 and t <= -1 + niter and j >= 0 and i >= j and i <= -1 + maxgrid; S4[t, i] -> S3[t, 0, i] : i >= 0 and i <= -1 + maxgrid and t >= 0 and t <= -1 + niter; S3[t, 0, i] -> S4[-1 + t, i] : t >= 1 and t <= -1 + niter and i >= 0 and i <= -1 + maxgrid and maxgrid >= 1; S0[t, j, i, 0] -> S1[-1 + t, j, i] : length >= 1 and t >= 1 and t <= -1 + niter and j >= 0 and i >= j and i <= -1 + maxgrid and j <= -1 + maxgrid; S2[t, j, i, cnt] -> S0[t, j, i, cnt] : cnt >= 1 and cnt <= -1 + length and i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter; S0[t, j, i, cnt] -> S0[-1 + t, j, i, cnt] : cnt >= 0 and cnt <= -1 + length and i >= j and i <= -1 + maxgrid and j >= 0 and t <= -1 + niter and t >= 1; S3[t, j, i] -> S5[-1 + t, j, i] : t >= 1 and t <= -1 + niter and j >= 1 and i >= j and i <= -1 + maxgrid and j <= -1 + maxgrid; S1[t, j, i] -> S2[-1 + t, j, i, 1] : length >= 2 and t >= 1 and t <= -1 + niter and j >= 0 and i >= j and i <= -1 + maxgrid and j <= -1 + maxgrid; S2[t, j, i, 1] -> S1[t, j, i] : t <= -1 + niter and length >= 2 and i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0; S4[t, i] -> S5[-1 + t, 1, 1 + i] : t >= 1 and t <= -1 + niter and i >= 0 and i <= -2 + maxgrid; S5[t, 1, i] -> S4[t, -1 + i] : i >= 1 and i <= -1 + maxgrid and t >= 0 and t <= -1 + niter and maxgrid >= 2; S4[t, i] -> S4[-1 + t, i] : t >= 1 and t <= -1 + niter and i >= 0 and i <= -1 + maxgrid; S2[t, j, i, -1 + length] -> S3[-1 + t, j, i] : length >= 2 and t >= 1 and t <= -1 + niter and j >= 0 and i >= j and i <= -1 + maxgrid and j <= -1 + maxgrid; S2[t, j, i, cnt] -> S2[-1 + t, j, i, cnt'] : t >= 1 and t <= -1 + niter and i <= -1 + maxgrid and cnt' <= 1 + cnt and cnt >= 1 and cnt' >= cnt and cnt' <= -1 + length and i >= j and j >= 0; S2[t, j, i, cnt] -> S2[t, j, i, -1 + cnt] : t >= 0 and t <= -1 + niter and j >= 0 and i >= j and i <= -1 + maxgrid and cnt >= 2 and cnt <= -1 + length; S5[t, j, i] -> S3[t, j, i] : i >= j and i <= -1 + maxgrid and j >= 1 and t <= -1 + niter and t >= 0 and j <= -1 + maxgrid; S3[t, j, i] -> S2[t, j, i, -1 + length] : i >= j and i <= -1 + maxgrid and j >= 0 and j <= -1 + maxgrid and t >= 0 and t <= -1 + niter and length >= 2; S3[t, j, i] -> S3[-1 + t, j, i] : i >= j and i <= -1 + maxgrid and j >= 0 and t <= -1 + niter and t >= 1 }";
		String memoryBasedPlutoSchedule = "[niter, maxgrid, length] -> { S2[t, j, i, cnt] -> [t, j - i, t + i, t + cnt, 1]; S4[t, i] -> [t, -i, t + i, length + t, 5]; S1[t, j, i] -> [t, j - i, t + i, t, 4]; S3[t, j, i] -> [t, j - i, t + i, length + t, 2]; S5[t, j, i] -> [t, j - i, t + i, length + t, 3]; S0[t, j, i, cnt] -> [t, j - i, t + i, t + cnt, 0] }";
		String memoryBasedFeautrierSchedule = "[niter, maxgrid, length] -> { S5[t, j, i] -> [2 + length + 2t + j, t, i, 0]; S2[t, j, i, cnt] -> [1 + 2t + j + cnt, t, i, cnt]; S1[t, j, i] -> [1 + 2t + j, t, i, 0]; S3[t, j, i] -> [1 + length + 2t + j, t, i, 0]; S0[t, j, i, cnt] -> [2t + j + cnt, t, i, cnt]; S4[t, i] -> [2 + length + 2t, i, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #170
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : inner_product_inner_product/scop_0
	 *   block : 
	 *     for : 1 <= l <= loop (stride = 1)
	 *       block : 
	 *         S0 (depth = 1) [q=0.0]
	 *         ([q]) = f([])
	 *         (Domain = [loop,n] -> {S0[l] : (l-1 >= 0) and (-l+loop >= 0)})
	 *         for : 0 <= k <= n-1 (stride = 1)
	 *           S1 (depth = 2) [q=add(q,mul(z[k],x[k]))]
	 *           ([q]) = f([q, z[k], x[k]])
	 *           (Domain = [loop,n] -> {S1[l,k] : (k >= 0) and (-k+n-1 >= 0) and (l-1 >= 0) and (-l+loop >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_03_inner_product_GScopRoot_inner_product_scop_0_ () {
		String path = "polymodel_src_livermore_loops_03_inner_product_GScopRoot_inner_product_scop_0_";
		// SCoP Extraction Data
		String domains = "[loop, n] -> { S0[l] : l >= 1 and l <= loop; S1[l, k] : k >= 0 and k <= -1 + n and l >= 1 and l <= loop }";
		String idSchedules = "[loop, n] -> { S0[l] -> [0, l, 0, 0, 0]; S1[l, k] -> [0, l, 1, k, 0] }";
		String writes = "[loop, n] -> { S0[l] -> q[]; S1[l, k] -> q[] }";
		String reads = "[loop, n] -> { S1[l, k] -> z[k]; S1[l, k] -> x[k]; S1[l, k] -> q[] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[loop, n] -> { S1[l, 0] -> S0[l] : l <= loop and n >= 1 and l >= 1; S1[l, k] -> S1[l, -1 + k] : k >= 1 and k <= -1 + n and l >= 1 and l <= loop }";
		String valueBasedPlutoSchedule = "[loop, n] -> { S1[l, k] -> [l, l + k, 0]; S0[l] -> [0, l, 1] }";
		String valueBasedFeautrierSchedule = "[loop, n] -> { S1[l, k] -> [1 + k, l]; S0[l] -> [0, l] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[loop, n] -> { S1[l, 0] -> S0[l] : n >= 1 and l >= 1 and l <= loop; S0[l] -> S0[-1 + l] : l >= 2 and l <= loop and n <= 0; S1[l, k] -> S1[l, -1 + k] : l >= 1 and l <= loop and k >= 1 and k <= -1 + n; S0[l] -> S1[-1 + l, -1 + n] : l >= 2 and l <= loop and n >= 1 }";
		String memoryBasedPlutoSchedule = "[loop, n] -> { S1[l, k] -> [l, k, 1]; S0[l] -> [l, 0, 0] }";
		String memoryBasedFeautrierSchedule = "[loop, n] -> { S1[l, k] -> [l, 1, k]; S0[l] -> [l, 0, 0] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
	// SCoP #171
	/**
	 * --- Gecos Scop Root - Start Pretty Print ---
	 * SCoP path : general_linear_recurrence_equations_general_linear_recurrence_equations/scop_0
	 *   for : 0 <= k <= n-1 (stride = 1)
	 *     block : 
	 *       S0 (depth = 1) [b5[k+kb5i]=add(sa[k],mul(stb5,sb[k]))]
	 *       ([b5[k+kb5i]]) = f([sa[k], stb5, sb[k]])
	 *       (Domain = [n,kb5i] -> {S0[k] : (k >= 0) and (-k+n-1 >= 0)})
	 *       S1 (depth = 1) [stb5=sub(b5[k+kb5i],stb5)]
	 *       ([stb5]) = f([b5[k+kb5i], stb5])
	 *       (Domain = [n,kb5i] -> {S1[k] : (k >= 0) and (-k+n-1 >= 0)})
	 * --- Gecos Scop Root - Pretty Print Done  ---
	 **/
	public void test_polymodel_src_livermore_loops_19_general_linear_recurrence_equations_GScopRoot_general_linear_recurrence_equations_scop_0_ () {
		String path = "polymodel_src_livermore_loops_19_general_linear_recurrence_equations_GScopRoot_general_linear_recurrence_equations_scop_0_";
		// SCoP Extraction Data
		String domains = "[n, kb5i] -> { S0[k] : k >= 0 and k <= -1 + n; S1[k] : k >= 0 and k <= -1 + n }";
		String idSchedules = "[n, kb5i] -> { S1[k] -> [0, k, 1]; S0[k] -> [0, k, 0] }";
		String writes = "[n, kb5i] -> { S1[k] -> stb5[]; S0[k] -> b5[kb5i + k] }";
		String reads = "[n, kb5i] -> { S1[k] -> b5[kb5i + k]; S0[k] -> sb[k]; S1[k] -> stb5[]; S0[k] -> stb5[]; S0[k] -> sa[k] }";
		// Value Based PRDG & Schedules
		String valueBasedPrdg = "[n, kb5i] -> { S0[k] -> S1[-1 + k] : k >= 1 and k <= -1 + n; S1[k] -> S0[k] : k >= 0 and k <= -1 + n; S1[k] -> S1[-1 + k] : k >= 1 and k <= -1 + n }";
		String valueBasedPlutoSchedule = "[n, kb5i] -> { S0[k] -> [k, 0]; S1[k] -> [k, 1] }";
		String valueBasedFeautrierSchedule = "[n, kb5i] -> { S0[k] -> [k, 0]; S1[k] -> [k, 1] }";
		// Memory Based PRDG & Schedules
		String memoryBasedPrdg = "[n, kb5i] -> { S0[k] -> S1[-1 + k] : k >= 1 and k <= -1 + n; S1[k] -> S0[k] : k >= 0 and k <= -1 + n; S1[k] -> S1[-1 + k] : k >= 1 and k <= -1 + n }";
		String memoryBasedPlutoSchedule = "[n, kb5i] -> { S0[k] -> [k, 0]; S1[k] -> [k, 1] }";
		String memoryBasedFeautrierSchedule = "[n, kb5i] -> { S0[k] -> [k, 0]; S1[k] -> [k, 1] }";
		translateAndComputeTest(path,domains, idSchedules, writes, reads, valueBasedPrdg, valueBasedPlutoSchedule, valueBasedFeautrierSchedule, memoryBasedPrdg, memoryBasedPlutoSchedule, memoryBasedFeautrierSchedule);
	}
	
}
//End of generation