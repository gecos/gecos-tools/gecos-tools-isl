package fr.irisa.cairn.jnimap.isl.tests.impl;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLConstraint;
import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLPrettyPrinter;

public class ISLDivsTest extends TestCase {

	public static void testBasicSet() {
		basicSetBody();
	}
	private static void basicSetBody() {
		ISLBasicSet bset =  ISLFactory.islBasicSet("[M] -> { [i] : exists (e0 = [(57 + i)/64]: i >= 0 and i <= -1 + M and 64e0 <= 57 + i and 64e0 >= -6 + i and 64e0 >= 1 + i) }");
		ISLBasicSet bset2 = ISLFactory.islBasicSet("[M] -> { [i] : exists (e0 = [(64 + i)/64]: i >= 0 and i <= -1 + M and 64e0 <= 57 + i and 64e0 >= 1 + i) }");
		System.out.println(bset);
		System.out.println(bset2);
		for (ISLConstraint c : bset.getConstraints()) {
			System.out.println(ISLPrettyPrinter.asString(c));
			System.out.println(c.javaPrint());
		}
	}
}
