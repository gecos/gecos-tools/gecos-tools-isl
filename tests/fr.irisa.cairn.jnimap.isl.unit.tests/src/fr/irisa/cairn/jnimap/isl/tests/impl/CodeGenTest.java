package fr.irisa.cairn.jnimap.isl.tests.impl;

import fr.irisa.cairn.jnimap.isl.ISLASTBuild;
import fr.irisa.cairn.jnimap.isl.ISLASTNode;
import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import junit.framework.TestCase;

public class CodeGenTest extends TestCase  {

	public void test1() {
		ISLUnionSet dom = ISLFactory.islUnionSet("[N, M] -> { S1[i, j] : 0 <= i < N and 0 <= j < M }");
		ISLUnionMap sch = ISLFactory.islUnionMap("[N, M] -> { S1[i, j] -> [i] }");
		//JNIISLUnionMap sch = ISLFactory.islUnionMap("[N, M] -> { S1[i, j] -> [i,j] }");
		
		ISLASTBuild build = ISLASTBuild.buildFromContext(ISLFactory.islSet("[N,M] -> { : N>0 and M>0}"));
		ISLASTNode node = build.generateWithExpansionNodes(sch.intersectDomain(dom), "EX");
		System.out.println(node.toString());
	}

	public void test2() {
		ISLUnionSet dom = ISLFactory.islUnionSet("[N,M] -> { S1[i,j] : 0<=i<N and 0<=j<M; S2[i,j] : 0<=i<N and 0<=j<M }");
		ISLUnionMap sch = ISLFactory.islUnionMap("[N, M] -> { S1[i, j] -> [i]; S2[i,j] -> [i+1] }");
		//JNIISLUnionMap sch = ISLFactory.islUnionMap("[N, M] -> { S1[i, j] -> [i,j] }");

		ISLASTBuild build = ISLASTBuild.buildFromContext(ISLFactory.islSet("[N,M] -> { : N>0 and M>0}"));
		ISLASTNode node = build.generateWithExpansionNodes(sch.intersectDomain(dom), "EX");
		System.out.println(node.toString());
	}
}
