package fr.irisa.cairn.jnimap.isl.tests.codegen

import fr.irisa.cairn.jnimap.isl.ISLASTBuild
import fr.irisa.cairn.jnimap.isl.ISLASTNode
import fr.irisa.cairn.jnimap.isl.ISLFactory
import fr.irisa.cairn.jnimap.isl.ISLSet
import fr.irisa.cairn.jnimap.isl.ISLUnionMap
import fr.irisa.cairn.jnimap.isl.tests.codegen.JNIISLASTChecker
import org.junit.Test

class ISLCodegenChecker {
	
	def codegen(ISLSet context, ISLUnionMap umap) {
		var ISLASTBuild build = ISLASTBuild.buildFromContext(context)
		var ISLASTNode root = ISLASTNode.buildFromSchedule(build, umap)
		println(root)
		JNIISLASTChecker.adapt(root)
	}

//	@Test def void test0() {
//		var JNIISLSet context = islSet("[M,N]-> { : M>=1 & N >= 1 }")
//		var JNIISLUnionMap umap = islUnionMap('''
//			[N,M] -> { 
//				A[i,j] -> [i,j] : 0 <= i <= N and 0 <= j <= M ;
//				B[i,j] -> [i+2,j] : 0 <= i <= j and 0 <= j <= M
//			}''')
//		var print =  ScopPrettyPrinter.print(codegen(context,umap))
//		System.out.println("*"+print+"*");
//	}

	@Test def void test1() {
		var ISLSet context = ISLFactory.islSet("[M,N]-> { : M>=1 & N >= 1 }")
		var ISLUnionMap umap = ISLFactory.islUnionMap('''
		[N,M] -> { 
			A[i,j] -> [i,2j] : 0 <= i <= N and 0 <= j <= M ;
			B[i,j] -> [i,j] : 0 <= i <= N and 0 <= j <= M 
		}''')
		codegen(context,umap);
	}
}
