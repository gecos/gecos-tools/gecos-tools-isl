package fr.irisa.cairn.jnimap.isl.tests.impl;

import static fr.irisa.cairn.jnimap.isl.ISLFactory.islSet;

import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import junit.framework.TestCase;

public class ProjectionTest extends TestCase {
	public static void testMain() {
		
		/*
		 * Exemples de IslJNITest
		 */
		
		// Censé projeter j
		ISLSet set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println(set_b+" => "+set_b.projectOut(ISLDimType.isl_dim_set, 0, 1));
		
		// Censé projeter i
		set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println(set_b+" => "+set_b.projectOut(ISLDimType.isl_dim_set, 1, 1));
	
		// Projette les paramètres
		set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println(set_b+" => "+set_b.projectOut(ISLDimType.isl_dim_param, 0, 2));

		/*
		 * Projection avec les autres JNIISLDimType -> Projection d'un paramètre à chaque fois
		 */
		System.out.println();
		System.out.println("JNIISLDimType.isl_dim_all");
		set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println(set_b+" => "+set_b.projectOut(ISLDimType.isl_dim_all, 0, 0));
		
		System.out.println("JNIISLDimType.isl_dim_div");
		set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println(set_b+" => "+set_b.projectOut(ISLDimType.isl_dim_div, 0, 0));
		
		System.out.println("JNIISLDimType.isl_dim_in");
		set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println(set_b+" => "+set_b.projectOut(ISLDimType.isl_dim_in, 0, 0));
		
		System.out.println("JNIISLDimType.isl_dim_out");
		set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println(set_b+" => "+set_b.projectOut(ISLDimType.isl_dim_out, 0, 0));
		
		/*
		 * Projection sans paramètres -> Provoque une erreur
		 */
	
		//Set quasiment identique, supprimé les paramètres et remplacé N par 5
		set_b = islSet("{ [j,i] : j>= i  & j-5< 0  & i>= 0  & i-3< 0  }");
		System.out.println();
		System.out.println(set_b+" => "+set_b.projectOut(ISLDimType.isl_dim_set, 0, 1));
		
	}
}
