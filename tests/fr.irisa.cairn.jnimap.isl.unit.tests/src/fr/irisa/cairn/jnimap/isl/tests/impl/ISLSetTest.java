package fr.irisa.cairn.jnimap.isl.tests.impl;

import static fr.irisa.cairn.jnimap.isl.ISLFactory.islSet;

import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLMatrix;
import fr.irisa.cairn.jnimap.isl.ISLPoint;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLSpace;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.ISL_FORMAT;
import junit.framework.TestCase;

public class ISLSetTest extends TestCase {

	public static void testMain() {
		mainBody();
	}
	private static void mainBody() {
		
//		#### next^2_d_y ####
//		[M] -> {
//		 S[i, j] -> S[-1 + i, 2 + j] : i >= 2 and i <= -1 + M and j >= 0 and j <= -3 + M;
//		 S[i, -1 + M] -> S[i, 1] : i >= 2 and i <= -1 + M and M >= 2;
//		 S[i, -2 + M] -> S[i, 0] : i >= 2 and i <= -1 + M and M >= 2 
//		}
//		#### le ####
//		[M] -> {
//		 S[i, j] -> S[i', j'] : i' >= 1 + i;
//		 S[i, j] -> S[i, j'] : j' >= 1 + j 
//		}		

		ISLSet set1 = islSet("[M] -> { S[i, j] : i <= -1 + M and M >= 2 and i >= 0 and j >= 0 and j <= -1 + M and j <= M - i	}");
		System.out.println(set1.coalesce().lexMax());
		set1 = islSet("[M]->{ [i,j,i',j'] :  i >= 2 and i <= -2 + M and M >= 2 & j=M-1 & j'=1 & i=i'}");
		System.out.println(set1);
		

		ISLSet orig = islSet("[N]->{ [i,j] : i >= 0 && i < N && j >= 0 && j < N-i}");
		ISLSet nopDom = islSet("[N]->{ [i,j] : i >= N-3 && i < N-1 && j > N -i && j <= N - i + 3}");
		
		ISLSet union = orig.copy().union(nopDom.copy());
		System.out.println(union);
	
		
		ISLSet set = islSet("[N] -> { [i, j] : "+
		 " i >= 0 and j >= 0 and j <= -5 + N - i "+
		 " or i >= 0 and i <= -5 + N"+
		 " or i >= 0 and i <= -4 + N"+
		 " or i >= 0 and i <= -3 + N"+
		 " or i >= 0 and i <= -4 + N"+
		 " or N >= 3"+
		 " or N >= 4 "+
		"}");
		
		set = set.gist(orig).coalesce();
		System.out.println(set);
		
		
		//*
		ISLSet existential =  
			islSet("[n] -> { [i,j] : exists (a = [i/10] : 0 <= i and i <= n and i - 10 a <= 6 and  exists (c  : 0 <= c <10  and i=10a+c and j=c))}" );

		ISLSet set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0 | j>= i  & j> 15  & i>= 0  & i=3  }");
		
		set_b=existential;
		ISLSpace dims= set_b.getSpace();
		set_b= set_b.lift();
		System.out.println(dims.dim(ISLDimType.isl_dim_param));
		System.out.println(dims.dim(ISLDimType.isl_dim_in));
		
		System.out.println(set_b.toString(ISL_FORMAT.POLYLIB));
		System.out.println(set_b.toString(ISL_FORMAT.LATEX));
		System.out.println(set_b.toString(ISL_FORMAT.ISL));
		System.out.println(set_b.toString(ISL_FORMAT.OMEGA));
			
		
		int numberOfBasicSet = set_b.getNbBasicSets();
		for(int i=0;i<numberOfBasicSet;i++) {
			System.out.println("BSET "+i);
			ISLBasicSet bset = set_b.getBasicSetAt(i);
			System.out.println("BasicSet :\n"+bset);
			System.out.println("Equalities :");
			ISLMatrix mat = bset.getEqualityMatrix(
					ISLDimType.isl_dim_set, 
					ISLDimType.isl_dim_div, 
					ISLDimType.isl_dim_param, 
					ISLDimType.isl_dim_cst);
			for(int k=0 ; k<mat.getNbRows();k++) {
				for(int m=0 ; m<mat.getNbCols();m++) {
					System.out.print(" "+mat.getElement(k, m));
				}
				System.out.println(" ");
			}
			System.out.println("Inequalities :");
			mat = bset.getInequalityMatrix(
					ISLDimType.isl_dim_set, 
					ISLDimType.isl_dim_div, 
					ISLDimType.isl_dim_param, 
					ISLDimType.isl_dim_cst);
			for(int k=0 ; k<mat.getNbRows();k++) {
				for(int m=0 ; m<mat.getNbCols();m++) {
					System.out.print(" "+mat.getElement(k, m));
				}
				System.out.println(" ");
			}
		}
	}
	
	public void testOps() {
		int dim = 0;
		int size = 4;
		ISLSet set1 = islSet("[N] -> { S[i, j] : 0 <= i < N && 0 <= j < N - i }");
		System.out.println(set1);
		ISLSet pad = set1.pad(dim, size);
		System.out.println(pad);
	}
	
	public void testUniverseIntersectEmpty() {
		String emptyStr = "[N] -> { [i, j] : 1 = 0 }";
		String universeStr = "[N] -> { [i, j] }";
		ISLSet empty = ISLFactory.islSet(emptyStr);
		ISLSet universe = ISLFactory.islSet(universeStr);
		System.out.println("empty = "+empty);
		System.out.println("universe = "+universe);
		ISLSet intersect = universe.intersect(empty);
		System.out.println(intersect);
		
	}
	
	public void testNext() {
		String setStr = 
				"[N] -> { [i, j] : (j <= 3 and i >= 0 and i <= -2 + N and j >= N - i) or (j >= 0 and j <= -1 + N - i and i >= 0 and i <= -1 + N) }";
//				"[N] -> { [i, j] : j >= 0 and j <= -1 + N - i and i >= 0 and i <= -1 + N }";
		ISLSet origDomain = ISLFactory.islSet(setStr);
		System.out.println("orig domain = "+origDomain);
		System.out.println();
		
		ISLMap lexGTonjDim = ISLFactory.islMap("[N] -> { [i, j] -> [i,j'] : j' > j }");
		ISLMap lexGToniDim = ISLFactory.islMap("[N] -> { [i, j] -> [i',j'] : i' > i }");
		System.out.println("lexGTonjDim = "+lexGTonjDim);
		System.out.println("lexGToniDim = "+lexGToniDim);
		System.out.println();

		ISLMap mapFromOrigToOrig = ISLMap.buildFromDomainAndRange(origDomain.copy(), origDomain.copy());
		
		ISLMap nextOnjDim = mapFromOrigToOrig.copy().intersect(lexGTonjDim).lexMin().simplify();
		System.out.println("nextOnjDim = "+nextOnjDim);
		ISLMap nextOnjDimGisted = nextOnjDim.copy().gist(ISLMap.buildFromDomainAndRange(origDomain.copy(),ISLSet.buildUniverse(origDomain.getSpace().copy()))).simplify();
		System.out.println("nextOnjDim gisted = "+nextOnjDimGisted);
		System.out.println();
		
		ISLSet done1 = nextOnjDim.copy().getDomain().simplify();
		ISLSet gistedDone2 = nextOnjDimGisted.copy().getDomain().simplify();
		ISLSet done2 = gistedDone2.copy().intersect(origDomain.copy()).simplify();
		ISLSet gistedDone1 = done1.copy().gist(origDomain.copy()).simplify();
		System.out.println(done1);
		System.out.println(done2);
		//XXX : 
		System.out.println(gistedDone2);
		System.out.println(gistedDone1);
		assertTrue(done1.copy().isEqual(done2.copy()));
		assertTrue(gistedDone1.copy().isEqual(gistedDone2.copy()));
		System.out.println();
		

		ISLSet remainFull1_1 = origDomain.copy().subtract(done1.copy()).simplify();
		ISLSet remainFull1_2 = origDomain.copy().intersect(done1.copy().complement()).simplify();
		System.out.println(remainFull1_1);
		System.out.println(remainFull1_2);
		assertTrue(remainFull1_1.copy().isEqual(remainFull1_2.copy()));
		System.out.println();

		ISLSet gistedRemain1 = origDomain.copy().subtract(done1.copy()).gist(origDomain.copy()).simplify();
		ISLSet gistedRemain2 = origDomain.copy().subtract(gistedDone1.copy()).gist(origDomain.copy()).simplify();
		ISLSet gistedRemain3 = origDomain.copy().intersect(gistedDone1.copy().complement()).gist(origDomain.copy()).simplify();
		System.out.println(gistedRemain1);
		System.out.println(gistedRemain2);
		System.out.println(gistedRemain3);
		assertTrue(gistedRemain1.copy().isEqual(gistedRemain2.copy()));
		System.out.println();
		

		ISLMap mapFromRemainToOrig = ISLMap.buildFromDomainAndRange(remainFull1_1.copy(), origDomain.copy());
		ISLMap nextOniDim = mapFromRemainToOrig.copy().intersect(lexGToniDim).lexMin().simplify();
		System.out.println("nextOniDim = "+nextOniDim);
		ISLMap nextOniDimGisted = nextOniDim.copy().gist(ISLMap.buildFromDomainAndRange(origDomain.copy(),ISLSet.buildUniverse(origDomain.getSpace().copy()))).simplify();
		System.out.println("nextOniDim gisted = "+nextOniDimGisted);
		ISLMap nextOniDimGisted2 = nextOniDim.copy().gist(ISLMap.buildFromDomainAndRange(remainFull1_1.copy(),ISLSet.buildUniverse(origDomain.getSpace().copy()))).simplify();
		System.out.println("nextOniDim gisted2 = "+nextOniDimGisted2);
		System.out.println();
		
		ISLSet done_1 = nextOniDim.copy().getDomain().simplify();
		ISLSet remain2 = nextOniDim.copy().getDomain().complement().intersect(remainFull1_1.copy()).simplify();
		System.out.println(done_1);
		System.out.println("remain 2 = "+remain2);
		
		ISLSet done_2 = remain2.copy().complement().gist(origDomain.copy()).simplify();
		System.out.println(done_2);
		System.out.println(origDomain);
//		
		
	}

	public void testNext2() {
		String setStr = 
				"[N] -> { [i, j] : (j <= 3 and i >= 0 and i <= -2 + N and j >= N - i) or (j >= 0 and j <= -1 + N - i and i >= 0 and i <= -1 + N) }";
//				"[N] -> { [i, j] : j >= 0 and j <= -1 + N - i and i >= 0 and i <= -1 + N }";
		ISLSet origDomain = ISLFactory.islSet(setStr);
		System.out.println("orig domain = "+origDomain);
		System.out.println();
		
		ISLMap lexGTonjDim = ISLFactory.islMap("[N] -> { [i, j] -> [i,j'] : j' > j }");
		ISLMap lexGToniDim = ISLFactory.islMap("[N] -> { [i, j] -> [i',j'] : i' > i }");
		System.out.println("lexGTonjDim = "+lexGTonjDim);
		System.out.println("lexGToniDim = "+lexGToniDim);
		System.out.println();

		ISLMap mapFromUnivToOrig = ISLMap.buildFromDomainAndRange(ISLSet.buildUniverse(origDomain.getSpace().copy()), origDomain.copy());
		
		ISLMap nextOnjDim = mapFromUnivToOrig.copy().intersect(lexGTonjDim).lexMin().simplify();
		ISLMap next = nextOnjDim.copy().intersectDomain(origDomain.copy()).simplify();
		System.out.println("nextOnjDim = "+nextOnjDim);
		System.out.println(next);
		ISLMap nextOnjDimGisted = next.copy().gist(ISLMap.buildFromDomainAndRange(origDomain.copy(),ISLSet.buildUniverse(origDomain.getSpace().copy()))).simplify();
		System.out.println("nextOnjDim gisted = "+nextOnjDimGisted);
		System.out.println();
		
//		
		
	}
	public void testPoint() {
		String setStr = "[N]->{ [i,j] : 0<=i<=j<=N }";
		
		ISLSet set1 = ISLFactory.islSet(setStr);
		ISLPoint point = set1.samplePoint();
		System.out.println(point);
		
		ISLBasicSet set2a = point.copy().toBasicSet();
		System.out.println(set2a);
		
		ISLSet set2b = point.copy().toSet();
		System.out.println(set2b);
		
		ISLUnionSet set2c = point.toUnionSet();
		System.out.println(set2c);
	}

}
