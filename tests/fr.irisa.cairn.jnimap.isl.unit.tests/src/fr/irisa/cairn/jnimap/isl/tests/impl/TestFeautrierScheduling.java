package fr.irisa.cairn.jnimap.isl.tests.impl;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLSchedule;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;

public class TestFeautrierScheduling extends TestCase {

	public static void computeSchedule(String dom, String prdg) {
		ISLUnionSet domains = ISLFactory.islUnionSet(dom);
		ISLUnionMap prdgMap = ISLFactory.islUnionMap(prdg);
		ISLSchedule schedule = ISLSchedule.computeFeautrierSchedule(domains.copy(), prdgMap.copy());
		
		
		
		
		System.out.println(schedule);
		ISLUnionMap map = schedule.getMap();
		System.out.println(map);
	}
	
	public static void testMatrixProduct() {
		String domains = "[M, N, P] -> { S0[i, j] : i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0; S1[i, j, k] : i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0 and k <= -1 + P and k >= 0 }";
		String prdg = "[M, N, P] -> { S1[i, j, k] -> S1[i, j, k-1] : i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0 and k <= -1 + P and k >= 1; S1[i, j, k] -> S0[i, j] : k = 0 and i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0 and P >= 1 }";
		computeSchedule(domains, prdg);
	}
}