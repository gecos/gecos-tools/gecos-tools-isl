package fr.irisa.cairn.jnimap.isl.tests.impl;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.extra.ISLLexShiftTools;

public class ISLLexPredTest extends TestCase {
	
	private static void compareNativeAndJava(String str, int coalescing_depth, int delta) {
		ISLSet set = ISLFactory.islSet(str);
		System.out.println(set);
		long time;
		time = System.currentTimeMillis();
		ISLMap np2 = ISLLexShiftTools.lexPredPower(set, coalescing_depth, delta);
		time = System.currentTimeMillis() - time;
		System.out.println("V1 ("+time+"ms) : "+np2);
		

		time = System.currentTimeMillis();
		ISLMap np1 = set.getLexPredPowerMap(coalescing_depth,delta);
		time = System.currentTimeMillis() - time;
		System.out.println("VNative ("+time+"ms) : "+np1);
		
		
		boolean eq = np1.isEqual(np2);
		if (!eq) System.out.println("\n  not eq");
		else System.out.println("\n  eq");
		assertTrue(eq);
	}


	public static void test0() {
		String str = "[N] -> { " +
				"[i] : 0 <= i < N " +
			"}";
		int delta = 12;
		int coalescing_depth = 5;
		compareNativeAndJava(str,coalescing_depth,delta);
	}
	
	public static void test1() {
		String str = "[N] -> { " +
				"[i,j] : 0 <= i < 8 && i <= j < N or 16 <= i < 32 && i <= j < N " +
			"}";
		int delta = 12;
		int coalescing_depth = 2;
		compareNativeAndJava(str,coalescing_depth,delta);
	}
	public static void test2() {
		String str = "[N] -> { " +
				"[i0,i1,0] : 0 <= i0 < N && i0 <= i1 < N " +
			"}";
		int delta = 1024;
		int coalescing_depth = 2;
		compareNativeAndJava(str,coalescing_depth,delta);
	}
	public static void test3() {
		String str = "[P, M, N] -> { " +
				"[i0, i1, i2] : i2 >= 0 and i1 >= 0 and i1 <= -1 + N and i0 >= 0 and i0 <= -1 + M and i2 <= -1 + P " +
			"}";
		int delta = 7;
		int coalescing_depth = 3;
		compareNativeAndJava(str,coalescing_depth,delta);
	}
	public static void test4() {
		String str = "[N] -> { " +
				"[0, i1, 0, i3, 1, i5, 0] : i5 >= i1 and i5 <= -1 + N and i3 >= 0 and i3 <= -2 + N - i1 and i1 >= 0}; " +
				"[0, i1, 0, i3, 0, 0, 0] : i3 >= 0 and i3 <= -2 + N - i1 and i1 >= 0 " +
			"}";
		int delta = 13;
		int coalescing_depth = 5;
		compareNativeAndJava(str,coalescing_depth,delta);
		
	}
	public static void test5() {
		String str = "[N] -> { " +
			"[i, j, k, 1] : k >= i and k <= -1 + N and j >= 0 and j <= -2 + N - i and i >= 0; " +
			"[i, j, k, 0] : j >= 0 and j <= -2 + N - i and i >= 0 and k = 0" +
		"}";
		int delta = 10;
		int coalescing_depth = 3;
		compareNativeAndJava(str,coalescing_depth,delta);
		

	}
}
