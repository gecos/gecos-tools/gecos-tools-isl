package fr.irisa.cairn.jnimap.isl.tests.impl;

import org.junit.Test;

import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLSchedule;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.ISLSchedule.JNIISLSchedulingOptions;

public class TestAdvancedScheduling {

	public static void computeSchedule(String dom, String prdg) {
		ISLUnionSet domains = ISLFactory.islUnionSet(dom);
		ISLUnionMap prdgMap = ISLFactory.islUnionMap(prdg);
		
		JNIISLSchedulingOptions options = new JNIISLSchedulingOptions();
		options.setAlgorithm(JNIISLSchedulingOptions.ISL_SCHEDULE_ALGORITHM_FEAUTRIER);
		options.setMax_constant_term(0);
		options.setMaximize_band_depth(0);
		options.setOuter_zero_distance(0);
		options.setSplit_scaled(0);

		ISLUnionMap validity = prdgMap.copy();
		ISLUnionMap proximity = prdgMap.copy();
		
		ISLSchedule schedule = ISLSchedule.computeSchedule(domains.copy(), validity, proximity, options);
		
		System.out.println(schedule);

		//bands are removed in ISL 0.19
//		List<JNIISLBand> bands = schedule.getBandForest().asJavaList();
//		System.out.println("Number of bands : "+bands.size());
//		int i = 1;
//		for (JNIISLBand band : bands) {
//			System.out.println("Band n° "+i+" : ");
//			System.out.println(" - Prefix  : "+band.getPrefixSchedule());
//			System.out.println(" - Partial : "+band.getPartialSchedule());
//			System.out.println(" - Suffix  : "+band.getSuffixSchedule());
//			i++;
//		}
		
		ISLUnionMap map = schedule.getMap();
		System.out.println(map);
	}
	
	@Test
	public void testMatrixProduct() {
		String domains = "[M, N, P] -> { S0[i, j] : i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0; S1[i, j, k] : i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0 and k <= -1 + P and k >= 0 }";
		String prdg = "[M, N, P] -> { S1[i, j, k] -> S1[i, j, k-1] : i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0 and k <= -1 + P and k >= 1; S1[i, j, k] -> S0[i, j] : k = 0 and i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0 and P >= 1 }";
		computeSchedule(domains, prdg);
	}

}
