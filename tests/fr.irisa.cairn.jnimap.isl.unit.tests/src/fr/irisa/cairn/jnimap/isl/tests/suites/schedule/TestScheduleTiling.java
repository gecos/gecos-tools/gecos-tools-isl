package fr.irisa.cairn.jnimap.isl.tests.suites.schedule;

import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.extra.JNIISLScheduleTiling;
import fr.irisa.cairn.jnimap.isl.jni.tests.AbstractTestFromISL;

public class TestScheduleTiling extends AbstractTestFromISL {

	private final static int tile_size = 8;
	
	@Override
	public void computeTest(String path, 
			ISLUnionSet domains, ISLUnionMap idSchedules, ISLUnionMap writes, ISLUnionMap reads,
			ISLUnionMap valueBasedPrdg, ISLUnionMap valueBasedPlutoSchedule, ISLUnionMap valueBasedFeautrierSchedule,
			ISLUnionMap memoryBasedPrdg, ISLUnionMap memoryBasedPlutoSchedule, ISLUnionMap memoryBasedFeautrierSchedule) {

		ISLUnionMap map = valueBasedPlutoSchedule;
//		JNIISLSchedulingOptions options = new JNIISLSchedulingOptions();
//		options.setAlgorithm(JNIISLSchedulingOptions.ISL_SCHEDULE_ALGORITHM_FEAUTRIER);
//		options.setMax_constant_term(0);
//		options.setMaximize_band_depth(0);
//		options.setOuter_zero_distance(0);
//		options.setSplit_scaled(0);
//
//		JNIISLUnionMap validity = prdg.copy();
//		JNIISLUnionMap proximity = prdg.copy();
//		
//		JNIISLSchedule schedule = JNIISLSchedule.computeSchedule(domains, validity, proximity, options);
//		7map = schedule.getMap();
		@SuppressWarnings("deprecation")
		ISLUnionMap tile = JNIISLScheduleTiling.tile(map,tile_size);
		System.out.println(tile);
	}
//	
//	@Override
//	public void test_polymodel_src_polybenchs_perso_prodmat_prodmat_scop_0() {
//		// TODO Auto-generated method stub
//		super.test_polymodel_src_polybenchs_perso_prodmat_prodmat_scop_0();
//	}
}
