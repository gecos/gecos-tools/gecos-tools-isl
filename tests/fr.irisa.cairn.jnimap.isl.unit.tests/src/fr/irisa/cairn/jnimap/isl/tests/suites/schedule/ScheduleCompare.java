package fr.irisa.cairn.jnimap.isl.tests.suites.schedule;

import fr.irisa.cairn.jnimap.isl.ISLSchedule;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import junit.framework.TestCase;

public class ScheduleCompare {


	public static void cmp(ISLUnionSet domains, ISLUnionMap prdg, int algo) {
		ISLSchedule emptySchedule = ISLSchedule.computeSchedule(domains.copy(), prdg.copy(), ISLUnionMap.buildEmpty(prdg.copy().getSpace()), algo);
		ISLSchedule prdgSchedule = ISLSchedule.computeSchedule(domains.copy(), prdg.copy(), prdg.copy(), algo);
		TestCase.assertTrue(emptySchedule.getMap().isEqual(prdgSchedule.getMap()));
	}
}
