package fr.irisa.cairn.jnimap.isl.tests.impl;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLSpace;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.JNIPtrBoolean;

public class TestUnionMap extends TestCase{
	
	final static String ex0=
		"[N]-> {"+
			"S1[i] -> [s0,s1,s2] : i-N< 0 and i>= 0 and s0=i and  s1=0 and  s2=0;"+
			"S2[i,j] -> [s0,s1,s2] : i-N< 0 and i>= 0 and i-2> 0 and j-N< 0 and j-i>= 0 and s0=i and  s1=j and  s2=1;"+
			"S3[i,j] -> [s0,s1,s2] : i-N< 0 and i>= 0 and i-2> 0 and j-N< 0 and j-i>= 0 and s0=i and  s1=i+j and  s2=1"+
		"}";

	public void testUnionMapfromString() {
		ISLUnionMap unionMap = ISLFactory.islUnionMap(ex0);
		//unionMap.transitiveClosure(JNIPtrInt)
		System.out.println(unionMap);
	}
	
	
	final static String ex1="{ A[i] -> A[i+1] : 0 <= i <= 3; B[] -> A[2] }";

	
	public void testUnionMapTransitiveClosure() {
		ISLUnionMap unionMap = ISLFactory.islUnionMap(ex1);
		System.out.println("input="+unionMap);
		JNIPtrBoolean exact = new JNIPtrBoolean();
		ISLUnionMap res  = unionMap.copy().transitiveClosure(exact);
		System.out.println("exact="+exact);
		System.out.println("res="+res);
		
	}

	final static String ex2=
		"[N]-> {"+
			"S[i,j] -> S[i+1,j+1]   : i-N< 0 and i>= 0 and j-N< 0 and j>= 0 "+
		"}";


	public void testMapTransitiveClosure() {
		ISLMap map = ISLFactory.islMap(ex2);
		System.out.println("input="+map);
		JNIPtrBoolean exact = new JNIPtrBoolean();
		ISLMap closure = map.copy().transitiveClosure(exact);
		System.out.println("exact="+exact);
		System.out.println("res="+closure);
	}

	public void testWrites() {
		String setTxt = "[N, M, P] -> { S3[] -> a[]; S0[i, j] -> s[]; S1[i, j, k] -> s[]; S2[i, j] -> res[i, j] }";
		ISLUnionMap map = ISLFactory.islUnionMap(setTxt);
		String mapstr = map.toString();
		System.out.println(mapstr);
		System.out.println(setTxt);
	}

	public void testExtractMap() {
		ISLUnionMap unionMap = ISLFactory.islUnionMap(ex0);
		ISLSpace dim = unionMap.getSpace();
		ISLMap map = unionMap.extractMap(dim);
		JNIPtrBoolean exact = new JNIPtrBoolean();
		ISLMap res= map.copy().transitiveClosure(exact);
		System.out.println("exact="+exact);
		System.out.println("res="+res);
	}
	


	public void testIntersectDomain() {
		String schedules = "[N] -> {S0[i,j] -> [i,j,0,0] ; S1[i,j,k] -> [i,j,1,k]}";
		String domains = "[N] -> {S0[i,j] : 0 <= i,j < N ; S1[i,j,k] : 0 <= i,j,k < N}";
		
		ISLUnionMap sched = ISLFactory.islUnionMap(schedules);
		ISLUnionSet doms = ISLFactory.islUnionSet(domains);
		
		ISLUnionMap scattering = sched.intersectDomain(doms);
		
		System.out.println(scattering.toString().replace(";",";\n"));
	}
}
