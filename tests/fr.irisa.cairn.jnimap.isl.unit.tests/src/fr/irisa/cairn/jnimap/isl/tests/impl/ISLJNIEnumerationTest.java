package fr.irisa.cairn.jnimap.isl.tests.impl;

import java.util.List;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLPoint;
import fr.irisa.cairn.jnimap.isl.ISLSet;

public class ISLJNIEnumerationTest extends TestCase {

	public void testSet1() {
		set1Body();
	}
	private void set1Body() {
		int size = 32;
		ISLSet set = ISLFactory.islSet("{ [i] : 0 <= i < "+size+"}");
		int n = set.getNbPoints();
		assertEquals(size, n);
	}

	public void testSet2() {
		int size = 15;
		ISLSet set = ISLFactory.islSet("{ [i,j] : 0 <= i < "+size+" and 0 <= j < "+size+"}");
		int n = set.getNbPoints();
		assertEquals(size*size, n);
	}

	public void testSet3() {
		int size = 99;
		ISLSet set = ISLFactory.islSet("{ [i,j] : 0 <= i < "+size+" and 0 <= j < i}");
		int n = set.getNbPoints();
		assertEquals((size*(size-1))/2, n);
	}

	public void testList1() {
		ISLSet set = ISLFactory.islSet("[N] -> { [i] : 0 <= i < N and N = 8}");
		
		List<ISLPoint> list = set.getPoints();
		System.out.println(list);
	}
}
