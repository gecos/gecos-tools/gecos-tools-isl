package fr.irisa.cairn.jnimap.isl.tests.impl;

import junit.framework.TestCase;

import org.junit.Test;

import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.codegen.FSMCodeGeneration;

public class OptimizedBouletFeautrierTest extends TestCase {





	@Test
	public static void test1() {
//		for (int i = 0; i < 50; i++) {
		String unionStr = "[N] -> { "
				+ "S0[i,j] : 0 <= i < N && 2 <= j <= N+2; "
				+ "S1[i,j] : 1 <= i < N && 0 <= j <= N && i+j <= N+5; "
				+ "}";
//		String sched = "[N] -> { "
//				+ "S0[i,j] -> [i,j] ; "
//				+ "S1[i,j] -> [i,j] "
//				+ "}";

		//FSMCodeGeneration.liveGuardOptimizations(ISLFactory.islUnionSet(unionStr), ISLFactory.islUnionMap(sched));
		FSMCodeGeneration.liveGuardOptimizations(ISLFactory.islUnionSet(unionStr));
//		}

	}

	/** extreme test */
	@Test
	public static void test2() {
//		for (int i = 0; i < 50; i++) {
		 String ex12 = "[k, n] -> { "+
		 "S0[l1, l2] :"+
		 	"(l1 <= -2 + n and l1 >= 2 - k and l1 >= k and l2 <= 2 + l1 and l2 <= 4 and k >= 0 and l2 >= 1 + l1) or "+
			"(l1 >= -4 + 2n and l1 <= -1 + k + n and n >= 2 + k and n >= 3 and l2 >= n and l2 <= 6 - n + l1) or "+ 
			"(l1 >= k and n >= 3 and l1 <= 1 and k >= 0 and l2 >= 1 + l1 and l2 <= 4) or "+
			"(l1 <= -1 + k + n and l2 >= n and n <= 4 + k and l1 >= -1 + n and l2 <= -3k + 2n + l1 and l1 <= -4 + 2n and 3l2 <= 1 - k + 4n and l2 <= 6 - n + l1);"+
		 "NOP[4 + 2k, 5 + k] : n = 5 + k and k >= 0 }";

//			String sched = "[N] -> { "
//					+ "S0[i,j] -> [i,j] ; "
//					+ "NOP[i,j] -> [i,j] "
//					+ "}";
//			
		//FSMCodeGeneration.liveGuardOptimizations(ISLFactory.islUnionSet(ex12), ISLFactory.islUnionMap(sched));
		FSMCodeGeneration.liveGuardOptimizations(ISLFactory.islUnionSet(ex12));
//		}
	}

}
