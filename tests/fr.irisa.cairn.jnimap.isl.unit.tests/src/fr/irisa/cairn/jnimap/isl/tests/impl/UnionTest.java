package fr.irisa.cairn.jnimap.isl.tests.impl;

import static fr.irisa.cairn.jnimap.isl.ISLFactory.islSet;

import fr.irisa.cairn.jnimap.isl.ISLSet;
import junit.framework.TestCase;

public class UnionTest extends TestCase {
	public static void testMain() {
		
		ISLSet set_a = islSet("[N,P]-> { [j,i] : j>= i  & j< 12  & i>= 0  & i-3< 0  }");

		ISLSet set_b = islSet("[N,P]-> { [j,i] : j>= i  & j> 15  & i>= 0  & i-3< 0  }");

		ISLSet set_c = set_a.union(set_b);

		System.out.println(set_c);
		
		ISLSet set_1 = islSet("[N,P]-> { [i,j,x,y] : 0<=i<N & 0<=j<P  }");

		ISLSet set_2 = islSet("[N,P]-> { [x,y,i,j] : 0<=x<N & 0<=y<P  }");

		ISLSet set_3 = set_1.intersect(set_2);

		System.out.println(set_3);
	}
}
