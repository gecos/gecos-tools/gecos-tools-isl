package fr.irisa.cairn.jnimap.isl.tests.impl;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLFold;
import fr.irisa.cairn.jnimap.isl.ISLPWQPolynomial;
import fr.irisa.cairn.jnimap.isl.ISLPWQPolynomialFold;
import fr.irisa.cairn.jnimap.isl.ISLQPolynomial;
import fr.irisa.cairn.jnimap.isl.ISLQPolynomialFold;
import fr.irisa.cairn.jnimap.isl.ISLQPolynomialFoldPiece;
import fr.irisa.cairn.jnimap.isl.ISLQPolynomialPiece;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLVal;

public class ISLPolynomialBoundsTest extends TestCase {

	public void computeTest(String polynomial) {
		System.out.println();
		
		ISLPWQPolynomial card = ISLFactory.islPWQPolynomial(polynomial);
		System.out.println("card = "+card);
		
		
		for (ISLQPolynomialPiece p : card.getPieces()) {
			assertNotNull(p);
		}

		for (ISLQPolynomialPiece p : card.getLiftedPieces()) {
			assertNotNull(p);
		}
		
		ISLPWQPolynomialFold bound = card.bound(ISLFold.isl_fold_min).coalesce();
		System.out.println("bound = card.bound(min) = "+bound);

		for (ISLQPolynomialFoldPiece p : bound.getLiftedPieces()) {
			assertNotNull(p);
		}
		for (ISLQPolynomialFoldPiece p : bound.getPieces()) {
//			System.out.println(p);
			ISLQPolynomialFold fold = p.getFold();
			ISLSet domain = p.getSet();
			for (ISLQPolynomial qp : fold.getQPolynomials()) {
				System.out.println(domain+" : "+qp);
				boolean isCst = qp.isConstant();
				if (isCst) {
					ISLVal val = qp.getConstantVal();
					long n = val.getNumerator();
					long d = val.getDenominator();
					System.out.println("Constant polynomial : "+n+"/"+d);
				} else {
					
				}
			}
			
		}
	}

	public void testFPTExample() {
		String polynomial = "[N] -> { [i, j] -> ((1 + N) - i) : j <= -1 + N - i and i >= 1 and j >= 1; [i, j] -> (((1 + N) - i) - j) : j = 0 and i <= -1 + N and i >= 1 }";
		computeTest(polynomial);
	}

	public void testFPTExamplebis() {
		String polynomial = "{ [N,i0, i1, i2, i3] -> N : i0 = 1 and i1 <= -1 + N and i2 <= -1 + N and i3 <= -1 + N and N >= 6 and i1 >= 0 and i2 >= 1 and i3 >= 1; [N,i0, i1, i2, i3] -> (N - i3) : i0 = 1 and i3 = 0 and i1 <= -1 + N and i2 <= -1 + N and N >= 6 and i2 >= 1 and i1 >= 0 }";
		computeTest(polynomial);
	}
	
	public void testMatMult() {
		String polynomial = "[N] -> { [i0, i1, i2, i3] -> N : i0 = 1 and i1 <= -1 + N and i2 <= -1 + N and i3 <= -1 + N and N >= 6 and i1 >= 0 and i2 >= 1 and i3 >= 1; [i0, i1, i2, i3] -> (N - i3) : i0 = 1 and i3 = 0 and i1 <= -1 + N and i2 <= -1 + N and N >= 6 and i2 >= 1 and i1 >= 0 }";
		computeTest(polynomial);
	}

	public void testMatMult3() {
		String polynomial = "[M, N, P] -> { [i0, i1, i2, i3] -> N : i0 = 1 and i3 <= -1 + N and i2 <= -1 + P and i1 <= -1 + M and i1 >= 0 and i2 >= 1 and i3 >= 1; [i0, i1, i2, i3] -> (N - i3) : i0 = 1 and i3 = 0 and N >= 1 and i2 <= -1 + P and i1 <= -1 + M and i1 >= 0 and i2 >= 1 }";
		computeTest(polynomial);
	}

	public void testMatMult3bis() {
		String polynomial = "{ [M, N, P,i0, i1, i2, i3] -> N : i0 = 1 and i3 <= -1 + N and i2 <= -1 + P and i1 <= -1 + M and i1 >= 0 and i2 >= 1 and i3 >= 1; [M, N, P,i0, i1, i2, i3] -> (N - i3) : i0 = 1 and i3 = 0 and N >= 1 and i2 <= -1 + P and i1 <= -1 + M and i1 >= 0 and i2 >= 1 }";
		computeTest(polynomial);
	}
}
