package fr.irisa.cairn.jnimap.isl.tests.impl;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLSet;

public class LiverangeTest extends TestCase {
	
	
//	final static String _prdg= " [M] -> { "+
//	"C[i0] -> C[j0] :" +
//	"0<= i0 <= M  and 0<= j0 <= M and j0=i0-3  "+ 
//	"}";
//
//	final static String _schedule= " [M] -> { "+
//	"C[i0] -> [c0 ] :"+ 
//		"c0=i0  "+
//	"}";
//
//	final static String _deltamap= " [M] -> { "+
//	"[i0, j0] -> [D0] :"+ 
//		"D0=j0-i0 "+
//	"}";

	final static String _dom= " [M] -> { [i0, i1] : 0<= i0 < M  and 0<= i1 < M }";

	final static String _prdg= " [M] -> { "+
	"[i0, i1] -> [j0, j1] :" +
	"0<= i0 < M  and 0<= i1 < M and 0<= j0 < M and 0<= j1 < M  and j0=i0 and j1=i1 or "+ 
	"0<= i0 < M  and 0<= i1 < M and 0<= j0 < M and 0<= j1 < M  and j0=i0-3 and j1=i1-4 "+ 
	"}";

	final static String _schedule= " [M] -> { "+
	"[i0, i1] -> [c0,c1 ] :"+ 
		"c0=i0 & c1=i1 "+
	"}";

	final static String _deltamap= " [M] -> { "+
	"[i0, i1,j0, j1] -> [D0,D1] :"+ 
		"D0=j0-i0 and D1=j1-i1 "+
	"}";

	
	
	public static void testLiveRange() {
		
		
		/**
		 
		 
		 CS=[M] -> { 
		 	[c0, c1, 1 + c0, c1'] : c1' >= 0 and c1' <= M and c0 >= 0 and c0 <= -1 + M and c1' <= c1; 
		 	[c0, c1, c0, c1'] : (c1' >= 0 and c1' <= M and c0 >= 1 and c0 <= M and c1' >= 1 + c1) or (c1 >= 0 and c1 <= M and c0 >= 1 and c0 <= M and c1' <= -1 + c1); 
		 	[c0, c1, -1 + c0, c1'] : c1 >= 0 and c1 <= M and c0 >= 1 and c0 <= M and c1' >= c1 
		 }
		 	
		DS=[M] -> { [1, D1] : D1 <= 0 and M >= 1; [0, D1] : (D1 >= 1 and M >= 1) or (D1 <= -1 and M >= 1); [-1, D1] : D1 >= 0 and M >= 1 }
		 */

		ISLSet domain= ISLFactory.islSet(_dom);
		System.out.println(domain);
//		JNIISLMap prdg = ISLFactory.islMap(_prdg);
//		JNIISLMap schedule = ISLFactory.islMap(_schedule);
		
//		
//		JNIISLSet DS = ISLArrayContractTools.buildDSSet(domain, prdg, schedule);
//
//		System.out.println("DS="+DS.copy().convexHull());
//		JNIISLSet res =null;
//		JNIISLSet proj = DS .copy();
//		long size = DS.getNDim();
//		for(int i =0; i< size;i++) {
//			proj = DS.copy();
//			if(i<size-1) {
//				proj = proj.copy().projectOut(JNIISLDimType.isl_dim_set, i+1, size-i-1).coalesce();
//				System.out.println("projecting out "+(size-i-1)+" dimensions starting after "+(i+1));
//			}
//			if(i>0) {
//				proj = proj.copy().projectOut(JNIISLDimType.isl_dim_set, 0, i).coalesce();
//				System.out.println("projecting out "+(i)+" dimensions starting after 0");
//			}
//			System.out.println("proj = "+proj);
//			JNIISLSet min = proj.copy().lexMin();
//
//			
//			System.out.println("min = "+min);
//			JNIISLSet max = proj.copy().lexMax();
//			if(res==null) {
//				res=max.copy();
//			} else {
//				System.out.println(max+"x"+res+"="+JNIISLSet.product(max.copy(), res.copy()));
//				res=JNIISLSet.product(max, res).flatten();
//			}
//			
//		}
//		System.out.println("res= "+res);
		
/*
 *conflict (i'<lastUse(i))=[M] -> { 
 *[c0, c1] -> [c0', c1'] : c0' >= 1 and c0' <= M and c1' >= 0 and c1' <= M and c0' <= c0; [c0, c1] -> [1 + c0, c1'] : c0 >= 0 and c0 <= -1 + M and c1' >= 0 and c1' <= M and c1' <= -1 + c1 }

 */
		
	}

}
