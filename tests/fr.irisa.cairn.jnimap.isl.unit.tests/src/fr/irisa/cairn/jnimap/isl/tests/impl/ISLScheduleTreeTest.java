package fr.irisa.cairn.jnimap.isl.tests.impl;

import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLIdentifier;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLSchedule;
import fr.irisa.cairn.jnimap.isl.ISLScheduleContextNode;
import fr.irisa.cairn.jnimap.isl.ISLScheduleDomainNode;
import fr.irisa.cairn.jnimap.isl.ISLScheduleExpansionNode;
import fr.irisa.cairn.jnimap.isl.ISLScheduleExtensionNode;
import fr.irisa.cairn.jnimap.isl.ISLScheduleFilterNode;
import fr.irisa.cairn.jnimap.isl.ISLScheduleGuardNode;
import fr.irisa.cairn.jnimap.isl.ISLScheduleMarkNode;
import fr.irisa.cairn.jnimap.isl.ISLScheduleNodeType;
import fr.irisa.cairn.jnimap.isl.ISLScheduleSequenceNode;
import fr.irisa.cairn.jnimap.isl.ISLScheduleSetNode;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLUnionPWMultiAff;
import junit.framework.TestCase;

public class ISLScheduleTreeTest extends TestCase{
	

	public static void testBuildDomainNode() {
		ISLSet domain = ISLFactory.islSet("[N]->{ [i,j] : 0<=i<=j<=N }");
		ISLScheduleDomainNode node = ISLScheduleDomainNode.buildFromDomain(domain.copy().toUnionSet());
		assert(node.getType() == ISLScheduleNodeType.isl_schedule_node_domain);
		assert(node.getDomain().isEqual(domain.toUnionSet()));
	}

	public static void testBuildExtensionNode() {
		ISLMap map = ISLFactory.islMap("[N]->{ [i,j]->[x,y] : 0<=x<=i and 0<=y<=j }");
		ISLScheduleExtensionNode node = ISLScheduleExtensionNode.buildFromExtensionMap(map.copy().toUnionMap());
		assert(node.getType() == ISLScheduleNodeType.isl_schedule_node_extension);
		assert(node.getExtension().isEqual(map.toUnionMap()));
	}
	
	public static void testBuildContextNode() {
		ISLSet domain = ISLFactory.islSet("[N]->{ [i,j] : 0<=i<=j<=N }");
		ISLScheduleDomainNode node = ISLScheduleDomainNode.buildFromDomain(domain.copy().toUnionSet());

		ISLSet context = ISLFactory.islSet("[N]->{ : N>10 }");
		ISLSchedule sch = node.getSchedule().insertContext(context.copy());

		ISLScheduleContextNode cnode = (ISLScheduleContextNode)sch.getRoot().getChild(0);

		assert(cnode.getType() == ISLScheduleNodeType.isl_schedule_node_context);
		assert(cnode.getContextDomain().isEqual(context));
	}

	
	public static void testBuildExpansionNode() {
		ISLSet dom1 = ISLFactory.islSet("[N]->{ [i,j] : 0<=i<=j<=N }");
		ISLSet dom2 = ISLFactory.islSet("[N]->{ [i,j,k] : 0<=i<=j<=N and 0<=k<=N }");
		
		ISLScheduleDomainNode dnode1 = ISLScheduleDomainNode.buildFromDomain(dom1.toUnionSet());
		ISLScheduleDomainNode dnode2 = ISLScheduleDomainNode.buildFromDomain(dom2.toUnionSet());
		
		ISLUnionPWMultiAff contraction = ISLFactory.ISLUnionPWMultiAff("[N]->{ [i,j,k]->[i,j] }");
			
		ISLSchedule sch = dnode1.getSchedule().expand(contraction.copy(), dnode2.getSchedule());
		ISLScheduleExpansionNode enode = (ISLScheduleExpansionNode)sch.getRoot().getChild(0);
		
		assert(enode.getType() == ISLScheduleNodeType.isl_schedule_node_expansion);
		assert(enode.getContraction().isPlainEqual(contraction));
	}

	public static void testBuildFilterNode() {
		ISLSet domain = ISLFactory.islSet("[N]->{ [i,j] : 0<=i<=j<=N }");
		ISLMap psch = ISLFactory.islMap("[N]->{ [i,j] -> [i+j] : }");
		ISLSet filter = ISLFactory.islSet("[N]->{ [i,j] : N>10 }");
		ISLScheduleDomainNode node = ISLScheduleDomainNode.buildFromDomain(domain.copy().toUnionSet());

		ISLSchedule schedule = node.getSchedule().insertPartialSchedule(psch.toUnionMap().toMultiUnionPWAff());
		ISLScheduleFilterNode fnode = schedule.getRoot().getChild(0).insertFilter(filter.copy().toUnionSet());

		assert(fnode.getType() == ISLScheduleNodeType.isl_schedule_node_filter);
		assert(fnode.getFilter().isEqual(filter.toUnionSet()));
	}

	public static void testBuildGuardNode() {
		ISLSet domain = ISLFactory.islSet("[N]->{ [i,j] : 0<=i<=j<=N }");
		ISLMap psch = ISLFactory.islMap("[N]->{ [i,j] -> [i+j] : }");
		ISLSet guard = ISLFactory.islSet("[N]->{ [i,j] : N>10 }");
		ISLScheduleDomainNode node = ISLScheduleDomainNode.buildFromDomain(domain.copy().toUnionSet());

		ISLSchedule schedule = node.getSchedule().insertPartialSchedule(psch.toUnionMap().toMultiUnionPWAff());
		ISLScheduleGuardNode gnode = schedule.getRoot().getChild(0).insertGuard(guard.copy());

		assert(gnode.getType() == ISLScheduleNodeType.isl_schedule_node_guard);
		assert(gnode.getGuard().isEqual(guard));
	}

	public static void testBuildMarkNode() {
		ISLSet domain = ISLFactory.islSet("[N]->{ [i,j] : 0<=i<=j<=N }");
		ISLMap psch = ISLFactory.islMap("[N]->{ [i,j] -> [i+j] : }");
		ISLIdentifier id = ISLFactory.islID("id");
		ISLScheduleDomainNode node = ISLScheduleDomainNode.buildFromDomain(domain.copy().toUnionSet());

		ISLSchedule schedule = node.getSchedule().insertPartialSchedule(psch.toUnionMap().toMultiUnionPWAff());
		ISLScheduleMarkNode mnode = schedule.getRoot().getChild(0).insertMark(id.copy());
		
		assert(mnode.getType() == ISLScheduleNodeType.isl_schedule_node_mark);
		assert(mnode.getIdentifier().getName().contentEquals(id.getName()));
	}

	public static void testBuildSequenceNode() {
		ISLSet dom1 = ISLFactory.islSet("[N]->{ [i,j] : 0<=i<=j<=N }");
		ISLSet dom2 = ISLFactory.islSet("[N]->{ [i,j,k] : 0<=i<=j<=N and 0<=k<=N }");
		
		ISLSchedule sch1 = ISLSchedule.buildFromDomain(dom1.toUnionSet());
		ISLSchedule sch2 = ISLSchedule.buildFromDomain(dom2.toUnionSet());
		
		ISLSchedule sch = sch1.sequence(sch2);
		ISLScheduleSequenceNode snode = (ISLScheduleSequenceNode)sch.getRoot().getChild(0);
		
		assert(snode.getType() == ISLScheduleNodeType.isl_schedule_node_sequence);
	}

	public static void testBuildSetNode() {
		ISLSet dom1 = ISLFactory.islSet("[N]->{ [i,j] : 0<=i<=j<=N }");
		ISLSet dom2 = ISLFactory.islSet("[N]->{ [i,j,k] : 0<=i<=j<=N and 0<=k<=N }");
		
		ISLSchedule sch1 = ISLSchedule.buildFromDomain(dom1.toUnionSet());
		ISLSchedule sch2 = ISLSchedule.buildFromDomain(dom2.toUnionSet());
		
		ISLSchedule sch = sch1.set(sch2);
		ISLScheduleSetNode snode = (ISLScheduleSetNode)sch.getRoot().getChild(0);
		
		assert(snode.getType() == ISLScheduleNodeType.isl_schedule_node_set);
	}
}
