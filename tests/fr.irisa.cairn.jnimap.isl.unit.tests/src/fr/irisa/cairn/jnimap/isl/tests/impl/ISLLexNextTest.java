package fr.irisa.cairn.jnimap.isl.tests.impl;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLMultiAff;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.extra.ISLLexShiftTools;

public class ISLLexNextTest extends TestCase {
	
	static boolean verbose = false;
	
	private static void compareNativeAndJava(String str, int coalescing_depth, int delta) {
		compareNativeAndJavaBody(str, coalescing_depth, delta);
	}
	private static void compareNativeAndJavaBody(String str, int coalescing_depth, int delta) {
		ISLSet set = ISLFactory.islSet(str);
		if (verbose) System.out.println(set);
		long time;
		time = System.currentTimeMillis();
		ISLMap np2 = ISLLexShiftTools.lexNextPower(set, coalescing_depth, delta);
		time = System.currentTimeMillis() - time;
		if (verbose) System.out.println("V1 ("+time+"ms) : "+np2);
		
		time = System.currentTimeMillis();
		ISLMap np1 = set.getLexNextPowerMap(coalescing_depth,delta);
		time = System.currentTimeMillis() - time;
		if (verbose) System.out.println("VNative ("+time+"ms) : "+np1);
		
		boolean eq = np1.isEqual(np2);
		if (verbose) {
			if (!eq) System.out.println("\n  not eq");
			else System.out.println("\n  eq");
		}
		assertTrue(eq);
	}

	public static void test0() {
		String str = "[N] -> { " +
				"[i] : 0 <= i < N " +
			"}";
		int delta = 12;
		int coalescing_depth = 1;
		compareNativeAndJava(str,coalescing_depth,delta);
	}
	
	public static void test1() {
		String str = "[N] -> { " +
				"[i,j] : 0 <= i < 8 && i <= j < N or 16 <= i < 32 && i <= j < N " +
			"}";
		int delta = 12;
		int coalescing_depth = 2;
		compareNativeAndJava(str,coalescing_depth,delta);
	}
	public static void test2() {
		String str = "[N] -> { " +
				"[i0,i1,0] : 0 <= i0 < N && i0 <= i1 < N " +
			"}";
		int delta = 1024;
		int coalescing_depth = 2;
		compareNativeAndJava(str,coalescing_depth,delta);
	}
	public static void test3() {
		String str = "[P, M, N] -> { " +
				"[i0, i1, i2] : i2 >= 0 and i1 >= 0 and i1 <= -1 + N and i0 >= 0 and i0 <= -1 + M and i2 <= -1 + P " +
			"}";
		int delta = 7;
		int coalescing_depth = 3;
		compareNativeAndJava(str,coalescing_depth,delta);
	}
	public static void test4() {
		String str = "[N] -> { " +
				"[0, i1, 0, i3, 1, i5, 0] : i5 >= i1 and i5 <= -1 + N and i3 >= 0 and i3 <= -2 + N - i1 and i1 >= 0}; " +
				"[0, i1, 0, i3, 0, 0, 0] : i3 >= 0 and i3 <= -2 + N - i1 and i1 >= 0 " +
			"}";
		int delta = 13;
		int coalescing_depth = 5;
		compareNativeAndJava(str,coalescing_depth,delta);
		
	}
	public static void test5() {
		String str = "[N] -> { " +
			"[i, j, k, 1] : k >= i and k <= -1 + N and j >= 0 and j <= -2 + N - i and i >= 0; " +
			"[i, j, k, 0] : j >= 0 and j <= -2 + N - i and i >= 0 and k = 0" +
		"}";
		int delta = 10;
		int coalescing_depth = 3;
		compareNativeAndJava(str,coalescing_depth,delta);
	}
	
	public static void test7() {
		body7();
	}
	private static void body7() {
		String str = "[n] -> {[x] : 0 <= x < n }";
		ISLSet set = ISLFactory.islSet(str);
		if (verbose) System.out.println(set);
		List<Map<ISLSet, ISLMultiAff>> orderedLexNext = ISLLexShiftTools.gistedOrderedLexNext(set, 1);
		if (verbose) System.out.println(orderedLexNext);
	}
	
	public static void test8() {
		body8();
	}
	private static void body8() {
		String str = "[BLOCK, NTAP] -> { " +
				"[1, i, j] -> [1, i, 1 + j] 		: (j >= 1 - BLOCK + i and j <= -2 + NTAP and j >= 0 and 2j <= i and i <= -3 + 2BLOCK and i >= 1) or (i <= -3 + BLOCK + NTAP and 2j <= 3 + i and j <= NTAP and 2j >= 1 + i and NTAP >= 2 and i >= 1 and i <= -3 + 2BLOCK);" +
				"[1, i, NTAP] -> [1, i, 1 + NTAP] 	: i <= -3 + BLOCK + NTAP and NTAP >= 2 and i >= 2NTAP" +
				"}";
		String str2 = "[BLOCK, NTAP] -> { [1, i, j] -> [1, i, 1 + j] 		: (j >= 1 - BLOCK + i and j <= -2 + NTAP and j >= 0 and 2j <= i and i <= -3 + 2BLOCK and i >= 1) or (i <= -3 + BLOCK + NTAP and 2j <= 3 + i and j <= NTAP and 2j >= 1 + i and NTAP >= 2 and i >= 1 and i <= -3 + 2BLOCK) or (i <= -3 + BLOCK + NTAP and NTAP >= 2 and i >= 2NTAP and j = NTAP)}";
		
		ISLMap m = ISLFactory.islMap(str);
		ISLMap m2 = ISLFactory.islMap(str2);
		if (verbose) {
			System.out.println("m  = "+m);
			System.out.println("m  = "+m.copy().simplify()+" (simplified)");
			System.out.println("m2 = "+m2);
			System.out.println("equivalent? "+m2.isEqual(m));
		}
	}
	
	public static void test10() {
		body10();
	}
	private static void body10() {
		String str = "[BLOCK, NTAP] -> { [1, i, j] -> [1, i, 1 + j] : (NTAP = 32 and i >= 1 and j <= 31 and 2j >= -1 + i and 2j <= i and BLOCK >= 33 and i <= 29 + BLOCK) or (NTAP = 32 and 2j >= 1 + i and BLOCK >= 33 and i >= 1 and i <= 29 + BLOCK and 2j <= 3 + i and j <= 32) or (NTAP = 32 and j <= 30 and j >= 0 and j >= 1 - BLOCK + i and 2j <= -2 + i and BLOCK >= 33); [1, i, 32] -> [1, i, 33] : NTAP = 32 and i <= 29 + BLOCK and i >= 64; [1, i, 31] -> [1, i, 32] : NTAP = 32 and i <= 29 + BLOCK and i >= 64; [0, i, j] -> [0, i, 1 + j] : NTAP = 32 and j <= 33 and i >= 0 and 2j <= 4 + i and j >= 1 - BLOCK + i and j >= 0 and i <= 30 + BLOCK and BLOCK >= 33 }";
		ISLMap m = ISLFactory.islMap(str);
		if (verbose) 
			for (Entry<ISLSet, ISLMultiAff> e : m.getClosedFormRelation().entrySet())
				System.out.println(e.getValue()+" : "+e.getKey());
	}
	
	public static void test11() {
		body11();
	}
	private static void body11() {
		String str = "[N] -> { [i, j] : (j <= 3 and i >= 0 and i <= -2 + N and j >= N - i) or (j >= 0 and j <= -1 + N - i and i >= 0) }";
		ISLSet dom = ISLFactory.islSet(str);
		int nb = 2;
		List<Map<ISLSet, ISLMultiAff>> gistedOrderedLexNext = ISLLexShiftTools.gistedOrderedLexNext(dom, nb);
		if (verbose)  {
			System.out.println("\n******************************************");
			System.out.println("****************   res   *****************");
			System.out.println("******************************************\n");
			for (Map<ISLSet, ISLMultiAff> m : gistedOrderedLexNext) 
				for (Entry<ISLSet, ISLMultiAff> e : m.entrySet()) {
					System.out.println(e.getValue()+" : ");
					for (ISLBasicSet bset : e.getKey().getBasicSets())
						System.out.println("\t"+bset);
				}
				
		}
	}
	
	
	public static void test12() {	
		body12();
	}
	private static void body12() {
		//floyd warshal + bubbles
//		String str = "[k, n] -> { [l1, l2] : (l2 >= 0 and l2 <= -1 + n and l2 <= l1 and l2 >= 1 - n + l1 and k >= 0 and n >= 1 + k and n >= 3) or (l1 <= -2 + n and l1 >= 2 - k and l1 >= k and l2 <= 2 + l1 and l2 <= 4 and k >= 0 and l2 >= 1 + l1) or (l1 >= -4 + 2n and l1 <= -1 + k + n and n >= 2 + k and n >= 3 and l2 >= n and l2 <= 6 - n + l1) or (l1 >= k and n >= 3 and l1 <= 1 and k >= 0 and l2 >= 1 + l1 and l2 <= 4) or (l1 <= -1 + k + n and l2 >= n and n <= 4 + k and l1 >= -1 + n and l2 <= -3k + 2n + l1 and l1 <= -4 + 2n and 3l2 <= 1 - k + 4n and l2 <= 6 - n + l1); [4 + 2k, 5 + k] : n = 5 + k and k >= 0 }";
		String str = "[k, n] -> { [l1, l2] : (l1 <= -2 + n and l1 >= 2 - k and l1 >= k and l2 <= 2 + l1 and l2 <= 4 and k >= 0 and l2 >= 1 + l1) or (l1 >= -4 + 2n and l1 <= -1 + k + n and n >= 2 + k and n >= 3 and l2 >= n and l2 <= 6 - n + l1) or (l1 >= k and n >= 3 and l1 <= 1 and k >= 0 and l2 >= 1 + l1 and l2 <= 4) or (l1 <= -1 + k + n and l2 >= n and n <= 4 + k and l1 >= -1 + n and l2 <= -3k + 2n + l1 and l1 <= -4 + 2n and 3l2 <= 1 - k + 4n and l2 <= 6 - n + l1); [4 + 2k, 5 + k] : n = 5 + k and k >= 0 }";
		ISLSet dom = ISLFactory.islSet(str);
		int nb = 2;
		List<Map<ISLSet, ISLMultiAff>> gistedOrderedLexNext = ISLLexShiftTools.gistedOrderedLexNext(dom, nb);
		if (verbose)  {
			System.out.println("\n******************************************");
			System.out.println("****************   res   *****************");
			System.out.println("******************************************\n");
			for (Map<ISLSet, ISLMultiAff> m : gistedOrderedLexNext) 
				for (Entry<ISLSet, ISLMultiAff> e : m.entrySet()) {
					System.out.println(e.getValue()+" : ");
					for (ISLBasicSet bset : e.getKey().getBasicSets())
						System.out.println("\t"+bset);
				}
				
		}
	}
	

	//example from Antoine Morvan's master report.
	public static void test13() {
		body13();
	}
	private static void body13() {
		ISLSet s1 = ISLFactory.islSet("[] -> {[i,j] : j <= -i+19 && j <= i+11 && j >= -i+11 && 2j >= -i+19 }");
		ISLSet s2 = ISLFactory.islSet("[] -> {[i,j] : 3j <= 2i+9 && 2j <= -i+34 && 3j >= 2i-12 && 2j >= -i+6 }");
		
		ISLSet dom = s1.union(s2);
		int nb = 2;
		List<Map<ISLSet, ISLMultiAff>> gistedOrderedLexNext = ISLLexShiftTools.gistedOrderedLexNext(dom, nb);
		if (verbose)  {
			System.out.println("\n******************************************");
			System.out.println("****************   res   *****************");
			System.out.println("******************************************\n");
			for (Map<ISLSet, ISLMultiAff> m : gistedOrderedLexNext) 
				for (Entry<ISLSet, ISLMultiAff> e : m.entrySet()) {
					System.out.println(e.getValue()+" : ");
					for (ISLBasicSet bset : e.getKey().getBasicSets()) {
						System.out.println("\t"+bset);
						bset.free();
					}
					e.getKey().free();
					e.getValue().free();
				}
				
		}
		dom.free();
	}
	
	public static void testPower0() {
		String str = "[N] -> { " +
				"[0, i1, 0, i3, 1, i5, 0] : i5 >= i1 and i5 <= -1 + N and i3 >= 0 and i3 <= -2 + N - i1 and i1 >= 0}; " +
				"[0, i1, 0, i3, 0, 0, 0] : i3 >= 0 and i3 <= -2 + N - i1 and i1 >= 0 " +
			"}";
		ISLSet set = ISLFactory.islSet(str);
		if (verbose) System.out.println(set);
		int delta = 5;
		int coalescing_depth = (int)set.getNbIndices();
		int N = 5;
		if (verbose) System.out.println("**************************************");

		
		ISLMap npJava = null;
		long time = 0;
		for (int i = 0; i < N; i++) {
			long tmptime = System.currentTimeMillis();
			npJava = ISLLexShiftTools.lexNextPower(set, coalescing_depth, delta);
			tmptime = System.currentTimeMillis()- tmptime;
			time += tmptime;
		}
		time = time / N;
		if (verbose) {
			System.out.println("Java : ");
			System.out.println(npJava);
			System.out.println("Average for "+N+" runs : "+time+"ms");
			System.out.println("**************************************");
		}

		time = 0;
		ISLMap npNative = null;
		for (int i = 0; i < N; i++) {
			long tmptime = System.currentTimeMillis();
			npNative = ISLLexShiftTools.lexNext(set, coalescing_depth);
			npNative = npNative.power(delta);
			tmptime = System.currentTimeMillis()- tmptime;
			time += tmptime;
		}
		
		time = time / N;
		boolean test = npNative.isEqual(npJava);
		if (verbose) 
		{
			System.out.println("Native : ");
			System.out.println(npNative);
			System.out.println("Average for "+N+" runs : "+time+"ms");
			System.out.println("**************************************");	
			
			System.out.println("Equivalent ? "+test);
		}
		assertTrue(test);
	}
	
	public void testPower1() {
		int delta = 10;	
		
		String domainsStr = "[M, L] -> { S0[t, i, j] : j >= t and j <= L - t and i >= t and i <= L - t and t >= 0 and t <= M; S1[t, i, j] : j >= 1 + t and j <= 1 + L - t and i >= 1 + t and i <= 1 + L - t and t >= 0 and t <= M }";
		String plutoScheduleStr = "[M, L] -> { S0[t, i, j] -> [t, i, -i + j] : j >= t and j <= L - t and i >= t and i <= L - t and t >= 0 and t <= M; S1[t, i, j] -> [t, i + j, j] : j >= 1 + t and j <= 1 + L - t and i >= 1 + t and i <= 1 + L - t and t >= 0 and t <= M }";
		

		ISLUnionSet domains = ISLFactory.islUnionSet(domainsStr);
		ISLUnionMap plutoSchedule = ISLFactory.islUnionMap(plutoScheduleStr);
		

		ISLSet set = domains.apply(plutoSchedule).getSetAt(0);
		
		if (verbose) System.out.println(set);
		int coalescing_depth = 3;

		ISLMap lexnext = ISLLexShiftTools.lexNext(set, coalescing_depth);
		if (verbose) System.out.println(lexnext);
		
//		JNIISLMap lexnext = ISLFactory.islMap("[M, L] -> { [i0, i1, i2] -> [o0, o1, o2] : 4i0 = -1 + L and 2i1 = 1 + L and 4i2 = -1 + L and 4o0 = -1 + L and 2o1 = 3 + L and 4o2 = -7 - L and L <= 1 + 4M and L >= 5; [i0, i1, i2] -> [i0, o1, 1 + L - i0 - o1] : 2i1 = 1 + L and 2i2 = -1 + L - 2i0 and i0 <= M and 2i0 <= L and 2o1 >= 2 + L and 2o1 <= 3 + L and 4i0 <= -2 + L and 2i0 >= -1 + L and i0 >= 0 and L >= -1; [i0, i1, 0] -> [o0, 2 + L, o2] : 2i0 = L and 2o0 = L and 2i1 = L and 2o2 = 2 + L and L >= 2 and L <= 2M; [i0, i1, 0] -> [o0, 1 + L, o2] : 2i0 = -1 + L and 2o0 = -1 + L and 2i1 = 1 + L and 2o2 = 1 + L and L >= 3 and L <= 1 + 2M; [i0, i1, L - i0 - i1] -> [i0, o1, 1 + L - i0 - o1] : (i0 <= M and i1 >= i0 and 2o1 >= 2 + L and 2o1 <= 3 + L and 4i0 <= -2 + L and 2i1 <= L and i0 >= 0 and i1 >= 0 and i1 >= L - i0 and 2i0 <= L and i1 <= L - 2i0) or (i0 <= M and i1 >= i0 and 2o1 >= 2 + L and 2o1 <= 3 + L and 4i0 <= -2 + L and 2i1 <= L and i0 >= 0 and i1 >= 0 and i1 >= 1 + L - 2i0 and 2i0 >= 1 + L) or (i0 <= M and i1 >= i0 and 2o1 >= 2 + L and 2o1 <= 3 + L and 4i0 <= -2 + L and 2i1 <= L and i0 >= 0 and i1 <= -2 and i1 >= 1 + L - 2i0 and 2i0 >= 1 + L); [i0, i1, L - i0 - i1] -> [i0, i1, 1 + i0] : i1 >= 1 + L - 2i0 and i0 <= M and 2i0 <= L and i0 >= 0 and i1 >= 2 + 2i0 and i1 <= L - i0; [i0, 2 + 2L - 2i0, 1 + L - i0] -> [1 + i0, o1, L - i0 - o1] : 2o1 <= 3 + L and 4i0 <= -6 + L and i0 >= 0 and i0 <= -1 + M and 2o1 >= 2 + L and 2i0 >= -1 + L; [i0, i1, i2] -> [i0, i1, 1 + L - i0 - i1] : i0 <= M and i1 <= L - 2i0 and i2 >= i0 - i1 and i2 <= -1 + L - i0 - i1 and 2i1 >= 2 + L and i1 <= L - i0 and i0 >= 0 and i1 <= -1 + i0; [i0, i1, i2] -> [i0, i1, 1 + i0] : i1 >= 1 + L - 2i0 and i0 <= M and i2 >= i0 - i1 and i2 <= -1 + L - i0 - i1 and i1 >= 2 + 2i0 and i1 <= L - i0 and i0 >= 0 and i1 <= -1 + i0; [i0, i1, i2] -> [i0, i1, 1 + i2] : (i2 >= i0 - i1 and i2 <= -1 + L - i0 - i1 and i1 >= i0 and i1 <= L - i0 and i0 >= 0 and i0 <= M and 2i1 <= 1 + L and i1 <= L - 2i0) or (i2 >= i0 - i1 and i2 <= -1 + L - i0 - i1 and i1 >= i0 and i1 <= L - i0 and i0 >= 0 and i0 <= M and i1 >= 1 + L - 2i0 and i1 <= 1 + 2i0) or (i0 <= M and i1 <= L - 2i0 and i2 >= i0 - i1 and i2 <= -1 + L - i0 - i1 and 2i1 >= 2 + L and i1 <= L - i0 and i0 >= 0 and i1 >= i0) or (i1 >= 1 + L - 2i0 and i0 <= M and i2 >= i0 - i1 and i2 <= -1 + L - i0 - i1 and i1 >= 2 + 2i0 and i1 <= L - i0 and i0 >= 0 and i2 <= i0 and i1 >= i0) or (i2 <= L - i0 and i2 <= -2 - i0 + i1 and i2 >= 1 + i0 and i2 >= 1 + L - i0 - i1 and i2 >= -1 - L + i0 + i1 and i0 >= 0 and i0 <= M); [i0, i1, -1 - i0 + i1] -> [i0, 1 + i1, L - i0 - i1] : (i0 <= M and i0 >= 0 and 2i1 >= 2 + L and i1 <= 1 + L and i1 <= -1 + L - 2i0 and i1 <= 1 + 2i0 and i1 <= -1 + L - i0) or (i0 <= M and i0 >= 0 and 2i1 >= 2 + L and i1 <= 1 + L and i1 <= -1 + L - 2i0 and 2i0 <= L and i1 >= L - i0) or (i0 <= M and i0 >= 0 and 2i1 >= 2 + L and i1 <= 1 + L and i1 <= -1 + L - 2i0 and 2i0 >= 1 + L); [i0, 2 + L, 1 + L - i0] -> [i0, 3 + L, 2 + i0] : (i0 <= M and L >= -2 and i0 >= 0 and 2i0 <= -1 + L) or (i0 <= M and L <= -4 and i0 >= 0 and 2i0 <= -1 + L); [i0, -1, 1 + L - i0] -> [i0, 0, -1 - L + i0] : (i0 <= M and L <= -4 and i0 >= 0 and i0 <= 1 + L and 2i0 <= L and 4i0 <= -2 + L) or (i0 <= M and L <= -4 and i0 >= 0 and i0 <= 1 + L and 2i0 <= L) or (i0 <= M and L <= -4 and i0 >= 0 and i0 <= 1 + L and 2i0 >= 1 + L and 4i0 <= -2 + L) or (i0 <= M and L <= -4 and i0 >= 0 and i0 <= 1 + L and 2i0 >= 1 + L); [i0, i1, 1 + L - i0] -> [i0, 1 + i1, -L + i0 + i1] : (i0 <= M and i1 >= 3 + L and i0 >= 0 and i1 <= 1 + 2L - 2i0 and i1 <= -2) or (i0 <= M and i1 >= 3 + L and i0 >= 0 and i1 <= 1 + 2L - 2i0 and i1 >= 0); [i0, i1, L - i0 - i1] -> [i0, i1, 1 + L - i0 - i1] : i0 <= M and i1 <= L - 2i0 and 2i0 <= L and i0 >= 0 and 2i1 >= 2 + L and i1 <= L - i0; [i0, i1, L - i0 - i1] -> [i0, 2 + 2i0, 1 + i0] : (i0 <= M and i1 >= i0 and i0 <= 1 + L and L >= -1 and 4i0 >= -1 + L and 2i0 <= L and i1 <= L - 2i0 and i1 >= 0 and 2i1 <= L and i0 >= 0 and i1 >= L - i0) or (i0 <= M and i1 >= i0 and i0 <= 1 + L and L >= -1 and 4i0 >= -1 + L and 2i0 <= L and i1 <= L - 2i0 and i1 >= 0 and 2i1 <= L and i0 <= -1) or (i0 <= M and i1 >= i0 and i0 <= 1 + L and L >= -1 and 4i0 >= -1 + L and 2i0 <= L and i1 <= L - 2i0 and i1 >= 0 and 2i1 >= 2 + L and i0 >= 0) or (i0 <= M and i1 >= i0 and i0 <= 1 + L and L >= -1 and 4i0 >= -1 + L and 2i0 <= L and i1 <= L - 2i0 and i1 >= 0 and 2i1 >= 2 + L and i0 <= -1) or (i0 <= M and i1 >= i0 and i0 <= 1 + L and L >= -1 and 4i0 >= -1 + L and 2i0 <= L and i1 <= L - 2i0 and i1 <= -2 and 2i1 <= L and i0 <= -1) or (i0 <= M and i1 >= i0 and i0 <= 1 + L and L >= -1 and 4i0 >= -1 + L and 2i0 <= L and i1 <= L - 2i0 and i1 <= -2 and 2i1 >= 2 + L and i0 <= -1); [i0, -1, 1 + L - i0] -> [i0, 2 + 2i0, 1 + i0] : i0 <= M and i0 <= -1 and i0 <= 1 + L and L >= -1 and 4i0 >= -1 + L and 2i0 <= L; [i0, i1, L - i0 - i1] -> [i0, 1 + i1, -1 + i0 - i1] : (i1 <= -1 + L - i0 and i1 >= i0 and 2i0 <= L and i1 >= 1 + L - 2i0 and i1 <= 2i0 and i0 <= M and L >= -1 and i0 <= -1 and 2i1 <= 1 + L) or (i1 <= -1 + L - i0 and i1 >= i0 and 2i0 <= L and i1 >= 1 + L - 2i0 and i1 <= 2i0 and i0 <= M and L >= -1 and 4i0 <= -2 + L and 2i1 >= 2 + L) or (i1 <= -1 + L - i0 and i1 >= i0 and 2i0 <= L and i1 >= 1 + L - 2i0 and i1 <= 2i0 and i0 <= M and 4i0 >= -1 + L and L >= -1 and i0 <= 1 + L) or (i1 <= -1 + L - i0 and i1 >= i0 and 2i0 <= L and i1 >= 1 + L - 2i0 and i1 <= 2i0 and i0 <= M and i0 <= 1 + L and L <= -2) or (i1 <= -1 + L - i0 and i1 >= i0 and 2i0 <= L and i1 >= 1 + L - 2i0 and i1 <= 2i0 and i0 <= M and i0 >= 2 + L) or (i1 >= i0 and i1 <= -1 + L - i0 and i0 >= 0 and 2i1 <= 1 + L and i0 <= M and i1 <= L - 2i0 and 4i0 >= -1 + L and 2i0 >= 1 + L and i0 <= 1 + L and L >= -1) or (i1 >= i0 and i1 <= -1 + L - i0 and i0 >= 0 and 2i1 <= 1 + L and i0 <= M and i1 <= L - 2i0 and i0 <= 1 + L and L <= -2) or (i1 >= i0 and i1 <= -1 + L - i0 and i0 >= 0 and 2i1 <= 1 + L and i0 <= M and i1 <= L - 2i0 and i0 >= 2 + L) or (i0 <= M and i1 >= i0 and 2i1 <= -1 + L and i0 >= 0 and 4i0 <= -2 + L and i1 <= L - 2i0 and i1 <= -1 + L - i0) or (i0 <= M and i1 >= i0 and 2i1 >= L and 2i1 <= 1 + L and 4i0 <= -2 + L and i1 <= L - 2i0 and i0 >= 0 and 2i0 <= 1 + L and i1 <= -1 + L - i0) or (i0 <= M and i1 >= i0 and i0 <= 1 + L and L >= -1 and 4i0 >= -1 + L and 2i0 <= L and i1 <= L - 2i0 and i1 <= 2i0 and 2i1 <= 1 + L and i1 <= -1 + L - i0 and i0 >= 0); [i0, L - i0, 0] -> [i0, 2 + 2i0, 1 + i0] : (2i0 <= -2 + L and i0 <= L and i0 <= M and i0 >= 1 and 3i0 >= -1 + L and L >= -1 and 4i0 <= -2 + L) or (2i0 <= -2 + L and i0 <= L and i0 <= M and i0 >= 1 and 3i0 >= -1 + L and 4i0 >= -1 + L and L >= -1) or (2i0 <= -2 + L and i0 <= L and i0 <= M and i0 >= 1 and 3i0 >= -1 + L and L <= -2); [0, 1, 0] -> [0, 2, 1] : L = 1 and M >= 0; [i0, i1, -1 - i0 + i1] -> [i0, 1 + i1, -1 + i0 - i1] : (i1 >= 2 + 2i0 and i0 <= M and i0 >= 0 and 2i1 >= 2 + L and i1 <= -1 + L - i0 and i1 >= 2 + L) or (i0 <= M and i0 >= 0 and 2i1 >= 2 + L and i1 <= 1 + L and i1 <= -1 + L - 2i0 and 2i0 <= 1 + L and i1 >= 2 + 2i0 and i1 <= -1 + L - i0) or (i1 >= 2 + 2i0 and i0 <= M and 2i1 >= 2 + L and i1 <= 1 + L and i1 >= L - 2i0 and i1 >= -2 and i1 <= -1 + L - i0 and i0 >= 0); [i0, i1, -1 - i0 + i1] -> [i0, 1 + i1, 1 + i0] : (i1 >= 2 + 2i0 and i0 <= M and 2i1 >= 2 + L and i1 <= 1 + L and i1 >= L - 2i0 and i0 <= -1 and i1 <= -1 + L - i0) or (i1 >= 2 + 2i0 and i0 <= M and 2i1 >= 2 + L and i1 <= 1 + L and i1 >= L - 2i0 and 2i0 <= L and i1 >= L - i0) or (i1 >= 2 + 2i0 and i0 <= M and 2i1 >= 2 + L and i1 <= 1 + L and i1 >= L - 2i0 and 2i0 >= 1 + L); [i0, 1 + 2i0, -1 + L - 3i0] -> [i0, 2 + 2i0, -2 - i0] : (3i0 <= -2 + L and i0 >= -1 and 2i0 <= L and 4i0 >= L and i0 <= M and L >= -1 and i0 <= 1 + L) or (3i0 <= -2 + L and i0 >= -1 and 2i0 <= L and 4i0 >= L and i0 <= M and i0 <= 1 + L and L <= -2) or (3i0 <= -2 + L and i0 >= -1 and 2i0 <= L and 4i0 >= L and i0 <= M and i0 >= 2 + L); [i0, 2 + 2L - 2i0, 1 + L - i0] -> [1 + i0, 1 + i0, 0] : (i0 <= -1 + M and 2i0 <= -2 + L and i0 >= 0 and i0 <= L and L <= -2) or (i0 <= -1 + M and 2i0 <= -2 + L and i0 >= 0 and i0 >= 1 + L) or (2i0 <= -2 + L and 4i0 <= -6 + L and i0 >= 0 and i0 <= -1 + M) or (4i0 >= -5 + L and 2i0 <= -2 + L and i0 >= 0 and i0 <= -1 + M and i0 <= L and L >= -1) }");
		
		ISLMap npJava = lexnext.copy();

		ISLMap map = npJava.copy();
		for (int i = 1; i < delta; i++) {
			map = map.coalesce().applyRange(npJava.copy()).coalesce();
		}
		npJava = map.coalesce();


		ISLMap npNative = lexnext.copy();
		npNative = npNative.coalesce().power(delta).coalesce();

		
		boolean equivalence = npNative.isEqual(npJava);
		assertTrue(equivalence);
		
	}
}
