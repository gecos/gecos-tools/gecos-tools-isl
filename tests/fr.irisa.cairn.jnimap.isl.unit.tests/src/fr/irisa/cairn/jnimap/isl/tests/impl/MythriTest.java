package fr.irisa.cairn.jnimap.isl.tests.impl;
import static fr.irisa.cairn.jnimap.isl.ISLFactory.islMap;
import static fr.irisa.cairn.jnimap.isl.ISLFactory.islSet;

import java.util.List;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLAff;
import fr.irisa.cairn.jnimap.isl.ISLBasicMap;
import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLConstraint;
import fr.irisa.cairn.jnimap.isl.ISLDataflowAnalysis;
import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLLocalSpace;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLMultiAff;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLSpace;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;

public class MythriTest extends TestCase {
	
	public void test_simple_test() {
		ISLSet set_a = islSet("[M, N]->{[i,j] : 0<=i<M & 0<=j<N}");
		ISLMap map_a = islMap("[M, N]->{[i,j]->[a,b] : a = 0 & b = 0 & i = 0 & j = 0 | " +
				"1<=i<M & 1<=j<N & a = i-1 & b = j - 1 | " +
				"a = i & b = j+1 & M<=i<2*M &  N<=j<2*N}");
		ISLMap map_a_dom = map_a.copy();
		ISLSet set_b = map_a_dom.getDomain();
		ISLSet set_b_copy = set_b.copy();
		List<ISLBasicSet> set_b_list = set_b_copy.getBasicSets();
		System.out.println("Basic Sets of "+set_b_copy+"are");
		for ( int i = 0; i < set_b_list.size(); i++) {
			ISLBasicSet t = set_b_list.get(i);
			System.out.println(t);
		}
		ISLMap map_a_copy = map_a.copy();
		List<ISLBasicMap> map_a_list = map_a_copy.getBasicMaps();
		for ( int i = 0; i < map_a_list.size(); i++) {
			ISLBasicMap t = map_a_list.get(i);
			System.out.println(t);
		}
		ISLMap map_a_range = map_a.copy();
		ISLSet set_c = map_a_range.getRange();
		System.out.println("LexMin of the set is " + set_a.lexMax());
		System.out.println("LexMax of the set_b is " + set_b.lexMin());
		System.out.println("LexMax of the set_c is " + set_c.lexMin());
	}
	
	public void test_apply_test() {
		ISLMap map_w = islMap("[M,N]->{[i,j]->[a,b] : 0<=i<M & 0<=j<N & a = i & b = j}");
		ISLMap map_r = islMap("[M,N]->{[i,j]->[a,b] : 0<=i<M & 0<=j<N & a = i - 1 & b = j}");
		ISLMap map_w_comp = map_w.copy().reverse();
		System.out.println("Reverse of write " + map_w_comp);
		ISLMap dep = map_w.copy().applyDomain(map_r.copy()); //, map_r.copy());
		ISLMap dep1 = map_r.copy().applyRange(map_w_comp.copy());
		System.out.println("Apply Domain " + dep);
		System.out.println("Apply Range " + dep1);
	}
	
	public void test_union_test() {
		ISLMap map_r = islMap("[M,N]->{[i,j]->[a,b] :  a = i & b = j+2}");
		ISLMap map_r1 = islMap("[M,N]->{[i,j]->[a,b] :  a = i + 1 & b = j}");
		ISLMap map_r2 = islMap("[M,N]->{[i,j]->[a,b] :  a = i + 1 & b = j + 1}");
		ISLMap map_unionr = map_r.copy().union(map_r1.copy());
		map_unionr = map_unionr.copy().union(map_r2.copy());
		System.out.println("Union of reads " + map_unionr);
		map_unionr = map_unionr.copy().coalesce();
		System.out.println("Union of read and write " + map_unionr);
		for ( int d = 0; d < map_unionr.copy().getNbOutputs(); d++ ) {
			ISLMap proj_a = map_unionr.copy().projectOut(ISLDimType.isl_dim_out, d, 1);
			System.out.println("Projecting out 0 dimension"+ proj_a);
			ISLBasicMap proj_basic_a = proj_a.copy().getBasicMapAt(0);
			System.out.println("BasicMap "+ proj_basic_a);
			int max_dim = -1;
			for ( int i = 0; i < proj_basic_a.copy().getNbConstraints(); i++ ) {
				ISLConstraint c = proj_basic_a.copy().getConstraintAt(i);
				System.out.println(c);
				for ( int j = 0; j < c.getNbDims(ISLDimType.isl_dim_in); j++) {
					long n = c.getCoefficient(ISLDimType.isl_dim_in, j);
					if ( n != 0) {
						if ( max_dim == -1)
							max_dim = j;
						if ( max_dim < j)
							max_dim = j;
					}
					System.out.println(n + "*" + c.getDimName(ISLDimType.isl_dim_in, j));
				}
			}
			System.out.println("For Array Dimension " + d + "Max loop is " + max_dim);
		}
	}
	
	public void test_array_test() {
		ISLMap map_r = islMap("[M,N]->{[i,j]->[a,b] :  a = i & b = j+2}");
		ISLMap map_r1 = islMap("[M,N]->{[i,j]->[a,b] :  a = j + 1 & b = i}");
		ISLMap map_r2 = islMap("[M,N]->{[i,j]->[a,b] :  a = i + 1 & b = j + 1}");
		ISLMap map_unionr = map_r.copy().union(map_r1.copy());
		map_unionr = map_unionr.copy().union(map_r2.copy());
		System.out.println("Union of reads " + map_unionr);
		map_unionr = map_unionr.copy().coalesce();
		System.out.println("Union of read and write " + map_unionr);
		for ( int d = 0; d < map_unionr.copy().getNbOutputs(); d++ ) {
			ISLMap proj_a = map_unionr.copy().projectOut(ISLDimType.isl_dim_out, d, 1);
			System.out.println("Projecting out 0 dimension"+ proj_a);
			for ( int in_d = 0; in_d < proj_a.copy().getNbInputs(); in_d++) {
				ISLMap proj_a_d = proj_a.copy().projectOut(ISLDimType.isl_dim_in, in_d, 1);
				System.out.println(proj_a_d);
				
			}
		}
	}
	
	public void test_df_analysis() {
		String d = "{ S0[0]; S1[i] : i >= 1 and i <= 7 }";
		String w = "{ S0[i] -> a[]; S1[i] -> a[] }";
		String r = "{ S1[i] -> a[] }";
		String idSchedule = "{ S0[i] -> [0, i, 0]; S1[i] -> [0, i, 0] }";
		ISLUnionSet domains = ISLFactory.islUnionSet(d);
		ISLUnionMap writes = ISLFactory.islUnionMap(w);
		ISLUnionMap reads = ISLFactory.islUnionMap(r);
		ISLUnionMap schedule = ISLFactory.islUnionMap(idSchedule);
		ISLUnionMap prdg = ISLDataflowAnalysis.computeValueBasedDependenceGraph(domains, writes, reads, schedule);
		System.out.println(prdg);
	}
	
	public void test_int_div_constraint_test() {
		ISLSpace space = ISLSpace.allocSetSpace(1, 3);
		space = space.setDimName(ISLDimType.isl_dim_param, 0, "N");
		space = space.setDimName(ISLDimType.isl_dim_set, 0, "i");
		space = space.setDimName(ISLDimType.isl_dim_set, 1, "j");
		space = space.setDimName(ISLDimType.isl_dim_set, 2, "k");
		space = space.setTupleName(ISLDimType.isl_dim_set, "S");
		System.out.println(space);
		ISLLocalSpace lspace = space.copy().toLocalSpace();
		ISLConstraint setConstraint1 = ISLConstraint.buildInequality(lspace.copy());
		setConstraint1 = setConstraint1.setCoefficient(ISLDimType.isl_dim_set, 0, 1); 
		setConstraint1 = setConstraint1.setConstant(-1); //i >= 1
		ISLConstraint setConstraint2 = ISLConstraint.buildInequality(lspace.copy());
		setConstraint2 = setConstraint2.setCoefficient(ISLDimType.isl_dim_set, 0, -1); 
		setConstraint2 = setConstraint2.setCoefficient(ISLDimType.isl_dim_param, 0, 1); //i <= N
		ISLConstraint setConstraint3 = ISLConstraint.buildEquality(lspace.copy());
		setConstraint3 = setConstraint3.setCoefficient(ISLDimType.isl_dim_set, 0, 1);
		setConstraint3 = setConstraint3.setCoefficient(ISLDimType.isl_dim_set, 1, 2);
		System.out.println(setConstraint3); //i = 2j
		ISLConstraint setConstraint4 = ISLConstraint.buildInequality(lspace.copy());
		setConstraint4 = setConstraint4.setCoefficient(ISLDimType.isl_dim_set, 2, 1); 
		setConstraint4 = setConstraint4.setConstant(-1); //i >= 1
		ISLConstraint setConstraint5 = ISLConstraint.buildInequality(lspace.copy());
		setConstraint5 = setConstraint5.setCoefficient(ISLDimType.isl_dim_set, 2, -1); 
		setConstraint5 = setConstraint5.setCoefficient(ISLDimType.isl_dim_param, 0, 1); //i <= N
		ISLBasicSet basicSet = ISLBasicSet.buildUniverse(space.copy());
		basicSet = basicSet.addConstraint(setConstraint1);
		basicSet = basicSet.addConstraint(setConstraint2);
		basicSet = basicSet.addConstraint(setConstraint3);
		basicSet = basicSet.addConstraint(setConstraint4);
		basicSet = basicSet.addConstraint(setConstraint5);
		System.out.println(basicSet);
		basicSet = basicSet.copy().projectOut(ISLDimType.isl_dim_set, 1, 1);
		System.out.println(basicSet);
	}
	
	public void test_int_div_nested_test() {
		ISLSpace space = ISLSpace.alloc(1, 4, 1);
		space = space.setDimName(ISLDimType.isl_dim_param, 0, "N");
		space = space.setDimName(ISLDimType.isl_dim_in, 0, "i");
		space = space.setDimName(ISLDimType.isl_dim_in, 1, "i'");
		space = space.setDimName(ISLDimType.isl_dim_in, 2, "j");
		space = space.setDimName(ISLDimType.isl_dim_in, 3, "j'");
		space = space.setDimName(ISLDimType.isl_dim_out, 0, "k'");
		space = space.setTupleName(ISLDimType.isl_dim_set, "S");
		System.out.println(space);
		ISLLocalSpace lspace = space.copy().toLocalSpace();
		ISLConstraint setConstraint1 = ISLConstraint.buildInequality(lspace.copy());
		setConstraint1 = setConstraint1.setCoefficient(ISLDimType.isl_dim_in, 0, 1); 
		setConstraint1 = setConstraint1.setConstant(-1); //i >= 1
		ISLConstraint setConstraint2 = ISLConstraint.buildInequality(lspace.copy());
		setConstraint2 = setConstraint2.setCoefficient(ISLDimType.isl_dim_in, 0, -1); 
		setConstraint2 = setConstraint2.setCoefficient(ISLDimType.isl_dim_param, 0, 1); //i <= N
		ISLConstraint setConstraint3 = ISLConstraint.buildEquality(lspace.copy());
		setConstraint3 = setConstraint3.setCoefficient(ISLDimType.isl_dim_in, 0, -3);
		setConstraint3 = setConstraint3.setCoefficient(ISLDimType.isl_dim_in, 1, 4);
		System.out.println(setConstraint3); //3i = 4i'
		ISLConstraint setConstraint4 = ISLConstraint.buildInequality(lspace.copy());
		setConstraint4 = setConstraint4.setCoefficient(ISLDimType.isl_dim_in, 2, 1); 
		setConstraint4 = setConstraint4.setConstant(-1); //i >= 1
		ISLConstraint setConstraint5 = ISLConstraint.buildInequality(lspace.copy());
		setConstraint5 = setConstraint5.setCoefficient(ISLDimType.isl_dim_in, 2, -1); 
		setConstraint5 = setConstraint5.setCoefficient(ISLDimType.isl_dim_param, 0, 1); //i <= N
		ISLConstraint setConstraint6 = ISLConstraint.buildEquality(lspace.copy());
		setConstraint6 = setConstraint6.setCoefficient(ISLDimType.isl_dim_in, 3, -7);
		setConstraint6 = setConstraint6.setCoefficient(ISLDimType.isl_dim_in, 2, 2);
		setConstraint6 = setConstraint6.setCoefficient(ISLDimType.isl_dim_in, 1, 1);
		ISLConstraint setConstraint7 = ISLConstraint.buildEquality(lspace.copy());
		setConstraint7 = setConstraint7.setCoefficient(ISLDimType.isl_dim_in, 3, 1);
		setConstraint7 = setConstraint7.setCoefficient(ISLDimType.isl_dim_out, 0, -1);
		ISLBasicMap basicMap = ISLBasicMap.buildUniverse(space.copy());
		basicMap = basicMap.addConstraint(setConstraint1);
		basicMap = basicMap.addConstraint(setConstraint2);
		basicMap = basicMap.addConstraint(setConstraint3);
		basicMap = basicMap.addConstraint(setConstraint4);
		basicMap = basicMap.addConstraint(setConstraint5);
		basicMap = basicMap.addConstraint(setConstraint6);
		basicMap = basicMap.addConstraint(setConstraint7);
		System.out.println(basicMap);
		basicMap = basicMap.copy().projectOut(ISLDimType.isl_dim_in, 1, 1);
		System.out.println(basicMap);
		basicMap = basicMap.copy().projectOut(ISLDimType.isl_dim_in, 2, 1);
		System.out.println(basicMap);
	}
	
	public void test_int_div_nested_set_test() {
		ISLSpace space = ISLSpace.allocSetSpace(1, 4);
		space = space.setDimName(ISLDimType.isl_dim_param, 0, "N");
		space = space.setDimName(ISLDimType.isl_dim_set, 0, "i");
		space = space.setDimName(ISLDimType.isl_dim_set, 1, "i'");
		space = space.setDimName(ISLDimType.isl_dim_set, 2, "j");
		space = space.setDimName(ISLDimType.isl_dim_set, 3, "j'");
		space = space.setTupleName(ISLDimType.isl_dim_set, "S");
		System.out.println(space);
		ISLLocalSpace lspace = space.copy().toLocalSpace();
		ISLConstraint setConstraint1 = ISLConstraint.buildInequality(lspace.copy());
		setConstraint1 = setConstraint1.setCoefficient(ISLDimType.isl_dim_set, 0, 1); 
		setConstraint1 = setConstraint1.setConstant(-1); //i >= 1
		ISLConstraint setConstraint2 = ISLConstraint.buildInequality(lspace.copy());
		setConstraint2 = setConstraint2.setCoefficient(ISLDimType.isl_dim_set, 0, -1); 
		setConstraint2 = setConstraint2.setCoefficient(ISLDimType.isl_dim_param, 0, 1); //i <= N
		ISLConstraint setConstraint3 = ISLConstraint.buildEquality(lspace.copy());
		setConstraint3 = setConstraint3.setCoefficient(ISLDimType.isl_dim_set, 0, -3);
		setConstraint3 = setConstraint3.setCoefficient(ISLDimType.isl_dim_set, 1, 4);
		System.out.println(setConstraint3); //3i = 4i'
		ISLConstraint setConstraint4 = ISLConstraint.buildInequality(lspace.copy());
		setConstraint4 = setConstraint4.setCoefficient(ISLDimType.isl_dim_set, 2, 1); 
		setConstraint4 = setConstraint4.setConstant(-1); //i >= 1
		ISLConstraint setConstraint5 = ISLConstraint.buildInequality(lspace.copy());
		setConstraint5 = setConstraint5.setCoefficient(ISLDimType.isl_dim_set, 2, -1); 
		setConstraint5 = setConstraint5.setCoefficient(ISLDimType.isl_dim_param, 0, 1); //i <= N
		ISLConstraint setConstraint6 = ISLConstraint.buildEquality(lspace.copy());
		setConstraint6 = setConstraint6.setCoefficient(ISLDimType.isl_dim_set, 3, -7);
		setConstraint6 = setConstraint6.setCoefficient(ISLDimType.isl_dim_set, 2, 2);
		setConstraint6 = setConstraint6.setCoefficient(ISLDimType.isl_dim_set, 1, 1);
		ISLBasicSet basicMap = ISLBasicSet.buildUniverse(space.copy());
		basicMap = basicMap.addConstraint(setConstraint1);
		basicMap = basicMap.addConstraint(setConstraint2);
		basicMap = basicMap.addConstraint(setConstraint3);
		basicMap = basicMap.addConstraint(setConstraint4);
		basicMap = basicMap.addConstraint(setConstraint5);
		basicMap = basicMap.addConstraint(setConstraint6);
		System.out.println(basicMap);
		basicMap = basicMap.copy().projectOut(ISLDimType.isl_dim_set, 1, 1);
		System.out.println(basicMap);
		basicMap = basicMap.copy().projectOut(ISLDimType.isl_dim_set, 2, 1);
		System.out.println(basicMap);
	}
	
	public void test_nested_aff_set() {
		ISLSpace space = ISLSpace.allocSetSpace(1, 2);
		space = space.setDimName(ISLDimType.isl_dim_param, 0, "N");
		space = space.setDimName(ISLDimType.isl_dim_set, 0, "i");
		space = space.setDimName(ISLDimType.isl_dim_set, 1, "j'");
		space = space.setTupleName(ISLDimType.isl_dim_set, "S");
		ISLLocalSpace lspace = space.copy().toLocalSpace();
		ISLAff aff = ISLAff.buildZero(lspace.copy());
		aff = aff.setCoefficient(ISLDimType.isl_dim_in, 0, 3);
		aff = aff.scaleDown(2);
		aff = aff.floor();
		ISLAff aff1 = ISLAff.buildZero(lspace.copy());
		aff1 = aff1.setCoefficient(ISLDimType.isl_dim_in, 1, 2);
		ISLAff aff2 = aff1.add(aff);
		aff2 = aff2.scaleDown(7);
		aff2 = aff2.floor();
		ISLConstraint c = aff2.toInequalityConstraint();
		ISLBasicSet basicSet = ISLBasicSet.buildUniverse(space);
		basicSet = basicSet.addConstraint(c);
		System.out.println(basicSet);
	}
	
	public void test_tiling_test() {
		ISLSpace space = ISLSpace.allocSetSpace(0, 1);
		//space = space.setName(JNIISLDimType.isl_dim_set, 0, "i");
		//space = space.setTupleName(JNIISLDimType.isl_dim_set, "S");
		System.out.println(space);
		ISLLocalSpace lspace = space.toLocalSpace();
		
		
		ISLAff aff = ISLAff.buildZero(lspace.copy());
		aff = aff.setCoefficient(ISLDimType.isl_dim_in, 0, 1);
		aff = aff.scaleDown(4);
		aff = aff.floor();
		
		ISLAff aff1 = ISLAff.buildZero(lspace.copy());
		aff1 = aff1.setCoefficient(ISLDimType.isl_dim_in, 0, 1);
		aff1 = aff1.mod(4);
		
		ISLSpace mspace = ISLSpace.alloc(0, 1, 2);
		ISLMultiAff maff = ISLMultiAff.buildZero(mspace.copy());
		maff = maff.setAff(0, aff);
		maff = maff.setAff(1, aff1);
		
		ISLBasicMap bmap = maff.toBasicMap();
		System.out.println("Tiling Map " + bmap);
	}
	
	
	public void test_test11() {         
		 String stmt = "[k, n] -> { S0[l1, 4] -> S0_49[l1, 4] : n = 3 and l1 <= -1 and k >= 0 and l1 >= k and l1 >= 2 - k; " +
		 		"S0[3, 4] -> S0_32[3, 4] : n = 5 and k <= 3 and k >= 2; S0[2, 4] -> S0_26[2, 4] : k <= 2 and n >= 5 and k >= 0 and n >= 6 + k; " +
		 		"S0[1, 2] -> S0_10[1, 2] : k = 1 and n >= 3; S0[1, 4] -> S0_45[1, 4] : k = 0 and n = 3; " +
		 		"S0[l1, 1 + l1] -> S0_4[l1, 1 + l1] : l1 >= k and l1 <= 0 and l1 <= -2 + n and l1 >= 2 - k and k >= 0 and n >= 3; " +
		 		"S0[2, 4] -> S0_33[2, 4] : n = 4 + k and k >= 1 and k <= 2; S0[0, 4] -> S0_55[0, 4] : k = 0 and n = 4; S0[-4 + 2n, l2] -> S0_20[-4 + 2n, l2] : n >= 3 and n <= 3 + k and n >= 2 + k and l2 <= -5 - 3k + 4n and l2 >= n and l2 <= 1 + n and 3l2 <= -2 - k + 4n; S0[l1, l2] -> S0_22[l1, l2] : k = 0 and l2 <= 3 and l2 >= 1 + l1 and l1 >= 0 and n >= 3 and l1 <= 1 and l1 >= -4 + 2n and l1 <= -1 + n and l2 <= 6 - n + l1 and l2 >= n; S0[2, 4] -> S0_36[2, 4] : n = 5 + k and k >= 0 and k <= 2; S0[l1, 1 + l1] -> S0_3[l1, 1 + l1] : l1 >= k and l1 <= 0 and l1 <= -2 + n and l1 >= 2 - k and k >= 0 and n >= 3; S0[-5 + 2n, n] -> S0_25[-5 + 2n, n] : n >= 4 and n <= 4 + k and n >= 2 + k; S0[2, 4] -> S0_34[2, 4] : k >= 0 and n >= 2 + k and n >= 5 and k <= 2 and n <= 3 + k; S0[1, 3] -> S0_5[1, 3] : k = 1 and n >= 3; S0[l1, 4] -> S0_40[l1, 4] : l1 <= -1 and l1 <= 1 - k and k >= 0 and n >= 4 - k and l1 >= k and n <= 2 and n <= 4 + k and l1 <= -1 + k + n and l1 >= -1 + n and 4n >= 11 + k and l1 <= -4 + 2n and l1 >= 4 + 3k - 2n and k <= 1; S0[l1, 4] -> S0_48[l1, 4] : n = 3 and l1 <= -1 and k >= 0 and l1 >= k and l1 >= 2 - k; S0[1, 4] -> S0_54[1, 4] : k = 1 and n = 4; S0[1, 4] -> S0_41[1, 4] : k = 1 and n >= 7; S0[1, 4] -> S0_46[1, 4] : k = 1 and n = 3; S0[-5 + 2n, 1 + n] -> S0_60[-5 + 2n, 1 + n] : n >= 2 + k and n <= 3 + k and n >= 4; S0[2 + 2k, 4 + k] -> S0_61[2 + 2k, 4 + k] : n = 4 + k and k >= 1; S0[l1, 4] -> S0_42[l1, 4] : l1 >= 2 - k and l1 <= -1 and k >= 0 and l1 <= -3 + n and l1 >= k and n >= 5 and n >= 6 + k; S0[2k, 4 + k] -> S0_37[2k, 4 + k] : n = 2 + k and k >= 1; S0[2, 4] -> S0_30[2, 4] : n = 4 and k <= 2 and k >= 1; S0[1, 4] -> S0_57[1, 4] : k = 1 and n = 6; S0[1, 4] -> S0_53[1, 4] : k = 1 and n = 5; S0[3, 4] -> S0_35[3, 4] : n = 5 + k and k <= 3 and k >= 0; S0[l1, 4] -> S0_39[l1, 4] : l1 <= -1 and l1 <= 1 - k and k >= 0 and n >= 4 - k and l1 >= k and n >= 5 and n >= 6 + k and k <= 1; S0[1, 4] -> S0_52[1, 4] : k = 0 and n = 4; S0[-4 + 2n, l2] -> S0_19[-4 + 2n, l2] : n >= 3 and n <= 3 + k and n >= 2 + k and l2 <= -4 - 3k + 4n and l2 >= n and l2 <= 1 + n and 3l2 <= 1 - k + 4n; S0[l1, l2] -> S0_18[l1, l2] : l1 >= -4 + 2n and l1 <= -1 + k + n and n >= 2 + k and n >= 3 and l2 >= n and l2 <= 5 - n + l1 and l1 >= k and l2 <= 3 and l1 <= 1 and k >= 0 and l2 >= 1 + l1; S0[1, 2] -> S0_8[1, 2] : k = 1 and n >= 3; S0[-4 + 2n, l2] -> S0_16[-4 + 2n, l2] : n >= 3 and n <= 3 + k and n >= 2 + k and l2 <= -5 - 3k + 4n and l2 >= n and l2 <= 1 + n and 3l2 <= -2 - k + 4n; S0[3, 4] -> S0_27[3, 4] : n = 4 + k and k <= 3 and k >= 2; S0[1, 4] -> S0_56[1, 4] : k = 0 and n = 5; S0[1, 4] -> S0_44[1, 4] : k = 0 and n = 3; S0[1, 3] -> S0_6[1, 3] : k = 1 and n >= 3; S0[l1, l2] -> S0_14[l1, l2] : l1 >= -4 + 2n and l1 <= -1 + k + n and n >= 2 + k and n >= 3 and l2 >= n and l2 <= 5 - n + l1; S0[l1, 1 + l1] -> S0_2[l1, 1 + l1] : l1 >= k and l1 <= 0 and l1 <= -2 + n and l1 >= 2 - k and k >= 0 and n >= 3; S0[-6 + 2n, n] -> S0_62[-6 + 2n, n] : n <= 3 + k and n >= 2 + k and n >= 5; S0[l1, 1 + l1] -> S0_1[l1, 1 + l1] : l1 >= k and l1 <= 0 and l1 <= -2 + n and l1 >= 2 - k and k >= 0; S0[l1, l2] -> S0_11[l1, l2] : l1 >= -4 + 2n and l1 <= -1 + k + n and n >= 2 + k and n >= 3 and l2 >= n and l2 <= 5 - n + l1 and l1 <= -2 + n and l1 >= 2 - k and l1 >= k and l2 <= 2 + l1 and l2 <= 4 and k >= 0 and l2 >= 1 + l1; S0[l1, l2] -> S0_12[l1, l2] : l1 >= -4 + 2n and l1 <= -1 + k + n and n >= 2 + k and n >= 3 and l2 >= n and l2 <= 5 - n + l1 and l1 <= -2 + n and l1 >= 2 - k and l1 >= k and l2 <= 2 + l1 and l2 <= 3 and k >= 0 and l2 >= 1 + l1 and l1 <= 1; S0[l1, l2] -> S0_13[l1, l2] : l1 >= -4 + 2n and l1 <= -1 + k + n and n >= 2 + k and n >= 3 and l2 >= n and l2 <= 5 - n + l1 and l1 <= -2 + n and l1 >= 2 - k and l1 >= k and l2 <= 1 + l1 and l2 <= 3 and k >= 0 and l2 >= l1; S0[1, 2] -> S0_9[1, 2] : k = 1 and n >= 3; S0[l1, l2] -> S0_15[l1, l2] : l1 >= -4 + 2n and l1 <= -1 + k + n and n >= 2 + k and n >= 3 and l2 >= n and l2 <= 5 - n + l1 and l1 >= k and l2 <= 3 and l1 <= 1 and k >= 0 and l2 >= l1; S0[1, 2] -> S0_7[1, 2] : k = 1 and n >= 3; S0[3, 4] -> S0_31[3, 4] : k <= 3 and n <= 3 + k and n >= 6 and n >= 2 + k and k >= 0; S0[l1, l2] -> S0_17[l1, l2] : l1 >= -4 + 2n and l1 <= -1 + k + n and n >= 2 + k and n >= 3 and l2 >= n and l2 <= 5 - n + l1 and l1 >= k and l2 <= 4 and l1 <= 1 and k >= 0 and l2 >= 1 + l1; S0[0, 4] -> S0_51[0, 4] : k = 0 and n = 3; S0[l1, 4] -> S0_43[l1, 4] : l1 >= 2 - k and l1 <= -1 and k >= 0 and l1 <= -3 + n and l1 >= k and n >= 5 and n >= 6 + k; S0[2, 3] -> S0_0[2, 3] : k <= 2 and k >= 0 and n >= 4; S0[3, 4] -> S0_29[3, 4] : k = 1 and n = 5; S0[0, 4] -> S0_58[0, 4] : k = 0 and n = 5; S0[-5 + 2n, 1 + n] -> S0_59[-5 + 2n, 1 + n] : n >= 2 + k and n <= 3 + k and n >= 4; S0[l1, l2] -> S0_23[l1, l2] : k = 0 and l2 <= 3 and l2 >= 1 + l1 and l1 >= 0 and n >= 3 and l1 <= 1 and l1 >= -4 + 2n and l1 <= -1 + n and l2 <= 5 - n + l1 and l2 >= -1 + n; S0[l1, l2] -> S0_21[l1, l2] : k = 0 and l2 <= 3 and l2 >= 1 + l1 and l1 >= 0 and n >= 3 and l1 <= 1 and l1 >= -4 + 2n and l1 <= -1 + n and l2 <= 5 - n + l1 and l2 >= n; S0[0, 4] -> S0_50[0, 4] : k = 0 and n >= 6; S0[1, 4] -> S0_47[1, 4] : k = 1 and n = 3; S0[l1, l2] -> S0_24[l1, l2] : k = 0 and l2 <= 3 and l2 >= 1 + l1 and l1 >= 0 and n >= 3 and l1 <= 1; " +
		 		"S0[1, 4] -> S0_38[1, 4] : k = 0 and n >= 6; " +
		 		"S0[2, 4] -> S0_28[2, 4] : k = 0 and n = 4 }";          
         for (ISLMap map : ISLFactory.islUnionMap(stmt).getMaps()) {                 
                 System.out.println(map);         
        }      
     } 
	
		
}