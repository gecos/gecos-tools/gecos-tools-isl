package fr.irisa.cairn.jnimap.isl.tests.suites.schedule;

import fr.irisa.cairn.jnimap.isl.ISLSchedule;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.extra.SchedulePrinter;
import fr.irisa.cairn.jnimap.isl.jni.tests.AbstractTestFromISL;

public class TestScheduleInspect extends AbstractTestFromISL {
	
	@Override
	public void computeTest(String path, 
			ISLUnionSet domains, ISLUnionMap idSchedules, ISLUnionMap writes, ISLUnionMap reads,
			ISLUnionMap valueBasedPrdg, ISLUnionMap valueBasedPlutoSchedule, ISLUnionMap valueBasedFeautrierSchedule,
			ISLUnionMap memoryBasedPrdg, ISLUnionMap memoryBasedPlutoSchedule, ISLUnionMap memoryBasedFeautrierSchedule) {
		ISLSchedule schedule = ISLSchedule.computePlutoSchedule(domains.copy(), valueBasedPrdg.copy());
		schedule = ISLSchedule.computeFeautrierSchedule(domains, valueBasedPrdg);
		SchedulePrinter.printSchedule(schedule);
	}
}
