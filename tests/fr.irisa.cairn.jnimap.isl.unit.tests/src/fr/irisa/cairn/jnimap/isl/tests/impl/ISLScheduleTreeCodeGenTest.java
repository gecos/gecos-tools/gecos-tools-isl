package fr.irisa.cairn.jnimap.isl.tests.impl;

import fr.irisa.cairn.jnimap.isl.ISLASTBuild;
import fr.irisa.cairn.jnimap.isl.ISLASTLoopType;
import fr.irisa.cairn.jnimap.isl.ISLASTNode;
import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLMultiVal;
import fr.irisa.cairn.jnimap.isl.ISLOptions;
import fr.irisa.cairn.jnimap.isl.ISLSchedule;
import fr.irisa.cairn.jnimap.isl.ISLScheduleBandNode;
import fr.irisa.cairn.jnimap.isl.ISLScheduleDomainNode;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.ISLUnionSetList;
import junit.framework.TestCase;

public class ISLScheduleTreeCodeGenTest extends TestCase {
	
	public static void testCodeGen_Set() {
		ISLSet domain = ISLFactory.islSet("[N]->{ [i,j] : 0<=i<=j<=N }");
		
		ISLScheduleDomainNode node = ISLScheduleDomainNode.buildFromDomain(domain.toUnionSet());
		ISLASTBuild build = ISLASTBuild.alloc(ISLContext.getInstance());
		ISLASTNode ast = build.generate(node.getSchedule());
		System.out.println(ast);
	}

	public static void testCodeGen_SetWithSchedule() {
		ISLSet domain = ISLFactory.islSet("[N]->{ [i,j] : 0<=i<=j<=N }");
		ISLMap scattering = ISLFactory.islMap("[N]->{ [i,j] -> [j,i] : }");
	
		ISLSchedule sch = ISLSchedule.buildFromDomain(domain.toUnionSet());
		sch = sch.insertPartialSchedule(scattering.toUnionMap().toMultiUnionPWAff());
		ISLASTBuild build = ISLASTBuild.alloc(ISLContext.getInstance());
		ISLASTNode ast = build.generate(sch);
		System.out.println(ast);
	}

	public static void testCodeGen_UnionSet() {
		ISLUnionSet domain = ISLFactory.islUnionSet("[N]->{ S1[i,j] : 0<=i<=j<=N; S2[x,y,z] : 0<=x<=y<=z<=N }");
	
		ISLSchedule sch = ISLSchedule.buildFromDomain(domain);
		ISLASTBuild build = ISLASTBuild.alloc(ISLContext.getInstance());
		ISLASTNode ast = build.generate(sch);
		System.out.println(ast);
	}

	public static void testCodeGen_UnionSetWithSchedule() {
		ISLUnionSet domain = ISLFactory.islUnionSet("[N]->{ S1[i,j] : 0<=i<=j<=N; S2[x,y,z] : 0<=x<=y<=z<=N }");
		ISLUnionMap scattering = ISLFactory.islUnionMap("[N]->{ S1[i,j] -> [j,i]; S2[x,y,z]->[x,y+z] }");
	
		ISLSchedule sch = ISLSchedule.buildFromDomain(domain);
		sch = sch.insertPartialSchedule(scattering.toMultiUnionPWAff());
		ISLASTBuild build = ISLASTBuild.alloc(ISLContext.getInstance());
		ISLASTNode ast = build.generate(sch);
		System.out.println(ast);
	}

	public static void testCodeGen_UnionSetWithSequence() {
		ISLUnionSet domain = ISLFactory.islUnionSet("[N]->{ S1[i,j] : 0<=i<=j<=N; S2[x,y,z] : 0<=x<=y<=z<=N }");
		ISLUnionMap scattering = ISLFactory.islUnionMap("[N]->{ S1[i,j] -> [i]; S2[x,y,z]->[x] }");
		ISLUnionSetList filter = ISLUnionSetList.build(ISLContext.getInstance(), 2);
		filter = filter.add(ISLFactory.islUnionSet("[N]->{ S1[i,j] : }"));
		filter = filter.add(ISLFactory.islUnionSet("[N]->{ S2[x,y,z] : }"));
	
		ISLSchedule sch = ISLSchedule.buildFromDomain(domain);
		sch = sch.insertPartialSchedule(scattering.toMultiUnionPWAff());
		sch = sch.getRoot().getChild(0).getChild(0).insertSequence(filter).getSchedule();
		
		ISLASTBuild build = ISLASTBuild.alloc(ISLContext.getInstance());
		ISLASTNode ast = build.generate(sch);
		System.out.println(ast);
	}

	public static void testCodeGen_Unroll() {
		ISLSet domain = ISLFactory.islSet("[N]->{ [i,j] : 0<=i<=N  and 0<=j<=5}");
		ISLMap outer = ISLFactory.islMap("[N]->{ [i,j] -> [i] : }");
		ISLMap inner = ISLFactory.islMap("[N]->{ [i,j] -> [j] : }");
	
		ISLSchedule sch = ISLSchedule.buildFromDomain(domain.toUnionSet());
		sch = sch.insertPartialSchedule(inner.toUnionMap().toMultiUnionPWAff());
		sch = sch.insertPartialSchedule(outer.toUnionMap().toMultiUnionPWAff());
		ISLScheduleBandNode band = (ISLScheduleBandNode)sch.getRoot().getChild(0).getChild(0);
		band = (ISLScheduleBandNode)band.setASTLoopType(0, ISLASTLoopType.isl_ast_loop_unroll);
		ISLASTBuild build = ISLASTBuild.alloc(ISLContext.getInstance());
		ISLASTNode ast = build.generate(band.getSchedule());
		System.out.println(ast);
	}

	public static void testCodeGen_Atomic() {
		ISLUnionSet domain = ISLFactory.islUnionSet("[N]->{ S1[i] : 0<=i<=N; S2[i] : 0<=i<=N }");
		ISLUnionMap scattering = ISLFactory.islUnionMap("[N]->{ S1[i] -> [i]; S2[i]->[i+1] }");
	
		ISLSchedule sch = ISLSchedule.buildFromDomain(domain);
		sch = sch.insertPartialSchedule(scattering.toMultiUnionPWAff());
		ISLScheduleBandNode band = (ISLScheduleBandNode)sch.getRoot().getChild(0);
		band = (ISLScheduleBandNode)band.setASTLoopType(0, ISLASTLoopType.isl_ast_loop_atomic);
		ISLASTBuild build = ISLASTBuild.alloc(ISLContext.getInstance());
		ISLASTNode ast = build.generate(band.getSchedule());
		System.out.println(ast);
	}

	public static void testCodeGen_Separate() {
		ISLUnionSet domain = ISLFactory.islUnionSet("[N]->{ S1[i] : 0<=i<=N; S2[i] : 0<=i<=N }");
		ISLUnionMap scattering = ISLFactory.islUnionMap("[N]->{ S1[i] -> [i]; S2[i]->[i+1] }");
	
		ISLSchedule sch = ISLSchedule.buildFromDomain(domain);
		sch = sch.insertPartialSchedule(scattering.toMultiUnionPWAff());
		ISLScheduleBandNode band = (ISLScheduleBandNode)sch.getRoot().getChild(0);
		band = (ISLScheduleBandNode)band.setASTLoopType(0, ISLASTLoopType.isl_ast_loop_separate);
		ISLASTBuild build = ISLASTBuild.alloc(ISLContext.getInstance());
		ISLASTNode ast = build.generate(band.getSchedule());
		System.out.println(ast);
	}

	public static void testCodeGen_Isolate() {
		ISLUnionSet domain = ISLFactory.islUnionSet("{ A[i] : 0 <= i < 100 }");
		ISLUnionMap scattering = ISLFactory.islUnionMap("{ A[i] -> [3i] }");
		ISLUnionSet options = ISLFactory.islUnionSet("{ isolate[[] -> [x]] : 10 <= x <= 20 }");
	
		ISLSchedule sch = ISLSchedule.buildFromDomain(domain);
		sch = sch.insertPartialSchedule(scattering.toMultiUnionPWAff());
		ISLScheduleBandNode band = (ISLScheduleBandNode)sch.getRoot().getChild(0);
		band = band.setASTBuildOptions(options);
		ISLASTBuild build = ISLASTBuild.alloc(ISLContext.getInstance());
		ISLASTNode ast = build.generate(band.getSchedule());
		System.out.println(ast);
	}

	public static void testCodeGen_IsolateWithUnroll() {
		ISLUnionSet domain = ISLFactory.islUnionSet("{ A[i] : 0 <= i < 100 }");
		ISLUnionMap scattering = ISLFactory.islUnionMap("{ A[i] -> [i] }");
		ISLUnionSet options = ISLFactory.islUnionSet("{ isolate[[] -> [x]] : 10 <= x <= 20 }");
	
		ISLSchedule sch = ISLSchedule.buildFromDomain(domain);
		sch = sch.insertPartialSchedule(scattering.toMultiUnionPWAff());
		ISLScheduleBandNode band = (ISLScheduleBandNode)sch.getRoot().getChild(0);
		band = band.setASTBuildOptions(options);
		band = band.setIsolateASTLoopType(0, ISLASTLoopType.isl_ast_loop_unroll);
		ISLASTBuild build = ISLASTBuild.alloc(ISLContext.getInstance());
		ISLASTNode ast = build.generate(band.getSchedule());
		System.out.println(ast);
	}

	public static void testCodeGen_Tiling() {
		ISLOptions.setTileScaleTileLoops(ISLContext.getInstance(), 0);
		ISLOptions.setTileShiftPointLoops(ISLContext.getInstance(), 0);
		
		ISLUnionSet domain = ISLFactory.islUnionSet("{ A[i,j] : 0 <= i,j and i + j <= 100 }");
		ISLUnionMap scattering = ISLFactory.islUnionMap("{ A[i,j] -> [i,j] }");
		ISLSchedule sch = ISLSchedule.buildFromDomain(domain);
		sch = sch.insertPartialSchedule(scattering.toMultiUnionPWAff());
		
		ISLScheduleBandNode band = (ISLScheduleBandNode)sch.getRoot().getChild(0);
		ISLMultiVal mval = ISLFactory.islMultiVal(band.getSpace(), 10, 10);
		band = band.tile(mval);

		ISLASTBuild build = ISLASTBuild.alloc(ISLContext.getInstance());
		ISLASTNode ast = build.generate(band.getSchedule());
		System.out.println(ast);
	}

	public static void testCodeGen_TilingWithIsolateAndUnroll() {
		ISLOptions.setTileScaleTileLoops(ISLContext.getInstance(), 0);
		ISLOptions.setTileShiftPointLoops(ISLContext.getInstance(), 0);
		
		ISLUnionSet domain = ISLFactory.islUnionSet("{ A[i,j] : 0 <= i,j and i + j <= 100 }");
		ISLUnionMap scattering = ISLFactory.islUnionMap("{ A[i,j] -> [i,j] }");
		ISLUnionSet options = ISLFactory.islUnionSet("{ isolate[[] -> [a,b]] : 0 <= 10a and 0<= 10b and 10a+9+10b+9 <= 100}");
		ISLSchedule sch = ISLSchedule.buildFromDomain(domain);
		sch = sch.insertPartialSchedule(scattering.toMultiUnionPWAff());
		ISLScheduleBandNode band = (ISLScheduleBandNode)sch.getRoot().getChild(0);

		ISLMultiVal mval = ISLFactory.islMultiVal(band.getSpace(), 10, 10);
		band = band.tile(mval);
		band = band.setASTBuildOptions(options);
		
		options = ISLFactory.islUnionSet("{ isolate[[a,b] -> [c,d]] : 0 <= 10a and 0<= 10b and 10a+9+10b+9 <= 100}");
		band = ((ISLScheduleBandNode)band.getChild(0));
		band = band.setASTBuildOptions(options);
		band = band.setIsolateASTLoopType(1, ISLASTLoopType.isl_ast_loop_unroll);

		ISLASTBuild build = ISLASTBuild.alloc(ISLContext.getInstance());
		ISLASTNode ast = build.generate(band.getSchedule());
		System.out.println(ast);
	}

}
