package fr.irisa.cairn.jnimap.isl.tests.impl;

import fr.irisa.cairn.jnimap.isl.ISLBasicMap;
import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLConstraint;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLPrettyPrinter;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import junit.framework.TestCase;

public class ISLConstraintsTest extends TestCase {

	public static void atestSimpleSet() {
		simpleSetBody();
	}
	private static void simpleSetBody() {
		String stringDefinition = "{ [i] : 0 <= i && i < 8 }";
		ISLSet set = ISLFactory.islSet(stringDefinition);
		assertNotNull(set);
		int nbBset = set.getNbBasicSets();
		assertEquals(1, nbBset);
		ISLBasicSet bset = set.getBasicSetAt(0);
		assertNotNull(bset);
		int nbConstraints = bset.getNbConstraints();
		assertEquals(2, nbConstraints);

		// assume order in the list is order of appearance from left to
		// right in string definition
		for (int i = 0; i < nbConstraints; i++) {
			ISLConstraint c = bset.getConstraintAt(i);
			System.out.println(ISLPrettyPrinter.asString(c));
			System.out.println(c.javaPrint());
			System.out.println(c.toString());
		}
	}

	public static void testBasicMap() {
		basicMapBody();
	}
	
	private static void basicMapBody() {
		String stringDefinition = "[M] -> { [i] -> [a] : 0 <= i && i < 8 && i = a + 2 }";
		ISLBasicMap bmap = ISLFactory.islBasicMap(stringDefinition);
		assertNotNull(bmap);
		int nbConstraints = bmap.getNbConstraints();
		assertEquals(3, nbConstraints);

		// assume order in the list is order of appearance from left to right in
		// string definition
		for (int i = 0; i < nbConstraints; i++) {
			ISLConstraint c = bmap.getConstraintAt(i);
			System.out.println(ISLPrettyPrinter.asString(c));
			System.out.println(c.javaPrint());
			System.out.println(c.toString());
		}
	}

}
