package fr.irisa.cairn.jnimap.isl.tests.impl;

import static fr.irisa.cairn.jnimap.isl.ISLFactory.islSet;

import java.util.Map;
import java.util.Map.Entry;

import fr.irisa.cairn.jnimap.isl.ISLAff;
import fr.irisa.cairn.jnimap.isl.ISLBasicMap;
import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLMultiAff;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLVertex;
import fr.irisa.cairn.jnimap.isl.ISLVertices;
import junit.framework.TestCase;

public class ISLVerticesTest extends TestCase {

	public static void testMain() {
		ISLSet set_b = islSet("[N,P]-> { [i,j] : j>= i  & j-N< 0  & i>= 0  & i-3< 0 | j>= i  & j> 15  & i>= 0  & i=3  }");
		ISLBasicSet ns = set_b.getBasicSetAt(0);
		ISLVertices vert = ns.computeVertices();
		for(int i=0; i<vert.getNbVertex();i++){
			ISLVertex vertex = vert.getVertexAt(i);
			System.out.println(vertex.getID());
			ISLBasicSet domain = vertex.getDomain();
			System.out.println(domain);
			
			ISLMultiAff expr = vertex.getExpr();
			System.out.println(expr);
			
/* 
 * The following no longer works in ISL 0.020
 * 
 * This is because the multi_aff (expr) is using set space, and now cannot be converted to basic map.
 * The new method exprt.toBasicSet() could be used to obtain the set, but the rest does not work for basic sets.
 * 
 * In fact, what is being computed (piece-wise mapping to ISLAff) is already in the original JNIISLMultiAff, so
 * the purpose of the test is unclear.  
 */

			
//			
//			JNIISLBasicMap bm = expr.toBasicMap();
//			System.out.println(bm);
//			
////			bm = bm.projectOut(JNIISLDimType.isl_dim_in, 0, expr.getSpace().getSize(JNIISLDimType.isl_dim_set));
////			System.out.println(bm);
//			Map<JNIISLSet, JNIISLMultiAff> closedFormRelation = bm.getClosedFormRelation();
//			assert(closedFormRelation.size() == 1);
//			for (Entry<JNIISLSet, JNIISLMultiAff> e : closedFormRelation.entrySet()) {
//				assert(e.getKey().getNumberOfBasicSet() == 1);
////				JNIISLBasicSet validityDomain = e.getKey().getBasicSetAt(0);
//				JNIISLMultiAff exprs = e.getValue();
//				for(JNIISLAff aff : exprs.getAffs()) {
//					System.out.println(aff);
//				}
//			}
//			
////			System.out.println("toto");
//			System.out.println(closedFormRelation);
//			
////			JNIISLBasicMap bm = new JNIISLBasicMap(expr,JNIISLSpace.idMapDimFromSetDim(expr.getSpace()));
////			System.out.println(bm);
		}
		
	}
}
