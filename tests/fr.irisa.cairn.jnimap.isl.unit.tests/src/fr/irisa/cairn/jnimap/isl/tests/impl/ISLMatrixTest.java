package fr.irisa.cairn.jnimap.isl.tests.impl;

import org.junit.Assert;

import fr.irisa.cairn.jnimap.isl.ISLMatrix;
import junit.framework.TestCase;

public class ISLMatrixTest extends TestCase {

	public static void testMatrixConversion() {
		
		long[][] mat = new long[][] {
			new long[] {0, 1, 2, 3},
			new long[] {0, -1, -2, -3},
			new long[] {4, 5, 6, 7},
			new long[] {-4, -5, -6, -7}
		};
		
		ISLMatrix islMat = ISLMatrix.buildFromLongMatrix(mat);
		
		long[][] mat2 = islMat.toLongMatrix();
		
		Assert.assertTrue(isEqual(mat, mat2));
	}
	
	public static void testNullspace() {
		
		long[][] mat = new long[][] {
			new long[] {1, 0, 0, 0, 0},
			new long[] {0, 1, 0, 0, 0},
			new long[] {0, 0, 0, 0, 0},
			new long[] {0, 0, 1, 0, 0},
			new long[] {0, 0, 0, 0, 1},
			new long[] {0, 0, 1, 0, 0},
			new long[] {0, 0, 0, 0, 2},
			new long[] {0, 0, 1, 0, 0}
		};
		
		long[][] expected = new long[][] {
			new long[] {0},
			new long[] {0},
			new long[] {0},
			new long[] {1},
			new long[] {0}
		};
		
		ISLMatrix islMat = ISLMatrix.buildFromLongMatrix(mat);
		
		long[][] ker = islMat.rightKernel().toLongMatrix();
		
		Assert.assertTrue(isEqual(expected, ker));
	}
	
	private static boolean isEqual(long[][] matA, long[][] matB) {
		if (matA == null && matB == null) return true;
		if (matA == null || matB == null) return false;
		if (matA.length != matB.length) return false;
		if (matA.length == 0) return true;
		if (matA[0].length != matB[0].length) return false;
	
		boolean equal = true;
		for (int r = 0; r < matA.length; r++) {
			for (int c = 0; c < matA[0].length; c++) {
				equal &= matA[r][c] == matB[r][c];
			}	
		}
		return equal;
	}
}
