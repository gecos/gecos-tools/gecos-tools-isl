package fr.irisa.cairn.jnimap.isl.tests.impl;

import static fr.irisa.cairn.jnimap.isl.ISLFactory.islMap;
import static fr.irisa.cairn.jnimap.isl.ISLFactory.islSet;

import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLSpace;
import fr.irisa.cairn.jnimap.isl.JNIPtrBoolean;
import junit.framework.TestCase;

public class ISLMapTest extends TestCase {
	
	static ISLSet set0 = islSet("{ S0[i,j,k] : 0<=i<8 & 0<=j<8 & k=8i+j }");
	static ISLSet set1 = islSet("{ S1[i,j,k] : 0<=i<8 & 0<=j<8 & k=8i+j+2 }");
	static ISLSet set2 = islSet("{ S2[i,j,k] : 0<=i<8 & 0<=j<8 & k=8i+j }");

	static ISLMap map0 = islMap("{ S1[i,j] -> [k] : 0<=i<8 & 0<=j<8 & k=8i+j }");
	static ISLMap map1 = islMap("{ S2[i,j] -> [k] : 0<=i<8 & 0<=j<8 & k=8i+j+2 }");
	static ISLMap map2 = islMap("{ S1[i,j] -> [k] : 0<=i<8 & 0<=j<8 & k=8i+j }");

	public static void testBijective() {

		ISLSet S2S1  = map1.copy().getRange().intersect(map2.copy().getRange());
		ISLSet copy = S2S1.copy();
		System.out.println(copy);
		ISLMap reverse = map1.copy().reverse();
		ISLSet apply = copy.apply(reverse.copy());
		System.out.println(apply);
		System.out.println(reverse);
		ISLMap l = reverse.copy().projectOut(ISLDimType.isl_dim_out, 0, 1);
		System.out.println(l);

		ISLMap nonjuggling = islMap("{ [i,j] -> [k] : 0<=i<12 & 0<=j<9 & k=8i+j}");
		System.out.println(nonjuggling+" bijective='"+nonjuggling.isBijective()+"'");

		ISLMap nonjuggling2 = islMap("{ [i,j] -> [i',j'] :  i'=i & j'=j}");
		System.out.println(nonjuggling2+" bijective='"+nonjuggling2.isBijective()+"'");
	}
	
	public  static void testLexicographic() {
		ISLSpace dimensions = set0.getSpace();
		
		System.out.println("dim=("+dimensions+")");
		System.out.println(dimensions);
		
		ISLMap local = dimensions.copy().toLexGTfirstMap(1);
		System.out.println(local);
		local = dimensions.copy().toLexGTfirstMap(2);
		System.out.println(local);
		local = dimensions.copy().toLexGTfirstMap(3);
		System.out.println(local);

	}

	public static void testSingleValued() {
		ISLMap map; boolean singleValued;
		
		map = islMap("{ S1[i,j] -> [a,b] : 0<=i<8 & 0<=j<8 & a = i & b = j}");
		singleValued = map.isSingleValued();
		System.out.println("is "+map+" single valued?\n >> "+singleValued);
		assertTrue(singleValued);
		
		map = islMap("{ S1[i,j] -> [a,b] : 0<=i<8 & 0<=j<8 & 0 <= a < i &  j <= b < 8}");
		singleValued = map.isSingleValued();
		System.out.println("is "+map+" single valued?\n >> "+singleValued);
		assertFalse(singleValued);
		
		map = islMap("{ [i] -> [a, b] : b = i - 8a and a >= 0 and a <= 7 and 8a <= i and 8a >= -7 + i } ");
		singleValued = map.isSingleValued();
		System.out.println("is "+map+" single valued?\n >> "+singleValued);
		assertTrue(singleValued);
		
		map = map.reverse();
		singleValued = map.isSingleValued();
		System.out.println("is "+map+" single valued?\n >> "+singleValued);
		assertTrue(singleValued);
		
		int tileCst = 8;
		map = islMap("[n] -> { [i] -> [a,b] : 0 <= i < n && i = "+tileCst+"a + b && 0 <= b < "+tileCst+"}");
		singleValued = map.isSingleValued();
		System.out.println("is "+map+" single valued?\n >> "+singleValued);
		assertTrue(singleValued);
		
		map = map.reverse();
		singleValued = map.isSingleValued();
		System.out.println("is "+map+" single valued?\n >> "+singleValued);
		assertTrue(singleValued);
		

		map = islMap("[n] -> { [i,j,k] -> [a,b,c] : 0 <= i < n && 0 <= j < n && 0 <= k < n && a = i && b = j && c = n-1}");
		singleValued = map.isSingleValued();
		System.out.println("is "+map+" single valued?\n >> "+singleValued);
		assertTrue(singleValued);
		
		map = map.reverse();
		singleValued = map.isSingleValued();
		System.out.println("is "+map+" single valued?\n >> "+singleValued);
		assertFalse(singleValued);
		
	}

	public static void testReachingPathLengths() {
		ISLMap map = islMap("{ [i,j] -> [a,b] : 0<=i<8 & 0<=j<8 & a = i & b = j}");
		System.out.println(map);
		JNIPtrBoolean exact = new JNIPtrBoolean();
		ISLMap rpl = map.reachingPathLengths(exact);
		System.out.println(rpl);
		System.out.println("exact : "+exact);
		

		map = islMap("[N] -> { [i,j] -> [a,b] : 0<=i<N & 0<=j<N & a = j & b = i}");
		System.out.println(map);
		rpl = map.reachingPathLengths(exact);
		System.out.println(rpl);
		System.out.println("exact : "+exact);
	}
	
	public void testGistMap() {
		ISLMap map = ISLFactory.islMap("{[i] -> [i'] : i' = i }");
		ISLMap res = map.copy().gist(map.copy());
		System.out.println(map);
		System.out.println(res);
	}
	
	
}
