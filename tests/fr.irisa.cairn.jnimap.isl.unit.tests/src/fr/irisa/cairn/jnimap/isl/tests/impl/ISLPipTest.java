package fr.irisa.cairn.jnimap.isl.tests.impl;

import static fr.irisa.cairn.jnimap.isl.ISLFactory.islSet;

import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import junit.framework.TestCase;

public class ISLPipTest extends TestCase {

	public static void testMain() {
		ISLSet set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0 | j>= i  & j> 15  & i>= 0 }");
		System.out.println(set_b);
		System.out.println(set_b.getNbBasicSets());
		ISLBasicSet bset = set_b.getBasicSetAt(0);
		System.out.println(bset);
		for(int i=0; i<set_b.getNbBasicSets();i++) {
			ISLBasicSet ns = set_b.getBasicSetAt(i);
			System.out.println(ns);
			//vertices.
		}
		ISLSet lexMin = set_b.lexMin();
		System.out.println(lexMin);
	}
}
