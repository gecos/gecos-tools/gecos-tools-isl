package fr.irisa.cairn.jnimap.isl.tests.impl;

import fr.irisa.cairn.jnimap.isl.ISLASTExprType;
import fr.irisa.cairn.jnimap.isl.ISLASTExpression;
import fr.irisa.cairn.jnimap.isl.ISLASTExpressionList;
import fr.irisa.cairn.jnimap.isl.ISLASTIdentifier;
import fr.irisa.cairn.jnimap.isl.ISLASTLiteral;
import fr.irisa.cairn.jnimap.isl.ISLASTOpType;
import fr.irisa.cairn.jnimap.isl.ISLASTOperation;
import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLIdentifier;
import fr.irisa.cairn.jnimap.isl.ISLVal;
import junit.framework.TestCase;

public class ISLASTExpressionTest extends TestCase{

	
	public static void testBuildASTLiteral() {
		int val = 23;
		ISLASTLiteral expr = ISLASTExpression.buildASTLiteral(ISLVal.buildFromLong(ISLContext.getInstance(), val));
		assert(expr.getType()==ISLASTExprType.isl_ast_expr_int);
		assert(expr.getValue()==val);
	}
	
	public static void testBuildASTIdentifier() {
		String name = "id";
		ISLASTIdentifier expr = ISLASTExpression.buildASTIdentifier(ISLIdentifier.alloc(ISLContext.getInstance(), name));
		assert(expr.getType()==ISLASTExprType.isl_ast_expr_id);
		assert(expr.getID().getName().contentEquals(name));
	}
	
	public static void testBuildASTOpAccess() {
		ISLASTIdentifier array = ISLASTExpression.buildASTIdentifier(ISLIdentifier.alloc(ISLContext.getInstance(), "A"));
		ISLASTExpressionList list = ISLASTExpressionList.build(ISLContext.getInstance(), 2);
		list = list.add(ISLASTExpression.buildASTLiteral(ISLVal.buildFromLong(ISLContext.getInstance(), 0)));
		list = list.add(ISLASTExpression.buildASTIdentifier(ISLIdentifier.alloc(ISLContext.getInstance(), "id")));
		ISLASTOperation op = ISLASTExpression.buildASTOpAccess(array, list);
		assert(op.getType()==ISLASTExprType.isl_ast_expr_op);
		assert(op.getOpType()==ISLASTOpType.isl_ast_op_access);
	}
	

	private static ISLASTExpression[] createExprs() {
		return new ISLASTExpression[] {
			ISLASTExpression.buildASTLiteral(ISLVal.buildFromLong(ISLContext.getInstance(), 0)),
			ISLASTExpression.buildASTIdentifier(ISLIdentifier.alloc(ISLContext.getInstance(), "id"))
		};
	}
	
	public static void testBuildASTOpAdd() {
		ISLASTExpression[] exprs = createExprs();
		ISLASTOperation op = ISLASTExpression.buildASTOpAdd(exprs[0], exprs[1]);
		assert(op.getType()==ISLASTExprType.isl_ast_expr_op);
		assert(op.getOpType()==ISLASTOpType.isl_ast_op_add);
	}
	
	public static void testBuildASTOpSub() {
		ISLASTExpression[] exprs = createExprs();
		ISLASTOperation op = ISLASTExpression.buildASTOpSub(exprs[0], exprs[1]);
		assert(op.getType()==ISLASTExprType.isl_ast_expr_op);
		assert(op.getOpType()==ISLASTOpType.isl_ast_op_sub);
	}
	
	public static void testBuildASTOpMul() {
		ISLASTExpression[] exprs = createExprs();
		ISLASTOperation op = ISLASTExpression.buildASTOpMul(exprs[0], exprs[1]);
		assert(op.getType()==ISLASTExprType.isl_ast_expr_op);
		assert(op.getOpType()==ISLASTOpType.isl_ast_op_mul);
	}
	
	public static void testBuildASTOpAnd() {
		ISLASTExpression[] exprs = createExprs();
		ISLASTOperation op = ISLASTExpression.buildASTOpAnd(exprs[0], exprs[1]);
		assert(op.getType()==ISLASTExprType.isl_ast_expr_op);
		assert(op.getOpType()==ISLASTOpType.isl_ast_op_and);
	}
}
