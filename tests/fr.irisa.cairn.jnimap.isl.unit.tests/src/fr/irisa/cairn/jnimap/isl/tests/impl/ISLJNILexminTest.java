package fr.irisa.cairn.jnimap.isl.tests.impl;

import java.util.Map;
import java.util.Map.Entry;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLAff;
import fr.irisa.cairn.jnimap.isl.ISLBasicMap;
import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLMultiAff;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISL_FORMAT;
import fr.irisa.cairn.jnimap.isl.JNIISLTools;

public class ISLJNILexminTest extends TestCase {

	
	private void computeTest(String mapStr) {
		ISLMap map = ISLFactory.islMap(mapStr);
		computeTest(map);
		map.free();
	}

	private void computeTest(ISLMap map) {
		assertNotNull(map);

		int nbDim = (int) map.dim(ISLDimType.isl_dim_out);

		ISLMap lexmin = map.copy().lexMin();
		System.out.println(lexmin);
		for (ISLBasicMap bmap : lexmin.getBasicMaps()) {
			Map<ISLSet, ISLMultiAff> cfr = bmap.getClosedFormRelation();
			int max = cfr.size();
			for (Entry<ISLSet, ISLMultiAff> entry : cfr.entrySet()) {
				System.out.println(entry.getKey());
				System.out.println(entry.getValue());
			}
			Map<ISLSet, ISLMultiAff> m = bmap.getClosedFormRelation();
			for (ISLSet set : m.keySet()) {
				assertFalse(set.isEmpty());
			}
			boolean allDisjoint = JNIISLTools.allDisjoint(m.keySet());
			assertTrue(allDisjoint);

			for (ISLMultiAff l : m.values()) {
				System.out.println(l);
				int n = l.getNbOutputs();
				assertEquals(nbDim, n);
				int n2 = l.getAffs().size();
				assertEquals(nbDim, n2);

				int i = 0;
				for (ISLAff aff : l.getAffs()) {
					System.out.print("\t"+map.getSpace().getDimName(ISLDimType.isl_dim_out, i++));
					String nativePrint = aff.toString().substring(aff.toString().lastIndexOf("->")+3, aff.toString().lastIndexOf("}")-1);
					System.out.println("\t Native print => \t"+nativePrint);

					String javaPrint = aff.javaPrint();
					System.out.println("\t\t Java inspection => \t"+javaPrint+"");
					
					//Disabling the assertion, which no longer works due to ISL detecting some of the modulo operations
					//https://repo.or.cz/isl.git/commit/5d73b0f5725efd761d39a78b1464d41dd05535f8
					if (!nativePrint.contains("mod"))
						assertEquals(nativePrint, javaPrint);
				}
			}
		}
	}
	
	public void testPred() {
		String bench = "[N] -> { [i] -> [i'] : i <= -1 + N and i >= 12 && i' = -12 + i}";
		computeTest(bench);
		
	}
	public void testInspection() {
		String bench = "{ [i] -> [i', j] : j = i - 8i' and i' >= 0 and i' <= 7 and 8i' <= i and 8i' >= -7 + i } ";
		computeTest(bench);
	}
	
	public void testInspection2() {
		String bench = "[M] -> { [i] -> [i', j] : 0 <= i < M and 0 <= j < 8 and i = 8i' + j } ";
		computeTest(bench);
	}
	
	public void testInspection3() {
		String bench = "[M] -> { [i] -> [i', j, k] :  0 <= i < M and 0 <= j < 8 and 0 <= k < 8 and i = 64i' + 8j + k } ";
		computeTest(bench);
	}
	
	public void testSet() {
		ISLBasicSet bset = ISLFactory.islBasicSet("[M] -> { [i] : i >= 0 and i <= M }");
		System.out.println(bset.copy());

		ISLSet set = bset.toSet();
		
		ISLMap map = ISLMap.buildFromRange(set);
		
		computeTest(map);
		
	}
	
	
	public void testUnionMap() {
		System.out.println("****************** UnionMap Lexmin Test ********************");
		/*
		 * this test shows that the lexopt functions in ISL give a result 
		 * per tuple instead of trying to merge the result of each tuple.
		 */
		ISLUnionMap um = ISLFactory.islUnionMap("[] -> { " +
				"R[i] -> S0[i'] : 0 <= i < 8 and 0 <= i' < 8 and i' <= i - 2 ;" +
				"R[i] -> S1[i'] : 0 <= i < 8 and 0 <= i' < 8 and i' <= i - 1" +
				"}");
		System.out.println(um);
		ISLUnionMap lexmin = um.copy().lexMin();
		System.out.println(lexmin);
				
	}
	
	public void testNbDimsProj() {
		System.out.println("************************");

		ISLSet set = ISLFactory.islSet("[N] -> { [j, i, k] : k >= j and k <= -1 + N and i >= 0 and i <= -2 + N - j and j >= 0 and j <= -2 + N }");
		System.out.println(set);
		System.out.println(set.lexFirstMap(3));
		
		System.out.println("************************");
		
		set = ISLFactory.islSet("[N] -> { [j, i, k] : k >= j and k <= -1 + N and i >= 0 and i <= -2 + N - j and j >= 0 and j <= -2 + N }");
		System.out.println(set);
		System.out.println(set.lexFirstMap(1));
		
		System.out.println("************************");
		
		set = ISLFactory.islSet("[N] -> { [i, j] : 0 <= i < N && i <= j < N }").coalesce();
		System.out.println(set);
		System.out.println(set.lexFirstMap(1));
	}
	
	
	public static void testMoveDims() {
		ISLSet A = ISLFactory.islSet("{[i,j,k] : 0<i+j<256 && 0<j-k<128 && k=j-2i }");
		ISLMap A_prime = ISLMap.buildFromDomain(A);
		ISLMap A_sec = A_prime.moveDims(ISLDimType.isl_dim_out, 0, ISLDimType.isl_dim_in, 1, 2);
		ISLMap lexmin = A_sec.lexMin();
		System.out.println(lexmin);
	}
}
