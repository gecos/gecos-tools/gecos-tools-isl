package fr.irisa.cairn.jnimap.isl.tests.impl;

import static fr.irisa.cairn.jnimap.isl.ISLFactory.islMap;
import static fr.irisa.cairn.jnimap.isl.ISLFactory.islSet;

import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLConstraint;
import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import junit.framework.TestCase;

public class CountingTest extends TestCase {

	public static void testMain() {
		mainBody();
	}

	private static void mainBody() {
		System.out.println("## Testing ISL");

		ISLSet set_a = islSet("[M,I,J]->{[i,j] : 0<=i<M & 0<=j<M & i<=j & i=I & j=J}");

		ISLMap map_next_i = islMap("[M]->{[i',j']->[i,j] : i'>i & j'=j & 0<=i<M & 0<=j<M & i<=j}");
		ISLMap map_next_j = islMap("[M]->{[i,j]->[i',j'] : j'>j & 0<=i<M & 0<=j<M & i<=j & 0<=i'<M & 0<=j'<M & i'<=j'}");

		ISLMap map_b = map_next_i.lexMin();
		System.out.println(map_b);
		System.out.println(map_b.getRange());
		System.out.println(map_b.getDomain());

		System.out.println("Map :" + map_next_j);
		ISLMap map_c = map_next_j.lexMin();
		System.out.println("LexMin :" + map_c);

		System.out.println("Range :" + map_c.getRange());
		System.out.println("Domain :" + map_c.getDomain());

		ISLMap map = islMap("[M,I,J]->{[i,j]->[i',j'] : "
				+ " i' = 0 & j' = 1 + j & j >= 0 & j < M-1 & j = i  |"
				+ " i' = i+1 & j' =  j  & j >= 0 & j < M-1 & j >= i & i < j"
				+ "}");
		// System.out.println("is bijective ? "+map.copy().isBijective());

		ISLSet apply = set_a.copy().apply(map.copy()).apply(map.copy());
		System.out.println("ApplyRange:" + apply.copy().detectEqualities());

		ISLSet existential = islSet("[n] -> { [i] : exists (a = [i/10] : 0 <= i and i <= n and i - 10 a <= 6) and exists (b = [2i/7] : 0 <= i and i <= n and i - 3 b <= 6) }");
		existential = existential.computeDivs();
		for (int i = 0; i < existential.getNbBasicSets(); i++) {
			ISLBasicSet basicSetAt = existential.getBasicSetAt(i);

			System.out.println(basicSetAt);
			for (int j = 0; j < basicSetAt.getNbConstraints(); j++) {
				ISLConstraint constraintAt = basicSetAt.getConstraintAt(j);

				System.out.println(constraintAt);
			}
		}

		 System.out.println("ApplyRange:"+map_c.copy().applyRange(map_b.copy()));
		 System.out.println("ApplyDomain:"+map_b.copy().applyDomain(map_c.copy()));
		 System.out.println("ApplyDomain:"+map_c.copy().applyDomain(map_b.copy()));
	}
}
