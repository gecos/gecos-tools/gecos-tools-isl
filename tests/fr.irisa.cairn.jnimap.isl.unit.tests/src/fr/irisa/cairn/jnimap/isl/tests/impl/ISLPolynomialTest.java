package fr.irisa.cairn.jnimap.isl.tests.impl;

import static fr.irisa.cairn.jnimap.isl.ISLFactory.islPWQPolynomial;

import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLPWQPolynomial;
import fr.irisa.cairn.jnimap.isl.ISLQPolynomial;
import fr.irisa.cairn.jnimap.isl.ISLSpace;
import fr.irisa.cairn.jnimap.isl.ISLVal;
import junit.framework.TestCase;

public class ISLPolynomialTest extends TestCase{


	public static void testQPolynomialsConstruction() {
		qpolynomialsConstructionBody();
	}
	private static void qpolynomialsConstructionBody() {
	ISLSpace allocSet = ISLSpace.allocSetSpace(1, 1);
		
		ISLQPolynomial p = ISLQPolynomial.buildZero(allocSet.copy());
		System.out.println(p);
		System.out.println("constant : "+p.isConstant());
		assertTrue(p.isConstant());

		
		p = ISLQPolynomial.buildRationalValue(allocSet.copy(), 5, 6);
		System.out.println(p);
		System.out.println("constant : "+p.isConstant());
		assertTrue(p.isConstant());
		
		p = ISLQPolynomial.buildOne(allocSet.copy());
		System.out.println(p);
		System.out.println("constant : "+p.isConstant());
		assertTrue(p.isConstant());
		
		p = ISLQPolynomial.buildNaN(allocSet.copy());
		System.out.println(p);
		System.out.println("constant : "+p.isConstant());
		assertTrue(p.isConstant());
		
		p = ISLQPolynomial.buildInfinity(allocSet.copy());
		System.out.println(p);
		System.out.println("constant : "+p.isConstant());
		assertTrue(p.isConstant());
		
		p = ISLQPolynomial.buildNegInfinity(allocSet.copy());
		System.out.println(p);
		System.out.println("constant : "+p.isConstant());
		assertTrue(p.isConstant());
		
		p = ISLQPolynomial.buildVarOnDomain(allocSet.copy(), ISLDimType.isl_dim_param, 0);
		System.out.println(p);
		System.out.println("constant : "+p.isConstant());
		assertFalse(p.isConstant());
		
		p = ISLQPolynomial.buildVarOnDomain(allocSet.copy(), ISLDimType.isl_dim_set, 0);
		System.out.println(p);
		System.out.println("constant : "+p.isConstant());
		assertFalse(p.isConstant());
	}

	public static void testQPolynomials() {
		qpolynomilsBody();
	}
	private static void qpolynomilsBody() {
		ISLSpace allocSet = ISLSpace.allocSetSpace(1, 1);
		
		ISLQPolynomial p = ISLQPolynomial.buildRationalValue(allocSet.copy(), 5, 6);
		p = p.scale(6);
		System.out.println(p);
		
		p = ISLQPolynomial.buildRationalValue(allocSet.copy(), 5, 6);
		System.out.println(p);
		ISLQPolynomial p2 = ISLQPolynomial.buildRationalValue(allocSet.copy(), 6, 1);
		p = p.mul(p2);
		System.out.println(p);
		
	}
	
	public static void testPWQPolynomial (){
		pwqpolynomialBody();
	}
	private static void pwqpolynomialBody() {

//		fail("Segfault"); // XXX segfault
		System.out.println("********* Polynomials *********");
		ISLPWQPolynomial P1 = islPWQPolynomial("[n] -> { [i, j] -> (n^2 + j^2*i^3) : j >= 0 and j <= i and i >= 1 and i <= -2 + n and n>=1 and n <= 16 }");
		System.out.println("P1 : "+P1);

		ISLPWQPolynomial P2 = islPWQPolynomial("[n] -> { [i, j] -> (i^4 + n^2 + j*i) : j >= i+1 and j <= -1 + n and i >= 1 and i <= -2 + n and n>=1 and  n <= 16 }");
		System.out.println("P2 : "+P2);

		System.out.println("********* Sum of Polynomials *********");
		ISLPWQPolynomial P3 = P1.add(P2);
		
		System.out.println("P3=P1+P2 : "+P3);
		
		System.out.println("********* Minima (iterative barvinok/bound.c)*********");
		
		ISLSpace dim = P3.copy().getSpace();
		System.out.println("dims : "+dim);
		
		long nvar = dim.dim(ISLDimType.isl_dim_set);
		System.out.println("nvar : "+nvar);
		ISLVal qp =  P3.copy().min();
		System.out.println("qp: "+qp);
		
		//FIXME rest of the code is no longer valid since QPoly.min now returns isl_val
//		qp = qp.dropDim(JNIISLDimType.isl_dim_in, 0, nvar);
//		System.out.println("qp: "+qp);
//		
//		JNIISLSet set = JNIISLSet.buildUniverseLike(P3.copy().getDomain());
//		JNIISLQPolynomialFold fold = new JNIISLQPolynomialFold(JNIISLFold.isl_fold_min, qp);
//		System.out.println(fold);
//
//		JNIISLPWQPolynomialFold pwFold = new JNIISLPWQPolynomialFold(JNIISLFold.isl_fold_min, set, fold);
//		System.out.println(pwFold);
//
//		pwFold = P3.bound(JNIISLFold.isl_fold_min);
//		System.out.println(pwFold);
	}

	public void testMin() {
//		fail("Segfault"); // XXX segfault
		System.out.println("------------------------------------");
		ISLPWQPolynomial islPWQPolynomial = islPWQPolynomial("[n] -> { (n^2 ) : n >= 1 and n <= 16 }");
		
		//Min and Max are not available in current ver
//		JNIISLPWQPolynomialFold bound = islPWQPolynomial.bound(JNIISLFold.isl_fold_max);
//		JNIISLVal max = bound.max();
//		System.out.println("*");
//		System.out.println(max.toString());
//		islPWQPolynomial = islPWQPolynomial("[n] -> { (n^2 ) : n >= 1 and n <= 16 }");
//		bound = islPWQPolynomial.bound(JNIISLFold.isl_fold_max);
//		JNIISLVal min = bound.min();
//		System.out.println("*");
//		System.out.println(min);
//		System.out.println(islPWQPolynomial("[N] -> { (-9 + 3 * N) : 5N >= 4 && 2N < 1024}").bound(JNIISLFold.isl_fold_min).getMax());
//		System.out.println(islPWQPolynomial("[N] -> { (-9 + 3 * N) : N >= 4 && N < 1024 }").bound(JNIISLFold.isl_fold_min).getMin());
	}
}









