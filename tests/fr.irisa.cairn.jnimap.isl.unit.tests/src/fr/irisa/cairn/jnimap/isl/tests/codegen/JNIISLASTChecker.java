package fr.irisa.cairn.jnimap.isl.tests.codegen;



import fr.irisa.cairn.jnimap.isl.IISLASTNodeVisitor;
import fr.irisa.cairn.jnimap.isl.ISLASTBlockNode;
import fr.irisa.cairn.jnimap.isl.ISLASTExpression;
import fr.irisa.cairn.jnimap.isl.ISLASTForNode;
import fr.irisa.cairn.jnimap.isl.ISLASTIdentifier;
import fr.irisa.cairn.jnimap.isl.ISLASTIfNode;
import fr.irisa.cairn.jnimap.isl.ISLASTMarkNode;
import fr.irisa.cairn.jnimap.isl.ISLASTNode;
import fr.irisa.cairn.jnimap.isl.ISLASTNodeList;
import fr.irisa.cairn.jnimap.isl.ISLASTOperation;
import fr.irisa.cairn.jnimap.isl.ISLASTUnscannedNode;
import fr.irisa.cairn.jnimap.isl.ISLASTUserNode;
import fr.irisa.cairn.jnimap.isl.tests.codegen.JNIISLExpressionChecker;


public class JNIISLASTChecker implements IISLASTNodeVisitor{
	
	
	protected JNIISLExpressionChecker exprConverter = new JNIISLExpressionChecker();
	
	public JNIISLASTChecker() {
	}
	
	public static void adapt(ISLASTNode r) {
		JNIISLASTChecker adapter = new JNIISLASTChecker();
		r.accept(adapter);
	}

	@Override
	public void visitISLASTNode(ISLASTNode obj) {
		throw new UnsupportedOperationException("visitISLASTNode not yet Implemented for "+obj);
	}

	@Override
	public void visitISLASTBlockNode(ISLASTBlockNode obj) {
		ISLASTNodeList c = obj.getChildren();
		for(int i=0;i<c.getNbNodes();i++) {
			ISLASTNode child = c.get(i);
			child.accept(this);
		}
	}

	@Override
	public void visitISLASTForNode(ISLASTForNode obj) {
		ISLASTNode body = obj.getBody();
		body.accept(this);
		ISLASTExpression inc = obj.getInc();
		exprConverter.buildExpr(inc);
		exprConverter.buildConstraint(obj.getCond());
		exprConverter.buildExpr(obj.getInit());
	}

	@Override
	public void visitISLASTIfNode(ISLASTIfNode obj) {
		ISLASTOperation cond = (ISLASTOperation) obj.getCond();
		ISLASTNode then = obj.getThen();
		then.accept(this);
		if(obj.hasElse()!=0) {
			ISLASTNode else1 = obj.getElse();
			else1.accept(this);
		}
	}

	@Override
	public void visitISLASTUserNode(ISLASTUserNode obj) {
		ISLASTOperation r = (ISLASTOperation) obj.getExpression();
		for(int i=1;i<r.getNbArgs();i++) {
			exprConverter.buildExpr(r.getArgument(i));
		}
	}

	@Override
	public void visitISLASTMarkNode(ISLASTMarkNode obj) {
		throw new UnsupportedOperationException("NYI");
	}

	@Override
	public void visitISLASTUnscannedNode(ISLASTUnscannedNode obj) {
		throw new UnsupportedOperationException("NYI");
	}
}
