package fr.irisa.cairn.jnimap.isl.tests.impl;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLConstraint;
import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLSpace;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;

public class TestLowLevelCreation  extends TestCase {
	

	
	public static void testSpace() {
		ISLSpace space = ISLSpace.allocSetSpace(1, 2);
		space = space.setDimName(ISLDimType.isl_dim_param, 0, "N");
		space = space.setDimName(ISLDimType.isl_dim_set, 0, "i");
		space = space.setDimName(ISLDimType.isl_dim_set, 1, "j");
		space = space.setTupleName(ISLDimType.isl_dim_set, "S");
		System.out.println(space);

		ISLSpace relationSpace = ISLSpace.alloc(2, 2, 1);
		relationSpace = relationSpace.setDimName(ISLDimType.isl_dim_param, 0, "M");
		relationSpace = relationSpace.setDimName(ISLDimType.isl_dim_param, 1, "N");
		relationSpace = relationSpace.setDimName(ISLDimType.isl_dim_in, 0, "i");
		relationSpace = relationSpace.setDimName(ISLDimType.isl_dim_in, 1, "j");
		relationSpace = relationSpace.setDimName(ISLDimType.isl_dim_out, 0, "k");
		relationSpace = relationSpace.setTupleName(ISLDimType.isl_dim_in, "S");
		relationSpace = relationSpace.setTupleName(ISLDimType.isl_dim_out, "T");
		System.out.println(relationSpace);
		
		//using previously built spaces
		ISLConstraint setConstraint1 = ISLConstraint.buildInequality(space.copy());
		setConstraint1 = setConstraint1.setCoefficient(ISLDimType.isl_dim_set, 0, 1);
		setConstraint1 = setConstraint1.setConstant(-1);
		System.out.println(setConstraint1); // [N] -> { S[i, j] : i >= 1 }
		ISLConstraint setConstraint2 = ISLConstraint.buildInequality(space.copy());
		setConstraint2 = setConstraint2.setCoefficient(ISLDimType.isl_dim_set, 1, 1);
		System.out.println(setConstraint2); // [N] -> { S[i, j] : j >= 0 }
		ISLConstraint setConstraint3 = ISLConstraint.buildInequality(space.copy());
		setConstraint3 = setConstraint3.setCoefficient(ISLDimType.isl_dim_set, 1, -1);
		setConstraint3= setConstraint3.setCoefficient(ISLDimType.isl_dim_set, 0, -1);
		setConstraint3 = setConstraint3.setCoefficient(ISLDimType.isl_dim_param, 0, 1);
		System.out.println(setConstraint3); // [N] -> { S[i, j] : j <= N - i }
		
		ISLConstraint relationConstraint1 = ISLConstraint.buildEquality(relationSpace.copy());
		relationConstraint1 = relationConstraint1.setCoefficient(ISLDimType.isl_dim_in, 0, 1);
		relationConstraint1 = relationConstraint1.setCoefficient(ISLDimType.isl_dim_in, 1, 1);
		relationConstraint1 = relationConstraint1.setCoefficient(ISLDimType.isl_dim_out, 0, -1);
		System.out.println(relationConstraint1); // [M, N] -> { S[i, j] -> T[k] : k = i + j }
		
		ISLBasicSet basicSet = ISLBasicSet.buildUniverse(space.copy());
		basicSet = basicSet.addConstraint(setConstraint1);
		basicSet = basicSet.addConstraint(setConstraint2);
		basicSet = basicSet.addConstraint(setConstraint3);
		System.out.println(basicSet); // [N] -> { S[i, j] : i >= 1 and j >= 0 and j <= N - i }
		
		// use of String factory to ease reading.
		ISLBasicSet basicSet2 = 
				ISLFactory.islBasicSet("[N] -> { S[i, j] : i <= N and j <= N and i+j > N+2 }");
		ISLSet set = basicSet.copy().toSet();
		ISLSet set2 = basicSet2.copy().toSet();
		set = set.union(set2).coalesce();
		System.out.println(set); 
		// [N] -> { S[i, j] : (i >= 1 and j >= 0 and j <= N - i) or (i <= N and j <= N and j >= 3 + N - i) }
		
		ISLUnionSet unionSet = set.copy().toUnionSet();
		unionSet = unionSet.union(ISLFactory.islUnionSet("[N] -> { [a] : 0 < a < N }"));
		System.out.println(unionSet); 
		// [N] -> {
		//   S[i, j] : (i >= 1 and j >= 0 and j <= N - i) or (i <= N and j <= N and j >= 3 + N - i); 
		//   [a] : a >= 1 and a <= -1 + N 
		// }
	}
	
	
	
	
}