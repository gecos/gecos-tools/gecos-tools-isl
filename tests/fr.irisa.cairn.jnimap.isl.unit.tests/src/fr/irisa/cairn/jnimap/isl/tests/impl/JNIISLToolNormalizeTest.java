package fr.irisa.cairn.jnimap.isl.tests.impl;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.JNIISLTools;

public class JNIISLToolNormalizeTest extends TestCase {
	
	public void test1() {
		ISLSet set1 = ISLFactory.islSet("[M,N] -> { [i,j] : 0 <= i < M && 0 <= j < N }");
		ISLSet set2 = ISLFactory.islSet("[M,N] -> { [i,j,k] : 0 <= i < M && 0 <= j < N && k = 10}");
		
		System.out.println(set1);
		System.out.println(set2);
		
		ISLSet setRes = JNIISLTools.normalize(set2, set1);
		
		System.out.println(setRes);
		
		assertNotNull(setRes);
	}
}
