package fr.irisa.cairn.jnimap.isl.tests.codegen;

import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.codegen.FSMCodeGeneration;
import fr.irisa.cairn.jnimap.isl.jni.tests.AbstractTestFromISL;

public class TestFSMCodeGeneration extends AbstractTestFromISL {

	@Override
	public void computeTest(String path, ISLUnionSet domains, ISLUnionMap idSchedules,
			ISLUnionMap writes, ISLUnionMap reads,
			ISLUnionMap valueBasedPrdg,
			ISLUnionMap valueBasedPlutoSchedule,
			ISLUnionMap valueBasedFeautrierSchedule,
			ISLUnionMap memoryBasedPrdg,
			ISLUnionMap memoryBasedPlutoSchedule,
			ISLUnionMap memoryBasedFeautrierSchedule) {
	
        ISLUnionMap scheduleWithOutputName = ISLMap.buildEmpty(valueBasedPlutoSchedule.getSpace().copy()).toUnionMap();
        for ( ISLMap m : valueBasedPlutoSchedule.copy().getMaps() ) { 
            if ( m.getOutputTupleName() == null ) { 
                m = m.setOutputTupleName(m.getInputTupleName());
            }
            scheduleWithOutputName = scheduleWithOutputName.addMap(m.copy());
        }   
        ISLUnionSet mergedUnionSet = domains.copy().apply(scheduleWithOutputName.copy());
        FSMCodeGeneration.liveGuardOptimizations(mergedUnionSet);
    }   
        
    @Override
    public void test_polymodel_src_polybenchs_perso_bor_fdtd_GScopRoot_func_scop_0_() {}
        
    /*@Override
    public void test_polybench_gecos_linear_algebra_kernels_2mm_2mm_GScopRoot_scop_new_() {}
        
    @Override
    public void test_polybench_gecos_datamining_correlation_correlation_GScopRoot_scop_new_ () {} */
        
    @Override
    public void test_polybench_gecos_linear_algebra_kernels_gemver_gemver_GScopRoot_scop_new_ () {}
        
    @Override
    public void test_polybench_gecos_linear_algebra_solvers_dynprog_dynprog_GScopRoot_scop_new_ () {}
        
    @Override
    public void test_polymodel_src_polybenchs_perso_STAP_Mat_Invert_GScopRoot_STAP_Mat_Invert_scop_0_() {}
    
    @Override
    public void test_polymodel_src_corner_cases_InputDomainTest_GScopRoot_intputDomainTest_scop_0_() {}
    
    @Override
    public void test_polymodel_src_corner_cases_filtre_GScopRoot_filtre_scop_0_() {}
    
    @Override
    public void test_polybench_gecos_datamining_covariance_covariance_GScopRoot_scop_new_() {}
    
    @Override
    public void test_polybench_gecos_linear_algebra_kernels_3mm_3mm_GScopRoot_scop_new_() {}

}
