package fr.irisa.cairn.jnimap.isl.tests.sandbox

import fr.irisa.cairn.jnimap.isl.ISLBasicMap
import fr.irisa.cairn.jnimap.isl.ISLBasicSet
import fr.irisa.cairn.jnimap.isl.ISLMap
import fr.irisa.cairn.jnimap.isl.ISLSet
import fr.irisa.cairn.jnimap.isl.codegen.FSMCodeGeneration.JNIFSM
import java.io.PrintStream
import java.util.HashSet

class FSMVHDLPrettyPrinter {
	//toto
	HashSet<String> stateList;	
	HashSet<String> commandList;	
	JNIFSM fsm
	String name
	new(String name, JNIFSM fsm) {
		this.fsm=fsm
		this.name=name
		stateList = new HashSet<String>(fsm.commands.maps.map([e|e.getInputTupleName]));
		commandList = new HashSet<String>(fsm.commands.maps.map([e|e.getOutputTupleName]));
	} 
	
	def generate() {
		val PrintStream ps =new PrintStream(name+".vhd");
		ps.append(
		'''
		library ieee;
		library work;
		
		use ieee.std_logic_1164.all;
		use ieee.std_logic_arith.all;
		use ieee.std_logic_unsigned.all;
		
		entity «name» is port (
			clk : int std_logic;
			rst : in std_logic;
			ce : in std_logic;
			------------------
			«FOR c : commandList SEPARATOR ";"»
			«c» : out std_logic
			«ENDFOR»
		
		);
		end entity;
		
		architecture RTL of «name» is
		
			type T_«name.toFirstUpper()» is (
				idle,
				«FOR String state : stateList SEPARATOR ","»
				«state»
				«ENDFOR»
			);
		
			signal ns,cs : T_«name.toFirstUpper»;
		
			constant ONE : std_logic:='1';
			constant ZERO : std_logic:='0';
			
		begin
		
			SYNC: process(clk,rst) 
			begin
				if (rst='1') then
					cs <= INIT;
				elsif rising_edge(clk) then
				case cs is
					«FOR state : fsm.states.sets »
						when «state.getTupleName» =>
							«FOR ISLMap m : fsm.transitions.maps.filter([t | t.getInputTupleName==state.getTupleName])»
								«generateTransition(m,state)»
							«ENDFOR»
							«FOR ISLMap m : fsm.commands.maps.filter([t | t.getInputTupleName==state.getTupleName])»
								«m.getOutputTupleName» <= '1'
							«ENDFOR»
					«ENDFOR»
					when others =>
						null;
				end case;
				end if; 
			end process;

		end RTL;
				
		
		'''
		);
		ps.close();
	}
	
	def generateTransition(ISLMap t,ISLSet m) {
		if(t.nbBasicMaps==1) {
			val ISLBasicMap bmp = t.basicMaps.get(0);
			val ISLBasicSet dom = bmp.copy.getDomain;
			val ISLBasicSet gisted_dom = dom.gist(m.copy.convexHull);
			
			/* here we must :
			 * - gist the dom basicSet w.r.t to the state domain to remove all redundant guards
			 * - update the loop indices with some closed form expression
			 */			
			 if(gisted_dom .constraints.size>0) {
				return '''	
				if («
					FOR c : gisted_dom .constraints SEPARATOR 
					" and "»(«c.toString»)«
					ENDFOR
					») then
					-- here we should update indices
				 	cs <= «t.getOutputTupleName»;
				end if;
				
				«bmp.closedFormRelation»
			''' 	
			 } else {
				return 
				'''
			 	-- Single transition
			 	cs <= «t.getOutputTupleName»;
				'''
			 }
			
		} if(t.nbBasicMaps==0) {
			return '''
		 	-- Warning no basicmap
		 	cs <= «t.getOutputTupleName»;
			'''
		} else {
			throw new UnsupportedOperationException("Unsuported");
		}
			
		
	}

}