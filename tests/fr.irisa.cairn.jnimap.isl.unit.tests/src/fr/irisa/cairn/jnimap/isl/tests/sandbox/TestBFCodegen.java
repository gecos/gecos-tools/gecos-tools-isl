package fr.irisa.cairn.jnimap.isl.tests.sandbox;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.codegen.FSMCodeGeneration;
import fr.irisa.cairn.jnimap.isl.codegen.FSMCodeGeneration.JNIFSM;
import fr.irisa.cairn.jnimap.isl.tests.sandbox.FSMVHDLPrettyPrinter;

public class TestBFCodegen extends TestCase {
	
	public void testBFCodeGen() {
			String unionStr = "[] -> { "+
						"S0[i,j]: i=0    && 3<j<=8;"+
						"S1[i,j]: j+i>4  && j<=8 && i>0 &&"+
						"         j>=0   && j+i <=10;"+
						"S2[i,j]: j+i=4  && j>=0 && i>=1;"+
						"S3[i,j]: j+i=10 && i<=10 && i>=3"+
			"}";
			
			JNIFSM r = FSMCodeGeneration.liveGuardOptimizations(ISLFactory.islUnionSet(unionStr));
			FSMVHDLPrettyPrinter cg = new FSMVHDLPrettyPrinter("test1", r);
			cg.generate();
	}

}
