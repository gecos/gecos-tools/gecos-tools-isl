package fr.irisa.cairn.jnimap.isl.tests.impl;

import static fr.irisa.cairn.jnimap.isl.ISLFactory.islMap;
import static fr.irisa.cairn.jnimap.isl.ISLFactory.islSet;

import fr.irisa.cairn.jnimap.isl.ISLBasicMap;
import fr.irisa.cairn.jnimap.isl.ISLBasicSet;
import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import junit.framework.TestCase;
public class IslJNITest extends TestCase {
	


	public static void testProjectionExamples() {
		projectionExamplesBody();
	}
	private static void projectionExamplesBody() {
		System.out.println("#############################################");
		System.out.println("   Projection examples ");
		System.out.println("#############################################");
		// projecting out the two indices
		ISLSet set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println("\nProjecting out i,j");
		System.out.println(" => "+set_b.projectOut(ISLDimType.isl_dim_set, 0, 2));
		
		
		// projecting out the index j
		set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println("\nProjecting out j");
		System.out.println(" => "+set_b.projectOut(ISLDimType.isl_dim_set, 0, 1));
		
		// projecting out the index i
		set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println("\nProjecting out i");
		System.out.println(set_b+" => "+set_b.projectOut(ISLDimType.isl_dim_set, 1, 1));

		// projecting out parameters N,M
		set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println(set_b.projectOut(ISLDimType.isl_dim_param, 0, 2));

		// projecting out parameters M
		set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println(set_b.projectOut(ISLDimType.isl_dim_param, 1, 1));
	}
	
	public static void testMain() {

//		FIXME test disabled due to the use of extend
		
//		JNIISLSet param = islSet("[P,Q]->{[] : 0<P and 0<Q}");
//		JNIISLSet set = islSet("[P,Q]->{[i,j] : 0<=i<P and 0<=j<Q}");
//		JNIISLSet param_cp = param.copy().extend(2, 2);
//		System.out.println(param);
//		System.out.println(param_cp);
//		System.out.println(set);
//		JNIISLSet intersect = JNIISLSet.intersect(param_cp.copy(), set.copy());
//		System.out.println(intersect);
//		System.out.println(intersect.gist(param_cp));
//		System.out.println(set.polyhedralHull());
//		
//		JNIISLMap map1 = islMap("[]->{[i,j] -> [i]}");
//		JNIISLMap map2 = islMap("[]->{[i] -> []}");
//		System.out.println(JNIISLMap.applyRange(map1, map2));
//		
//		
//		JNIISLSet set_a = islSet("[]->{[i] : i > 10 && i< 152}");
//		System.out.println(set_a);
//		
//		JNIISLMap map_a = islMap("{[i,j] -> [k] : k=8*i+j & 0<= k < 64 & 0 <= i < 8 & 0 <= j < 8}");
//		System.out.println(map_a);
//		JNIISLMap invmap_a  = map_a.copy().reverse();
//		
//		
//		JNIISLSet set_b = islSet("[K] -> {[i,j] :  K=8*i+j & 0<= K < 64 & 0 <= i < 8 & 0 <= j < 8}");
//		System.out.println(set_b);
//		JNIISLSet invmap_b  = set_b.copy().lexMin();
//		
//		System.out.println("lexmin :"+invmap_b);
//
//		map_a = islMap("{[i] -> [i,i] : 0 <= i < 8 }");
//
//		JNIISLMap lexMin = invmap_a.copy().lexMin();
//		System.out.println("lexmin ="+lexMin);
//		JNIISLMap lexMax = invmap_a.copy().lexMax();
//		System.out.println("lexmax ="+lexMax);
//		
//		JNIISLMap map_b = map_a.computeDivs();
//		System.out.println(map_b);
//		
//		
//		JNIISLBasicSet bset = ISLFactory.islBasicSet("[mat_ch_dim1, matinv_ch_dim1, matinv_ch_dim2, mat_ch_dim2] -> { [i, k, l] : k = i and i >= 0 and i <= -1 + mat_ch_dim1 and i <= -1 + mat_ch_dim2 and l >= i and l <= -1 + 2mat_ch_dim2 }");
//		System.out.println(bset);
//		JNIISLSet set12 = JNIISLSet.fromBasicSet(bset);
//		System.out.println(set12);
	}
	
	public static void testCoBTest() {
		CoBtestBody();
	}
	private static void CoBtestBody() {
		System.out.println("##########################");
		System.out.println("## Change of Base ");
		System.out.println("##########################");
		ISLSet set_a = islSet("[N,M]->{[i,j] : i < N & 0 < i & j < M & 0 < j}");
		System.out.println("## set : " +set_a);
		ISLMap map_a = islMap("[N,M]->{[i,j]->[i',j'] : i'=i & j'=i+j}");
		//JNIISLMap map_a = islMap("[N,M]->{[i,j]->[j,i] }");
		System.out.println("## transform :"+map_a );
		ISLMap reverse = map_a.copy().reverse();
		System.out.println("## reverse transform :"+reverse);
		ISLSet set_b = set_a.apply(map_a);
		System.out.println("## transformed set  :"+set_b );

	}

	public static void testIntersectUnionExamples() {
		intersectUnionExamplesBody();
	}
	private static void intersectUnionExamplesBody() {
		System.out.println("## Intersect/Union example");
		ISLSet set_a = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		ISLSet set_b = islSet("[N,P]-> { [j,i] : j>= P  & j-N< 0  & i>= 0  & 2i-j+2*P< 0  }");
		System.out.println("a = "+set_a+";");
		System.out.println("b = "+set_b+";");
		System.out.println("a inter b = "+set_a.copy().intersect(set_b.copy())+";");
		System.out.println("Hello!");
		set_a = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println("Bye!");
		set_b = islSet("[N,P]-> { [j,i] : j>= P  & j-N< 0  & i>= 0  & 2i-j+2*P< 0  }");
		System.out.println("a = "+set_a+";");
		System.out.println("b = "+set_b+";");
		System.out.println("a union b = "+set_a.union(set_b)+";");
	}
	
	public static void testBasicMapTest() {
		basicMapTestBody();
	}
	private static void basicMapTestBody() {
		System.out.println("## Testing map");
		ISLMap bmap_a = ISLFactory.islMap("{[i,j]->[k]}");
		System.out.println(bmap_a);
		System.out.println(bmap_a.copy().computeDivs());
		System.out.println("## convex hull");
		System.out.println(bmap_a.copy().convexHull());
		System.out.println("## is empty ");
		System.out.println(bmap_a.isEmpty());
		System.out.println("## domain ");
		System.out.println(bmap_a.getDomain());
		System.out.println("## nbIn ");
		System.out.println(bmap_a.getNbInputs());
		System.out.println("## nbOut");
		System.out.println(bmap_a.getNbOutputs());
		System.out.println("## nbParam ");
		System.out.println(bmap_a.getNbParams());
		System.out.println("## range");
		System.out.println(bmap_a.getRange());
		
	}
	
	public static void testBasicSetTest() {
		basicSetTestBody();
	}
	private static void basicSetTestBody() {
		System.out.println("## Testing set");
		ISLSet bset_a = ISLFactory.islSet("[]->{[i] : 0<i & i < 10}");
		System.out.println(bset_a);
		System.out.println("## isEmpty");
		System.out.println(bset_a.isEmpty());
		System.out.println("## lexMin");
		System.out.println(bset_a.copy().lexMin());
		System.out.println("## lexMax");
		System.out.println(bset_a.copy().lexMax());
		System.out.println("## AffHull");
		ISLBasicSet affineHull = bset_a.copy().affineHull();
		System.out.println(affineHull);
	}
	
	public static void testParameterOrder() {
		parameterOrderBody();
	}
	private static void parameterOrderBody() {
		ISLSet set1 = ISLFactory.islSet("[M,N]->{[i] : M<i & i < N}");
		System.out.println(set1);
		ISLSet set2 = ISLFactory.islSet("[N,M]->{[i] : M<i & i < N}");
		System.out.println(set2);
		
		ISLSet set = set1.intersect(set2).coalesce();
		System.out.println(set);
	}
	
	public static void testDiffOperator() {
		ISLSet set1 = ISLFactory.islSet("[M,N]->{[i] : M<i & i < N & i != 0 & i != 1}");
		System.out.println(set1);
		set1.free();
	}
	
	public static void testQaffine() {
		ISLMap m1 = ISLFactory.islMap("[N] -> { [i] -> [i',ii] : exists (a = [i/8] : i' = a && ii = i-8a ) }");
		ISLMap m2 = ISLFactory.islMap("[N] -> { [i] -> [i',ii] : i = 8i'+ii && 0 <= ii < 8 }");
		System.out.println(m1.getBasicMapAt(0));
		System.out.println(m2.getBasicMapAt(0));
		assertTrue(m1.isEqual(m2));
		m1.free();
		m2.free();
	}
	
	public void test_polymodel_src_polybenchs_perso_prodmat_prodmat_scop_0 () {
		polymodel_src_polybenchs_perso_prodmat_prodmat_scop_0Body();
	}
	private static void polymodel_src_polybenchs_perso_prodmat_prodmat_scop_0Body() {
//		String domains = "[M, N, P] -> { S0[i, j] : j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + M; S1[i, j, k] : k >= 0 and k <= -1 + P and j >= 0 and j <= -1 + N and i >= 0 and i <= -1 + M }";
//		String idSchedStr = "[M, N, P] -> { S0[i, j] -> [i,j,0,0]; S1[i,j,k] -> [i,j,1,k]}";
		String wAccesses = "[M, N, P] -> {S0[i, j] -> C[i][j]; S1[i, j, k] -> C[i][j] }";
//		String rAccesses = "[M, N, P] -> {S1[i, j, k] -> C[i][j]; S1[i, j, k] -> A[i][k]; S1[i, j, k] -> B[k][j] }";
		
		
//		String prdgStr = "[M, N, P] -> { S1[i, j, 0] -> S0[i, j] : P >= 1 and j >= 0 and i >= 0 and i <= -1 + M and j <= -1 + N; S1[i, j, k] -> S1[i, j, -1 + k] : i >= 0 and i <= -1 + M and k >= 1 and k <= -1 + P and j >= 0 and j <= -1 + N }";
		String plutoSchedule = "[M, N, P] -> { S1[i, j, k] -> [8i, j, k, 1]; S0[i, j] -> [i, 2j, 0, 0] }";


//		JNIISLUnionSet doms = ISLFactory.islUnionSet(domains);
//		JNIISLUnionMap idSched = ISLFactory.islUnionMap(idSchedStr);
		ISLUnionMap wa = ISLFactory.islUnionMap(wAccesses);
//		JNIISLUnionMap ra = ISLFactory.islUnionMap(rAccesses);
//		JNIISLUnionMap prdg = ISLFactory.islUnionMap(prdgStr);
		ISLUnionMap sch = ISLFactory.islUnionMap(plutoSchedule);
		
//		JNIISLUnionMap computeValueBasedADA = JNIISLDataflowAnalysis.computeValueBasedADA(doms.copy(), wa.copy(), ra.copy(), idSched.copy());
//		assertTrue(prdg.copy().isEqual(computeValueBasedADA) != 0);
//		
//		JNIISLSchedule computePlutoSchedule = JNIISLSchedule.computePlutoSchedule(doms.copy(), prdg.copy());
//		assertTrue(sch.copy().isEqual(computePlutoSchedule.getMap()) != 0);
		
		ISLUnionMap applyDomain = wa.applyDomain(sch);
		for (ISLMap m : applyDomain.getMaps()) {
			for (ISLBasicMap bmap : m.getBasicMaps()) {
				System.out.println(bmap);
				System.out.println(bmap.getClosedFormRelation());
			}
		}
//		System.out.println(applyDomain);
	}
}
