package fr.irisa.cairn.jnimap.isl.tests.impl;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLDataflowAnalysis;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;

public class TestDataflowAnalysis extends TestCase {

	public static void computeAda(String d, String w, String r, String s) {
		ISLUnionSet domains = ISLFactory.islUnionSet(d);
		ISLUnionMap writes = ISLFactory.islUnionMap(w);
		ISLUnionMap reads = ISLFactory.islUnionMap(r);
		ISLUnionMap schedule = ISLFactory.islUnionMap(s);
		ISLUnionMap prdg = ISLDataflowAnalysis.computeValueBasedDependenceGraph(domains, writes, reads, schedule);
		System.out.println(prdg);
	}
	
	public static void testDataflowMatrixProduct() {
		String domains = "[M, N, P] -> { S0[i, j] : i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0; S1[i, j, k] : i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0 and k <= -1 + P and k >= 0 }";
		String writes = "[M, N, P] -> { S0[i, j] -> C[i, j]; S1[i, j, k] -> C[i, j] }";
		String reads = "[M, N, P] -> { S1[i, j, k] -> B[k, j]; S1[i, j, k] -> C[i, j]; S1[i, j, k] -> A[i, k] }";
		String idSchedule = "[M, N, P] -> { S1[i, j, k] -> [0, i, 0, j, 1, k, 0]; S0[i, j] -> [0, i, 0, j, 0, 0, 0] }";
		computeAda(domains,writes, reads, idSchedule);
	}
	

	
	public static void testIf() {
		String domains = "{ S0[0]; S1[i] : i >= 1 and i <= 7 }";
		String writes = "{ S0[i] -> a[]; S1[i] -> a[] }";
		String reads = "{ S1[i] -> a[] }";
		String idSchedule = "{ S0[i] -> [0, i, 0]; S1[i] -> [0, i, 0] }";
		computeAda(domains,writes, reads, idSchedule);
	}
	
}
