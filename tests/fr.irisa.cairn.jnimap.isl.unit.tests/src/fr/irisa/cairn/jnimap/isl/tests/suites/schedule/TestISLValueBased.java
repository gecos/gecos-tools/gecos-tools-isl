package fr.irisa.cairn.jnimap.isl.tests.suites.schedule;

import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.ISLSchedule.JNIISLSchedulingOptions;
import fr.irisa.cairn.jnimap.isl.jni.tests.AbstractTestFromISL;

public class TestISLValueBased extends AbstractTestFromISL {
	
	@Override
	public void computeTest(String path, 
			ISLUnionSet domains, ISLUnionMap idSchedules, ISLUnionMap writes, ISLUnionMap reads,
			ISLUnionMap valueBasedPrdg, ISLUnionMap valueBasedPlutoSchedule, ISLUnionMap valueBasedFeautrierSchedule,
			ISLUnionMap memoryBasedPrdg, ISLUnionMap memoryBasedPlutoSchedule, ISLUnionMap memoryBasedFeautrierSchedule) {
		ScheduleCompare.cmp(domains, valueBasedPrdg, JNIISLSchedulingOptions.ISL_SCHEDULE_ALGORITHM_ISL);
	}

}