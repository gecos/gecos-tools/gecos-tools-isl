package fr.irisa.cairn.jnimap.isl.tests.suites;

import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.extra.ISLLexShiftTools;
import fr.irisa.cairn.jnimap.isl.jni.tests.AbstractTestFromISL;

public class TestNext extends AbstractTestFromISL {

	static long ttimeJava = 0;
	static long ttimeNative = 0;
	
	@Override
	public void computeTest(String path, 
			ISLUnionSet domains, ISLUnionMap idSchedules, ISLUnionMap writes, ISLUnionMap reads,
			ISLUnionMap valueBasedPrdg, ISLUnionMap valueBasedPlutoSchedule, ISLUnionMap valueBasedFeautrierSchedule,
			ISLUnionMap memoryBasedPrdg, ISLUnionMap memoryBasedPlutoSchedule, ISLUnionMap memoryBasedFeautrierSchedule) {
		
		ISLSet set = domains.apply(valueBasedPlutoSchedule).getSetAt(0);
		
		System.out.println(set);
		int delta = 7;
		int coalescing_depth = 2;
		int N = 1;
		System.out.println("**************************************");

		
		ISLMap npJava = null;
		long time = 0;
		for (int i = 0; i < N; i++) {
			long tmptime = System.currentTimeMillis();
			npJava = ISLLexShiftTools.lexNextPower(set, coalescing_depth,delta);
//			npJava = ISLLexShiftTools.lexNext(set, coalescing_depth);
//			npJava = ISLLexShiftTools.slow_power(npJava,delta,false);
			tmptime = System.currentTimeMillis()- tmptime;
			time += tmptime;
		}
		time = time / N;
		System.out.println("Java : ");
		System.out.println(npJava);
		System.out.println("Average for "+N+" runs : "+time+"ms");
		System.out.println("**************************************");
		ttimeJava += time;

		time = 0;
		ISLMap npNative = null;
		for (int i = 0; i < N; i++) {
			long tmptime = System.currentTimeMillis();
			npNative = ISLLexShiftTools.lexNext(set, coalescing_depth);
			npNative = npNative.power(delta);
			tmptime = System.currentTimeMillis()- tmptime;
			time += tmptime;
		}
		
		time = time / N;
		System.out.println("Native : ");
		System.out.println(npNative);
		System.out.println("Average for "+N+" runs : "+time+"ms");
		System.out.println("**************************************");
		ttimeNative += time;
		if(ttimeNative==0)ttimeNative++;
		
		boolean equivalence = npNative.isEqual(npJava);
		System.out.println("Equivalent ? "+equivalence+" \t\t Speedup native : "+(ttimeJava*100/ttimeNative)+"%");
		assertTrue(equivalence);
		
	}

	@Override
	public void test_polymodel_src_polybenchs_perso_bor_fdtd_GScopRoot_func_scop_0_() {}

	@Override
	public void test_polymodel_others_external_tests_test_GScopRoot_test2_scop_0_() {
		// TODO Auto-generated method stub
		super.test_polymodel_others_external_tests_test_GScopRoot_test2_scop_0_();
	}
	
}
