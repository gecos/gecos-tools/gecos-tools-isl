package fr.irisa.cairn.jnimap.isl.tests.codegen

import fr.irisa.cairn.jnimap.isl.ISLASTExpression
import fr.irisa.cairn.jnimap.isl.ISLASTLiteral
import fr.irisa.cairn.jnimap.isl.ISLASTOperation
import static fr.irisa.cairn.jnimap.isl.ISLASTOpType.*
import fr.irisa.cairn.jnimap.isl.ISLASTIdentifier

class JNIISLExpressionChecker {

	new() {
	}
	  
	def dispatch void buildExpr(ISLASTLiteral obj) {
	}
	
	def dispatch void buildExpr(ISLASTIdentifier obj) {
		if (obj.getID==null) {
			throw new RuntimeException("Cannot build void object from "+obj)
		}
	}
	def dispatch void buildExpr(ISLASTOperation obj) {
		val arg0 = obj.getArgument(0)
		val arg1 = obj.getArgument(1)
		buildExpr(arg0)
		buildExpr(arg1)
		val opType = obj.getOpType()
		if(opType===null) {
			println(obj);
		}
		val res= switch(opType.getValue()) {
		
		case ISL_AST_OP_MINUS : {}
		case ISL_AST_OP_ADD  : {}
		case ISL_AST_OP_SUB  : {}
		case ISL_AST_OP_MUL:{}
		case ISL_AST_OP_MIN:{}
		case ISL_AST_OP_MAX:{}
		case ISL_AST_OP_DIV: 	{}	//exact division i.e the result is known to be integer 
		case ISL_AST_OP_FDIV_Q : {}	
		case ISL_AST_OP_PDIV_Q :{}	//result of integer division used when op1 is known to be non-negative
		case ISL_AST_OP_PDIV_R :	{} //remainder of integer division used when p is known to be non-negative
			
		default:
				throw new RuntimeException(obj.getOpType().getName+" not supported")
			
		}
		
	}
	
	def dispatch void buildConstraint(ISLASTExpression obj) {
		throw new RuntimeException("Cannot build void object from "+obj)
	}
	def dispatch void buildConstraint(ISLASTOperation obj) {
		buildExpr(obj.getArgument(0))
		buildExpr(obj.getArgument(1))
		if (obj.getOpType()!=null) {
			switch(obj.getOpType().getValue()) {
//			 case ISL_AST_OP_EQ :
//				voidBuilder::eq((op1),(op2))
//			 case ISL_AST_OP_LE :
//				voidBuilder::le((op1),(op2))
//			 case ISL_AST_OP_LT :
//				voidBuilder::lt((op1),(op2))
//			 case ISL_AST_OP_GE :
//				voidBuilder::ge((op1),(op2))
//			 case ISL_AST_OP_GT :
//				voidBuilder::gt((op1),(op2))
//			default:
			}
		} else {
			throw new RuntimeException("Cannot build void object from "+obj)
		}
	}
	
	def void buildUnionOfConstraintSystem(ISLASTOperation obj) {
		switch(obj.getOpType().getValue()) {
			 case ISL_AST_OP_OR:
					for (i : 0..obj.nbArgs-1 ) {
						buildConstraint(obj.getArgument(i));
					} 
			 case ISL_AST_OP_AND: {
						buildConstraintSystem(obj);
			  		}
			 default : {
			 	buildConstraint(obj)
			 }
		}
	 	
	}
	

	def void buildConstraintSystem(ISLASTOperation obj) {
		val opType  = obj.getOpType()
		switch(opType.getValue()) {
			 case ISL_AST_OP_AND : {
					for (i : 0..obj.nbArgs-1 ) {
						buildConstraint(obj.getArgument(i))
					} 
				}
			 case ISL_AST_OP_MIN : {
					for (i : 0..obj.nbArgs-1 ) {
						buildConstraint(obj.getArgument(i))
					} 
				}
			 case ISL_AST_OP_EQ :{
				buildConstraint(obj);
				}
			 case ISL_AST_OP_LE :{
				buildConstraint(obj);
				}
			 case ISL_AST_OP_LT :{
				buildConstraint(obj);
				}
			 case ISL_AST_OP_GE :{
				buildConstraint(obj);
				}
			 case ISL_AST_OP_GT : {
				buildConstraint(obj);
				}
				default : {
					throw new RuntimeException("Cannot build void object from "+obj)
				}
		}
		
	}

	
	def void buildStatement(ISLASTOperation obj) {
		val optype = obj.getOpType()
		switch(optype.getValue()) {
			 case ISL_AST_OP_CALL: {
 				for(i : 1..obj.nbArgs) {
					throw new RuntimeException("Cannot build voidMacro object from "+obj)
 				}
 			}
 			default :{
				throw new RuntimeException("Cannot build voidMacro object from "+obj)
 			}
		}
	}
	

}
