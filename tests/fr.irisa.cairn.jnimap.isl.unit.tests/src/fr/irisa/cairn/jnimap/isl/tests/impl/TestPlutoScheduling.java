package fr.irisa.cairn.jnimap.isl.tests.impl;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLSchedule;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.ISLSchedule.JNIISLSchedulingOptions;

public class TestPlutoScheduling extends TestCase {

	public static void computeSchedule(String dom, String prdg) {
		ISLUnionSet domains = ISLFactory.islUnionSet(dom);
		ISLUnionMap prdgMap = ISLFactory.islUnionMap(prdg);
		
		
		JNIISLSchedulingOptions options = new JNIISLSchedulingOptions();
		options.setAlgorithm(JNIISLSchedulingOptions.ISL_SCHEDULE_ALGORITHM_ISL);
		
		
		ISLSchedule schedule = ISLSchedule.computeSchedule(domains, prdgMap.copy(), prdgMap.copy(), options);

		//tiling
//		for (JNIISLBand band : schedule.getBandForest()) {
//			System.out.println(band);
//			JNIISLVector vec = new JNIISLVector(JNIISLContext.getCtx(), 1);
//			vec = vec.setElement(0, 8);
//			int tile = band.tile(vec);
//			System.out.println(tile);
//			System.out.println(band);
//		}
		
		
//		SchedulePrinter.printSchedule(schedule);
//		System.out.println(schedule);
		ISLUnionMap map = schedule.getMap();
		System.out.println(map);
	}
	
	public static void testMatrixProduct() {
		String domains = "[M, N, P] -> { S0[i, j] : i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0; S1[i, j, k] : i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0 and k <= -1 + P and k >= 0 }";
		String prdg = "[M, N, P] -> { S1[i, j, k] -> S1[i, j, k-1] : i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0 and k <= -1 + P and k >= 1; S1[i, j, k] -> S0[i, j] : k = 0 and i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0 and P >= 1 }";
		computeSchedule(domains, prdg);
	}
	
	public static void testGauss() {
		String domains = "[N, M] -> { S0[i, j] : j >= 0 and j <= -1 + M and i >= 1 and i <= -2 + N; S1[i, j] : j >= 1 and j <= -2 + M and i >= 1 and i <= -2 + N }";
		String prdg = "[N, M] -> { S1[i, j] -> S0[i, 1 + j] : i >= 1 and i <= -2 + N and j >= 1 and j <= -2 + M; S1[i, j] -> S0[i, j] : i >= 1 and i <= -2 + N and j >= 1 and j <= -2 + M; S1[i, j] -> S0[i, -1 + j] : i >= 1 and i <= -2 + N and j >= 1 and j <= -2 + M }";
		computeSchedule(domains, prdg);
	}

}
