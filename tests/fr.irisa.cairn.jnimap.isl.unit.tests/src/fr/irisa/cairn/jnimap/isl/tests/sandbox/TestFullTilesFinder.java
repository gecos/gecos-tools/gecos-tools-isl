package fr.irisa.cairn.jnimap.isl.tests.sandbox;
import static fr.irisa.cairn.jnimap.isl.ISLFactory.islMap;
import static fr.irisa.cairn.jnimap.isl.ISLFactory.islSet;

import fr.irisa.cairn.jnimap.isl.ISLDimType;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLPoint;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import junit.framework.TestCase;

public class TestFullTilesFinder extends TestCase {

	public static void enumerate(ISLSet set) {
		for (ISLPoint p : set.getPoints())
			System.out.println(p);
	}

	public static void test1() {
		int tilei = 2;
		int tilej = 3;
		

		ISLMap skewSched = islMap("[N] -> { [i,j] -> [i',j'] : i'=i+j && j'=j }");
		ISLMap tileSched = islMap("[N] -> { [i,j] -> [i',j',ii,jj] : " +
				"0 <= ii < "+tilei+" && 0 <= jj < "+tilej+ " && " +
				"i = i'*"+tilei+" + ii && j = j'*"+tilej+" + jj }");
		ISLSet set = islSet("[N]-> { [j,i] : 0 <= i < N && 0 <= j < N }");
		
		System.out.println(set);
		ISLSet skew = set.apply(skewSched);
		System.out.println(skew);
		ISLSet tiled = skew.apply(tileSched);
		System.out.println(tiled);

//		tiled = tiled.intersect(islSet("[N] -> { [i,j,ii,jj] : N=7 }")).projectOut(JNIISLDimType.isl_dim_param, 0, 1);
		
		
//		JNIISLSet proj1 = tiled.copy().projectOut(JNIISLDimType.isl_dim_set, 2, 2);
//		System.out.println(proj1);
		
		ISLSet proj2 = tiled.copy().apply(islMap("[N] -> {[i,j,ii,jj] -> [i',j', a,b] : " +
				"i' = i && j'=j && 0 <= a < "+tilei+" && 0 <= b < "+tilej+"}"));
		System.out.println(proj2);
		
		ISLSet t = proj2.subtract(tiled).projectOut(ISLDimType.isl_dim_set, 2, 2);
		System.out.println(t);
		
		t = t.intersect(islSet("[N] -> { [i,j] : N=7 }")).projectOut(ISLDimType.isl_dim_param, 0, 1).coalesce();

		System.out.println(t);
		enumerate(t);
	}
}
