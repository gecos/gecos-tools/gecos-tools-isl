package fr.irisa.cairn.jnimap.isl.tests.suites.schedule;

import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.extra.JNIISLFlowComputation;
import fr.irisa.cairn.jnimap.isl.extra.JNIISLScheduleTiling;
import fr.irisa.cairn.jnimap.isl.jni.tests.AbstractTestFromISL;

public class TestScheduleFlowComputation extends AbstractTestFromISL {

	private final static int tile_size = 8;
	
	public void tileBand(ISLUnionMap plutoSchedule, ISLUnionMap prdg, int[] tileSize) {
		
		int outerTileLevel = 0;
		int innerTileLevel = 1;
		ISLUnionMap tile = JNIISLScheduleTiling.tileBand(plutoSchedule, outerTileLevel, innerTileLevel, tileSize, 2, 4);
		ISLUnionMap prdgTransD = prdg.copy().applyDomain(tile.copy());
		ISLUnionMap prdgTrans = prdgTransD.copy().applyRange(tile.copy());
		prdgTrans = prdgTrans.coalesce();
			
		for ( int i = 0; i < 4; i++) {
			for ( int j = 0; j < 4; j++ ) {
				ISLUnionMap flowIn = JNIISLFlowComputation.computeFlowInSet(tile.copy(), prdgTrans.copy(), outerTileLevel + 1, i, j, 4);
				System.out.println(" flowIn at Processor " + i + ": " + flowIn);
			}
        }
		
		
		for ( int i = 0; i < 4; i++) {
			for ( int j = 0; j < 4; j++) {
				ISLUnionMap flowOut = JNIISLFlowComputation.computeFlowOutSet(tile.copy(), prdgTrans.copy(), outerTileLevel+1, i, j, 4);
				System.out.println("FlowOut: " + flowOut);
			}
		}
			
	}
		
	
	@Override
	public void computeTest(String path, 
			ISLUnionSet domains, ISLUnionMap idSchedules, ISLUnionMap writes, ISLUnionMap reads,
			ISLUnionMap valueBasedPrdg, ISLUnionMap valueBasedPlutoSchedule, ISLUnionMap valueBasedFeautrierSchedule,
			ISLUnionMap memoryBasedPrdg, ISLUnionMap memoryBasedPlutoSchedule, ISLUnionMap memoryBasedFeautrierSchedule) {

		int[] tileSize = {tile_size, 4};
		if ( valueBasedPlutoSchedule.copy().getMapAt(0).getRange().getNbIndices() > 1) {
			tileBand(valueBasedPlutoSchedule, valueBasedPrdg, tileSize); 
		} 
		
	}

}
