package fr.irisa.cairn.jnimap.isl.tests.suites;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.irisa.cairn.jnimap.isl.ISLMultiAff;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.jni.tests.AbstractTestFromISL;
import fr.irisa.cairn.jnimap.isl.memorylayout.ISLMemoryLayoutTools;

public class TestArrayContraction extends AbstractTestFromISL {

	/**
	 * Contracts all written arrays.
	 */
	public void computeTest(String path, ISLUnionSet domains, ISLUnionMap idSchedules,
			ISLUnionMap writes, ISLUnionMap reads,
			ISLUnionMap valueBasedPrdg,
			ISLUnionMap valueBasedPlutoSchedule,
			ISLUnionMap valueBasedFeautrierSchedule,
			ISLUnionMap memoryBasedPrdg,
			ISLUnionMap memoryBasedPlutoSchedule,
			ISLUnionMap memoryBasedFeautrierSchedule) {
		List<String> arrays = new ArrayList<>();
		for (ISLSet set : writes.copy().getRange().coalesce().getSets())
			arrays.add(set.getTupleName());
		for (String arrayName : arrays) {
			ISLMemoryLayoutTools allocator = new ISLMemoryLayoutTools();
			//FIXME use domains.copy().paramSet()
			List<Map<ISLSet, ISLMultiAff>> alloc = allocator.contractSuccessiveModuloBasic(arrayName, domains.copy().getSetAt(0).params(), domains, writes, reads, valueBasedPrdg, valueBasedFeautrierSchedule);
			String str = arrayName+ISLMemoryLayoutTools.printAlloc(alloc);
			System.out.println(str);
		}
	}

	
	@Override
	public void test_polymodel_src_polybenchs_perso_STAP_Mat_Invert_GScopRoot_STAP_Mat_Invert_scop_0_() {}
	@Override
	public void test_polybench_gecos_stencils_adi_adi_GScopRoot_kernel_adi_scop_0_() {}
	@Override
	public void test_polymodel_src_polybenchs_perso_adi_core_patched_fail_indices_GScopRoot_adi_core_scop_0_() {}
	@Override
	public void test_polymodel_src_polybenchs_perso_adi_core_patched_GScopRoot_adi_core_scop_0_() {}
	@Override
	public void test_polymodel_src_polybenchs_perso_SetupLargerBlocks_GScopRoot_SetupLargerBlocks_scop_0_() {}
	
//
//	@Override
//	public void test_polybench_gecos_linear_algebra_solvers_gramschmidt_gramschmidt_GScopRoot_kernel_gramschmidt_scop_0_() {
//		// TODO Auto-generated method stub
//		super.test_polybench_gecos_linear_algebra_solvers_gramschmidt_gramschmidt_GScopRoot_kernel_gramschmidt_scop_0_();
//	}
}
