package fr.irisa.cairn.jnimap.isl.tests.impl;

import junit.framework.TestCase;
import fr.irisa.cairn.jnimap.isl.IISLDataflowResults;
import fr.irisa.cairn.jnimap.isl.ISLDataflowAnalysis;
import fr.irisa.cairn.jnimap.isl.ISLFactory;
import fr.irisa.cairn.jnimap.isl.ISLUnionMap;

public class TestDataflow extends TestCase {
/*
 * --------------------------
   i = 0;
   do {
     j = 0;
     do {
       k = 0;
       do {
         C[i][j] = C[i][j] + A[i][k] * B[k][j];
         ++k;
       } while (k < K);
       ++j;
     } while (j < N);
     ++i;
   } while (i < M); 	
 */
/*
 *     
 *      
 *      Sink: 
 { 
 	dobody_A[i0, i1, i2] -> A[392i0 + 8i2] : 
 		i0 >= 0 and i0 <= 35 and i1 >= 0 and i1 <= 35 and i2 >= 0 and i2 <= 35;
   	dobody_C[i0, i1, i2] -> C[392i0 + 8i1] : 
   		i0 >= 0 and i0 <= 35 and i1 >= 0 and i1 <= 35 and i2 >= 0 and i2 <= 35; 
   	dobody_B[i0, i1, i2] -> B[8i1 + 392i2] : 
   		i0 >= 0 and i0 <= 35 and i1 >= 0 and i1 <= 35 and i2 >= 0 and i2 <= 35; 
   	FinalRead__C[0] -> C[o0]; 
   	FinalRead__B[0]->  B[o0]; 
   	FinalRead__A[0] -> A[o0] 
 }

Must Source:
 "{" 
   	"dobody_C[i0, i1, i2] -> C[392i0 + 8i1] :"+ 
   	" i0 >= 0 and i0 <= 35 and i1 >= 0 and i1 <= 35 and i2 >= 0 and i2 <= 35" 
 "}"

May Source:
 {  
 }

Schedule:
 { 
 	"dobody_C[i0, i1, i2] -> scattering[0, i0, 0, i1, 0, i2,0];"
 	"dobody_B[i0, i1, i2] -> scattering[0, i0, 0, i1, 0, i2, 0];"
	"dobody_A[i0, i1, i2] -> scattering[0, i0, 0, i1, 0, i2, 0];"
	"FinalRead__A[i0] -> scattering[1, o1, o2, o3, o4, o5, o6];"
	"FinalRead__C[i0] -> scattering[1, o1, o2, o3, o4, o5, o6];"
	"FinalRead__B[i0] -> scattering[1, o1, o2, o3, o4, o5, o6]" 
} 
	
*/
	
	public static void computeDataFlow(String name, String reads, String writes, String maySrc, String schedule) {
		ISLUnionMap sink = ISLFactory.islUnionMap(reads);
		ISLUnionMap must_source = ISLFactory.islUnionMap(writes);
		ISLUnionMap may_source = ISLFactory.islUnionMap(maySrc);
		ISLUnionMap sch = ISLFactory.islUnionMap(schedule);
		IISLDataflowResults data = ISLDataflowAnalysis.computeDependences(sink, must_source, may_source, null, sch);
		System.out.println("====== TestDataflow(" + name + ")=====");
		System.out.println("Must_dep="+data.getMustDependence());
		System.out.println("Must_no_src="+data.getMustNoSource());
		System.out.println();
	}
	
	/*
	 * 
	 * { 
	 * 	dobody_C[i0, i1, 35] -> FinalRead__C[0] : 
	 * 		i1 <= 1714 - 49i0 and i1 >= -49i0 and i1 >= 0 and i1 <= 35; 
	 *  dobody_C[35, i1, 35] -> FinalRead__C[0] : 
	 *  	i1 >= 0 and i1 <= 35; 
	 *  dobody_C[i0, i1, i2] -> dobody_C[i0, i1, 1 + i2] : 
	 *  	i0 >= 0 and i0 <= 35 and i1 >= 0 and i1 <= 35 and i2 >= 0 and i2 <= 34 
	 *  }

	 */
	
	public static void testDataflowWhile() {
		String reads= " { "+
				"dobody_A[i0, i1, i2] -> A[392i0 + 8i2] :"+ 
					"i0 >= 0 and i0 <= 35 and i1 >= 0 and i1 <= 35 and i2 >= 0 and i2 <= 35;"+
				"dobody_C[i0, i1, i2] -> C[392i0 + 8i1] :" +
					"i0 >= 0 and i0 <= 35 and i1 >= 0 and i1 <= 35 and i2 >= 0 and i2 <= 35;"+ 
				"dobody_B[i0, i1, i2] -> B[8i1 + 392i2] :" +
					"i0 >= 0 and i0 <= 35 and i1 >= 0 and i1 <= 35 and i2 >= 0 and i2 <= 35;"+ 
				"FinalRead__C[0] -> C[o0]; "+
				"FinalRead__B[0]->  B[o0]; "+
				"FinalRead__A[0] -> A[o0] "+
		   	"}";
		String writes = "{"+ 
			   	"dobody_C[i0, i1, i2] -> C[392i0 + 8i1] :"+ 
			   	" i0 >= 0 and i0 <= 35 and i1 >= 0 and i1 <= 35 and i2 >= 0 and i2 <= 35"+ 
			 "}";
		String maySrc = "{}";
		String schedule=
				"{"+
			 		"dobody_C[i0, i1, i2] -> scattering[0, i0, 0, i1, 0, i2,0];"+
			 		"dobody_B[i0, i1, i2] -> scattering[0, i0, 0, i1, 0, i2, 0];"+
					"dobody_A[i0, i1, i2] -> scattering[0, i0, 0, i1, 0, i2, 0];"+
					"FinalRead__A[i0] -> scattering[1, o1, o2, o3, o4, o5, o6];"+
					"FinalRead__C[i0] -> scattering[1, o1, o2, o3, o4, o5, o6];"+
					"FinalRead__B[i0] -> scattering[1, o1, o2, o3, o4, o5, o6]" +
				"}";
		computeDataFlow("while", reads, writes, maySrc, schedule);
	}
	

	
	public static void testDataflowMatrixProduct() {
		String S0constraints = "i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0";
		String S1constraints = "i <= -1 + M and i >= 0 and j <= -1 + N and j >= 0 and k <= -1 + P and k >= 0";

		String maySrc = "[M, N, P] -> {}";
		String reads = String.format("[M, N, P] -> { S1[i, j, k] -> B[k, j] : %s; S1[i, j, k] -> C[i, j] : %s; S1[i, j, k] -> A[i, k] : %s}", S1constraints, S1constraints, S1constraints);
		String writes = String.format("[M, N, P] -> { S0[i, j] -> C[i, j] : %s; S1[i, j, k] -> C[i, j] : %s}", S0constraints, S1constraints);
		String idSchedule = "[M, N, P] -> { S1[i, j, k] -> [0, i, 0, j, 1, k, 0]; S0[i, j] -> [0, i, 0, j, 0, 0, 0] }";
		
		computeDataFlow("matrix product", reads,writes, maySrc, idSchedule);
	}

}
