package fr.irisa.cairn.jnimap.isl.tests.impl;

import static fr.irisa.cairn.jnimap.isl.ISLFactory.islSet;

import fr.irisa.cairn.jnimap.isl.ISLContext;
import fr.irisa.cairn.jnimap.isl.ISLMap;
import fr.irisa.cairn.jnimap.isl.ISLSet;
import fr.irisa.cairn.jnimap.isl.JNIPtrBoolean;
import junit.framework.TestCase;

public class ISLPowerTest extends TestCase {

	public static void test1() {
		body1();
	}
	
	private static void body1() {
		ISLSet set_b = islSet("[N,P]-> { [j,i] : j>= i  & j-N< 0  & i>= 0  & i-3< 0  }");
		System.out.println(set_b);
		ISLMap next = set_b.getLexNextMap(2);
		System.out.println(next);
		
		JNIPtrBoolean exact = new JNIPtrBoolean();
		ISLMap nextk = next.parametricPower(exact);
		System.out.println(nextk);
		System.out.println("exact ? "+ exact);
	}
}
