package fr.irisa.cairn.jnimap.isl.tests.suites.schedule;

import fr.irisa.cairn.jnimap.isl.ISLUnionMap;
import fr.irisa.cairn.jnimap.isl.ISLUnionSet;
import fr.irisa.cairn.jnimap.isl.extra.JNIISLScheduleTiling;
import fr.irisa.cairn.jnimap.isl.jni.tests.AbstractTestFromISL;

public class TestScheduleTileBand extends AbstractTestFromISL {

	private final static int tile_size = 8;
	
	
	@Override
	public void computeTest(String path, 
			ISLUnionSet domains, ISLUnionMap idSchedules, ISLUnionMap writes, ISLUnionMap reads,
			ISLUnionMap valueBasedPrdg, ISLUnionMap valueBasedPlutoSchedule, ISLUnionMap valueBasedFeautrierSchedule,
			ISLUnionMap memoryBasedPrdg, ISLUnionMap memoryBasedPlutoSchedule, ISLUnionMap memoryBasedFeautrierSchedule) {

		int[] tileSize = {tile_size, 4};
		if ( valueBasedPlutoSchedule.copy().getMapAt(0).getRange().getNbIndices() > 1) {
			ISLUnionMap tile = JNIISLScheduleTiling.tileBand(valueBasedPlutoSchedule, 0, 1, tileSize, 2, 4);
			System.out.println(tile);
		} 
	}

}
