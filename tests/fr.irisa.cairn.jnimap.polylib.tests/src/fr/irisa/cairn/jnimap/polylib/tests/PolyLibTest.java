package fr.irisa.cairn.jnimap.polylib.tests;

import org.junit.Test;

import fr.irisa.cairn.jnimap.polylib.PolyLibPolyhedron;
import fr.irisa.cairn.jnimap.polylib.extra.PolylibVertexRay;
import fr.irisa.cairn.jnimap.polylib.extra.PolylibVertexRay.DualPolyhedron;

public class PolyLibTest {

	@Test
	public void testPolyLib() {
		
		//"[n] -> { [i] : exists (a = [i/10] : 0 <= i and i <= n and i - 10 a <= 6) }"
		long[][] mat = new long[][] {
			new long[] {1, 1, 0, 0, 0},
			new long[] {1, -1, 1, 0, 0},
			new long[] {1, -1, 0, 10, 6},
			new long[] {1, 1, 0, -10, 0}
		};
		
		compute(mat);
	}

	public static void compute(long[][] mat ) {
		PolyLibPolyhedron p = PolyLibPolyhedron.createFromLongMatrix(mat);

		DualPolyhedron dp = PolylibVertexRay.compute(p);
		System.out.println(dp);
	}

//	//XXX : remove ISL dependency
//	static void main_exe(String[] args) {
//		if (args.length != 1) {
//			System.out.println("usage : java -jar constraint2vertices.jar \"isl_basic_set string specification\"");
//		} else {
//			String bsetStr = args[0];
//			JNIISLBasicSet bset = null;
//			try {
//				bset = ISLFactory.islBasicSet(bsetStr);
//			} catch (Exception e) {
//				bset = null;
//			}
//			if (bset == null) {
//				System.out.println("ERROR. String : \n\""+bsetStr+"\"\n >> does not represent a basic set (ISL format).");
//			} else {
//				compute(bset);
//			}
//		}
//	}
}
