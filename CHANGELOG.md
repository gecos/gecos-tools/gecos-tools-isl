# Changelog

## Release version 2.5.1 (25 jan 2024)
* Adds leftHermite function in ISLMatrix

## Release version 2.5.0 (23 jan 2024)
* Add missing methods in ISLMatrix and ISLBasicSet
* Add isl bindings for Mac arm64

## Release version 2.4.0 (25 jun 2020)
* Add wrap/curry related methods
* Add missing methos in ISLMultiPWAff

## Release version 2.3.0 (02 mar 2020)
* Remove old ISLDataflowAnalysis using deprecated functions.
* Add new ISLDatafloAnalysis using ISLUnionAccessInfo.

## Release version 2.2.0 (02 mar 2020)
* Upgrade ISL to 0.22.1

## Release version 2.1.0 (20 sep 2019)
* Initial support for schedule trees

## Release version 2.0.3 (20 sep 2019)
* Regenerated with new JNIMapper using Java11
* bug fix in ISLUnionMap.buildFromMaps

## Release version 2.0.2 (23 jul 2019)
* Added methods to ISLMatrix
* - to/from long matrix
* - rank, rowBasis, rowBasisExtension, and hasLinearlyIndependentRows

## Release version 2.0.1 (22 jul 2019)
* Fixes to 2.0.0 found when testing with Alpha
* - renameDims implementation was incorrectly starting to rename at dim(dimType)
* - dimNames now return null instead of a list with null as its element when index names are not available

## Release version 2.0.0 (22 jul 2019)
* Major changes to JNI bindings. 
* - JNI prefix to class names has been removed
* - jni sub package has been removed (everything is generated under jnimap.xxx instead of jnimap.jni.xxx)
* - Support for interfaces in new jnimapper is used to standardize user code
* - Method names are made consistent following a new convention
* - Deprecated methods in 1.X.X are removed
* see commit d08a7a5baf8591d9653f8a7d07019c9823320bff for details

## Release version 1.8.3 (16 jul 2019)
* Upgrade ISL to 0.21

## Release version 1.8.1 (01 apr 2019)

* - Fixed issues with running Barvinok fron binaries stored in jar
* - Added simple tests for Barvinok

## Release version 1.8.0 (01 apr 2019)

* - Fixed Barvinok feature

## Release version 1.7.1 (31 mar 2019)

* - Added to multiLineString method to JNIISLUnionMap 
* - Fixed incorrect index calculation in JNIISLMap.projectOutAllBut
* - added getAllInputTuples method in JNIISLUnionMap
* - Fixed inconsistent methods names buildFromMaps intor buildFromSets

## Release version 1.7.0 (07 Mar 2019)
 * Moving to 1.7.0 since 1.6.1 was actually adding new ISL functions
 * There is also a deployment of 1.6.0 called 1.6.1, so the versions were overlapping

## Release version 1.6.1 (07 Mar 2019)
 
 * Added helper methods to create unions from list of sets/maps
 * Added samplePoint
 * Added eval for ISLAff and ISLMultiAff

## Release version 1.6.0 (08 fev 2019)

* - Yet another fix in target def
* - Fixed AST checker to avoid NPE when converting expressions
* - Changed eclipse repo in target definition
* - Added helper methods to deal with dimensions management

## Release version 1.5.0 (23 oct 2018)

* - updated makefiles
* - Added libraries for Linux64
* Merge branch 'develop' of https://gitlab.inria.fr/gecos/gecos-tools/gecos-tools-isl into develop
* fixed config.mk in jniisl
* Update README.md
* recompiled binary for macosx64
* added support for stride info
* fixed barvinok feature having same id as isl feature
* [CI] pom file updates
* adding barvinok feature
* migrating barvinok to git
* migrating polylib bindings to git
* recompiled binaries for macosx64
* changed gmp location to /usr/local instead of complibs when compiling for macosx64
* fixed issues in recordError where UnsatisfiedLinkError was hidden by another exception
* updated Isl.map to use gmp and recompiled binaries for mac64
* Fix projectOutAllBut indexing
* start working on next version SNAPSHOT

## Release version 1.3.0 (04 juil. 2018)

* recompiled for macosx
* compile native for linux 64
* [Update] gecos-jnimapper to release version 1.1.0 and regenerated mapping
* Added min/max detection option for codegen
* Build native for linux_64
* adding set_dim to isl_multi_aff
* added isIdentity to JNIISLMultiAff
* reorganize tests and ignored TestISLCodeGen
* [CI] fix failure when the build has no tests
* fix previous commit: forgot to update gecos-buildtools
* [CI] gitlab-ci can now use variable 'JENKINS_FAIL_MODE' to specify the behaviour depending on test results:
* Build for linux_64 (update isl to version 0.19)
* added JNIMap.toSet that converts relations to an equivalent set that does not distinguish in/out dims
* ISL 0.19 binaries for maxosx64
* Isl.jnimap updated to support ISL 0.19
* fixes to ISLTest and PrettyPrinters to account for removed/deprecated functions in ISL 0.19
* modified mergeAsSingleSet in UnionSet to match the implementation with the comments

## Release version 1.2.0 (13 mar 2018)

* Added methods to directly obtain RAW/WAR/WAW dependencies
* buffer is now cleared (memset to 0) each time record_start is called in ISLUser_stdio.c
* Fix Jenkins pipeline configuration
* updates to recordError methods in JNIISLTools:
* Added methods to JNITools for wrapping calls to ISL (given as lambda functions) with error handling code that records stderr outputs. ISLFactory methods were updated to use this interface accordingly.
* moving jnimap.isl to Java8
* Merge branch 'develop' of https://gitlab.inria.fr/gecos/gecos-tools/gecos-tools-isl into develop
* applied the fix to jnimap generator (2nd time)
* Merge branch 'develop' of https://gitlab.inria.fr/gecos/gecos-tools/gecos-tools-isl into develop
* applied change to jnimap generator to move calls to taken into the try block instead of finally
* start working on next version SNAPSHOT

## Release version 1.1.0 (13 févr. 2018)

* [Releng] update dependency on gecos-tools-emf to version v1.0.1.
* [Releng] update gecos-buildtools to latest version.
* [Mod] re-compiled libraries for both linux64 and  macosx64.
* [Add] runtime checks to `ISLFactory` methods for building JNIISL objects from `String`.
  Giving null `String` was causing JVM to crash.
* [Mod] `ISLFactory` now use error recording to include isl error messages in the exceptions thrown when building of isl objects failed.
* [Add] bindings for `ISLMultiUnionPWAff`.
* [Add] missing support for `toMap()`, `flatProduct()` and `pullback()` in `ISLMultiAff`.
* [Rem] remove `flatten()` from `ISLPWMultiAff` (it is actually for `MultiAff`).
* [Add] `ISLAff` and `ISLMultiAff` to `ISLFactory`.
* [Rem] remove GMP binary for macosx64; GMP dependence has been removed.

---


## Release version 1.0.0 (06 Nov 2017)

Migrated ISL plugins from SVN repository at */trunk/tools/jnibindings/fr.irisa.cairn.jnimap.isl (rev 17883)*

This provides JNI bindings for ISL version 0.18 (git tag: isl-0.18-356-g0b05d01)


### Add
* JNIISL methods to record isl error messages (record stdout/stderr)

For the full previous changes history refer to the SVN repository.
---
